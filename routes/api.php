    <?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['basicauth','lang'],'namespace'=>'Api'],function(){
    Route::post('home', 'ReleaseController@index'); 
    Route::post('genres', 'ReleaseController@genres'); 
    Route::post('searching','ReleaseController@search');
    Route::post('song','SongController@index');
    Route::post('song_details','SongController@details');
    Route::post('artist', 'ArtistController@index');
    Route::post('artist_details', 'ArtistController@details');    
    Route::post('language','SongController@languages');
    
    Route::post('station','ActivityController@station');
    Route::post('radio','ActivityController@radio');
    

    Route::post('add_history','ActivityController@add_history');
    Route::post('history','ActivityController@history');
    Route::post('sharing','ActivityController@sharing');
    Route::post('views','ActivityController@views');    
    
    Route::post('register', 'UserController@register');
    Route::post('otpVerify', 'UserController@otpVerify');  
    Route::post('resendotp', 'UserController@resendOtp');  
    Route::post('login', 'UserController@login');    
    Route::post('country', 'UserController@country');
    
    
    Route::post('forgot_password', 'UserController@forgotPassword');
    Route::post('reset_password','UserController@resetPassword');  
    Route::post('commentList','ActivityController@commentList');

    Route::post('about-us','ActivityController@aboutUs');
    Route::post('privacy-policy','ActivityController@privacyPolicy');
    Route::post('terms-and-conditions','ActivityController@termsAndConditions');
    Route::post('help','ActivityController@help');
    
});


Route::group(['middleware' => ['auth:api','lang'],'namespace'=>'Api'], function(){  
    Route::post('song_details_list','SongController@details');
    Route::post('artist_details_list', 'ArtistController@details');
    Route::post('logout', 'UserController@logout');
    Route::post('profile', 'UserController@profile');
    Route::post('edit_profile','UserController@editProfile');    
    Route::post('add_favorite','ActivityController@addFavorite');
    Route::post('favorite','ActivityController@favorite');
    

    
    Route::post('playlist','ActivityController@playlist');
    Route::post('playlist_type', 'ActivityController@playlistType');    
    Route::post('add_playlist','ActivityController@addPlaylist');
    
    Route::post('rating','ActivityController@rating');
    Route::post('add_download','ActivityController@addDownload');
    Route::post('download','ActivityController@download');
    
    
    
    Route::post('follows','ActivityController@follows');
    
    Route::post('add_comment','ActivityController@addComment');
    Route::post('add_reply','ActivityController@addReply');    

    Route::post('rateus','ActivityController@rateus');
});
