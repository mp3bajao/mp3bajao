<?php
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});
Route::get('/config-cache', function() {
    Artisan::call('config:cache');
    return "Config is cleared";
});
Auth::routes(['verify' => true,'register' => false]);
Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale);
    return redirect()->back();
});

Route::get('/.well-known/pki-validation/F7693A6FAC808783F7B45E96DFC56112.txt', function (){
    echo  '2D5D215A3582BA17AAC4090BAFA670E21464F8351EBF3DCF9329931B4F12294A comodoca.com 607406fae97ef';
});


Route::group(['namespace'=>'Frontend'], function() {
    Route::get('/', 'HomeController@index')->name('home');      
    Route::get('/logout', 'HomeController@logout')->name('logout');
    Route::get('/genres', 'GenreController@index')->name('genres'); 
    Route::get('/artist', 'ArtistController@index')->name('artist'); 
    Route::get('/artist/load/{page?}', 'ArtistController@loading')->name('artist.loading'); 
    Route::get('/artist-details/{id?}', 'ArtistController@details')->name('artist.details'); 
    Route::get('/song', 'SongController@index')->name('song'); 
    Route::get('/song/load/{page?}/{slug?}/{value?}', 'SongController@loading')->name('song.loading');
    Route::get('/song-details/{id?}/{from?}', 'SongController@details')->name('song.details'); 
    Route::get('/album-details/{id?}', 'SongController@albumDetails')->name('album.details'); 
    Route::get('/playlist', 'ActivityController@playlist')->name('playlist');     
    Route::get('/favorites', 'ActivityController@favorites')->name('favorites');     
    Route::get('/history', 'ActivityController@history')->name('history');   
    Route::get('/searching/{search?}', 'ActivityController@searching')->name('searching'); 
    Route::get('/radio', 'ActivityController@radio')->name('radio'); 
    
        Route::get('/addViews', 'ActivityController@addViews')->name('addViews'); 
    
    Route::get('/comment-list/{table_type}/{table_id}/{page?}', 'ActivityController@commentList')->name('comment');   
    
    
    
    Route::get('/imageTowebp/{size?}', 'HomeController@imageTowebp')->name('imageTowebp');
    
    Route::get('/imageTowebpplaylist/{size?}', 'HomeController@imageTowebpPlaylist')->name('imageTowebpPlaylist');
    
    Route::get('/imageTowebpArtist/{size?}', 'HomeController@imageTowebpAtrist')->name('imageTowebpArtist');
    
    Route::get('/imageTowebpGeneres/{size?}', 'HomeController@imageTowebpGeneres')->name('imageTowebpGeneres');
    Route::get('/imageTowebpBanner/{size?}', 'HomeController@imageTowebpBanner')->name('imageTowebpBanner');
    
    Route::get('/about-us', 'HomeController@aboutus')->name('about-us');
    Route::get('/privacy-policy', 'HomeController@privacyPolicy')->name('privacy-policy');
    Route::get('/terms-and-conditions', 'HomeController@termsAndConditions')->name('terms-and-conditions');
});


Route::any('backpanel/sftp/multi_upload_album/{id}', 'Backend\SftpController@multiUploadAlbum')->name('sftp.multi_upload_album');


Route::group([ 'prefix' => 'backpanel', 'as' => 'backpanel.','namespace'=>'Backend', 'middleware' => ['auth','verified']], function() {    
    Route::get('/', 'HomeController@index')->name('dashboard');    
    Route::resource('api/roles', 'RoleController');
    Route::get('roles', 'RoleController@frontend')->name('roles');
    Route::get('roles/edit/{id}', 'RoleController@edit_frontend')->name('role.edit');

    
    Route::get('users', 'UsersController@index')->name('users.index');
    Route::any('users/create/', 'UsersController@create')->name('users.create');
    Route::any('users/edit/{id?}', 'UsersController@edit')->name('users.edit');
    Route::any('users/status/{id?}/{status?}', 'UsersController@status')->name('users.status');
    Route::any('users/view/{id?}', 'UsersController@show')->name('users.view');
    Route::any('users/destroy/{id?}', 'UsersController@destroy')->name('users.destroy');
    
    Route::get('staff', 'StaffController@index')->name('staff.index');
    Route::any('staff/create/', 'StaffController@create')->name('staff.create');
    Route::any('staff/edit/{id?}', 'StaffController@edit')->name('staff.edit');
    Route::any('staff/status/{id?}/{status?}', 'StaffController@status')->name('staff.status');
    Route::any('staff/view/{id?}', 'StaffController@show')->name('staff.view');
    Route::any('staff/destroy/{id?}', 'StaffController@destroy')->name('staff.destroy');
    

    Route::resource('api/content', 'ContentController');
    Route::get('content', 'ContentController@frontend')->name('content');
    Route::get('content/edit/{id}', 'ContentController@edit_frontend');
    Route::get('content/changeStatus/{id}/{status}', 'ContentController@changeStatus');
    Route::get('content/view/{id}', 'ContentController@show');

    
    Route::get('genres', 'GenreController@index')->name('genres.index');
    Route::any('genres/create/', 'GenreController@create')->name('genres.create');
    Route::any('genres/edit/{id?}', 'GenreController@edit')->name('genres.edit');
    Route::any('genres/status/{id?}/{status?}', 'GenreController@status')->name('genres.status');
    Route::any('genres/view/{id?}', 'GenreController@show')->name('genres.view');
    Route::any('genres/destroy/{id?}', 'GenreController@destroy')->name('genres.destroy');
    
    
    
    Route::get('chart', 'ChartController@index')->name('chart.index');
    Route::any('chart/create/', 'ChartController@create')->name('chart.create');
    Route::any('chart/edit/{id?}', 'ChartController@edit')->name('chart.edit');
    Route::any('chart/status/{id?}/{status?}', 'ChartController@status')->name('chart.status');
    Route::any('chart/view/{id?}', 'ChartController@show')->name('chart.view');
    Route::any('chart/destroy/{id?}', 'ChartController@destroy')->name('chart.destroy');
    
    Route::get('multi-chart', 'MultiChartController@index')->name('mulitchart.index');
    Route::get('multi-chart-list', 'MultiChartController@listIndex')->name('mulitchart.list');
    Route::get('multi-chart-list-banner', 'MultiChartController@listBannerIndex')->name('mulitchartbanner.list');
    Route::any('multi-chart/create/', 'MultiChartController@create')->name('mulitchart.create');
    Route::any('multi-chart/edit/{id?}', 'MultiChartController@edit')->name('mulitchart.edit');
    Route::any('multi-chart/status/{id?}/{status?}', 'MultiChartController@status')->name('mulitchart.status');
    Route::any('multi-chart/view/{id?}', 'MultiChartController@show')->name('mulitchart.view');
    Route::any('multi-chart/destroy/{id?}', 'MultiChartController@destroy')->name('mulitchart.destroy');
    
    
    Route::any('chart/add_chart_rank', 'ChartController@addChartRank')->name('chart.add_chart_rank');
    Route::any('chart/add_chart_song_rank', 'ChartController@addChartSongRank')->name('chart.add_chart_song_rank');
    
    Route::get('chart-song-list', 'ChartController@ChartSongindex')->name('chart.song.index');
    Route::get('music-list', 'ChartController@Musicindex')->name('chart.music.index');
    //Route::get('cart-artist-list', 'ChartController@ArtistIndex')->name('chart.artist.index');
    
    Route::get('cart-artist-song-list', 'ChartController@ArtistSongIndex')->name('chart.artist.song.index');
    
    Route::get('chart_search', 'ChartController@chartsearch')->name('chart.search');
    Route::get('chart_search_artist', 'ChartController@chartsearchArtist')->name('chart.search.artist');
    Route::get('add_top_list', 'ChartController@addTopLsist')->name('chart.add_top_list');
    
    Route::get('add_top_artist_list', 'ChartController@addTopArtistList')->name('chart.add_top_artist_list');
    Route::any('chart/add_chart_artist_rank', 'ChartController@addChartArtistRank')->name('chart.add_chart_artist_rank');
   
    
    
    
    Route::get('artist-list', 'ArtistController@indexList')->name('artist.index_list');
    Route::get('artist', 'ArtistController@index')->name('artist.index');
    Route::any('artist/create/', 'ArtistController@create')->name('artist.create');
    Route::any('artist/edit/{id?}', 'ArtistController@edit')->name('artist.edit');
    Route::any('artist/status/{id?}/{status?}', 'ArtistController@status')->name('artist.status');
    Route::any('artist/view/{id?}', 'ArtistController@show')->name('artist.view');
    Route::any('artist/destroy/{id?}', 'ArtistController@destroy')->name('artist.destroy');
    
    Route::get('banner', 'BannerController@indexList')->name('banner.index_list');
    Route::get('banner', 'BannerController@index')->name('banner.index');
    Route::any('banner/create/', 'BannerController@create')->name('banner.create');
    Route::any('banner/edit/{id?}', 'BannerController@edit')->name('banner.edit');
    Route::any('banner/status/{id?}/{status?}', 'BannerController@status')->name('banner.status');
    Route::any('banner/add_banner_rank', 'BannerController@addBannerRank')->name('banner.add_banner_rank');
    Route::any('banner/view/{id?}', 'BannerController@show')->name('banner.view');
    Route::any('banner/destroy/{id?}', 'BannerController@destroy')->name('banner.destroy');
    Route::get('banner_search', 'BannerController@bannersearch')->name('banner.search');
    
    
    Route::get('sub-genres', 'SubGenreController@index')->name('subgenres.index');
    Route::any('sub-genres/create/', 'SubGenreController@create')->name('subgenres.create');
    Route::any('sub-genres/edit/{id?}', 'SubGenreController@edit')->name('subgenres.edit');
    Route::any('sub-genres/status/{id?}/{status?}', 'SubGenreController@status')->name('subgenres.status');
    Route::any('sub-genres/view/{id?}', 'SubGenreController@show')->name('subgenres.view');
    Route::any('sub-genres/destroy/{id?}', 'SubGenreController@destroy')->name('subgenres.destroy');
    
    Route::get('playlist', 'PlaylistController@index')->name('playlist.index');
    Route::any('playlist/create/', 'PlaylistController@create')->name('playlist.create');
    Route::any('playlist/edit/{id?}', 'PlaylistController@edit')->name('playlist.edit');
    Route::any('playlist/status/{id?}/{status?}', 'PlaylistController@status')->name('playlist.status');
    Route::any('playlist/view/{id?}', 'PlaylistController@show')->name('playlist.view');
    Route::any('playlist/destroy/{id?}', 'PlaylistController@destroy')->name('playlist.destroy');
    Route::get('playlist/search', 'PlaylistController@playlistsearch')->name('playlist.search');
    Route::any('playlist/edit_index', 'PlaylistController@editIndex')->name('playlist.edit_index');
    Route::any('playlist/add_playlist_list', 'PlaylistController@addPlaylistList')->name('playlist.add_playlist_list');
    
    Route::get('store', 'StoreController@index')->name('store.index');
    Route::any('store/create/', 'StoreController@create')->name('store.create');
    Route::any('store/edit/{id?}', 'StoreController@edit')->name('store.edit');
    Route::any('store/status/{id?}/{status?}', 'StoreController@status')->name('store.status');
    Route::any('store/view/{id?}', 'StoreController@show')->name('store.view');
    Route::any('store/destroy/{id?}', 'StoreController@destroy')->name('store.destroy');
    
    Route::get('sftp', 'SftpController@index')->name('sftp.index');
    Route::any('sftp/folder_tree/{id?}', 'SftpController@folderTree')->name('sftp.folder_tree');
    Route::any('sftp/upload_album/{id?}', 'SftpController@uploadAlbum')->name('sftp.upload_album');
    Route::any('sftp/report/', 'SftpController@report')->name('sftp.report');
    
    Route::any('sftp/sftp_file_checker/', 'SftpController@AlbumChecker')->name('sftp.sftp_file_checker');
    
        

    
//    Route::post('/chart_import','SftpController@chartImport')->name('chart.import');
    
    //Route::post('media/delete', 'HomeController@mediaDelete')->name('ajax.media.delete');    
    Route::get('album', 'AlbumController@index')->name('album.index');
    Route::get('album/view', 'AlbumController@show')->name('album.show');
    Route::get('album/edit', 'AlbumController@edit')->name('album.edit');
    //Route::post('album/denine', 'AlbumController@denine')->name('album.denine');
    //Route::get('album/approve', 'AlbumController@approve')->name('album.approve');
    //Route::get('album/final', 'AlbumController@finalUpload')->name('album.final'); 
    //Route::get('album/queue', 'AlbumController@addQueue')->name('album.queue');
    //Route::get('album/resend', 'AlbumController@resend')->name('album.resend');
    //Route::get('album/final_yat', 'AlbumController@finalUploadYAT')->name('album.final_yat'); 
    //Route::get('album/final_sftp', 'AlbumController@finalUploadSFTP')->name('album.final_sftp');
    //Route::get('album/uploadStoreWise', 'AlbumController@finalUploadStoreWise')->name('album.uploadStoreWise');
    //Route::get('album/updateMetadataOnTiktok', 'AlbumController@updateMetadataOnTiktok')->name('album.updateMetadataOnTiktok');
    Route::get('album/track', 'AlbumController@track')->name('album.track');
    //Route::get('album/new', 'AlbumController@indexNew')->name('album.new.index');
    
    Route::get('album/create', 'AlbumController@create')->name('album.create');
    Route::get('album/view', 'AlbumController@show')->name('album.show');
    Route::post('album/delete', 'AlbumController@destroy')->name('album.delete');
    Route::get('album/update_status', 'AlbumController@updateStatus')->name('album.status');  
    Route::post('album/store', 'AlbumController@store')->name('album.store');
    Route::post('album/update/{id}/{type}', 'AlbumController@update')->name('album.update');    
    Route::post('ajax/subgenre', 'AlbumController@subgenreAjax')->name('ajax.subgenre');
    Route::post('ajax/formsubmit', 'AlbumController@formSubmitAjax')->name('ajax.formsubmit');
    Route::get('ajax/store/set', 'AlbumController@storeSet')->name('ajax.store.set');
    Route::get('ajax/store/selectall', 'AlbumController@storeSelectAll')->name('ajax.store.selectall');
    Route::get('ajax/store/view', 'AlbumController@storeView')->name('ajax.store.view');
    Route::get('album/submission', 'AlbumController@submissionView')->name('album.submission');
    Route::get('album/export', 'AlbumController@exportXls')->name('album.export');
    Route::get('album/importMetadata', 'AlbumController@importMetadata')->name('album.importMetadata');
    Route::get('album/transferUPC', 'AlbumController@transferUPC')->name('album.transferUPC');
    Route::post('album/transferUPCStore', 'AlbumController@transferUPCStore')->name('album.transferUPCStore');
    // Route::get('album/edit', 'AlbumController@edit')->name('release.edit');  
    
    
    Route::get('video', 'VideoController@index')->name('video.index');
    Route::get('video/view', 'VideoController@show')->name('video.show');
    Route::get('video/edit', 'VideoController@edit')->name('video.edit');
    //Route::post('video/denine', 'VideoController@denine')->name('video.denine');
    //Route::get('video/approve', 'VideoController@approve')->name('video.approve');
    //Route::get('video/final', 'VideoController@finalUpload')->name('video.final'); 
    //Route::get('video/queue', 'VideoController@addQueue')->name('video.queue');
    //Route::get('video/resend', 'VideoController@resend')->name('video.resend');
    //Route::get('video/final_yat', 'VideoController@finalUploadYAT')->name('video.final_yat'); 
    //Route::get('video/final_sftp', 'VideoController@finalUploadSFTP')->name('video.final_sftp');
    //Route::get('video/uploadStoreWise', 'VideoController@finalUploadStoreWise')->name('video.uploadStoreWise');
    //Route::get('video/updateMetadataOnTiktok', 'VideoController@updateMetadataOnTiktok')->name('video.updateMetadataOnTiktok');
    Route::get('video/track', 'VideoController@track')->name('video.track');
    //Route::get('video/new', 'VideoController@indexNew')->name('video.new.index');
    
    Route::get('video/create', 'AlbumController@create')->name('video.create');
    Route::get('video/view', 'AlbumController@show')->name('video.show');
    Route::post('video/delete', 'AlbumController@destroy')->name('video.delete');
    Route::get('video/update_status', 'AlbumController@updateStatus')->name('video.status');  
    Route::post('video/store', 'AlbumController@store')->name('video.store');
    Route::post('video/update/{id}/{type}', 'AlbumController@update')->name('video.update');    
    Route::post('ajax_video/subgenre', 'AlbumController@subgenreAjax')->name('ajax_video.subgenre');
    Route::post('ajax_video/formsubmit', 'AlbumController@formSubmitAjax')->name('ajax_video.formsubmit');
    Route::get('ajax_video/store/set', 'AlbumController@storeSet')->name('ajax_video.store.set');
    Route::get('ajax_video/store/selectall', 'AlbumController@storeSelectAll')->name('ajax_video.store.selectall');
    Route::get('ajax_video/store/view', 'AlbumController@storeView')->name('ajax_video.store.view');
    Route::get('video/submission', 'AlbumController@submissionView')->name('video.submission');
    Route::get('video/export', 'AlbumController@exportXls')->name('video.export');
    Route::get('video/importMetadata', 'AlbumController@importMetadata')->name('video.importMetadata');
    Route::get('video/transferUPC', 'AlbumController@transferUPC')->name('video.transferUPC');
    Route::post('video/transferUPCStore', 'AlbumController@transferUPCStore')->name('video.transferUPCStore');
    // Route::get('video/edit', 'AlbumController@edit')->name('release.edit');  
    
    
    
     /*video*/
    /*Route::post('media_video/delete', 'HomeController@mediaDelete')->name('ajax.media.video.delete');    
    Route::get('release_video', 'ReleaseVideoController@index')->name('video.index');
    Route::get('release_video/denine', 'ReleaseVideoController@denine')->name('video.denine');
    Route::get('release_video/approve', 'ReleaseVideoController@approve')->name('video.approve');
    Route::get('release_video/final', 'ReleaseVideoController@finalUpload')->name('video.final'); 
    Route::get('release_video/final_yat', 'ReleaseVideoController@finalUploadYAT')->name('video.final_yat');
    Route::get('release_video/final_sftp', 'ReleaseVideoController@finalUploadSFTP')->name('video.final_sftp');
    Route::get('release_video/track', 'ReleaseVideoController@track')->name('video.track');
    Route::get('release_video/new', 'ReleaseVideoController@indexNew')->name('video.new.index');
    Route::get('release_video/edit', 'ReleaseVideoController@edit')->name('video.edit');
    Route::get('release_video/create', 'ReleaseVideoController@create')->name('video.create');
    Route::get('release_video/view', 'ReleaseVideoController@show')->name('video.show');
    Route::post('release_video/delete', 'ReleaseVideoController@destroy')->name('video.delete');
    Route::get('release_video/update_status', 'ReleaseVideoController@updateStatus')->name('video.status');  
    Route::post('release_video/store', 'ReleaseVideoController@store')->name('video.store');
    Route::post('release_video/update/{id}/{type}', 'ReleaseVideoController@update')->name('video.update');    
    Route::post('ajax_video/subgenre', 'ReleaseVideoController@subgenreAjax')->name('ajax.video.subgenre');
    Route::post('ajax_video/formsubmit', 'ReleaseVideoController@formSubmitAjax')->name('ajax.video.formsubmit');
    Route::get('ajax_video/store/set', 'ReleaseVideoController@storeSet')->name('ajax.video.store.set');
    Route::get('ajax_video/store/selectall', 'ReleaseVideoController@storeSelectAll')->name('ajax.video.store.selectall');
    Route::get('ajax_video/store/view', 'ReleaseVideoController@storeView')->name('ajax.video.store.view');
    Route::get('release_video/submission', 'ReleaseVideoController@submissionView')->name('video.submission');
    Route::get('release_video/export', 'ReleaseVideoController@exportXls')->name('video.export');
    
    Route::get('albums_video', 'TrackVideoController@albums')->name('track.video.albums');
    Route::any('track_video/upload', 'TrackVideoController@trackUpload')->name('track.video.upload');
    Route::post('track_video/media/delete', 'TrackVideoController@trackDelete')->name('track.video.track_delete');
    */
    
    
    
    
    
    
    Route::get('track', 'TrackController@index')->name('track.index');
    Route::get('track-list/{release_id?}', 'TrackController@trackList')->name('track.list');
    Route::post('track/checker', 'TrackController@trackChecker')->name('track.checker');
//    Route::get('albums', 'TrackController@albums')->name('track.albums');
    Route::get('track/edit', 'TrackController@edit')->name('track.edit');
    Route::get('track/create', 'TrackController@create')->name('track.create');
    Route::get('track/view', 'TrackController@show')->name('track.show');
    Route::post('track/delete', 'TrackController@destroy')->name('track.delete');
    Route::get('track/update_status', 'TrackController@updateStatus')->name('track.status');
    Route::post('track/update_preview_time', 'TrackController@updatePreviewTime')->name('track.update_preview_time');
    Route::post('track/store/{id}/{type}', 'TrackController@store')->name('track.store');
    Route::post('track/update/{id}/{type}', 'TrackController@update')->name('track.update');
    
    Route::any('track/upload/{track_id?}', 'TrackController@trackUpload')->name('track.upload');
    
    Route::post('track/setup', 'TrackController@trackSetup')->name('track.setup');    
    Route::post('track/media/delete', 'TrackController@trackDelete')->name('track.track_delete');
//    Route::post('file_import', 'ImportController@importParent')->name('file.import');
//    Route::get('file_export', 'ImportController@excelParent')->name('file.export');
//    Route::post('file_import_metadata', 'ReleaseController@importMetadataStore')->name('file.importMetadata');
     
    
    
    
    
    Route::get('/roles_list', 'RoleController@roles_list');
    Route::get('/allroles', 'RoleController@allroles');
    Route::get('/permissions', 'PermissionController@index')->name('permissions');
    Route::get('/getRole', 'PermissionController@getRole');
    Route::get('/getPermissions/{id?}', 'PermissionController@getPermissions');
    Route::get('savePermission/{permission_id}/{role_id}', 'PermissionController@savePermission');
    Route::get('deletePermission/{permission_id}/{role_id}', 'PermissionController@deletePermission');
    Route::get('saveUserPermission/{permission_id}/{user_id}', 'PermissionController@saveUserPermission');
    Route::get('deleteUserPermission/{permission_id}/{user_id}', 'PermissionController@deleteUserPermission');

    Route::get('settings', 'SettingController@frontend')->name('settings');
    Route::post('/changePassword','SettingController@changePassword')->name('changePassword');
    Route::post('/sendVerificationLink','SettingController@sendVerificationLink')->name('sendVerificationLink');
    Route::post('/saveProfile','SettingController@saveProfile')->name('saveProfile');
    Route::get('/reset_email/{userid}/{token}','SettingController@emailUpdate')->name('reset.email');
    
    Route::get('permissions/user_listing', 'PermissionController@perm_userData')->name('ajax.permUserdata');
    Route::get('permissions/user_permissions/{id?}','PermissionController@user_permissions')->name('user_permissions'); 
    Route::get('permissions/add_role_permission/{r_id?}/{p_name?}', 'PermissionController@saveRolePermission')->name('add_role_permission');
    Route::get('permissions/delete_role_permission/{r_id?}/{p_name?}', 'PermissionController@deleteRolePermission')->name('delete_role_permission');
    Route::get('permissions/add_user_permission/{u_id?}/{p_name?}', 'PermissionController@saveUserPermission')->name('add_user_permission');
    Route::get('permissions/delete_user_permission/{u_id?}/{p_name?}', 'PermissionController@deleteUserPermission')->name('delete_user_permission');
    

    Route::get('station', 'StationController@index')->name('station.index');
    Route::any('station/create/', 'StationController@create')->name('station.create');
    Route::any('station/edit/{id?}', 'StationController@edit')->name('station.edit');
    Route::any('station/status/{id?}/{status?}', 'StationController@status')->name('station.status');
    Route::any('station/view/{id?}', 'StationController@show')->name('station.view');
    Route::any('station/destroy/{id?}', 'StationController@destroy')->name('station.destroy');
    Route::get('station/search', 'StationController@playlistsearch')->name('station.search');
    
    
    
    Route::get('program', 'ProgramController@index')->name('program.index');
    Route::any('program/create/', 'ProgramController@create')->name('program.create');
    Route::any('program/edit/{id?}', 'ProgramController@edit')->name('program.edit');
    Route::any('program/status/{id?}/{status?}', 'ProgramController@status')->name('program.status');
    Route::any('program/view/{id?}', 'ProgramController@show')->name('program.view');
    Route::any('program/destroy/{id?}', 'ProgramController@destroy')->name('program.destroy');
    Route::get('program/search', 'ProgramController@playlistsearch')->name('program.search');
    Route::any('program/edit_index', 'ProgramController@editIndex')->name('program.edit_index');
    Route::any('program/add_playlist_list', 'ProgramController@addPlaylistList')->name('program.add_playlist_list');
    Route::get('program/position/{id?}/{position?}', 'ProgramController@position')->name('program.position');
    
    Route::get('adds_program', 'AddsProgramController@index')->name('adds_program.index');
    Route::any('adds_program/create/', 'AddsProgramController@create')->name('adds_program.create');
    Route::any('adds_program/edit/{id?}', 'AddsProgramController@edit')->name('adds_program.edit');
    Route::any('adds_program/status/{id?}/{status?}', 'AddsProgramController@status')->name('adds_program.status');
    Route::any('adds_program/view/{id?}', 'AddsProgramController@show')->name('adds_program.view');
    Route::any('adds_program/destroy/{id?}', 'AddsProgramController@destroy')->name('adds_program.destroy');


    Route::get('radio', 'RadioController@index')->name('radio.index');
    Route::any('radio/create/', 'RadioController@create')->name('radio.create');
    Route::any('radio/edit/{id?}', 'RadioController@edit')->name('radio.edit');
    Route::any('radio/status/{id?}/{status?}', 'RadioController@status')->name('radio.status');
    Route::any('radio/view/{id?}', 'RadioController@show')->name('radio.view');
    Route::any('radio/destroy/{id?}', 'RadioController@destroy')->name('radio.destroy');
    Route::any('radio/add_radio_list', 'RadioController@addRadioList')->name('radio.add_radio_list');
    Route::get('radio_program_list/{id?}', 'RadioController@RadioProgramList')->name('radio.programList');
});
