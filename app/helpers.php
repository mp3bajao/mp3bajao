<?php
if (!function_exists('file_checker')) 
{
    function file_checker($file,$type=null) 
    {
        if (isset($file) && !empty($file) && file_exists(base_path() .'/public/uploads/'. $type.'/'.$file)) 
        {
            $images = url('uploads/'. $type.'/'.$file);
        }
        else 
        {
            $images = url('public/images/'.$type.'.png');            
        }
        return $images;
    }
}
if(!function_exists('notificationUserType')) {
 
    function notificationUserType() 
    {       
         $data = array('All'=>'1','User'=>'2',"Celebrity"=>'3');
        return $data;
    }
}

if(!function_exists('notificationType')) {
 
    function notificationType() 
    {       
         $data = array('Email'=>'1','SMS'=>'2',"PushNotification"=>'3','All'=>'4');
        return $data;
    }
}
function calculateDimensions($width, $height, $maxwidth, $maxheight) {

    if ($width != $height) {
        if ($width > $height) {
            $t_width = $maxwidth;
            $t_height = (($t_width * $height) / $width);
            //fix height
            if ($t_height > $maxheight) {
                $t_height = $maxheight;
                $t_width = (($width * $t_height) / $height);
            }
        } else {
            $t_height = $maxheight;
            $t_width = (($width * $t_height) / $height);
            //fix width
            if ($t_width > $maxwidth) {
                $t_width = $maxwidth;
                $t_height = (($t_width * $height) / $width);
            }
        }
    } else
        $t_width = $t_height = min($maxheight, $maxwidth);

    return array('height' => (int) $t_height, 'width' => (int) $t_width);
}
if (! function_exists('array_add')) {
    /**
     * Add an element to an array using "dot" notation if it doesn't exist.
     *
     * @param  array   $array
     * @param  string  $key
     * @param  mixed   $value
     * @return array
     */
    function array_add($array, $key, $value)
    {
        return Arr::add($array, $key, $value);
    }
}

/*
if (!function_exists('image_upload')) {
    function image_upload($file, $path) {
        $path = 'storage/app/public/' . $path . '/';
        $imgsize = getimagesize($file);
        $width = $imgsize[0];
        $height = $imgsize[1];
        $imgre = calculateDimensions($width, $height, 450, 450);
        $image = $file;
        $extension = $image->getClientOriginalExtension();
        $orignalname = $file->getClientOriginalName();
        $fileNameExt = time() . "-" . rand(1000, 9999);
        $fileName = $fileNameExt . '.' . $extension;               // renameing image
        $fileNamethum = $fileNameExt . '_thumb.' . $extension;
        if (in_array($extension, ['jpeg', 'jpg', 'JPG', 'JPEG', 'png', 'PNG', 'gif', 'GIF'])) {
            $thumb_img = Image::make($image->getRealPath())->resize($imgre['width'], $imgre['height']);
            $thumb_img->save($path . $fileNamethum, 100);
            $image->move($path, $fileName);
            return array(true, $path . $fileName, $path . $fileNamethum, $extension, $orignalname);
        } else {
            return array(false, "file should be in jpeg, jpg,png,gif format / double extension not allow.", '');
        }
    }
}*/



if (!function_exists('image_upload')) { 
    function image_upload($file, $pathName, $multipalImageName = null,$size=0.4) {
        $path = public_path().'/uploads/' . $pathName . '/';
        $newFolder = strtoupper(date('M'). date('Y')).'/';
        $folderPath			=	$path.$newFolder;
        if(!File::exists($folderPath)){
             File::makeDirectory($folderPath, $mode = 0777,true);
        }
        $imgsize = getimagesize($file);
        $width = $imgsize[0];
        $height = $imgsize[1];
        $imgre = calculateDimensions($width, $height, 450, 450);
        $image = $file;
        $extension = $image->getClientOriginalExtension();
        //$orignalname = $file->getClientOriginalName();       
        if($multipalImageName == null){
            $fileName = time().'-'.$pathName.'.'.$extension;
        }else{
            $fileName = time().'-'.$pathName.'-'.$multipalImageName.'.'.$extension;
        }
        
      
        if (in_array($extension, ['jpeg', 'jpg', 'JPG', 'JPEG', 'png', 'PNG', 'gif', 'GIF'])) {
            $image->move($folderPath, $fileName);
            
           
            $image_file = explode('/',$newFolder . $fileName);
            $img = file_get_contents(url('/uploads/'.$pathName.'/'.$newFolder . $fileName));
            $percent = $size;
            $im = imagecreatefromstring($img);
            $width = imagesx($im);
            $height = imagesy($im);
            $newwidth = $width * $percent;
            $newheight = $height * $percent;
            $thumb = imagecreatetruecolor($newwidth, $newheight);
            imagecopyresized($thumb, $im, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
            $file_name = str_replace('.'.$extension,'.webp',$image_file[1]);
            imagewebp($thumb, 'public/uploads/'.$pathName.'/'.$image_file[0].'/'.$file_name, 100);
            $storage = Storage::disk('s3')->putFileAs('uploads/'.$pathName.'/'.$image_file[0],'public/uploads/'.$pathName.'/'.$image_file[0].'/'.$file_name,$file_name);
            file_checker_and_delete('public/uploads/'.$pathName.'/'.$image_file[0].'/'.$file_name);                                  
            return array(true, $newFolder . $fileName , $extension, $fileName,$newFolder.$file_name);
        } else {
            return array(false, "file should be in jpeg, jpg,png,gif format / double extension not allow.", '');
        }
    }
}


function userUrl($url) {
    /*if (Auth::user()->userType->user_type == '2') {
        return url('subadmin/' . $url);
    }
    if (Auth::user()->userType->user_type == '3') {
        return url('subadmin/' . $url);
    }else if (Auth::user()->userType->user_type == '5') {
        return url('internship/' . $url);
    } else {*/
    
        return url('backpanel/' . $url);
    //}
}

function redirectUser($url) {
    /*if (Auth::user()->userType->user_type == '2') {
        return redirect("subadmin/" . $url);
    } else if (Auth::user()->userType->user_type == '3') {
        return redirect("subadmin/" . $url);
    } else if (Auth::user()->userType->user_type == '5') {
        return redirect("internship/" . $url);
    } else {*/
        return redirect("backpanel/" . $url);
    //}
}

function redirectRoute($name, $pream = null) {
    /*if (Auth::user()->userType->user_type == '2') {
        if (isset($pream)) {
            return redirect()->route('subadmin.' . $name, $pream);
        } else {
            return redirect()->route('subadmin.' . $name);
        }
    } else if (Auth::user()->userType->user_type == '3') {
        if (isset($pream)) {
            return redirect()->route('subadmin.' . $name, $pream);
        } else {
            return redirect()->route('subadmin.' . $name);
        }
    } else if (Auth::user()->userType->user_type == '5') {
        if (isset($pream)) {
            return redirect()->route('internship.' . $name, $pream);
        } else {
            return redirect()->route('internship.' . $name);
        }
    } else {
        if (isset($pream)) {
            return redirect()->route('backpanel.' . $name, $pream);
        } else {*/
            return redirect()->route('backpanel.' . $name);
        //}
    //}
}

function routeUser($name, $pream = null) {
    /*
    if (Auth::user()->userType->user_type == '2') {
        if (isset($pream)) {
            return route('subadmin.' . $name, $pream);
        } else {
            return route('subadmin.' . $name);
        }
    } else if (Auth::user()->userType->user_type == '3') {
        if (isset($pream)) {
            return route('subadmin.' . $name, $pream);
        } else {
            return route('subadmin.' . $name);
        }
    } else if (Auth::user()->userType->user_type == '5') {
        if (isset($pream)) {
            return route('internship.' . $name, $pream);
        } else {
            return route('internship.' . $name);
        }
    } else {*/
        if (isset($pream)) {
            return route('backpanel.' . $name, $pream);
        } else {
            return route('backpanel.' . $name);
        }
    //}
}

function routeView($name, $pream = null) {
    /*
    if (Auth::user()->userType->user_type == '2') {
        if (isset($pream)) {
            return route('subadmin.' . $name, $pream);
        } else {
            return route('subadmin.' . $name);
        }
    } else if (Auth::user()->userType->user_type == '3') {
        if (isset($pream)) {
            return route('subadmin.' . $name, $pream);
        } else {
            return route('subadmin.' . $name);
        }
    } else if (Auth::user()->userType->user_type == '5') {
        if (isset($pream)) {
            return route('internship.' . $name, $pream);
        } else {
            return route('internship.' . $name);
        }
    } else {*/
    if(isset(Auth::user()->id)){
        if (isset($pream)) {
            return view('backpanel.' . $name, $pream);
        } else {
            return view('backpanel.' . $name);
        }
    }
    else{
         return view('frontend.' . $name);
    }
}

function routeFormUser($name) {
    /*if (Auth::user()->userType->user_type == '2') {
        return "subadmin." . $name;
    } else if (Auth::user()->userType->user_type == '3') {
        return "subadmin." . $name;
    } else if (Auth::user()->userType->user_type == '5') {
        return "internship." . $name;
    } else {*/
        return "backpanel." . $name;
    //}
}





if (!function_exists('file_checker_and_delete')) {

    function file_checker_and_delete($file) {
        if (isset($file) && !empty($file) && file_exists(base_path() . '/' . $file)) {
            unlink($file);
        }
    }

}

if (!function_exists('makeDirectory')) {
    function makeDirectory($path, $mode = 0777, $recursive = false, $force = false)
    {
        if ($force)
        {
            if (!file_exists($path)) {
                return @mkdir($path, $mode, $recursive);
            }
        }
        else
        {
            if (!file_exists($path)) {
                return mkdir($path, $mode, $recursive);
            }
        }
}
}
if (!function_exists('makeDelete')) {
    function makeDelete($path)
    {
            if (file_exists($path)) {
                return rmdir($path);
            }
    }
}
if (!function_exists('renameDirectory')) {
    function renameDirectory($oldpath,$newpath)
    {
        rename($oldpath,$newpath);     
    }
}

function createEditAction($route, $para, $options='') {
    $html = '<a href="' . routeUser($route, $para) . '" data-toggle="tooltip" data-placement="top" title="' . __('admin_lang.edit') . '"  class="btn btn-xs btn-primary" '.$options.'><i class="fa fa-edit"></i></a>';
    //$html = '<a href="' . $url . '" class="btn btn-xs btn-success pencil_anchor">';
    //$html.= '<i class="fa fa-pencil"></i>';
    // $html.= '</a>';
    return $html;
}

function createChargeAction($id, $sub_exp_date) {
    $btn = "btn-success";
    $title = "Subscription expire on " . $sub_exp_date;
    if (strtotime($sub_exp_date) < strtotime(date('Y-m-d'))) {
        $btn = "btn-danger";
        $title = "Subscription expired on " . $sub_exp_date;
    }
    $html = '<a title="' . $title . '" href="javascript:void(0)" class="btn btn-xs ' . $btn . ' chargeamt" id="' . $id . '">';
    $html .= '<i class="fa fa-usd" aria-hidden="true"></i>';
    $html .= '</a>';
    return $html;
}

function createViewAction($route, $para,$options='') {
    $html = '<a href="' . routeUser($route, $para) . '" data-toggle="tooltip" data-placement="top" title="' . __('admin_lang.view') . '" class="btn btn-xs btn-warning" '.$options.'><i class="fa fa-eye"></i></a>';
    /* $html = '<a href="' . $url . '" class="btn btn-xs btn-success pencil_anchor">';
      $html.= '<i class="fa fa-eye"></i>';
      $html.= '</a>'; */
    return $html;
}
function createIntrestedAction($route, $para) {
    $html = '<a href="' . routeUser($route, $para) . '" data-toggle="tooltip" data-placement="top" title="' . __('admin_lang.interested') . '" class="btn btn-xs btn-default"><i class="fa fa-ticket "></i></a>';
    return $html;
}

function createDefaultAction($route, $para,$class,$icon,$title) {
    if($title=="denine"){
        $html = '<a data-target="#openModal" data-toggle="modal" data-placement="top" title="' . __('admin_lang.'.$title) . '" class="btn btn-xs btn-'.$class.' modal-trigger" data-route="' . routeUser($route, $para) . '" href="#openModal"><i class="fa fa-'.$icon.' "></i></a>';
    }else{
        $html = '<a onclick="return confirm(`Are you sure you want to '.$title.'?`)" href="' . routeUser($route, $para) . '" data-toggle="tooltip" data-placement="top" title="' . __('admin_lang.'.$title) . '" class="btn btn-xs btn-'.$class.'"><i class="fa fa-'.$icon.' "></i></a>';
    }
    return $html;
}

function createDeleteAction($route, $para, $base_route, $role = null) {
    $html = '<a href="javascript:void(0)" class="btn btn-xs btn-danger del" title="' . __('admin_lang.delete') . '" data-route="' . routeUser($route, $para) . '" data-base_url="' . routeUser($base_route, $role) . '">';
    $html .= '<i class="fa fa-trash"></i>';
    $html .= '</a>';
    return $html;
}



if (!function_exists('selectbox')) 
{
    function selectbox($name,$array,$findvalue='',$extraclass,$placeholder)
    {
        $html = '<select name="'.$name.'" class="form-control '.$extraclass.' '.$name.'">';
        $html .= '<option value="">--- Select'.ucfirst($placeholder).' ---</option>';
        foreach($array as $key => $value)
        {
             $html .= '<option value="'.$key.'">'.ucfirst($value).'</option>';
        }
        return $html .='</select>';
        
    }
}

if (!function_exists('url_checker')) {
    
   function url_checker($file,$server='s3',$path=null,$size=0.4,$fileType='') {
        if(!empty($file))
        {
            
            if($server=='public')
            {
                //$images = url('/').'/image.php?size='.$size.'&image='.base64_encode(url($path.$file));
                $images = url($path.$file);
            }
            else if($server == 'public_to_s3')
            {
                $images = Storage::disk('s3')->url($path.$file);      
            }
            else{
                if($fileType=='image')
                {
                    //$images = url('/').'/image.php?size='.$size.'&image='.base64_encode(Storage::disk('s3')->url($file));                
                    $images = Storage::disk('s3')->url($file);                
                }
                else{
                    $images = Storage::disk('s3')->url($file);                                    
                }
            }

        }
        else
        {
            $images = url('public/images/ablum.jpg');
        }
        return $images;
    }

    if (!function_exists('file_checker_and_delete')) {

    function file_checker_and_delete($file) {
        if (isset($file) && !empty($file) && file_exists(base_path() . '/' . $file)) {
            unlink(base_path() . '/' .$file);
        }
    }

}

    if (!function_exists('makeDirectory')) {
        function makeDirectory($path, $mode = 0777, $recursive = false, $force = false)
        {
            if ($force)
            {
                if (!file_exists($path)) {
                    return @mkdir($path, $mode, $recursive);
                }
            }
            else
            {
                if (!file_exists($path)) {
                    return mkdir($path, $mode, $recursive);
                }
            }
        }
    }
    if (!function_exists('makeDelete')) {
        function makeDelete($path)
        {
                if (file_exists($path)) {
                    return rmdir($path);
                }
        }
    }
    if (!function_exists('renameDirectory')) {
        function renameDirectory($oldpath,$newpath)
        {
            rename($oldpath,$newpath);     
        }
    }
    
    if (!function_exists('chatList')) {
        function chartList()
        {
            return  [
                'chart'=>'Top Chart',
                'trending'=>'Top Trending',
                'release'=>'Top Release',
                'artish'=>'Top Artist',
                'hindi'=>'Top Hindi',
                'punjabi'=>'Top Punjabi',
                'hip_hop'=>'Top Hip Hop',
                'ghzazals'=>'Top Ghzazals',
                'qawwalis'=>'Top Qawwalis',
                'folk'=>'Top Folk',
                'bhakti'=>'Top Bhakti',
                'international'=>'Top International',
                'bollywood'=>'Top Bollywood',
                'sufi'=>'Top Sufi',
                'children'=>'Top Childrens Music',
                'pop'=>'Top Pop',
                'dance'=>'Top Dance',
                'classicals'=>'Top Classicals',
                'rock'=>'Top Rock',
            ];
        }
    }
    
    if(!function_exists('apicurl')){
        function apicurl($route,$token_type,$parms){
            
            if($token_type=='Basic')
            {
                $token = 'Basic '.base64_encode("mp3bajao:mp3bajao@2020");
            }else{      
                $token = isset($_COOKIE['token'])? $_COOKIE['token']:null;
                $token = 'Bearer '.$token;

            }
           
            $curl = curl_init();
            $url  = config('app.url');
            curl_setopt_array($curl, array(
              CURLOPT_URL => $url.'api/'.$route,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 100,
              CURLOPT_TIMEOUT => 30000, // increase this
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,                
              CURLOPT_IPRESOLVE=> CURL_IPRESOLVE_V4,
              CURLOPT_FOLLOWLOCATION => 1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => json_encode($parms),
              CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "accept: application/json",
                "authorization: ".$token,
                "cache-control: no-cache",
                "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
                "postman-token: 01c96c8a-3fd5-052a-f6d7-177b43d82095",
                "Access-Control-Allow-Origin: *"
              ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);

            if ($err) {
              echo "cURL Error #:" . $err;
            } else {
                if($http_status==200)
                {
                   $response =  json_decode($response);
                   return  array('status'=>$response->status,'message'=>$response->message,'data'=>$response->data);                    
                }
                else if($http_status==401)
                {
                    return array('status'=>false,'message'=>'Unauthorized');
                }
                else if($http_status==500)
                {  
                   return array('status'=>false,'message'=>'Internal server error');; 
                }
                else if($http_status==404)
                {
                     return array('status'=>false,'message'=>'page not found'); 
                }                
            }
        }
    }
}
 
?>