<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Review extends Model
{    
    protected $table = 'review';
    protected $fillable = [
        'order_id','product_id','user_id','review','is_active'
    ];

    protected $hidden = [
        'updated_at'
    ];
   
}
