<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Sharing extends Model
{    
    protected $table = 'sharing';
    protected $fillable = [
        'table_type','table_id','user_id'
    ];

    protected $hidden = [
        'updated_at'
    ];
   
}
