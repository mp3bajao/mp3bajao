<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;
class Label extends Model {
    
    protected $primaryKey = 'id';
    public $table = "labels";
    //protected $appends = ['posted_by'];

    public function getModel($limit=null, $offset=null, $search=null, $orderby=null, $order=null) {
        $q = Label::select('labels.*','users.username')
                ->leftJoin('users', 'labels.user_id', '=','users.id')
                ->where('labels.status',1);
        
        if(Auth::user()->role_id==4){
            $q->where('labels.sub_user_id',Auth::user()->id);
        }elseif(Auth::user()->role_id==3){
            $q->where('labels.sub_user_id',Auth::user()->id);
        }elseif(Auth::user()->role_id==2){
            $q->where('labels.user_id',Auth::user()->id);
        }
       
        $orderby = $orderby ? $orderby : 'labels.created_at';
        $order = $order ? $order : 'desc';
        if ($search && !empty($search)) {
            $q->where(function($query) use ($search) {
                $query->whereRaw('LOWER(labels.title) ' . 'LIKE ' . '"%' . strtolower($search) . '%"')
                      ->orWhereRaw('LOWER(users.username) ' . 'LIKE ' . '"%' . strtolower($search) . '%"');
            });
        }
        $response = $q->orderBy($orderby, $order);
        return $response;
    }

    public static function getAccessLabel(){
        if(Auth::user()->role_id==1){
            $two_level = User::where('status','1')->where('parent_id',0)->pluck('id')->toArray();
            $users = array_merge($two_level,[0]);
            $labels = Label::whereIn('sub_user_id',$users)->pluck('title','id')->toArray();
        }elseif(Auth::user()->role_id==2){
            $third_level = User::where('status','1')->where('parent_id',Auth::user()->id)->pluck('id')->toArray();
            $users = array_merge($third_level,[Auth::user()->id]);
            $labels = Label::whereIn('sub_user_id',$users)->pluck('title','id')->toArray();
        }
        return $labels;
    }
}