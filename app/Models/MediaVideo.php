<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;
class MediaVideo extends Model {
    
   /* protected $fillable = [
        'table_name','table_column_id','file_tyle','type','file','file_thumb','file_duration','orignal_name',
    ];*/
    protected $primaryKey = 'media_id';
    public $table = "medias_videos";
 
    public function getFileAttribute($value) {
         return  url_checker($value); 
    }
}