<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;
class TopChart extends Model {
    
    protected $primaryKey = 'id';
    
    public $table = "top_charts"; 
    protected $appends = ['track_count'];

    public function getModel($limit=null, $offset=null, $search=null, $orderby=null, $order=null,$type) 
    {
        $q = $this->where('chart_id',$type);       
        $orderby = $orderby ? $orderby : 'artist.created_at';
        $order = $order ? $order : 'desc';
        if ($search && !empty($search)) {
            $q->where(function($query) use ($search) {
                $query->whereRaw('LOWER(top_charts.upc) ' . 'LIKE ' . '"%' . strtolower($search) . '%"');
            });
        }
        $response = $q->orderBy($orderby, $order);
        return $response;
    }
    
    
   /* public function getCombTrack()
    {
        if($this->chart_type=='SINGLE')
        {
            if($top_chart->format=='Album')
            {
               return $this->hasOne("App\Models\Release", 'id', 'track_id');  
            }
            else
            {
               return $this->hasOne("App\Models\Track", 'id', 'track_id');
            }
            
        }
        elseif($this->chart_type=='MULTIPLE')
        {
              return $this->hasOne("App\Models\PlaylistCategory", 'id', 'track_id');
        }
        
        return $this->morphTo();
    }*/
    
    
    public function getTrack() {
        return $this->hasOne("App\Models\Track", 'id', 'track_id')->TrackCount();
    }
    public function getRelease() 
    {
        return $this->hasOne("App\Models\Release", 'id', 'track_id')->TrackCount();
    }
    
    public function getPlaylistCategory() {
        return $this->hasOne("App\Models\PlaylistCategory", 'id', 'track_id')->TrackCount();
    }
    
    public function getMultiChart() {
        return $this->hasOne("App\Models\MultiChart", 'id', 'chart_group_id');
    }
    
    public function getTrackCountAttribute(){
        if($this->format=='Track')
        {
            return 1;
        }
        else if($this->format=='Album')
        {
            return Track::where('release_id',$this->track_id)->count(); 
        }
        else
        {
           return $this->getPlaylistCategory->track_count; 
        }
        
    }
}