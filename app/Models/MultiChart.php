<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;
class MultiChart extends Model {
    
    protected $primaryKey = 'id';
    
    public $table = "multi_charts";
     protected $fillable = [
        'name','image','status'
    ];
    //protected $appends = ['posted_by'];

    public function getModel($limit=null, $offset=null, $search=null, $orderby=null, $order=null) 
    {
        $q = $this->select('multi_charts.*')->where('status',0);
       
        $orderby = $orderby ? $orderby : 'artist.created_at';
        $order = $order ? $order : 'desc';
        if ($search && !empty($search)) {
            $q->where(function($query) use ($search) {
                $query->whereRaw('LOWER(multi_charts.title) ' . 'LIKE ' . '"%' . strtolower($search) . '%"');
            });
        }

        $response = $q->orderBy($orderby, $order);
        return $response;
    }    
    public function getImageAttribute($value) {
        return url_checker($value,'public','uploads/category/');    
    }
}