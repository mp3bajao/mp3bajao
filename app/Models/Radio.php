<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;
class Radio extends Model {
    
    protected $primaryKey = 'id';
    
    public $table = "radios";
    
    protected $fillable = [
        'name','station_id','name','start_time','end_time','week'   ,'status'
    ]; 
    
    public function getRadioList() {
        return $this->HasMany("App\Models\RadioPlaylist", 'radio_id', 'id');
    }
}