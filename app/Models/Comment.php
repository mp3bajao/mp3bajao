<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;
class Comment extends Model {    
    protected $primaryKey = 'id';    
    public $table = "comments";
    protected $fillable = [
        'user_id','comment_id','reply'
    ];
    
    public function getUser()
    {
        return $this->HasOne("App\User", 'id', 'user_id');
    }
    public function getReply()
    {
        return $this->HasMany("App\Models\Reply", 'comment_id', 'id')->orderBy('id','desc');
    }
}