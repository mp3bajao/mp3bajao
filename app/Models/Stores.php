<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stores extends Model
{
   protected $primaryKey = 'id';
   public $table = "stores";
    protected $fillable = [
        'title','data-exchange-id','data-destination-party-id','receiver','image','sender','username','password','status'
    ];
   /*public function getImageAttribute($value){
    return $value;
   }*/
}
