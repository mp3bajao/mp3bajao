<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;
class Banner extends Model {
    
    protected $primaryKey = 'id';
    
    public $table = "banners";
    protected $fillable = [
        'name','image','status','chart_type','image_webp'
    ];
    protected $appends = ['track_list'];
    //protected $appends = ['posted_by'];

    public function getModel($limit=null, $offset=null, $search=null, $orderby=null, $order=null) {
        $q = $this->select('banners.*')->where('status',0);
       
        $orderby = $orderby ? $orderby : 'banners.created_at';
        $order = $order ? $order : 'desc';
        if ($search && !empty($search)) {
            $q->where(function($query) use ($search) {
                $query->whereRaw('LOWER(banners.title) ' . 'LIKE ' . '"%' . strtolower($search) . '%"');
            });
        }

        $response = $q->orderBy($orderby, $order);
        return $response;
    }
    
    public function getImageAttribute($value)
    {
        if($this->image_webp)
        {
            return url_checker($this->image_webp,'public_to_s3','uploads/category/',0.3);                
        }
        else
        {
            return url_checker($value,'public','uploads/category/',0.3);                
        }
    }  
    
    public function getTrackListAttribute($value)
    {
        if($this->chart_type=='SINGLE'){
            if($this->format=='Track'){
                $track  =  Track::where('id',$this->upc)->first();
                return $track->title ?? '-' ;
            }
            else
            {
                $track  =  Release::where('id',$this->upc)->first();
                return $track->title  ?? '-'    ;
            }
        }
        else{
            $playlist  = PlaylistCategory::where('id',$this->upc)->first();
            return $playlist->name ?? '-';
        }
    }
}