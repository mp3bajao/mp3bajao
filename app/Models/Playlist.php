<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Http\Resources\Track as TrackResource;

class Playlist extends Model
{    
    protected $table = 'playlists';
    
    protected $hidden = [
        'updated_at'
    ];
    
    protected $fillable = 
    [
       'user_id','playlist_type','track_id','status'
    ];
    
    public function getTrack() 
    {        
       return  $this->HasOne("App\Models\Track", 'id', 'track_id');
    }   
}
