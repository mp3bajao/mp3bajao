<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UPCVIDEO extends Model
{
   protected $primaryKey = 'id';
   public $table = "upcs_video";
}
