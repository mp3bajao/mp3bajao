<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Notification extends Model
{    
    protected $table = 'notifications';
    protected $fillable = [
        'user_type','notification_type','title','message','user_ids','celebrity_ids',
    ];

    protected $hidden = [
        'updated_at'
    ];
   
}
