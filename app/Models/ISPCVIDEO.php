<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ISPCVIDEO extends Model
{
   protected $primaryKey = 'id';
   public $table = "ispcs_videos";
}
