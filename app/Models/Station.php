<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;
class Station extends Model {
    
    protected $primaryKey = 'id';
    
    public $table = "stations";
    
    protected $fillable = [
        'name','state_id','status'
    ];   
}