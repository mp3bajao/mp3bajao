<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Models\StoreRelease;

class ReleaseVideo extends Model {
    
    protected $primaryKey = 'id';
    public $table = "releases_videos";
    protected $appends = ['genre_value','subgenre_value','label_value','track','price_value'];

    public function getModel($limit=null, $offset=null, $search=null, $orderby=null, $order=null,$type=null,$current_status,$user_id='') {
        $status = array(0);
        $q = ReleaseVideo::select('releases_videos.*');
        if(!empty($user_id))
        {
          $q->where('releases_videos.user_id',$user_id);  
        }
        if($current_status!='All')
        {
          $q->where('releases_videos.release_current_status',$current_status);       
        }
        if(Auth::user()->role_id==1){
            if($type=='final'){
                $q->whereIn('releases_videos.status',[3,4]);
            }elseif($type=='correct'){
                $q->where('releases_videos.status',2); 
            }else{
                $q->where('releases_videos.status',1);    
            }
            if(Auth::user()->is_same_level==1 && Auth::user()->allowed_label_id!=""){
                $allowed_labels = User::getAllowedLabels();
                //$allowed_users = User::getAllowedUsers();
                $q->whereIn('label_id',$allowed_labels);
            }
        }else if(Auth::user()->role_id==2){
            ($type == 'all' ? $status = array(1,3,4) : ($type == 'finalize' ? $status = array(0) :($type == 'correct' ? $status=array(2):($type == 'processing' ? $status=array(1):array(0))) ));
            if(Auth::user()->is_same_level==1 && Auth::user()->allowed_label_id!=""){
                $my_user_id = Auth::user()->parent_id;
                $allowed_labels = User::getAllowedLabels();
                $q->whereIn('label_id',$allowed_labels);
            }elseif(Auth::user()->is_same_level==1){
                $my_user_id = Auth::user()->parent_id;
            }else{
                $my_user_id = Auth::user()->id;
            }
            $children_users = User::getChildrenUsers($my_user_id,2);
            $children_users= array_merge($children_users,[$my_user_id]);
            $q->whereIn('sub_user_id',$children_users);
            $q->whereIn('status',$status);
        }elseif(Auth::user()->role_id==3){
            ($type == 'all' ? $status = array(1,3,4) : ($type == 'finalize' ? $status = array(0) :($type == 'correct' ? $status=array(2):($type == 'processing' ? $status=array(1):array(0))) ));
            $q->whereRaw('(sub_user_id='.Auth::user()->id." OR user_id=".Auth::user()->id.")")->whereIn('status',$status);
        }elseif(Auth::user()->role_id==4){
            ($type == 'all' ? $status = array(1,3,4) : ($type == 'finalize' ? $status = array(0) :($type == 'correct' ? $status=array(2):($type == 'processing' ? $status=array(1):array(0))) ));
            $q->where('sub_user_id',Auth::user()->id)->whereIn('status',$status);
        }
                

       
        if ($search && !empty($search)) {
            $q->where(function($query) use ($search) {
                $query->whereRaw('LOWER(releases_videos.title) ' . 'LIKE ' . '"%' . strtolower($search) . '%"')
                        ->orWhereRaw('LOWER(releases_videos.producer_catalogue_number) ' . 'LIKE ' . '"%' . strtolower($search) . '%"')
                        ->orWhereRaw('LOWER(releases_videos.upc_temp) ' . 'LIKE ' . '"%' . strtolower($search) . '%"')
                        ->orWhereRaw('LOWER(releases_videos.UPC) ' . 'LIKE ' . '"%' . strtolower($search) . '%"')
                        ->orWhereRaw('LOWER(releases_videos.id) ' . 'LIKE ' . '"%' . strtolower($search) . '%"')
                        ->orWhereRaw('LOWER(releases_videos.producer_catalogue_number) ' . 'LIKE ' . '"%' . strtolower($search) . '%"');
            });
        }
        if($type=='final'){
            $orderby = $orderby ? 'releases_videos.approve_at' : 'releases_videos.approve_at';
            $order = $order ? $order : 'desc';
        }else{
            $orderby = $orderby ? $orderby : 'releases_videos.approve_at';
            $order = $order ? $order : 'desc';   
        }
        $response = $q->orderBy($orderby, $order);
        
       /* if($type=='final'){          
            $orderby = $orderby ? 'releases_videos.approve_at' : 'releases_videos.approve_at';
            $order = $order ? $order : 'desc';
            }else{
              $orderby = $orderby ? $orderby : 'releases_videos.created_at';
              $order = $order ? $order : 'desc';   
            }
        $response = $q->orderBy($orderby, $order);
        */
        return $response;
    }
    public function getTrackModel($limit=null, $offset=null, $search=null, $orderby=null, $order=null,$id=null) {
        $q =  Track::select('tracks.*')->where('release_id',$id);
               
        $orderby = $orderby ? $orderby : 'tracks.created_at';
        $order = $order ? $order : 'asc';
        if ($search && !empty($search)) {
            $q->where(function($query) use ($search) {
                $query->whereRaw('LOWER(tracks.title) ' . 'LIKE ' . '"%' . strtolower($search) . '%"');
            });
        }
        $response = $q->orderBy($orderby, $order);
        return $response;
    }
    
    
    public function getAlbumProfileAttribute($value) {
        return url_checker($value);    
    }
    public function getGenreValueAttribute($value) {
        $data = Genre::select('title')->where('id',$this->genre_id)->first();        
        return isset($data->title)?$data->title:'';    
    }
    public function getSubgenreValueAttribute($value) {
        $data = Subgenre::select('title')->where('id',$this->subgenre_id)->first();        
        return isset($data->title)?$data->title:'';    
    }
    public function getLabelValueAttribute($value) {
        $data = Label::select('title')->where('id',$this->label_id)->first();        
        return isset($data->title)?$data->title:'';    
    }
    /*public function getTrackAttribute($value) {
        $data = Track::where('release_id',$this->id)->count();        
        return isset($data)?$data:0;    
    }*/
    
    public function getMedia() {
        return $this->hasOne("App\Models\MediaVideo", 'album_id', 'id');   
    }
    
    public function getPriceValueAttribute($value) {
        $data = Price::select('title')->where('id',$this->price_id)->first();        
        return isset($data->title)?$data->title:0;    
    }
    public function getUser() {
        return $this->BelongsTo("App\Models\User", 'user_id', 'id');
    }
    public function getReleaseTrack() {
        return $this->hasMany("App\Models\Track", 'release_id', 'id');
    }
    public function getStoreRelease() {
        return $this->hasMany("App\Models\StoreReleaseVideo", 'release_id', 'id')->where('status',1);
    }
    public function getAllStoreRelease() {
        return $this->hasMany("App\Models\StoreReleaseVideo", 'release_id', 'id');
    }
    public function getStoreReleaseSend() {
        return $this->hasMany("App\Models\StoreReleaseVideo", 'release_id', 'id')->where('status',1)->where('is_send',1);
    }
}