<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Http\Resources\Track as TrackResource;

class History extends Model
{    
    protected $table = 'histories';
    

    protected $hidden = [
        'updated_at'
    ];
    
    public function getTrack() {
        
       return  $this->HasOne("App\Models\Track", 'id', 'track_id');
    }
   
}
