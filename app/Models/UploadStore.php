<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class UploadStore extends Model
{
   public $table = "upload_stores";
   
   protected $dates = [
        'created_at',
        'updated_at',
    ];

   protected $fillable = [
        'id',
        'store_id',
        'folder_name',
        'status',
   ];  
   
   public function getCreatedAtAttribute($value){
       return date('d M, Y h:i A',strtotime($value));
   }
}
