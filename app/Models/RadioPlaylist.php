<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;
class RadioPlaylist extends Model {
    
    protected $primaryKey = 'id';
    
    public $table = "radio_playlist";
    
    protected $fillable = [
        'radio_id','table_type','table_id','duration','id'
    ];   
    
    public function getProgram() 
    {
        return $this->hasOne("App\Models\ProgramCategory", 'id', 'table_id');
    }

}