<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Log extends Model
{
    protected $tab  ='logs';     
    protected $hidden = [
        'updated_at', 'deleted_at',
    ];
}
