<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;
class Genre extends Model {
    
    protected $primaryKey = 'id';
    
    public $table = "genres";
     protected $fillable = [
        'name','description','status','image','image_webp'
    ];
    
    //protected $appends = ['posted_by'];

    public function getModel($limit=null, $offset=null, $search=null, $orderby=null, $order=null) {
        $q = $this->select('genres.*')->where('status',0);
       
        $orderby = $orderby ? $orderby : 'genres.created_at';
        $order = $order ? $order : 'desc';
        if ($search && !empty($search)) {
            $q->where(function($query) use ($search) {
                $query->whereRaw('LOWER(genres.title) ' . 'LIKE ' . '"%' . strtolower($search) . '%"');
            });
        }

        $response = $q->orderBy($orderby, $order);
        return $response;
    }
    
    public function getImageAttribute($value)
    {
        if(!empty($this->image_webp))
        {
            return url_checker($this->image_webp,'public_to_s3','uploads/category/',0.3);                
        }
        else
        {
            return url_checker($value,'public','uploads/category/',0.3);                
        }
    }

}