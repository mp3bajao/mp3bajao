<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Http\Resources\Track as TrackResource;

class PlaylistCategory extends Model
{    
    protected $table = 'playlist_categories';
    
    protected $fillable = [
       'name','image','user_id','status','image_webp'
    ];
    
    protected $appends = ['track_count','is_like','total_like','total_sharing','total_download','rating',];
     
    protected $hidden = [
        'updated_at'
    ];  
    
       public function scopeTrackCount($query){ 
        $query->select('playlist_categories.*')
                ->selectRaw('(select count(*) from playlists where playlists.playlist_type = playlist_categories.id) as track_count')
                ->selectRaw('(select count(*) from comments where comments.table_type="Playlist" and comments.table_id = playlist_categories.id) as total_comment')
                ->selectRaw('(select (ip_address) from sharing where sharing.table_type = "Playlist" AND sharing.table_id = playlist_categories.id) as total_sharing')          
                ->selectRaw('(select (ip_address) from views where table_type = "Playlist" and  table_id = playlist_categories.id) as total_views')
                ->selectRaw('(select avg(rating) from rating where rating.type="Playlist" and rating.track_id = playlist_categories.id) as rating')
                ->selectRaw('(select count(*) from favorites where favorites.type="Playlist" and favorites.status=1 and favorites.track_id = playlist_categories.id) as total_like');
        
    }
    
     public function getTotalSharingAttribute($value) {
       // $data = Sharing::where('table_type','Playlist')->where('table_id',$this->id)->first();
        return isset($value) ? count(explode(',',$value)) : 0;
    }
    public function getTotalViewsAttribute($value) {
        return $value ?? 0;   
    }
    
    public function getIsLikeAttribute($value)
    {
        if(isset(Auth::user()->id))
        {
            return  Favorite::where('track_id',$this->id)->where('type','Playlist')->where('user_id',Auth::user()->id)->count();
        }
        else
        {
            return 0;
        }
    }
    
    
    public function getTotalLikeAttribute($value)
    {
        return $value ?? 0;
       //return  Favorite::where('track_id',$this->id)->where('type','Playlist')->count();
    }
  
    public function getTotalCommentAttribute($value)
    {
        return $value ?? 0;
    }
    public function getTotalDownloadAttribute($value)
    {
        return 0;
    }
    public function getRatingAttribute($value)
    {
        return $value ?? 0;
        //return Rating::where('type','Playlist')->where('track_id',$this->id)->avg('rating');
    }
    
    public function getImageAttribute($value)
    {
        if(isset($this->image_webp))
        {
            return url_checker($this->image_webp,'public_to_s3','uploads/category/',.2);            
        }
        else
        {
            return url_checker($value,'public','uploads/category/',.2);
        }
    }
    
    public function getSinglePlaylist()
    {
        return $this->HasOne("App\Models\Playlist", 'playlist_type', 'id');
    }
    public function getPlaylist()
    {
        return $this->HasMany("App\Models\Playlist", 'playlist_type', 'id');
    }
    public function getUser()
    {
        return $this->HasOne("App\User", 'id', 'user_id');
    }
    
    public function  getTrackCountAttribute($value)
    {
        return $value ?? 0;
       // return Playlist::where('playlist_type',$this->id)->count();
    }
}
