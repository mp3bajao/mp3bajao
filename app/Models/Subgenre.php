<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;
class Subgenre extends Model {
    
    protected $primaryKey = 'id';
    public $table = "subgenre";
     protected $fillable = [
        'title','genre_id','status'
    ];
    //protected $appends = ['posted_by'];

    

}