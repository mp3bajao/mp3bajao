<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Favorite;
use App\Models\Playlist;
use Auth;
class Track extends Model {
    
    protected $primaryKey = 'id';
    public    $table = "tracks";
    protected $appends = ['language_name','lyrics_language_name','is_favorite','is_playlist','rating','total_like','total_sharing','total_views'];
    
    protected  $fillable = ['release_id','ISRC','is_upload'];

    public function getModel($limit=null, $offset=null, $search=null, $orderby=null, $order=null,$release_id=null) {
        $q = Track::select('tracks.*');
        if(!empty($release_id))
        {
            $q->where('release_id',$release_id);                            
        }
            
        $orderby = $orderby ? $orderby : 'tracks.created_at';
        $order = $order ? $order : 'desc';
        if ($search && !empty($search)) {
            $q->where(function($query) use ($search) {
                $query->whereRaw('LOWER(tracks.title) ' . 'LIKE ' . '"%' . strtolower($search) . '%"');
            });
        }

        $response = $q->orderBy($orderby, $order);
        return $response;
    }
    

    public function getMediaModel($limit=null, $offset=null, $search=null, $orderby=null, $order=null,$release_id=null) {
        $q = Media::select('medias.*')->where('album_id',$release_id);                
        $orderby = $orderby ? $orderby : 'medias.created_at';
        $order = $order ? $order : 'asc';
        if ($search && !empty($search)) {
            $q->where(function($query) use ($search) {
                $query->whereRaw('LOWER(medias.orignal_name) ' . 'LIKE ' . '"%' . strtolower($search) . '%"');
            });
        }

        $response = $q->orderBy($orderby, $order);
        return $response;
    }
    
    public function getMediaVideoModel($limit=null, $offset=null, $search=null, $orderby=null, $order=null,$release_id=null) {
        $q = MediaVideo::select('medias_videos.*')->where('medias_videos.album_id',$release_id);                
        $orderby = $orderby ? $orderby : 'medias_videos.created_at';
        $order = $order ? $order : 'asc';
        if ($search && !empty($search)) {
            $q->where(function($query) use ($search) {
                $query->whereRaw('LOWER(medias_videos.orignal_name) ' . 'LIKE ' . '"%' . strtolower($search) . '%"');
            });
        }

        $response = $q->orderBy($orderby, $order);
        return $response;
    }
    
      public function scopeTrackCount($query){
        return $query->select('tracks.*')
               // ->selectRaw('(select count(*) from tracks where tracks.release_id = releases.id) as track_count')
               // ->selectRaw('(select count(*) from tracks where tracks.release_id = releases.id) as track')
                ->selectRaw('(select (ip_address) from sharing where sharing.table_type = "Track" AND sharing.table_id = tracks.id) as total_sharing')
                ->selectRaw('(select (name) from genres where genres.id = tracks.genre_id) as genre_value')
                ->selectRaw('(select (title) from subgenre where subgenre.id = tracks.subgenre_id) as subgenre_value')
            ->selectRaw('(select count(*) from comments where comments.table_type="Track" and comments.table_id = tracks.id) as total_comment')                               
                // ->selectRaw('(select (title) from labels where labels.id = tracks.label_id) as label_value')
                ->selectRaw('(select (title) from languages where languages.id = tracks.title_language) as language_name')
                ->selectRaw('(select (title) from languages where languages.id = tracks.lyrics_language) as lyrics_language_name')
                ->selectRaw('(select sum(ip_address) from views where table_type = "Track" and  table_id = tracks.id) as total_views')
                ->selectRaw('(select avg(rating) from rating where rating.type="Track" and rating.track_id = tracks.id) as rating')
                ->selectRaw('(select count(*) from favorites where favorites.type="Track" and favorites.status=1 and favorites.track_id = tracks.id) as total_like');
        
    }
    
    /*public function getPrimaryArtistAttribute($value){
        if(count(explode('|',$value))>1)
        {
            return 'various artists';
        }
        else
        {
            return $value;             
        }
        return $value; 
    }*/
    
    public function getTotalLikeAttribute($value)
    {
        //return Favorite::where('track_id',$this->id)->where('type','Track')->where('status',1)->count();
        return $value ?? 0;
    }
    
    public function getTotalSharingAttribute($value) {
        //$data = Sharing::where('table_type','Track')->where('table_id',$this->id)->first();
        return isset($value) ? count(explode(',',$value)) : 0;
    }
    public function getTotalViewsAttribute($value) {
//      $data = Views::where('table_type','Track')->where('table_id',$this->id)->first();
 //     return  isset($data) ? $data->ip_address : 0; 
        return $value ?? 0;
    }
    
    public function getIsFavoriteAttribute($value){
        if(isset(Auth::user()->id))
        {
            return Favorite::where('track_id',$this->id)->where('user_id',Auth::user()->id)->where('type','Track')->where('status',1)->count();
        }
        else
        {
            return 0;
        }
    }
    
    
    public function getIsPlaylistAttribute($value){
        if(isset(Auth::user()->id))
        {
            return Playlist::where('track_id',$this->id)->where('user_id',Auth::user()->id)->count();
        }
        else
        {
            return 0;
        }
    }

    public function getLanguageNameAttribute($value) {
      //  $data = Language::select('title')->where('language_code',$this->title_language)->first();        
        return isset($value)?$value:0;    
    }
    
    public function getLyricsLanguageNameAttribute($value) {
       // $data = Language::select('title')->where('language_code',$this->lyrics_language)->first();        
        return isset($value)?$value:0;    
    }
    public function getRatingAttribute($value) {
        return $value ?? 0;
//        return   Rating::where('track_id',$this->id)->where('type','Track')->avg('rating');               
    }
    
    public function getTrackMedia() {
        return $this->hasOne("App\Models\Media", 'media_id', 'track_id');
    }
    
    public function getMedia() {
        return $this->hasOne("App\Models\Media", 'track_id', 'id');
    }
    
    public function getRelease() {
        return $this->belongsTo("App\Models\Release", 'release_id', 'id');
    }

    public static function getTracks($limit=null, $offset=null, $search=null){
        $tracks = Track::select('tracks.title','tracks.id','file_duration')->join('medias', 'tracks.track_id', '=', 'medias.media_id')->where('user_id',Auth::user()->id);
        $orderby = 'tracks.title';
        $order = 'asc';
        if ($search && !empty($search)) {
            $tracks->where(function($query) use ($search) {
                $query->where('tracks.title','LIKE','%'.$search.'%')
                    ->orWhere('tracks.primary_artist','LIKE','%' . $search . '%');
            });
        }

        $response = $tracks->orderBy($orderby, $order);
        return $response;
    }

}