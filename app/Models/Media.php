<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Media  extends Model
{    
    protected $fillable = [
        'table_name','table_id','type','file_name','file_path','extension'
    ];
    
    protected $appends = ['songs'];

    protected $hidden = [
        'updated_at', 'deleted_at',
    ];
    protected $tab  ='medias'; 
    
    protected  $table = 'medias';
    
    public function getSongsAttribute()
    {
        return url_checker(str_replace('flac','mp3',$this->base_path.'/'.$this->folder_path));
    }

}
