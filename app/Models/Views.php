<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Views extends Model
{    
    protected $table = 'views';
    protected $fillable = [
        'table_type','table_id','user_id'
    ];

    protected $hidden = [
        'updated_at'
    ];
   
    public function getTrack() {
        return $this->HasOne("App\Models\Track", 'id', 'table_id');
    }
}
