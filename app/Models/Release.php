<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Models\StoreRelease;

class Release extends Model {
    
    protected $primaryKey = 'id';
    public $table = "releases";
    protected  $fillable = ['is_upload','UPC'];
     
    protected $appends = ['genre_value','subgenre_value','label_value','track','is_favorite','is_playlist','rating','total_like','total_sharing','total_views'];

    public function getModel($limit=null, $offset=null, $search=null, $orderby=null, $order=null,$type=null,$current_status=null,$user_id=0) {
        $status = array(0);
        $q = Release::select('releases.*');        
        if ($search && !empty($search)) {
            $q->where(function($query) use ($search) {
                $query->whereRaw('LOWER(releases.title) ' . 'LIKE ' . '"%' . strtolower($search) . '%"')
                        ->orWhereRaw('LOWER(releases.producer_catalogue_number) ' . 'LIKE ' . '"%' . strtolower($search) . '%"')
                        ->orWhereRaw('LOWER(releases.upc_temp) ' . 'LIKE ' . '"%' . strtolower($search) . '%"')
                        ->orWhereRaw('LOWER(releases.UPC) ' . 'LIKE ' . '"%' . strtolower($search) . '%"')
                        ->orWhereRaw('LOWER(releases.id) ' . 'LIKE ' . '"%' . strtolower($search) . '%"')
                        ->orWhereRaw('LOWER(releases.primary_artist) ' . 'LIKE ' . '"%' . strtolower($search) . '%"')
                        ->orWhereRaw('LOWER(releases.producer_catalogue_number) ' . 'LIKE ' . '"%' . strtolower($search) . '%"');
                        $lb=Label::whereRaw('LOWER(title) ' . 'LIKE ' . '"%' . strtolower($search) . '%"')->pluck('id')->toArray();
                        $query->orWhereIn('label_id',$lb);
            });            
        }
        if($type=='final'){
            $orderby = $orderby ? 'releases.approve_at' : 'releases.approve_at';
            $order = $order ? $order : 'desc';
        }else{
            $orderby = $orderby ? $orderby : 'releases.created_at';
            $order = $order ? $order : 'desc';   
        }
        $response = $q->orderBy($orderby, $order);
        return $response;
    }
    
    public function getTrackModel($limit=null, $offset=null, $search=null, $orderby=null, $order=null,$id=null) {
        $q =  Track::select('tracks.*')->where('release_id',$id);
               
        $orderby = $orderby ? $orderby : 'tracks.created_at';
        $order = $order ? $order : 'asc';
        if ($search && !empty($search)) {
            $q->where(function($query) use ($search) {
                $query->whereRaw('LOWER(tracks.title) ' . 'LIKE ' . '"%' . strtolower($search) . '%"');
            });
        }
        $response = $q->orderBy($orderby, $order);
        return $response;
    }
    
    public function scopeTrackCount($query){
        $query->select('releases.*')
                ->selectRaw('(select count(*) from tracks where tracks.release_id = releases.id) as track_count')
                ->selectRaw('(select count(*) from tracks where tracks.release_id = releases.id) as track')
                ->selectRaw('(select (ip_address) from sharing where sharing.table_type = "Album" AND sharing.table_id = releases.id) as total_sharing')
                ->selectRaw('(select count(*) from comments where comments.table_type="Album" and comments.table_id = releases.id) as total_comment')
                ->selectRaw('(select (name) from genres where genres.id = releases.genre_id) as genre_value')
                ->selectRaw('(select (title) from subgenre where subgenre.id = releases.subgenre_id) as subgenre_value')
                ->selectRaw('(select (title) from labels where labels.id = releases.label_id) as label_value')
                ->selectRaw('(select sum(ip_address) as aggregate from views where table_type = "Track" and exists (select * from tracks where views.table_id = tracks.id and release_id = releases.id)) as total_views')
                ->selectRaw('(select avg(rating) from rating where rating.type="Album" and rating.track_id = releases.id) as rating')
                ->selectRaw('(select count(*) from favorites where favorites.type="Album" and favorites.status=1 and favorites.track_id = releases.id) as total_like');
    }
        
    public function getAlbumProfileAttribute($value) {
        if($this->album_profile_webp)
        {
            return url_checker($this->album_profile_webp,'s3','',0.4,'image');    
        }
        else
        {
         return url_checker($value,'s3','',0.4,'image');    
        }         
    }
    public function getTotalSharingAttribute($value) {
       // $data = Sharing::where('table_type','Album')->where('table_id',$this->id)->first();
        return !empty($value) ? count(explode(',',$value)) : 0;
    }
    public function getTotalViewsAttribute($value) {
       /* $release_id = $this->id;
         $data = Views::where('table_typed','Track')->whereHas('getTrack',function($query) use ($release_id){
             $query->where('release_id',$release_id);
         })->sum('ip_address');*/
        return !empty($value) ? $value : 0;  
        return 0;
    }
    
    public function getGenreValueAttribute($value) {
       // $data = Genre::select('name')->where('id',$this->genre_id)->first();        
        return isset($value)?$value:'';    
    }
    
    public function getSubgenreValueAttribute($value) {
       // $data = Subgenre::select('title')->where('id',$this->subgenre_id)->first();        
        return isset($value)?$value:'';    
    }
    
    public function getLabelValueAttribute($value) {
        //$data = Label::select('title')->where('id',$this->label_id)->first();        
        return isset($value)?$value:'';    
    }
    
    public function getTrackAttribute($value) {
        //$data = Track::where('release_id',$this->id)->count();        
        return isset($value)?$value:0;    
    }
    
    public function getRatingAttribute($value) {
       // $data =  Rating::where('track_id',$this->id)->where('type','Album')->avg('rating');   
        return  $value ?? 0;
    }
    
    public function getPrimaryArtistAttribute($value){
        $data = Track::where('release_id',$this->id)->pluck('primary_artist')->toArray(); 
        $data = array_unique($data);
        if(count($data)>1)
        {
            return 'various artists';
        }
        else
        {
            return implode('| ',$data);             
        }

    }
    
    public function getIsFavoriteAttribute($value){
        if(isset(Auth::user()->id))
        {
            return Favorite::where('track_id',$this->id)->where('user_id',Auth::user()->id)->where('type','Album')->where('status',1)->count();
        }
        else
        {
            return 0;
        }
    }
    
    public function getTrackCountAttribute($value){
       /// return Track::where('release_id',$this->id)->count();
        return $value ?? 0;
    }
    
    public function getTotalLikeAttribute($value)
    {
        return $value ?? 0;
        //return Favorite::where('track_id',$this->id)->where('type','Album')->where('status',1)->count();
    }
   
    public function getIsPlaylistAttribute($value){
        if(isset(Auth::user()->id))
        {
            return Playlist::where('track_id',$this->id)->where('user_id',Auth::user()->id)->count();
        }
        else
        {
            return 0;
        }
    }
    
    public function getUser() {
        return $this->BelongsTo("App\User", 'user_id', 'id');
    }
    public function getGenre() {
        return $this->HasOne("App\Models\Genre", 'id', 'genre_id');
    }
    
    public function getSubGenre() {
        return $this->HasOne("App\Models\Subgenre", 'id', 'subgenre_id');
    }

    public function getTrack() {
        return $this->HasOne("App\Models\Track", 'release_id', 'id');
    }
    
    public function getTrackMany() {
        return $this->HasMany("App\Models\Track", 'release_id', 'id');
    }
    
    public function getReleaseTrack() {
        return $this->hasMany("App\Models\Track", 'release_id', 'id');
    }
    
    public function getCreatedByAttribute($value){
        $check_id = $this->sub_user_id?$this->sub_user_id:$this->user_id;
        return "Unknown";
    }
}