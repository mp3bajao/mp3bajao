<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;
class Artist extends Model {
    
    protected $primaryKey = 'id';
    
    public $table = "artist";
     protected $fillable = [
        'name','image','status','image_webp'
    ];
    protected $appends = ['rating','is_favorite','rating','total_like','total_sharing','follow','unfollow','is_follow','total_views','track_count'];
    //protected $appends = ['posted_by'];

    public function getModel($limit=null, $offset=null, $search=null, $orderby=null, $order=null) {
        $q = $this->select('artist.*')->where('status',0);
       
        $orderby = $orderby ? $orderby : 'artist.created_at';
        $order = $order ? $order : 'desc';
        if ($search && !empty($search)) {
            $q->where(function($query) use ($search) {
                $query->whereRaw('LOWER(artist.title) ' . 'LIKE ' . '"%' . strtolower($search) . '%"');
            });
        }

        $response = $q->orderBy($orderby, $order);
        return $response;
    }
    
    public function getTrackCountAttribute($value){
        return  Track::whereRaw('LOWER(primary_artist) ' . 'LIKE ' . '"%' . strtolower($this->name) . '%"')->count();
    }
    
      public function scopeTrackCount($query){
        $query->select('artist.*')
                //->selectRaw('(select count(*) from tracks where tracks.release_id = releases.id) as track_count')
                //->selectRaw('(select count(*) from tracks where tracks.release_id = releases.id) as track')
                ->selectRaw('(select (ip_address) from sharing where sharing.table_type = "Artist" AND sharing.table_id = artist.id) as total_sharing')
                ->selectRaw('(select count(*) from follows where follows.artist_id = artist.id and status=1) as follow')
                ->selectRaw('(select count(*) from follows where follows.artist_id = artist.id and status=0) as unfollow')
                //->selectRaw('(select (name) from genres where genres.id = releases.genre_id) as genre_value')
                //->selectRaw('(select (title) from subgenre where subgenre.id = releases.subgenre_id) as subgenre_value')
                //->selectRaw('(select (title) from labels where labels.id = releases.label_id) as label_value')
                //->selectRaw('(select sum(ip_address) as aggregate from views where table_type = "Track" and exists (select * from tracks where views.table_id in (select (id) from track where LOWER(primary_artist) LIKE "% artist.name %"))) as total_views')
                ->selectRaw('(select count(*) from comments where comments.table_type="Artist" and comments.table_id = artist.id) as total_comment')                               
                ->selectRaw('(select avg(rating) from rating where rating.type="Artist" and rating.track_id = artist.id) as rating')
                ->selectRaw('(select count(*) from favorites where favorites.type="Artist" and favorites.status=1 and favorites.track_id = artist.id) as total_like');
        }
    
    public function getImageAttribute($value)
    {

        if(isset($value))
        {
            if($this->image_webp)
            {
                return url_checker($this->image_webp,'public_to_s3','uploads/category/');                
            }
            else
            {
                return url_checker($value,'public','uploads/category/');                
            }
        }
        else
        {
                  //whereRaw('FIND_IN_SET(?,primary_artist)', [$this->name])
            $tracks = Track::whereRaw('LOWER(primary_artist) ' . 'LIKE ' . '"%' . strtolower($this->name) . '%"')->first();           
            return $tracks->getRelease->album_profile ?? url_checker($value);
        }
    }
    
    public function getTotalSharingAttribute($value) {
       // $data = Sharing::where('table_type','Artist')->where('table_id',$this->id)->first();
        return isset($value) ? count(explode(',',$value->ip_address)) : 0;
    }
    
    public function getTotalViewsAttribute($value) {
        //$tracks = Track::whereRaw('LOWER(primary_artist) ' . 'LIKE ' . '"%' . strtolower($this->name) . '%"')->pluck('id')->toArray();        
       //     $data = Views::where('table_type','Track')->whereIn('table_id',$tracks)->sum('ip_address');
      //  return $view =   isset($data)  ? $data : 0;
        $view = $this->top_views;
        if($view < 10000)
        {
            return number_format($view);
        }
        elseif($view > 10000 && $view < 100000)
        {
            return number_format($view/1000).'k';  
        }
        elseif($view > 100000 && $view < 10000000)
        {
            return number_format($view/100000).'m';  
        }   
        return 0;
    }
    
    
    public function getFollowAttribute($value){
       //return Follow::where('artist_id',$this->id)->where('status',1)->count();       
        return $value ?? 0;
    }
    
    public function getUnfollowAttribute($value){
       //return Follow::where('artist_id',$this->id)->where('status',0)->count();       
        return $value ?? 0;
    }
    
    public function getIsFollowAttribute($value){
        if(isset(Auth::user()->id))
        {
            return Follow::where('artist_id',$this->id)->where('user_id',Auth::user()->id)->where('status',1)->count();       
        }
        else
        {
         return  0; 
        }
    }
    
    public function getRatingAttribute($value) {
        //$data =  Rating::where('track_id',$this->id)->where('type','Artist')->avg('rating');    
        return $value ?? 0;
    }
    
    
      public function getIsFavoriteAttribute($value){
        if(isset(Auth::user()->id))
        {
            return Favorite::where('track_id',$this->id)->where('user_id',Auth::user()->id)->where('type','Artist')->where('status',1)->count();
        }
        else
        {
            return 0;
        }
    }
    public function getTotalLikeAttribute($value)
    {
        return $value ?? 0;
       // return Favorite::where('track_id',$this->id)->where('type','Artist')->where('status',1)->count();
    }
    
    
}