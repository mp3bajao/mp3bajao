<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Rating extends Model
{    
    protected $table = 'rating';
    protected $fillable = [
        'order_id','product_id','user_id','rating'
    ];

    protected $hidden = [
        'updated_at'
    ];
   
}
