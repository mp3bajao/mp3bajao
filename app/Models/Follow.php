<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;
class Follow extends Model {
    
    protected $primaryKey = 'id';    
    public $table = "follows";
     protected $fillable = [
        'artist_id','user_id','status'
    ];
}