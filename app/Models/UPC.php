<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class UPC extends Model
{
   public $table = "upcs";
   
   protected $dates = [
        'created_at',
        'updated_at',
    ];

   protected $fillable = [
        'id',
        'user_id',
        'key_value',
        'value',
        'total_count',
        'music_type',
   ];   
}
