<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;
class Chart extends Model {
    
    protected $primaryKey = 'id';
    
    public $table = "charts";
    
    protected $fillable = [
        'name','image','status'
    ];
    
    
    public function getHomeTopChart(){
        //return $this->HasMany("App\Models\Track", 'release_id', 'id');
        return $this->hasMany('App\Models\TopChart','chart_id','id')->where('type','Home')->orderBy('rank','asc');
    }
}