<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Http\Resources\Track as TrackResource;

class ProgramCategory extends Model
{    
    protected $table = 'program_categories';
    
    protected $fillable = [
       'name','image','user_id','status','image_webp'
    ];
    
    protected $appends = ['adds_count','track_count','is_like','total_like','total_sharing','total_views','total_comment','total_download','rating',];
     
    protected $hidden = [
        'updated_at'
    ];  
    
    public function scopeTrackCount($query){
        $query->select('releases.*')
                ->selectRaw('(select count(*) from tracks where tracks.release_id = releases.id) as track_count')
                ->selectRaw('(select count(*) from tracks where tracks.release_id = releases.id) as track')
                ->selectRaw('(select (ip_address) from sharing where sharing.table_type = "Album" AND sharing.table_id = releases.id) as total_sharing')
                ->selectRaw('(select (name) from genres where genres.id = releases.genre_id) as genre_value')
                ->selectRaw('(select (title) from subgenre where subgenre.id = releases.subgenre_id) as subgenre_value')
                ->selectRaw('(select (title) from labels where labels.id = releases.label_id) as label_value')
                ->selectRaw('(select sum(ip_address) as aggregate from views where table_type = "Track" and exists (select * from tracks where views.table_id = tracks.id and release_id = releases.id)) as total_views')
                ->selectRaw('(select avg(rating) from rating where rating.type="Album" and rating.track_id = releases.id) as rating')
                ->selectRaw('(select count(*) from favorites where favorites.type="Album" and favorites.status=1 and favorites.track_id = releases.id) as total_like');
    }
    
    
    
    public function getTotalSharingAttribute($value) {
        $data = Sharing::where('table_type','Program')->where('table_id',$this->id)->first();
        return isset($data->ip_address) ? count(explode(',',$data->ip_address)) : 0;
    }
    
    public function getTotalViewsAttribute($value) {
        $track = Program::where('program_type',$this->id)->pluck('track_id')->toArray();
        $data = Views::where('table_type','Track')->whereIn('table_id',$track)->sum('ip_address');
        return isset($data) ? $data : 0;   
    }
    
    public function getIsLikeAttribute($value)
    {
        if(isset(Auth::user()->id))
        {
            return  Favorite::where('track_id',$this->id)->where('type','Program')->where('user_id',Auth::user()->id)->count();
        }
        else
        {
            return 0;
        }
    }
    
    
    public function getTotalLikeAttribute($value)
    {
       return  Favorite::where('track_id',$this->id)->where('type','Program')->count();
    }
    
    public function getTotalShareAttribute($value)
    {
        return 0;
    }
    
    public function getTotalCommentAttribute($value)
    {
        return 0;
    }
    
    public function getTotalDownloadAttribute($value)
    {
        return 0;
    }
    public function getRatingAttribute($value)
    {
        return Rating::where('type','Program')->where('track_id',$this->id)->avg('rating');
    }
    
    public function getImageAttribute($value)
    {
        if($this->image_webp)
        {
            return url_checker($this->image_webp,'public_to_s3','uploads/category/');                
        }
        else
        {
            return url_checker($value,'public','uploads/category/');                
        }
        //return url_checker($value,'public','uploads/category/',.2);
    }
    
    public function getSingleProgram()
    {
        return $this->HasOne("App\Models\Program", 'program_type', 'id');
    }
    
    public function getUser()
    {
        return $this->HasOne("App\User", 'id', 'user_id');
    }
    
    public function  getProgramList()
    {
       return $this->hasMany("App\Models\Program", 'program_type', 'id')->orderBy('position','asc');
    }

    
    public function  getTrackCountAttribute($value)
    {
        return Program::where('program_type',$this->id)->where('table_type','Program')->count();
    }
    
    public function  getAddsCountAttribute($value)
    {
        return Program::where('program_type',$this->id)->where('table_type','Adds')->count();
    }
}
