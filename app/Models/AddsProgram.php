<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;
class AddsProgram extends Model {
    
    protected $primaryKey = 'id';
    
    public $table = "adds_programs";
     protected $fillable = [
        'name','image','status','duration','time'
    ];
   // protected $appends = ['rating','is_favorite','rating','total_like','total_sharing','total_views'];
    //protected $appends = ['posted_by'];

    public function getModel($limit=null, $offset=null, $search=null, $orderby=null, $order=null) {
        $q = $this->select('artist.*')->where('status',0);
       
        $orderby = $orderby ? $orderby : 'artist.created_at';
        $order = $order ? $order : 'desc';
        if ($search && !empty($search)) {
            $q->where(function($query) use ($search) {
                $query->whereRaw('LOWER(artist.title) ' . 'LIKE ' . '"%' . strtolower($search) . '%"');
            });
        }

        $response = $q->orderBy($orderby, $order);
        return $response;
    }
    
    public function getTrackAttribute($value)
    {
      return url_checker($value,'public_to_s3','uploads/adds_program/');  
    }
    public function getImageAttribute($value)
    {

        if(isset($value))
        {
            return url_checker($value,'public','uploads/category/');
        }
        else
        {
            $tracks = Track::whereRaw('LOWER(primary_artist) ' . 'LIKE ' . '"%' . strtolower($this->name) . '%"')->first();           
            return $tracks->getRelease->album_profile ?? url_checker($value);
        }
    }
    
    

    
}