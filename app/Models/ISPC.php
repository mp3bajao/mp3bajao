<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ISPC extends Model
{
    public $table = "ispcs";

    public static function getISRC(){
   	$ipsc = ISPC::count();
        $ipsc = 10000+($ipsc+1);
        $digits = strlen($ipsc);
        $series = ($digits-5);
        $middle = 15+$series;
        $ipsc = substr($ipsc, -5);
        $isrc = 'INS7A'.$middle.$ipsc;
        return $isrc;
    }
}
