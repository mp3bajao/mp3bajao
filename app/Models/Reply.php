<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;
class Reply extends Model {
    
    protected $primaryKey = 'id';    
    public $table = "replys";
    protected $fillable = [
        'user_id','table_type','table_id','comment'
    ];
    public function getUser()
    {
        return $this->HasOne("App\User", 'id', 'user_id');
    }
     
}