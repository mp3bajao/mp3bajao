<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Http\Resources\Track as TrackResource;

class Program extends Model
{    
    protected $table = 'programs';
    
    protected $hidden = [
        'updated_at'
    ];
    
    protected $fillable = 
    [
       'user_id','program_type','track_id','status','position'
    ];
    
    public function getTrack() 
    {        
       return  $this->HasOne("App\Models\Track", 'id', 'track_id');
    }
    
    public function getAdds() 
    {        
       return  $this->HasOne("App\Models\AddsProgram", 'id', 'track_id');
    }   
}
