<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Auth;

class UsersAddress extends Model
{    
    protected $table = 'user_address';
    protected $fillable = [
        'user_id','address','latitude','longitude','street_line_1','street_line_2','building_number','address_type','city','state'
    ];

    protected $hidden = [
        'updated_at'
    ];
   
}
