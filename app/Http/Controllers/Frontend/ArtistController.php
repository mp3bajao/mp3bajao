<?php
namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class ArtistController extends Controller {
    
    
    
    public function index(Request $request){
        $parms = $request->all();
        $data = apicurl('artist','Basic',$parms);
        $data = ['data'=>$data,'parms'=>$parms];
        return view('frontend.artist',$data);
       
    }
    public function loading(Request $request,$page){
        $parms = ['page'=>$page];
        $data = apicurl('artist','Basic',$parms);
        $data = ['data'=>$data,'parms'=>$parms];
        return view('frontend.artist_loading',$data);
       
    }
    
    public function details(Request $request,$id){
        $parms = ['id'=> base64_decode($id)];
        $data = apicurl('artist_details_list','Bearer',$parms);

        
        if($data['status']==false)
        {
            $data= apicurl('artist_details','Basic',$parms);
        }

        $comment = apicurl('commentList','Basic',['table_type'=>'artist','table_id'=>$data['data']->id,'page'=>1]);        

        $data = ['data'=>$data,'comment'=>$comment];
        return view('frontend.artist-details',$data);        
    }

}
