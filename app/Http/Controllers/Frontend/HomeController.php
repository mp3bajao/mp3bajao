<?php
namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Release;
use App\Models\PlaylistCategory;
use App\Models\Artist;
use App\Models\Banner;
use App\Models\Genre;
use Redirect;
use Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class HomeController extends Controller {
    
    public function index(){    
        $userdata =  Session::get('userData');        
        $parms = ['ip_address'=>$_SERVER['REMOTE_ADDR']];
        $data = apicurl('home','Basic',$parms);   
        $data= ['data'=>$data];
        return view('frontend.index',$data);
    }
    
    public function logout(){
        $parms =[];
        $data = apicurl('logout','Bearer',$parms);
        if($data['status']==true)
        {
            Session::reflash();
            Session::forget('userData');
        }
        return Redirect::back()->withErrors(['succes', 'Logout Successfully.']);
    }
    
    public function page($slug){        
        return view('frontend.'.$slug);
    }
    
    public function imageTowebp(){
        $data = Release::where('is_converted',0)->orderBy('id','asc')->get();
        foreach($data as $data){
                $image = $data->album_profile;
                //dd($image);
                $img = file_get_contents($image); 
                $percent = 0.550;
                $im = imagecreatefromstring($img);	
                $width = imagesx($im);
                $height = imagesy($im);
                $newwidth = $width * $percent;
                $newheight = $height * $percent;
                $thumb = imagecreatetruecolor($newwidth, $newheight);
                imagecopyresized($thumb, $im, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                $file_name = str_replace('.jpg','.webp',$data->folder_path);
                imagewebp($thumb, 'public/album/'.$file_name, 100);
                $storage = Storage::disk('s3')->putFileAs($data->base_path,'public/album/'.$file_name,$file_name);
                $data->album_profile_webp = $data->base_path.'/'.$file_name;
                $data->is_converted=1;
                $data->save();               
        }
    }
    
    public function imageTowebpPlaylist(){
         $data = PlaylistCategory::where('is_converted',0)->where('image','!=',null)->orderBy('id','asc')->get();
        foreach($data as $data){
               // dd($data->getRawOriginal()['image']);
            $image = $data->getRawOriginal()['image'];
            $image_file = explode('/',$image);

                $image = $data->image;
                $img = file_get_contents($image); 
                $percent = $_GET['size']  ?? 0.2;
                $im = imagecreatefromstring($img);	
                $width = imagesx($im);
                $height = imagesy($im);
                $newwidth = $width * $percent;
                $newheight = $height * $percent;
                $thumb = imagecreatetruecolor($newwidth, $newheight);
                imagecopyresized($thumb, $im, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

                $file_name = str_replace('.jpg','.webp',$image_file[1]);
             
               // header('Content-type: image/webp');
                imagewebp($thumb, 'public/uploads/category/'.$image_file[0].'/'.$file_name, 100);
                    $storage = Storage::disk('s3')->putFileAs('uploads/category/'.$image_file[0],'public/uploads/category/'.$image_file[0].'/'.$file_name,$file_name);
               //    unlink('public/album/'.$file_name);
                $data->image_webp = $image_file[0].'/'.$file_name;
                $data->is_converted=1;
                $data->save();               
        }
        
        
    }
    public function imageTowebpAtrist(){
         $data = Artist::where('is_converted',0)->where('image','!=',null)->orderBy('id','asc')->get();
        foreach($data as $data){
               // dd($data->getRawOriginal()['image']);
            $image = $data->getRawOriginal()['image'];
            $image_file = explode('/',$image);

                $image = $data->image;
                $img = file_get_contents($image); 
                $percent = $_GET['size']  ?? 0.4;
                $im = imagecreatefromstring($img);	
                $width = imagesx($im);
                $height = imagesy($im);
                $newwidth = $width * $percent;
                $newheight = $height * $percent;
                $thumb = imagecreatetruecolor($newwidth, $newheight);
                imagecopyresized($thumb, $im, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

                $file_name = str_replace('.jpg','.webp',$image_file[1]);
             
               // header('Content-type: image/webp');
                imagewebp($thumb, 'public/uploads/category/'.$image_file[0].'/'.$file_name, 100);
                    $storage = Storage::disk('s3')->putFileAs('uploads/category/'.$image_file[0],'public/uploads/category/'.$image_file[0].'/'.$file_name,$file_name);
                //unlink('public/album/'.$file_name);
                $data->image_webp = $image_file[0].'/'.$file_name;
                $data->is_converted=1;
                $data->save();               
        }
        
        
    }
    
     public function imageTowebpGeneres(){
         $data = Genre::where('is_converted',0)->where('image','!=','')->orderBy('id','asc')->get();
        foreach($data as $data){
               // dd($data->getRawOriginal()['image']);
            $image = $data->getRawOriginal()['image'];
            $image_file = explode('/',$image);

                $image = $data->image;
                $img = file_get_contents($image); 
                $percent = $_GET['size']  ?? .2;
                $im = imagecreatefromstring($img);	
                $width = imagesx($im);
                $height = imagesy($im);
                $newwidth = $width * $percent;
                $newheight = $height * $percent;
                $thumb = imagecreatetruecolor($newwidth, $newheight);
                imagecopyresized($thumb, $im, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

                $file_name = str_replace('.jpg','.webp',$image_file[1]);
             
               // header('Content-type: image/webp');
                imagewebp($thumb, 'public/uploads/category/'.$image_file[0].'/'.$file_name, 100);
                    $storage = Storage::disk('s3')->putFileAs('uploads/category/'.$image_file[0],'public/uploads/category/'.$image_file[0].'/'.$file_name,$file_name);
                //unlink('public/album/'.$file_name);
                $data->image_webp = $image_file[0].'/'.$file_name;
                $data->is_converted=1;
                $data->save();               
        }
        
        
    }
     public function imageTowebpBanner(){
         $data = Banner::where('is_converted',0)->where('image','!=',null)->orderBy('id','asc')->get();
        foreach($data as $data){
               // dd($data->getRawOriginal()['image']);
            $image = $data->getRawOriginal()['image'];
            $image_file = explode('/',$image);

                $image = $data->image;
                $img = file_get_contents($image); 
                $percent = $_GET['size']  ?? 0.4;
                $im = imagecreatefromstring($img);	
                $width = imagesx($im);
                $height = imagesy($im);
                $newwidth = $width * $percent;
                $newheight = $height * $percent;
                $thumb = imagecreatetruecolor($newwidth, $newheight);
                imagecopyresized($thumb, $im, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

                $file_name = str_replace('.jpg','.webp',$image_file[1]);
             
               // header('Content-type: image/webp');
                imagewebp($thumb, 'public/uploads/category/'.$image_file[0].'/'.$file_name, 100);
                    $storage = Storage::disk('s3')->putFileAs('uploads/category/'.$image_file[0],'public/uploads/category/'.$image_file[0].'/'.$file_name,$file_name);
                //unlink('public/album/'.$file_name);
                $data->image_webp = $image_file[0].'/'.$file_name;
                $data->is_converted=1;
                $data->save();               
        }       
    }
    
    public function aboutus(Request $request){
         return view('frontend.aboutus');
    }
    
    public function privacyPolicy(Request $request)
    {
        return view('frontend.privacyPolicy');
    }
    
    public function termsAndConditions(Request $request)
    {
      return view('frontend.termsAndConditions');  
    }
}
