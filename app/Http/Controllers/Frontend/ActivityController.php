<?php
namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Session;

class ActivityController extends Controller {
    
    public function playlist(Request $request){
        $parms = [];
        $category = apicurl('playlist_type','Bearer',$parms);
        $data = array();
        $type_id ='';
        if($category['status']==true){
            if(isset($request->type_id))
            {
                $parms = ['page'=>1,'playlist_type'=>$request->type_id];
                $type_id = $request->type_id;
            }
            else
            {
                $parms = ['page'=>1,'playlist_type'=>$category['data'][0]->id];
                $type_id = $category['data'][0]->id;
            }        
            $data = apicurl('playlist','Bearer',$parms);  
        }
        
        
        $data = ['data' =>$data,'category'=>$category,'type_id'=>$type_id];
        return view('frontend.playlist',$data);       
    }
    
    public function favorites(Request $request){
        $parms = ['id'=>$request->id];
        $data= apicurl('favorite','Bearer',$parms);      
        $data = ['data' =>$data];
        return view('frontend.favorites',$data);        
    }
    public function history(Request $request){
        $parms = ['ip_address'=>$_SERVER['REMOTE_ADDR']];
        $data= apicurl('history','Basic',$parms);        
        $data = ['data' =>$data];
        return view('frontend.history',$data);        
    }
    public function searching($search=null){
        if(isset($search)){
          $parms = ['search'=> str_replace('+',' ',$search)];     
        }
        else{
        $parms = [];           
        }
 
        $data= apicurl('searching','Basic',$parms);        
        $data = ['data' =>$data];
    
        return view('frontend.searching',$data);        
    }

    public function commentList($table_type,$table_id,$page)
    {
        $parms['table_type'] = $table_type;
        $parms['table_id'] = $table_id;
        $parms['page'] = $page;
        $userdata =  Session::get('userData');
        $data= apicurl('commentList','Basic',$parms);     

        $data = ['comment' =>$data];
        return view('frontend.comment',$data);        
    }
    
    public function radio(Request $request){
        $parms = ['station'=>2];
        $station= apicurl('station','Basic',$parms); 
        if($request->station_id){
          $parms = ['station'=>$request->station_id];  
        }
        else{
            if($station['status']==true){
                $parms = ['station'=>$station['data'][0]->id];
            }        
        }
        
        $data= apicurl('radio','Basic',$parms); 
        $data = ['data'=>$data,'station'=>$station];
        return view('frontend.radio',$data); 
    }
    
    public function addViews(Request $request){
        $track_id = \App\Models\Views::where('table_type','Track')->pluck('table_id')->toArray();
        $track = \App\Models\Track::whereIn('id',$track_id)->get();
        foreach($track as $track){
             $track_count = \App\Models\Views::where('table_type','Track')
                     ->where('table_id',$track->id)
                     ->sum('ip_address');
            $artist = \App\Models\Artist::whereIn('name',explode('|',$track->primary_artist))->get();
            if(isset($artist[0]))
            {
                foreach($artist as $artist)
                {
                  // echo $track_count;
                $artist->top_views  = $track_count;
                $artist->save();
                }
            }        
        }    
    }
}
