<?php
namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class SongController extends Controller {
   
    public function index(Request $request){
        $parms = $request->all();
       
        if(isset($parms['charts']))
        {
            $parms['charts'] = base64_decode($parms['charts']);
        }
        else if(isset($parms['genres']))
        {
             $parms['genres'] = base64_decode($parms['genres']);
        }
        else if(isset($parms['lang']))
        {
             $parms['lang'] = $parms['lang'];
        }
        else if(isset($parms['search']))
        {
             $parms['search'] = $parms['search'];
        }        
        $language = apicurl('language','Basic',$parms); 
        $lang = $parms['lang'] ?? null;

        $data = apicurl('song','Basic',$parms);  
     
        $data = ['data'=>$data,'parms'=>$parms,'language'=>$language,'lang'=>$lang];     
        return view('frontend.song',$data);       
    }
    
    public function loading(Request $request,$page,$slug,$value){
        $parms = $request->all();

        $parms['page'] = $page;
        if(isset($slug) && $slug=='charts')
        {
            $parms['charts'] = base64_decode($value);
        }
        else if(isset($slug) && $slug=='genres')
        {
             $parms['genres'] = base64_decode($value);
        }
        else if(isset($slug) && $slug=='lang')
        {
             $parms['lang'] = base64_decode($value);
        }
        else if(isset($slug) && $slug=='search')
        {
             $parms['search'] = base64_decode($value);
        }
       
        $language = apicurl('language','Basic',$parms); 
        $lang = $parms['lang'] ?? null;
        $data = apicurl('song','Basic',$parms);
      
        $data = ['data'=>$data,'parms'=>$parms,'language'=>$language,'lang'=>$lang];     
        return view('frontend.song_loading',$data);       
    }
    
    public function details(Request $request){
        $parms = ['id'=>base64_decode($request->id),'form'=>$request->from];  
       
        $data = apicurl('song_details_list','Bearer',$parms);
        
        if($data['status']==false)
        {
            $data = apicurl('song_details','Basic',$parms);        
        }
        $comment = apicurl('commentList','Basic',['table_type'=>$data['data']->from,'table_id'=>$data['data']->id,'page'=>1]);           

        $data = ['data'=>$data,'comment'=>$comment];
        return view('frontend.song-details',$data);        
    }
}
