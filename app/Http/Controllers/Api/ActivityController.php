<?php 
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Models\Release; 
use App\Models\Genre; 
use App\Models\Artist; 
use App\Models\Track; 
use App\Models\Favorite; 
use App\Models\Playlist; 
use App\Models\Rating; 
use App\Models\Download; 
use App\Models\History; 
use App\Models\Sharing; 
use App\Models\Views; 
use App\Models\PlaylistCategory; 
use App\Models\Follow;
use App\Models\Comment;
use App\Models\Reply;
use App\Models\Content;
use App\Models\Station;
use App\Models\Radio;
use App\Models\RadioPlaylist;
use App\Http\Resources\Notifications as NotificationResource;
use App\Http\Resources\Genres as GenreResource;
use App\Http\Resources\Artist as ArtistResource;
use App\Http\Resources\Release as ReleaseResource;
use App\Http\Resources\Track as TrackResource;
use App\Http\Resources\TrackDetails as TrackDetailsResource;
use Illuminate\Support\Facades\Auth; 
use Validator,App,Mail,DateTime;
class ActivityController extends Controller 
{

    public function __construct(Request $request) 
    {
        $this->request =  $request->all();      
    }
    
    private function timeZoneUpdate($request ,$user_id)
    {
        $timezone = $request->header('timezone');
        if(isset($timezone))
        {
            $this->Models->where('id',$user_id)->update(['time_zone'=>$timezone]);             
            return true;
        }
        else
        {
           $this->Models->where('id',$user_id)->update(['time_zone'=>'Asia/Kolkata']);  
        }
        return false;
    }
    
    public function addFavorite(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [ 
            'type' => 'required',
            'status'=>'required',     
            'track_id'=>'required'            
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {           
            $data = Favorite::where(['track_id'=>$input['track_id'],'type'=>$input['type'],'user_id'=>Auth::user()->id])->first();
            if(!isset($data))
            {
                $data = new Favorite;   
                $data->user_id = Auth::user()->id;
                $data->track_id = $input['track_id'];
                $data->type = $input['type'];
                
            }
            $data->status = $input['status'];
            $data->save();
            if($input['status']==1)
            {
                $this->message ='Like successfully';                
            }
            else
            {
                $this->message ='Dislike Successfully';                
            }

            $this->status = true;
            $this->code = 200;
            $this->data = $data;                             
        } 
        return $this->jsonResponse();  
    }
    
    public function favorite(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [ 
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {           
            $data = Favorite::where(['user_id'=>Auth::user()->id,'status'=>1])->pluck('track_id')->toArray();
            $track  = Track::whereIn('id',$data)->with('getRelease')->get();
            if(isset($track[0]))
            {
                $data = TrackDetailsResource::collection($track)->response()->getData(true);  
                $this->message ='Success';      
                $this->status = true;
                $this->code = 200;
                $this->data = $data['data'];                                            
            }
            else
            {
                $this->message ='No Favorite Song Available.';   
            } 
        } 
        return $this->jsonResponse(); 
    }
    
    
    public function addPlaylist(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [
            'status'=>'required',     
            'track_id'=>'required',         
            'playlist_type'=>'required',
            "playlist_type_name" => "required_if:playlist_type,==,newplay"
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        { 
            if(isset($input['playlist_type']) && $input['playlist_type']=='newplay')
            {
                $category = PlaylistCategory::where('user_id',Auth::user()->id)->where('name', trim($input['playlist_type_name']))->first();
                if(!isset($category))
                {
                    $category           = new PlaylistCategory;
                    $category->user_id  = Auth::user()->id;
                    $category->name     = trim($input['playlist_type_name']);
                    $category->status   = 1;
                    $category->save();
                    $input['playlist_type'] = $category->id;
                }
                else
                {
                    $input['playlist_type'] = $category->id;
                }
            }
            $data = Playlist::where(['track_id'=>$input['track_id'],'user_id'=>Auth::user()->id,'playlist_type'=>$input['playlist_type']])->first();
            if(!isset($data))
            {
                $data = new Playlist;   
                $data->user_id = Auth::user()->id;
                $data->track_id = $input['track_id'];
                $data->playlist_type = $input['playlist_type'];
                $data->status = $input['status'];
                $data->save();
                $this->message ='Add on Play list successfully.'; 
                $this->status = true;
                $this->code = 200;
                $this->data = $data; 
            }
            else
            {
                $this->message ='Already added on play list.';                
            }                                       
        } 
        return $this->jsonResponse();
    }
    
    public function playlist(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [ 
            'playlist_type'=>'required'
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {           
            $data = Playlist::where(['user_id'=>Auth::user()->id,'playlist_type'=>$input['playlist_type']])->pluck('track_id')->toArray();
            $track  = Track::whereIn('id',$data)->get();
            if(isset($track[0]))
            {
                $data = TrackDetailsResource::collection($track)->response()->getData(true);  
                $this->message ='Success';      
                $this->status = true;
                $this->code = 200;
                $this->data = $data['data'];                                            
            }
            else
            {
                $this->message ='No Playlist Song Available.';   
            }                                  
        } 
        return $this->jsonResponse(); 
    }
    
    public function playlistType(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [ 
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {
            $data = PlaylistCategory::select('name','id')->where(['user_id'=>Auth::user()->id])->get();
            if(isset($data[0]))
            {
                $this->message ='Success';      
                $this->status = true;
                $this->code = 200;
                $this->data = $data;                                            
            }
            else
            {
                $this->message ='No Playlist Song Available.';   
            }                                  
        } 
        return $this->jsonResponse(); 
    }
    
    
    public function rating(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [
            'status'    => 'required',     
            'track_id'  => 'required',            
            'type'      => 'required',            
            'rating'    => 'required',            
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {           
            $data = Rating::where(['track_id'=>$input['track_id'],'user_id'=>Auth::user()->id,'type'=>$input['type']])->first();
            if(!isset($data))
            {
                $data           = new Rating;   
                $data->user_id  = Auth::user()->id;
                $data->track_id = $input['track_id'];
                $data->type     = $input['type'];
            }
            $data->rating = $input['rating'];
            $data->save();
            $this->message ='Rating successfully'; 
            $this->status = true;
            $this->code = 200;
            $this->data = $data;                             
        } 
        return $this->jsonResponse();
    }
    
    public function addDownload(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [ 
            'track_id'=>'required'            
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {
            $data = Download::where(['track_id'=>$input['track_id'],'user_id'=>Auth::user()->id])->first();
            if(!isset($data))
            {
                $data = new Download;   
                $data->user_id = Auth::user()->id;
                $data->track_id = $input['track_id'];
            }
            $data->save();
            $this->message ='Successfully';  
            $this->status = true;
            $this->code = 200;
            $this->data = $data['data'];                             
        }
        return $this->jsonResponse();  
    }
    
    public function download()
    {

    }
    
    
    public function add_history(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [ 
            'ip_address' => 'required',
            'track_id'=>'required'            
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {           
            $data = History::where(['track_id'=>$input['track_id'],'ip_address'=>$input['ip_address']])->first();
            if(!isset($data))
            {
                $data = new History;   
                $data->track_id = $input['track_id'];
                $data->ip_address = $input['ip_address'];
                $data->save();
                $this->message ='Add history successfully.';  
                $this->status = true;
                $this->code = 200;
                $this->data = $data['data'];                
            }
            else{
                $data->updated_at = date('Y-m-d H:i:s');
                $data->save();
                $this->message ='Aleady added history.'; 
            }
        } 
        return $this->jsonResponse();  
    }
    
 
    
   public function history(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [ 
             'ip_address' => 'required',
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {
            
            $history = History::where(['ip_address'=>$input['ip_address']])->orderby('updated_at','desc')->limit(20)->pluck('track_id')->toArray();
            $ids_ordered = implode(',', $history);
            if(!empty($ids_ordered)){
                $q  = Track::whereIn('id',$history)->with('getRelease') ->orderByRaw("FIELD(id, $ids_ordered)")->with('getRelease');
                if(isset($input['limit']))
                {
                    $q->limit($input['limit']);
                }       
                $track = $q->orderBy('id','asc')->get();  
            }
            else
            {
                $q  = Track::whereIn('id',$history)->with('getRelease')->inRandomOrder();
                if(isset($input['limit']))
                {
                    $q->limit($input['limit']);
                }
                $track = $q->orderBy('id','asc')->get();
            }
            $trackData = array();
            if(isset($track[0]))
            {
                $data = TrackDetailsResource::collection($track)->response()->getData(true);   
                $this->message ='Success';      
                $this->status = true;
                $this->code = 200;
                $this->data = $data['data'];                                            
            }
            else
            {
                $this->message ='No History Available.';   
            }                                  
        } 
        return $this->jsonResponse(); 
    } 
    
    public function sharing(Request $request)
    {
       $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [ 
            'ip_address' => 'required',
            'type' => 'required',
            'type_id' => 'required',
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {           
            
            $data = Sharing::where('table_type',$input['type'])->where('table_id',$input['type_id'])->first();
            if(isset($data))
            {
               $ipaddress = explode(',',$data->ip_address);
               $ipaddress[]  =  $input['ip_address'];
               $ipaddress = array_unique($ipaddress);
               $data->ip_address = implode(',', $ipaddress);
               $data->save();
            }
            else
            {
                $data = new Sharing;
                $data->table_type = $input['type'];
                $data->table_id = $input['type_id'];;
                $data->ip_address = $input['ip_address'];
                $data->save();
            }
            $this->message ='Success';      
            $this->status = true;
            $this->code = 200;                                                          
        } 
        return $this->jsonResponse();  
    }
    
    public function views(Request $request)
    {
       $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [ 
            'ip_address' => 'required',
            'type' => 'required',
            'type_id' => 'required',
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {
            $data = Views::where('table_type',$input['type'])->where('table_id',$input['type_id'])->first();
            $track = Track::where('id',$input['type_id'])->first();
            if($track){
                $artist = Artist::whereIn('name',explode('|',$track->primary_artist))->get();
                if(isset($artist[0]))
                {
                    foreach($artist  as $artist1)
                    {
                        $artist1->top_views = $artist1->top_views+1;
                        $artist1->save();
                    }
                }
            }
            if(isset($data))
            {
               $ipaddress = $data->ip_address+1;
               $data->ip_address = $ipaddress;
               $data->save();
            }
            else
            {
                $data = new Views;
                $data->table_type = $input['type'];
                $data->table_id = $input['type_id'];;
                $data->ip_address =1;
                $data->save();
            }
            $this->message ='Success';      
            $this->status = true;
            $this->code = 200;                                                          
        } 
        return $this->jsonResponse();  
    }
    
    public function follows(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [
            'status'    => 'required',     
            'artist_id'  => 'required'         
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {           
            $data = Follow::where(['artist_id'=>$input['artist_id'],'user_id'=>Auth::user()->id])->first();
            if(!isset($data))
            {
                $data           = new Follow;   
                $data->user_id  = Auth::user()->id;
                $data->artist_id = $input['artist_id'];
            }
            $data->status = $input['status'];
            $data->save();
            if($input['status']==1)
            {
                $this->message ='Followed successfully';                 
            }
            else
            {
                $this->message ='Unfollow successfully';
            }
            $this->status = true;
            $this->code = 200;
            $this->data = $data;                             
        } 
        return $this->jsonResponse();
    }
    
    public function addComment(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [
            'table_type'    => 'required',     
            'table_id'  => 'required',
            'comment' => 'required'
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {    
            $data = new Comment;
            $data->user_id = Auth::user()->id;
            $data->table_type= $input['table_type'];
            $data->table_id= $input['table_id'];
            $data->comment = $input['comment'];
            $data->save();
            $this->message ='comment send successfully';                 
            $this->status = true;
            $this->code = 200;
            $this->data = $data;    
        }
         return $this->jsonResponse();
    }
    
    public function addReply(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [
            'comment_id'  => 'required',
            'reply' => 'required'
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {    
            $data = new Reply;
            $data->user_id = Auth::user()->id;
            $data->comment_id= $input['comment_id'];
            $data->reply = $input['reply'];
            $data->save();
            $this->message ='reply send successfully';                 
            $this->status = true;
            $this->code = 200;
            $this->data = $data;    
        }
        return $this->jsonResponse();
    }
    
    public function commentList(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [
            'table_type'    => 'required',     
            'table_id'  => 'required',
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {    
            $data = Comment::where('table_type',$input['table_type'])
                    ->with('getUser','getReply.getUser')
                    ->where('table_id',$input['table_id'])
                    ->orderBy('id','desc')
                    ->paginate(20);

            $this->message ='comment successfully';                 
            $this->status = true;
            $this->code = 200;
            $this->data = $data;    
        }
        return $this->jsonResponse();
    }

    public function aboutUs(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {    
            $data = Content::where('slug','about-us')->first();
            $this->message ='Page get successfully';                 
            $this->status = true;
            $this->code = 200;
            $this->data = $data;    
        }
        return $this->jsonResponse();
    }

    public function privacyPolicy(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {    
            $data = Content::where('slug','privacy-policy')->first();
            $this->message ='Page get successfully';                 
            $this->status = true;
            $this->code = 200;
            $this->data = $data;    
        }
        return $this->jsonResponse();
    }
    public function termsAndConditions(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {    
            $data = Content::where('slug','terms-and-condition')->first();
            $this->message ='Page get successfully';                 
            $this->status = true;
            $this->code = 200;
            $this->data = $data;    
        }
        return $this->jsonResponse();
    }
    public function help(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {    
            $data = Content::where('slug','help')->first();
            $this->message ='Page get successfully';                 
            $this->status = true;
            $this->code = 200;
            $this->data = $data;    
        }
        return $this->jsonResponse();
    }

    public function rateus(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [
            'rating'  => 'required',
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {    
            $data = User::where('id',Auth::user()->id)->first();
            $data->rating= $input['rating'];
            $data->save();
            $this->message ='Rating give successfully';                 
            $this->status = true;
            $this->code = 200;
            $this->data = $data;    
        }
        return $this->jsonResponse();
    }

    public function station(Request $request){
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [
            //'rating'  => 'required',
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {    
            $data = Station::where('status',1)->get();
            if(isset($data[0]))
            {
                $this->message ='Station successfully';                 
                $this->status = true;
                $this->code = 200;
                $this->data = $data;                   
            }
            else
            {
                $this->message ='Station not fount';                    
            }
 
        }
        return $this->jsonResponse();
    }
    
    public function radio(Request $request)
    {
       $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [
            'station'  => 'required',
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {    
            $radio = Radio::with('getRadioList.getProgram.getProgramList.getTrack.getRelease','getRadioList.getProgram.getProgramList.getTrack.getMedia','getRadioList.getProgram.getProgramList.getAdds')->where('week',date('l'))->where('station_id',$input['station'])->orderBy('start_time','asc')->get();
            if(isset($radio[0]))
            {   
                $program = array();
                date_default_timezone_set("Asia/Kolkata");
                
                
                foreach($radio as $r1 => $radioList)
                {

                    $enddate= date('Y-m-d '.$radioList->end_time);
                    $startdate= date('Y-m-d '.$radioList->start_time);
                   
                                       
                    $date2 = new DateTime($startdate);
                    $date3 = new DateTime($enddate);
                    
                    $interval = date_diff($date2,$date3);

                    $hours =  (($interval->format('%h')*60)*60);
                    $min =  ($interval->format('%i')*60);
                    $sec =  $interval->format('%s');
                    $seccount = $hours+$min+$sec;
                    $current_date = date('Y-m-d H:i:s',strtotime('-'.$seccount.' second',strtotime(date('Y-m-d H:i:s'))));
                    $date1 = new DateTime($current_date);
                  

                    if($date2 >= $date1)
                    {


                        if(isset($radioList->getRadioList[0]))
                        {
                            $start_date = $startdate;
                            foreach($radioList->getRadioList as $key => $radio_list_one)
                            {
                                $program[$r1][$key]['id'] = $radioList->id;
                                $program[$r1][$key]['program_name'] = $radio_list_one->getProgram->name;
                                $program[$r1][$key]['program_image'] = $radio_list_one->getProgram->image;                      
                                if($radio_list_one->getProgram->getProgramList[0])
                                {
                                    $programListArray = array();
                                 
                                    foreach($radio_list_one->getProgram->getProgramList as $key_two => $getProgramList)
                                    {

                                        $end_date = date('Y-m-d H:i:s',strtotime('+'.$getProgramList->duration.' second',strtotime(date($start_date))));
                                          $date2 = new DateTime($start_date);
                                            $date3 = new DateTime(date('Y-m-d H:i:s')); 
                                           $seccount1=0;
                                            if($date2 <= $date3)
                                            {
                                                $interval1 = date_diff($date2,$date3);

                                                $hours1 =  (($interval1->format('%h')*60)*60);
                                                $min1 =  ($interval1->format('%i')*60);
                                                $sec1 =  $interval1->format('%s');
                                                $seccount1 = $hours1+$min1+$sec1;
                                            }
                                        if($getProgramList->table_type=='Adds')
                                        {
                                            $programList['type']=$getProgramList->table_type;
                                            $programList['name']=$getProgramList->getAdds->name;
                                            $programList['image']=$getProgramList->getAdds->image;
                                            $programList['start_time']=$start_date;
                                            $programList['end_time']=$end_date;
                                            $programList['skip']=$seccount1;  
                                            $programList['duration']=$getProgramList->duration;                                        
                    
                                            
                                            
                                        } 
                                        elseif($getProgramList->table_type=='Program')
                                        {
                                            $programList['type']=$getProgramList->table_type;
                                            $programList['name']=$getProgramList->getTrack->title;
                                            $programList['image']=$getProgramList->getTrack->getRelease->album_profile;
                                            $programList['track']=$getProgramList->getTrack->getMedia->songs;  
                                            $programList['start_time']=$start_date;
                                            $programList['end_time']=$end_date;
                                            $programList['skip']=$seccount1;
                                            $programList['duration']=$getProgramList->duration;
                                        }
                                        $programListArray[] = $programList;
                                        $start_date = $end_date;
                                    }
                                    $program[$r1][$key]['track']= $programListArray;
                                }
                                if($key==0)
                                {
                                    $p12['program_name'] = $radio_list_one->getProgram->name;
                                    $p12['program_image'] = $radio_list_one->getProgram->image;  
                                   
                                }

                                $p1['track'][] = $programListArray;
                            }    
                            
                            $trackp = array();
                            if(!empty($p1['track']))
                            {
                              foreach($p1['track'] as $track)  
                              {
                                foreach($track as $keys => $track1)
                                {
                                    $duration = 300;      
                                    $backdate = date('Y-m-d H:i:s',strtotime('-'.$duration.' second',strtotime(date('Y-m-d H:i:s'))));
                                    $date4 = new DateTime($track1['start_time']);
                                    $date5 = new DateTime($backdate); 
                                    $seccount1=0;
                                    if($date4 >= $date5)
                                    {
                                       $trackp[] =$track1; 
                                    }                                     
                                }
                              }
                              $p12['track'] = $trackp;                              
                            }
                        }
                    }
                }
                
                
                //dd(date('Y-m-d H:i:s'));

                $this->message ='Station successfully';                 
                $this->status = true;
                $this->code = 200;
                $this->data = $p12;  
            }
            else
            {
                $this->message ='Radio not found';                                 
            }
        }
        return $this->jsonResponse();
    }
    
}