<?php 
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Models\Release; 
use App\Models\Genre; 
use App\Models\Artist; 
use App\Models\Track; 
use App\Models\Chart; 
use App\Models\TopChart; 
use App\Models\History; 
use App\Models\Banner; 
use App\Models\PlaylistCategory; 
use Illuminate\Support\Facades\Cache;
use App\Http\Resources\Notifications as NotificationResource;
use App\Http\Resources\Genres as GenreResource;
use App\Http\Resources\Artist as ArtistResource;
use App\Http\Resources\Release as ReleaseResource;
use App\Http\Resources\Track as TrackResource;
use App\Http\Resources\Banner as BannerResource;
use App\Http\Resources\Playlist as PlaylistResource;



use Illuminate\Support\Facades\Auth; 
use Validator,App,Mail;
class ReleaseController extends Controller 
{

    public function __construct(Request $request) 
    {
        $this->request =  $request->all();
        $this->Models = new Release();
        $this->Models_two = new Artist();        
    }
    
    private function timeZoneUpdate($request ,$user_id){
        $timezone = $request->header('timezone');
        if(isset($timezone))
        {            
             $this->Models->where('id',$user_id)->update(['time_zone'=>$timezone]);             
             return true;
        }
        else
        {
           $this->Models->where('id',$user_id)->update(['time_zone'=>'Asia/Kolkata']);  
        }
        return false;
    }
    
    public function index(Request $request){
     $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [ 
            'ip_address' => 'required',
            
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {
            $value = Cache::remember('home', 60,function () { 
                $chart = Chart::with('getHomeTopChart.getRelease.getTrack.getMedia','getHomeTopChart.getTrack.getRelease','getHomeTopChart.getTrack.getMedia','getHomeTopChart.getPlaylistCategory.getSinglePlaylist.getTrack.getMedia')->where('status',1)->orderBy('rank','asc')->get();
                $data1 = array();                
                if(isset($chart[0]))
                {

                   foreach($chart as $chart)
                    {
                        $data1[strtolower(str_replace(' ','_', $chart->name))]['name'] = $chart->name;
                        $data1[strtolower(str_replace(' ','_', $chart->name))]['description'] = $chart->description;
                        if(isset($chart->getHomeTopChart[0])){                        
                         $track_type1 = array();
                          foreach($chart->getHomeTopChart as $top_chart){
                            $track_type = array();                        
                            if( $top_chart->chart_type == 'SINGLE')
                            {
                               if($top_chart->format=='Album')
                               {                               
                                    $track_type['chart_type'] = $top_chart->chart_type;
                                    $track_type['Chart_title'] = null;
                                    $track_type['Chart_Image'] = null;
                                    $track_type['format'] = $top_chart->format;
                                    $release    = $top_chart->getRelease;                          
                                    $release = new ReleaseResource($release);
                                    $track_type['data'] = $release;
                                    $track_type1[] = $track_type;
                               }
                               else
                               {
                                    $track_type['chart_type'] = $top_chart->chart_type;
                                    $track_type['Chart_title'] = null;
                                    $track_type['Chart_Image'] = null;
                                    $track_type['format'] = $top_chart->format; 
                                    $track    = $top_chart->getTrack;
                                    if(isset($track))
                                    {
                                        $track = new TrackResource($track);
                                        $track_type['data'] = $track;
                                    }else{
                                    $track_type['data'] =   $top_chart->track_id;
                                    }

                                    $track_type1[] = $track_type;
                               }

                            }

                            if( $top_chart->chart_type == 'MULTIPLE')
                            {
                               $track_type['chart_type'] = $top_chart->chart_type;

                               $track_type['Chart_title'] = ucfirst($top_chart->getPlaylistCategory->name);
                               $track_type['Chart_Image'] = $top_chart->getPlaylistCategory->image;
                               $track_type['format'] = $top_chart->format??'Playlist';

                               $track    = $top_chart->getPlaylistCategory;
                               $track = new PlaylistResource($track);
                               $track_type['data'] = $track;
                               $track_type1[] = $track_type;
                            }

                          } 
                          $data1[strtolower(str_replace(' ','_', $chart->name))]['track'] = $track_type1;
                       }
                    }

                    $genres = Genre::where('status',1)->limit(10)->orderBy('id','desc')->get();
                    $artist = Artist::where('status',1)->limit(10)->orderBy('is_top','desc')->get();
                    $genres = GenreResource::collection($genres)->response()->getData(true);
                    $artist = ArtistResource::collection($artist)->response()->getData(true);
                    $resent = Release::where('status',0)->with('getTrack.getTrackMedia')->inRandomOrder()->limit(10)->get();
                    $resent = ReleaseResource::collection($resent)->response()->getData(true);

                    $banner = Banner::where('status',1)->orderBy('is_top','asc')->get();
                    $banner = BannerResource::collection($banner)->response()->getData(true);
                    $data['banner']        = $banner['data'];
                    $data['album']         = $data1;
                    $data['recent']        = !empty($resent)? $resent['data']: null; 
                    $data['genres']        = $genres['data'];
                    $data['artist']        = $artist['data']; 
                    return  $data;

                }
                else
                {
                    $this->message = 'No Chart found.';   
                }
            });
                $this->message = 'Success.';
                $this->status = true;
                $this->code = 200;
                $this->data = $value; 
        }  
          return $this->jsonResponse();  
    }
    
   
    
    
    public function search(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [ 
            
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {
           
            $q = Genre::where('status',1);
            if(isset($input['search']) && !empty($input['search']))
            {
                $search = $input['search'];
                $q->where(function($query) use ($search) {
                    $query->where('name', 'LIKE', '%' . $search . '%');
                });
            }
            $genres = $q->inRandomOrder()->limit(6)->get();  
            
            $q = Artist::where('status',1);
            if(isset($input['search']) && !empty($input['search']))
            {
                $search = $input['search'];
                $q->where(function($query) use ($search) {
                    $query->where('name', 'LIKE', '%' . $search . '%');
                });
            }
            $artist = $q->inRandomOrder()->limit(6)->get();
            $q = Track::with('getRelease','getMedia')->where('is_upload',1);
            if(isset($input['search']) && !empty($input['search']))
            {
                $search = $input['search'];
                $q->where(function($query) use ($search) {
                    $query->where('title', 'LIKE', '%' . $search . '%');
                });
            }
            $track = $q->inRandomOrder()->limit(6)->get();
            
            $q = Release::where('status',0)->with('getTrack','getTrack.getTrackMedia');
            if(isset($input['search']) && !empty($input['search']))
            {
                $search = $input['search'];
                $q->where(function($query) use ($search) {
                    $query->where('title', 'LIKE', '%' . $search . '%');
                });
            }    
            $release = $q->inRandomOrder()->limit(6)->get();
            
            $genres= GenreResource::collection($genres)->response()->getData(true);
            $artist= ArtistResource::collection($artist)->response()->getData(true);
            $release= ReleaseResource::collection($release)->response()->getData(true);
            $track= TrackResource::collection($track)->response()->getData(true); 
            
            $data['genres']         = $genres['data'];
            $data['artist']         = $artist['data'];           
            $data['track']          = $track['data'];
            $data['release']        = $release['data'];
            
            
            $this->message = 'Success.';
            $this->status = true;
            $this->code = 200;
            $this->data = $data; 
            
        } 
         return $this->jsonResponse();  
    }
        
    public function genres(Request $request)
    {
      $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [ 
            
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {
            $data = Cache::remember('genre', 60,function () { 
                $genres = Genre::where('status',1)->get();
               return GenreResource::collection($genres)->response()->getData(true);            
            }); 
            if(!empty($data['data']))
            {
                $this->message ='Success';
                $this->status = true;
                $this->code = 200;
                $this->data = $data['data'];                 
            }
            else
            {
              $this->message = 'Not found.';  
            }

            
        } 
         return $this->jsonResponse();  
    }
    
    public function artist(Request $request)
    {
      $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [ 
            
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {
           
            $data = Artist::where('status',1)->paginate(40);
            $data= ArtistResource::collection($data)->response()->getData(true);
            if(!empty($data['data']))
            {
                $this->message ='Success';
                $this->status = true;
                $this->code = 200;
                $this->data = $data;                 
            }
            else
            {
              $this->message = 'Not found.';  
            }
        } 
         return $this->jsonResponse();  
    }
    
    public function notification(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [ 
            'page' => 'required',
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {
            $this->timeZoneUpdate($request,Auth::user()->id);
            $this->Models_nin->where(['to_id'=>Auth::user()->id])->update(['is_read'=>1]);
            $test =  $this->Models_nin->select('notifications.*')->where(['to_id'=>Auth::user()->id])->orderBy('id','desc')->paginate(20);
           
            $test= NotificationResource::collection($test)->response()->getData(true);
            $test['first_page_url'] = $test['links']['first'];
            $test['last_page_url'] = $test['links']['last'];
            $test['prev_page_url'] = $test['links']['prev'];
            $test['next_page_url'] = $test['links']['next'];
            $test['current_page'] = $test['meta']['current_page'];
            $test['last_page'] = $test['meta']['last_page'];
            $test['from'] = $test['meta']['from'];
            $test['path'] = $test['meta']['path'];
            $test['per_page'] = $test['meta']['per_page'];
            $test['to'] = $test['meta']['to'];
            $test['total'] = $test['meta']['total'];
            unset($test['links']);
            unset($test['meta']);
            if(isset($test['data'][0]))
            {
                $this->message = 'Notification get successfully.';
                $this->status = true;
                $this->code = 200;
                $this->data = $test; 
            }
            else
            {
                $this->message = 'Notification not found.';
            }
        }
        return $this->jsonResponse();  
    }    
}