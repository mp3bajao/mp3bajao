<?php 
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use JWTAuth;
use App\Models\UserLang; 
use App\Models\UserDevice; 
use App\Models\UserOtp; 
use App\Models\Notification;
use App\Models\Country;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\Notifications as NotificationResource;
use Illuminate\Support\Facades\Auth; 
use Validator,App,Mail;
use Illuminate\Support\Facades\Hash;
class UserController extends Controller 
{

    public function __construct(Request $request) 
    {
        $this->request =  $request->all();
        $this->Models = new User();
        $this->Models_two = new UserDevice();
        $this->Models_thr = new UserOtp();
        $this->Models_nin = new Notification(); 
    }
    
    private function timeZoneUpdate($request ,$user_id){
        $timezone = $request->header('timezone');
        if(isset($timezone))
        {            
             $this->Models->where('id',$user_id)->update(['time_zone'=>$timezone]);             
             return true;
        }
        else
        {
           $this->Models->where('id',$user_id)->update(['time_zone'=>'Asia/Kolkata']);  
        }
        return false;
    }
    
    public function generateRandomString($type=null, $length = 6) 
    {
        if($type=='N')
        {
         $string = '0123456789';   
        }
        else if($type=='A')
        {
            $string ='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }
        else{
            $string = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }
        return substr(str_shuffle(str_repeat($string, ceil($length/strlen($string)) )),1,$length);
    }
    
    Public function resendOtp(Request $request){
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;
        $message = [
            'mobile.required' => 'Mobile Number is required.',
            'mobile.min' => 'The Mobile number must be at least 6 characters',
            'mobile.max' => 'The Mobile number may not be greater than 10 characters.',
        ];
            
        $validator = Validator::make($input, [ 
            'mobile'    => 'required|numeric|digits_between:8,10',
            'country_code'          =>  'required',
            'device_type'          =>  'required',
            'device_id'          =>  'required'
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        { 
         
            $user = $this->Models::where(['country_code'=>$input['country_code'],'mobile'=>$input['mobile']])->first();
            if(isset($user))
            {                            
                $otp = $this->Models_thr->where('user_id',$user->id)->first();
                if(!isset($otp))
                {
                    $otp  = new $this->Models_thr;
                    $otp->user_id = $user->id;
                }
                $otps = $this->generateRandomString('N',6);
                // $otps = 123456;
                $otp->otp = $otps;
                $otp->save(); 
                $this->sendOtpMail($user,$otps);
                $this->message = 'Verification code has been sent on  your mail. please visit your mail box.';                                                     
                $device = $this->Models_two->where(['user_id'=>$user->id,'device_type'=>$input['device_type']])->first();                    
                if(!isset($device))
                {
                    $device = new $this->Models_two;
                    $device['user_id'] = $user->id;
                    $device['device_type'] =  $input['device_type'];
                }
                $device['device_token'] =  $input['device_id'];
                $device->save();
                                        
                $this->status = true;
                $this->code = 200;   
                $user = $this->Models->where('id',$user->id)->first();
                $data = new UserResource($user);
                $this->data = $data;                    
            }
            else
            {
              $this->message ='Your account has been deactivated or is blocked.';     
            }                    
        }
          return $this->jsonResponse();
    }
    
    public function register(Request $request){
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;
        
            $message = [
                'mobile.required' => 'Mobile Number is required.',
                'mobile.min' => 'The Mobile number must be at least 6 characters',
                'mobile.max' => 'The Mobile number may not be greater than 10 characters.',
                'password.regex' => 'I suggest you to use password confirmed to ensure user typing correct password. Within the 6 characters our regex should contain at least 3 of a-z or A-Z and number and special character.',
            ];
            
            $validator = Validator::make($input, [ 
                'name'      => 'required|max:255',
                'mobile'    => 'required|numeric|digits_between:8,10',
                'email'     => 'required|email|max:255',
                'password' => 'required|min:6|max:15|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[@!$#%]).*$/|required_with:password_confirmation|same:password_confirmation',
                'password_confirmation'   => 'required',
                'country_code'          =>  'required',
                'device_type'          =>  'required',
                'device_id'          =>  'required'
            ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        { 
            $user = User::where('email',$input['email'])->first();
            if(!isset($user))
            {
                $user = User::where('mobile',$input['mobile'])->where('country_code',$input['country_code'])->first();
                if(!isset($user))
                {
                    $input['password'] = Hash::make($input['password']); 
                    $input['status'] =  0;
                    $input['email_verified_at'] =  null;
                    $input['is_mobile_verify'] =  0;
                    
                    if(isset($input['referral_code']))
                    {
                        $data = $this->Models->where(['is_mobile_verify'=>1,'deleted_at'=>null,'referral_code'=>$input['referral_code']])->first();
                        if(isset($data))
                        {
                            $input['referral_by'] =  $data->referral_code;   
                            $input['referral_user_id'] =  $data->id;   
                        }
                    }            
                    $input['referral_code']= $this->generateRandomString(null,10);                
                    $input['role_id'] = 0;
                    $user = $this->Models->create($input);                    
                    
                    $otp = $this->Models_thr->where('user_id',$user->id)->first();
                    if(!isset($otp))
                    {
                        $otp  = new $this->Models_thr;
                        $otp->user_id = $user->id;
                    }
                    $otps = $this->generateRandomString('N',6);
                    //$otps = 123456;
                    $otp->otp = $otps;
                    $otp->save();
                    $this->sendOtpMail($user,$otps);
                    $this->message = 'Verification code has been sent on  your mail. please visit your mail box.';
                    $device = $this->Models_two->where(['user_id'=>$user->id,'device_type'=>$input['device_type']])->first();
                    if(!isset($device))
                    {
                        $device = new $this->Models_two;
                        $device['user_id'] = $user->id;
                        $device['device_type'] =  $input['device_type'];
                    }
                    $device['device_token'] =  $input['device_id'];
                    $device->save();
                    $this->status = true;
                    $this->code = 200;   
                    $user = $this->Models->where('id',$user->id)->first();
                    $data = new UserResource($user);
                    $this->data = $data;                     
                }
                else
                {   
                    $user->email = $input['email'];
                    $user->save();
                    
                    $otp = $this->Models_thr->where('user_id',$user->id)->first();
                    if(!isset($otp))
                    {
                        $otp  = new $this->Models_thr;
                        $otp->user_id = $user->id;
                    }
                    $otps = $this->generateRandomString('N',6);
                   // $otps = 123456;
                    $otp->otp = $otps;
                    $otp->save(); 
                    $this->sendOtpMail($user,$otps);
                    $this->message = 'Verification code has been sent on  your mail. please visit your mail box.';                                                     
                    $device = $this->Models_two->where(['user_id'=>$user->id,'device_type'=>$input['device_type']])->first();                    
                    if(!isset($device))
                    {
                        $device = new $this->Models_two;
                        $device['user_id'] = $user->id;
                        $device['device_type'] =  $input['device_type'];
                    }
                    $device['device_token'] =  $input['device_id'];
                    $device->save();
                                        
                    $this->status = true;
                    $this->code = 200;   
                    $user = $this->Models->where('id',$user->id)->first();
                    $data = new UserResource($user);
                    $this->data = $data;
                }
            }
            else
            {
                if(in_array($user->status,[0]))
                {
                   $otp = $this->Models_thr->where('user_id',$user->id)->first();
                    if(!isset($otp))
                    {
                        $otp  = new $this->Models_thr;
                        $otp->user_id = $user->id;
                    }
                    $otps = $this->generateRandomString('N',6);
                    //$otps = 123456;
                    $otp->otp = $otps;
                    $otp->save(); 
                    $this->sendOtpMail($user,$otps);
                    $this->message = 'Verification code has been sent on  your mail. please visit your mail box.';                                                     
                    $device = $this->Models_two->where(['user_id'=>$user->id,'device_type'=>$input['device_type']])->first();                
                    if(!isset($device))
                    {
                        $device = new $this->Models_two;
                        $device['user_id'] = $user->id;
                        $device['device_type'] =  $input['device_type'];
                    }
                    $device['device_token'] =  $input['device_id'];
                    $device->save();
                    
                    
                    $this->status = true;
                    $this->code = 200;   
                    $user = $this->Models->where('id',$user->id)->first();
                    $data = new UserResource($user);
                    $this->data = $data; 
                }
                else if(in_array($user->status,[1]))
                {
                    $this->message ='Your account has been already registered.';
                }
                else
                {
                   $this->message ='Your account has been deactivated or is blocked.';    
                }
            }            
        }
        return $this->jsonResponse();
    }
    
    public function login(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata =$input;
        $validation = [
            'device_type'       => 'required',
            'device_id'         => 'required',
            'mobile'            => 'nullable|numeric|digits_between:8,10|',
            'country_code'      => 'required',
            'email'             => 'nullable|email',
            'password'          => 'required',
        ]; 
        $message = [];
        $validator = Validator::make($input, $validation,$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {
            $input['role_id'] = 0;
            if(isset($input['email']))
            {
                $user = $this->Models->where(['email'=>$input['email']])->first();                
            }
            if(isset($input['mobile']))
            {
                $user = $this->Models->where(['mobile'=>$input['mobile'],'country_code'=>$input['country_code']])->first();    
            }
                       
            if(isset($user))
            {
                if($user->status==1)
                {
                    if($user->is_mobile_verify==1 || $user->email_verified_at!=null)
                    {                       
                        $this->message = 'Login successfully.';                                                     
                        $device = $this->Models_two->where(['user_id'=>$user->id,'device_type'=>$input['device_type']])->first();                
                        if(!isset($device))
                        {
                            $device = new $this->Models_two;
                            $device['user_id'] = $user->id;
                            $device['device_type'] =  $input['device_type'];
                        }
                        $device['device_token'] =  $input['device_id'];
                        $device->save();

                       
                        $this->code = 200;   
                        $user = $this->Models->where('id',$user->id)->first();
                        $credentials = ['email'=>$user->email,'password'=>$input['password']];
                        if (! $token = JWTAuth::attempt($credentials)) {
                           
                            $this->message = 'Invalid Users';
                            return $this->jsonResponse();
                        }
                        $this->status = true;
                        $user['token']= $token;
                        $data = new UserResource($user);
                        $this->data = $data;                     
                    }
                    else
                    {
                        $this->message ='Your account has not been Verified.';    
                    }
                }
                else
                {
                   $this->message ='Your account has been deactivated or is blocked.';    
                } 
            }
            else
            {
                $this->message ='Your are not registered. Please registered.';    
            }
        }
        return $this->jsonResponse();
    }   
    
    /*protected function createNewToken($token,$message){
        
        return response()->json([
            'access_token' => $token,
            'token_type' => 'Bearer',
            'status' => 1,
            'message'=>$message,
            'user' => auth()->user()
        ]);
    }*/
    
    
    public function forgotPassword(Request $request){
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;
        
        $validator = Validator::make($input, [ 
            'email'   => 'required|email',
        ]);
 
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {
           $user = $this->Models->where(['email'=>$input['email']])->first();                                                                              
            if(isset($user))
            {
                $otp = $this->Models_thr->where('user_id',$user->id)->first();
                if(!isset($otp))
                {
                    $otp  = new $this->Models_thr;
                    $otp->user_id = $user->id;
                }                                
                $otps = $this->generateRandomString('N',6);
                
                //$otps = 123456;
                $otp->otp = $otps;
                $otp->save();  
                        
                $this->status = true;
                $this->code = 200; 
                $this->sendOtpMail($user,$otps);
                $data = new UserResource($user);
                $this->data = $data;
                $this->message ='Otp code has been sent on your mail address.';  
            }
            else
            {
             $this->message ='You are not registered.';     
            }
        }
        return $this->jsonResponse();
    }
    
    public function resetPassword(Request $request){
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;
        
        $validator = Validator::make($input, [ 
            'email'   => 'required|email',
            'otp'   => 'required',
            'password' => 'required|min:6|max:15|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[@!$#%]).*$/|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation'   => 'required',
        ]);
 
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {
           $user = $this->Models->where(['email'=>$input['email']])->first();                                                                              
            if(isset($user))
            {
                $otp = $this->Models_thr->where('user_id',$user->id)
                        ->where('otp',$input['otp'])
                        ->first();
                if(isset($otp))
                {
                    $user->password = Hash::make($input['password']);
                    $user->save();
                    $this->status = true;
                    $this->code = 200;  
                     $data = new UserResource($user);
                    $this->data = $data;
                    $this->message ='Password reset successfully.'; 
                }
                else
                {
                    $this->message ='Invalid OTP.';     
                }
                
            }
            else
            {
                $this->message ='You are not registered.';     
            }
        }
        return $this->jsonResponse();
    }
    
    public function otpVerify(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;
        
            $message = [
                'mobile.required' => 'Mobile Number is required.',
                'mobile.min' => 'The Mobile number must be at least 8 characters',
                'mobile.max' => 'The Mobile number may not be greater than 10 characters.'
            ];
            
            $validator = Validator::make($input, [ 
                'country_code'   => 'required',
                'mobile'=> 'required|numeric|digits_between:8,10',
                'device_type'   => 'required',
                'device_id'     => 'required',
                'otp'           => 'required',
                'password'      => 'required',
            ],$message);
 

        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {  
            $input['role_id'] = 0;
            $user = $this->Models->where(['country_code'=>$input['country_code'],'mobile'=>$input['mobile']])->first();                                                                              
            if(isset($user))
            {
                if($user->status==0)
                {
                    if($user->role_id==$input['role_id'])
                    {
                        $otp = $this->Models_thr->where('user_id',$user->id)->first();            
                        if(isset($otp->otp) && $otp->otp==$input['otp'])
                        {                                                       
                            $user->status = 1;
                            $user->is_mobile_verify = 1;                                                        
                            $user->email_verified_at = date('Y-m-d H:i:s');                                                        
                            $user->save();                                                        
                            //$this->timeZoneUpdate($request,$user->id);
                            $credentials = ['mobile'=>$user->mobile,'password'=>$input['password']];
                            if (! $token = JWTAuth::attempt($credentials)) {                               
                                $this->message = 'Invalid Users';
                                return $this->jsonResponse();
                            }
                            $message  = 'Login Successfully';
                            //return $this->createNewToken($token,$message); 
                    
                    
                           // $token = $user->createToken('MyApp')->accessToken; 
                            $user['token']= $token;
                            $this->status = true;
                            $this->code = 200;   
                            $user =$this->Models->find($user->id);
                            $user = $this->Models->where('id',$user->id)->first();
                            $user['token']= $token;
                            $user['total_notification'] = 0;
                            //$user['total_notification']= $this->Models_nin->where(['to_id'=>$user->id,'is_read'=>0])->count();
                            $data = new UserResource($user);

                            $this->status = true;
                            $this->code = 200;  
                            $this->data = $data;
                            $this->message ='Register Successfully.';                                                                                       
                        }
                        else
                        {
                            $this->message ='Invalid OTP.'; 
                        }
                    }
                    else
                    {
                        $this->message ='you are not authorized to access this app.';    
                    }
                }
                else if($user->status==1)
                {
                     $this->message ='Your account has been already registered.';
                }
                else
                {
                   $this->message ='Your account has not been activated or is blocked.';    
                }
            }
            else
            {
                $this->message ='You have entered an invalid mobile number. Please try again.';   
            }
        }
        return $this->jsonResponse();
    }
    
    /*public function reSendOtp(Request $request)
    {
        $this->code = 200; 
        $input =  $request->all(); 
        $this->requestdata = $input;
        
        
        $message = [
            'mobile.required' => 'Mobile Number is required.',
            'mobile.min' => 'The Mobile number must be at least 8 characters',
            'mobile.max' => 'The Mobile number may not be greater than 10 characters.'
        ]; 
        
        $validator = Validator::make($input, [
            'email'=> 'required|email',
            'device_type'   => 'required',
            'device_id'     => 'required',
        ],$message);

        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {                  
            $user = $this->Models->where(['country_code'=>$input['country_code'],'mobile'=>$input['mobile']])->first();
            $input['role_id'] = 0;            
            if(isset($user))
            {
                if($user->status==1)
                {
                    if($user->role_id==$input['role_id'])
                    {
                        //$this->timeZoneUpdate($request,$user->id);
                        $otp = $this->Models_thr->where('user_id',$user->id)->first();
                        if(!isset($otp))
                        {
                            $otp->user_id = $user->id;
                        }                                
                        $otps = $this->generateRandomString('N',4);
                        $otps = 1234;
                        $otp->otp = $otps;
                        $otp->save();   
                        $this->status = true;
                        $this->message = 'Otp send successfully.';
                    }
                    else
                    {
                        $this->message ='you are not authorized to access this app.';    
                    }
                }
                else
                {
                   $this->message ='Your account has not been activated or is blocked.';    
                }
            }
            else
            {
                $this->message ='You have entered an invalid mobile number. Please try again.';   
            }
        }
        return $this->jsonResponse();
    }*/
    
    
    public function sendOtpMail($user=null,$otp=123456)
    {
        $user->role_type =  ($user->role_id==0)? 'User' :'User';
        $data['user'] =$user;        
        Mail::send('vendor.notifications.otp',compact('otp'), function($message)use($data) {
            $message->to($data['user']->email, config('app.name'))->subject(config('app.name').' One Time Password (OTP) for '.$data['user']->role_type.' Verification');
            $message->from('support@mp3bajao.com',config('app.name'));
        });
    }

    public function socialLogin(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata =$input; 
        $message = [
                //'dob.required' => 'Date of Birth is required.',
                //'contact_number.required' => 'Mobile Number is required.',
                //'contact_number.min' => 'The Mobile number must be at least 10 characters',
                //'contact_number.max' => 'The Mobile number may not be greater than 10 characters.'
            ];
            $validator = Validator::make($input, [ 
                'name'           => 'required|max:50',
                'device_type'    => 'required',
                'device_id'      => 'required',
                'email'          => 'required|email',
                'social_id'      => 'required',
                'social_type'    => 'required',
                'latitude'       => 'required',
                'longitude'      => 'required',
            ]);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {
            $input['role_id'] = 0;
            $user = $this->Models->where(['social_type'=>$input['social_type'],'social_id'=>$input['social_id']])->first();            
            if(isset($user)){
                if($user->status==1)
                {
                    $this->timeZoneUpdate($request,$user->id);
                    if($user->role_id==$input['role_id'])
                    {
                        $user->latitude = $input['latitude'];
                        $user->longitude = $input['longitude']; 
                        $user->is_mobile_verify = 1;
                        $user->save();                        
                        $device = $this->Models_two->where(['user_id'=>$user->id,'device_type'=>$input['device_type']])->first();                
                        if(!isset($device))
                        {
                            $device = new $this->Models_two;
                            $device['user_id'] = $user->id;
                            $device['device_type'] =  $input['device_type'];
                        }
                        $device['device_id'] =  $input['device_id'];
                        $device->save();
                        $token = $user->createToken('MyApp')->accessToken; 
                        $user['token']= $token;
                        $this->data = $user->makeHidden(['updated_at','remember_token','email_verified_at','dob','address','referral_user_id','referral_code','referral_by','deleted_at','get_otp.user_id','getSpecialty.updated_at','experience','commission','reason']);
                        $this->message ='Login Successfully.';
                    }
                    else
                    {
                      $this->message ='you are not authorized to access this app.';    
                    }
                }
                else
                {
                  $this->message ='Your account has not been activated or is blocked.';  
                }
            }
            else
            {
                if(isset($input['email']))
                {
                    $user = $this->Models->where(['email'=>$input['email']])->with(['getSpecialty','getDocument'])->first();            
                }
                else
                {
                    $user =null;
                }
                if(isset($user))
                {
                    if($user->status==1)
                    {
                        if($user->role_id==$input['role_id'])
                        {
                            $user->name = $input['name'];
                            $user->social_type = $input['social_type'];
                            $user->social_id = $input['social_id'];
                            $user->password = 123456; 
                            $user->status =  1;
                            $user->latitude = $input['latitude'];
                            $user->longitude = $input['longitude']; 
                            $user->is_mobile_verify = 1;
                            $user->save();
                            $device = $this->Models_two->where(['user_id'=>$user->id,'device_type'=>$input['device_type']])->first();                                            
                            if(!isset($device))
                            {
                                $device = new $this->Models_two;
                                $device['user_id'] = $user->id;
                                $device['device_type'] =  $input['device_type'];
                            }
                            $device['device_id'] =  $input['device_id'];
                            $device->save();
                            
                            $token = $user->createToken('MyApp')->accessToken; 
                            $user['token']= $token;
                            $this->data = $user->makeHidden(['updated_at','remember_token','email_verified_at','dob','address','referral_user_id','referral_code','referral_by','deleted_at','get_otp.user_id','getSpecialty.updated_at','experience','commission','reason']);
                            $this->message ='Login Successfully.';                    
                            
                        }
                        else
                        {
                            $this->message ='you are not authorized to access this app.';    
                        }
                    }
                    else
                    {
                        $this->message ='Your account has not been activated or is blocked.';  
                    }
                }
                else
                {                           
                    $user = new $this->Models;
                    $user->name = $input['name'];
                    $user->email = $input['email'] ?? null;
                    $user->social_type = $input['social_type'];
                    $user->social_id = $input['social_id'];
                    $user->password = Hash::make($this->generateRandomString(null,10)); 
                    $user->status =  1;
                    $user->latitude = $input['latitude'];
                    $user->longitude = $input['longitude']; 
                    $user->is_mobile_verify = 1;
                    $user->role_id = $input['role_id'];
                    $user->save();
                    $device = $this->Models_two->where(['user_id'=>$user->id,'device_type'=>$input['device_type']])->first();                                            
                    if(!isset($device))
                    {
                        $device = new $this->Models_two;
                        $device['user_id'] = $user->id;
                        $device['device_type'] =  $input['device_type'];
                    }
                    $device['device_id'] =  $input['device_id'];
                    $device->save();
                    $token = $user->createToken('MyApp')->accessToken; 
                    $user['token']= $token;
                    $this->message ='Login Successfully.';
                }                        
            }
            $this->status = true;
            $this->code = 200;
            $this->data = $user->makeHidden(['updated_at','remember_token','email_verified_at','dob','address','referral_user_id','referral_code','referral_by','deleted_at','get_otp.user_id','getSpecialty.updated_at','experience','commission','reason']);
        }
        return $this->jsonResponse();
    }
    
    public function logout(Request $request)
    {
        //$this->timeZoneUpdate($request,Auth::user()->id);
        $input = $request->all();
        $this->requestdata = $input;
        $this->Models_two->where('user_id',Auth::user()->id)->delete();
        $user = Auth::logout();       
        $this->status = true;
        $this->code = 200;
        $this->message ='Logout successfully.'; 
        return $this->jsonResponse();
    }
   
    public function profile(Request $request)
    { 
        //$this->timeZoneUpdate($request,Auth::user()->id);
        $input = $request->all();
        $this->requestdata = $input; 
        $data = $this->Models->where(['id'=>Auth::user()->id])->first(); 
        $data = new UserResource($data);
        $this->data = $data;                     
        $this->status = true;
        $this->code = 200;
        $this->message ='User get successfully.'; 
        return $this->jsonResponse();
    }
    
    public function editProfile(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $profile_image =  $request->file('profile_image');
        $message = [];
            $validator = Validator::make($request->all(), [ 
                'name'          => 'required|min:1|max:191',
                'email'         => 'required|email|unique:users,email, '. Auth::user()->id .',id',
                'mobile'        => 'required|numeric|digits_between:7,15|unique:users,mobile, '. Auth::user()->id . ',id,country_code,'.str_replace('+','',$input['country_code']),            
                'profile_image' => 'mimes:jpeg,png,jpg,gif|max:2048',
            ],$message);
        if ($validator->fails()) {
            $this->errorValidation($validator);
        }else{
            $data = Auth::user();
            //$this->timeZoneUpdate($request,$data->id);
            
            $data->name = $input['name'];
            $data->email = $input['email'];
            $data->country = $input['country'];
            if(isset($input['country_code']))
            {
                $data->country_code = $input['country_code'];                
            }
            if(isset($input['mobile']))
            {
                $data->mobile = $input['mobile'];                
            }
            if(isset($profile_image))
            {
                $result = image_upload($profile_image,'user');
                $data->image = $result[2];            
            }                          
            $data->save();            
            $this->data = $this->Models->where('id',Auth::user()->id)->first();
            $this->status = true;
            $this->code = 200;
            $this->message ='User profile update successfully.';
        }
        return $this->jsonResponse();
    }
  
    public function pages(Request $request)
    {
        $this->code = 200;
        $input = $request->all();
        $validator = Validator::make($input, [ 
            'slug'     => 'required',
        ]);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {            
            $data = $this->Models_six->where(['slug'=>$input['slug'],'status'=>1])->first();            
            if(isset($data))
            { 
                $this->data = $data;
                $this->status = true;
                $this->message = 'page get successfully.';
            }
            else
            {
                $this->message ='Page not found.';   
            }
        }
        return $this->jsonResponse();    
    }
        
    public function notification(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [ 
            'page' => 'required',
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {
           // $this->timeZoneUpdate($request,Auth::user()->id);
            $this->Models_nin->where(['to_id'=>Auth::user()->id])->update(['is_read'=>1]);
            $data =  $this->Models_nin->select('notifications.*')->where(['to_id'=>Auth::user()->id])->orderBy('id','desc')->paginate(20);           
            $data= NotificationResource::collection($data)->response()->getData(true);            
            if(isset($data['data'][0]))
            {
                $this->message = 'Notification get successfully.';
                $this->status = true;
                $this->code = 200;
                $this->data = $data; 
            }
            else
            {
                $this->message = 'Notification not found.';
            }
        }
        return $this->jsonResponse();  
    }  
    
    public function notificationDelete(Request $request)
    {
        $input =  $request->all();
        $this->requestdata = $input;
        $this->timeZoneUpdate($request,Auth::user()->id);
        $data = $this->Models_nin->where(['to_id'=>Auth::user()->id]);
        if(isset($input['notification_id']))
        {
           $data->where('id',$input['notification_id']);  
        }
        $data->delete();
        $this->message = 'Notification Delete successfully.';
        $this->status = true;
        $this->code = 200;
        return $this->jsonResponse();
    }   

    public function faq(Request $request)
    {
       $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [ 
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        { 
            $data =  $this->Models_twl->where('status',1)->get(); 
            if(isset($data[0]))
            {
                $this->message = 'Faq get successfully.';
                $this->status = true;
                $this->code = 200;
                $this->data = $data;
            }
            else
            {
              $this->message = 'Faq not found.';  
            }           
        }
       return $this->jsonResponse(); 
    }
    
    public function country(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [ 
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        { 
            $data =  Country::orderBy('name','asc')->get(); 
            if(isset($data[0]))
            {
                $this->message = 'Successfully.';
                $this->status = true;
                $this->code = 200;
                $this->data = $data;
            }
            else
            {
              $this->message = 'found.';  
            }           
        }
       return $this->jsonResponse(); 
    }
     
}