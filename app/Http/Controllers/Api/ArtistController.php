<?php 
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Models\Release; 
use App\Models\Genre; 
use App\Models\Artist; 
use App\Models\Track; 
use App\Models\PlaylistCategory;
use App\Http\Resources\Notifications as NotificationResource;
use App\Http\Resources\Genres as GenreResource;
use App\Http\Resources\Artist as ArtistResource;
use App\Http\Resources\Release as ReleaseResource;
use App\Http\Resources\Track as TrackResource;
use App\Http\Resources\Playlist as PlaylistResource;
use App\Http\Resources\TrackDetails as TrackDetailsResource;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth; 
use Validator,App,Mail;
class ArtistController extends Controller 
{

    public function __construct(Request $request) 
    {
        $this->request =  $request->all();
        $this->Models = new Release();
        $this->Models_two = new Artist();        
    }
    
    private function timeZoneUpdate($request ,$user_id){
        $timezone = $request->header('timezone');
        if(isset($timezone))
        {            
             $this->Models->where('id',$user_id)->update(['time_zone'=>$timezone]);             
             return true;
        }
        else
        {
           $this->Models->where('id',$user_id)->update(['time_zone'=>'Asia/Kolkata']);  
        }
        return false;
    }
    
    

    public function index(Request $request)
    {
      $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [ 
            
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {
           $page =  $input['page'] ?? 1;
            $data = Cache::remember('artist_'.$page, 120,function () { 
                $data = Artist::trackCount()->where('status',1)->orderBy('top_views','desc')->paginate(24);
                return ArtistResource::collection($data)->response()->getData(true);
            });
            if(!empty($data['data']))
            {
                $this->message ='Success';
                $this->status = true;
                $this->code = 200;
                $this->data = $data;                 
            }
            else
            {
              $this->message = 'Not found.';  
            }
        } 
         return $this->jsonResponse();  
    }
    
    public function details(Request $request)
    {
         $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [ 
            
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {
            //$data = Cache::remember('artistDetails_'.$input['id'], 120,function ()use($request) { 
                $artist  = Artist::trackCount()->where('id',$request->id)->orWhereRaw('LOWER(name) ' . 'LIKE ' . '"%' . strtolower($request->id) . '%"')->first();
                $data['id'] = $artist->id;
                $data['name'] = $artist->name;
                $data['image'] = $artist->image;
                $data['is_like'] = $artist->is_favorite;
                $data['track_count'] = $artist->track_count;
                $data['total_like']  = $artist->total_like;
                $data['total_sharing'] = $artist->total_sharing;
                $data['total_comment'] = $artist->total_comment;
                $data['total_views'] = $artist->total_views;
                $data['is_playlist'] = 0;
                $data['rating'] = $artist->rating;
                $data['from'] = 'Artist';
                $data['follow'] = $artist->follow;
                $data['unfollow'] = $artist->unfollow;
                $data['is_follow'] =$artist->is_follow;
                $q = Track::with('getRelease','getMedia')->whereRaw('LOWER(primary_artist) ' . 'LIKE ' . '"%' . strtolower($artist->name) . '%"');
                $release_id =  $q->orderBy('id','desc')->pluck('release_id')->toArray();
                $track_id =  $q->orderBy('id','desc')->pluck('id')->toArray();
                
                $tracks = $q->get();                
                $tracks= TrackDetailsResource::collection($tracks)->response()->getData(true);
                $data['track_list'] = $tracks['data'];
                
                $release = Release::whereIn('id',$release_id)->with('getTrack')->orderby('id','desc')->limit(6)->get();
                $release = ReleaseResource::collection($release)->response()->getData(true);;
                $data['release_latest'] = $release['data'];
                
                $release_ids = Release::whereIn('id',$release_id)->with('getTrack')->orderby('id','desc')->limit(6)->pluck('id')->toArray();
                $release = Release::whereIn('id',$release_id)->whereNotIn('id',$release_ids)->with('getTrack')->limit(20)->get();
                //dd($release);
                $release = ReleaseResource::collection($release)->response()->getData(true);;
                $data['release_list'] = $release['data'];
                

                $playlist = PlaylistCategory::with('getSinglePlaylist.getTrack.getMedia')->whereHas('getPlaylist',function($q)use($track_id){
                    $q->whereIn('track_id',$track_id);
                })->get();
                if(isset($playlist[0]))
                {
                $playlist = PlaylistResource::collection($playlist)->response()->getData(true);;
                $data['playlist'] = $playlist['data'];
                }
                else{
                    $data['playlist'] = null;
                }
                
                
                
                //return $data;
            //});
            
            
            if(isset($data))
            {
                $this->message ='Success';
                $this->status = true;
                $this->code = 200;
                $this->data = $data;                 
            }
            else
            {
              $this->message = 'Not found.';  
            }
        } 
         return $this->jsonResponse();
    }
    

}