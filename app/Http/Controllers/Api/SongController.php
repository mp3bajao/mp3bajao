<?php 
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Models\Release; 
use App\Models\Genre; 
use App\Models\Artist; 
use App\Models\Track; 
use App\Models\Language; 
use App\Models\TopChart; 
use App\Models\Chart; 
use App\Models\Playlist; 
use App\Models\PlaylistCategory; 
use App\Http\Resources\Notifications as NotificationResource;
use App\Http\Resources\Genres as GenreResource;
use App\Http\Resources\Artist as ArtistResource;
use App\Http\Resources\Release as ReleaseResource;
use App\Http\Resources\Track as TrackResource;
use App\Http\Resources\TrackAlbum as TrackAlbumResource;


use App\Http\Resources\TrackDetails as TrackDetailsResource;
use App\Http\Resources\Playlist as PlaylistResource;
use App\Http\Resources\PlaylistDetails as PlaylistDetailsResource;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth; 
use Validator,App,Mail;

class SongController extends Controller 
{
    public function __construct(Request $request) 
    {
        $this->request =  $request->all();
        $this->Models = new Release();
        $this->Models_two = new Artist();        
    }
    
    private function timeZoneUpdate($request ,$user_id){
        $timezone = $request->header('timezone');
        if(isset($timezone))
        {            
             $this->Models->where('id',$user_id)->update(['time_zone'=>$timezone]);             
             return true;
        }
        else
        {
           $this->Models->where('id',$user_id)->update(['time_zone'=>'Asia/Kolkata']);  
        }
        return false;
    }
    
    public function index(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [ 
            
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {
            $track  = null;
            $track_type = array();
            $page = $input['page'] ?? 1;
            
            if(isset($input['charts']))
            {
                $charts = $input['charts'];
                $data = Cache::remember('song_search_'.$page.'_'.trim(strtolower(str_replace('_',' ', $charts))), 120,function ()use($charts) {
                $chart = Chart::whereRaw('LOWER(`name`) LIKE ? ',[trim(strtolower(str_replace('_',' ', $charts))).'%'])->first();
                $q =  TopChart::with('getRelease.getTrack.getMedia','getTrack.getRelease','getTrack.getMedia','getPlaylistCategory.getSinglePlaylist.getTrack.getMedia')->where('type','Home')->where('chart_id',$chart->id);
                $top_charts= $q->orderBy('rank','asc')->paginate(24); 
                
                
                 if(isset($top_charts[0])){
                     $track_type1 = array();
                      foreach($top_charts as $top_chart){
                        $track_type = array();                        
                        if( $top_chart->chart_type == 'SINGLE')
                        {
                           if($top_chart->format=='Album')
                           {
                                $track_type['chart_type'] = $top_chart->chart_type;
                                $track_type['Chart_title'] = null;
                                $track_type['Chart_Image'] = null;
                                $track_type['format'] = $top_chart->format  ?? 'Album';
                                $release    = $top_chart->getRelease;                          
                                $release = new ReleaseResource($release);
                                $track_type['data'] = $release;
                                $track_type1[] = $track_type;
                           }
                           else
                           {
                                $track_type['chart_type'] = $top_chart->chart_type;
                                $track_type['Chart_title'] = null;
                                $track_type['Chart_Image'] = null;
                                $track_type['format'] = $top_chart->format ?? 'Track'; 
                                $track    = $top_chart->getTrack;                          
                                $track = new TrackResource($track);
                                $track_type['data'] = $track;
                                $track_type1[] = $track_type;
                           }

                        }
                        
                        if( $top_chart->chart_type == 'MULTIPLE')
                        {
                          
                           $track_type['chart_type'] = $top_chart->chart_type;
                           
                           $track_type['Chart_title'] = ucfirst($top_chart->getPlaylistCategory->name);
                           $track_type['Chart_Image'] = $top_chart->getPlaylistCategory->image;
                           $track_type['format'] = $top_chart->format ?? 'Playlist';
                           $track    = $top_chart->getPlaylistCategory;
                           $track = new PlaylistResource($track);
                           $track_type['data'] = $track;
                           $track_type1[] = $track_type;
                        }
                        
                      } 
                      $data1['track'] = $track_type1;
                      $data1['links']['first'] = $top_charts->path().'?page=1';
                      $data1['links']['last'] = $top_charts->path().'?page='.$top_charts->lastPage();
                      $data1['links']['prev'] = $top_charts->path().'?page='.($top_charts->currentPage()-1);
                      $data1['links']['next'] = $top_charts->path().'?page='.($top_charts->currentPage()+1);
                      
                      $data1['meta']['current_page'] = $top_charts->currentPage();
                      $data1['meta']['from'] = $top_charts->lastPage();
                      $data1['meta']['last_page'] = $top_charts->lastPage();
                      $data1['meta']['path'] = $top_charts->path();
                      $data1['meta']['per_page'] = $top_charts->perPage();
                      $data1['meta']['to'] = $top_charts->perPage();
                      $data1['meta']['total'] = $top_charts->total();
                   }
                return $track  = $data1 ?? null;
                });
            }
            elseif(isset($input['lang']))
            {
               $data = Cache::remember('song_search_'.$page.'_'.$input['lang'], 120,function ()use($input) {
                $q =  TopChart::with('getRelease.getTrack.getMedia','getTrack.getRelease','getTrack.getMedia','getPlaylistCategory.getSinglePlaylist.getTrack.getMedia')->where('type','Music')->where('language',$input['lang']);
                $top_charts= $q->orderBy('rank','asc')->paginate(24); 
                 if(isset($top_charts[0])){
                     $track_type1 = array();
                      foreach($top_charts as $top_chart){
                        $track_type = array();                        
                        if( $top_chart->chart_type == 'SINGLE')
                        {
                           if($top_chart->format=='Album')
                           {
                                $track_type['chart_type'] = $top_chart->chart_type;
                                $track_type['Chart_title'] = null;
                                $track_type['Chart_Image'] = null;
                                $track_type['format'] = $top_chart->format ?? 'Album';
                                $release    = $top_chart->getRelease;                          
                                $release = new ReleaseResource($release);
                                $track_type['data'] = $release;
                                $track_type1[] = $track_type;
                           }
                           else
                           {
                                $track_type['chart_type'] = $top_chart->chart_type;
                                $track_type['Chart_title'] = null;
                                $track_type['Chart_Image'] = null;
                                $track_type['format'] = $top_chart->format ?? 'Track'; 
                                $track    = $top_chart->getTrack;                          
                                $track = new TrackResource($track);
                                $track_type['data'] = $track;
                                $track_type1[] = $track_type;
                           }

                        }
                        
                        if( $top_chart->chart_type == 'MULTIPLE')
                        {
                          
                           $track_type['chart_type'] = $top_chart->chart_type;
                           
                           $track_type['Chart_title'] = ucfirst($top_chart->getPlaylistCategory->name);
                           $track_type['Chart_Image'] = $top_chart->getPlaylistCategory->image;
                           $track_type['format'] = $top_chart->format ?? 'Playlist';
                           $track    = $top_chart->getPlaylistCategory;
                           $track = new PlaylistResource($track);
                           $track_type['data'] = $track;
                           $track_type1[] = $track_type;
                        }
                        
                      } 
                      $data1['track'] = $track_type1;
                      $data1['links']['first'] = $top_charts->path().'?page=1';
                      $data1['links']['last'] = $top_charts->path().'?page='.$top_charts->lastPage();
                      $data1['links']['prev'] = $top_charts->path().'?page='.($top_charts->currentPage()-1);
                      $data1['links']['next'] = $top_charts->path().'?page='.($top_charts->currentPage()+1);
                      
                      $data1['meta']['current_page'] = $top_charts->currentPage();
                      $data1['meta']['from'] = $top_charts->lastPage();
                      $data1['meta']['last_page'] = $top_charts->lastPage();
                      $data1['meta']['path'] = $top_charts->path();
                      $data1['meta']['per_page'] = $top_charts->perPage();
                      $data1['meta']['to'] = $top_charts->perPage();
                      $data1['meta']['total'] = $top_charts->total();
                   }
                return $track  = $data1 ?? null;
               });
            }
            
            elseif(isset($input['genres']))
            {
                $data = Cache::remember('song_search_'.$page.'_'.$input['genres'], 120,function ()use($input) {
                $releases    = Track::TrackCount()->with('getRelease','getMedia')->where('genre_id',$input['genres'])->orderBy('id','desc')->paginate(24);
                if($releases->total()>0)
                {
                    foreach($releases as $release)
                    {
                        $track_type['chart_type'] = 'Single';
                        $track_type['Chart_title'] = null;
                        $track_type['Chart_Image'] = null;
                        $track_type['format'] = 'Album';                                           
                        $release = new TrackAlbumResource($release);
                        $track_type['data'] = $release;
                        $track_type1[] = $track_type; 
                    }

                    $data1['track'] = $track_type1;
                    $data1['links']['first'] = $releases->path().'?page=1';
                    $data1['links']['last'] = $releases->path().'?page='.$releases->lastPage();
                    $data1['links']['prev'] = $releases->path().'?page='.($releases->currentPage()-1);
                    $data1['links']['next'] = $releases->path().'?page='.($releases->currentPage()+1);

                    $data1['meta']['current_page'] = $releases->currentPage();
                    $data1['meta']['from'] = $releases->lastPage();
                    $data1['meta']['last_page'] = $releases->lastPage();
                    $data1['meta']['path'] = $releases->path();
                    $data1['meta']['per_page'] = $releases->perPage();
                    $data1['meta']['to'] = $releases->perPage();
                    $data1['meta']['total'] = $releases->total();  
                }
                return $track  = $data1 ?? null;
                });
            }
            elseif(isset($input['search']))
            {
                $page = $input['page'] ?? 1;
                $search= $input['search'];
                $data = Cache::remember('song_search_'.$page.'_'.$search, 120,function ()use($page,$search,$input) { 
               
                $releases  = Track::TrackCount()->with('getRelease','getMedia')->where(function($query1)use($search){
                $query1->where('title', 'LIKE', '%' . $search . '%')
                   ->orWhere('primary_artist', 'LIKE', '%' . $search . '%')
                   ->orWhere('composer', 'LIKE', '%' . $search . '%')
                   ->orWhere('producer', 'LIKE', '%' . $search . '%')
                   ->orWhere('production_year', 'LIKE', '%' . $search . '%')
                   ->orWhere('publisher', 'LIKE', '%' . $search . '%')
                   ->orWhere('author', 'LIKE', '%' . $search . '%');                                 
                   })->paginate(24); 
                if($releases->total()>0)
                {
                    foreach($releases as $release)
                    {
                        $track_type['chart_type'] = 'Single';
                        $track_type['Chart_title'] = null;
                        $track_type['Chart_Image'] = null;
                        $track_type['format'] = 'Album';                                           
                        $release = new TrackAlbumResource($release);
                        $track_type['data'] = $release;
                        $track_type1[] = $track_type; 
                    }

                    $data1['track'] = $track_type1;
                    $data1['links']['first'] = $releases->path().'?page=1';
                    $data1['links']['last'] = $releases->path().'?page='.$releases->lastPage();
                    $data1['links']['prev'] = $releases->path().'?page='.($releases->currentPage()-1);
                    $data1['links']['next'] = $releases->path().'?page='.($releases->currentPage()+1);

                    $data1['meta']['current_page'] = $releases->currentPage();
                    $data1['meta']['from'] = $releases->lastPage();
                    $data1['meta']['last_page'] = $releases->lastPage();
                    $data1['meta']['path'] = $releases->path();
                    $data1['meta']['per_page'] = $releases->perPage();
                    $data1['meta']['to'] = $releases->perPage();
                    $data1['meta']['total'] = $releases->total();  
                }
                return $track  = $data1 ?? null;
                });
            }
            else
            {
                $data = Cache::remember('song_'.$page, 120,function ()use($input) { 
                $releases    = Release::TrackCount()->with('getTrack.getMedia')->orderBy('id','desc')->paginate(24);
                if($releases->total()>0)
                {
                    foreach($releases as $release)
                    {
                        $track_type['chart_type'] = 'Single';
                        $track_type['Chart_title'] = null;
                        $track_type['Chart_Image'] = null;
                        $track_type['format'] = 'Album';                                           
                        $release = new ReleaseResource($release);
                        $track_type['data'] = $release;
                        $track_type1[] = $track_type; 
                    }

                    $data1['track'] = $track_type1;
                    $data1['links']['first'] = $releases->path().'?page=1';
                    $data1['links']['last'] = $releases->path().'?page='.$releases->lastPage();
                    $data1['links']['prev'] = $releases->path().'?page='.($releases->currentPage()-1);
                    $data1['links']['next'] = $releases->path().'?page='.($releases->currentPage()+1);

                    $data1['meta']['current_page'] = $releases->currentPage();
                    $data1['meta']['from'] = $releases->lastPage();
                    $data1['meta']['last_page'] = $releases->lastPage();
                    $data1['meta']['path'] = $releases->path();
                    $data1['meta']['per_page'] = $releases->perPage();
                    $data1['meta']['to'] = $releases->perPage();
                    $data1['meta']['total'] = $releases->total();                                      
                }
                return $track  = $data1 ?? null;
                });
            }
            
            

           $this->code = 200;
            if(isset($data))
            {
                $this->message = 'Success.';
                $this->status = true;
               
                $this->data = $data;   
            }
            else{
               $this->message = 'Track Not found'; 
            }
            
        }   
        return $this->jsonResponse();  
    }
    
    public function index1(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [ 
            
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {           
            $q = Release::where('status',0)->with('getTrack.getTrackMedia');
            if(isset($input['search']) && !empty($input['search']))
            {
                $search = $input['search'];
                $q->where(function($query) use ($search) {
                    $query->where('title', 'LIKE', '%' . $search . '%')
                            ->orWhereHas('getGenre',function($query1)use($search){
                                 $query1->where('name', 'LIKE', '%' . $search . '%');
                            })
                            ->orWhereHas('getSubGenre',function($query1)use($search){
                                 $query1->where('title', 'LIKE', '%' . $search . '%');
                            })
                            ->orWhereHas('getTrack',function($query1)use($search){
                                 $query1->where('title', 'LIKE', '%' . $search . '%')
                                        ->orWhere('primary_artist', 'LIKE', '%' . $search . '%')
                                        ->orWhere('composer', 'LIKE', '%' . $search . '%')
                                        ->orWhere('producer', 'LIKE', '%' . $search . '%')
                                        ->orWhere('production_year', 'LIKE', '%' . $search . '%')
                                        ->orWhere('publisher', 'LIKE', '%' . $search . '%')
                                        ->orWhere('author', 'LIKE', '%' . $search . '%');
                            });
                });
             }
             
             if(isset($input['slug']) && !empty($input['slug']))
             {
                if(in_array($input['slug'],['bhakit','Devotional & Spiritual']))
                {
                  $q->where('genre_id',2);   
                }
                if(in_array($input['slug'],['qawwalis','sufi']))
                {
                  $q->where('genre_id',28);   
                }
                if(in_array($input['slug'],['ghzazals']))
                {
                  $q->where('genre_id',23);   
                }
                if(in_array($input['slug'],['hip_hop']))
                {
                  $q->where('genre_id',9);   
                }
                if(in_array($input['slug'],['punjabi']))
                {
                  $q->where('genre_id',26);   
                }
                if(in_array($input['slug'],['hindi','bollywood']))
                {
                  $q->where('genre_id',21);   
                }
             }
             
            if(isset($input['genres']) && !empty($input['genres']))
            {
                $search = $input['genres'];
                $q->where(function($query) use ($search) {
                    $query->where('genre_id',$search);
                });
            }
            
            if(isset($input['lang']) && !empty($input['lang']))
            {
                $search = $input['lang'];
                $q->whereHas('getTrack',function($query1)use($search){
                    $query1->where('title_language',$search);
                });
            }
            
            $release =  $q->orderBy('is_top', 'asc')->orderBy('id','desc')->paginate(96);
            if(isset($release[0]))
            {
                $release= ReleaseResource::collection($release)->response()->getData(true);
                $this->message = 'Success.';
                $this->status = true;
                $this->code = 200;
                $this->data = $release;                             
            }
            else{
              $this->message = 'Songs not avalable.';  
            }

        } 
        return $this->jsonResponse();  
    }
    
    
    public function details(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [ 
            'id' => 'required',
            'form' => 'required'
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {
            if(strtolower($input['form'])=='playlist')
            {
                $data = PlaylistCategory::TrackCount()->with('getUser')->where('id',$input['id'])->first();                 
                if(isset($data))
                {
                    $playlist = Playlist::where('playlist_type',$data->id)->pluck('track_id')->toArray();
                    $track  = Track::TrackCount()->with('getMedia','getRelease')->whereIn('id',$playlist)->orderBy('id','desc')->get();
                    $data['from'] = 'Playlist';
                    $data['primary_artist'] = $data->getUser->name;
                    $data['total_comment'] = $data->total_comment;
                    $data['total_views'] = $data->total_views;
                    //dd($data->total_sharing);
                    $data['total_sharing'] = $data->total_sharing;
                    $data['track_list']= TrackDetailsResource::collection($track)->response()->getData(true);
                    $release = Release::with('getTrack.getMedia')->inRandomOrder()->limit(15)->get();
                    $data['release']= ReleaseResource::collection($release)->response()->getData(true);
                    $this->message = 'Success.';
                    $this->status = true;
                    $this->code = 200;
                    $this->data = $data;
                }
                else
                {
                    $this->message = 'Playlist not found.';
                    $this->status = false;
                    $this->code = 200;     
                }
            }
            if(strtolower($input['form'])=='track')
            {
                $data = array();
                $track = Track::TrackCount()->with('getMedia','getRelease')->where('id',$input['id'])->with('getRelease')->orderBy('id','desc')->first();  
                if(isset($track))
                {
                    $data['id'] = $track->id;
                    $data['name'] = $track->title;
                    $data['image'] = $track->getRelease->album_profile;
                    $data['song']  = $track->getMedia->songs;
                    $data['primary_artist'] = $track->primary_artist;
                    $data['is_like']    = $track->is_favorite;
                    $data['track_count'] = 1;
                    $data['total_like']  = $track->total_like;
                    $data['total_sharing'] = $track->total_sharing;
                    $data['total_comment'] = $track->total_comment;
                    $data['total_views'] = $track->total_views;
                    $data['is_playlist'] = $track->is_playlist;
                    $data['rating'] = $track->rating;



                    $data['from'] = 'Track';
                    $track  = Track::TrackCount()->where('id',$input['id'])->with('getRelease','getMedia')->get();  
                    $data['track_list']= TrackDetailsResource::collection($track)->response()->getData(true);
                    $release = Release::with('getTrack.getMedia')->inRandomOrder()->limit(15)->get();
                    $data['release']= ReleaseResource::collection($release)->response()->getData(true);
                    $this->message = 'Success.';
                    $this->status = true;
                    $this->code = 200;
                    $this->data = $data;  
                }
                else
                {
                    $this->message = 'Song not found.';
                    $this->status = false;
                    $this->code = 200;     
                }
            }
            if(strtolower($input['form'])=='album'){
              $album = Release::TrackCount()->with('getTrack.getMedia')->where('id',$input['id'])->first();
/*echo '<pre>';
              print_r($album);
              die;
              */
              if(isset($album))
              {
                $data['id'] = $album->id;
                $data['name'] = $album->title;
                $data['image'] = $album->album_profile;
                $data['song']  = $album->getTrack->getMedia->songs;
                $data['primary_artist'] = $album->primary_artist;
                $data['is_like']    = $album->is_favorite;
                $data['track_count'] = $album->track_count;
                $data['total_like']  = $album->total_like;
                $data['total_sharing'] = $album->total_sharing;
                $data['total_comment'] = $album->total_comment;
                $data['total_views'] = $album->total_views;
                $data['is_playlist'] = $album->is_playlist;
                $data['rating'] = $album->rating;
   
                $data['from'] = 'Album';
                $track  = Track::TrackCount()->where('release_id',$input['id'])->with('getRelease','getMedia')->orderBy('id','desc')->get();  
                $data['track_list']= TrackDetailsResource::collection($track)->response()->getData(true);
                $release = Release::with('getTrack.getMedia')->inRandomOrder()->limit(15)->get();
                $data['release']= ReleaseResource::collection($release)->response()->getData(true);
                $this->message = 'Success.';
                $this->status = true;
                $this->code = 200;
                $this->data = $data;                 
              }
              else
              {
                    $this->message = 'Album not found.';
                    $this->status = false;
                    $this->code = 200; 
              }
            }
            
          
                          
        } 
        return $this->jsonResponse();  
    }
    
    
    public function languages(Request $request)
    {
        $this->code = 200;
        $input =  $request->all();
        $this->requestdata = $input;        
        $message = [];
        $validator = Validator::make($input, [ 
        ],$message);
        if ($validator->fails()) 
        {
            $this->errorValidation($validator);
        }
        else
        {
            $data  = Language::get();            
            $this->message = 'Success.';
            $this->status = true;
            $this->code = 200;
            $this->data = $data;               
        } 
        return $this->jsonResponse();  
    }
    
}