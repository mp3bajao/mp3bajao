<?php
namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Gate;
use App\Models\Media;
use File,DB;
use App\Models\Stores;

class StoreController extends Controller
{
    protected $lang='store';
    protected $page='store';
    
    public function __construct() {
        $this->Models = new  Stores;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       if($request->ajax())
       {
        Gate::authorize(ucfirst($this->page).'-section');
        $data=$this->Models->select('title','image','created_at','id','status')->where('sender','!=','');
        return Datatables::of($data)->editColumn('created_at', function ($data) {
            return $data->created_at->format('m/d/Y h:m:s'); 
        })->addIndexColumn()->make(true);
        }
       else
       {
            $data = ['lang'=>$this->lang,'page'=>$this->page]; 
            return routeView($this->page.'.index',$data);
       }
    }
    
    /*public function frontend()
    {
        Gate::authorize('Genres-section');
        return view('category.listing');
    }*/

    public function edit_frontend($id)
    {
        Gate::authorize(ucfirst($this->page).'-edit');
        $data['category'] = $this->Models->findOrFail($id);
        return view($this->page.'.edit',$data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        Gate::authorize(ucfirst($this->page).'-create');
        if($request->ajax())
        {
            if($request->method()=='POST')
            {
                $mesasge = [
                    'name.required'=>'The Name field is required.',                    
                    'name.max'=>'The name may not be greater than 255 characters.',
                    'image.size'  => 'the file size is less than 2MB',
                ];
                $this->validate($request, [
                   'name'  => 'required|max:255',
                   'data-exchange-id'  => 'required|max:255|',
                   'data-destination-party-id'  => 'required|max:255',
                   'username'  => 'required|max:255',
                   'password'  => 'required|max:255',
                   'image' => 'image|mimes:jpeg,png,jpg,gif ,svg|max:2048',
                ],$mesasge);
                $input = $request->all();
                $file = $request->file('image');
                try{ 
                    
                     $dataInput =[ 
                        'title' => $input['name'],
                        'data-exchange-id' =>$input['data-exchange-id'],
                        'data-destination-party-id' =>$input['data-destination-party-id'],
                        'username' =>$input['username'],
                        'password' =>$input['password'],
                         'status'   =>1,
                         'sender'   => str_replace(' ','',strtoupper($input['name']))
                    ];
                            
                    if(isset($file))
                    {
                       $result = image_upload($file,'store');
                       $dataInput['image']= $result[1];
                    }
                    
                   $data = $this->Models->create($dataInput);
                    
                    
                    $result['message'] = ucfirst($this->lang).' has been created';
                    $result['status'] = 1;
                    return response()->json($result);

                } catch (Exception $e){
                    $result['message'] = ucfirst($this->lang).' can`t created';
                    $result['status'] = 0;
                    return response()->json($result);            
                }
            }        
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
        Gate::authorize(ucfirst($this->page).'-section');
        $data = $this->Models->findOrFail($id);
        $data = ['lang'=>$this->lang,'page'=>$this->page,'category'=>$data]; 
        return routeView($this->page.'.edit',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //
        Gate::authorize(ucfirst($this->page).'-edit');
        if($request->ajax())
        {
            if($request->method()=='POST')
            {
                $mesasge = [
                    'name.required'=>'The Name field is required.',                    
                    'name.max'=>'The name may not be greater than 255 characters.',
                    'image.size'  => 'the file size is less than 2MB',
                ];
                $this->validate($request, [
                   'name'  => 'required|max:255',
                   'data-exchange-id'  => 'required|max:255|',
                   'data-destination-party-id'  => 'required|max:255',
                   'username'  => 'required|max:255',
                   'password'  => 'required|max:255',
                   'image' => 'image|mimes:jpeg,png,jpg,gif ,svg|max:2048',
                ],$mesasge);

                $input = $request->all();
                $cate_id = $id;
                $file = $request->file('image');
                try{
                    $dataInput =[ 
                        'title' => $input['name'],
                        'data-exchange-id' =>$input['data-exchange-id'],
                        'data-destination-party-id' =>$input['data-destination-party-id'],
                        'username' =>$input['username'],
                        'password' =>$input['password'],
                    ];
                            
                    if(isset($file))
                    {
                       $result = image_upload($file,'store');
                       $dataInput['image']= $result[1];
                    }
                   
                    $data = $this->Models->where('id',$cate_id)->update($dataInput);
                     
                    $result['message'] = ucfirst($this->lang).' updated successfully.';
                    $result['status'] = 1;
                    return response()->json($result);
                }
                catch (Exception $e)
                {
                    $result['message'] = ucfirst($this->lang).' Can`t be updated.';
                    $result['status'] = 0;
                    return response()->json($result);           
                }
            }
            else{
                $data = $this->Models->findOrFail($id);
                $data = ['lang'=>$this->lang,'page'=>$this->page,'category'=>$data]; 
                return routeView($this->page.'.edit',$data);
            }
        }      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gate::authorize(ucfirst($this->page).'-delete');
        if($this->Models::findOrFail($id)->delete()){
            $result['message'] = 'Food Category deleted successfully';
            $result['status'] = 1;
        }else{
            $result['message'] = 'Food Category Can`t deleted';
            $result['status'] = 0;
        }
        return response()->json($result);
    }
    public function status($id, $status)
    {
        $details = $this->Models::find($id); 
        if(!empty($details)){
            if($status == 'active'){
                $inp = ['status' => 1];
            }else{
                $inp = ['status' => 0];
            }
            $Category = $this->Models::findOrFail($id);
            if($Category->update($inp)){
                if($status == 'active'){
                    $result['message'] = ucfirst($this->lang).' is activate successfully';
                    $result['status'] = 1;
                }else{
                    $result['message'] = ucfirst($this->lang).' is deactivate successfully';
                    $result['status'] = 1; 
                }
            }else{
                $result['message'] = ucfirst($this->lang).' status can`t be updated!!';
                $result['status'] = 0;
            }
        }else{
            $result['message'] = 'Invaild '.ucfirst($this->lang).'!!';
            $result['status'] = 0;
        }
        return response()->json($result);
    }
}
