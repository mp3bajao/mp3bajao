<?php
namespace App\Http\Controllers;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\Models\EmailTemplate;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Mail;

use File,DB;

class EmailTemplateController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Gate::authorize('Category-section');
        $email=EmailTemplate::select('notifications.*');
        return Datatables::of($email)->editColumn('created_at', function ($email) {
            return $email->created_at->format('m/d/Y h:m:s'); 
        })->addIndexColumn()->make(true);
    }
    
    public function frontend()
    {
        Gate::authorize('Category-section');
        return view('emails.listing',$data);
    }

    public function edit_frontend($id)
    {
        Gate::authorize('Category-edit');
        $data['emails'] = EmailTemplate::findOrFail($id);
        return view('emails.edit',$data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        Gate::authorize('Category-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Gate::authorize('Category-create');
         // validate
		$this->validate($request, [           
            'name'=>'required',
            'description'=>'required',
        ]);
 
        $notification = new Notification();

        $notification->name 		= $request->name;
        $notification->notification_type= $request->notification_type;
        $notification->title 			= $request->title;
        $notification->message 			= $request->message;
        
        if($notification->save()){
            $result['message'] = 'Notification Send Successfully';
            $result['status'] = 1;
        }else{
            $result['message'] = 'Notification Can`t be Send';
            $result['status'] = 0;
        }
        return response()->json($result);
    }





}
