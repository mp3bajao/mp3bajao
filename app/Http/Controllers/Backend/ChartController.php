<?php
namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\User;
use App\Models\Country;
use App\Models\Release;
use App\Models\TopChart;
use App\Models\Genre;
use App\Models\Chart;
use App\Models\Language;
use App\Models\Track;
use App\Models\MultiChart;
use App\Models\Artist;
use App\Models\PlaylistCategory;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Gate;
class ChartController extends Controller
{
    protected $lang='chart';
    protected $page='chart';
    
    public function __construct() 
    {          
       $this->Models        = new Chart(); 
       $this->Models_two    = new TopChart();
       $this->Models_three  = new Language();   
       $this->Models_four   = new MultiChart();   
       $this->Models_five   = new Artist();   
       $this->Models_six   = new PlaylistCategory();   
       $this->sortable_columns = [
            0 => 'id',
            1 => 'id',
            2 => 'id',
            3 => 'title',
            4 => 'posted_by',
            5 => 'track_type',
            6 => 'created_at',
        ];
       $this->sortable_columns2 = [
            0 => 'id',
            1 => 'id',
            2 => 'id',
            3 => 'name',
            6 => 'created_at',
        ];
     
    }
    
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data=$this->Models->select('name','created_at','id','status','rank','description');
            return Datatables::of($data)->editColumn('created_at', function ($data) {
                return $data->created_at->format('m/d/Y h:m:s'); 
            })->addIndexColumn()->make(true);
        }
        else
        {
            $data = ['lang'=>$this->lang,'page'=>$this->page]; 
            return routeView($this->page.'.index',$data);
        }
    }
    
    
    public function create(Request $request)
    {
        //Gate::authorize(ucfirst($this->page).'-create');
        if($request->ajax())
        {
            if($request->method()=='POST')
            {
                $mesasge = [
                    'name.required'=>'The Name field is required.',                    
                    'name.max'=>'The name may not be greater than 255 characters.',
                ];
                $this->validate($request, [
                   'name'  => 'required|max:255',
                   'description'  => 'required',
                ],$mesasge);
                $input = $request->all(); 
                try{  
                    $rank = $this->Models::orderBy('rank','desc')->first();
                    $rank = isset($rank) ? $rank->rank : 0;
                    $data = new $this->Models;
                    
                    $data->name = $input['name'];
                    $data->description = $input['description'] ?? null;
                    $data->rank = ($rank+1);
                    $data->status= 1;
                    $data->save();
                    
                    $result['message'] = ucfirst($this->lang).' has been created';
                    $result['status'] = 1;
                    return response()->json($result);

                } catch (Exception $e){
                    $result['message'] = ucfirst($this->lang).' can`t created';
                    $result['status'] = 0;
                    return response()->json($result);            
                }
            }        
        }
    }
    
    public function edit(Request $request, $id)
    {
        //
       // Gate::authorize(ucfirst($this->page).'-edit');
        if($request->ajax())
        {
            if($request->method()=='POST')
            {
                $mesasge = [
                    'name.required'=>'The Name field is required.',                    
                    'name.max'=>'The name may not be greater than 255 characters.',
                    //'image.size'  => 'the file size is less than 2MB',
                ];
                $this->validate($request, [
                   'name'  => 'required|max:255|unique:genres',
                   'description'  => 'required|max:255',
                   //'image' => 'image|mimes:jpeg,png,jpg,gif ,svg|max:2048',
                ],$mesasge);

                $input = $request->all();
                $cate_id = $id;
                $file = $request->file('image');
                try{

                    if(isset($file))
                    {
                       $result = image_upload($file,'category');
                       $data = $this->Models->where('id',$cate_id)->update(['name'=>$input['name'],'image'=>$result[1],'description'=>$input['description']]);
                    }
                    else
                    {
                        $data = $this->Models->where('id',$cate_id)->update(['name'=>$input['name'],'description'=>$input['description']]);
                    }
                     
                    $result['message'] = ucfirst($this->lang).' updated successfully.';
                    $result['status'] = 1;
                    return response()->json($result);
                }
                catch (Exception $e)
                {
                    $result['message'] = ucfirst($this->lang).' Can`t be updated.';
                    $result['status'] = 0;
                    return response()->json($result);           
                }
            }
            else{
                $data = $this->Models->findOrFail($id);
                $data = ['lang'=>$this->lang,'page'=>$this->page,'category'=>$data]; 
                return routeView($this->page.'.edit',$data);
            }
        }      
    }
    
    public function destroy($id)
    {
        //Gate::authorize(ucfirst($this->page).'-delete');
        if(Category::findOrFail($id)->delete()){
            $result['message'] = 'Food Category deleted successfully';
            $result['status'] = 1;
        }else{
            $result['message'] = 'Food Category Can`t deleted';
            $result['status'] = 0;
        }
        return response()->json($result);
    }
    
    public function status($id, $status)
    {
        $details = $this->Models::find($id); 
        if(!empty($details)){
            if($status == 'active'){
                $inp = ['status' => 1];
            }else{
                $inp = ['status' => 0];
            }
            $Category = $this->Models::findOrFail($id);
            if($Category->update($inp)){
                if($status == 'active'){
                    $result['message'] = ucfirst($this->lang).' is activate successfully';
                    $result['status'] = 1;
                }else{
                    $result['message'] = ucfirst($this->lang).' is deactivate successfully';
                    $result['status'] = 1; 
                }
            }else{
                $result['message'] = ucfirst($this->lang).' status can`t be updated!!';
                $result['status'] = 0;
            }
        }else{
            $result['message'] = 'Invaild '.ucfirst($this->lang).'!!';
            $result['status'] = 0;
        }
        return response()->json($result);
    }
    
    
    public function addChartRank(Request $request)
    {
        $details = $this->Models::find($request['id']);        
        if(!empty($details)){
            $details->rank = $request['values'];
            $details->save();
            
            /*$list = $this->Models::where('id','>',$request['id'])->get();
            $range =  $request['values'];
            if(isset($list[0]))
            {
                foreach($list as $list)
                {
                  $list->rank = ($range+1);
                  $list->save();            
                  $range++;
                }
            }*/ 
            $result['message'] = ucfirst($this->lang).' is activate successfully';
            $result['status'] = 1;
        }
        return response()->json($result);
    }
    public function addChartSongRank(Request $request)
    {
        $details = $this->Models_two::find($request['id']);        
        if(!empty($details)){
            $details->rank = $request['values'];
            $details->save();
            $result['message'] = ucfirst($this->lang).' is activate successfully';
            $result['status'] = 1;
        }
        return response()->json($result);
    }
    public function addChartArtistRank(Request $request)
    {
        $details = $this->Models_five::find($request['id']);        
        if(!empty($details)){
            $details->is_top = $request['values'];
            $details->save();
            $result['message'] = ucfirst($this->lang).' is activate successfully';
            $result['status'] = 1;
        }
        return response()->json($result);
    }
    
    
        
    public function ChartSongindex(Request $request)
    {
       if($request->ajax())
       {
            $limit = $request->input('length');
            $start = $request->input('start');
            $search = $request['search']['value'];
            $orderby = $request['order']['0']['column'];
            $order = $orderby != "" ? $request['order']['0']['dir'] : "";
            $draw = $request['draw'];
            $type = $request->chart_type;
            $chart_id = $request->chart_id;


            $totaldata = $this->Models_two->getModel($limit, $start, $search, $this->sortable_columns[$orderby], $order,$chart_id)->count();
            $response = $this->Models_two->getModel($limit, $start, $search, $this->sortable_columns[$orderby], $order,$chart_id)
                    ->offset($start)
                    ->limit($limit)
                    ->get();
            
            if (!$response) {
                $data = [];
                $paging = [];
            } else {
                $data = $response;
                $paging = $response;
            }

            $datas = array();
            $i = 1;
            $break = 60;
            foreach ($data as $value) {
                $u['id'] = $i;
                if($value->chart_type=='SINGLE')
                {
                    $u['upc'] = $value->getTrack->getRelease->UPC;
                    $name = isset($value->getMultiChart->name) ? ucfirst($value->getMultiChart->name).' - ' : '';                   
                    if($value->format=='Track')
                    {
                     $u['title'] = ucfirst($value->getTrack->title ?? '').'<br><small><b> '.$name.ucfirst($value->chart_type).' - '. str_replace('0','',$value->format ).'</b></small>';                                               
                    }
                    else{
                    $u['title'] = ucfirst($value->getRelease->title ?? '').'<br><small><b> '.$name.ucfirst($value->chart_type).' - '. str_replace('0','',$value->format ).'</b></small>';                                                
                    }

                    $u['album_type'] = ucfirst($value->chart_type);
                    $u['track'] = $value->track_count; 
                }
                else
                {
                    $u['upc'] = $value->getPlaylistCategory->name;
                    $u['title'] = ucfirst($value->getPlaylistCategory->name).'<br><small><b> '.ucfirst($value->chart_type).'</b></small>';
                    $u['album_type'] = ucfirst($value->chart_type);
                    $u['track'] = $value->track_count;    
                }
                $u['rank'] = '<input type="number" class="form-control changeRank" data-id="'.$value->id.'" min="0" value="'.$value->rank.'">';
                $u['actions'] = '<input type="checkbox" class="add_top_list" value="'.$value->track_id.'" checked>';
                $datas[] = $u;
                $i++;
                unset($u);
            }
            $return = [
                "draw" => intval($draw),
                "recordsFiltered" => intval($totaldata),
                "recordsTotal" => intval($totaldata),
                "data" => $datas
            ];
            return $return;
        }
       else
       {

            $chart = $this->Models::where('status',1)->orderBy('id','asc')->get();  
            $multichart = $this->Models_six::where('status',1)->orderBy('id','desc')->get();
            $language = $this->Models_three::where('status',1)->orderBy('id','asc')->get();            
            $data = ['lang'=>$this->lang,'page'=>$this->page,'chart'=>$chart,'language'=>$language,'multichart'=>$multichart]; 
            return routeView($this->page.'.song_index',$data);
       }
       
    }
    
    public function Musicindex(Request $request)
    {
       if($request->ajax())
       {
            $limit = $request->input('length');
            $start = $request->input('start');
            $search = $request['search']['value'];
            $orderby = $request['order']['0']['column'];
            $order = $orderby != "" ? $request['order']['0']['dir'] : "";
            $draw = $request['draw'];
            $type = $request->chart_type;
            $totaldata = $this->Models_two->getModel($limit, $start, $search, $this->sortable_columns[$orderby], $order,$type)->where(['type'=>'Music','language'=>$request->language])->count();
            $response = $this->Models_two->getModel($limit, $start, $search, $this->sortable_columns[$orderby], $order,$type)
                        ->where(['type'=>'Music','language'=>$request->language])
                        ->offset($start)
                        ->limit($limit)
                        ->get();
            
            if (!$response) {
                $data = [];
                $paging = [];
            } else {
                $data = $response;
                $paging = $response;
            }

            $datas = array();
            $i = 1;
            $break = 60;
            foreach ($data as $value) {
                $u['id'] = $i;
                $u['upc'] = $value->getTrack->getRelease->UPC;
                $name = isset($value->getMultiChart->name) ? ucfirst($value->getMultiChart->name).' - ' : '';                   
                if($value->format=='Track')
                {
                    $u['title'] = ucfirst($value->getTrack->title ?? '').'<br><small><b> '.$name.ucfirst($value->chart_type).' - '. str_replace('0','',$value->format ).'</b></small>'; 
                }
                else
                {
                    $u['title'] = ucfirst($value->getRelease->title ?? '').'<br><small><b> '.$name.ucfirst($value->chart_type).' - '. str_replace('0','',$value->format ).'</b></small>';
                }
                $u['rank'] = '<input type="number" class="form-control changeRank" data-id="'.$value->id.'" min="0" value="'.$value->rank.'">';
                $u['actions'] = '<input type="checkbox" class="add_top_list" value="'.$value->track_id.'" checked>';
                $datas[] = $u;
                $i++;
                unset($u);
            }
            $return = [
                "draw" => intval($draw),
                "recordsFiltered" => intval($totaldata),
                "recordsTotal" => intval($totaldata),
                "data" => $datas
            ];
            return $return;
        }
       else
       {
            $language = $this->Models_three::where('status',1)->orderBy('id','asc')->get();            
            $data = ['lang'=>$this->lang,'page'=>$this->page,'language'=>$language]; 
            return routeView($this->page.'.music_index',$data);
       }
       
    }
  
    public function chartsearch(Request $request)
    {
        $input = $request->all();
        $search = $input['serching'];
        $q = Track::select('tracks.*','releases.UPC')
            ->leftJoin('releases', 'releases.id', '=', 'tracks.release_id')
            ->where(function($query)use($search){
        $query->whereRaw('LOWER(tracks.title) ' . 'LIKE ' . '"%' . strtolower($search) . '%"')
            ->orwhereRaw('LOWER(tracks.primary_artist) ' . 'LIKE ' . '"%' . strtolower($search) . '%"')
            ->orwhereRaw('LOWER(releases.UPC) ' . 'LIKE ' . '"%' . strtolower($search) . '%"')
            ->orwhereRaw('LOWER(releases.id) ' . 'LIKE ' . '"%' . strtolower($search) . '%"')
            ->orwhereRaw('LOWER(releases.format) ' . 'LIKE ' . '"%' . strtolower($search) . '%"');
        });        
        $data = $q->limit(100)->get();
        $data = ['data'=>$data];
        return routeView($this->page.'.search',$data);
    }
    
    
    public function chartsearchArtist(Request $request)
    {
        $input = $request->all();         
        $artist = $this->Models_five::where('id',$input['chart_id'])->first();
        $search = $input['serching'];
        $q = Track::select('tracks.*','releases.UPC')
            ->leftJoin('releases', 'releases.id', '=', 'tracks.release_id')
            ->where(function($query)use($search){
        $query->whereRaw('LOWER(tracks.title) ' . 'LIKE ' . '"%' . strtolower($search) . '%"')
            ->orwhereRaw('LOWER(tracks.primary_artist) ' . 'LIKE ' . '"%' . strtolower($search) . '%"')
            ->orwhereRaw('LOWER(releases.UPC) ' . 'LIKE ' . '"%' . strtolower($search) . '%"');
        })->whereRaw('FIND_IN_SET(?,tracks.primary_artist)', [$artist->name]);        
        $data = $q->limit(100)->get();
        $data = ['data'=>$data];
        return routeView($this->page.'.search',$data);
    }
    
    public function addTopLsist(Request $request){
        $input = $request->all();
        if($input['type']=='false')
        {
           if($input['chart_type']=='Music')
           {
               TopChart::where('type','Music')->where('language',$input['language'])->where('track_id',$input['values'])->delete();
           }
           else if($input['chart_type']=='Home')
           {
                TopChart::where('type','Home')->where('chart_id',$input['chart_id'])->where('track_id',$input['values'])->delete();   
           }
        }
        else
        {
           if($input['chart_type']=='Music')
           {
                $parms =  ['type'=>$input['chart_type'],'language'=>$input['language'],'track_id'=>$input['values']];               
                $data = TopChart::where($parms)->first(); 
                if(!isset($data))
                {
                    $totalchart = TopChart::where('language',$input['language'])->count();
                    $data = new TopChart;   
                    $data->language = $input['language'];
                    $data->type = 'Music';
                    $data->chart_type = 'SINGLE';
                    $data->format = $input['format'] ?? '';
                    $data->chart_id = 0;
                    $data->track_id = $input['values'];
                    $data->rank = ($totalchart+1);
                    $data->save();  
                    
                }
           }
           else if($input['chart_type']=='Home')
           {
                if($input['album_type'] == 'SINGLE')
                {
                    $parms =  [
                        'type'=>$input['chart_type'],
                        'chart_type'=>$input['album_type'],
                        'chart_id'=>$input['chart_id'],
                        'track_id'=>$input['values'],
                        'format'=>$input['format'] ?? ''
                    ];  
                    $data = TopChart::where($parms)->first();
                    if(!isset($data))
                    {
                        $totalchart = TopChart::where(['type'=>$input['chart_type'],'chart_id'=>$input['chart_id'],'chart_type'=>'SINGLE'])->count();
                        $totalchart1 = TopChart::where(['type'=>$input['chart_type'],'chart_id'=>$input['chart_id'],'chart_type'=>'MULTIPLE'])->groupBy('chart_group_id')->get();                        
                        $totalchart = $totalchart+count($totalchart1);
                        $data = new TopChart;   
                        $data->type = 'Home';
                        $data->chart_type = $input['album_type'];
                        $data->chart_group_id =0;
                        $data->chart_type =$input['album_type'];
                        $data->chart_id = $input['chart_id'];
                        $data->track_id = $input['values'];
                        $data->format = $input['format'] ?? '';
                        $data->rank = ($totalchart+1);
                        $data->save();  
                        $data->chart_group_id = $data->id;
                        $data->save();
                    }
                
                }
                else
                {
                   $parms =  [
                        'type'=>$input['chart_type'],
                        'chart_type'=>$input['album_type'],
                        'chart_group_id'=> $input['values'],
                        'chart_id'=>$input['chart_id'],
                        'track_id'=>$input['values']
                    ];
                    $data = TopChart::where($parms)->first();
                    if(!isset($data))
                    {
                        $totalchart = TopChart::where(['type'=>$input['chart_type'],'chart_id'=>$input['chart_id'],'chart_type'=>'SINGLE'])->count();
                        $totalchart1 = TopChart::where(['type'=>$input['chart_type'],'chart_id'=>$input['chart_id'],'chart_type'=>'MULTIPLE'])->groupBy('chart_group_id')->get();                        
                        $totalchart = $totalchart+count($totalchart1);
                        $data = new TopChart;   
                        $data->type = 'Home';
                        $data->chart_type = $input['album_type'];
                        $data->chart_group_id =$input['values'];
                        $data->chart_id = $input['chart_id'];
                        $data->track_id = $input['values'];
                        $data->rank = ($totalchart+1);
                        $data->save();  
                    }
                }
               
               
               $data = TopChart::where($parms)->first(); 
           }
           
           if(!isset($data))
           {
                         
           }
        }
    }
    
    public function addTopArtistList(Request $request){
        $input = $request->all();
        if($input['type']=='false')
        {
           
            TopChart::where('type','Artist')->where('chart_id',$input['chart_id'])->where('track_id',$input['values'])->delete();   
        }
        else
        {

            $parms =  [
                'type'=>$input['chart_type'],
                'chart_id'=>$input['chart_id'],
                'track_id'=>$input['values']
            ];  
            $data = TopChart::where($parms)->first();
            if(!isset($data))
            {
                $totalchart = TopChart::where(['type'=>$input['chart_type'],'chart_id'=>$input['chart_id']])->count();                
                $data = new TopChart;   
                $data->type = 'Artist';
                //$data->chart_type = $input['album_type'];
                $data->chart_group_id =0;
                $data->chart_id = $input['chart_id'];
                $data->track_id = $input['values'];
                $data->rank = ($totalchart+1);
                $data->save();  
                $data->chart_group_id = $data->id;
                $data->save();
            }
            $data = TopChart::where($parms)->first();            
        }
    }
    
    
    public function ArtistSongIndex(Request $request)
    {
       if($request->ajax())
       {
            $limit = $request->input('length');
            $start = $request->input('start');
            $search = $request['search']['value'];
            $orderby = $request['order']['0']['column'];
            $order = $orderby != "" ? $request['order']['0']['dir'] : "";
            $draw = $request['draw'];
            $type = $request->chart_type;
            $chart_id = $request->chart_id;
            $totaldata = $this->Models_two->getModel($limit, $start, $search, $this->sortable_columns[$orderby], $order,$chart_id)->where(['type'=>'Artist'])->count();
            $response = $this->Models_two->getModel($limit, $start, $search, $this->sortable_columns[$orderby], $order,$chart_id)
                    ->where(['type'=>'Artist'])
                    ->offset($start)
                    ->limit($limit)
                    ->get();
            
            if (!$response) {
                $data = [];
                $paging = [];
            } else {
                $data = $response;
                $paging = $response;
            }

            $datas = array();
            $i = 1;
            $break = 60;
            foreach ($data as $value) {
                $u['id'] = $i;
                $u['upc'] = $value->getTrack->getRelease->UPC;
                $u['title'] = ucfirst($value->getTrack->title).'<br><small>'.ucfirst($value->chart_type).'</small>';
                $u['album_type'] = ucfirst($value->chart_type);
                $u['track'] = $value->track_count;                
                $u['rank'] = '<input type="number" class="form-control changeRank" data-id="'.$value->id.'" min="0" value="'.$value->rank.'">';
                $u['actions'] = '<input type="checkbox" class="add_top_list" value="'.$value->track_id.'" checked>';
                $datas[] = $u;
                $i++;
                unset($u);
            }
            $return = [
                "draw" => intval($draw),
                "recordsFiltered" => intval($totaldata),
                "recordsTotal" => intval($totaldata),
                "data" => $datas
            ];
            return $return;
        }
        else
        {
            $chart = $this->Models_five::where('status',1)->orderBy('name','asc')->get();  
            $multichart = $this->Models_four::where('status',1)->orderBy('id','desc')->get();
            $language = $this->Models_three::where('status',1)->orderBy('id','asc')->get();            
            $data = ['lang'=>$this->lang,'page'=>$this->page,'chart'=>$chart,'language'=>$language,'multichart'=>$multichart]; 
            return routeView($this->page.'.artist_index',$data);
        }       
    }
    
    
     public function ArtistIndex(Request $request)
    {
       if($request->ajax())
       {
            $limit = $request->input('length');
            $start = $request->input('start');
            $search = $request['search']['value'];
            $orderby = $request['order']['0']['column'];
            $order = $orderby != "" ? $request['order']['0']['dir'] : "";
            $draw = $request['draw'];
            $type = $request->chart_type;
            $chart_id = $request->chart_id;
            $chart = $this->Models_two->where('type','Artist')->pluck('chart_id')->toArray();
            $totaldata = $this->Models_five->whereIn('id',$chart)->count();
            $response = $this->Models_five->whereIn('id',$chart)->offset($start)
                    ->limit($limit)
                    ->get();
            
            
            if (!$response) {
                $data = [];
                $paging = [];
            } else {
                $data = $response;
                $paging = $response;
            }

            $datas = array();
            $i = 1;
            $break = 60;
            foreach ($data as $value) {
                $u['id'] = $i;
                $u['title'] = ucfirst($value->name);
                $u['rank'] = '<input type="number" class="form-control changeArtistRank" data-id="'.$value->id.'" min="0" value="'.$value->is_top.'">';
                $datas[] = $u;
                $i++;
                unset($u);
            }
            $return = [
                "draw" => intval($draw),
                "recordsFiltered" => intval($totaldata),
                "recordsTotal" => intval($totaldata),
                "data" => $datas
            ];
            return $return;
        }
        else
        {
            $chart = $this->Models_five::where('status',1)->orderBy('name','asc')->get();  
            $multichart = $this->Models_four::where('status',1)->orderBy('id','desc')->get();
            $language = $this->Models_three::where('status',1)->orderBy('id','asc')->get();            
            $data = ['lang'=>$this->lang,'page'=>$this->page,'chart'=>$chart,'language'=>$language,'multichart'=>$multichart]; 
            return routeView($this->page.'.artist_index',$data);
        }       
    }
}
