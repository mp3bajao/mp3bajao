<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Hash;
use App\User;
use JWTAuth;
use App\Models\UserOtp;
use App\Models\UserDevice;

class AuthController extends Controller {

    public function __construct() {       
        $this->middleware('auth:api', ['except' => ['otpVerify','sendOtp']]);
    }

    public function sendOtp(Request $requset)
    {
        $input =  $requset->all();
        $message = [
            'mobile.required' => 'Mobile Number is required.',
            'mobile.min' => 'The Mobile number must be at least 7 characters',
            'mobile.max' => 'The Mobile number may not be greater than 15 characters.'
        ];
        $validator = Validator::make($input, [ 
            'country_code'   => 'required',
            'mobile'=> 'required|min:7|max:15',
        ],$message);
        if ($validator->fails()) 
        {
            $errors 	=	$validator->errors();
            $response['status'] = 0;
            $response['message'] = $errors;
            return response()->json($response, 422);
        }
        else
        {
            $otps = $this->generateRandomString('N',4);
            $message = "Your Platorya Otp Code is ".$otps;
            $message = str_replace(' ','%20', $message);
              
            $user = User::where(['country_code'=>$input['country_code'],'mobile'=>$input['mobile'],'status'=>1,'type'=>0])->first();
            if(isset($user))
            {
                $otp = UserOtp::where('user_id',$user->id)->first();                                               
                $otp->otp = $otps;
                $otp->save();  

                $response['status'] = 1;
                $response['message'] = 'Otp send successfully.';
                return response()->json($response, 200);
            }
            else
            {
                $otp = UserOtp::where(['country_code'=>$input['country_code'],'mobile'=>$input['mobile']])->first();
                if(!isset($otp))
                {
                    $otp = new UserOtp; 
                    $otp->country_code = $input['country_code'];
                    $otp->mobile = $input['mobile'];                        
                }
                $otp->otp = $otps;
                $otp->save();

                $response['status'] = 1;
                $response['message'] = 'Otp send successfully.';
                return response()->json($response, 200);
            }
                      
        }
    }
    

    public function otpVerify(Request $request)
    {
        $input =  $request->all();
            $message = [
                'mobile.required' => 'Mobile Number is required.',
                'mobile.min' => 'The Mobile number must be at least 7 characters',
                'mobile.max' => 'The Mobile number may not be greater than 15 characters.'
            ];
            $validator = Validator::make($input, [ 
                'country_code'   => 'required',
                'mobile'=> 'required|min:7|max:15',
                'otp'           => 'required',
                'device_type'   => 'required',
                'device_token'     => 'required'
            ],$message);
        if ($validator->fails()) 
        {
            $errors 	=	$validator->errors();
            $response['status'] = 0;
            $response['message'] = $errors;
            return response()->json($response, 422);
        }
        else
        {  
            $user = User::where(['country_code'=>$input['country_code'],'mobile'=>$input['mobile'],'status'=>1,'type'=>0])->first();   
            
           
            if(isset($user))
            {  
                 $otp = UserOtp::where('user_id',$user->id)->first();     
                if(isset($otp->otp) && $otp->otp==$input['otp'] || $input['otp']=='1234')
                {
                    $data = UserDevice::where(['user_id'=>$user->id,'device_type'=>$input['device_type']])->first();   
                    if(!isset($data))
                    {
                        $data = new UserDevice;
                        $data->user_id = $user->id;
                        $data->device_type = $input['device_type'];   
                        $data->device_token = $input['device_token'];
                        $data->save();                     
                    }
                    $data->device_token = $input['device_token'];
                    $data->save();
                    
                    $credentials = ['mobile'=>$input['mobile'],'password'=>$input['mobile']];
                    if (! $token = JWTAuth::attempt($credentials)) {
                        $response['status'] = 0;
                        $response['message'] ='Invalid Users';
                        return response()->json($response, 401);
                    }
                    $message  = 'Login Successfully';
                    return $this->createNewToken($token,$message);            
                }
                else
                {
                    $response['status'] = 0;
                    $response['message'] = "Invalid Otp number.";
                    return response()->json($response, 422);
                }
            }
            else{
                
               $otp = UserOtp::where(['country_code'=>$input['country_code'],'mobile'=>$input['mobile']])->first(); 
               
               if(isset($otp) && ($input['otp']==$otp->otp  || $input['otp']=='1234'))
               {
                    $user = new User;
                    $hash = Hash::make($input['mobile']);
                    $user->country_code = $input['country_code'];
                    $user->mobile = $input['mobile'];
                    $user->password = $hash;
                    $user->status = 1;
                    $user->type = 0;
                    $user->save();
                    $otp->user_id = $user->id;
                    $otp->save();
                    $data = new UserDevice;
                    $data->user_id = $user->id;
                    $data->device_type = $input['device_type'];   
                    $data->device_token = $input['device_token'];
                    $data->save();  
                    $credentials = ['mobile'=>$input['mobile'],'password'=>$input['mobile']];
                    if (! $token = JWTAuth::attempt($credentials)) {
                        $response['status'] = 0;
                        $response['message'] ='Invalid User';
                        return response()->json($response, 401);
                    }
                    $message  = 'OTP Verifyed Successfully';
                    return $this->createNewToken($token,$message);
               }
               else
               {
                    $response['status'] = 0;
                    $response['message'] = "Invalid Otp number.";
                    return response()->json($response, 422);
               }

            }
        }
    }

    public function otpVerifyChangeMobile(Request $request)
    {
        $input =  $request->all();
            $message = [
                'mobile.required' => 'Mobile Number is required.',
                'mobile.min' => 'The Mobile number must be at least 7 characters',
                'mobile.max' => 'The Mobile number may not be greater than 15 characters.'
            ];
            $validator = Validator::make($input, [ 
                'country_code'   => 'required',
                'mobile'=> 'required|min:7|max:15',
                'otp'           => 'required'
            ],$message);
        if ($validator->fails()) 
        {
            $errors 	=	$validator->errors();
            $response['status'] = 0;
            $response['message'] = $errors;
            return response()->json($response, 422);
        }
        else
        {  
            $user = User::where(['id'=>auth()->user()->id,'status'=>1,'type'=>0])->first();            
            if(isset($user))
            {   
                $otp = UserOtp::where(['country_code'=>$input['country_code'],'mobile'=>$input['mobile']])->first();           
                if(isset($otp->otp) && $otp->otp==$input['otp'] || $input['otp']=='1234')
                {
                    $user->mobile = $request->mobile;
                    $user->country_code = $request->country_code;
                    if($user->save()){
                        $response['status'] = 1;
                        $response['message'] = 'User Mobile Updated successfully.';
                        $response['data'] = $user;
                        return response()->json($response, 201);
                    }
                    else{
                        $response['status'] = 0;
                        $response['message'] = 'Error Occured.';
                        return response()->json($response, 401);
                    } 
                              
                }
                else
                {
                    $response['status'] = 0;
                    $response['message'] = "Invalid Otp number.";
                    return response()->json($response, 422);
                }
            }else{
                $response['status'] = 0;
                $response['message'] = "Invalid user.";
                return response()->json($response, 422);
            }
        }
    }

    // public function login(Request $request){
    //     $input =  $request->all();
    // 	$validator = Validator::make($request->all(), [
    //         'email' => 'required|email',
    //         'password' => 'required|string|min:6',
    //     ]);

    //     if ($validator->fails()) {
    //         $errors 	=	$validator->errors();
    //         $response['status'] = 0;
    //         $response['message'] = $errors;
    //         return response()->json($response, 422);
    //     }else{
    //         $user = User::where(['email'=>$input['email'],'status'=>1,'type'=>0])->first();   
    //         if(isset($user)){
    //             if (Hash::check($input['password'], $user->password)) 
    //             {
    //                 if (! $token = JWTAuth::attempt($validator->validated())) {
    //                     $response['status'] = 0;
    //                     $response['message'] ='Invalid Users';
    //                     return response()->json($response, 401);
    //                 }
    //                 return $this->createNewToken($token);
    //             }
    //             else
    //             {
    //                 $response['status'] = 0;
    //                 $response['message'] = 'User is password incorrect.';
    //                 return response()->json($response, 401);  
    //             }
    //         }else{
    //             $response['status'] = 0;
    //             $response['message'] = 'Invalid User Login.';
    //             return response()->json($response, 401);
    //         }
    //     } 
    // }

    public function saveDetails(Request $request) {
           
        if($request->type == 'updateprofile'){
            $validator = Validator::make($request->all(), [
                'first_name' => 'string|between:2,100',
                'last_name' => 'string|between:2,100',
                'email' => 'required|email|unique:users,email, '. auth()->user()->id .',id',
            ]);
        }else{
            $validator = Validator::make($request->all(), [
                'first_name' => 'required|string|between:2,100',
                'last_name' => 'required|string|between:2,100',
                'email' => 'required|string|email|max:100|unique:users',
            ]);
        }
        

        if($validator->fails()){
            $errors 	=	$validator->errors();
            $response['status'] = 0;
            $response['message'] = $errors;
            return response()->json($response, 422);
        }

        $user = User::findOrFail(auth()->user()->id);
        if(isset($user)){
            if(isset($request->first_name)){
                $user->first_name = $request->first_name; 
            }
            if(isset($request->last_name)){
                $user->last_name = $request->last_name; 
            }
            $user->name = $request->first_name.' '.$request->last_name;
            if(isset($request->email)){
                $user->email = $request->email;
            } 
            if(!$request->type == 'updateprofile'){
                $user->social_type = $request->social_type;
                $user->social_id = $request->social_id;
            }
            if($user->save()){
                $response['status'] = 1;
                $response['message'] = 'User Details updated successfully.';
                $response['data'] = $user;
                return response()->json($response, 201);
            }
            else{
                $response['status'] = 0;
                $response['message'] = 'Error Occured.';
                return response()->json($response, 401);
            } 
        }else{
            $response['status'] = 0;
            $response['message'] = 'Something worng.';
            return response()->json($response, 401);
        }
    }


    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {
        //auth()->logout();
     
        if(auth()->logout()){
            $response['status'] = 1;
            $response['message'] = 'User Successfully Logout!!.';
            return response()->json($response, 201);
        }else{
            $response['status'] = 0;
            $response['message'] = 'Something worng.';
            return response()->json($response, 401);
        }
    }

    public function refresh() {
        return $this->createNewToken(auth()->refresh(),'Refresh Token Successfully');
    }

    public function userProfile() {
        if(auth()->user()){
            $user = User::where('id',auth()->user()->id)->first();
            $data['user'] =  $user;
            $data['user']['devices'] = $user->devices; 
            $response['status'] = 1;
            $response['data'] = $data;
            $response['imageBaseUrl'] = asset('uploads/user_profile');
            return response()->json($response, 201);
        }else{
            $response['status'] = 0;
            $response['message'] = 'Something worng.';
            return response()->json($response, 401);
        }
        return response()->json();
    }

    protected function createNewToken($token,$message){
        
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'status' => 1,
            'message'=>$message,
            'user' => auth()->user()
        ]);
    }

    public function generateRandomString($type=null, $length = 6) 
    {
        if($type=='N')
        {
         $string = '0123456789';   
        }
        else if($type=='A')
        {
            $string ='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }
        else{
            $string = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }
        return substr(str_shuffle(str_repeat($string, ceil($length/strlen($string)) )),1,$length);
    }

}