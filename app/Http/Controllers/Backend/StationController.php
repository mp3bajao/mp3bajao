<?php
namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\User;
use App\Models\Country;
use App\Models\Station;
use App\Models\State;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Gate;
class StationController extends Controller
{
    protected $lang='station';
    protected $page='station';
    
    public function __construct() 
    {          
       $this->Models        = new Station(); 
       $this->Model_two        = new State(); 
    }
    
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data=$this->Models->select('name','created_at','id','status');
            return Datatables::of($data)->editColumn('created_at', function ($data) {
                return $data->created_at->format('m/d/Y h:m:s'); 
            })->addIndexColumn()->make(true);
        }
        else
        {
            $state = $this->Model_two::where('country_id',101)->get();
            $data = ['lang'=>$this->lang,'page'=>$this->page,'state'=>$state]; 
            return routeView($this->page.'.index',$data);
        }
    }
    
    
    public function create(Request $request)
    {
        //Gate::authorize(ucfirst($this->page).'-create');
        if($request->ajax())
        {
            if($request->method()=='POST')
            {
                $mesasge = [
                    'name.required'=>'The Name field is required.',                    
                    'name.max'=>'The name may not be greater than 255 characters.',
                ];
                $this->validate($request, [
                   'name'  => 'required|max:255',
                   'state'  => 'required',
                ],$mesasge);
                $input = $request->all(); 
                try{  
                    $data = new $this->Models;                    
                    $data->name = $input['name'];
                    $data->state_id = $input['state'] ?? null;
                    $data->status= 1;
                    $data->save();
                    
                    $result['message'] = ucfirst($this->lang).' has been created';
                    $result['status'] = 1;
                    return response()->json($result);

                } catch (Exception $e){
                    $result['message'] = ucfirst($this->lang).' can`t created';
                    $result['status'] = 0;
                    return response()->json($result);            
                }
            }        
        }
    }
    
    public function edit(Request $request, $id)
    {
        //
       // Gate::authorize(ucfirst($this->page).'-edit');
        if($request->ajax())
        {
            if($request->method()=='POST')
            {
                $mesasge = [
                    'name.required'=>'The Name field is required.',                    
                    'name.max'=>'The name may not be greater than 255 characters.',
                    //'image.size'  => 'the file size is less than 2MB',
                ];
                $this->validate($request, [
                   'name'  => 'required|max:255|unique:genres',
                   //'image' => 'image|mimes:jpeg,png,jpg,gif ,svg|max:2048',
                ],$mesasge);

                $input = $request->all();
                $cate_id = $id;
                $file = $request->file('image');
                try{

                    if(isset($file))
                    {
                       $result = image_upload($file,'category');
                       $data = $this->Models->where('id',$cate_id)->update(['name'=>$input['name'],'image'=>$result[1]]);
                    }
                    else
                    {
                        $data = $this->Models->where('id',$cate_id)->update(['name'=>$input['name']]);
                    }
                     
                    $result['message'] = ucfirst($this->lang).' updated successfully.';
                    $result['status'] = 1;
                    return response()->json($result);
                }
                catch (Exception $e)
                {
                    $result['message'] = ucfirst($this->lang).' Can`t be updated.';
                    $result['status'] = 0;
                    return response()->json($result);           
                }
            }
            else{
                $data = $this->Models->findOrFail($id);
                $state = $this->Model_two::where('country_id',101)->get();
                $data = ['lang'=>$this->lang,'page'=>$this->page,'category'=>$data,'state'=>$state]; 
                return routeView($this->page.'.edit',$data);
            }
        }      
    }
    
    public function destroy($id)
    {
        //Gate::authorize(ucfirst($this->page).'-delete');
        if(Category::findOrFail($id)->delete()){
            $result['message'] = 'Food Category deleted successfully';
            $result['status'] = 1;
        }else{
            $result['message'] = 'Food Category Can`t deleted';
            $result['status'] = 0;
        }
        return response()->json($result);
    }
    
    public function status($id, $status)
    {
        $details = $this->Models::find($id); 
        if(!empty($details)){
            if($status == 'active'){
                $inp = ['status' => 1];
            }else{
                $inp = ['status' => 0];
            }
            $Category = $this->Models::findOrFail($id);
            if($Category->update($inp)){
                if($status == 'active'){
                    $result['message'] = ucfirst($this->lang).' is activate successfully';
                    $result['status'] = 1;
                }else{
                    $result['message'] = ucfirst($this->lang).' is deactivate successfully';
                    $result['status'] = 1; 
                }
            }else{
                $result['message'] = ucfirst($this->lang).' status can`t be updated!!';
                $result['status'] = 0;
            }
        }else{
            $result['message'] = 'Invaild '.ucfirst($this->lang).'!!';
            $result['status'] = 0;
        }
        return response()->json($result);
    }
    
    

        
}
