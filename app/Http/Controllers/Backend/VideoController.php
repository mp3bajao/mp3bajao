<?php
namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Gate;
use File;
use App\User;
use App\Models\Genre;
use App\Models\Subgenre;
use App\Models\Release;

use App\Models\Label;
use App\Models\Track;
use App\Models\UPC;
use App\Models\Media;
use Auth;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Owenoj\LaravelGetId3\GetId3;
use Aws\S3\S3Client;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Filesystem;

class VideoController extends Controller
{
     protected $lang='video';
    protected $page='video';
    public function __construct() 
    {
        $this->Models= new Release();

        $this->Models_tre = new Label();
        $this->Models_for = new Subgenre();
        $this->Models_fiv = new Track();
           // $this->middleware('auth');
        $this->sortable_columns = [
            0 => 'id',
            1 => 'id',
            2 => 'id',
            3 => 'title',
            4 => 'posted_by',
            5 => 'track_type',
            6 => 'created_at',
        ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Gate::authorize('Product-section');
        if($request->ajax())
        {
            
           // \Session::put('previous_start_count', $request->input('start'));
          //  \Session::put('previous_user_id', $request->input('user_id'));
            $limit = $request->input('length');
            $start = $request->input('start');
            $search = $request['search']['value'];
            $orderby = $request['order']['0']['column'];
            $order = $orderby != "" ? $request['order']['0']['dir'] : "";
            $draw = $request['draw'];
            $type = $request->type;
            $totaldata = $this->Models->getModel($limit, $start, $search, $this->sortable_columns[$orderby], $order,$request->type,$request->current_status,$request->user_id)->count();
            $response = $this->Models->getModel($limit, $start, $search, $this->sortable_columns[$orderby], $order,$request->type,$request->current_status,$request->user_id)
                       ->offset($start)
                    ->limit($limit)
                    ->get();
            
            if (!$response) {
                $data = [];
                $paging = [];
            } else {
                $data = $response;
                $paging = $response;
            }

            $datas = array();
            $i = 1;
            $break = 60;
            foreach ($data as $value) {
                $u['release_id'] = '<br/><b style="padding-top: 14px;">'.$value->id.'</b>';
                $u['DT_RowId'] =($start+$i);
                
                $u['title'] = '<b style="color:#409cf6">';
                if (strlen($value->title) > 60) {
                    $pos = strpos($value->title, ' ', 60);
                    if ($pos !== false) {
                        $u['title'] .= ucfirst(substr(implode(PHP_EOL, str_split(ucfirst($value->title), $break)), 0, $pos)) . '...';
                    } else {
                        $u['title'] .= ucfirst(substr(implode(PHP_EOL, str_split(ucfirst($value->title), $break)), 0, 60)) . '...';
                    }
                } else {
                    $u['title'] .= ucfirst(implode(PHP_EOL, str_split(ucfirst($value->title), $break)));
                }
                $u['title'] .= '</b><br/><small>';
                $primary_artist1='';
                $primary_artist = Track::where('release_id',$value->id)->pluck('primary_artist')->toArray();
                if(!empty($primary_artist))
                {
                $primary_artist =array_unique(explode('|',str_replace(',','|',implode('|',$primary_artist))));//ask
                if(count($primary_artist)>3){ $primary_artist1 = 'Various Artists '; }else{ $primary_artist1 = implode(',',$primary_artist); }                
                }
                $u['title'] .= ucwords(str_replace('|',',',$primary_artist1));
                $u['title'] .='</small>';     
                $u['no_of_track'] = $value->track;                
                $u['genre'] = $value->genre_value;                
                $u['year'] = $value->production_year;                
                $u['album'] ='<img src="'.$value->album_profile.'" width="40px" class="img_zoom" data-url="'.$value->album_profile.'"/>';                
                $u['created_by'] =$value->created_by ;
                $r_date = $value->release_date?date('M d, Y',strtotime($value->release_date)):'';
                $u['label'] = !empty($value->label_value)?$value->label_value.'<br>'.$r_date:'<small>no info</small>';
                $u['producer_catalogue_number'] ='Cat# :';                
                $u['producer_catalogue_number'] .= isset($value->producer_catalogue_number)?$value->producer_catalogue_number:'empty';                
                $u['producer_catalogue_number'] .= '<br>';                
                $u['producer_catalogue_number'] .=  isset($value->UPC)?$value->UPC:'';
                $u['producer_catalogue_number'] .= '<br>Release Id:';                  
                $u['producer_catalogue_number'] .= $value->id;                  /*$value->getAllStoreRelease->count()*/              
                $u['created_at'] = $value->created_at->format('d M,Y');
                $u['actions']='<div class="btn-group" role="group" aria-label="User Actions">';
                $u['actions'] .= createViewAction($this->page.'.show',['type'=>$request->type,'id'=>encrypt($value->id)],'target="_blank"');                    
                $u['actions'] .= createEditAction($this->page.'.edit',['type'=>'release','id'=>encrypt($value->id)],'target="_blank"');
                $u['actions'] .= '<a href="javascript:;" data-toggle="tooltip" data-placement="top" data-release_data="'.$value->id.'" title="' . __('backend.track') . '" class="btn btn-xs btn-success trackList"><i class="fa fa-music" aria-hidden="true"></i></a>';
                //$u['actions'] .= createDefaultAction($this->page . '.denine', ['id' => $value->id,'type'=>'all'],'default','times-circle-o','denine');
                /*if($value->release_current_status=='TakeDown'){
                    $u['actions'] .= createViewAction($this->pageUrl.'.show',['type'=>$request->type,'id'=>encrypt($value->id)],'target="_blank"');
                    $u['DT_RowId1'] ='<i class="fa fa-close" data-toggle="tooltip" data-placement="top"  class="img_zoom" title="This release hase been TakeDown" style="font-size:30px;margin: 7px; color:#f00;"></i>'; 
                }elseif(in_array(Auth::user()->role_id,[2,3,4])){
                    if(in_array($value->status,[0,2]))
                    {
                    
                        if(checkRolePermission($this->page.'-edit',1)){
                        $u['actions'] .= createEditAction($this->page.'.edit',['type'=>'release','id'=>encrypt($value->id)],'target="_blank"');
                        } if(checkRolePermission($this->page.'-view',1)){
                        $u['actions'] .= createViewAction($this->page.'.show',['type'=>$request->type,'id'=>encrypt($value->id)],'target="_blank"');
                        } if(checkRolePermission($this->page.'-edit',1) && $value->status!=2){
                        $u['actions'] .= createDeleteAction($this->page.'.delete', ['id'=>encrypt($value->id)],$this->page.'.index');               
                        } 
                        /*if($type=='all'){
                            $u['actions'] .= '<a href="javascript:;" data-toggle="tooltip" data-placement="top" data-release_data="'.$value->id.'" title="' . __('admin_lang.upload_on_server') . '" class="btn btn-xs btn-success releaseupload"><i class="fa fa-cloud-upload"></i></a>';
                        }*
                    }
                    else{
                        $u['actions'] .= createViewAction($this->page.'.show',['type'=>$request->type,'id'=>encrypt($value->id)],'target="_blank"');
                    }
                }else{
                    if($type == "rejected"){
                        $u['actions'] .= createViewAction($this->page.'.show',['type'=>$request->type,'id'=>encrypt($value->id)],'target="_blank"');                    
                        $u['actions'] .= createEditAction($this->page.'.edit',['type'=>'release','id'=>encrypt($value->id)],'target="_blank"');
                        $u['actions'] .= createDefaultAction($this->page . '.denine', ['id' => $value->id,'type'=>'all'],'default','times-circle-o','denine');
                        $u['actions'] .= createDefaultAction($this->page . '.resend', ['id' => $value->id,'type'=>$request->type],'danger','check','resend');
                    }else{
                        if(in_array($value->status,[3,4])){
                            $u['actions'] .= createEditAction($this->page.'.edit',['type'=>'release','id'=>encrypt($value->id)],'target="_blank"');                        
                            $u['actions'] .= createViewAction($this->page.'.show',['type'=>$request->type,'id'=>encrypt($value->id)],'target="_blank"');
                            $u['actions'] .= createDefaultAction($this->page. '.denine', ['id' => $value->id,'type'=>'all'],'default','times-circle-o','denine');
                            $u['actions'] .= '<a href="javascript:;" data-toggle="tooltip" data-placement="top" data-release_data="'.$value->id.'" title="' . __('admin_lang.upload_on_server') . '" class="btn btn-xs btn-success releaseupload"><i class="fa fa-cloud-upload"></i></a>';
                            //$u['actions'] .= createDeleteAction($this->pageUrl.'.delete', ['id'=>encrypt($value->id)],$this->pageUrl.'.index');
                        }else{
                            

                            //$u['actions'] .= createDefaultAction($this->page. '.denine', ['id' => $value->id,'type'=>'all'],'default','times-circle-o','denine');
                            // $u['actions'] .= '<a href="javascript:;" data-toggle="tooltip" data-placement="top" data-release_data="'.$value->id.'" title="approve" class="btn btn-xs btn-success releaseupload"><i class="fa fa-check-square-o"></i></a>';
                           // $u['actions'] .= createDefaultAction($this->page. '.approve', ['id' => $value->id,'type'=>'all'],'success','check-square-o','approve');
                        }
                    }
                }*/
                //$u['actions'] .= createDeleteAction($this->pageUrl.'.delete', ['id'=>encrypt($value->id)],$this->pageUrl.'.index');               
                $u['actions'] .= '</div>';
                $datas[] = $u;
                $i++;
                unset($u);
            }
            $return = [
                "draw" => intval($draw),
                "recordsFiltered" => intval($totaldata),
                "recordsTotal" => intval($totaldata),
                "data" => $datas
            ];
            return $return;            
        }
        else
        {
            $data = ['lang'=>$this->lang,'page'=>$this->page]; 
            return routeView($this->page.'.index',$data);
        }
    }
    
    public function edit(Request $request) 
    {
    //    checkRolePermission($this->pageUrl.'-edit');
        $id = decrypt($request->id);
        $datas = Release::where('id', $id)->first();
        //check if duplcate UPC
        $count_upc = Release::where('id','<', $datas->id)->where('upc_temp', $datas->upc_temp)->count();
        if($count_upc>0){
            $request->session()->flash('alert-danger', 'There is some issue with this release, please contact to Administrator.');
            return redirect()->back();
        }
        $Total_track = Track::where('release_id',$id)->get()->count();
        if($Total_track>1){
            Release::where('id', $id)->update(['format'=>'ALBUM']);  
        }
        
        $data_check = $datas;
        $i=0;
        $j=0;
        $k=0;
        $l=0;
        if(!isset($datas->title)){ $i++; }else{ if(in_array($datas->title,[null,'0',''])){  $i++; }}
        if(!isset($datas->primary_artist)){ $i++;}else{ if(in_array($datas->primary_artist,[null,'0',''])){ $i++; }}
        if(!isset($datas->genre_id)){ $i++;}else{ if(in_array($datas->genre_id,[null,'0',''])){ $i++; }}
        if(!isset($datas->label_id)){ $i++;}else{ if(in_array($datas->label_id,[null,'0',''])){ $i++; }}
        if(!isset($datas->format)){ $i++;}else{ if(in_array($datas->format,[null,'0',''])){ $i++; }}
        //if(!isset($datas->UPC)){ $i++;}else{ if(in_array($datas->UPC,[null,'0',''])){ $i++; }}
        if(!isset($datas->original_release_date)){ $i++;}else{ if(in_array($datas->original_release_date,[null,'0',''])){ $i++; }}
        if(!isset($datas->p_line)){ $i++;}else{ if(in_array($datas->p_line,[null,'0',''])){ $i++; }}
        if(!isset($datas->c_line)){ $i++;}else{ if(in_array($datas->c_line,[null,'0',''])){ $i++; }}
        if(!isset($datas->production_year)){ $i++;}else{ if(in_array($datas->production_year,[null,'0',''])){ $i++; }}
        if(!isset($datas->price_id)){ $j++;}else{ if(in_array($datas->price_id,[null,'0',''])){ $j++; }}
        if(!isset($datas->release_date)){ $k++;}else{ if(in_array($datas->release_date,[null,'0',''])){ $k++; }}       
        if(!isset($datas->getOriginal()['album_profile'])){ $l++;}else{ if(in_array($datas->getOriginal()['album_profile'],[null,'0',''])){ $l++; }} 
        $total_track='0';
        $total_track_count=0;
        $total_media_count=0;
        $total_track = Track::where('release_id',$datas->id)->get()->count();
        $primary_artist = Track::where('release_id',$datas->id)->pluck('primary_artist')->toArray();
        $primary_artist =array_unique(explode('|',str_replace(',','|',implode('|',$primary_artist))));
        $total_track_count = Track::where(['release_id'=>$datas->id,'track_id'=>0])->get()->count();
        $total_media_count =Media::where(['album_id'=>$datas->id])->get()->count();
        $e_release = $i;
        $e_album = $l;
        $e_price = $j;
        $e_release_date = $k;

        $genre = Genre::pluck('name','id')->toArray();        
        $subgenre = Subgenre::where('genre_id',$datas->genre_id)->pluck('title','id')->toArray();        
        //$price = Price::pluck('title','id')->toArray();
        if(Auth::user()->role_id==4){
            $label = Label::where('sub_user_id',Auth::user()->id)->pluck('title','id')->toArray();
        }elseif(Auth::user()->role_id==3){
            $label = Label::where('sub_user_id',$datas->sub_user_id)->pluck('title','id')->toArray();
        }elseif(Auth::user()->role_id==1){
            $label = Label::where('sub_user_id',$datas->sub_user_id)->pluck('title','id')->toArray();
        }else{
            $label = Label::where('sub_user_id',$datas->sub_user_id)->pluck('title','id')->toArray();
        }
        if(Auth::user()->is_same_level==1 && Auth::user()->allowed_label_id!=""){
            $allowed_labels = User::getAllowedLabels();
            $labels = Label::whereIn('id',$allowed_labels)->pluck('title','id')->toArray();
        }elseif(Auth::user()->is_same_level==0){
            $labels = array_add($label, 'new', 'New Label');
        }else{
            $labels = $label;
        }
        //$store = StoreRelease::where('release_id',$id)->get();        
        $data = [
            'page_title'  =>  'Admin | Update  '.ucwords($this->page),
            'sdata'       =>  $datas,
            'page'    =>  $this->page,
            'lang'    =>  $this->lang,
            'genre'       =>  $genre,
            'subgenre'    =>  $subgenre,
           //    'price'       =>  $price,
            'label'       =>  $labels,
            'type'        =>  $request->type,
            'total_track' =>  $total_track,
            'e_release'   =>  $e_release,
            'e_price'     =>  $e_price,
            'e_album'     =>  $e_album,
            'e_release_date'        =>  $e_release_date,
            'total_track_count'     =>  $total_track_count,
            'total_media_count'     =>  $total_media_count,
            'primary_artist'     =>  $primary_artist,
         //   'store'     =>  $store,
        ];        
        return routeView($this->page.'.edit', $data);
    }
    
    public function show(Request $request){
        //checkRolePermission($this->pageUrl.'-view');
        $id = decrypt($request->id);
        $id = decrypt($request->id);
        $datas = Release::where('id', $id)->first();
        $data_check = $datas;
        $i=0;
        $j=0;
        $k=0;
        if(!isset($datas->title)){ $i++; }else{ if(in_array($datas->title,[null,'0',''])){  $i++; }}
        if(!isset($datas->primary_artist)){ $i++;}else{ if(in_array($datas->primary_artist,[null,'0',''])){ $i++; }}
        if(!isset($datas->genre_id)){ $i++;}else{ if(in_array($datas->genre_id,[null,'0',''])){ $i++; }}
        if(!isset($datas->label_id)){ $i++;}else{ if(in_array($datas->label_id,[null,'0',''])){ $i++; }}
        if(!isset($datas->format)){ $i++;}else{ if(in_array($datas->format,[null,'0',''])){ $i++; }}
        if(!isset($datas->UPC)){ $i++;}else{ if(in_array($datas->UPC,[null,'0',''])){ $i++; }}
        if(!isset($datas->original_release_date)){ $i++;}else{ if(in_array($datas->original_release_date,[null,'0',''])){ $i++; }}
        if(!isset($datas->p_line)){ $i++;}else{ if(in_array($datas->p_line,[null,'0',''])){ $i++; }}
        if(!isset($datas->c_line)){ $i++;}else{ if(in_array($datas->c_line,[null,'0',''])){ $i++; }}
        if(!isset($datas->production_year)){ $i++;}else{ if(in_array($datas->production_year,[null,'0',''])){ $i++; }}
        if(!isset($datas->price_id)){ $j++;}else{ if(in_array($datas->price_id,[null,'0',''])){ $j++; }}
        if(!isset($datas->release_date)){ $k++;}else{ if(in_array($datas->release_date,[null,'0',''])){ $k++; }}       
        $total_track='';
        $total_track_count='';
        $total_media_count=0;
        $total_track = Track::where('release_id',$datas->id)->get()->count();
        $total_track_count = Track::where(['release_id'=>$datas->id,'track_id'=>0])->get()->count();
        $total_media_count =Media::where(['album_id'=>$datas->id])->get()->count();
        $e_release = $i;
        $e_price = $j;
        $e_release_date = $k;



        $genre = Genre::pluck('name','id')->toArray();        
        $subgenre = Subgenre::where('genre_id',$datas->genre_id)->pluck('title','id')->toArray();        
        $label = Label::pluck('title','id')->toArray();        
        $data = [
            'page_title'  =>  'Admin | Update  '.ucwords($this->page),
            'sdata'       =>  $datas,
            'page'    =>  $this->page,
            'lang'    =>  $this->lang,
            'genre'       =>  $genre,
            'subgenre'    =>  $subgenre,
            'label'       =>  $label,
            'type'        =>  $request->type,
            'total_track' =>  $total_track,
            'e_release'   =>  $e_release,
            'e_price'     =>  $e_price,
            'e_release_date'        =>  $e_release_date,
            'total_track_count'     =>  $total_track_count,
            'total_media_count'     =>  $total_media_count
        ];
        return routeView($this->page.'.view', $data);
    }
    
    public function formSubmitAjax(Request $request)
    {
        $type = $request->type;
        $value = $request->val; 
        if($type=='primary_artist' || $type=='featuring_artist')
        {
          $data = Release::where('id', $request->release_id)->update([$type=>implode('|',$value)]);   
        }
        else if($type=='genre_id' )
        {
            $data = Release::where('id', $request->release_id)->update([$type=>$value]);
            $data1 = Subgenre::where('genre_id',$value)->first();
            $data = Release::where('id', $request->release_id)->update(['subgenre_id'=>$data1->id]);
        }
        else{
            $data = Release::where('id', $request->release_id)->update([$type=>$value]);
        }
    }
    
    
        /*----------------------------------------------*/
    public function update(Request $request,$id,$type){
       // checkRolePermission($this->pageUrl.'-edit');
        $id = decrypt($id);
        $data = Release::where('id', $id)->first();  
        if($type =='release'){
            $result = $this->release($request,$id,$type,$data);
            if($result['status']=='error'){
                return back()->withErrors($result['message'])->withInput();
            }
            else
            {
                  $request->session()->flash('alert-success', $result['message']);
                  return redirectRoute($this->pageUrl.'.edit',['id'=> encrypt($id),'type'=>$type]);
            }
        }elseif($type =='upload_assets'){
            $this->uploadAssets($request,$id,$type,$data);            
        }elseif($type =='edit_assets'){
            $this->editAssets($request,$id,$type,$data);
        }elseif($type =='price_tire'){
            $result = $this->priceTire($request,$id,$type,$data);
            if($result['status']=='error'){
                return back()->withErrors($result['message'])->withInput();
            }
            else
            {
                  $request->session()->flash('alert-success', $result['message']);
                  return redirectRoute($this->pageUrl.'.edit',['id'=> encrypt($id),'type'=>$type]);
            }
        }elseif($type =='release_date'){
            $result = $this->releaseDate($request,$id,$type,$data);
            if($result['status']=='error'){
                return back()->withErrors($result['message'])->withInput();
            }
            else
            {
                  $request->session()->flash('alert-success', $result['message']);
                  return redirectRoute($this->pageUrl.'.edit',['id'=> encrypt($id),'type'=>$type]);
            }
        }elseif($type =='store'){
            if(Auth::user()->role_id==1){
                $input = $request->all();
                $i=0;
                foreach($input['store_id'] as $storeID){
                    $store_release = StoreRelease::where('id', $storeID)->first();
                    $store_release->store_url = $input['store_url'][$i];
                    $store_release->save();
                    $i++;
                }
            }
            return redirectRoute($this->pageUrl.'.edit',['id'=> encrypt($id),'type'=>$type]);
        }
        elseif($type =='submission'){
            $result =  $this->submission($request,$id,$type,$data);
            if($result['status']=='error'){
                return back()->withErrors($result['message'])->withInput();
            }
            else{
                $request->session()->flash('alert-success', $result['message']);
                return redirectRoute($this->pageUrl.'.submission',['id'=> encrypt($id),'type'=>$type]);
            }
        }
    }
    
    public function release($request,$id,$type,$data){
       // checkRolePermission($this->pageUrl.'-list');        
        $input = $request->all();        
        if(!empty($request->file('album_profile')))
        {
            $extension = $request->file('album_profile')->getClientOriginalExtension();
            if(in_array($extension, ['jpg','jpge']))
            {                        
                $fille_path = 'NewReleases/'.$data->upc_temp;
                $file_name = $data->upc_temp.'.'.$extension;
                
                dd($fille_path.'/'.$file_name);
                Storage::disk('s3')->putFileAs($fille_path, $request->file('album_profile'),$file_name);                        
                $store = Storage::disk('s3')->url($fille_path.'/'.$file_name);
                dd($store);
                $data->album_profile =  $fille_path.'/'.$file_name;
                $data->folder_path =  $file_name;
                $data->base_path =  $fille_path;                        
            }
        }
        $data->save();            
        return ['status'=>'sueesss','message'=>trans("admin_lang.record_update_success")];
             
    }
    

    public function uploadAssets($request,$id,$type,$data){
        $input = $request->all();
    }
    
    public function editAssets($request,$id,$type,$data){
        $input = $request->all();
    }
    
    public function priceTire($request,$id,$type,$data){
        $input = $request->all();
        $massage =[
            'price_id.required'=> 'The  main price tier field is required.'
        ];
        $validator = Validator::make($input, [
                'price_id'             => 'required',
        ],$massage);
        if (!$this->validate_admin($validator)) {
            $errors = $validator->errors();
            return ['status'=>'error','message'=>$errors];
        } else {
            $data->price_id = !empty($input['price_id'])?$input['price_id']:0;
                   
            $data->save();
           return ['status'=>'sueesss','message'=>trans("admin_lang.record_update_success")];
        }
    }
    
    public function releaseDate($request,$id,$type,$data){
       $input = $request->all();
        $massage =[
            'release_date.required'=> 'The  main price tier field is required.'
        ];
        $validator = Validator::make($input, [
                'release_date'             => 'required',
        ],$massage);
        if (!$this->validate_admin($validator)) {
            $errors = $validator->errors();
            return ['status'=>'error','message'=>$errors];
        } else {
            $data->release_date = !empty($input['release_date'])?$input['release_date']:0;
                        
            $data->save();
           return ['status'=>'sueesss','message'=>trans("admin_lang.record_update_success")];
        }
    }
    
    public function submission($request,$id,$type,$data){
        $input = $request->all();
        StoreRelease::where('release_id',$data->id)->whereNotIn('store_id',[4,14,20,21,22,23,24,25,26,28,29,31,32,33,34,35,36,37,38,39,43,44,45,46,47])->update(['status'=>'0']);
        if(!isset($data->UPC))
        {
            $upc = UPC::where(['value'=>$data->upc_temp])->orderBy('id','asc')->first();        
            $data->UPC = $upc->value; 
            if(Auth::user()->role_id!=1)
            {
                           
                $data->status = 1;            
                $data->save();
                $upc->user_id=Auth::user()->is_same_level?Auth::user()->parent_id: Auth::user()->id;
                $upc->total_count=1;
                $upc->save();
            }
        }
        elseif(empty($data->UPC)){
            $upc = UPC::where(['value'=>$data->upc_temp])->orderBy('id','asc')->first();        
            $data->UPC = $upc->value;
            if(Auth::user()->role_id!=1)
            {
                           
                $data->status = 1;
                $data->save();  
                $upc->total_count=1;
                $upc->user_id=Auth::user()->is_same_level?Auth::user()->parent_id: Auth::user()->id;
                $upc->save();
            }
        }
        else{
            if(Auth::user()->role_id!=1)
            {  
                $upc = UPC::where(['value'=>$data->upc_temp])->orderBy('id','asc')->first();        
                $data->UPC = $upc->value;                         
                $data->status = 1;
                $data->save();
            }
        }
        return ['status'=>'sueesss','message'=>'Your release has been successfully created.'];
    }
    /*-------------------------------------------------*/
}
