<?php
namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\User;
use App\Models\Country;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Gate;
class UsersController extends Controller
{
    protected $lang='Users';
    protected $page='users';
    
    public function __construct() {
    
            
    }
    
    public function index(Request $request)
    {
       Gate::authorize('Users-section');
       if($request->ajax())
       {
            $user=User::select('name','email','mobile','type','created_at','id','status')->where('role_id',  0);
            return Datatables::of($user)->editColumn('created_at', function ($user) {
                return $user->created_at->format('d M, Y'); 
            })->editColumn('mobile', function ($user) {
                return $user->mobile = $user->country_code.' '.$user->mobile;
            })->addIndexColumn()->make(true);
       }
       else
       {
            $roles=Role::all();
            $country=Country::select('phonecode','name','id')->get();
            $data = ['lang'=>$this->lang,'page'=>$this->page,'country'=>$country,'roles'=>$roles]; 
            return routeView($this->page.'.index',$data);
       }
    }
  
    public function create(Request $request)
    {
        Gate::authorize('Users-create');
        if($request->ajax())
        {
            if($request->method()=='POST')
            {
                $this->validate($request, [           
                    'first_name'=>'required|max:255',
                    'last_name'=>'required|max:255',
                    'email'=>'required|email|max:255|unique:users',
                    'mobile'=> 'required|numeric|digits_between:7,15|unique:users,mobile,,0,country_code,'.$request->country_code,
                    'country_code' 		=> 'required',
                    'password' => 'required|min:6|max:20',
                    'confirm_password' => 'required|same:password',
                        ]);
                        // create a new task based on user tasks relationship
                        $user = User::create([
                            'name' => $request->first_name . ' '.  $request->last_name,
                            'first_name' => $request->first_name,
                            'last_name' => $request->last_name,
                            'mobile' => $request->mobile,                    
                            'email' => $request->email,
                            'country_code' => $request->country_code,
                            'password' => Hash::make($request->password),
                            'address' => $request->address,
                            'latitude'=>$request->latitude,
                            'longitude'=>$request->longitude,
                            'type' => '0',
                            'status' => 1,
                        ]);

                if($user->id){
                    $result['message'] = 'Customer '.$user->name.' has been created';
                    $result['status'] = 1;
                }else{
                    $result['message'] = 'Customer Can`t created';
                    $result['status'] = 0;
                }
                return response()->json($result);   
            } 
        }
    }

    
    public function show(Request $request, $id)
    {
        Gate::authorize('Users-section');
        $data = User::findOrFail($id);
        $data = ['lang'=>$this->lang,'page'=>$this->page,'users'=>$data]; 
        return routeView($this->page.'.view',$data);
    }
    
    public function edit(Request $request, $id)
    {
        Gate::authorize('Users-edit');
        if($request->ajax())
        {
            if($request->method()=='POST')
            {
                $this->validate($request, [           
                    'first_name'=>'required|max:255',
                    'last_name'=>'required|max:255',
                    'country_code' 		=> 'required',
                    'mobile'=> 'required',
                    'confirm_password' => 'same:password',
                ]);

                $input=[
                    'name' => $request->first_name . ' '.  $request->last_name,
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'mobile' => $request->mobile,                    
                    'email' => $request->email,
                    'password' => Hash::make($request->password),
                    'address' => $request->editaddress,
                    'latitude'=>$request->editlatitude,
                    'longitude'=>$request->editlongitude,
                    'country_code' => $request->country_code,
                ];
                if($request->password){
                    $input['password']=Hash::make($request->password);
                }
                $staff = User::findOrFail($id);
                $staff->update($input);
                return response()->json($staff->find($staff->id));
            }
            else
            {                        
                $data = User::findOrFail($id);
                $country =Country::select('phonecode','name','id')->get();        
                $data = ['lang'=>$this->lang,'page'=>$this->page,'users'=>$data, 'country'=>$country]; 
                return routeView($this->page.'.edit',$data);
            }
        }
    }
    
    

    public function destroy($id)
    {
        Gate::authorize('Users-delete');
        return User::findOrFail($id)->delete();
    }

    public function status($id, $status)
    {
        $details = User::find($id); 
        if(!empty($details)){
            if($status == 'active'){
                $inp = ['status' => 1];
            }else{
                $inp = ['status' => 0];
            }
            $User = User::findOrFail($id);
            if($User->update($inp)){
                if($status == 'active'){
                    $result['message'] = 'Customer Account is activate successfully';
                    $result['status'] = 1;
                }else{
                    $result['message'] = 'Customer Account is deactivate successfully';
                    $result['status'] = 1; 
                }
            }else{
                $result['message'] = 'Customer Account status can`t be updated!!';
                $result['status'] = 0;
            }
        }else{
            $result['message'] = 'Invaild user!!';
            $result['status'] = 0;
        }
        return response()->json($result);
    }
}
