<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;

use App\User;


class HomeController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
    
         
	public function __construct() {
            
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function index() {
		$no_of_customers=0;
                $no_of_chef=0;
		$no_of_celebrity=0;
		$no_of_orders=0;
        return view('home',compact('no_of_customers','no_of_chef','no_of_celebrity','no_of_orders'));
	}

}
