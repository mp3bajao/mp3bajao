<?php
namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use MP3File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Gate;
use App\Models\Media;
use File,DB,Storage;
use App\Models\AddsProgram;
use App\Models\Program;
//use App\Models\Track;

class AddsProgramController extends Controller
{
    protected $lang='adds program';
    protected $page='adds_program';
    
    public function __construct() {
        $this->Models = new  AddsProgram;         
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    public function index(Request $request)
    {
       if($request->ajax())
       {
        //Gate::authorize(ucfirst($this->page).'-section');
        $data=$this->Models->select('name','created_at','id','status','duration');
        return Datatables::of($data)->editColumn('created_at', function ($data) {
            return $data->created_at->format('m/d/Y h:m:s'); 
        })->addIndexColumn()->make(true);
        }
       else
       {
            $data = ['lang'=>$this->lang,'page'=>$this->page]; 
            return routeView($this->page.'.index',$data);
       }
    }
    
    /*public function frontend()
    {
        Gate::authorize('Genres-section');
        return view('category.listing');
    }*/

    public function edit_frontend($id)
    {
       // Gate::authorize(ucfirst($this->page).'-edit');
        $data['category'] = $this->Models->findOrFail($id);
        return view($this->page.'.edit',$data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //Gate::authorize(ucfirst($this->page).'-create');
        if($request->ajax())
        {
            if($request->method()=='POST')
            {
                $mesasge = [
                    'name.required'=>'The Name field is required.',                    
                    'name.max'=>'The name may not be greater than 255 characters.',
                    'image.size'  => 'the file size is less than 2MB',
                ];
                

                $this->validate($request, [
                   'name'  => 'required|max:255|unique:genres',
                   'file' => 'required|mimes:mp3,mpeg',
                    'time'=> 'required'
                ],$mesasge);
                $input = $request->all(); 
                try{            
                    $data = new $this->Models;
                   
                    if ($request->file('file')) {
                        $value = $request->file('file');
                        
                        $extension = $value->getClientOriginalExtension();
                        $fileNameWithExtension = $value->getClientOriginalName();
                        $filename = pathinfo($fileNameWithExtension, PATHINFO_FILENAME);
                         $filename1 = "program_".time().'.'.$extension;
                        $fileNameToStore = "uploads/adds_program/".$filename1;
                        Storage::disk('s3')->put($fileNameToStore, fopen($value, "r+"));
                        $data->track = $filename1;                        
                    }
                           
                    
                    

                                
                    /*if ($request->file('image')) {
                        $file = $request->file('image');
                        $result = image_upload($file,'category');
                        if($result[0]==true){
                            $data->image = $result[1];
                        }
                    }*/
                    
                    /*$duration = explode(':',$input['time']);
                    $hours  = (($duration[0]*60)*60);
                    $min    = ($duration[1]*60);
                    $sec    = $duration[2];
                    
                    $duration = ($hours+$min+$sec);
                    */        
                            
                    $data->name = $input['name'];
                    $data->duration = round($input['time']);
                    //$data->time = round($input['time']);
                    $data->status= 1;
                    $data->save();
                    
                    $result['message'] = ucfirst($this->lang).' has been created';
                    $result['status'] = 1;
                    return response()->json($result);

                } catch (Exception $e){
                    $result['message'] = ucfirst($this->lang).' can`t created';
                    $result['status'] = 0;
                    return response()->json($result);            
                }
            }        
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
        Gate::authorize(ucfirst($this->page).'-section');
        $data = $this->Models->findOrFail($id);
        $data = ['lang'=>$this->lang,'page'=>$this->page,'category'=>$data]; 
        return routeView($this->page.'.edit',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //
      //  Gate::authorize(ucfirst($this->page).'-edit');
        if($request->ajax())
        {
            if($request->method()=='POST')
            {
                $mesasge = [
                    'name.required'=>'The Name field is required.',                    
                    'name.max'=>'The name may not be greater than 255 characters.',
                    'image.size'  => 'the file size is less than 2MB',
                ];
                $this->validate($request, [
                   'name'  => 'required|max:255|unique:genres',
                   'time'=> 'required',
                ],$mesasge);

                $input = $request->all();
                $cate_id = $id;
                $file = $request->file('file');
                try{

                    if(isset($file))
                    {
                        $data = $this->Models->where('id',$cate_id)->first();
                        $filename = 'uploads/adds_program/'.$data->getRawOriginal('track');
                        Storage::disk('s3')->delete($filename);

                        if ($request->file('file')) {
                            $value = $request->file('file');
                            $extension = $value->getClientOriginalExtension();
                            $fileNameWithExtension = $value->getClientOriginalName();
                            $filename = pathinfo($fileNameWithExtension, PATHINFO_FILENAME);
                            $filename1 = "program_".time().'.'.$extension;
                            $fileNameToStore = "uploads/adds_program/".$filename1;
                            Storage::disk('s3')->put($fileNameToStore, fopen($value, "r+"));

                        }    
                        
                    /*$duration = explode(':',$input['time']);
                    $hours  = (($duration[0]*60)*60);
                    $min    = ($duration[1]*60);
                    $sec    = $duration[2];
                    
                    $duration = ($hours+$min+$sec);*/
                       //$result = image_upload($file,'category');
                       $data = $this->Models->where('id',$cate_id)->update(['name'=>$input['name'],'track'=>$filename1,'duration'=>round($input['time'])]);
                    }
                    else
                    {
                        $data = $this->Models->where('id',$cate_id)->update(['name'=>$input['name']]);
                    }
    
                     
                    $result['message'] = ucfirst($this->lang).' updated successfully.';
                    $result['status'] = 1;
                    return response()->json($result);
                }
                catch (Exception $e)
                {
                    $result['message'] = ucfirst($this->lang).' Can`t be updated.';
                    $result['status'] = 0;
                    return response()->json($result);           
                }
            }
            else{
                $data = $this->Models->findOrFail($id);
                $data = ['lang'=>$this->lang,'page'=>$this->page,'category'=>$data]; 
                return routeView($this->page.'.edit',$data);
            }
        }      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       // Gate::authorize(ucfirst($this->page).'-delete');
        $data = $this->Models::findOrFail($id);
        if(isset($data)){
            $filename = 'uploads/adds_program/'.$data->getRawOriginal('track');
            Storage::disk('s3')->delete($filename);
            
            Program::where('table_type','Adds')->where('track_id',$data->id)->delete();
            $data->delete();
            $result['message'] = ucfirst($this->lang).' deleted successfully';
            $result['status'] = 1;
        }else{
            $result['message'] = ucfirst($this->lang).' Can`t deleted';
            $result['status'] = 0;
        }
        return response()->json($result);
    }
    public function status($id, $status)
    {
        $details = $this->Models::find($id); 
        if(!empty($details)){
            if($status == 'active'){
                $inp = ['status' => 1];
            }else{
                $inp = ['status' => 0];
            }
            $Category = $this->Models::findOrFail($id);
            if($Category->update($inp)){
                if($status == 'active'){
                    $result['message'] = ucfirst($this->lang).' is activate successfully';
                    $result['status'] = 1;
                }else{
                    $result['message'] = ucfirst($this->lang).' is deactivate successfully';
                    $result['status'] = 1; 
                }
            }else{
                $result['message'] = ucfirst($this->lang).' status can`t be updated!!';
                $result['status'] = 0;
            }
        }else{
            $result['message'] = 'Invaild '.ucfirst($this->lang).'!!';
            $result['status'] = 0;
        }
        return response()->json($result);
    }
}
