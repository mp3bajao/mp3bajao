<?php
namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Traits\HasRoles;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\User;
use App;

class PermissionController extends Controller
{
    protected $lang='permissions';
    protected $page='permissions';
    public function __construct()
    {
        
    }

    public function index(){
        Gate::authorize('Permission-section');
        $permissions=Permission::all()->toArray();
        $roles=$this->getRoles();
        $locale = App::getLocale();
        $data = ['lang'=>$this->lang,'page'=>$this->page,'permissions'=>$permissions,'roles'=>$roles]; 
        return routeView($this->page.'.permissions',$data);
    }

    public function getRoles()
    {
        Gate::authorize('Permission-section');
        $role=Role::all();
        foreach ($role as $key=>$rol) {
            $permissions = $rol->permissions->pluck('id')->toArray();
            $role[$key]['permission_ids']=$permissions;
            // dd($permissions);
        }
        return $role->toArray();
    }

    
     public function user_permissions($userid=''){
      Gate::authorize('Permission-section');  
      Gate::authorize('Permission-user');
       $user = User::findOrFail($userid);
       $permissions=Permission::all()->toArray();
       $user_permission=$user->getAllPermissions()->pluck('id')->toArray();       
        if($userid == '')
        {
            return redirect('permissions');
        }
        if(!User::find($userid))
        {
            return redirect('permissions');
        }
       $data = ['lang'=>$this->lang,'page'=>$this->page,'permissions'=>$permissions,'user_permission'=>$user_permission,'user'=>$user]; 
       return routeView($this->page.'.permissions',$data);
    }  
   
   /**
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function perm_userData()
    {
        $user=User::select('email','name','id')->where('id', '!=', 1)->where('role_id',2);
        return Datatables::of($user)
         ->addColumn('action', function ($user) {
                return '<a href="'.routeUser('user_permissions',[$user->id]).'"  title="Add Permissions" class="btn btn-xs btn-primary add_permissions"><i class="fa fa-edit"></i>Permissions</a>';
            }) ->make(true);
    }


    public function saveRolePermission($role_id,$permission_id){
        Gate::authorize('Permission-role');
        $role = Role::findOrFail($role_id);
        if($role->givePermissionTo($permission_id)){
         $result=array(
             'status'=>true,
             'message'=>__('Access given to ').$role->name
         );
        }
        else{
            $result=array(
                'status'=>false,
                'message'=>__('Something went wrong.')
            );
        }
        return response()->json($result);
    }

    public function deleteRolePermission($role_id,$permission_id){
        Gate::authorize('Permission-role');
        $role = Role::findOrFail($role_id);
        if($role->revokePermissionTo($permission_id)){
         $result=array(
             'status'=>true,
             'message'=>__('Access revoked to ').$role->name
         );
        }
        else{
            $result=array(
                'status'=>false,
                'message'=>__('Something went wrong.')
            );
        }
        return response()->json($result);
    }

    public function saveUserPermission($user_id,$permission_id){
        Gate::authorize('Permission-user');
        $user = User::findOrFail($user_id);
        if($user->givePermissionTo($permission_id)){
         $result=array(
             'status'=>true,
             'message'=>__('Access given to ').$user->name
         );
        }
        else{
            $result=array(
                'status'=>false,
                'message'=>__('Something went wrong.')
            );
        }
        return response()->json($result);
    }

    public function deleteUserPermission($user_id,$permission_id){
        Gate::authorize('Permission-user');
        $user = User::findOrFail($user_id);
        if($user->revokePermissionTo($permission_id)){
         $result=array(
             'status'=>true,
             'message'=>__('Access revoked to ').$user->name
         );
        }
        else{
            $result=array(
                'status'=>false,
                'message'=>__('Something went wrong.')
            );
        }
        return response()->json($result);
    }
}
