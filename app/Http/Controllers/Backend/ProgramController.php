<?php
namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Gate;
use App\Models\Media;
use File,DB,Auth,Redirect,Storage;
use App\Models\ProgramCategory;
use App\Models\Program;
use App\Models\Station;
use App\Models\GenreLang;
use App\Models\Language;
use App\Models\Track;
use App\Models\AddsProgram;

class ProgramController extends Controller
{
    protected $lang='program';
    protected $page='program';
    
    public function __construct()
    {
        $this->Models = new  ProgramCategory;
        $this->Models_two = new  Program;
        $this->Models_three = new Track();
        $this->Models_four = new Station();
        $this->Models_fiv = new AddsProgram();
        
        
        $this->sortable_columns = [
            0 => 'id',
            1 => 'title',
            2 => 'posted_by',
            3 => 'track_type',
            4 => 'created_at',
        ];
    }

    public function index(Request $request)
    {
       if($request->ajax())
       {
            $data=$this->Models->select('program_categories.name','program_categories.image','program_categories.station_id','program_categories.created_at','program_categories.status','program_categories.duration','program_categories.id','stations.name as users_name','stations.id as users_id')
                    ->leftJoin('stations', 'stations.id', '=', 'program_categories.station_id');
            return Datatables::of($data)->editColumn('created_at', function ($data) {
                return $data->created_at->format('m/d/Y h:m:s'); 
            })->addIndexColumn()->make(true);
        }
        else
        {
            $station = $this->Models_four::where('status',1)->get();
            
            $data = ['lang'=>$this->lang,'page'=>$this->page,'station'=>$station]; 
            return routeView($this->page.'.index',$data);
        }
    }
    
    public function editIndex(Request $request)
    {
        if ($request->ajax()) {
            
            $limit = $request->input('length');
            $start = $request->input('start');
            $search = $request['search']['value'];
            $orderby = $request['order']['0']['column'];
            $order = $orderby != "" ? $request['order']['0']['dir'] : "";
            $draw = $request['draw'];

            $totaldata = $this->Models_two::with('getTrack','getAdds')->where('program_type',$request->input('playlist_type_id'))->count();
            $response = $this->Models_two::with('getTrack','getAdds')->where('program_type',$request->input('playlist_type_id'))
                    ->offset($start)
                    ->orderBy('position','asc')
                    ->limit($limit)
                    ->get();;
            
/*            $totaldata = $this->Models_three->getModel($limit, $start, $search, $this->sortable_columns[$orderby], $order,$release_id=null)->whereIn('id',$track_id)->count();
            $response = $this->Models_three->getModel($limit, $start, $search, $this->sortable_columns[$orderby], $order,$release_id=null)->whereIn('id',$track_id)
                       ->offset($start)
                       ->limit($limit)
                       ->get();*/
            
            if (!$response) {
                $data = [];
                $paging = [];
            } 
            else 
            {
                $data = $response;
                $paging = $response;
            }

            $datas = array();
            $i = 1;
            $break = 60;
            foreach ($data as $value) {
                $u['DT_RowId'] = '<i class="fa fa-music" aria-hidden="true"></i>';
                $u['table_type'] = $value->table_type;
                
                if($value->table_type=='Program')
                {
                    
               
                    $u['album_name'] = '<b>'.ucfirst($value->getTrack->getRelease->title).'</b><br><small>'.$value->getTrack->getRelease->UPC.'</small>';
                    $u['title'] = !empty($value->getTrack->title)?$value->title.'. <b>':$i.'. <b>';
                    if (strlen($value->getTrack->title) > 60) {
                        $pos = strpos($value->getTrack->title, ' ', 60);
                        if ($pos !== false) {
                            $u['title'] .= ucfirst(substr(implode(PHP_EOL, str_split(ucfirst($value->getTrack->title), $break)), 0, $pos)) . '...';
                        } else {
                            $u['title'] .= ucfirst(substr(implode(PHP_EOL, str_split(ucfirst($value->getTrack->title), $break)), 0, 60)) . '...';
                        }
                    } else {
                        $u['title'] .= ucfirst(implode(PHP_EOL, str_split(ucfirst($value->getTrack->title), $break)));
                    }    
                    $u['title'] .='</b><br/><small>';
                    $u['title'] .=isset($value->getTrack->ISRC)?$value->getTrack->ISRC:'empty';
                    $u['title'] .='</small>'; 
                    $primary_artist= str_replace('|',', ',$value->getTrack->primary_artist); 
                    $u['track_type'] = isset($primary_artist)?ucwords($primary_artist):'empty';
                    $u['checked'] = '<input type="checkbox" class="add_top_list" data-table_type="Program" value="'.$value->getTrack->id.'" checked="">';
                }
                else
                {
                   $u['album_name']  = '';
                   $u['title'] =  '<b>'.ucwords($value->getAdds->name).'</b>';
                   $u['track_type'] ='';
                   $u['checked'] = '<input type="checkbox" class="add_top_list" data-table_type="Adds" value="'.$value->getAdds->id.'" checked="">';
                }
                $u['position'] = '<input type="number" class="position form-control" name="position" value="'.$value->position.'" data-id="'.$value->id.'">';
                //$u['status'] = isset($value->getMedia)? 'Uploaded':'Empty';                
                $u['created_at'] = $value->created_at->format('d M,Y');
                $u['actions']='<div class="btn-group" role="group" aria-label="User Actions">';                              
                $u['actions'] = $value->duration;
                $u['actions'] .= '</div>';
                $datas[] = $u;
                $i++;
                unset($u);
            }
            $return = [
                "draw" => intval($draw),
                "recordsFiltered" => intval($totaldata),
                "recordsTotal" => intval($totaldata),
                "data" => $datas
            ];
            return $return;
        }  
    }

    public function edit_frontend($id)
    {
        Gate::authorize(ucfirst($this->page).'-edit');
        $data['category'] = $this->Models->findOrFail($id);
        return view('category.edit',$data);
    }

    public function create(Request $request)
    {
       // Gate::authorize(ucfirst($this->page).'-create');
        if($request->ajax())
        {
            if($request->method()=='POST')
            {
                $mesasge = [
                    'name.required'=>'The Name field is required.',                    
                    'name.max'=>'The name may not be greater than 255 characters.',
                    'image.size'  => 'the file size is less than 2MB',
                ];
                $this->validate($request, [
                   'name'  => 'required|max:255|unique:genres',
                   'image' => 'image|mimes:jpeg,png,jpg,gif ,svg|max:2048',
                ],$mesasge);
                $input = $request->all(); 
                try{            
                   $data = new $this->Models;
                    if ($request->file('image')) {
                        $file = $request->file('image');
                        $result = image_upload($file,'category',null,0.4);
                        if($result[0]==true){
                            $data->image = $result[1];
                            $data->image_webp = $result[4];
                            $data->is_converted=1;
                            $data->save();
                        }
                    }
                    $data->station_id = $input['station_id'];
                    $data->name = $input['name'];
                    $data->status= 1;
                    $data->save();


                    $result['message'] = ucfirst($this->lang).' has been created';
                    $result['status'] = 1;
                    return response()->json($result);

                } catch (Exception $e){
                    $result['message'] = ucfirst($this->lang).' can`t created';
                    $result['status'] = 0;
                    return response()->json($result);            
                }
            }        
        }
    }

    public function show(Request $request, $id)
    {
        //
        Gate::authorize(ucfirst($this->page).'-section');
        $data = $this->Models->findOrFail($id);
        $data = ['lang'=>$this->lang,'page'=>$this->page,'category'=>$data]; 
        return routeView($this->page.'.edit',$data);
    }

    public function edit(Request $request, $id)
    {
        //
       // Gate::authorize(ucfirst($this->page).'-edit');
        /*if($request->ajax())
        {*/
       // dd($request->method());
            if($request->method()=='POST')
            {

                $mesasge = [
                    'name.required'=>'The Name field is required.',                    
                    'name.max'=>'The name may not be greater than 255 characters.',
                    'image.size'  => 'the file size is less than 2MB',
                ];
                $this->validate($request, [
                   'name'  => 'required|max:255',
                   'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg,png',
                ],$mesasge);

                $input = $request->all();
                $cate_id = $id;
                
                $file = $request->file('image');
                
                try{    
                    $data = $this->Models->where('id',$cate_id)->first();
                     
                 if ($request->file('image')) {
                        $image = $data->getRawOriginal()['image'];
                        file_checker_and_delete('/public/uploads/category/'.$image);
                        $file = $request->file('image');
                        $result = image_upload($file,'category',null,0.4);
                        if($result[0]==true){
                            $data->image = $result[1];
                            $data->image_webp = $result[4];
                            $data->is_converted=1;
                            $data->save();
                        }                      
                    }
                    $data->station_id = $input['station_id'];
                    $data->name = $input['name'];
                    $data->save();
                    
                    $result['message'] = ucfirst($this->lang).' updated successfully.';
                    $result['status'] = 1;
//                    return response()->json($result);
                    return Redirect::back()->withErrors(['msg', 'updated successfully.']);
                }
                catch (Exception $e)
                {
                    $result['message'] = ucfirst($this->lang).' Can`t be updated.';
                    $result['status'] = 0;
                    return Redirect::back()->withErrors(['msg', ucfirst($this->lang).' Can`t be updated.']);
//                    return response()->json($result);           
                }
            }
            else{
                $station = $this->Models_four::where('status',1)->get();
                $adds = $this->Models_fiv->where('status',1)->get();
                $data = $this->Models->findOrFail($id);
                $genre = $this->Models_two::where('status',1)->get();
                $data = ['lang'=>$this->lang,'page'=>$this->page,'genre'=>$genre,'data'=>$data,'station'=>$station,'adds'=>$adds]; 
                return routeView($this->page.'.edit',$data);
            }
        //}      
    }

    public function update(Request $request, $id)
    {
        Gate::authorize(ucfirst($this->page).'-edit');
        // validate
	 $mesasge = [
            'name.en.required'=>'The Name field is required.',
            'description.en.required'=>'The description field is required.',
            'name.en.max'=>'The name may not be greater than 255 characters.',
            'image.size'  => 'the file size is less than 2MB',
          ];
          $this->validate($request, [
               'name.en'  => 'required|max:255',
               'description.en'=>'required',
               'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
          ],$mesasge);
            
        $input = $request->all();
        $cate_id = $id;
        
        $file = $request->file('image');
        try{
            $lang = Language::pluck('lang')->toArray();
            foreach($lang as $lang)
            {
                if($lang=='en')
                {
                    if(isset($file))
                    {
                        $result = image_upload($file,'category');
                        $data = Category::where('id',$cate_id)->update(['name'=>$input['name'][$lang],'description'=>$input['description'][$lang],'image'=>$result[1]]);
                    }
                    else
                    {
                        $data = Category::where('id',$cate_id)->update(['name'=>$input['name'][$lang],'description'=>$input['description'][$lang]]);
                    }                    
                }
                $dataLang = CategoryLang::where(['category_id'=>$cate_id,'lang'=>$lang])->first();
                if(isset($dataLang))
                {
                   $dataLang = CategoryLang::where(['category_id'=>$cate_id,'lang'=>$lang])->update(['name'=>$input['name'][$lang],'description'=>$input['description'][$lang]]);                                   
                }
                else
                {
                    $dataLang = new  CategoryLang;
                    $dataLang->category_id = $cate_id;
                    $dataLang->name = $input['name'][$lang];
                    $dataLang->description = $input['description'][$lang];
                    $dataLang->lang = $lang;
                    $dataLang->save();
                }
            }
            $result['message'] = 'Food Category updated successfully.';
            $result['status'] = 1;
            return response()->json($result);
        }
        catch (Exception $e)
        {
            $result['message'] = 'Food Category Can`t be updated.';
            $result['status'] = 0;
            return response()->json($result);           
        }
    }

    public function destroy($id)
    {
        //Gate::authorize(ucfirst($this->page).'-delete');
         $data = $this->Models::findOrFail($id);
        if(isset($data)){
            $filename = 'uploads/program/'.$data->getRawOriginal('track');
           // Storage::disk('s3')->delete($filename);            
            Program::where('program_type',$data->id)->delete();
            $data->delete();
            
            $result['message'] = 'Playlist deleted successfully';
            $result['status'] = 1;
        }else{
            $result['message'] = 'Playlist deleted  deleted';
            $result['status'] = 0;
        }
        return response()->json($result);
    }
    
    public function status($id, $status)
    {
        $details = $this->Models::find($id); 
        if(!empty($details)){
            if($status == 'active'){
                $inp = ['status' => 1];
            }else{
                $inp = ['status' => 0];
            }
            $Category = $this->Models::findOrFail($id);
            if($Category->update($inp)){
                if($status == 'active'){
                    $result['message'] = ucfirst($this->lang).' is activate successfully';
                    $result['status'] = 1;
                }else{
                    $result['message'] = ucfirst($this->lang).' is deactivate successfully';
                    $result['status'] = 1; 
                }
            }else{
                $result['message'] = ucfirst($this->lang).' status can`t be updated!!';
                $result['status'] = 0;
            }
        }else{
            $result['message'] = 'Invaild '.ucfirst($this->lang).'!!';
            $result['status'] = 0;
        }
        return response()->json($result);
    }
    
     public function position($id, $position)
    {
        $details = $this->Models_two::find($id);         
        $details->position = $position;
        $details->save();
        $result['message'] = ucfirst($this->lang).' is activate successfully';
        $result['status'] = 1;
        return response()->json($result);
    }
    
    
    
    
    public function playlistsearch(Request $request)
    {
        $input = $request->all();
        $search = $input['serching'];
        $q = Track::select('tracks.*','releases.UPC')
            ->leftJoin('releases', 'releases.id', '=', 'tracks.release_id')
            ->where(function($query)use($search){
        $query->whereRaw('LOWER(tracks.title) ' . 'LIKE ' . '"%' . strtolower($search) . '%"')
            ->orwhereRaw('LOWER(tracks.primary_artist) ' . 'LIKE ' . '"%' . strtolower($search) . '%"')
            ->orwhereRaw('LOWER(releases.UPC) ' . 'LIKE ' . '"%' . strtolower($search) . '%"');
        });        
        $data = $q->limit(100)->get();
        $data = ['data'=>$data];
        return routeView($this->page.'.search',$data);
    }
    
    public function addPlaylistList(Request $request){
        $input = $request->all();
        //  dd($input); 
        if($input['type']=='true')
        {
             $playlist = $this->Models::find($input['playlist_type_id']);
            $data = $this->Models_two::where('station_id',$playlist->station_id)
                  ->where('program_type',$input['playlist_type_id'])
                  ->where('track_id',$input['values'])
                   ->where('table_type',$input['table_type'])                   
                  ->first();
          
            if(!isset($data))
            {
                $playlist = $this->Models::find($input['playlist_type_id']);
                $media =  $this->Models_three::with('getMedia')->where('id',$input['values'])->first();
                
                if(isset($media->getMedia->file_duration))
                {
                    $duration  = explode(':',$media->getMedia->file_duration);
                    $hours = (($duration[0]*60)*60);
                    $mintue = ($duration[1]*60);
                    $secound = $duration[2];
                    $time = $hours+$mintue+$secound;                 
                }
                else
                {
                  $time = 0;  
                }
                $totalData = $this->Models_two::where('station_id',$playlist->station_id)
                  ->where('program_type',$input['playlist_type_id'])->count();
                
                $data = new $this->Models_two;
                $data->station_id = $playlist->station_id;
                $data->table_type = $input['table_type'];
                $data->program_type = $input['playlist_type_id'];
                $data->track_id = $input['values'];
                $data->duration = $time;
                $data->position = ($totalData+1);
                $data->save();                  
            }
            elseif($input['table_type']=='Adds')
            {
                $playlist = $this->Models::find($input['playlist_type_id']);
                $media =  $this->Models_three::with('getMedia')->where('id',$input['values'])->first();
                
                if(isset($media->getMedia->file_duration))
                {
                    $duration  = explode(':',$media->getMedia->file_duration);
                    $hours = (($duration[0]*60)*60);
                    $mintue = ($duration[1]*60);
                    $secound = $duration[2];
                    $time = $hours+$mintue+$secound;                 
                }
                else
                {
                  $time = 0;  
                }
                $totalData = $this->Models_two::where('station_id',$playlist->station_id)
                  ->where('program_type',$input['playlist_type_id'])->count();
                
                $data = new $this->Models_two;
                $data->station_id = $playlist->station_id;
                $data->table_type = $input['table_type'];
                $data->program_type = $input['playlist_type_id'];
                $data->track_id = $input['values'];
                $data->duration = $time;
                $data->position = ($totalData+1);
                $data->save();    
            }
        }
        else
        {
            $playlist = $this->Models::find($input['playlist_type_id']);
            $data = $this->Models_two::where('station_id',$playlist->station_id)
                  ->where('program_type',$input['playlist_type_id'])
                  ->where('track_id',$input['values'])
                  ->delete();  
        }
        $total_time = $this->Models_two->where('program_type', $input['playlist_type_id'])->where('station_id',$playlist->station_id)->sum('duration');
        $prog = $this->Models->where('id',$input['playlist_type_id'])->first();
        $prog->duration =$total_time;
        $prog->save();
    }
    
   
}