<?php
namespace App\Http\Controllers\Backend;
use App\User;
use App\Models\UserDetails;
use App\Models\Permission;
use App\Models\PermissionRole;
use App\Models\UserType;
use Auth;
use Valid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Storage;
use App\Models\MediaVideo;
use App\Models\Genre;
use App\Models\Language;
use App\Models\Subgenre;
use App\Models\ReleaseVideo;
use App\Models\Release;
use App\Models\Price;
use App\Models\Label;
use App\Models\Track;
use App\Models\UPC;
use App\Models\StoresVideo;
use App\Models\StoreReleaseVideo;
use App\Lib\PushNotification;
use App\Models\ISPCVIDEO;
use Mail;

class ReleaseVideoController extends Controller {
    
    public $pageUrl="video";
    public $langPath="release_video";
    public function __construct() {
        $this->Models = new ReleaseVideo();
        $this->Models_fiv = new Track();
        $this->Models_elv = new ISPCVIDEO();
        $this->sortable_columns = [
            0 => 'id',
            1 => 'id',
            2 => 'id',
            3 => 'title',
            4 => 'posted_by',
            5 => 'track_type',
            6 => 'created_at',
        ];
        $this->sortable_columns_track = [
            0 => 'id',
            1 => 'id',
            2 => 'primary_artist',
            3 => 'ISRC',
            4 => 'price_id',
        ];
        $this->non_nicol_stores = explode(",",env('NON_NICOL_STORES'));
        $this->ftp_stores = explode(",",env('FTP_STORES'));
    }
    public function index(Request $request) {
        //checkRolePermission($this->langPath.'-list');
        if ($request->ajax()) {
            
            $limit = $request->input('length');
            $start = $request->input('start');
            $search = $request['search']['value'];
            $orderby = $request['order']['0']['column'];
            $order = $orderby != "" ? $request['order']['0']['dir'] : "";
            $draw = $request['draw'];
            $totaldata = $this->Models->getModel($limit, $start, $search, $this->sortable_columns[$orderby], $order,$request->type,$request->current_status,$request->user_id)->count();
            $response = $this->Models->getModel($limit, $start, $search, $this->sortable_columns[$orderby], $order,$request->type,$request->current_status,$request->user_id)
                       ->offset($start)
                    ->limit($limit)
                    ->get();
            
            if (!$response) {
                $data = [];
                $paging = [];
            } else {
                $data = $response;
                $paging = $response;
            }

            $datas = array();
            $i = 1;
            $break = 60;
            foreach ($data as $value) {
                $u['release_id'] = '<br/><b style="padding-top: 14px;">'.$value->id.'</b>';
                $u['DT_RowId'] ='<img src="'.asset('public/video.jpg').'" data-toggle="tooltip" data-placement="top" title="This release is a Music" style="width:30px;margin: 7px;"/>';
                $u['DT_RowId1'] ='';
                if(in_array($value->status,[0,2]))
                {
                    $u['DT_RowId1'] .='<img src="'.asset('public/process.png').'" data-toggle="tooltip" data-placement="top"   title="This release needs to be finished" style="width:30px;margin: 7px;"/>';
                }
                else{
                    if(Auth::user()->role_id==1)
                    {
                        if($value->status==1)
                        {
                            $u['DT_RowId1'] .='<i class="fa fa-cog" style="font-size: 30px;padding: 7px;" aria-hidden="true"></i>';
                        }
                        else{
                            $u['DT_RowId1'] .='<img src="'.asset('public/right.png').'" data-toggle="tooltip" data-placement="top"  class="img_zoom" data-url="'.asset('public/record.png').'" title="This release needs to be finished" style="width:30px;margin: 7px;"/>';                                            
                        }                       
                    }
                    else{
                        $u['DT_RowId1'] .='<img src="'.asset('public/right.png').'" data-toggle="tooltip" data-placement="top"  class="img_zoom" data-url="'.asset('public/record.png').'" title="This release needs to be finished" style="width:30px;margin: 7px;"/>';                   
                    }
                }
                
                $u['title'] = '<b style="color:#409cf6">';
                if (strlen($value->title) > 60) {
                    $pos = strpos($value->title, ' ', 60);
                    if ($pos !== false) {
                        $u['title'] .= ucfirst(substr(implode(PHP_EOL, str_split(ucfirst($value->title), $break)), 0, $pos)) . '...';
                    } else {
                        $u['title'] .= ucfirst(substr(implode(PHP_EOL, str_split(ucfirst($value->title), $break)), 0, 60)) . '...';
                    }
                } else {
                    $u['title'] .= ucfirst(implode(PHP_EOL, str_split(ucfirst($value->title), $break)));
                }
                $u['title'] .= '</b><br/><small>';
                /*$primary_artist1='';
                $primary_artist = TrackVideo::where('release_id',$value->id)->pluck('primary_artist')->toArray();
                if(!empty($primary_artist))
                {
                $primary_artist =array_unique(explode('|',str_replace(',','|',implode('|',$primary_artist))));//ask
                if(count($primary_artist)>3){ $primary_artist1 = 'Various Artists '; }else{ $primary_artist1 = implode(',',$primary_artist); }                
                }
                */
                
                $u['title'] .= ucwords(str_replace('|',',',$value->primary_artist));
                $u['title'] .='</small>';
                $u['album'] ='<img src="'.$value->album_profile.'" width="40px" class="img_zoom" data-url="'.$value->album_profile.'"/>';                
                $u['track_type'] ='Digital' ;
                $r_date = $value->release_date?date('M d, Y',strtotime($value->release_date)):'';
                $u['label'] = !empty($value->label_value)?$value->label_value.'<br>'.$r_date:'<small>no info</small>';
                $u['producer_catalogue_number'] ='Cat# :';                
                $u['producer_catalogue_number'] .= isset($value->producer_catalogue_number)?$value->producer_catalogue_number:'empty';                
                $u['producer_catalogue_number'] .= '<br>';                
                $u['producer_catalogue_number'] .=  isset($value->UPC)?$value->UPC:'';
                $u['producer_catalogue_number'] .= '<br>Release Id:';                  
                $u['producer_catalogue_number'] .= $value->id;                  
                $u['terrs'] =  '<span class="label label-default"><i class="fa fa-globe" aria-hidden="true"></i> 220 terrs</span><br><span class="label label-default"><i class="fa fa-th" aria-hidden="true"></i> '.$value->getAllStoreRelease->count().' stores</span><br>';
                $u['terrs'] .=  '<span class="label label-default';
                if($value->getStoreReleaseSend->count()>0){
                $u['terrs'] .= ' checkrelease"  style="cursor: all-scroll;"  aria-hidden="true" data-release_data="'.$value->id.'" data-store_type="1"';
                }else{
                    $u['terrs'] .= '"';
                }
                $u['terrs'] .= '><i class="fa fa-check " aria-hidden="true"  ></i> '.$value->getStoreReleaseSend->count().'/'.$value->getStoreRelease->count().' stored</span>';                
                $u['actions']='<div class="btn-group" role="group" aria-label="User Actions">';              
                if(in_array(Auth::user()->role_id,[2,3,4])){
                    if(in_array($value->status,[0,2]))
                    {
                       // if(checkRolePermission($this->langPath.'-edit',1)){
                        $u['actions'] .= createEditAction($this->pageUrl.'.edit',['type'=>'release','id'=>encrypt($value->id)]);
                       // } if(checkRolePermission($this->langPath.'-view',1)){
                        $u['actions'] .= createViewAction($this->pageUrl.'.show',['type'=>$request->type,'id'=>encrypt($value->id)]);
                       // } if(checkRolePermission($this->langPath.'-edit',1)){
                        $u['actions'] .= createDeleteAction($this->pageUrl.'.delete', ['id'=>encrypt($value->id)],$this->pageUrl.'.index');               
                       // }                    
                    }
                    else{
                        $u['actions'] .= createViewAction($this->pageUrl.'.show',['type'=>$request->type,'id'=>encrypt($value->id)]);
                    }
                }else{
                    if(in_array($value->status,[3,4]))
                    {
                        $u['actions'] .= createEditAction($this->pageUrl.'.edit',['type'=>'release','id'=>encrypt($value->id)]);                        
                        $u['actions'] .= createViewAction($this->pageUrl.'.show',['type'=>$request->type,'id'=>encrypt($value->id)]);
                        $u['actions'] .= '<a href="javascript:;" data-toggle="tooltip" data-placement="top" data-release_data="'.$value->id.'" title="' . __('admin_lang.upload_on_server') . '" class="btn btn-xs btn-success releaseupload"><i class="fa fa-cloud-upload"></i></a>';
                        //$u['actions'] .= createDeleteAction($this->pageUrl.'.delete', ['id'=>encrypt($value->id)],$this->pageUrl.'.index');               
                    }
                    else
                    {
                        $u['actions'] .= createViewAction($this->pageUrl.'.show',['type'=>$request->type,'id'=>encrypt($value->id)]);                    
                        $u['actions'] .= createEditAction($this->pageUrl.'.edit',['type'=>'release','id'=>encrypt($value->id)]);
                        $u['actions'] .= createDefaultAction($this->pageUrl . '.denine', ['id' => $value->id,'type'=>'all'],'default','times-circle-o','denine');
                       // $u['actions'] .= '<a href="javascript:;" data-toggle="tooltip" data-placement="top" data-release_data="'.$value->id.'" title="approve" class="btn btn-xs btn-success releaseupload"><i class="fa fa-check-square-o"></i></a>';
                        $u['actions'] .= createDefaultAction($this->pageUrl . '.approve', ['id' => $value->id,'type'=>'all'],'success','check-square-o','approve');                    
                    }  
                }
                //$u['actions'] .= createDeleteAction($this->pageUrl.'.delete', ['id'=>encrypt($value->id)],$this->pageUrl.'.index');               
                $u['actions'] .= '</div>';
                $datas[] = $u;
                $i++;
                unset($u);
            }
            $return = [
                "draw" => intval($draw),
                "recordsFiltered" => intval($totaldata),
                "recordsTotal" => intval($totaldata),
                "data" => $datas
            ];
            return $return;
        }
       /* if(Auth::user()->role_id==1){
            $user = User::where('role_id',2)->orderBy('username','asc')->pluck('username','id')->toArray();
            $data = [
                'pagePath'=>$this->pageUrl,
                'type'=>$request->type,
                'langPath' => $this->langPath,
                'page_title'=>'Admin | Manage '. ucwords($this->pageUrl),
                'user'=>$user
                ];
        }else{*/
            $data = [
                'pagePath'=>$this->pageUrl,
                'type'=>$request->type,
                'langPath' => $this->langPath,
                'page_title'=>'Admin | Manage '. ucwords($this->pageUrl),
                ];
       // }
        return routeView($this->pageUrl.'.index',$data);
    }
    
    public function create(Request $request) {
       // checkRolePermission($this->langPath.'-add');
        $data = [
            "page_title" => "Admin | Manage ".ucwords($this->pageUrl),
            "langPath" => $this->langPath,
            'pagePath'=>$this->pageUrl,
            ];
        return view('admin.'.$this->pageUrl.'.create', $data);
    }
    
    public function upcCodeChecker(){
        for($i=0; $i<=100;$i++){
            $upc = UPC::where(['key_value'=>'UPC','total_count'=>0])->orderBy('id','asc')->first();
            $upcCheckInRelease = ReleaseVideo::Where('upc_temp',$upc->value)->first();
            if(!isset($upcCheckInRelease)){
                return $upc;
                break;
            }else{
                $upc->total_count=1;
                $upc->save();
            }
        }
    }
    
    public function store(Request $request) {
        //checkRolePermission($this->langPath.'-add');
        $input = $request->all();
        $validator = Validator::make($input, [
                    'title' => 'required|unique:releases_videos,title,NULL,id,sub_user_id,' . Auth::user()->id,
        ]);

        if (!$this->validate_admin($validator)) {
            $errors = $validator->errors();
            return back()->withErrors($errors)->withInput();
        } else {
            try {
                $upc = $this->upcCodeChecker();
                if(isset($upc)){
                    $upc->total_count=-1;
                    $upc->save();
                    // $release = ReleaseVideo::where('title', $input['title'])->first();
                    // if(!isset($release)){
                        $newrelease = new ReleaseVideo();
                        $newrelease->title = $input['title'];
                        $newrelease->sub_user_id = Auth::user()->id; 
                        $newrelease->user_id = parentId(); 
                        $newrelease->track_type=$input['globel'];
                        $newrelease->upc_temp=$upc->value;
                        $newrelease->price_id=1;
                        //add isrc new code  
                        $isrc = ISPCVIDEO::count();
                        $isrc = 'INS8A10'.(10000+($isrc+1));                
                        $ispc = new ISPCVIDEO;
                        $ispc->key_value='ISRC';
                        $ispc->value=$isrc;
                        $ispc->save();
                        $newrelease->ISRC = $isrc; 
                        $newrelease->save();

                        //add store and release 
                        $store= StoresVideo::get();
                        if(isset($store[0])){
                            foreach($store as $store){
                               $store_release = StoreReleaseVideo:: where(['release_id'=>$newrelease->id,'store_id'=>$store->id])->first();
                               if(!isset($store_release)){
                                    $store_release = new StoreReleaseVideo;
                                    $store_release->release_id= $newrelease->id;
                                    $store_release->store_id= $store->id;
                                    $store_release->status= 1;
                                    if($store->id==31){
                                        $store_release->is_send = 1;
                                    }
                                    $store_release->save();
                               }
                            }
                        }
                        $request->session()->flash('alert-success', trans("admin_lang.record_add_success"));
                        return redirectRoute($this->pageUrl.'.index');
                    // }else{
                    //    $request->session()->flash('alert-danger', 'This title already used.');
                    //     return redirect()->back(); 
                    // }
                }
                else{
                    $request->session()->flash('alert-danger', 'No Upc found');
                    return redirect()->back();
                }
            }catch (ModelNotFoundException $e){
                $message = trans("admin_lang.record_update_failed");
                $request->session()->flash('alert-danger', $message);
                return redirect()->back();
            }
        }
    }

    public function edit(Request $request) {
       // checkRolePermission($this->langPath.'-edit');
        $id = decrypt($request->id);
        /*$Total_track = $this->Models_fiv::where('release_id',$id)->get()->count();
        if($Total_track>1)
        {
          ReleaseVideo::where('id', $id)->update(['format'=>'ALBUM']);  
        }*/
        
       // $upc = UPC::where(['key_value'=>'UPC','total_count'=>0])->orderBy('id','asc')->first();        
        //$upc = $upc->value;
        $datas = ReleaseVideo::where('id', $id)->first();
        $data_check = $datas;
        $i=0;
        $j=0;
        $k=0;
        $l=0;
        if(!isset($datas->title)){ $i++; }else{ if(in_array($datas->title,[null,'0',''])){  $i++; }}
        if(!isset($datas->primary_artist)){ $i++;}else{ if(in_array($datas->primary_artist,[null,'0',''])){ $i++; }}
        if(!isset($datas->genre_id)){ $i++;}else{ if(in_array($datas->genre_id,[null,'0',''])){ $i++; }}
        if(!isset($datas->label_id)){ $i++;}else{ if(in_array($datas->label_id,[null,'0',''])){ $i++; }}
        //if(!isset($datas->format)){ $i++;}else{ if(in_array($datas->format,[null,'0',''])){ $i++; }}
        //if(!isset($datas->UPC)){ $i++;}else{ if(in_array($datas->UPC,[null,'0',''])){ $i++; }}
        //if(!isset($datas->original_release_date)){ $i++;}else{ if(in_array($datas->original_release_date,[null,'0',''])){ $i++; }}
        if(!isset($datas->p_line)){ $i++;}else{ if(in_array($datas->p_line,[null,'0',''])){ $i++; }}
        if(!isset($datas->c_line)){ $i++;}else{ if(in_array($datas->c_line,[null,'0',''])){ $i++; }}
        if(!isset($datas->production_year)){ $i++;}else{ if(in_array($datas->production_year,[null,'0',''])){ $i++; }}
        if(!isset($datas->price_id)){ $j++;}else{ if(in_array($datas->price_id,[null,'0',''])){ $j++; }}
        if(!isset($datas->release_date)){ $k++;}else{ if(in_array($datas->release_date,[null,'0',''])){ $k++; }}       
        if(!isset($datas->getOriginal()['album_profile'])){ $l++;}else{ if(in_array($datas->getOriginal()['album_profile'],[null,'0',''])){ $l++; }} 
        $total_track='0';
        $total_track_count=0;
        $total_media_count=0;
        //$total_track = $this->Models_fiv::where('release_id',$datas->id)->get()->count();
        //$primary_artist = $this->Models_fiv::where('release_id',$datas->id)->pluck('primary_artist')->toArray();
        //$primary_artist =array_unique(explode('|',str_replace(',','|',implode('|',$primary_artist))));
        //$total_track_count = $this->Models_fiv::where(['release_id'=>$datas->id,'track_id'=>0])->get()->count();
        $total_media_count =MediaVideo::where(['album_id'=>$datas->id])->get()->count();
        $e_release = $i;
        $e_album = $l;
        $e_price = $j;
        $e_release_date = $k;

        $genre = Genre::pluck('title','id')->toArray();        
        $subgenre = Subgenre::where('genre_id',$datas->genre_id)->pluck('title','id')->toArray();        
        $price = Price::pluck('title','id')->toArray();
        if(Auth::user()->role_id==4){
            $label = Label::where('sub_user_id',Auth::user()->id)->pluck('title','id')->toArray();
        }elseif(Auth::user()->role_id==3){
            $label = Label::where('sub_user_id',$datas->sub_user_id)->pluck('title','id')->toArray();
        }elseif(Auth::user()->role_id==1){
            $label = Label::where('sub_user_id',$datas->sub_user_id)->pluck('title','id')->toArray();
        }else{
            $label = Label::where('sub_user_id',$datas->sub_user_id)->pluck('title','id')->toArray();
        }
        $labels = array_add($label, 'new', 'New Label');        
        $store = StoreReleaseVideo::where('release_id',$id)->get();  
        $language = Language::pluck('title','language_code')->toArray();
        $data = [
            'page_title'  =>  'Admin | Update  '.ucwords($this->pageUrl),
            'sdata'       =>  $datas,
            'pagePath'    =>  $this->pageUrl,
            'langPath'    =>  $this->langPath,
            'genre'       =>  $genre,
            'subgenre'    =>  $subgenre,
            'price'       =>  $price,
            'label'       =>  $labels,
            'type'        =>  $request->type,
            'e_release'   =>  $e_release,
            'e_price'     =>  $e_price,
            'e_album'     =>  $e_album,
            'e_release_date'        =>  $e_release_date,
            'total_media_count'     =>  $total_media_count,
            'store'         =>  $store,
            'language'      =>  $language,
        ];
        
        return view('admin.'.$this->pageUrl.'.edit', $data);
    }
    
    public function update(Request $request,$id,$type){
       // checkRolePermission($this->langPath.'-edit');
        $id = decrypt($id);
        $data = ReleaseVideo::where('id', $id)->first();
        if($type =='release'){
            $result = $this->release($request,$id,$type,$data);
            if($result['status']=='error'){
                return back()->withErrors($result['message'])->withInput();
            }else{
                  $request->session()->flash('alert-success', $result['message']);
                  return redirectRoute($this->pageUrl.'.edit',['id'=> encrypt($id),'type'=>$type]);
            }
        }elseif($type =='upload_assets'){
            $this->uploadAssets($request,$id,$type,$data);
        }elseif($type =='edit_assets'){
            $this->editAssets($request,$id,$type,$data);
        }elseif($type =='price_tire'){
            $result = $this->priceTire($request,$id,$type,$data);
            if($result['status']=='error'){
                return back()->withErrors($result['message'])->withInput();
            }else{
                  $request->session()->flash('alert-success', $result['message']);
                  return redirectRoute($this->pageUrl.'.edit',['id'=> encrypt($id),'type'=>$type]);
            }
        }elseif($type =='release_date'){
            $result = $this->releaseDate($request,$id,$type,$data);
            if($result['status']=='error'){
                return back()->withErrors($result['message'])->withInput();
            }else{
                $request->session()->flash('alert-success', $result['message']);
                return redirectRoute($this->pageUrl.'.edit',['id'=> encrypt($id),'type'=>$type]);
            }
        }elseif($type =='store'){
            return redirectRoute($this->pageUrl.'.edit',['id'=> encrypt($id),'type'=>$type]);
        }
        elseif($type =='submission'){
            $result =  $this->submission($request,$id,$type,$data);
            if($result['status']=='error'){
                return back()->withErrors($result['message'])->withInput();
            }
            else{
                  $request->session()->flash('alert-success', $result['message']);
                  return redirectRoute($this->pageUrl.'.submission',['id'=> encrypt($id),'type'=>$type]);
            }
        }
    }
    
    public function release($request,$id,$type,$data){
    //checkRolePermission($this->langPath.'-list');        
        $input = $request->all();
        $massage =[
            'primary_artist.0.required'=> 'The primary artist field is required.'
        ];
        $validator = Validator::make($input, [
                'title'             => 'required|unique:releases_videos,title,'.$data->id.',id,sub_user_id,' . Auth::user()->id,
                'primary_artist.0'  => 'required',
                'genre_id'          => 'required',
                'label_id'          => 'required',
                'p_line'         => 'required',
                'c_line'         => 'required',
                'production_year'     => 'required',
                'title_language'      => 'required',
                'description'         => 'required|min:0|max:5000',
                'producer_catalogue_number' => 'required',
        ],$massage);
        if (!$this->validate_admin($validator)) {
            $errors = $validator->errors();
            return ['status'=>'error','message'=>$errors];
        } else {
            $data->title = $input['title'];
            $data->subtitle = !empty($input['subtitle'])?$input['subtitle']:'';
            $data->primary_artist = implode('|',$input['primary_artist']);
            $data->featuring_artist = !empty($input['featuring_artist'])?implode('|',$input['featuring_artist']):array();
            $data->director = !empty($input['director'])?implode('|',$input['director']):array();
            $data->film_director = !empty($input['film_director'])?implode('|',$input['film_director']):array();
            $data->keywords = !empty($input['keywords'])?implode('|',$input['keywords']):array();
            //$data->actor = !empty($input['actor'])?implode('|',$input['actor']):array();
            $data->editor = !empty($input['editor'])?implode('|',$input['editor']):array();
            $data->producer = !empty($input['producer'])?implode('|',$input['producer']):array();
            $data->publisher = !empty($input['publisher'])?$input['publisher']:'';
            $data->remixer = !empty($input['remixer'])?implode('|',$input['remixer']):array();
            $data->genre_id = $input['genre_id'];
            $data->subgenre_id = !empty($input['subgenre_id'])?$input['subgenre_id']:0;
            $data->additional_information = isset($input['additional_information'])?$input['additional_information']:'';
            $data->publisher = isset($input['publisher'])?$input['publisher']:'';
            $data->description = isset($input['description'])?$input['description']:'';
            if($input['label_id']=="new"){
                $label = new Label;
                $label->title = !empty($input['new_label'])?$input['new_label']:0;
                $label->status = 1;
                $label->user_id = parentId();
                $label->sub_user_id = Auth::user()->id;
                $label->save();
                $label_id = $label->id;

                $data->label_id = !empty($label_id)?$label_id:0;
            }else{
                $data->label_id = !empty($input['label_id'])?$input['label_id']:0;
            }
            $data->p_line = $input['p_line'];
            $data->c_line = $input['c_line'];
            $data->production_year = $input['production_year'];            
            $data->UPC = !empty($input['UPC'])?$input['UPC']:null;
            $data->producer_catalogue_number = !empty($input['producer_catalogue_number'])?$input['producer_catalogue_number']:0;
            if(!empty($request->file('album_profile')))
                {
                    $extension = $request->file('album_profile')->getClientOriginalExtension();
                    if(in_array($extension, ['jpg','jpge']))
                    {                        
                        $fille_path = 'NewReleasesVideo/'.$data->upc_temp;
                        $file_name = $data->upc_temp.'.'.$extension;
                        Storage::disk('s3')->putFileAs($fille_path, $request->file('album_profile'),$file_name);                        
                        $store = Storage::disk('s3')->url($fille_path.'/'.$file_name);                         
                        $data->album_profile =  str_replace('https://s3.ap-south-1.amazonaws.com/','http://',$store);
                        $data->folder_path =  $file_name;
                        $data->base_path =  $fille_path;                        
                    }
                }
            $data->save();            
            return ['status'=>'sueesss','message'=>trans("admin_lang.record_update_success")];
        }      
    }
    
    public function formSubmitAjax(Request $request){
        $type = $request->type;
        $value = $request->val; 
        if($type=='primary_artist' || $type=='featuring_artist' || $type=='director'|| $type=='film_director' || $type=='actor' || $type=='editor'|| $type=='producer'|| $type=='publisher'|| $type=='remixer' || $type=='keywords')
        {
          $data = ReleaseVideo::where('id', $request->release_id)->update([$type=>implode('|',$value)]);   
        }
        else if($type=='genre_id' )
        {
            $data = ReleaseVideo::where('id', $request->release_id)->update([$type=>$value]);
            $data1 = Subgenre::where('genre_id',$value)->first();
            $data = ReleaseVideo::where('id', $request->release_id)->update(['subgenre_id'=>$data1->id]);
        }
        else{
            $data = ReleaseVideo::where('id', $request->release_id)->update([$type=>$value]);
        }
    }
    
    public function uploadAssets($request,$id,$type,$data){
        $input = $request->all();
    }
    
    public function editAssets($request,$id,$type,$data){
        $input = $request->all();
    }
    
    public function priceTire($request,$id,$type,$data){
        $input = $request->all();
        $massage =[
            'price_id.required'=> 'The  main price tier field is required.'
        ];
        $validator = Validator::make($input, [
                'price_id'             => 'required',
        ],$massage);
        if (!$this->validate_admin($validator)) {
            $errors = $validator->errors();
            return ['status'=>'error','message'=>$errors];
        } else {
            $data->price_id = !empty($input['price_id'])?$input['price_id']:0;
                   
            $data->save();
           return ['status'=>'sueesss','message'=>trans("admin_lang.record_update_success")];
        }
    }
    
    public function releaseDate($request,$id,$type,$data){
       $input = $request->all();
        $massage =[
            'release_date.required'=> 'The  main price tier field is required.'
        ];
        $validator = Validator::make($input, [
                'release_date'             => 'required',
        ],$massage);
        if (!$this->validate_admin($validator)) {
            $errors = $validator->errors();
            return ['status'=>'error','message'=>$errors];
        } else {
            $data->release_date = !empty($input['release_date'])?$input['release_date']:0;
                        
            $data->save();
           return ['status'=>'sueesss','message'=>trans("admin_lang.record_update_success")];
        }
    }
    
    public function submission($request,$id,$type,$data){
        $input = $request->all();
        if(!isset($data->UPC))
        {
            $upc = UPC::where(['value'=>$data->upc_temp])->orderBy('id','asc')->first();        
            $data->UPC = $upc->value; 
            if(Auth::user()->role_id!=1)
            {
                           
                $data->status = 1;            
                $data->save();
                $upc->user_id=Auth::user()->id;
                $upc->total_count=1;
                $upc->save();
            }
        }
        elseif(empty($data->UPC)){
            $upc = UPC::where(['value'=>$data->upc_temp])->orderBy('id','asc')->first();        
            $data->UPC = $upc->value;
            if(Auth::user()->role_id!=1)
            {
                           
                $data->status = 1;
                $data->save();  
                $upc->total_count=1;
                $upc->user_id=Auth::user()->id;
                $upc->save();
            }
        }
        else{
            if(Auth::user()->role_id!=1)
            {                           
                $data->status = 1;
                $data->save();
            }
        }
        return ['status'=>'sueesss','message'=>'Your release has been successfully created.'];
    }
    
    
    public function submissionView(Request $request){
        $id = decrypt($request->id);
        $datas = ReleaseVideo::where('id',$id)->first();
        $data = [
            'page_title'  =>  'Admin | Update  '.ucwords($this->pageUrl),
            'sdata'       =>  $datas,
            'pagePath'    =>  $this->pageUrl,
            'langPath'    =>  $this->langPath,
            ];
        return view('admin.'.$this->pageUrl.'.submission_view', $data);
    }
    
    public function show(Request $request){
        //checkRolePermission($this->langPath.'-view');
        $id = decrypt($request->id);
        $id = decrypt($request->id);
        $upc = UPC::where(['key_value'=>'UPC','total_count'=>0])->orderBy('id','asc')->first();
        
        $upc = $upc->value;
        $datas = ReleaseVideo::where('id', $id)->first();
        $data_check = $datas;
        $i=0;
        $j=0;
        $k=0;
        if(!isset($datas->title)){ $i++; }else{ if(in_array($datas->title,[null,'0',''])){  $i++; }}
        if(!isset($datas->primary_artist)){ $i++;}else{ if(in_array($datas->primary_artist,[null,'0',''])){ $i++; }}
        if(!isset($datas->genre_id)){ $i++;}else{ if(in_array($datas->genre_id,[null,'0',''])){ $i++; }}
        if(!isset($datas->label_id)){ $i++;}else{ if(in_array($datas->label_id,[null,'0',''])){ $i++; }}
        //if(!isset($datas->format)){ $i++;}else{ if(in_array($datas->format,[null,'0',''])){ $i++; }}
        if(!isset($datas->UPC)){ $i++;}else{ if(in_array($datas->UPC,[null,'0',''])){ $i++; }}
        //if(!isset($datas->original_release_date)){ $i++;}else{ if(in_array($datas->original_release_date,[null,'0',''])){ $i++; }}
        if(!isset($datas->p_line)){ $i++;}else{ if(in_array($datas->p_line,[null,'0',''])){ $i++; }}
        if(!isset($datas->c_line)){ $i++;}else{ if(in_array($datas->c_line,[null,'0',''])){ $i++; }}
        if(!isset($datas->production_year)){ $i++;}else{ if(in_array($datas->production_year,[null,'0',''])){ $i++; }}
        if(!isset($datas->price_id)){ $j++;}else{ if(in_array($datas->price_id,[null,'0',''])){ $j++; }}
        if(!isset($datas->release_date)){ $k++;}else{ if(in_array($datas->release_date,[null,'0',''])){ $k++; }}       
        
        //$total_track='';
        //$total_track_count='';
        $total_media_count=0;
        //$total_track = Track::where('release_id',$datas->id)->get()->count();
        //$total_track_count = Track::where(['release_id'=>$datas->id,'track_id'=>0])->get()->count();
        $total_media_count = MediaVideo::where(['album_id'=>$datas->id])->get()->count();
        $e_release = $i;
        $e_price = $j;
        $e_release_date = $k;



        $genre = Genre::pluck('title','id')->toArray();        
        $subgenre = Subgenre::where('genre_id',$datas->genre_id)->pluck('title','id')->toArray();        
        $price = Price::pluck('title','id')->toArray();        
        $label = Label::pluck('title','id')->toArray();        
        $data = [
            'page_title'  =>  'Admin | Update  '.ucwords($this->pageUrl),
            'sdata'       =>  $datas,
            'pagePath'    =>  $this->pageUrl,
            'langPath'    =>  $this->langPath,
            'genre'       =>  $genre,
            'subgenre'    =>  $subgenre,
            'price'       =>  $price,
            'label'       =>  $label,
            'upc'         =>  $upc,
            'type'        =>  $request->type,
            //'total_track' =>  $total_track,
            'e_release'   =>  $e_release,
            'e_price'     =>  $e_price,
            'e_release_date'        =>  $e_release_date,
            //'total_track_count'     =>  $total_track_count,
            'total_media_count'     =>  $total_media_count
        ];
        return view('admin.'.$this->pageUrl.'.view', $data);
    }  
    
    public function denine(Request $request) {
        $id = $request->id;
        try {
            $data = ReleaseVideo::findOrFail($id);
            $data->status=2;            
            $data->save();
            $request->session()->flash('alert-success', trans("admin_lang.album_denine"));
            return redirectRoute($this->pageUrl.'.index');   
        } catch (ModelNotFoundException $e) {
            $request->session()->flash('alert-danger',trans("admin_lang.record_update_failed"));     
        }
    }
    
    public function approve(Request $request) {
        $id = $request->id;
        try {
            $data = ReleaseVideo::findOrFail($id);
            $data->status=4;
            $data->approve_at=date('Y-m-d H:i:s');
            $data->save();
            $request->session()->flash('alert-success', trans("admin_lang.album_approve"));
            return redirectRoute($this->pageUrl.'.index');
        } catch (ModelNotFoundException $e) {
            $request->session()->flash('alert-danger',trans("admin_lang.record_update_failed"));     
        }
    }
    
    
    public function destroy(Request $request) {
       // checkRolePermission($this->langPath.'-delete');
         $id = decrypt($request->id);
        try {
            $data = ReleaseVideo::findOrFail($id);
            //Track::where('release_id',$id)->delete();
            $media = MediaVideo::where('album_id',$id)->get();
            foreach($media as $media)
            {
                $media->delete();
            }
            if(!empty($data->base_path)){
                Storage::disk('s3')->deleteDirectory($data->base_path);
            }
            $upc = UPC::where(['value'=>$data->upc_temp])->update(['total_count'=>0]);
            $data->delete();
            $request->session()->flash('alert-success', 'Record deleted successfully.');
            return response(['msg' => 'Content Deleted', 'status' => 'success']);
        } catch (ModelNotFoundException $e) {
            return response(['msg' => 'Failed deleting the content', 'status' => 'failed']);
        }
    }
    
    /*Ajax Service*/
    public function subgenreAjax(Request $request){
        $data = Subgenre::where('genre_id',$request->genre_id)->get();
        $html = '';
        foreach($data as $data)
        {
            $html .= '<option value="'.$data->id.'">'.$data->title.'</option>';
        }
        return $html;        
    }
    
    public function storeSet(Request $request){
        $data  =  StoreReleaseVideo::where('id',$request->id)->update(['status'=>$request->status]);
        return ['status'=>true];
    }

    public function storeSelectAll(Request $request){
        $releaseId = decrypt($request->release_id);
        $data  =  StoreReleaseVideo::where('release_id',$releaseId)->update(['status'=>$request->status]);
        return ['status'=>true];
    }
    
    public function storeView(Request $request){
        $data  =  ReleaseVideo::where('id',$request->release_id)->first();
        $store_type = isset($request->store_type)?$request->store_type:0;
        return view('admin.'.$this->pageUrl.'.viewStore',compact('data','store_type'));
    }

    /* Album Store */


    /*public function uploadOnStore(Request $request){

        $action='InitialDelivery';
        $data = $this->Models::where('status',4)->where('release_current_status','NONE')->limit(20)->get();
        $action = 'InitialDelivery';
        if(isset($data[0]))
        {
            foreach($data as  $key => $release)
            {
                $storeData = StoreReleaseVideo::where('release_id',$release->id)->where('is_send',0)->whereNotIn('store_id',$this->non_nicol_stores)->where('status',1)->get();
                if(isset($storeData[0])){
                    foreach($storeData as $store)
                    {
                        $storage = $store->getStore->toArray();                
                        $result = $this->storeXML($storage,$release,$action);                
                        if($result['status']==true)
                        {
                            $store->xml = $result['message'];
                            if($action=='InitialDelivery')
                            {
                                $store->is_send = 1;
                            }
                            $store->save();
                        }
                        else
                        {
                            $store->xml = $result['message'];
                            $store->is_send = -1;
                            $store->save();
                        }
                    }
                }
                $release->release_current_status=$action;
                $release->save();
            }
        }
    }

    */
    
   
    public function finalUpload(Request $request) {
        
        $id = $request->release_id; 
        $action = $request->action; 
        $company_id = 'PADPIDA2019041703R';
        try {
            $data = ReleaseVideo::findOrFail($id);
            $i=0;
            $j=0;              
                foreach($data->getStoreRelease as  $key => $store)
                {

                    //if(!in_array($store->store_id,$this->non_nicol_stores))
                    //{
                        $storage = $store->getStore->toArray();
                        if($store->status==1)
                        {
                            if($store->is_send==0 && $action=='InitialDelivery'){
                                $result = $this->storeXML($storage,$data,$action);
                                if($result['status']==true){
                                    $store->xml = $result['message'];
                                    $store->is_send = 1;
                                    $store->save();
                                    $i++;
                                }else{
                                    $store->xml = $result['message'];
                                    $store->save();
                                    $request->session()->flash('alert-danger','<b>'.$result['message'].'</b>');
                                    return redirectRoute($this->pageUrl.'.index',['type'=>'final']);
                                }
                            }elseif($store->is_send==1 && $action!='InitialDelivery'){
                                $result = $this->storeXML($storage,$data,$action);
                                if($result['status']==true){
                                    $store->xml = $result['message'];
                                    $store->save();
                                    $i++;
                                }else{
                                    $store->xml = $result['message'];
                                    $store->save();
                                    $request->session()->flash('alert-danger','<b>'.$result['message'].'</b>');
                                    return redirectRoute($this->pageUrl.'.index',['type'=>'final']);
                                }
                            }
                            
                        }                 
                    //}
                }
                $data->release_current_status=$action;            
                $data->status=4;
                $data->save();
                $request->session()->flash('alert-success','<b>'.$i.'</b>  notification send successfully or <b>'.$j.'</b> notification already send.');
                return redirectRoute($this->pageUrl.'.index',['type'=>'final']);
        } catch (ModelNotFoundException $e) {
            $request->session()->flash('alert-danger',trans("admin_lang.record_update_failed"));
            return redirectRoute($this->pageUrl.'.index',['type'=>'final']);     
        }
    }
    
    public function storeXML($storage,$data,$action){        
        $company_id = 'PADPIDA2019041703R';
        $message = '<message>'
                    . '<release>'
                    . '<data-exchange-id>'.$storage['data-exchange-id'].'</data-exchange-id>'
                    . '<data-origin-party-id>'.$company_id.'</data-origin-party-id>'
                    . '<data-destination-party-id>'.$storage['data-destination-party-id'].'</data-destination-party-id>'
                    . '<release-id>'.$data->UPC.'</release-id>'
                    . '<search type="album-release-id">'.$data->id.'</search>'
                    . '<delivery-type>'.$action.'</delivery-type>'
                    . '<exists-and-not-proc>true</exists-and-not-proc>'
                    . '<queue-date-time></queue-date-time>'
                    . '<delivery-date-time/>'
                . '</release>'
            . '</message>';
            return  $this->notification_send($message);
    }
    
    public function notification_send($message){
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_PORT => "8080",
        CURLOPT_URL => env('DYSTRIBUTION_STAG_URL'),
        //CURLOPT_URL => env('DYSTRIBUTION_PROD_URL'),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 500,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $message,
        CURLOPT_HTTPHEADER => array(
          "username:mewebservice1",
          "Password:9m893M7",
          "Authorization: Basic bWV3ZWJzZXJ2aWNlMTo5bTg5M003",
          "Content-Type: application/xml",
          //"Postman-Token: 8c8e7333-fa69-40ad-8a0e-be5afaf8b281",
          "cache-control: no-cache"
        ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {             
           //$xml = simplexml_load_string(html_entity_decode($err));
           //$json = json_encode($xml);
           //$array = json_decode($json,TRUE);
           return ['status'=>false,'message'=>$err]; 
        } else {
                      
            $xml = simplexml_load_string(html_entity_decode($response));
            $json = json_encode($xml);
            $array = json_decode($json,TRUE);
            if(isset($array['result']['error-result']) && $array['result']['error-result']=='FAIL' && $array['result']['error-description']=='No new releases to add to queue')
            {
               return ['status'=>true,'message'=>$array['result']['error-description']]; 
            }
            elseif(isset($array['result']['error-result']) && $array['result']['error-result']=='FAIL')
            {
               return ['status'=>false,'message'=>$array['result']['error-description']];  
            }
            elseif(isset($array['result']['file']))
            {
               return ['status'=>true,'message'=>$array['result']['file']];    
            }
        }
    }
    public function finalUploadSFTP(Request $request){
        \Session::put('redirect_to_previous', "1");
        $result = $this->sftpMaker($request['release_id'],$request['action'],$request['receiver']);
        if($result['status']==true){
            $request->session()->flash('alert-success',$result['message']);
        }else{
            $request->session()->flash('alert-danger',$result['message']);
        }
        return redirectRoute($this->pageUrl.'.index',['type'=>'final']);
    }
    
    public function sftpMaker($id=null,$action = 'InitialDelivery',$receiver=''){
        //try{ 
            $action = $action?$action:'InitialDelivery';
            if(isset($receiver) && $receiver!=""){
                $sftp_stores = StoresVideo::where('receiver',[$receiver])->pluck('id')->toArray();
            }else{
                $sftp_stores = StoresVideo::whereIn('receiver',['BOOMPLAY'])->pluck('id')->toArray();
            }
            if($action=='InitialDelivery'){
                $is_send = 0;
                $updateIndicator = 'OriginalMessage';
                $messageType = 'NewReleaseMessage';
            }else{
                $is_send = 1;
                $updateIndicator = 'UpdateMessage';
                $messageType = 'NewReleaseMessage';
            }
            if($action=='TakeDown'){
                $messageType = 'NewDealMessage';
            }
            //echo $is_send;die;
            if(!isset($id)){
                $data = ReleaseVideo::where('status',4)->where('release_current_status','NONE')->whereHas('getStoreRelease',function($q) use($sftp_stores,$is_send){
                    $q->whereIn('store_id',$sftp_stores)
                    ->where('is_send',$is_send)
                    ->where('status',1);
                })->groupBy('releases_videos.id')->with('getMedia')->limit(20)->get();
            }else{
                $data = ReleaseVideo::where('status',4)->where('release_current_status','NONE')->whereHas('getStoreRelease',function($q) use($sftp_stores,$is_send){
                    $q->whereIn('store_id',$sftp_stores)
                    ->where('is_send',$is_send)
                    ->where('status',1);
                })->where('releases_videos.id',$id)->with('getMedia')->get();
            }
            //pr($data,1);
            if(isset($data[0])){
                foreach($data as $release){
                    $track_no= 0;
                    $miliseconds = round(microtime(true) * 1000);
                    $main_folder = date("YmdHis").substr($miliseconds,-3);
                    if(isset($release->ftp_one_folder) && !empty($release->ftp_one_folder)){
                        //$main_folder = $release->ftp_one_folder;
                        $release->ftp_one_folder = $main_folder;
                        $release->save();
                    } else{
                        //$miliseconds = round(microtime(true) * 1000);
                        //$main_folder = date("YmdHis").substr($miliseconds,-3);
                        $release->ftp_one_folder = $main_folder;
                        $release->save();
                    }
                      
                    //boomplay==3
                    if(in_array(3, $sftp_stores)){
                        $track_no= 0;
                        $ftp_path = $main_folder.'/'.$release->UPC;
                        $ftp_file_path = $ftp_path.'/resources/';
                        $path = 'storage/app/public/NewReleasesVideo/';
                        $file = fopen($path.$release->UPC.".xml","w");
                        $data = ['release'=>$release,'updateIndicator'=>$updateIndicator,'messageType'=>$messageType];
                        $xml = view('admin.'.$this->pageUrl.'.releaseXmlBoomplay',$data);
                        fwrite($file,$xml);
                        fclose($file);

                        $filename = 'BatchComplete_'.$main_folder;
                        $blank_xml = fopen($path.$filename.".xml","w");
                        $xml = view('admin.'.$this->pageUrl.'.batchXmlBoomplay',$data);
                        fwrite($blank_xml,$xml);
                        fclose($blank_xml);

                        if(isset($release->getMedia) && $action=='InitialDelivery'){
                            $media = $release->getMedia;
                            if(isset($media)){
                                //if($media->ftp_one_send==0){
                                    $flacFileName = $media->folder_path;
                                    $currentPath = $media->file;
                                    Storage::disk('boomplaysftp')->put($ftp_file_path.'/'.$flacFileName,file_get_contents($currentPath));
                                    //$media->ftp_one_send = 1;
                                    $media->save();
                                //}
                                $track_no++;
                            }
                        }
                        if($action=='InitialDelivery'){
                            $Image = Storage::disk('boomplaysftp')->put($ftp_file_path.'/'.$release->folder_path,file_get_contents($release->album_profile));
                            Storage::disk('boomplaysftp')->setVisibility($ftp_file_path, 'public');
                        }
                        $xml = Storage::disk('boomplaysftp')->put($ftp_path.'/'.$release->UPC.'.xml',file_get_contents($path.'/'.$release->UPC.'.xml'));
                        $xml = Storage::disk('boomplaysftp')->put($main_folder.'/'.$filename.'.xml',file_get_contents($path.'/'.$filename.'.xml'));
                        Storage::disk('boomplaysftp')->setVisibility($main_folder, 'public');
                        Storage::disk('boomplaysftp')->setVisibility($ftp_path, 'public');
                        
                        if($xml==true){
                            file_checker_and_delete($path.$release->UPC.'.xml');
                            file_checker_and_delete($path.$filename.'.xml');
                        }
                        StoreReleaseVideo::where('release_id',$release->id)->where('store_id',3)->where('status',1)->update(['is_send'=>1]);
                    }

                    $data = ['release'=>$release,'track_no'=>$track_no,'betch_id'=>$main_folder];
                    $email_subject = __('Notifacation : SaregamaTone Release Video '.$release->UPC.' Delivery SFTP.');
                    /*Mail::send('emails.ddex_notifacation', $data, function($message) use ($email_subject,$data) {
                         $message->to('sanjivaniofficial@gmail.com', env('APP_NAME', 'SaregamaTone Digital'))->subject($email_subject);
                    });*/
                    
                    //$release->ftp_one_send = 1;
                    //$release->save();
                }
                return ['status'=>true,'message'=>'The video release uploaded on sftp successfully.'];
            }else{
               return ['status'=>false,'message'=>'This video release already uploaded on sftp.'];
            }
        /*} catch (\Exception $e){
            return ['status'=>false,'message'=>'Something error in upload release in sftp.'];
        }*/
    }
   /* public function  xmlMaker($id=null){    
        try{ 
        if(!isset($id)){
        $data = $this->Models::where('status',4)->where('release_current_status','InitialDelivery')->where('yat_send',0)->whereHas('getStoreRelease',function($q){
         $q->whereIn('store_id',$this->ftp_stores);   
        })->get();
        }
        else{
        $data = $this->Models::where('status',4)->where('release_current_status','InitialDelivery')->where('id',$id)->where('yat_send',0)->get();
        }

        
        if(isset($data[0])){    
            foreach($data as $release){
                $track_no= 0;
                $path = 'storage/app/public/NewReleases/';
                $file = fopen($path.$release->UPC.".xml","w");                 
                $data = ['release'=>$release];
                $xml = view('admin.'.$this->pageUrl.'.releaseXml',$data);
                fwrite($file,$xml);
                fclose($file);
                if(isset($release->getReleaseTrack[0])){
                    foreach($release->getReleaseTrack as $track){
                        $media = $track->getTrackMedia;
                        if(isset($media)){
                            if($media->yat_send==0){
                            Storage::disk('ftp')->put($media->base_path.'/'.$media->folder_path,file_get_contents($media->file));
                            $media->yat_send = 1;
                            $media->save();             
                            }
                            $track_no++;
                        }                    
                    }                
                }                
                $Image = Storage::disk('ftp')->put('NewReleases/'.$release->UPC.'/'.$release->folder_path,file_get_contents($release->album_profile));                
                $xml = Storage::disk('ftp')->put('NewReleases/'.$release->UPC.'/'.$release->UPC.'.xml',file_get_contents($path.'/'.$release->UPC.'.xml'));
                if($xml==true){
                    file_checker_and_delete($path.$release->UPC.'.xml');
                }

                $data = ['release'=>$release,'track_no'=>$track_no];
                $email_subject = __('Notifacation : SaregamaTone Release '.$release->UPC.' Delivery.');
                
                Mail::send('emails.ddex_notifacation', $data, function($message) use ($email_subject,$data) {
                     $message->to('sanjivaniofficial@gmail.com', env('APP_NAME', 'SaregamaTone Digital'))->subject($email_subject);
                });               
                
                StoreReleaseVideo::where('release_id',$release->id)->whereIn('store_id',$this->ftp_stores)->where('status',1)->update(['is_send'=>1]);
                $release->yat_send = 1;
                $release->save();                                
            }
            return ['status'=>true,'message'=>'The release uploaded on ftp successfully.'];
        }
        else{
           return ['status'=>false,'message'=>'This release already uploaded on ftp.'];   
        }
        
        } catch (\Exception $e){
            return ['status'=>false,'message'=>'Something error in upload release in ftp.'];
        }
    }
    
    public function finalUploadYAT(Request $request){
        $result = $this->xmlMaker($request['release_id']);
        if($result['status']==true)
        {
            $request->session()->flash('alert-success',$result['message']);  
        }
        else
        {
            $request->session()->flash('alert-danger',$result['message']);   
        }
        
        return redirectRoute($this->pageUrl.'.index',['type'=>'final']); 
    }
    */
  /*  
    public function exportXls(Request $request){        
        $user = User::where('role_id',2)->orderBy('username','asc')->pluck('username','id')->toArray();
        $data = [
            'page_title'  =>  'Admin | Update  '.ucwords($this->pageUrl),
            'sdata'       =>  $user,
            'pagePath'    =>  $this->pageUrl,
            'langPath'    =>  $this->langPath,
            ];
        return view('admin.'.$this->pageUrl.'.export',$data);
    }
    */
    
    public function searchRelease(Request $request){
        $html ='';
        $search = $request->search;
        if ($search && !empty($search)) {
        $q = $this->Models_fiv::select('tracks.id','tracks.ISRC','tracks.title');
        
        
            $q->where(function($query) use ($search) {
                $query->whereRaw('LOWER(tracks.title) ' . 'LIKE ' . '"%' . strtolower($search) . '%"')
                    ->orWhereRaw('LOWER(tracks.ISRC) ' . 'LIKE ' . '"%' . strtolower($search) . '%"');
            });        
        $data = $q->orderBy('ISRC','asc')->get();
        $html ='<table>';
            if(isset($data[0])){
                foreach($data as $sdata){
                    $html .='<tr>';
                    $html .='<td><a href="javascript:;" class="track_details" data-track_title="'.$sdata->title.'" data-track_ISRC="'.$sdata->ISRC.'" data-track_id="'.$sdata->id.'">'.$sdata->title.' ('.$sdata->ISRC.')</a></td>';
                    $html .='</tr>';  
                }
              $html.='</table>';  
            }
        }
        return $html;
    }    
}
