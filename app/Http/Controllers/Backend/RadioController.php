<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Validator;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Models\Radio;
use App\Models\Station;
use App\Models\ProgramCategory;
use App\Models\AddsProgram;
use App\Models\RadioPlaylist;
use JWTAuth;
use DB,DateTime;
class  RadioController extends Controller {

    protected $lang='radio manage';
    protected $page='radio';
    
    public function __construct() { 
        $this->Models = new  Radio; 
        $this->Models_two = new Station;
        $this->Models_thr  = new RadioPlaylist();
    }

    public function index(Request $request)
    {
       if($request->ajax())
       {
        //Gate::authorize(ucfirst($this->page).'-section');
        $data=$this->Models->select('radios.name','radios.week','radios.id','radios.status','radios.start_time','radios.end_time','radios.created_at','stations.name as station_name')
        ->leftJoin('stations', 'stations.id', '=', 'radios.station_id');
        return Datatables::of($data)->editColumn('created_at', function ($data) {
            return $data->created_at->format('m/d/Y h:m:s'); 
        })->addIndexColumn()->make(true);
        }
       else
       {
            $station = $this->Models_two->where('status',1)->pluck('name','id')->toArray();
            $data = ['lang'=>$this->lang,'page'=>$this->page,'station'=>$station]; 
            return routeView($this->page.'.index',$data);
       }
    }
    
    
    public function RadioProgramList(Request $request,$id)
    {
     
        $data = Radio::with('getRadioList.getProgram')->find($id);
        
        $data = ['data'=>$data];
        return routeView($this->page.'.list',$data);  
    }
    
    public function create(Request $request)
    {

        if($request->ajax())
        {
            if($request->method()=='POST')
            {
                $mesasge = [
                    'station_id.required'=>'The Station field is required.',
                ];
                $this->validate($request, [
                   'station_id'    => 'required',
                   'week'       => 'required', 
                   'start_time' => 'required',
                   'end_time'   => 'required',
                   'week'       => 'required',
                   'name'       => 'required|max:255'
                ],$mesasge);
                $input = $request->all(); 
                try{
                                        
                    $data = new $this->Models;                    
                    $data->station_id = $input['station_id'];
                    $data->name = $input['name'];
                    $data->week = $input['week'];
                    $data->start_time = $input['start_time'];
                    $data->end_time = $input['end_time'];  
                    $start_time = new DateTime(date('Y-m-d').' '.$input['start_time']);
                    $end_time =new DateTime(date('Y-m-d').' '.$input['end_time']);
                    $date = $start_time->diff($end_time);
                    $hours = $date->h;
                    $min = $date->i;
                    $sec = $date->s;
                    $duration = ((($hours*60)*60)+($min*60)+$sec);
                    $data->duration= $duration;
                    $data->status= 1;
                    $data->save();


                    $result['message'] = ucfirst($this->lang).' has been created';
                    $result['status'] = 1;
                    return response()->json($result);

                } catch (Exception $e){
                    $result['message'] = ucfirst($this->lang).' can`t created';
                    $result['status'] = 0;
                    return response()->json($result);            
                }
            }        
        }
    }
    
    public function edit(Request $request,$id)
    {
        if($request->ajax())
       {
        //Gate::authorize(ucfirst($this->page).'-section');
        $data=$this->Models->select('radios.name','radios.week','radios.id','radios.status','radios.start_time','radios.end_time','radios.created_at','stations.name as station_name');
        return Datatables::of($data)->editColumn('created_at', function ($data) {
            return $data->created_at->format('m/d/Y h:m:s'); 
        })->addIndexColumn()->make(true);
        }else{
        $data=$this->Models->find($id);
        $programList = $this->Models_thr::where('radio_id',$id)->where('table_type','program')->pluck('table_id')->toArray();
        $addsList = $this->Models_thr::where('radio_id',$id)->where('table_type','adds')->pluck('table_id')->toArray();
        $program = ProgramCategory::where('status',1)->where('station_id',$data->station_id)->get();
        $data = ['lang'=>$this->lang,'page'=>$this->page,'data'=>$data,'program'=>$program,'programList'=>$programList,'addsList'=>$addsList];  
        return routeView($this->page.'.edit',$data);   
        }
    }
    
    public function addRadioList(Request $request){
        $input = $request->all();
        if($input['type']=='true')
        {
            $radio = $this->Models::find($input['radio_id']);
           $data = RadioPlaylist::where('radio_id',$input['radio_id'])->where('table_type',$input['table_type'])->where('table_id',$input['table_id'])->first(); 
           if(!isset($data))
           {
               $duration = RadioPlaylist::where('radio_id',$input['radio_id'])->sum('duration');
               if(($duration) < $radio->duration)
               {               
                $data = new RadioPlaylist;
                $data->radio_id = $input['radio_id'];
                $data->table_type = $input['table_type'];
                $data->table_id = $input['table_id'];
                $data->duration = $input['duration'];
                $data->save();
               }
               return array('status'=>true,'message'=>'success');
           }
           else
            {
               return array('status'=>false,'message'=>'Duration is not grater than time duration');
           }
        }
        else
        {
           RadioPlaylist::where('radio_id',$input['radio_id'])->where('table_type',$input['table_type'])->where('table_id',$input['table_id'])->delete(); 
            return array('status'=>true,'message'=>'success');
        }
    }
    
     public function destroy($id)
    {
       // Gate::authorize(ucfirst($this->page).'-delete');
        $data = $this->Models::findOrFail($id);
        if(isset($data)){
           // $filename = 'uploads/adds_program/'.$data->getRawOriginal('track');
           // Storage::disk('s3')->delete($filename);
            
            $this->Models_thr->where('radio_id',$data->id)->delete();
            $data->delete();
            $result['message'] = ucfirst($this->lang).' deleted successfully';
            $result['status'] = 1;
        }else{
            $result['message'] = ucfirst($this->lang).' Can`t deleted';
            $result['status'] = 0;
        }
        return response()->json($result);
    }
    
 
    public function status($id, $status)
    {
        $details = $this->Models::find($id); 
        if(!empty($details)){
            if($status == 'active'){
                $inp = ['status' => 1];
            }else{
                $inp = ['status' => 0];
            }
            $Category = $this->Models::findOrFail($id);
            if($Category->update($inp)){
                if($status == 'active'){
                    $result['message'] = ucfirst($this->lang).' is activate successfully';
                    $result['status'] = 1;
                }else{
                    $result['message'] = ucfirst($this->lang).' is deactivate successfully';
                    $result['status'] = 1; 
                }
            }else{
                $result['message'] = ucfirst($this->lang).' status can`t be updated!!';
                $result['status'] = 0;
            }
        }else{
            $result['message'] = 'Invaild '.ucfirst($this->lang).'!!';
            $result['status'] = 0;
        }
        return response()->json($result);
    }

}