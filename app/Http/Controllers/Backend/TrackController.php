<?php
namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\UserDetails;
use App\Models\Permission;
use App\Models\PermissionRole;
use App\Models\UserType;
use App\Lib\VIDEOSTREAM;
use Auth;
use Valid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Storage;
use App\Models\Media;
use App\Models\Genre;
use App\Models\Subgenre;
use App\Models\Release;
//use App\Models\Price;
use App\Models\Label;
use App\Models\Track;
use App\Models\Language;
use App\Models\ISPC;
use App\Lib\PushNotification;

class TrackController extends Controller {
    
    public $page="track";
    public $lang="track";
    public function __construct() {
        $this->Models = new Track();
        $this->Models_one = new Genre();
//        $this->Models_two = new Price();
        $this->Models_tre = new Label();
        $this->Models_for = new Subgenre();
        $this->Models_fiv = new Release();
        $this->Models_six = new Language();
        $this->Models_sev = new ISPC();
        $this->Models_eig = new Media();
        $this->sortable_columns = [
            0 => 'id',
            1 => 'title',
            2 => 'posted_by',
            3 => 'track_type',
            4 => 'created_at',
        ];
        $this->sortable_columns_media = [
            0 => 'media_id',
            1 => 'orignal_name',
            2 => 'file_type',
            3 => 'file_type',
        ];
    }
    
    
    
    
    public function index(Request $request) {
        if ($request->ajax()) {
            $limit = $request->input('length');
            $start = $request->input('start');
            $search = $request['search']['value'];
            $orderby = $request['order']['0']['column'];
            $order = $orderby != "" ? $request['order']['0']['dir'] : "";
            $draw = $request['draw'];
            
            $release_id = $request['release_id'] ?? 0;
            $totaldata = $this->Models->getModel($limit, $start, $search, $this->sortable_columns[$orderby], $order,$release_id)->count();
            $response = $this->Models->getModel($limit, $start, $search, $this->sortable_columns[$orderby], $order,$release_id)
                       ->offset($start)
                       ->limit($limit)
                       ->get();
            
            if (!$response) {
                $data = [];
                $paging = [];
            } else {
                $data = $response;
                $paging = $response;
            }

            $datas = array();
            $i = 1;
            $break = 60;
            foreach ($data as $value) {
                $u['DT_RowId'] = '<i class="fa fa-music" aria-hidden="true"></i>';
                $u['album_id'] = $value->getRelease->id;
                $u['ISRC'] = $value->ISRC;
                $u['album_name'] = '<b>'.ucfirst($value->getRelease->title).'</b><br><small>'.$value->getRelease->UPC.'</small>';   ;
                $u['title'] = !empty($value->titletrack_number)?$value->titletrack_number.'. <b>':$i.'. <b>';
                if (strlen($value->title) > 60) {
                    $pos = strpos($value->title, ' ', 60);
                    if ($pos !== false) {
                        $u['title'] .= ucfirst(substr(implode(PHP_EOL, str_split(ucfirst($value->title), $break)), 0, $pos)) . '...';
                    } else {
                        $u['title'] .= ucfirst(substr(implode(PHP_EOL, str_split(ucfirst($value->title), $break)), 0, 60)) . '...';
                    }
                } else {
                    $u['title'] .= ucfirst(implode(PHP_EOL, str_split(ucfirst($value->title), $break)));
                }  
                $u['title'] .='</b><br/><small>';
                $u['title'] .=isset($value->ISRC)?$value->ISRC:'empty';
                $u['title'] .='</small>'; 
                $primary_artist= str_replace('|',', ',$value->primary_artist);
                $u['track_type'] = isset($primary_artist)?ucwords($primary_artist):'empty';
                $u['status'] = isset($value->getMedia)? 'Uploaded':'Empty';
                $u['created_at'] = $value->created_at->format('d M,Y');
                $u['actions']='<div class="btn-group" role="group" aria-label="User Actions">';                              
                $u['actions'] .= '<a href="javascript:;" data-track_id="'.$value->id.'" data-track_name="'.ucwords($value->title).'" data-track_artist="'.ucwords(str_replace(',',', ',$value->primary_artist)).'" data-cover="'.$value->getRelease->album_profile.'" data data-song="'.url_checker(str_replace('flac','mp3',$value->getMedia->base_path.'/'.$value->getMedia->folder_path)).'" class="btn btn-success btn-sm playsong"><i class="fas fa-play"></i></a>';
                $u['actions'] .= '<a href="javascript:;" data-track_id="'.$value->id.'"  class="btn btn-info btn-sm trakeUpload"><i class="fas fa-upload"></i></a>';
                $u['actions'] .= '<a href="javascript:;" data-href_url="' . routeUser($this->page.'.edit', ['type'=>'edit_assets','id'=>encrypt($value->id)]) . '" data-toggle="tooltip" data-placement="top" title="' . __('admin_lang.edit') . '"  class="btn btn-xs btn-primary edit_track"><i class="fa fa-edit"></i></a>';
                $u['actions'] .= createDeleteAction($this->page.'.delete', ['id'=>encrypt($value->id)],'album.edit',['id'=>encrypt($value->release_id),'type'=>'edit_assets']);               
                $u['actions'] .= '</div>';
                $datas[] = $u;
                $i++;
                unset($u);
            }
            $return = [
                "draw" => intval($draw),
                "recordsFiltered" => intval($totaldata),
                "recordsTotal" => intval($totaldata),
                "data" => $datas
            ];
            return $return;
        }
        $release = $this->Models_fiv->pluck('title','id')->toArray();
        $data = [
            'page'=>$this->page,
            'lang' => $this->lang,
            'release'=>$release,
            'page_title'=>'Admin | Manage '. ucwords($this->page),
            ];
        return routeView($this->page.'.index',$data);
    }
    
    public function trackList(Request $request,$release_id){
        $data = $this->Models->where('release_id',$release_id)->with('getMedia')->get();
        $data = ['data'=>$data];
        return routeView($this->page.'.track',$data);
    }
    
    public function albums(Request $request) {
        if ($request->ajax()) {
            $limit = null;
            $start = null;
            $search = $request['search']['value'];
            $orderby = $request['order']['0']['column'];
            $order = $orderby != "" ? $request['order']['0']['dir'] : "";
            $draw = $request['draw'];
            $release_id = $request->id;
            $totaldata = $this->Models->getMediaModel($limit, $start, $search, $this->sortable_columns_media[$orderby], $order,$release_id)->count();
            $response = $this->Models->getMediaModel($limit, $start, $search, $this->sortable_columns_media[$orderby], $order,$release_id)
                   //    ->offset($start)
                   // ->limit($limit)
                    ->get();
            
            if (!$response) {
                $data = [];
                $paging = [];
            } else {
                $data = $response;
                $paging = $response;
            }

            $datas = array();
            $i = 1;
            $break = 60;
            foreach ($data as $value) {
                $u['DT_RowId'] = $i;

                if (strlen($value->orignal_name) > 60) {
                    $pos = strpos($value->orignal_name, ' ', 60);
                    if ($pos !== false) {
                        $u['title'] = ucfirst(substr(implode(PHP_EOL, str_split(ucfirst($value->orignal_name), $break)), 0, $pos)) . '...';
                    } else {
                        $u['title'] = ucfirst(substr(implode(PHP_EOL, str_split(ucfirst($value->orignal_name), $break)), 0, 60)) . '...';
                    }
                } else {
                    $u['title'] = ucfirst(implode(PHP_EOL, str_split(ucfirst($value->orignal_name), $break)));
                }                  
                $u['upload'] ='<i class="fa fa-check-circle-o fa-1x text-success"></i>';
                $u['type'] = strtoupper($value->file_type);                
                $u['actions']='<div class="btn-group" role="group" aria-label="User Actions">';              
                $u['actions'] .= createDeleteAction($this->pageUrl.'.track_delete', ['id'=>encrypt($value->media_id)],'release.edit',['id'=>encrypt($value->album_id),'type'=>'upload_assets']);               
                $u['actions'] .= '</div>';
                $datas[] = $u;
                $i++;
                unset($u);
            }
            $return = [
                "draw" => intval($draw),
                "recordsFiltered" => intval($totaldata),
                "recordsTotal" => intval($totaldata),
                "data" => $datas
            ];
            return $return;
        }
    }
    
    public function create(Request $request) {
        
        $id = decrypt($request->id);
        $isrc = ISPC::getISRC();
        /*$ipsc = $this->Models_sev::get()->count();
        $ipsc = 10000+($ipsc+1);
        $digits = strlen($ipsc);
        $series = ($digits-5);
        $middle = 15+$series;
        $ipsc = substr($ipsc, -5);
        $isrc = 'INS7A'.$middle.$ipsc;*/
        
        $type = $request->type;
        $form_type = $request->form_type;
        $datas = $this->Models_fiv::where('id', $id)->first();
        $genre = $this->Models_one::pluck('name','id')->toArray();        
        //$price = $this->Models_two::pluck('title','id')->toArray();
        $label = $this->Models_tre::pluck('title','id')->toArray();
        $language = $this->Models_six::orderBy('title','asc')->pluck('title','language_code')->toArray();
        $subgenre = $this->Models_for::where('genre_id',$datas->genre_id)->pluck('title','id')->toArray();        
        $data = [
            "page_title"    => "Admin | Manage ".ucwords($this->page),
            "lang"          => $this->lang,
            'page'          => $this->page,
            'sdata'         => $datas,
            'type'          => $type,
            'genre'         => $genre,
            //'price'         => $price,
            'label'         => $label,
            'language'      => $language,
            'subgenre'      => $subgenre,
            'form_type'     => $form_type,
            'isrc'          => $isrc,
            ];
        return routeView($this->page.'.form',$data);
    }

    public function store(Request $request) {      
        $input = $request->form_data;
       
        $massage =[

            //'price_id.required'=> 'The  main price tier field is required.',
            'release_date.required'=> 'The  price tier field is required.',
            'ISRC.required'=> 'The ISRC field is required.'
        ];
        $validator = Validator::make($input, [
                    'track_type'         => 'required',            
                    'secondary_track_type'         => 'required',            
                    'title'         => 'required',            
                    'p_line'         => 'required',            
                    'production_year'         => 'required',            
                    'ISRC'         => 'required|unique:tracks,ISRC',
                    'genre_id'         => 'required',            
                    'track_for_the_release'         => 'required',            
                    //'price_id'         => 'required',            
                    //'producer_catalogue_number'         => 'required',            
                    'title_language'         => 'required',            
                    //'lyrics_language'         => 'required',            
        ],$massage);

        if (!$this->validate_admin($validator)) {
            $errors = $validator->errors();
           
             return json_encode(['status'=>false,'message'=>'validation error found']);
        } else {
            try {               
                $id = decrypt($request->id);
                $track_count = $this->Models::where('release_id',$id)->count();
                $data = new $this->Models;
                $data->release_id = $id;
                $data->track_number = $track_count+1;
                $data->track_type = $input['track_type']; 
                $data->secondary_track_type=$input['secondary_track_type'];                        
                $data->instrumental=$input['instrumental'];                        
                $data->title=$input['title'];                        
                $data->subtitle=!empty($input['subtitle'])?$input['subtitle']:'';                        
                $data->primary_artist=!empty($input['primary_artist'])?implode('|',$input['primary_artist']):'';                        
                $data->featuring_artist=!empty($input['featuring_artist'])?implode('|',$input['featuring_artist']):'';                        
                $data->remixer=!empty($input['remixer'])?implode('|',$input['remixer']):'';                        
                $data->author=!empty($input['author'])?implode('|',$input['author']):'';                        
                $data->composer=!empty($input['composer'])?implode('|',$input['composer']):'';                        
                $data->arranger=!empty($input['arranger'])?implode('|',$input['arranger']):'';                        
                $data->producer=!empty($input['producer'])?implode('|',$input['producer']):'';                        
                $data->p_line=!empty($input['p_line'])?$input['p_line']:'';                        
                $data->production_year=!empty($input['production_year'])?$input['production_year']:'';                        
                $data->publisher=!empty($input['publisher'])?$input['publisher']:'';                        
                $data->ISRC=!empty($input['ISRC'])?$input['ISRC']:'';                        
                $data->genre_id=!empty($input['genre_id'])?$input['genre_id']:0;                        
                $data->subgenre_id=!empty($input['subgenre_id'])?$input['subgenre_id']:0;                        
                $data->track_for_the_release=!empty($input['track_for_the_release'])?$input['track_for_the_release']:0;                        
                $data->price_id=!empty($input['price_id'])?$input['price_id']:0;                        
                $data->producer_catalogue_number=!empty($input['producer_catalogue_number'])?$input['producer_catalogue_number']:'';                        
                $data->parental_advisory=!empty($input['parental_advisory'])?$input['parental_advisory']:'';                        
                $data->preview_start=!empty($input['preview_start'])?$input['preview_start']:30;                        
                $data->title_language=!empty($input['title_language'])?$input['title_language']:'';                        
                $data->lyrics_language=!empty($input['lyrics_language'])?$input['lyrics_language']:'';                        
                $data->track_lyrics=!empty($input['track_lyrics'])?$input['track_lyrics']:'';                        
                $data->save();
                $ipsc = new $this->Models_sev;
                $ipsc->key_value='ISRC';
                $ipsc->value=$data->ISRC;
                $ipsc->save();
                 return json_encode(['status'=>true,'message'=>trans("admin_lang.record_add_success")]);
            }
            catch (ModelNotFoundException $e)
            {
                 return json_encode(['status'=>false,'message'=>trans("admin_lang.record_update_failed")]);
            }
        }
    }
    
  
    public function edit(Request $request) {
        $id = decrypt($request->id);
        
       $datas = $this->Models::where('id', $id)->first();
        $type = $request->type;
        $form_type = $request->form_type;
        //$datas = $this->Models_fiv::where('id', $id)->first();
        $genre = $this->Models_one::pluck('name','id')->toArray();        
        //$price = $this->Models_two::pluck('title','id')->toArray();
        $label = $this->Models_tre::pluck('title','id')->toArray();
        $language = $this->Models_six::orderBy('title','asc')->pluck('title','language_code')->toArray();  
        $subgenre = $this->Models_for::where('genre_id',$datas->genre_id)->pluck('title','id')->toArray();        
        $data = [
            "page_title"    => "Admin | Manage ".ucwords($this->page),
            "lang"      => $this->lang,
            'page'      => $this->page,
            'sdata'         => $datas,
            'type'          => $type,
            'genre'         => $genre,
           // 'price'         => $price,
            'label'         => $label,
            'language'      => $language,
            'subgenre'      => $subgenre,
            'form_type'      => $form_type,
            ]; 
        return routeView($this->page.'.form',$data);
    }

    public function update(Request $request,$id,$type){
        $input = $request->all();      
        $data = $this->Models::where('id', $input['track_id_main'])->first();  
        $massage =[

            //'price_id.required'=> 'The  main price tier field is required.',
            'release_date.required'=> 'The  price tier field is required.',
            'ISRC.required'=> 'The ISRC field is required.'
        ];
        //dd($request->all('form_data'));
        $this->validate($request, [
                    'track_type'         => 'required',            
                    'secondary_track_type'         => 'required',            
                    'title'         => 'required',            
                    'p_line'         => 'required',            
                    'production_year'         => 'required',            
                    'ISRC'         => 'required|unique:tracks,ISRC,'.$data->id,
                    'genre_id'         => 'required',            
                    'track_for_the_release'         => 'required',            
                    //'price_id'         => 'required',            
                    //'producer_catalogue_number'         => 'required',            
                    'title_language'         => 'required',            
                    //'lyrics_language'         => 'required',            
        ],$massage);

        /*if (!$this->validate_admin($validator)) {
            $errors = $validator->errors();
            return json_encode(['status'=>false,'message'=>'validation error found']);
        } else {*/
        try{
                $data->track_type = $input['track_type']; 
                $data->secondary_track_type=$input['secondary_track_type'];                        
                $data->instrumental=$input['instrumental'] ?? 'Yes';                        
                $data->title=$input['title'];                        
                $data->subtitle=!empty($input['subtitle'])?$input['subtitle']:'';                        
                $data->primary_artist=!empty($input['primary_artist'])?implode('|',$input['primary_artist']):'';                        
                $data->featuring_artist=!empty($input['featuring_artist'])?implode('|',$input['featuring_artist']):'';                        
                $data->remixer=!empty($input['remixer'])?implode('|',$input['remixer']):'';                        
                $data->author=!empty($input['author'])?implode('|',$input['author']):'';                        
                $data->composer=!empty($input['composer'])?implode('|',$input['composer']):'';                        
                $data->arranger=!empty($input['arranger'])?implode('|',$input['arranger']):'';                        
                $data->producer=!empty($input['producer'])?implode('|',$input['producer']):'';                        
                $data->p_line=!empty($input['p_line'])?$input['p_line']:'';                        
                $data->production_year=!empty($input['production_year'])?$input['production_year']:'';                        
                $data->publisher=!empty($input['publisher'])?$input['publisher']:'';                        
                $data->ISRC=!empty($input['ISRC'])?$input['ISRC']:'';                        
                $data->genre_id=!empty($input['genre_id'])?$input['genre_id']:0;                        
                $data->subgenre_id=!empty($input['subgenre_id'])?$input['subgenre_id']:0;                        
                $data->track_for_the_release=!empty($input['track_for_the_release'])?$input['track_for_the_release']:0;                        
                $data->price_id=!empty($input['price_id'])?$input['price_id']:0;                        
                $data->producer_catalogue_number=!empty($input['producer_catalogue_number'])?$input['producer_catalogue_number']:'';                        
                $data->parental_advisory=!empty($input['parental_advisory'])?$input['parental_advisory']:'';                        
                $data->preview_start=!empty($input['preview_start'])?$input['preview_start']:30;                        
                $data->title_language=!empty($input['title_language'])?$input['title_language']:'';                        
                $data->lyrics_language=!empty($input['lyrics_language'])?$input['lyrics_language']:'';                        
                $data->track_lyrics=!empty($input['track_lyrics'])?$input['track_lyrics']:'';                        
                $data->save();
                return json_encode(['status'=>true,'message'=>trans("admin_lang.record_update_success")]);
            }
            catch (ModelNotFoundException $e)
            {
                return json_encode(['status'=>false,'message'=>trans("admin_lang.record_update_failed")]);
            }
        //}
    }
    
    public function trackDelete(Request $request) {
        $id = decrypt($request->id);
        try {
            $data = Media::where('media_id',$id)->first();
            $data1 = $data;
            Storage::disk('s3')->delete($data->base_path.'/'.$data->folder_path);
            $data->delete();
            $mediaData = Media::where('album_id',$data1->album_id)->where('media_id','!=',$id)->orderBy('media_id','asc')->get();
            if(isset($mediaData[0]))
            {
                foreach($mediaData as $key =>  $mediaData){
                    if($mediaData->media_id > $id)
                    {
                        $file = explode('_',$mediaData->folder_path);
                        $file_ext = explode('.',$file[2]);
                        $new_file = $file[0].'_'.$file[1].'_'.str_pad($key+1, 2, '0', STR_PAD_LEFT).'.'.$file_ext[1];                
                        Storage::disk('s3')->move($mediaData->base_path.'/'.$mediaData->folder_path, $mediaData->base_path.'/'.$new_file);
                        $store = Storage::disk('s3')->url($mediaData->base_path.'/'.$new_file);
                        $mediaData->file= $store;
                        $mediaData->folder_path= $new_file;
                        $mediaData->save();
                    }
                }
            }
            $request->session()->flash('alert-success', 'Record deleted successfully.');
            return response(['msg' => 'Content Deleted', 'status' => 'success']);
        } catch (ModelNotFoundException $e) {
            return response(['msg' => 'Failed deleting the content', 'status' => 'failed']);
        }
    }
    
    public function trackChecker(Request $request){
       $data =  $this->Models::where('release_id',$request->release_id)->where('track_id',0)->count();
       return json_encode(['counter'=>$data]);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public function show(Request $request){
        $id = decrypt($request->id);
        $datas = $this->Models::where('id', $id)->first();
        $data = [
            'page_title' => 'Admin | View  '.ucwords($this->pageUrl),
            'sdata' => $datas,
            'pagePath'=>$this->pageUrl,
            "langPath" => $this->langPath,
        ];
        return view('admin.'.$this->pageUrl.'.view', $data);
    }  
    
    public function updatePublication(Request $request) {
        $id = $request->id;
        try {
            $data = $this->Models::findOrFail($id);
            $data->globel = $request->status;
            $data->save();
            $request->session()->flash('alert-success', trans("admin_lang.record_update_publication"));
            return "success";
   
        } catch (ModelNotFoundException $e) {
            $request->session()->flash('alert-danger',trans("admin_lang.record_update_failed"));     
        }
    }
    
    public function destroy(Request $request) {
         $id = decrypt($request->id);
        try {
            $data = $this->Models::findOrFail($id);            
            $data->delete();
            $request->session()->flash('alert-success', 'Record deleted successfully.');
            return response(['msg' => 'Content Deleted', 'status' => 'success']);
        } catch (ModelNotFoundException $e) {
            return response(['msg' => 'Failed deleting the content', 'status' => 'failed']);
        }
    }
    
    /*Ajax Service*/
    public function subgenreAjax(Request $request){
        $data = $this->Models_for::where('genre_id',$request->genre_id)->get();
        $html = '';
        foreach($data as $data)
        {
            $html .= '<option value="'.$data->id.'">'.$data->title.'</option>';
        }
        return $html;        
    }
    
   public function trackUpload(Request $request,$track_id =null){
        if ($request->method() == 'POST') {
         
            if(!empty($request->file('audio_file')))
            {    
                
                $track = $this->Models->where('id',$request->track_id)->first();
                $release = $this->Models_fiv::where('id',$track->release_id)->first();            
                $i= 0;
                $j= 0;
                $k= 0;
                $l= 0;
                foreach($request->file('audio_file') as $file){ 
                    if(!empty($file))
                    {
                        $orignalname = $file->getClientOriginalName();
                        $extension = $file->getClientOriginalExtension();   

                        $media = Media::where('track_id',$request->track_id)->first();
                        if(isset($media))
                        {
                            if(in_array($extension,['FLAC','flac','MP3','mp3','MPEG','mpeg']))
                            {
                                //$release_count = $this->Models_eig::where('album_id',$request['id'])->count();
                                //$release_count = $release_count+1;
                               // $media->album_id  =  $request['id'];
                               // $media->track_id  =  $release_count;
                               // $media->user_id  =  Auth::user()->id;
                                $media->type      =  'AUDIO';
                                $fille_path = $media->base_path;


                                $file_name1 =$media->folder_path;
                                $file_name = explode('.',$media->folder_path);
                                $file_name = $file_name[0].'.'.$extension;
                                $orignalname = $file->getClientOriginalName();
                                $ffmpeg_cmd = "ffmpeg -i ".$file." 2>&1 | grep Duration | sed 's/Duration: \(.*\), start/\1/g'";
                                //$ffmpeg_cmd = $this->ffmpeg_path . " -y -i '" . $video . '\' 2>&1 | cat | egrep -e \'(Duration|Stream)\'';
                                //@exec($ffmpeg_cmd);
                                //$output = $ffmpeg_cmd;
                                //dd($output);
                                if(in_array($extension,['AIFF','aiff','wav']))
                                {
                                   /* $extension='flac';
                                    $path = 'storage/app/public/NewReleases/';
                                    $videoFile = $file_name1. ".".$extension;
                                    $file_name=$videoFile;
                                    $files1 =  $audio->covertWavtoFlac($path . $videoFile);
                                    $currentPath= base_path().'/'.$path . $videoFile;
                                    if($files1==true)
                                    {
                                        $orignalname =  explode('.',$orignalname);
                                        $orignalname = $orignalname[0]. ".".$extension;
                                        Storage::disk('s3')->put($fille_path.'/'.$file_name, file_get_contents($currentPath));
                                        file_checker_and_delete($path . $videoFile);
                                    }
                                    else
                                    {
                                        return json_encode(array('status'=>true,'type'=>'ktype','invalid'=>'Audio File Not Converted. Invalid file'));                                                                  
                                    }*/
                                }
                                else
                                {
                                    Storage::disk('s3')->putFileAs($fille_path, $file,$file_name);                                                    
                                }
                                //$store = Storage::disk('s3')->url($fille_path.'/'.$file_name);
                                //$media->file          =  $fille_path.'/'.$file_name;
                                //$media->folder_path   =  $file_name;
                                //$media->base_path     =  $fille_path;
                                //$media->file_type     =  $extension;
                                //$media->file_duration = $duration;
                                //$media->orignal_name  =  $orignalname;
//                             
                                //$media->save();
                                $i++;                    
                            }else{
                                $k++;
                            }                                                  
                        }
                        else{
                            $l++;
                        }
                    }
                }
                if($l>0)
                {
                        return json_encode(array('status'=>true,'type'=>'ktype','invalid'=>'Media does not exist.'));                                                
                }
                else{
                    if($i==0)
                    {
                        return json_encode(array('status'=>true,'type'=>'ktype','invalid'=>$k.' File are invalid formats. You can upload the following format:  MP3, FLAC'));                                
                    }
                    elseif($k==0)
                    {
                       return json_encode(array('status'=>true,'type'=>'itype','success'=>$i.' File Uploaded Successfully.'));                
                    }
                    else{
                    return json_encode(array('status'=>true,'type'=>'Both','success'=>$i.' File Uploaded Successfully.','invalid'=>$k.' File are invalid formats. You can upload the following format:  MP3, WAV, FLAC, AIFF, WMA.'));                
                    }
                }
            }
            else
            {
                return json_encode(array('status'=>false,'message'=>'File is not found. Please upload file.'));
            }
        }
        else
        {
            $data = [
            'page'=>$this->page,
            'lang' => $this->lang,
            'release'=>$track_id,
            'page_title'=>'Admin | Manage '. ucwords($this->page),
            ];
            return routeView($this->page.'.trakeUpload',$data);            
        }
        
        
        
    }
    
    public function ftpupload(){
       $destination = Storage::disk('ftp')->put('NewReleases/0744702487096_01_08.flac',file_get_contents('https://saregamatone.s3.ap-south-1.amazonaws.com/NewReleases/0744702487096/0744702487096_01_08.flac'));                
       dd($destination);
    }
    
    
    
    public function trackSetup(Request $request){

        $dats = $this->Models::where('track_id',$request->input('song_id'))->first();
        if(!isset($dats))
        {
            $this->Models::where('id',$request->input('track_id'))->update(['track_id'=>$request->input('song_id')]); 
            $data = $this->Models::where('id',$request->input('track_id'))->first();
            $data1 = $this->Models::where('release_id',$data->release_id)->where('track_id',0)->count();        
            return json_encode(array('status'=>'true','value_data'=>$data1));
        }
        else{
            return json_encode(array('status'=>'false','value_data'=>'You are already used this song. Please chouse other song.'));   
        }
    }

    public function updatePreviewTime(Request $request) {
        try {
            $track_id = $request->track_id;
            $preview_start = $request->preview_start;
            $data = $this->Models::findOrFail($track_id);
            $data->preview_start = $preview_start?$preview_start:30;
            $data->save();
            $request->session()->flash('alert-success', trans("admin_lang.record_update_success"));
            //return "success";
            return json_encode(['status'=>true,'message'=>trans("admin_lang.record_update_success")]);
        } catch (ModelNotFoundException $e) {
            //$request->session()->flash('alert-danger',trans("admin_lang.record_update_failed"));   
            return json_encode(['status'=>false,'message'=>trans("admin_lang.record_update_failed")]);  
        }
    }
}
