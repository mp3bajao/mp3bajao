<?php
namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\User;
use App\Models\Country;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Gate;
use File;
use App\Models\Stores;
use App\Models\UPC;
use App\Models\ISPC;
use App\Models\Genre;
use App\Models\Subgenre;
use App\Models\Release;
use App\Models\Track;
use App\Models\Label;
use App\Models\Media;
use App\Models\TopChart;
use App\Models\UploadStore;
use App\Models\Artist;
use GrahamCampbell\Flysystem\Facades\Flysystem;
use League\Flysystem\Sftp\SftpAdapter;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Owenoj\LaravelGetId3\GetId3;

use Aws\S3\S3Client;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Filesystem;


class SftpController extends Controller
{
    protected  $recipient;
    protected  $sender;
    protected  $slash = '/';
   // protected  $slash = '\\';
    
    public $page="sftp";
    public $lang="Sftp Store";
    public function __construct() 
    {
        //$this->middleware('auth');         
        //$this->slash = env('LIVE_SLASH');
        //$this->root_path = env('LIVE_FOLDER_ROOT');        
           
        $this->Models_one = new Stores;
        $this->Models_two = new UPC;
        $this->Models_three = new Release();
        $this->Models_four = new Track();
        $this->Models_five = new Label();
        $this->Models_six = new Genre();
        $this->Models_seven = new Subgenre();
        $this->Models_eigth = new Media();
        $this->Models_nine = new UploadStore();
        $this->Models_nine = new UploadStore();
        $store =  $this->Models_one->select('data-destination-party-id')->find(1)->toArray();
        $this->recipient = $store['data-destination-party-id'];        
    }
    
    public function chart(Request $request)
    {
        $data = ['lang'=>'Top Chart','page'=>$this->page]; 
        return routeView($this->page.'.chart',$data);   
    }
    
    /*public function index(Request $request)
    {
        $folder = Storage::disk('backstage')->directories('NewRelease');     
        dd($folder);
    }*/
    
    /*public function folderTree(Request $request,$id)
    {
        $store =  $this->Models_one->where('id',$id)-> where('sender','!=','')->first();
        if(isset($store))
        {
            $main_path = $this->root_path;
            $sftp_folder =  $store->username;
            $main_folder_path = $main_path.$this->slash.$sftp_folder.$this->slash.'files';
            //dd($main_folder_path);
            $data = array_slice(scandir($main_folder_path),2); 
            $data=['data'=>$data,'main_folder_path'=>$main_folder_path,'slash'=>$this->slash];            
            return routeView($this->page.'.folder_tree',$data);  
        }
    }*/
    
    public function multiUploadAlbum($id=0)
    {
        $store =  $this->Models_one->where('id',$id)-> where('sender','!=','')->first();
        if(isset($store))
        { 
            $folder = Storage::disk($store->username)->directories('NewRelease');
            if(!empty($folder))
            {
                foreach($folder as $folder)
                {
                    $file_list = Storage::disk($store->username)->allFiles($folder);                    
                    foreach ($file_list as $key => $value) 
                    {
                        $BatchComplete_xml =  'BatchComplete_'.str_replace('NewRelease/','', $folder).'.xml';
                  
                        $exit = Storage::disk($store->username)->has($folder.'/'.$BatchComplete_xml);
                        if($exit==false)
                        {
                           // dd($exit);  
                            $file_list = Storage::disk($store->username)->directories($folder);
                            if(!empty($file_list))
                            {
                                $UPC = Storage::disk($store->username)->directories($folder);
                                $data = new UploadStore;
                                $data->store_id = $store->id;
                                $data->folder_name = $folder;
                                $data->upc = str_replace($folder.'/','', $UPC[0]);
                                $data->status = -1;
                                $data->error_message = 'batch complete or upc xml file is avalable.';
                                $data->save();
                                //Storage::disk($store->username)->deleteDirectory($folder);
                                $this->deleteDir(storage_path('app/public/'.$folder));
                            }
                            else{
                                Storage::disk($store->username)->deleteDirectory($folder);
                                $this->deleteDir(storage_path('app/public/'.$folder)); $this->deleteDir(storage_path('app/public/'.$folder)); 
                            }
                        }
                        else
                        {

                            $size = Storage::disk($store->username)->size($value);
                            if($size > 0)
                            {
                               Storage::disk('public')->put($value, Storage::disk($store->username)->get($value));                            
                            }
                            else
                            {

                                $file_list = Storage::disk($store->username)->directories($folder);
                               
                                if(!empty($file_list))
                                {
                                    $UPC = Storage::disk($store->username)->directories($folder);
                                    $data = new UploadStore;
                                    $data->store_id = $store->id;
                                    $data->folder_name = $folder;
                                    $data->upc = str_replace($folder.'/','', $UPC[0]);
                                    $data->status = -1;
                                    $data->error_message = 'batch complete or upc xml file is invalid.';
                                    $data->save();
                                    //Storage::disk($store->username)->deleteDirectory($folder);
                                    $this->deleteDir(storage_path('app/public/'.$folder));
                                }
                                /*else{
                                   Storage::disk($store->username)->deleteDirectory($folder);  
                                }*/
                            }                            
                        }

                    }
                    $file_list = Storage::disk($store->username)->allFiles($folder);
                    if(!empty($file_list))
                    {
                        @exec ("find ".storage_path()." -type d -exec chmod 0777 {} +");
                        @exec ("find ".storage_path()." -type f -exec chmod 0777 {} +");
                        $data = new UploadStore;
                        $data->store_id = $store->id;
                        $data->folder_name = $folder;
                        $data->save();
                        $result = $this->sftpfileUpload($store->username,$folder);
                        if($result['status']==true)
                        {

                            $data->status = 1;
                            $data->error_message = $result['message'];

                        }
                        else
                        {
                            $data->status = -1;
                            $data->error_message = $result['message'];
                        }
                        $data->save();
                        Storage::disk($store->username)->deleteDirectory($folder);  
                    }
                }
            }
        }        
    }
    
        
    public function sftpfileUpload($sftp_folder,$folder)
    {
        $BatchComplete_xml =  'BatchComplete_'.str_replace('NewRelease/','',$folder).'.xml';
        $localFilePath = storage_path('app/public/'.$folder.'/'.$BatchComplete_xml);  

        
        $BatchComplete_xml_file =  simplexml_load_file($localFilePath); 
        $BatchComplete_xml_result = $this->batchComplete($BatchComplete_xml_file);
        if($BatchComplete_xml_result['status']==false)
        {                        
            return array('status'=>$BatchComplete_xml_result['status'],'message'=>$BatchComplete_xml_result['message']);
        }
        $releaseType = $BatchComplete_xml_result['type'];
        $UPC = $BatchComplete_xml_result['UPC'];
       // dd($UPC[0]);
        $uploadstore = UploadStore::orderBy('id','desc')->first();
        $uploadstore->upc = $UPC;
        $uploadstore->save();
        
        $release_xml =  $UPC.'.xml';  
     
        $maneFolder = storage_path('app/public/'.$folder.'/'.$UPC); 
          // dd($maneFolder.'/'.$release_xml);
        $release_xml =  simplexml_load_file($maneFolder.'/'.$release_xml);
        $result = $this->releaseUpload($release_xml,$UPC,$releaseType,$folder);
        $this->deleteDir(storage_path('app/public/'.$folder));
        return array('status'=>$result['status'],'message'=>$result['message']);        
    }        
    
    public function releaseUpload($data,$upc,$type,$folder)
    {
        try
        {
           /* $this->Models_three::updateOrCreate(
                    ['UPC' =>$data->ReleaseList->Release[0]->ReleaseId->ICPN],
                    ['UPC' =>$data->ReleaseList->Release[0]->ReleaseId->ICPN]
                );
            */
            
            
            $this->Models_three::firstOrNew(array('UPC' =>$data->ReleaseList->Release[0]->ReleaseId->ICPN));            
            $release = $this->Models_three::firstOrNew(array('UPC' =>$data->ReleaseList->Release[0]->ReleaseId->ICPN));
            if(!isset($release))
            {
               $release = new $this->Models_three;
               $release->UPC       = $data->ReleaseList->Release[0]->ReleaseId->ICPN;
               
            }
                $release->user_id    = $this->sender;
                $release->title     = $data->ReleaseList->Release[0]->ReferenceTitle->TitleText;
                $release->format    = $data->ReleaseList->Release[0]->ReleaseType;
               // $release->UPC       = $data->ReleaseList->Release[0]->ReleaseId->ICPN;
                $release->upc_temp  = $data->ReleaseList->Release[0]->ReleaseId->ICPN;
                $release->producer_catalogue_number = $data->ReleaseList->Release[0]->ReleaseId->CatalogNumber;
                $release->ftp_one_folder = $data->MessageHeader->MessageId;
                $release->label_id  = $this->label($data->ReleaseList->Release[0]->ReleaseDetailsByTerritory->LabelName);
                $release->label_id  = $this->label($data->ReleaseList->Release[0]->ReleaseDetailsByTerritory->LabelName);
                $release->genre_id  = $this->genre($data->ReleaseList->Release[0]->ReleaseDetailsByTerritory->Genre->GenreText);
                $release->subgenre_id = $this->subgenre($data->ReleaseList->Release[0]->ReleaseDetailsByTerritory->Genre->SubGenre,$release->genre_id);
                $release->original_release_date = $data->ReleaseList->Release[0]->ReleaseDetailsByTerritory->OriginalReleaseDate;
                $release->production_year = $data->ReleaseList->Release[0]->PLine->Year;
                $release->p_line    = $data->ReleaseList->Release[0]->PLine->PLineText;
                $release->c_line    = $data->ReleaseList->Release[0]->CLine->CLineText;


                $release->base_path = 'NewReleases/'. $data->ReleaseList->Release[0]->ReleaseId->ICPN;            
                $imgHeight = $data->ResourceList->Image->ImageDetailsByTerritory->TechnicalImageDetails->ImageHeight;
                $imgWidth = $data->ResourceList->Image->ImageDetailsByTerritory->TechnicalImageDetails->ImageWidth;
                $folder_path = $data->ResourceList->Image->ImageDetailsByTerritory->TechnicalImageDetails->File->FilePath;
                $file_name = $data->ResourceList->Image->ImageDetailsByTerritory->TechnicalImageDetails->File->FileName;
                $file_hash = $data->ResourceList->Image->ImageDetailsByTerritory->TechnicalImageDetails->File->HashSum;


                $file = storage_path('app/public/'.$folder.'/'.$upc.'/resources/'.$file_name);
                if(!file_exists($file))
                {
                    return array('status'=>false,'message'=>'Album cover image  does not exist in to the folder.');
                }

                $newfilepath = $file;            
                $thumb_img = Image::make($file)->resize(450, 450);
                $thumb_img->save($newfilepath, 100);
                $storage = Storage::disk('s3')->putFileAs($release->base_path, $newfilepath,$file_name);
                $release->album_profile = $release->base_path.'/'.$file_name;
                $release->folder_path = $file_name;
                $release->save(); 
                $result = $this->track($data,$upc,$type,$release,$folder);
                if($result['status']==true)
                {
                    $this->Models_four::whereIn('id',$result['track_id'])->update(['is_upload'=>1]);
                    $release->is_upload = 1;
                    $release->save(); 
                    $this->Models_two->where('value',$release->upc_temp)->update(['total_count'=>1]);
                    return $result;
                }
                else
                {
                    //$this->Models_four::whereIn('id',$result['track_id'])->delete();
                    //$this->Models_eigth::whereIn('id',$result['media_id'])->delete();
                    //$release->delete();
                    return  $result;
                }
            //}
        }
        catch(Exception  $e)
        {
            return array('status'=>false,'message'=>'System getting something error in xml file.');
        }
        catch(ErrorException  $e)
        {
           return array('status'=>false,'message'=>'System getting something error in xml file.');
        }
    }
    
    public function track($data,$upc,$type,$releaseData,$folder)
    {      
        $i=0;
        $track_id = [];
        $media_id = [];
        foreach($data->ResourceList->SoundRecording  as  $release)
        {
            try
            {
                $releaseList = $data->ReleaseList->Release[$i+1];
                
                $track = $this->Models_four::where('release_id',$releaseData->id)->where('ISRC',$release->SoundRecordingId->ISRC)->first();
                if(!isset($track))
                {
                    $track = new $this->Models_four; 
                    $track->release_id = $releaseData->id;
                    $track->ISRC = $release->SoundRecordingId->ISRC;
                }

                    $track->track_type = 'MUSIC';
                    $track->secondary_track_type = 'original';
                    $track->instrumental = 'instrumental';
                    $track->title = $releaseList->ReferenceTitle->TitleText;
                    $track->subtitle = $releaseList->ReferenceTitle->SubTitle;
                    $track->title_language = $releaseList->LanguageOfPerformance;
                    $track->lyrics_language = $releaseList->LanguageOfPerformance;
                    $duration = $release->Duration;

                    //DisplayArtist
                    $primary_artist =array();
                    foreach($release->SoundRecordingDetailsByTerritory->DisplayArtist as $displayArist)
                    {
                       $primary_artist[] = $displayArist->PartyName->FullName;
                    }
                    $track->primary_artist = implode('|', $primary_artist);
                    $this->artist(implode('|', $primary_artist));
                    //DisplayPreducer
                    $producer =array();
                    foreach($release->SoundRecordingDetailsByTerritory->ResourceContributor as $ResourceContributor)
                    {
                       $producer[] = $ResourceContributor->PartyName->FullName;
                    }
                    $track->producer = implode('|', $producer);

                    // Artist
                    $composer =array();
                    $lyricist =array();
                    $MusicPublisher =array();
                    foreach($release->SoundRecordingDetailsByTerritory->IndirectResourceContributor as $IndirectResourceContributor)
                    {
                        if($IndirectResourceContributor->IndirectResourceContributorRole =='Composer')
                        {
                            $composer[] = $IndirectResourceContributor->PartyName->FullName;                   
                        }
                        else if($IndirectResourceContributor->IndirectResourceContributorRole =='Lyricist')
                        {
                           $lyricist[] = $IndirectResourceContributor->PartyName->FullName;  
                        }
                        else if($IndirectResourceContributor->IndirectResourceContributorRole =='MusicPublisher')
                        {
                           $MusicPublisher[] = $IndirectResourceContributor->PartyName->FullName;  
                        }
                    }

                    $track->arranger = implode('|', $composer);
                    $track->composer = implode('|', $composer);
                    $track->author = implode('|', $lyricist);
                    $track->publisher = implode('|', $MusicPublisher);
                    $track->p_line = $release->SoundRecordingDetailsByTerritory->PLine->PLineText;
                    $track->production_year = $release->SoundRecordingDetailsByTerritory->PLine->Year;
                    $track->genre_id = $this->genre($release->SoundRecordingDetailsByTerritory->Genre->GenreText);
                    $track->subgenre_id = $this->subgenre($release->SoundRecordingDetailsByTerritory->Genre->SubGenre,$track->genre_id);
                    $track->track_for_the_release = 'No';
                    $track->producer_catalogue_number = $releaseData->producer_catalogue_number;
                    $track->parental_advisory = 'Not Explicit';
                    $track->lyrics_language = 'Not Explicit';
                    $track->title_language = strtoupper($release->LanguageOfPerformance);
                    $track->save(); 
                    $track_id[] = $track->id;

                    $base_path = 'NewReleases/'. $data->ReleaseList->Release[0]->ReleaseId->ICPN; 
                    $file_name_input = $release->SoundRecordingDetailsByTerritory->TechnicalSoundRecordingDetails->File->FileName;
                    $file_name_output = str_replace('flac', 'mp3', $file_name_input);                
                    $folder_path = $release->SoundRecordingDetailsByTerritory->TechnicalSoundRecordingDetails->File->FilePath;
                    $file_input = storage_path('app/public/'.$folder.'/'.$upc.'/resources/'.$file_name_input);
                    $file_output = storage_path('app/public/'.$folder.'/'.$upc.'/resources/'.$file_name_output);

                    if(!file_exists($file_input))
                    {
                        return array('status'=>false,'message'=>'Audio file did not exist in to the folder.','track_id'=>$track_id,'media_id'=>$media_id);
                    }
                    if(md5_file($file_input)!=$release->SoundRecordingDetailsByTerritory->TechnicalSoundRecordingDetails->File->HashSum->HashSum)
                    {
                        return array('status'=>false,'message'=>'Audio file format is not correct. Please check file formate or HashSum file key.','track_id'=>$track_id,'media_id'=>$media_id);                    
                    }

                    $cmd = 'ffmpeg -i '.$file_input.' -ab 320k -map_metadata 0 -id3v2_version 3 '.$file_output;
                    @exec($cmd);                
                    $storage = Storage::disk('s3')->putFileAs($base_path, $file_output,$file_name_output);

                    $duration = str_replace('S','',str_replace('M',':',str_replace('H',':',str_replace('PT','',$release->Duration))));
                
                    $media = $this->Models_eigth->where('album_id',$releaseData->id)->where('track_id',$track->id)->first();
                    if(!isset($media))
                    {
                        $media = new $this->Models_eigth;
                        $media->album_id  = $releaseData->id;
                        $media->track_id  = $track->id;
                        $media->user_id   = $this->sender;            
                        $media->type      = 'AUDIO';
                        $media->file_type = 'mp3';
                        $media->base_path = $base_path;
                        $media->folder_path = $file_name_output;
                        $media->file_duration = $duration;
                        $media->orignal_name = 'mp3';
                        $media->save();
                        $media_id[] = $media->id;
                    }
                    else
                    {
                      $media_id[]  = $media->id;
                    }
                    $i++; 
                //}
            }
            catch(Exception  $e)
            {
                return array('status'=>false,'message'=>'System getting something error in xml file..','track_id'=>$track_id,'media_id'=>$media_id);
            }
            catch(ErrorException  $e)
            {
               return array('status'=>false,'message'=>'System getting something error in xml file.','track_id'=>$track_id,'media_id'=>$media_id);
            }            
        } 
        return array('status'=>true,'message'=>'Album  are upload on s3 bucket successfully.','track_id'=>$track_id,'media_id'=>$media_id);
    }
    
    
    
 
    
    public function batchComplete($data)
    {
        $sender = $this->Models_one->where('data-destination-party-id',$data->MessageHeader->MessageSender->PartyId)->first()->toArray();
        if(isset($sender) && ($data->MessageHeader->MessageSender->PartyId!=$this->recipient))
        {
            $this->upc($data->MessageInBatch->IncludedReleaseId->ICPN,$sender);
            $type = $data->MessageInBatch->MessageType; 
            $data = ['status'=>true,'message'=>'success','type'=>$type,'UPC'=>$data->MessageInBatch->IncludedReleaseId->ICPN];
        }
        else
        {
            $data = ['status'=>false,'message'=>$data->MessageHeader->MessageSender->PartyName->FullName.' is not authenticated store'];
        }
        return $data;
    }
    
    
    
    public function upc($data,$sender,$type=0)
    {
        $this->sender = $sender['id'];
        $upc =  $this->Models_two->where(['user_id'=>$sender['id'],'key_value'=>'UPC','value'=>$data])->first();
        if(!isset($upc))
        {
            $this->Models_two->create([
                'user_id'    => $sender['id'],
                'key_value'  => 'UPC',
                'value'      => $data,
                'music_type' => $type,
                'total_count' => -1 
            ]);
        }    
       
    }
    
    public function label($data)
    {
        $label =  $this->Models_five->whereRaw('LOWER(`title`) LIKE ? ',['%'.trim(strtolower($data)).'%'])->first();
        if(!isset($label))
        {
            $label = new $this->Models_five;        
            $label->user_id = $this->sender;
            $label->title  = $data;
            $label->status = 1;
            $label->save();
        } 
        return $label->id;      
    }
    
    public function genre($data)
    {
        $genre =  $this->Models_six->whereRaw('LOWER(`name`) LIKE ? ',['%'.trim(strtolower($data)).'%'])->first();
        if(!isset($genre))
        {
            $genre = new $this->Models_six;        
            $genre->name = $data;
            $genre->status = 1;
            $genre->save();
        } 
        return $genre->id;
       
    }

    public function subgenre($data,$genre_id)
    {
        $subgenre =  $this->Models_seven->whereRaw('LOWER(`title`) LIKE ? ',['%'.trim(strtolower($data)).'%'])->first();
        if(!isset($subgenre))
        {
            $subgenre = new $this->Models_seven;        
            $subgenre->genre_id = $genre_id;
            $subgenre->title = $data;
            $subgenre->status = 1;
            $subgenre->save();
        } 
        return $subgenre->id;       
    }
    
    public  function deleteDir($dirPath) {
        if (! is_dir($dirPath)) {
            return false;
           // throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }
    
    public function artist($data){
       $array = explode('|',$data);       
       $array = array_map('trim', $array);
       foreach($array as $array)
       {
          $artist = Artist::where('name',$array)->first();
          if(!isset($artist))
          {
             Artist::insert(['name'=>$array]); 
          }
       }
    }
    
    public function report(Request $request){
        if($request->ajax())
        {
        $category= UploadStore::select('stores.title','upload_stores.store_id','upload_stores.folder_name','upload_stores.status','upload_stores.error_message','upload_stores.created_at','upload_stores.id','upload_stores.upc')->whereIn('upload_stores.status',[1,-1])
         ->leftJoin('stores', 'stores.id', '=', 'upload_stores.store_id');
         return Datatables::of($category)->addIndexColumn()->make(true);
        }else{
           $data = ['lang'=>$this->lang,'page'=>$this->page]; 
            return routeView($this->page.'.report',$data);
       }
    }    
}
