<?php
namespace App\Http\Controllers;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\Models\Notification;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Mail;

use File,DB;

class NotificationController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Gate::authorize('Category-section');
        $notification=Notification::select('notifications.*');
        return Datatables::of($notification)->editColumn('created_at', function ($notification) {
            return $notification->created_at->format('m/d/Y h:m:s'); 
        })->editColumn('user_type', function ($notification) {
			$notification_type = array_flip(notificationUserType());
			$notification_type[$notification['user_type']];
            return $notification_type[$notification['user_type']]; 
		})
		->editColumn('notification_type', function ($notification) {
			$notification_type = array_flip(notificationType());
			$notification_type[$notification['notification_type']];
            return $notification_type[$notification['notification_type']]; 
        })->addIndexColumn()->make(true);
    }
    
    public function frontend()
    {
        Gate::authorize('Category-section');
        $data['user_type'] = array_flip(notificationUserType());
        $data['notification_type'] = array_flip(notificationType());
        return view('notifications.listing',$data);
    }

    public function edit_frontend($id)
    {
        Gate::authorize('Category-edit');
        $data['notifications'] = Notification::findOrFail($id);
        return view('notifications.edit',$data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        Gate::authorize('Category-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Gate::authorize('Category-create');
         // validate
		$this->validate($request, [           
            'user_type'=>'required',
            'notification_type'=>'required',
            'title' => 'required',
            'message'=> 'required'
        ]);
        
        $user_type 	= $request->user_type;
        $title 		=$request->title;
        $message 	= $request->message;

        switch ($request->notification_type) {
            case '2':
                $this->__sendSMS($user_type,$title,$message);
                break;
            case '3':
                $this->__sendPushNotification($user_type,$title,$message);
                break;
            case '4':
                $this->__sendSMS($user_type,$title,$message);
                $this->__sendPushNotification($user_type,$title,$message);
                $this->__sendEmail($user_type,$title,$message);
                break;
            default:
                $this->__sendEmail($user_type,$title,$message);
                break;
        }
 
        $notification = new Notification();

        $notification->user_type 		= $request->user_type;
        $notification->notification_type= $request->notification_type;
        $notification->title 			= $request->title;
        $notification->message 			= $request->message;
        
        if($notification->save()){
            $result['message'] = 'Notification Send Successfully';
            $result['status'] = 1;
        }else{
            $result['message'] = 'Notification Can`t be Send';
            $result['status'] = 0;
        }
        return response()->json($result);
    }

    private function __sendEmail($user_type,$title,$message)
	{
	 	$userdetails 		= $this->__getUserdetails();
	 	$celebritydetails 	= $this->__getCelebritydetails();
        
	 	switch ($user_type) {
	 		case '1':
	 			if (!empty($userdetails) && !empty($celebritydetails)) {
	 				$result = array_merge($userdetails,$celebritydetails);
	 			}
	 			break;

	 		case '3':
	 				$result = $celebritydetails;
	 			break;
	 		
	 		default:
	 				$result = $userdetails;
	 			break;
		 }

	 	if (!empty($result)) {
	 		foreach ($result as $value) {

	 			if (!empty($value['email'])) {
	
					$details = [
						'new_email'=>$value['email'],
						'title' => $title
					 ];
					//  dd($details);
					try {
						$beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
						$beautymail->send('emails.notifications', $details, function($message) use ($details)
						{
							$message
								->to($details['new_email'])
								->subject($details['title']);
						});
						 return true; 
					} catch(\Exception $e){
						dd($e->getMessage());
					}
				}
		 	}
	 	}
	 	return true;
	}

	/**
	* Function for send SMS
	*
	* @param null
	*
	* @return null. 
	*/

	private function __sendSMS($user_type,$title,$message)
	{
	 	$userdetails 		= $this->__getUserdetails();
	 	$celebritydetails 	= $this->__getCelebritydetails();
         echo $user_type;
         die;
	 	switch ($user_type) {
	 		case '1':
	 			if (!empty($userdetails) && !empty($celebritydetails)) {
	 				$result = array_merge($userdetails,$celebritydetails);
	 			}
	 			break;

	 		case '2':
	 				$result = $celebritydetails;
	 			break;
	 		
	 		default:
	 				$result = $userdetails;
	 			break;
	 	}

	 	if (!empty($result)) {
	 		foreach ($result as $value) {
	 			if (!empty($value['mobile'])) {
	 				$mobile = $value['country_code'].''.$value['mobile'];
	 				$this->sendMessages($mobile,$message);
				}				
		 	}
	 	}
	 	return true;
	}

	/**
	* Function for send PushNotification
	*
	* @param null
	*
	* @return null. 
	*/

	private function __sendPushNotification($user_type,$title,$message)
	{
	 	$userdetails 		= $this->__getUserdetails();
	 	$celebritydetails 	= $this->__getCelebritydetails();
         echo $user_type;
         die;
	 	switch ($user_type) {
	 		case '1':
	 			if (!empty($userdetails) && !empty($celebritydetails)) {
	 				$result = array_merge($userdetails,$celebritydetails);
	 			}
	 			break;

	 		case '2':
	 				$result = $celebritydetails;
	 			break;
	 		
	 		default:
	 				$result = $userdetails;
	 			break;
	 	}

	 	if (!empty($result)) {
	 		foreach ($result as $value) {
	 			if (!empty($value['device_token'])) {
				}				
		 	}
	 	}
	 	return true;
	}

	/**
	* Function for get Users mobile number and email and Device ids;
	*
	* @param null
	*
	* @return null. 
	*/

	private function __getUserdetails()
	{
	 	$userdetails =  User::select('country_code','email','mobile','name')->where([
					    ['type', '=',0],
					    ['status', '=',1]
					])
					->get();
		if (!empty($userdetails->toArray())) {
			return $userdetails->toArray();
		}else{
			return false;
		}
	}

	/**
	* Function for get Celebrity mobile number and email and Device ids;
	* @param null
	* @return null. 
	*/

	private function __getCelebritydetails()
	{
 		$celebritydetails =  User::select('country_code','email','mobile','name')->where([
                            ['type', '=',2],
                            ['status', '=',1]
								])
	 							->get();

	 	if (!empty($celebritydetails->toArray())) {
			return $celebritydetails->toArray();
		}else{
			return false;
		}
	}

}
