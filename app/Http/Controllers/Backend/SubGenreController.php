<?php
namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Gate;
use App\Models\Media;
use File,DB;
use App\Models\Genre;
use App\Models\Subgenre;
use App\Models\GenreLang;
use App\Models\Language;

class SubGenreController extends Controller
{
    protected $lang='subgenres';
    protected $page='subgenres';
    
    public function __construct() 
    {
        $this->Models = new  Subgenre;
        $this->Models_two = new  Genre;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       if($request->ajax())
       {
        //Gate::authorize(ucfirst($this->page).'-section');
        $data=$this->Models->select('subgenre.title','subgenre.created_at','subgenre.id','subgenre.status','genres.name as genres_name')
                ->leftJoin('genres', 'genres.id', '=', 'subgenre.genre_id');
        return Datatables::of($data)->editColumn('created_at', function ($data) {
            return $data->created_at->format('m/d/Y h:m:s'); 
        })->addIndexColumn()->make(true);
        }
       else
       {
            $genre = $this->Models_two::where('status',1)->get();
            $data = ['lang'=>$this->lang,'page'=>$this->page,'genre'=>$genre]; 
            return routeView($this->page.'.index',$data);
       }
    }
    
    /*public function frontend()
    {
        Gate::authorize('Genres-section');
        return view('category.listing');
    }*/

    public function edit_frontend($id)
    {
        Gate::authorize(ucfirst($this->page).'-edit');
        $data['category'] = $this->Models->findOrFail($id);
        return view('category.edit',$data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        Gate::authorize(ucfirst($this->page).'-create');
        if($request->ajax())
        {
            if($request->method()=='POST')
            {
                $mesasge = [
                    'name.required'=>'The Name field is required.',                    
                    'name.max'=>'The name may not be greater than 255 characters.',
                    'image.size'  => 'the file size is less than 2MB',
                ];
                $this->validate($request, [
                   'name'  => 'required|max:255|unique:genres',
                   'image' => 'image|mimes:jpeg,png,jpg,gif ,svg|max:2048',
                ],$mesasge);
                $input = $request->all(); 
                try{            
                   $data = new $this->Models;
                    if ($request->file('image')) {
                        $file = $request->file('image');
                        $result = image_upload($file,'category');
                        if($result[0]==true){
                            $data->image = $result[1];
                        }
                    }
                    $data->genre_id = $input['genres'];
                    $data->title = $input['name'];
                    $data->status= 1;
                    $data->save();
                    
                    $result['message'] = ucfirst($this->lang).' has been created';
                    $result['status'] = 1;
                    return response()->json($result);

                } catch (Exception $e){
                    $result['message'] = ucfirst($this->lang).' can`t created';
                    $result['status'] = 0;
                    return response()->json($result);            
                }
            }        
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
        Gate::authorize(ucfirst($this->page).'-section');
        $data = $this->Models->findOrFail($id);
        $data = ['lang'=>$this->lang,'page'=>$this->page,'category'=>$data]; 
        return routeView($this->page.'.edit',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //
        Gate::authorize(ucfirst($this->page).'-edit');
        if($request->ajax())
        {
            if($request->method()=='POST')
            {
                $mesasge = [
                    'name.required'=>'The Name field is required.',                    
                    'name.max'=>'The name may not be greater than 255 characters.',
                    'image.size'  => 'the file size is less than 2MB',
                ];
                $this->validate($request, [
                   'name'  => 'required|max:255|unique:genres',
                   'image' => 'image|mimes:jpeg,png,jpg,gif ,svg|max:2048',
                ],$mesasge);

                $input = $request->all();
                $cate_id = $id;
                $file = $request->file('image');
                try{    
                    $data = $this->Models->where('id',$cate_id)->update(['genre_id'=>$input['genres'],'title'=>$input['name']]);
                    $result['message'] = ucfirst($this->lang).' updated successfully.';
                    $result['status'] = 1;
                    return response()->json($result);
                }
                catch (Exception $e)
                {
                    $result['message'] = ucfirst($this->lang).' Can`t be updated.';
                    $result['status'] = 0;
                    return response()->json($result);           
                }
            }
            else{
                $data = $this->Models->findOrFail($id);
                $genre = $this->Models_two::where('status',1)->get();
                $data = ['lang'=>$this->lang,'page'=>$this->page,'genre'=>$genre,'data'=>$data]; 
                return routeView($this->page.'.edit',$data);
            }
        }      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Gate::authorize(ucfirst($this->page).'-edit');
        // validate
	 $mesasge = [
            'name.en.required'=>'The Name field is required.',
            'description.en.required'=>'The description field is required.',
            'name.en.max'=>'The name may not be greater than 255 characters.',
            'image.size'  => 'the file size is less than 2MB',
          ];
          $this->validate($request, [
               'name.en'  => 'required|max:255',
               'description.en'=>'required',
               'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
          ],$mesasge);
            
        $input = $request->all();
        $cate_id = $id;
        
        $file = $request->file('image');
        try{
            $lang = Language::pluck('lang')->toArray();
            foreach($lang as $lang)
            {
                if($lang=='en')
                {
                    if(isset($file))
                    {
                        $result = image_upload($file,'category');
                        $data = Category::where('id',$cate_id)->update(['name'=>$input['name'][$lang],'description'=>$input['description'][$lang],'image'=>$result[1]]);
                    }
                    else
                    {
                        $data = Category::where('id',$cate_id)->update(['name'=>$input['name'][$lang],'description'=>$input['description'][$lang]]);
                    }                    
                }
                $dataLang = CategoryLang::where(['category_id'=>$cate_id,'lang'=>$lang])->first();
                if(isset($dataLang))
                {
                   $dataLang = CategoryLang::where(['category_id'=>$cate_id,'lang'=>$lang])->update(['name'=>$input['name'][$lang],'description'=>$input['description'][$lang]]);                                   
                }
                else
                {
                    $dataLang = new  CategoryLang;
                    $dataLang->category_id = $cate_id;
                    $dataLang->name = $input['name'][$lang];
                    $dataLang->description = $input['description'][$lang];
                    $dataLang->lang = $lang;
                    $dataLang->save();
                }
            }
            $result['message'] = 'Food Category updated successfully.';
            $result['status'] = 1;
            return response()->json($result);
        }
        catch (Exception $e)
        {
            $result['message'] = 'Food Category Can`t be updated.';
            $result['status'] = 0;
            return response()->json($result);           
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gate::authorize(ucfirst($this->page).'-delete');
        if(Category::findOrFail($id)->delete()){
            $result['message'] = 'Food Category deleted successfully';
            $result['status'] = 1;
        }else{
            $result['message'] = 'Food Category Can`t deleted';
            $result['status'] = 0;
        }
        return response()->json($result);
    }
    
    public function status($id, $status)
    {
        $details = $this->Models::find($id); 
        if(!empty($details)){
            if($status == 'active'){
                $inp = ['status' => 1];
            }else{
                $inp = ['status' => 0];
            }
            $Category = $this->Models::findOrFail($id);
            if($Category->update($inp)){
                if($status == 'active'){
                    $result['message'] = ucfirst($this->lang).' is activate successfully';
                    $result['status'] = 1;
                }else{
                    $result['message'] = ucfirst($this->lang).' is deactivate successfully';
                    $result['status'] = 1; 
                }
            }else{
                $result['message'] = ucfirst($this->lang).' status can`t be updated!!';
                $result['status'] = 0;
            }
        }else{
            $result['message'] = 'Invaild '.ucfirst($this->lang).'!!';
            $result['status'] = 0;
        }
        return response()->json($result);
    }
}