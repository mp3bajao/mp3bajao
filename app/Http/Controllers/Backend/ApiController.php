<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Models\Category;
use App\Models\Gift;
use App\Models\Products;
use App\Models\Media;
use App\Models\UsersAddress;
use JWTAuth;
use App\Models\Cart;
use App\Models\UserOtp;
use DB;
class ApiController extends Controller {

    public function __construct() {       
        $this->middleware('auth:api');
        define("A_TO_Z",'a_to_z');
        define("Z_TO_A",'z_to_a');
    }

   public function getAllData(Request $request){

        $serachData = $request->all();
        $limit = 10;
        $radius = 10;
        $getCategory =$this->getCategory($serachData,$limit);
        $getDish = $this->getDishLatAndLong($serachData,$limit,$radius);
        $getCelebrity = $this->getCelebrity($serachData,$limit);
        $getHostDish =  $this->getDish($serachData, $limit);
        $getBanner = $this->getBanner();
        $cartCount = $this->getCart();
        
        if(count($getCategory)){
            $data['category'] = $getCategory;
            $data['categoryImageBaseUrl'] = asset('uploads/category').'/';
        }else{
            $data['category'] = [];
        }

        if(count($getDish)){
            $data['dish'] = $getDish;
            $data['dishImageBaseUrl'] = asset('uploads/product').'/';
        }else{
            $data['dish'] = [];
        }

        if(count($getCelebrity)){
            $data['celebrity'] = $getCelebrity;
            $data['celebrityImageBaseUrl'] = asset('uploads/user').'/';
        }else{
            $data['celebrity'] = [];
        }

        if(count($getHostDish)){
            $data['hotDish'] = $getHostDish;
            $data['hotDishImageBaseUrl'] = asset('uploads/product').'/';
        }else{
            $data['hotDish'] = [];
        }

        if(count($getBanner)){
            $data['banner'] = $getBanner;
            $data['bannerImageBaseUrl'] = asset('uploads/banner').'/';
        }else{
            $data['banner'] = [];
        }
        $data['cartCount'] = $cartCount;
        $response['status'] = 1;
        $response['data'] = $data;
        return response()->json($response, 200);
    }



    public function getBanner(){
        $bannerList = Media::select('*')->where(['medias.table_name'=>'Banner']);
        
        $bannerDetail = $bannerList->get();

        return $bannerDetail;
    }

    

    public function getCart(){
        $userData = auth()->user();
        $userId =  $userData->id;
        $query = Cart::where('user_id',$userId)->get();
        $totalQty = 0;
        if(count($query)){
            foreach($query as $data){
                $totalQty += $data->qty;
            }
        }else{
            $totalQty = 0;
        }
        return $totalQty;
    }

    public function getAllAddress(){
        $userData = auth()->user();
        $userId =  $userData->id;
        if(!empty($userId)){

            $userAddressList =  UsersAddress::where('user_id',$userId)->get();
            if(count($userAddressList)){
                $data['allAddress'] = $userAddressList;

                $defaultAddress = array();
                $defaultAddress['address'] =  $userData->address;
                $defaultAddress['latitude'] =  $userData->latitude;
                $defaultAddress['longitude'] =  $userData->longitude;
                $data['defaultAddress'] = $defaultAddress;

            }else{
                $data['allAddress'] = [];
            }
            if(count($data)){
                $response['status'] = 1;
                $response['data'] = $data;
                return response()->json($response, 201);
            }else{
                $response['status'] = 0;
                $response['message'] = 'Adderss Not Found.';
                return response()->json($response, 401);
            }
        }else{
            $response['status'] = 0;
            $response['message'] = 'Something worng.';
            return response()->json($response, 401);
        }
    }


    public function addAddress(Request $request){
        $userData = auth()->user();
        $userId =  $userData->id;
        $validator = Validator::make($request->all(), [           
            'address'=>'required',
            'longitude'=>'required',
            'latitude' => 'required',
            'address_type'=>'required',
            'state'=>'required',
            'city'=>'required',
            ]);
        if ($validator->fails()) 
        {
            $errors 	=	$validator->errors();
            $response['status'] = 0;
            $response['message'] = $errors;
            return response()->json($response, 422);
        }else{
            if(!empty($userId)){
                $inputs = [
                    'longitude' => $request->longitude,
                    'latitude' => $request->latitude,
                    'address_type'=>$request->address_type,
                    'state'=>$request->state,
                    'city'=>$request->city,
                    'building_number' =>$request->building_number,
                    'street_line_1' =>$request->street_line_1,
                    'street_line_2' =>$request->street_line_2,
                ];
                $address = '';
                if($request->has('address_type')){
                    $address .= $request->address_type.', ';
                }
                if($request->has('building_number')){
                    $address .= $request->building_number.', ';
                }
                if($request->has('street_line_1')){
                    $address .= $request->street_line_1.', ';
                }
                if($request->has('street_line_2')){
                    $address .= $request->street_line_2.', ';
                }
                if($request->has('city')){
                    $address .= $request->city.', ';
                }
                if($request->has('state')){
                    $address .= $request->state;
                }
                $inputs['address'] = $address;
                if(array_key_exists('address_id',$request->all())){
                    $address_id  = $request->address_id;
                    $inputs['user_id'] = $userId;
                    $userAdderssList = UsersAddress::findOrFail($address_id);
                    if($userAdderssList->update($inputs)){
                        $response['status'] = 1;
                        $response['message'] = 'Address Updated Successfully.';
                        return response()->json($response, 201);
                    }else{
                        $response['status'] = 0;
                        $response['message'] = 'Address Can`t be Updated.';
                        return response()->json($response, 401);
                    }
                }else{
                    $inputs['user_id'] = $userId;
                    $userAdderss = UsersAddress::create($inputs);
                    if($userAdderss){
                        $response['status'] = 1;
                        $response['message'] = 'Address Added Successfully.';
                        return response()->json($response, 201);
                    }else{
                        $response['status'] = 0;
                        $response['message'] = 'Address Can`t be added.';
                        return response()->json($response, 401);
                    }
                }
            }else{
                $response['status'] = 0;
                $response['message'] = 'Something worng.';
                return response()->json($response, 401);
            }
        }
        
    }

    public function deleteAddress(Request $request){
        $userData = auth()->user();
        $userId =  $userData->id;
        if(!empty($userId)){
            if(array_key_exists('address_id',$request->all())){
                $address_id  = $request->address_id;
               $addressDetails =  UsersAddress::where('id', $address_id)->first();
            
                if(!empty($addressDetails)){
                    UsersAddress::where('id', $address_id)->where('user_id', $userId)->delete();
                    $defaultAddress  = UsersAddress::where('user_id', $userId)->get()->count();
                    $inp = ['address' => '','latitude'=>'','longitude'=>''];
                    if($defaultAddress == 0){
                        $User = User::findOrFail($userId);
                        $User->update($inp);      
                    }
                    $response['status'] = 1;
                    $response['message'] = 'Address deleted Successfully.';
                    return response()->json($response, 201);
                }else{
                    $response['status'] = 0;
                    $response['message'] = 'Address can`t deleted.';
                    return response()->json($response, 201);
                }
                
            }else{
                $response['status'] = 0;
                $response['message'] = 'Error Occured.';
                return response()->json($response, 401);
            }
            
        }else{
            $response['status'] = 0;
            $response['message'] = 'Something worng.';
            return response()->json($response, 401);
        }
        
        
    }


    public function defaultAddress(Request $request){
        $userData = auth()->user();
        $userId =  $userData->id;
        if(!empty($userId)){
            if(array_key_exists('address_id',$request->all())){
                $address_id  = $request->address_id;
               $addressDetails =  UsersAddress::where('id', $address_id)->where('user_id', $userId)->first();
             
                if(!empty($addressDetails)){
                    $inputs = [
                        'address'=> $addressDetails->address,
                        'latitude'=>$addressDetails->latitude,
                        'longitude' =>$addressDetails->longitude
                    ];
                    $usersList = User::findOrFail($userId);
                    $usersList->update($inputs);
                    $response['status'] = 1;
                    $response['message'] = 'Default Address set Successfully.';
                  
                    $response['data'] = $inputs;
                    
                    return response()->json($response, 201);
                }else{
                    $response['status'] = 0;
                    $response['message'] = 'Address can`t be set.';
                    return response()->json($response, 201);
                }
                
            }else{
                $response['status'] = 0;
                $response['message'] = 'Error Occured.';
                return response()->json($response, 401);
            }
            
        }else{
            $response['status'] = 0;
            $response['message'] = 'Something worng.';
            return response()->json($response, 401);
        }
    }

    public function getAllCategory(Request $request){
        $serachData = $request->all();
        $limit =  'All';
        $getCategory =$this->getCategory($serachData,$limit);

        
        if(count($getCategory)){
            $data['category'] = $getCategory;
        }else{
            $data['category'] = [];
        }

        $response['status'] = 1;
        $response['data'] = $data;
        $response['imageBaseUrl'] = asset('uploads/category').'/';
        
        return response()->json($response, 200);
    }


    public function getAllDish(Request $request){
        $serachData = $request->all();
        $limit =  'All';
        $radius = 10;
        $getDish =$this->getDishLatAndLong($serachData,$limit,$radius);

        
        if(count($getDish)){
            $data['dish'] = $getDish;
        }else{
            $data['dish'] = [];
        }

        $response['status'] = 1;
        $response['data'] = $data;
        $response['imageBaseUrl'] = asset('uploads/product').'/';
        return response()->json($response, 200);
    }


    public function getAllCelebrityCategory(Request $request){
        $serachData = $request->all();
        $limit =  'All';
        $radius = 10;

        $getCelebrityCategory = $this->getCelebrityCategory($serachData);
        $serachData['category_id'] = $getCelebrityCategory[0]->id;
        
        $getCelebrityDetails =$this->getCelebrityDetail($serachData);
        $response['status'] = 1;
        $response['celebrityDetails'] = $getCelebrityDetails;
        $response['getCelebrityCategory'] = $getCelebrityCategory;
        $response['celebrityBaseUrl'] = asset('uploads/user').'/';
        return response()->json($response, 200);
    }

    public function getAllCelebrityDish(Request $request){
        $serachData = $request->all();
        $limit =  'All';
        $radius = 10;
        $response['product'] = $this->getDishLatAndLongCategory($serachData,$limit,$radius);
        $response['productBaseUrl'] = asset('uploads/product').'/';
        return response()->json($response, 200);
    }


    public function getCelebrityCategory($serachData){

        $productsList =  Category::select('categories.*')
            ->join('products','products.category_id','=','categories.id')
            ->groupBy('categories.id')
            ->where('products.is_active',1)
            ->where('products.celebrity_id', $serachData['celebrity_id']);
           

        if(array_key_exists("category_id",$serachData)){    
            $category_id = $serachData['category_id']; 
            $productsList->Where("products.category_id", $category_id);
        }
        if(array_key_exists("sort_by",$serachData)){
			$sort = $serachData['sort_by'];
			if($sort == A_TO_Z){  
				$productsList->orderBy('categories.name', 'asc');
			}else if($sort == Z_TO_A){
				$productsList->orderBy('categories.name', 'desc');
			}
        }
        $productsDetail = $productsList->get();
        return $productsDetail;
    }

    public function getAllCelebrity(Request $request){
        $serachData = $request->all();
        $limit =  'All';
        $getCelebrity =$this->getCelebrity($serachData,$limit);
        
        if(count($getCelebrity)){
            $data['celebrity'] = $getCelebrity;
        }else{
            $data['celebrity'] = [];
        }

        $response['status'] = 1;
        $response['data'] = $data;
        $response['imageBaseUrl'] = asset('uploads/user').'/';
        return response()->json($response, 200);
    }


    public function getAllGift(Request $request){
        $serachData = $request->all();
        $limit =  'All';
        $getGift =$this->getGift($serachData,$limit);
        
        if(count($getGift)){
            $data['gift'] = $getGift;
        }else{
            $data['gift'] = [];
        }

        $response['status'] = 1;
        $response['data'] = $data;
        $response['imageBaseUrl'] = asset('uploads/gift').'/';
        return response()->json($response, 200);
    }

    public function getAllGiftCategory(Request $request){
        $serachData = $request->all();
        $limit =  'All';
        $getGiftCategory =$this->getGiftCategory($serachData,$limit);
        
        if(count($getGiftCategory)){
            $data['giftCategory'] = $getGiftCategory;
        }else{
            $data['giftCategory'] = [];
        }

        $response['status'] = 1;
        $response['data'] = $data;
        $response['imageBaseUrl'] = asset('uploads/gift-category').'/';
        return response()->json($response, 200);
    }
    public function getCelebrityDetails(Request $request){
        $queryString = $request->all();
        $limit =  'All';
        $getCelebrity =$this->getCelebrityDetail($queryString);
        $getCategory = $this->getCelebrityCategory($queryString);
        $getDish = $this->getDish($queryString,$limit);
        
        if(count($getCelebrity)){
            $data['celebrity'] = $getCelebrity;
        }else{
            $data['celebrity'] = [];
        }

        $response['status'] = 1;
        $response['data'] = $data;
        $response['category'] = $getCategory;
        $response['dish'] = $getDish;
        $response['celebrityBaseUrl'] = asset('uploads/user').'/';
        $response['categoryBaseUrl'] = asset('uploads/category').'/';
        $response['dishBaseUrl'] = asset('uploads/product').'/';
        return response()->json($response, 200);
    }


    public function getDishDetails(Request $request){
        $serachData = $request->all();
        $getDish =$this->getDishDetail($serachData);

        
        if(count($getDish)){
            $data['dish'] = $getDish;
            $response['dishImageBaseUrl'] = asset('uploads/product').'/';
            $response['celebrityImageBaseUrl'] = asset('uploads/user').'/';
        }else{
            $data['dish'] = [];
        }

        $response['status'] = 1;
        $response['data'] = $data;
        return response()->json($response, 200);
    }
    
    public function getCelebrityDetail($queryString){
        
        $usersList = User::select('users.*')->where(['users.type'=>3,'users.status'=>1]);
       
        if(array_key_exists("celebrity_id",$queryString)){
            $celebrity_id = $queryString['celebrity_id'];
			$usersList->where("users.id",$celebrity_id);
        }
        $usersDetail = $usersList->get();

        return $usersDetail;
    }

    public function getCategory($serachData,$limit){
        $categoryList = Category::where(['type'=>1, 'status'=>1]);
        if(array_key_exists("search_text",$serachData)){
			$search = $serachData['search_text'];
			$categoryList->Where("categories.name",'like','%'.$search.'%');
        }
        if($limit != 'All'){
            $categoryList->limit($limit);
        }
        $categoryDetail= $categoryList->get();
        return $categoryDetail;
    }
    

    public function getGift($serachData,$limit){
        $giftList = Gift::where(['is_active'=>1]);
        if(array_key_exists("search_text",$serachData)){
			$search = $serachData['search_text'];
			$giftList->Where("gifts.name",'like','%'.$search.'%');
        }
        if($limit != 'All'){
            $giftList->limit($limit);
        }
        if(array_key_exists("category_id",$serachData)){
            $category_id = $serachData['category_id'];
			$giftList->where("gifts.category_id",$category_id);
        }
        if(array_key_exists("sort_by",$serachData)){
			$sort = $serachData['sort_by'];
			if($sort == A_TO_Z){  
				$giftList->orderBy('gifts.name', 'asc');
			}else if($sort == Z_TO_A){
				$giftList->orderBy('gifts.name', 'desc');
			}
        }
        $giftDetail= $giftList->get();
        return $giftDetail;
    }


    public function getGiftCategory($serachData,$limit){
        $giftCategoryList = Category::where(['type'=>2,'status'=>1]);
        if(array_key_exists("search_text",$serachData)){
			$search = $serachData['search_text'];
			$giftCategoryList->Where("categories.name",'like','%'.$search.'%');
        }
        if($limit != 'All'){
            $giftCategoryList->limit($limit);
        }
        $giftDetail= $giftCategoryList->get();
        return $giftDetail;
    }
    public function getCelebrity($serachData,$limit){
        
        $usersList = User::select('users.*','celebrity_categories.name as genres_name')->leftjoin('celebrity_categories','celebrity_categories.id','=','users.genres')->where(['users.type'=>3,'users.status'=>1]);
        
        if(array_key_exists("search_text",$serachData)){
			$search = $serachData['search_text'];
			$usersList->Where("users.name",'like','%'.$search.'%');
        }
        if($limit != 'All'){
            $usersList->limit($limit);
        }
        if(array_key_exists("sort_by",$serachData)){
			$sort = $serachData['sort_by'];
			if($sort == A_TO_Z){  
				$usersList->orderBy('users.name', 'asc');
			}else if($sort == Z_TO_A){
				$usersList->orderBy('users.name', 'desc');
			}
        }
        $usersDetail = $usersList->get();

        return $usersDetail;
    }

    public function getDish($serachData,$limit){
        $productsList = Products::with('User')->where('products.is_active',1);
       
        if(array_key_exists("search_text",$serachData)){    
            $search = $serachData['search_text']; 
			$productsList->Where("products.name",'like','%'.$search.'%');
        }

        if(array_key_exists("category_id",$serachData)){    
            $category_id = $serachData['category_id']; 
			$productsList->Where("products.category_id", $category_id);
        }

        if(array_key_exists("celebrity_id",$serachData)){    
            $celebrity_id = $serachData['celebrity_id']; 
			$productsList->Where("products.celebrity_id", $celebrity_id);
        }
        if(array_key_exists("sort_by",$serachData)){
			$sort = $serachData['sort_by'];
			if($sort == A_TO_Z){  
				$productsList->orderBy('products.name', 'asc');
			}else if($sort == Z_TO_A){
				$productsList->orderBy('products.name', 'desc');
			}
        }
        if($limit != 'All'){
            $productsList->limit($limit);
        }
        $productsDetail = $productsList->get();
        return $productsDetail;
    }

    public function getDishLatAndLong($serachData,$limit,$radius){
        $subQuery = "(select id, name, address, latitude, longitude ,
        ( 6371 * acos( cos( radians(".$serachData['latitude'].") ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(".$serachData['longitude'].")
        ) + sin( radians(".$serachData['latitude'].") ) * sin( radians( latitude ) ) ) ) AS distance from `users` WHERE type = 2 AND STATUS  = 1 HAVING `distance` < ".$radius." order by `distance` asc) t";

        $productsList =  Products::select('products.*','t.distance')->with('User')->with('Cart')
            ->join('product_assign_to_chef','product_assign_to_chef.product_id','=','products.id')
            ->join('users','users.id' ,'=', 'products.celebrity_id')
            ->join(DB::raw($subQuery),'t.id','=','product_assign_to_chef.chef_id')
            ->groupBy('products.id')
            ->where('products.is_active',1);
            
        
            if(array_key_exists("search_text",$serachData)){    
                $search = $serachData['search_text']; 
                $productsList->Where("products.name",'like','%'.$search.'%');
            }
    
            if(array_key_exists("category_id",$serachData)){    
                $category_id = $serachData['category_id']; 
                $productsList->Where("products.category_id", $category_id);
            }

            if(array_key_exists("celebrity_id",$serachData)){    
                $celebrity_id = $serachData['celebrity_id']; 
                $productsList->Where("products.celebrity_id", $celebrity_id);
            }
            if($limit != 'All'){
                $productsList->limit($limit);
            }
            $productsDetail = $productsList->get();
        return $productsDetail;
    }

    public function getDishLatAndLongCategory($serachData,$limit,$radius){
        $subQuery = "(select id, name, address, latitude, longitude ,
        ( 6371 * acos( cos( radians(".$serachData['latitude'].") ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(".$serachData['longitude'].")
        ) + sin( radians(".$serachData['latitude'].") ) * sin( radians( latitude ) ) ) ) AS distance from `users` WHERE type = 2 AND STATUS  = 1 HAVING `distance` < ".$radius." order by `distance` asc) t";

        $productsList =  Products::select('products.*','t.distance')->with('User')
            ->join('product_assign_to_chef','product_assign_to_chef.product_id','=','products.id')
            ->join('users','users.id' ,'=', 'products.celebrity_id')
            ->join(DB::raw($subQuery),'t.id','=','product_assign_to_chef.chef_id')
            ->groupBy('products.id')
            ->where('products.is_active',1);

        
            if(array_key_exists("search_text",$serachData)){    
                $search = $serachData['search_text']; 
                $productsList->Where("products.name",'like','%'.$search.'%');
            }
    
            if(array_key_exists("category_id",$serachData)){    
                $category_id = $serachData['category_id']; 
                $productsList->Where("products.category_id", $category_id);
            }

            if(array_key_exists("celebrity_id",$serachData)){    
                $celebrity_id = $serachData['celebrity_id']; 
                $productsList->Where("products.celebrity_id", $celebrity_id);
            }
            if($limit != 'All'){
                $productsList->limit($limit);
            }
            $productsDetail = $productsList->paginate(1);
        return $productsDetail;
    }

    private function findNearestChaf($serachData,$limit, $radius)
    {

        $subQuery = "(select id, name, address, latitude, longitude ,
                    ( 6371 * acos( cos( radians(".$serachData['latitude'].") ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(".$serachData['longitude'].")
                    ) + sin( radians(".$serachData['latitude'].") ) * sin( radians( latitude ) ) ) ) AS distance from `users` WHERE type = 2 AND STATUS  = 1 HAVING `distance` < ".$radius." order by `distance` asc) t";

        //        $test = "SELECT products.* from products 
        //        inner join product_assign_to_chef on (product_assign_to_chef.product_id = products.id) 
        //        inner join (select id, name, address, latitude, longitude ,
        //        ( 6371 * acos( cos( radians(26.922070) ) *
        //        cos( radians( latitude ) )
        //        * cos( radians( longitude ) - radians(75.778885)
        //        ) + sin( radians(26.922070) ) *
        //        sin( radians( latitude ) ) )
        //        ) AS distance from `users` WHERE type = 2 AND STATUS  = 1 HAVING `distance` < 10 order by `distance` asc
        // ) t ON (t.id =product_assign_to_chef.chef_id) 
        // group by products.id";

        $productsList =  Products::select('products.*','t.distance')->with('User')
            ->join('product_assign_to_chef','product_assign_to_chef.product_id','=','products.id')
            ->join('users','users.id' ,'=', 'products.celebrity_id')
            ->join(DB::raw($subQuery),'t.id','=','product_assign_to_chef.chef_id')
            ->groupBy('products.id')
            ->where('products.is_active',1);

        
            if(array_key_exists("search_text",$serachData)){    
                $search = $serachData['search_text']; 
                $productsList->Where("products.name",'like','%'.$search.'%');
            }
    
            if(array_key_exists("category_id",$serachData)){    
                $category_id = $serachData['category_id']; 
                $productsList->Where("products.category_id", $category_id);
            }
            if(array_key_exists("sort_by",$serachData)){
                $sort = $serachData['sort_by'];
                if($sort == A_TO_Z){  
                    $productsList->orderBy('products.name', 'asc');
                }else if($sort == Z_TO_A){
                    $productsList->orderBy('products.name', 'desc');
                }
            }
            if($limit != 'All'){
                $productsList->limit($limit);
            }
            $productsDetail = $productsList->get();

        // $restaurants = User::selectRaw("id, name, address, latitude, longitude ,
        //                 ( 6371 * acos( cos( radians(?) ) *
        //                 cos( radians( latitude ) )
        //                 * cos( radians( longitude ) - radians(?)
        //                 ) + sin( radians(?) ) *
        //                 sin( radians( latitude ) ) )
        //                 ) AS distance", [$latitude, $longitude, $latitude])
        //     ->where('status', '=', 1)
        //     ->having("distance", "<", $radius)
        //     ->orderBy("distance",'asc')
        //     ->limit(20)
        //     ->get();

        // echo '<pre>';
        // print_r(DB::getQueryLog());
        // die;
        return $productsList;
    }
    public function getDishDetail($queryString){
        $productsList = Products::with('User')->with('ProductImages')->with('ProductIngredients')->with('Cart')->with('Category')->where('products.is_active',1);
       
        if(array_key_exists("dish_id",$queryString)){    
            $dish_id = $queryString['dish_id']; 
			$productsList->Where("products.id",$dish_id);
        }

        $productsDetail = $productsList->get();
        return $productsDetail;
    }

}