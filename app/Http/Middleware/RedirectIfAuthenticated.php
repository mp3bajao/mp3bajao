<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        //dd(Auth::guard($guard));
        if (Auth::guard($guard)->check()) {
            if(Auth::user()->roles[0]->name=='Admin')
            {
                return redirect(RouteServiceProvider::BackPanel);                
            }
            else
            {
                return redirect(RouteServiceProvider::Home);
            }
        }
        return $next($request);
    }
}
