<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class AuthenticateBasicAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->header('PHP_AUTH_USER')  !='mp3bajao' && $request->header('PHP_AUTH_PW')!='mp3bajao@2020')
        {
             return response()->json('Unauthorized',401);  
        }
        else{
           return $next($request);            
        }

    }
}
