<?php

namespace App\Http\Middleware;
use Closure;
use Session,App;
use Illuminate\Http\Request;
class Locale {
   
    public function handle(Request $request, Closure $next) 
    {
        if (Session::has('locale')) 
        {
            App::setLocale(Session::get('locale'));
        }
        else 
        {
            $availableLangs = ['en','ar'];            
            $userLangs = preg_split('/,|;/', $request->server('HTTP_ACCEPT_LANGUAGE'));
            foreach ($availableLangs as $lang) 
            {
                if(in_array($lang, $userLangs)) 
                {                    
                    App::setLocale($lang);
                    Session::push('locale', $lang);
                    break;
                }
            }
        }
        return $next($request);
    }
}