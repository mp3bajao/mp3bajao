<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Playlist extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {       
        return [
            'id' => $this->id,
            'name' => $this->name, 
            'image' => $this->image, 
            'track_id' => $this->getSinglePlaylist->getTrack->id ?? null, 
            'song'  => $this->getSinglePlaylist->getTrack->getMedia->songs ?? null,
            'duration'  => $this->getSinglePlaylist->getTrack->getMedia->file_duration ?? null,
            'primary_artist' => $this->getSinglePlaylist->getTrack->primary_artist ?? null,
            'total_views'=>$this->total_views,
            'total_comment'=>$this->total_comment,
    
        ];
    }
}
