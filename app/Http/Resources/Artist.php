<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Artist extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => $this->image,
            'rating' => $this->rating,
            'is_like'=>$this->is_favorite,
            'total_like'=>$this->total_like,
            'follow'=>$this->follow,
            'unfollow'=>$this->unfollow,
            'is_follow'=>$this->is_follow,
            'total_views'=>$this->total_views ?? 0
        ];
    }
}
