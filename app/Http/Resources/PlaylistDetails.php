<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PlaylistDetails extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {       
        return [            
            'id' => $this->id,
            'name' => $this->title,
            'image' => $this->getRelease->album_profile ?? null,
            'song'  => $this->getMedia->songs ?? null,
            'duration'  => $this->getMedia->file_duration ?? null,
            'primary_artist' => $this->primary_artist ?? null,
        ];
    }
}
