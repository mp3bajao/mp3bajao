<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Release extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       
        return [
            'id' => $this->id,
            'name' => $this->title,
            'image' => $this->album_profile  ?? null,
            'track_id'  => $this->getTrack->id ?? null,
            'song'  => $this->getTrack->getMedia->songs ?? null,
            'duration'  => $this->getTrack->getMedia->file_duration ?? null,
            'primary_artist' => $this->getTrack->primary_artist ?? null,
            'format' => ($this->format=='Single') ? 'Track' : 'Album',
            'total_views'=>$this->total_views,
            'total_comment'=>$this->total_comment,
        ];
    }
}
