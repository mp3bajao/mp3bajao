<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => $this->image,
            'mobile' => $this->mobile,
            'country_code' => $this->country_code,
            'email' => $this->email,
            'referral_code' => $this->referral_code,
            'social_type' => $this->social_type,
            'social_id' => $this->social_id,
            'token'     => $this->token ?? null,
            'token_type'=>'Bearer',
            'total_notification'=>$this->total_notification 
        ];
    }
}
