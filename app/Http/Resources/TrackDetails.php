<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TrackDetails extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [            
            'id' => $this->id,
            'name' => $this->title,
            'image' => $this->getRelease->album_profile  ?? null,
            'song'  => $this->getMedia->songs  ?? null,
            'primary_artist' => $this->primary_artist  ?? null,
            'duration' => $this->getMedia->file_duration  ?? null,
            'composer' => $this->composer  ?? null,
            'featuring_artist' => $this->featuring_artist  ?? null,
            'remixer' => $this->remixer  ?? null,
            'author' => $this->author  ?? null,
            'composer' => $this->composer  ?? null,
            'arranger' => $this->arranger  ?? null,
            'producer' => $this->producer  ?? null,
            'publisher' => $this->publisher  ?? null,
            'author' => $this->author  ?? null,
            'duration' => $this->getMedia->file_duration ?? null , 
            'is_like' => $this->is_favorite  ?? null,
            'is_playlist' => $this->is_playlist  ?? null,    
            'rating' => $this->rating  ?? null,    
            'p_line' => $this->p_line  ?? null,  
            'total_views'=>$this->total_views,
            'total_comment'=>$this->total_comment,
        ];
    }
}
