<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TrackAlbum extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       
        return [            
            'id' => $this->getRelease->id,
            'name' => $this->title,
            'image' => $this->getRelease->album_profile  ?? null,
            'song'  => $this->getMedia->songs ?? null,
            'track_id'  => $this->id  ?? null,
            'primary_artist' => $this->primary_artist  ?? null,
            'instrumental' => $this->instrumental  ?? null,
            'duration' => $this->getMedia->file_duration ?? 0 ,
            'total_views'=>$this->total_views,
            'total_comment'=>$this->total_comment,
                //'featuring_artist' => $this->featuring_artist,
                //'remixer' => $this->remixer,
                //'remixer' => $this->author,
                //'composer' => $this->composer,
                //'arranger' => $this->arranger,
                //'producer' => $this->producer,
                //'publisher' => $this->publisher,
                //'author' => $this->author,
               
                //'duration' => $this->getMedia->file_duration , 
                //'is_favorite' => $this->is_favorite,
                //'is_playlist' => $this->is_playlist,    
                //'rating' => $this->rating,    
                //'p_line' => $this->p_line,    
                
            
        ];
    }
}
