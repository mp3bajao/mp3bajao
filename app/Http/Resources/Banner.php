<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Banner extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {       
        return [
            'id' => $this->id,
            'name' => $this->name,
            'chart_type' => $this->chart_type,
            'format' => $this->format,            
            'track_id' => $this->upc,            
            'track_id' => $this->upc,            
            'image' => $this->image,            
        ];
    }
}
