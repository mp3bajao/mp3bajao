<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Models\Products;
use App\Models\UserDevice;
use App\Models\Media;
use App\Models\CelebrityCategory;
use App\Models\Country;

class User extends Authenticatable implements MustVerifyEmail, JWTSubject  {
	
    use Notifiable;
    use HasRoles;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
    protected  $appends = ['country'];
    protected $fillable = [
    	'id','name', 'email', 'password','type','first_name','last_name','gender','mobile','address','country_code','status','latitude','longitude','social_type','social_id','parent_chef_id'
    ];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
    protected $hidden = [
    	'password', 'remember_token',
    ];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
    protected $casts = [
    	'email_verified_at' => 'datetime',
    ];

	/**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier() {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims() {
        return [];         
    }
	
	
    public function devices()
    {
        return $this->hasMany('App\Models\UserDevice','user_id');
    }
    public function getImageAttribute($value) 
    {
      return file_checker($value,'user');  
    }
    public function getCountryAttribute($value) 
    {
        $countryName=Country::select('name')->where(['phonecode'=>$this->country_code])->first();
            if($countryName){
                $countryName = $countryName->name;
            }
        return $countryName;
    }
}


