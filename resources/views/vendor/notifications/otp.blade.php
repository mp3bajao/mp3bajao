
  <div style="padding-left: 1.5rem;padding-right: 1.5rem;width: 700px;padding-right: .75rem;padding-left: .75rem;margin-right: auto;margin-left: auto;">
   
    <!-- Outer Row -->
    <div style="justify-content: center!important;display: flex;flex-wrap: wrap; margin-right: -.75rem;margin-left: -.75rem;">

      <div style=" flex: 0 0 41.66667%;">

        <div style="overflow: hidden!important;margin-bottom: 3rem!important;margin-top: 3rem!important;box-shadow: 0 1rem 3rem rgba(0,0,0,.175)!important;border: 0!important;position: relative; display: flex;flex-direction: column;min-width: 0; word-wrap: break-word;background-color: #fff;  background-clip: border-box; border: 1px solid #e3e6f0;border-radius: 0rem;">
          <div style="    padding: 0!important;flex: 1 1 auto;box-sizing: border-box;">
            <!-- Nested Row within Card Body -->
            <div style="display: flex;flex-wrap: wrap;margin-right: -.75rem;margin-left: -.75rem;">
              <div class="col-lg-12 d-none d-lg-block "></div>
              <div class="col-lg-12">
                <div style=" padding: 3rem!important;">
                  <div style="text-align: center!important;">
                     <img src="{{ URL::asset('assets/images/logo2.png')}}" width="40%"/>
                      <hr style="margin-top: 1rem;margin-bottom: 1rem;border: 0;border-top: 1px solid rgba(0,0,0,.1);"/>
                    <h1 style="color: #3a3b45!important; margin-bottom: 1.5rem!important;font-size: 1.5rem;font-weight: 400;line-height: 1.2;margin-top: ">Welcome To Mp3bajao ! </h1>
                    <p style=" margin-top: 0;margin-bottom: 1rem;font-size: 0.8rem;font-weight: 400;line-height: 1.5;color: #858796;text-align: center;">You'll need to verify you OTP number</p>      
                     <br/> <br/>
                    <div style="border: 2px dashed #ccc;font-size: 24px;padding: 5px;text-align: center;color: #0000009c;font-weight: 700;">{{$otp}}</div>
                  </div><br/>
                   <br/>                    
                  <hr style="margin-top: 1rem;margin-bottom: 1rem;border: 0; border-top: 1px solid rgba(0,0,0,.1);">
                  <div style="text-align: center!important;">
                        <p style="font-size: 80%; font-weight: 400;    line-height: 1.5; color: #858796;">©  Mp3bajao.com . {{date('Y')}}</p>
                  </div>                  
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>
    