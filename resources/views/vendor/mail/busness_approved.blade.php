  <div style="padding-left: 1.5rem;padding-right: 1.5rem;width: 700px;padding-right: .75rem;padding-left: .75rem;margin-right: auto;margin-left: auto;">
    <!-- Outer Row -->
    <div style="justify-content: center!important;display: flex;flex-wrap: wrap; margin-right: -.75rem;margin-left: -.75rem;">

      <div style=" flex: 0 0 41.66667%;">

        <div style="overflow: hidden!important;margin-bottom: 3rem!important;margin-top: 3rem!important;box-shadow: 0 1rem 3rem rgba(0,0,0,.175)!important;border: 0!important;position: relative; display: flex;flex-direction: column;min-width: 0; word-wrap: break-word;background-color: #fff;  background-clip: border-box; border: 1px solid #e3e6f0;border-radius: 0rem;">
          <div style="    padding: 0!important;flex: 1 1 auto;box-sizing: border-box;">
            <!-- Nested Row within Card Body -->
            <div style="display: flex;flex-wrap: wrap;margin-right: -.75rem;margin-left: -.75rem;">
              <div class="col-lg-12 d-none d-lg-block "></div>
              <div class="col-lg-12">
                <div style=" padding: 3rem!important;">
                  <div style="text-align: center!important;">
                     <img src="{{ URL::asset('assets/images/logo.png')}}" width="40%"/>
                      <hr style="margin-top: 1rem;margin-bottom: 1rem;border: 0;border-top: 1px solid rgba(0,0,0,.1);"/>
                    <h1 style="color: #3a3b45!important; margin-bottom: 1.5rem!important;font-size: 1.5rem;font-weight: 400;line-height: 1.2;margin-top: ">Welcome To Espitalia ! </h1>
                    <p style=" margin-top: 0;margin-bottom: 1rem;font-size: 0.8rem;font-weight: 400;line-height: 1.5;color: #858796;text-align: center;">{!! ucwords($data['template']->name) !!}</p>      
                     <br/> <br/>
                     <div style="border: 2px dashed #ccc; padding: 5px;text-align: center;color: #0000009c;font-weight: 400;">
                         <img src="{{$data['user']->profile_image_thumb}}" alt="" style="width: 100px; max-width: 600px; height: auto; margin: auto; display: block; border-radius: 60px;">
                         <br/>
                         Dear {{ucwords($data['user']->name)}}<br/>{{$data['template']->subject}}<br/><p>{!! $data['template']->description !!}</p>                         
                     </div>
                  </div><br/>
                   <br/>                    
                  <hr style="margin-top: 1rem;margin-bottom: 1rem;border: 0; border-top: 1px solid rgba(0,0,0,.1);">
                  <div style="text-align: center!important;">
                        <p style="font-size: 80%; font-weight: 400;    line-height: 1.5; color: #858796;">©  espitalia. {{date('Y')}}</p>
                  </div>                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php /* ?>
@extends('layouts.frontend.master-nav')
@section('content')
    <tr>
          <td valign="middle" class="hero bg_white" style="padding: 2em 0 4em 0;">
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
            	<tr>
            		<td style="padding: 0 2.5em; text-align: center; padding-bottom: 3em;">
            			<div class="text">
            				<h2>{!! ucwords($data['template']->name) !!}</h2>
            			</div>
            		</td>
            	</tr>
            	<tr>
			          <td style="text-align: center;">
			          	<div class="text-author">
				          	<img src="images/person_2.jpg" alt="" style="width: 100px; max-width: 600px; height: auto; margin: auto; display: block;">
				          	<h3 class="name"><p>Dear {{ucwords($data['user']->name)}},</p></h3>
				          	<span class="position">{{$data['template']->subject}}</span>
				           	<p>{!! $data['template']->description !!}</p>
			           	</div>
			          </td>
			        </tr>
            </table>
          </td>
	      </tr><!-- end tr -->

@endsection*/