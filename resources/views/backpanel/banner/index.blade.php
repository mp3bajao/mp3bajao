@extends('layouts.master')
@section('content')


<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}"> 
<!-- Content Header (Page header) -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">{{ __('backend.'.$lang).' '.__('backend.manager') }}</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">{{ __('backend.home') }}</a></li>
                <li class="breadcrumb-item active">{{ __('backend.'.$lang).' '.__('backend.manager') }}</li>
            </ol>
          
              <a href="#" class="btn btn-primary btn-xs d-none d-lg-block m-l-15" title="{{ __('backend.add_title_') }}" data-toggle="modal" data-target="#add_modal" ><i class="fa fa-plus"></i> {{ __('backend.add') }}</a>
         
        </div>
    </div>
</div>
<!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
    <div class="row">
        <div class="col-md-12">
        <div class="card card-primary card-outline">
                <div class="card-body">
                    <div class="table-responsive">
                    <table  id="category_listing" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>{{ __('backend.sr_no') }}</th>
                                <th>{{ __('backend.image') }}</th>
                                <th>{{ __('backend.name') }}</th>
                                <th>{{ __('backend.type') }}</th>
                                <th>{{ __('backend.status') }}</th>  
                                <th>{!! __('backend.created_at') !!}</th>
                                <th>{{ __('backend.rank') }}</th>
                                <th>{{ __('backend.action') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
    </div>
</div>

</div>
    <!-- /.content -->

<!-- Modals -->

<div class="modal fade" id="add_modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <form method="POST" action="{{ routeUser($page.'.create') }}" id="add_category">
    @csrf
      <div class="modal-header">
        <h4 class="modal-title">{{ __('backend.add').' '.__('backend.'.$lang) }}</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="tab-content" style="margin-top:10px">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label class="control-label" for="name"> {{__('backend.name')}}*</label>
            <input type="text" name="name" value="" id="name" class="form-control" placeholder=" Name"  />
          </div>
          <div class="form-group">
            <label class="col-md-12" for="image">Image</label>
            <input type="file" id="file" name="image" class="form-control">
            <div id="image_preview"><img height="100" width="100" id="previewing" src="{{ URL::asset('images/image.png')}}"></div>
          </div>
        </div>
          <div class="col-md-6">
             <div class="form-group">
                  <label class="col-md-12" for="image">Type</label>
                 <select class="form-control album_type" name="album_type">
                    <option value="SINGLE">SINGLE</option>
                    <option value="MULTIPLE">MULTIPLE</option>
                </select>
             </div>
            <div class="col-md-12 multileAlbums" style="display:none">
                  <div class="form-group">
                    <input type="text" placeholder="Search Playlist" class="form-control searchplaylist">
                    <div class="col-md-12 showsearchplaylist" style="overflow-y: auto; max-height: 500px;"></div>
                </div>
            </div>                                                               
            <div class="col-md-12 upcsearch">
                <div class="form-group">
                    <input type="text" placeholder="Search Album, UPC, Song, Atrist" class="form-control searchbox">
                    <div class="col-md-12 searchList" style="overflow-y: auto; max-height: 500px;"></div>
                </div>
            </div>
        </div> 
      </div>
      </div>
          
      <div class="row">
        
      </div>
            
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary  btn-xs save"><span class="spinner-grow spinner-grow-sm formloader" style="display: none;" role="status" aria-hidden="true"></span> Save</button>
      </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="editModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">{{ __('backend.edit').' '.__('backend.'.$lang) }}</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div id="edit_category_response"></div>  
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="viewModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">{{ __('backend.view') .' '.__('backend.'.$lang) }}</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div id="view_response"></div>  
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/parsley.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
var ajax_datatable;
$(document).ready(function(){
$('#add_category').parsley();
ajax_datatable = $('#category_listing').DataTable({
    processing: true,
    serverSide: true,
    ajax: '{{ routeUser($page.'.index') }}',
    columns: [
      { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
      { data: 'image', name: 'name' },
      { data: 'name', name: 'name' },
      { data: 'chart_type', name: 'chart_type' },
      { data: 'status', name: 'status' },
      { data: 'created_at', name: 'created_at' },
      { data: 'is_top', name: 'is_top' },
      {data: 'id', name: 'id', orderable: false, searchable: false}
    ],
    order: [ [5, 'desc'] ],
    rowCallback: function(row, data, iDisplayIndex) {  
      
      var links='';
      links += `<div class="btn-group" role="group" >`;
     
      links += `<a href="#" data-category_id="${data.id}" title="Edit Details" class="btn btn-primary btn-xs edit_category" ><span class="fa fa-edit"></span></a>`;
      
     var image =`<img src="${data.image}" width="100px">`; 
     
      //links += `<a href="#" data-category_id="${data.id}" title="Delete category" class="btn btn-danger btn-xs delete_category " ><span class="fa fa-trash"></span></a>`;
      var rank =`<input type="number" class="form-control changeArtistRank" data-id="${data.id}" min="0" value="${data.is_top}">`;
      
      links += `</div>`;
      var status = '';
      if(data.status === 1){
        status += `<a href="#" data-category_id="${data.id}" title="{{__('backend.active_category')}}" data-status="deactive" class="change_status"><span class='label label-rounded label-success'>{{__('backend.active')}}</span></a>`;
      }else{
        status += `<a href="#" data-category_id="${data.id}" title="{{__('backend.deactive_category')}}" data-status="active" class="change_status"><span class='label label-rounded label-warning'>{{__('backend.deactive')}}</span></a>`;
      }
      $('td:eq(1)', row).html(image);
      $('td:eq(4)', row).html(status);
      $('td:eq(6)', row).html(rank);
      $('td:eq(7)', row).html(links);
      },
});



$(document).on('change','.album_type',function(){
   var type =  $(this).val();
   if(type == 'MULTIPLE')
   {
      $('.multileAlbums').show(); 
      $('.upcsearch').hide();
   }
   else
   {
      $('.multileAlbums').hide(); 
      $('.upcsearch').show();
   }
     searchAlbum(); 
});

$(document).on('keyup','.searchbox',function(){
    searchAlbum();   
});


$(document).on('keyup','.searchEditbox',function(){
    searchEditAlbum();   
});

function searchAlbum()
{
    if($('.searchbox').val().length >= 3)
    {
        var serching = $('.searchbox').val();
        var chart_id = $('.chart_type option:selected').val();
        var chart_type ='Music';
        var language = $('.language option:selected').val();
        $.ajax({
          type: 'get',
          data: {serching:serching,language:language,chart_type:chart_type,chart_id:chart_id},
          dataType:'html',
          url: "{{ routeUser($page.'.search') }}",
          success:function(res)
          {
              $('.searchList').html(res);

          },   
          error:function(jqXHR,textStatus,textStatus){
            console.log(jqXHR);
            toastr.error(jqXHR.statusText)
          }
      });
    }
}
function searchEditAlbum()
{
    var serching = $('.searchEditbox').val();      
    if($('.searchEditbox').val().length >= 3)
    {
        var serching = $('.searchEditbox').val();
        var chart_id = $('.chart_type option:selected').val();
        var chart_type ='Music';
        var language = $('.language option:selected').val();
        $.ajax({
          type: 'get',
          data: {serching:serching,language:language,chart_type:chart_type,chart_id:chart_id},
          dataType:'html',
          url: "{{ routeUser($page.'.search') }}",
          success:function(res)
          {
              $('.searchEditList').html(res);
          },   
          error:function(jqXHR,textStatus,textStatus){
            console.log(jqXHR);
            toastr.error(jqXHR.statusText)
          }
      });
    }
}

$(document).on('click','.change_status',function(e){
      e.preventDefault();
      status = $(this).data('status');
      if(status == 'active'){
        var response = confirm('Are you sure want to active?');
      }else{
        var response = confirm('Are you sure want to deactive?');
      }
      if(response){
        id = $(this).data('category_id');
        $.ajax({
          type: 'post',
          data: {_method: 'get', _token: "{{ csrf_token() }}"},
          dataType:'json',
          url: "{!! routeUser($page.'.status')!!}" + "/" + id +'/'+status,
          success:function(res){
            if(res.status === 1){ 
              toastr.success(res.message);
              ajax_datatable.draw();
            }else{
              toastr.error(res.message);
            }
          },   
          error:function(jqXHR,textStatus,textStatus){
            console.log(jqXHR);
            toastr.error(jqXHR.statusText)
          }
      });
      }
      return false;
    }); 
    
    $("#add_category").on('submit',function(e){
      e.preventDefault();
      var _this=$(this); 
        var formData = new FormData(this);
        $.ajax({
            url:'{{ routeUser($page.'.create')}}',
            dataType:'json',
            data:formData,
            type:'POST',
            cache:false,
            contentType: false,
            processData: false,
            beforeSend: function (){before(_this)},
            // hides the loader after completion of request, whether successfull or failor.
            complete: function (){complete(_this)},
            success:function(res){
                  if(res.status === 1){ 
                    $('#add_modal').modal('hide') ; 
                    toastr.success(res.message);
                    $('#add_category')[0].reset();
                    $('#previewing').attr('src','images/image.png');
                    $('#add_category').parsley().reset();
                    ajax_datatable.draw();
                  }else{
                    toastr.error(res.message);
                  }
              },
            error:function(jqXHR,textStatus,textStatus){
              if(jqXHR.responseJSON.errors){
                $.each(jqXHR.responseJSON.errors, function( index, value ) {
                  toastr.error(value)
                });
              }else{
                toastr.error(jqXHR.responseJSON.message)
              }
            }
          });
          return false;   
        });

    //Edit staff
    $(document).on('click','.view_btn',function(e){
        e.preventDefault();
        $('#view_response').empty();
        id = $(this).attr('data-category_id');
        $.ajax({
           url:'{{ routeUser($page.'.view')}}/'+id,
           dataType: 'html',
           success:function(result)
           {
            $('#view_response').html(result);
           } 
        });
        $('#viewModal').modal('show');
     });

    //Edit staff
    $(document).on('click','.edit_category',function(e){
        e.preventDefault();
        $('#edit_category_response').empty();
        id = $(this).attr('data-category_id');
        $.ajax({
           url:'{{ routeUser($page.'.edit')}}/'+id,
           dataType: 'html',
           success:function(result)
           {
            $('#edit_category_response').html(result);
           } 
        });
        $('#editModal').modal('show');
    });
     
     
    $(document).on('keyup','.changeArtistRank',function(){
        var values = $(this).val();
        var id = $(this).data('id');
        $.ajax({
            type: 'get',
            data: {values:values,id:id},
            dataType:'html',
              url: "{{ routeUser($page.'.add_banner_rank') }}",
            success:function(res){
             // ajax_datatable.draw();
            },   
            error:function(jqXHR,textStatus,textStatus){
              console.log(jqXHR);
              toastr.error(jqXHR.statusText)
            }
        });
    }); 



    $(document).on('click','.delete_category',function(e){
      e.preventDefault();
      var response = confirm('Are you sure want to delete ?');
      if(response){
        id = $(this).data('category_id');
        $.ajax({
            type: 'post',
            data: {_method: 'delete', _token: "{{ csrf_token() }}"},
            dataType:'json',
            url: "{!! routeUser($page.'.destroy')!!}" + "/" + id,
            success:function(res){
              if(res.status === 1){ 
                  toastr.success(res.message);
                  ajax_datatable.draw();
                }else{
                  toastr.error(res.message);
                }
            },   
            error:function(jqXHR,textStatus,textStatus){
              console.log(jqXHR);
              toastr.error(jqXHR.statusText)
            }
        });
      }
      return false;
    }); 
    
    $("#file").change(function(){
        var fileObj = this.files[0];
        var imageFileType = fileObj.type;
        var imageSize = fileObj.size;

        var match = ["image/jpeg","image/png","image/jpg"];
        if(!((imageFileType == match[0]) || (imageFileType == match[1]) || (imageFileType == match[2]))){
          $('#previewing').attr('src','images/image.png');
          toastr.error('Please Select A valid Image File <br> Note: Only jpeg, jpg and png Images Type Allowed!!');
          return false;
        }else{
          //console.log(imageSize);
          if(imageSize < 1000000){
            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(this.files[0]);
          }else{
            toastr.error('Images Size Too large Please Select 1MB File!!');
            return false;
          }
        }
      });
    });

    function imageIsLoaded(e){
	//console.log(e);
	$("#file").css("color","green");
	$('#previewing').attr('src',e.target.result);
    }

    $(document).on('keyup','.searchplaylist',function(){
        if($('.searchplaylist').val().length >= 3)
        {
            mutipCharge();
        }
    });
    
    mutipCharge();
    function mutipCharge(){
        var search = $('.searchplaylist').val();        
            $.ajax({
              type: 'get',
              data: {search:search},
              dataType:'html',
                url: "{{ routeUser('mulitchartbanner.list') }}",
              success:function(res){
                $('.showsearchplaylist').html(res);
              },   
              error:function(jqXHR,textStatus,textStatus){
                console.log(jqXHR);
                toastr.error(jqXHR.statusText)
              }
        });
    }
    
    $(document).on('click',"input[name=upc]",function(){       
        $('.music_format').attr('value',$(this).data('format'));
    });
    
</script>
@endsection
