<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">                   


<table  id="listing" class="table table-striped table-bordered" style="width:100%">
    <thead>
        <tr>
            <th>#</th>    
            <th>{{ trans('backend.track') }}</th>
            <th>{{ trans('backend.artist') }}</th>
            <th>{{ trans('backend.album_name')}}</th>            
            <th>{{ trans("backend.action") }}
            <th>{{ trans('backend.listen')}}</th>
        </tr>
    </thead>
    <tbody>

    </tbody>
</table>

        
<div class="modal" id="myModal" >
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
              <h4 class="modal-title">Track Assets Metadata</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
      <!-- Modal body -->
      <div class="modal-body">
          <div class="modal-body-content colspace_col">
              
          </div>
      </div>
    </div>
  </div>
                   
</div>
@section('script')    
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/parsley.min.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>

<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>

<script>
var ajax_datatable;
    ajax_datatable = $('#listing').DataTable({
        processing: true,
        serverSide: true,
        ajax:{
          url:'{{ routeUser("playlist.edit_index") }}',
          data: function (d) {
            return $.extend({}, d, {
                "playlist_type_id":"{{$data->id}}",
            });
          }
        },
        columns: [
          { data: 'DT_RowId', name: 'DT_RowId', orderable: false, searchable: false, "width": "20px"},
          { data: 'title', name: 'title' },
          { data: 'track_type', name: 'track_type' },
          { data: 'album_name', name: 'album_name' },
          { data: 'checked', name: 'checked' },
          { data: 'actions', name: 'actions' },
        ],
        order: [ [0, 'desc'] ],
    });

    $('.release_id').on('change', function(e) {
          ajax_datatable.draw();
            e.preventDefault();
    });

    $('#refresh').click(function(){
        $('#chef_id').val('');
        $('#celebrity_id').val('');
        ajax_datatable.draw();
    }); 

 
  

</script>

@endsection
