@extends('layouts.master')
@section('content')
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}"> 
<!-- Content Header (Page header) -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">{{ __('backend.'.$lang).' '.__('backend.manager') }}</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">{{ __('backend.home') }}</a></li>
                <li class="breadcrumb-item active">{{ __('backend.'.$lang).' '.__('backend.manager') }}</li>
            </ol>
          
             
        </div>
    </div>
</div>
<!-- /.content-header -->
<!-- Main content -->
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-outline">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form method="POST" action="{{  routeUser($page.'.edit',[$data->id]) }}" id="edit_role" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-5">
                                  <div class="form-group">
                                    <label class="control-label" for="name"> {{__('backend.name')}} *</label>
                                    <input type="text" name="name" value="{{$data->name}}" id="name" class="form-control" placeholder="Name"  />
                                  </div>
                                </div>                                 
                                <div class="col-md-5">
                                  <div class="form-group">
                                    <label class="col-md-12" for="image">Image</label>
                                    <input type="file" id="editfile" name="image" class="form-control">

                                  </div>
                                </div>
                                <div class="col-md-1">
                                    <label class="col-md-12" for="image">&nbsp;</label>
                                    <div id="image_preview"><img height="40" width="40" id="editpreviewing" src="{{$data->image}}"></div>
                                </div>
                                <div class="col-md-1">
                                    <label class="col-md-12" for="image">&nbsp;</label>
                                    <button type="submit" class="btn btn-primary save"><span class="spinner-grow spinner-grow-sm formloader" style="display: none;" role="status" aria-hidden="true"></span> Save</button>                                    
                                </div>
                            </div> 
                            <hr style="margin: 1em -15px">
                        </form>
                    </div>
                        
                </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" placeholder="Search Album, UPC, Song, Atrist" class="form-control searchbox">                                   
                            </div>
                        <div class="searchList" style="overflow-y: auto; max-height: 500px;"></div>                            
                        </div>
                        <div class="col-md-8">
                            <div class="table-responsive"> 
                                @include('backpanel.'.$page.'.table')                    
                            </div>
                            @include('layouts.audio-player' )                            
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/parsley.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
var ajax_datatable;
$(document).ready(function(){

    
    $(document).on('keyup','.searchbox',function(){
        searchAlbum();   
    });

    function searchAlbum(){
        if($('.searchbox').val().length >= 3)
        {
            var serching = $('.searchbox').val();
            var chart_type = "Home";
            var chart_id = $('.chart_type option:selected').val();
            var album_type = $('.album_type option:selected').val();
            var album_id = $('.album_id option:selected').val();

            $.ajax({
              type: 'get',
              data: {serching:serching,chart_type:chart_type,chart_id:chart_id,album_type:album_type,album_id:album_id},
              dataType:'html',
              url: "{{ routeUser($page.'.search') }}",
              success:function(res)
              {
                  $('.searchList').html(res);
              },   
              error:function(jqXHR,textStatus,textStatus){
                console.log(jqXHR);
                toastr.error(jqXHR.statusText)
              }
          });
        }
    }
    
    $(document).on('click','.add_top_list',function(){
        var values = $(this).val(); 
        var playlist_type_id = "{{$data->id}}";
        var chart_type = 'Playlist';
        if($(this).is(':checked'))
        {
            var type = 'true';    
        }
        else
        {
            var type = 'false';    
        }
    
        $.ajax({
            type: 'get',
            data: {type:type,values:values,playlist_type_id:playlist_type_id},
            dataType:'html',
            url: "{{ routeUser($page.'.add_playlist_list') }}",
            success:function(res){
             ajax_datatable.draw();
            e.preventDefault();
            },   
            error:function(jqXHR,textStatus,textStatus){
              console.log(jqXHR);
              toastr.error(jqXHR.statusText)
            }
        });
    });

    $("#file").change(function(){
        var fileObj = this.files[0];
        var imageFileType = fileObj.type;
        var imageSize = fileObj.size;

        var match = ["image/jpeg","image/png","image/jpg"];
        if(!((imageFileType == match[0]) || (imageFileType == match[1]) || (imageFileType == match[2]))){
          $('#previewing').attr('src','images/image.png');
          toastr.error('Please Select A valid Image File <br> Note: Only jpeg, jpg and png Images Type Allowed!!');
          return false;
        }else{
          //console.log(imageSize);
          if(imageSize < 1000000){
            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(this.files[0]);
          }else{
            toastr.error('Images Size Too large Please Select 1MB File!!');
            return false;
          }
        }
      });
    });

    function imageIsLoaded(e){
	//console.log(e);
	$("#file").css("color","green");
	$('#previewing').attr('src',e.target.result);
    }
</script>
@endsection
