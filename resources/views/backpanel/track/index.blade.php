@extends('layouts.master')
@section('content')

<!-- Content Header (Page header) -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">{{ __('backend.'.$lang).' '.__('backend.manager') }}</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">{{ __('Home') }}</a></li>
                <li class="breadcrumb-item active">{{ __('backend.'.$lang).' '.__('backend.manager') }}</li>
            </ol>

        </div>
    </div>
</div>
<!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
    <div class="row">
        <div class="col-md-12">
        <div class="card card-primary card-outline">
                <div class="card-body">
                  
                    <div class="table-responsive"> 
                        {{ Form::select('release_id',$release,null,['class'=>'form-control release_id','placeholder'=>'---'. ucwords(trans("backend.select").' '.trans("backend.album")).'---','style'=>'text-transform: capitalize;']) }}
                        @include('backpanel.'.$page.'.table')                    
                    </div>
                      @include('layouts.audio-player' )
                </div>
            </div>
    </div>
</div>
</div>
    
@endsection