    {!! Form::model($sdata,  ['route'=>[routeFormUser($page.'.'.$form_type),encrypt($sdata->id),$type], 'method'=>'post','files'=>true,'class'=>'upload_submit_track']) !!}

<input type="hidden" name="route_url" class="route_url" value="{{ routeUser($page.'.'.$form_type,['id'=>encrypt($sdata->id),'type'=>$type]) }}">
<input type="hidden" name="track_id_main" value="{{$sdata->id}}">
<div class="row">
    <div class="col-sm-12">
        <div class="bg box box-default">             
            <div class="row">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                   <label for="title" class="control-label required">{{ trans('backend.primary_track_type') }}</label>
                   <div>{{ Form::radio('track_type',$sdata->track_type, true) }} {{ucwords(strtolower($sdata->track_type))}}</div>                 
                </div>   
                <div class="form-group">
                   <label for="title" class="control-label required">{{ trans('backend.secondary_track_type') }}</label>
                   <div class="row">
                       <div class="col-md-2">{{ Form::radio('secondary_track_type','original', !empty($sdata->secondary_track_type)?($sdata->secondary_track_type=='original')?true:false:true) }} Original</div>
                       <div class="col-md-2">{{ Form::radio('secondary_track_type','karaoke', !empty($sdata->secondary_track_type && $sdata->secondary_track_type=='karaoke')?true:false) }} Karaoke</div>
                       <div class="col-md-2">{{ Form::radio('secondary_track_type','medley', !empty($sdata->secondary_track_type && $sdata->secondary_track_type=='medley')?true:false) }} Medley</div>
                       <div class="col-md-2">{{ Form::radio('secondary_track_type','cover', !empty($sdata->secondary_track_type && $sdata->secondary_track_type=='cover')?true:false) }} Cover</div>
                       <div class="col-md-4">{{ Form::radio('secondary_track_type','cover_by_cover_band', !empty($sdata->secondary_track_type && $sdata->secondary_track_type=='cover_by_cover_band')?true:false) }}  Cover by cover band</div>
                       </div>                 
                </div> 
                
                <div class="form-group">
                   <label for="title" class="control-label required">{{ trans('backend.instrumental') }}</label>
                   <div class="row">
                       <div class="col-md-2">{{ Form::radio('instrumental','Yes',  !empty($sdata->instrumental)?($sdata->instrumental=='Yes')?true:false:false) }} Yes</div>              
                    <div class="col-md-2">{{ Form::radio('instrumental','No', !empty($sdata->instrumental && $sdata->instrumental=='No')?true:true) }} No</div>                
                </div>
                </div>
        	<div class="form-group @if ($errors->has('title')) has-error @endif">
                   <label for="title" class="control-label required">{{ trans('backend.title') }}</label>
                   {{ Form::text('title',null,['class'=>'form-control','id'=>'title','placeholder'=>trans("backend.title")])}}
                 @if ($errors->has('title'))<span class="error">{{ $errors->first('title') }}</span>@endif
                </div>
                <div class="form-group @if ($errors->has('subtitle')) has-error @endif">
                   <label for="subtitle" class="control-label">{{ trans('backend.version') }}</label>
                   {{ Form::text('subtitle',null,['class'=>'form-control','placeholder'=>trans("backend.version")])}}
                 @if ($errors->has('subtitle'))<span class="error">{{ $errors->first('subtitle') }}</span>@endif
                </div>
                
                <?php $primary_artist =  isset($sdata->primary_artist)?explode('|',$sdata->primary_artist):array();?>
                <div class="form-group @if ($errors->has('primary_artist.0')) has-error @endif">
                   <label for="primary_artist" class="control-label required">{{ trans('backend.primary') }}</label>
                   <div class="row">
                       <div class="col-md-11">{{ Form::text('primary_artist[]',isset($primary_artist[0])?$primary_artist[0]:null,['class'=>'form-control primary_artist','placeholder'=>trans("backend.primary")])}}</div>
                       <div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-success add_primary_artist"><i class="fa fa-plus-circle"></i></a></div>
                   </div>
                 @if ($errors->has('primary_artist.0'))<span class="error">{{ $errors->first('primary_artist.0') }}</span>@endif
                 <span class="error primary_artist_error" ></span>
                </div>
                    @if(count($primary_artist)>1)
                   @for($i=1;$i<count($primary_artist);$i++)
                        <div class="form-group">
                           <label for="primary_artist " class="control-label "></label>
                       <div class="row">
                       <div class="col-md-11">
                       {{ Form::text('primary_artist[]',isset($primary_artist[$i])?$primary_artist[$i]:null,['class'=>'form-control primary_artist','placeholder'=>trans("backend.primary")])}}
                       <br/>
                       </div>
                       <div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-danger remove_field"><i class="fa fa-minus-circle"></i></a></div>
                       </div>
                       </div>
                   @endfor
                   @endif
                    <div class="primary_artist_append"></div>
                       
                       
                       
                       
                  <?php $featuring_artist =  isset($sdata->featuring_artist)?explode('|',$sdata->featuring_artist):array();?>
                <div class="form-group @if ($errors->has('featuring_artist')) has-error @endif">
                   <label for="featuring" class="control-label ">{{ trans('backend.featuring') }}</label>
                   <div class="row">
                        <div class="col-md-11">{{ Form::text('featuring_artist[]',isset($featuring_artist[0])?$featuring_artist[0]:null,['class'=>'form-control featuring_artist featuring_artist_0','placeholder'=>trans("backend.featuring")])}}</div>
                       <div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-success add_featuring_artist"><i class="fa fa-plus-circle"></i></a></div>
                   </div>

                 @if ($errors->has('featuring_artist'))<span class="error">{{ $errors->first('featuring_artist') }}</span>@endif
                 <span class="error featuring_artist_error" ></span>
                </div>
                @if(count($featuring_artist)>1)
                   @for($i=1;$i<count($featuring_artist);$i++)
                       <div class="form-group">
                           <label for="primary_artist " class="control-label "></label>
                       <div class="row">
                       <div class="col-md-11">
                        {{ Form::text('featuring_artist[]',isset($featuring_artist[$i])?$featuring_artist[$i]:null,['class'=>'form-control featuring_artist featuring_artist_0','placeholder'=>trans("backend.featuring")])}}
                       <br/>
                       </div>
                       <div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-danger remove_field"><i class="fa fa-minus-circle"></i></a></div>
                       </div>
                       </div>
                   @endfor
                @endif
                <div class="featuring_artist_append"></div>
                       
                <?php $remixer =  isset($sdata->remixer)?explode('|',$sdata->remixer):array();?>
                <div class="form-group @if ($errors->has('remixer')) has-error @endif">
                   <label for="remixer" class="control-label ">{{ trans('backend.remixer') }}</label>
                   <div class="row">
                       <div class="col-md-11">{{ Form::text('remixer[]',isset($remixer[0])?$remixer[0]:null,['class'=>'form-control remixer','placeholder'=>trans("backend.remixer")])}}</div>
                       <div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-success add_remixer"><i class="fa fa-plus-circle"></i></a></div>
                   </div>

                 @if ($errors->has('remixer'))<span class="error">{{ $errors->first('remixer') }}</span>@endif
                 <span class="error remixer_error" ></span>
                </div>
                @if(count($remixer)>1)
                   @for($i=1;$i<count($remixer);$i++)
                       <div class="form-group">
                           <label for="primary_artist " class="control-label "></label>
                       <div class="row">
                       <div class="col-md-11">
                        {{ Form::text('remixer[]',isset($remixer[$i])?$remixer[$i]:null,['class'=>'form-control remixer','placeholder'=>trans("backend.remixer")])}}
                       <br/>
                       </div>
                       <div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-danger remove_field"><i class="fa fa-minus-circle"></i></a></div>
                       </div>
                       </div>
                   @endfor
                @endif
                <div class="remixer_append"></div>
                       
                <?php $author =  isset($sdata->author)?explode('|',$sdata->author):array();?>
                <div class="form-group @if ($errors->has('author.0')) has-error @endif">
                   <label for="remixer" class="control-label required">{{ trans('backend.author') }}</label>
                   <div class="row">
                       <div class="col-md-11">{{ Form::text('author[]',isset($author[0])?$author[0]:null,['class'=>'form-control author','placeholder'=>trans("backend.author")])}}</div>
                       <div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-success add_author"><i class="fa fa-plus-circle"></i></a></div>
                   </div>

                 @if ($errors->has('author.0'))<span class="error">{{ $errors->first('author.0') }}</span>@endif
                 <span class="error author_error" ></span>
                </div>
                @if(count($author)>1)
                   @for($i=1;$i<count($author);$i++)
                       <div class="form-group">
                           <label for="primary_artist " class="control-label "></label>
                       <div class="row">
                       <div class="col-md-11">
                        {{ Form::text('author[]',isset($author[$i])?$author[$i]:null,['class'=>'form-control author','placeholder'=>trans("backend.author")])}}
                       <br/>
                       </div>
                       <div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-danger remove_field"><i class="fa fa-minus-circle"></i></a></div>
                       </div>
                       </div>
                   @endfor
                @endif
                <div class="author_append"></div>
                       
                <?php $composer =  isset($sdata->composer)?explode('|',$sdata->composer):array();?>
                <div class="form-group @if ($errors->has('composer.0')) has-error @endif">
                   <label for="composer" class="control-label">{{ trans('backend.composer') }}</label>
                   <div class="row">
                       <div class="col-md-11">{{ Form::text('composer[]',isset($composer[0])?$composer[0]:null,['class'=>'form-control composer','placeholder'=>trans("backend.composer")])}}</div>
                       <div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-success add_composer"><i class="fa fa-plus-circle"></i></a></div>
                   </div>

                 @if ($errors->has('composer.0'))<span class="error">{{ $errors->first('composer.0') }}</span>@endif
                 <span class="error composer_error" ></span>
                </div>
                @if(count($composer)>1)
                   @for($i=1;$i<count($composer);$i++)
                       <div class="form-group">
                           <label for="primary_artist " class="control-label "></label>
                       <div class="row">
                       <div class="col-md-11">
                        {{ Form::text('composer[]',isset($composer[$i])?$composer[$i]:null,['class'=>'form-control composer','placeholder'=>trans("backend.composer")])}}
                       <br/>
                       </div>
                       <div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-danger remove_field"><i class="fa fa-minus-circle"></i></a></div>
                       </div>
                       </div>
                   @endfor
                @endif
                <div class="composer_append"></div>
                       
                       
                <?php $arranger =  isset($sdata->arranger)?explode('|',$sdata->arranger):array();?>
                <div class="form-group @if ($errors->has('arranger')) has-error @endif">
                   <label for="arranger" class="control-label required">{{ trans('backend.arranger') }}</label>
                   <div class="row">
                       <div class="col-md-11">{{ Form::text('arranger[]',isset($arranger[0])?$arranger[0]:null,['class'=>'form-control arranger','placeholder'=>trans("backend.arranger")])}}</div>
                       <div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-success add_arranger"><i class="fa fa-plus-circle"></i></a></div>
                   </div>

                 @if ($errors->has('arranger'))<span class="error">{{ $errors->first('arranger') }}</span>@endif
                 <span class="error arranger_error" ></span>
                </div>
                @if(count($arranger)>1)
                   @for($i=1;$i<count($arranger);$i++)
                       
                       <div class="form-group">
                           <label for="primary_artist " class="control-label "></label>
                      <div class="row">
                       <div class="col-md-11">
                        {{ Form::text('arranger[]',isset($arranger[$i])?$arranger[$i]:null,['class'=>'form-control arranger','placeholder'=>trans("backend.arranger")])}}
                       <br/>
                       </div>
                       <div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-danger remove_field"><i class="fa fa-minus-circle"></i></a></div>
                       </div>
                       </div>
                   @endfor
                @endif
                <div class="arranger_append"></div>
                       
                <?php $producer =  isset($sdata->producer)?explode('|',$sdata->producer):array();?>
                <div class="form-group @if ($errors->has('producer')) has-error @endif">
                   <label for="producer" class="control-label ">{{ trans('backend.producers') }}</label>
                   <div class="row">
                       <div class="col-md-11">{{ Form::text('producer[]',isset($producer[0])?$producer[0]:null,['class'=>'form-control producer','placeholder'=>trans("backend.producers")])}}</div>
                       <div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-success add_producer"><i class="fa fa-plus-circle"></i></a></div>
                   </div>

                 @if ($errors->has('producer'))<span class="error">{{ $errors->first('producer') }}</span>@endif
                 <span class="error producer_error" ></span>
                </div>
                @if(count($producer)>1)
                   @for($i=1;$i<count($producer);$i++)
                       <div class="form-group">
                           <label for="primary_artist " class="control-label "></label>
                       <div class="row">
                       <div class="col-md-11">
                        {{ Form::text('producer[]',isset($producer[$i])?$producer[$i]:null,['class'=>'form-control producer','placeholder'=>trans("backend.producers")])}}
                       <br/>
                       </div>
                       <div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-danger remove_field"><i class="fa fa-minus-circle"></i></a></div>
                       </div>
                       </div>
                   @endfor
                @endif
                <div class="producer_append"></div>
                       
                       
                <div class="form-group @if ($errors->has('p_line')) has-error @endif">
                   <label for="p_line" class="control-label required">  ℗ {{ trans('backend.line') }}</label>
                   {{ Form::text('p_line',null,['class'=>'form-control','id'=>'p_line','placeholder'=>trans("backend.line")])}}
                 @if ($errors->has('p_line'))<span class="error">{{ $errors->first('p_line') }}</span>@endif
                </div>

                <div class="form-group @if ($errors->has('production_year')) has-error @endif">
                   <label for="production_year" class="control-label required">{{ trans('backend.production_year') }}</label>
                   <select name="production_year" class="form-control production_year" placeholder="Select Year">
                       <option>Select Year</option>
                       @for($i=date('Y')+1;$i>1990; $i--)
                       <option value="{{$i}}" @if(isset($sdata->production_year) && $sdata->production_year==$i) selected @endif>{{$i}}</option>
                       @endfor                       
                   </select>
                 @if ($errors->has('production_year'))<span class="error">{{ $errors->first('production_year') }}</span>@endif
                </div>
                       
                       
                <div class="form-group @if ($errors->has('publisher')) has-error @endif">
                   <label for="title" class="control-label"> {{ trans('backend.publisher') }}</label>
                   {{ Form::text('publisher',isset($sdata->publisher)?$sdata->publisher:null,['class'=>'form-control','placeholder'=>trans("backend.publisher")])}}
                 @if ($errors->has('publisher'))<span class="error">{{ $errors->first('publisher') }}</span>@endif
                </div>
                       
                <div class="form-group @if ($errors->has('ISRC')) has-error @endif">
                   <label for="title" class="control-label required"> {{ trans('backend.ISRC') }}</label>
                   {{ Form::text('ISRC',isset($isrc)?$isrc:null,['class'=>'form-control','maxlength'=>"15",'placeholder'=>trans("backend.ISRC")." (allowed format: JM-K40-14-00212)"])}}
                 @if ($errors->has('ISRC'))<span class="error">{{ $errors->first('ISRC') }}</span>@endif
                </div>

                       
                       
                       
                       
                <div class="form-group  @if ($errors->has('genre_id')) has-error @endif">
                 <label for="genre_id" class="control-label required">{{ trans("backend.genre") }}</label>
                    {{ Form::select('genre_id',$genre,isset($sdata->genre_id)?$sdata->genre_id:null,['class'=>'form-control genre_id','placeholder'=> ucwords(trans("backend.select").' '.trans("backend.genre")),'style'=>'text-transform: capitalize;']) }}
                    @if ($errors->has('genre_id'))<span class="error">{{ $errors->first('genre_id') }}</span>@endif
                </div>
                
                <div class="form-group  @if ($errors->has('subgenre_id')) has-error @endif">
                 <label for="subgenre_id" class="control-label">{{ trans("backend.subgenre") }}</label>
                    {{ Form::select('subgenre_id',$subgenre,isset($sdata->subgenre_id)?$sdata->subgenre_id:null,['class'=>'form-control subgenre_id','placeholder'=> ucwords(trans("backend.select").' '.trans("backend.subgenre")),'style'=>'text-transform: capitalize;']) }}
                    @if ($errors->has('subgenre_id'))<span class="error">{{ $errors->first('subgenre_id') }}</span>@endif
                </div>
                       
                 <div class="form-group">
                   <label for="title" class="control-label required">Is this track considered a key track for the release ?</label>
                   <div class="row">
                       <div class="col-md-2">{{ Form::radio('track_for_the_release','Yes', false) }} Yes</div>              
                    <div class="col-md-2">{{ Form::radio('track_for_the_release','No', true) }} No</div>                
                </div>
                </div>
                
                       <?php /*
                <div class="form-group  @if ($errors->has('price_id')) has-error @endif" style="display:none">
                    <label for="format" class="control-label ">{{ trans("backend.price_tire") }}</label>
                    {{ Form::select('price_id',$price,null,['class'=>'form-control price_id','placeholder'=> ucwords(trans("backend.price_tire")),'style'=>'text-transform: capitalize;']) }}
                    @if ($errors->has('price_id'))<span class="error">{{ $errors->first('price_id') }}</span>@endif
                </div>    
                      */ ?>
                       
              
                <div class="form-group @if ($errors->has('producer_catalogue_number')) has-error @endif">
                   <label for="title" class="control-label">{{ trans('backend.producer_catalogue_number') }}</label>
                   {{ Form::text('producer_catalogue_number',null,['class'=>'form-control','placeholder'=>trans("backend.producer_catalogue_number")])}}
                 @if ($errors->has('producer_catalogue_number'))<span class="error">{{ $errors->first('producer_catalogue_number') }}</span>@endif
                </div>
                       
                 <div class="form-group">
                   <label for="title" class="control-label required">Parental advisory</label>
                   <div class="row">
                       <div class="col-md-3">{{ Form::radio('parental_advisory','Explicit', !empty($sdata->parental_advisory)?($sdata->parental_advisory=='Explicit')?true:false:false) }} Explicit</div>              
                    <div class="col-md-4">{{ Form::radio('parental_advisory','Not Explicit', !empty($sdata->parental_advisory && $sdata->parental_advisory=='Not Explicit')?true:true) }} Not Explicit</div>                
                    <div class="col-md-4">{{ Form::radio('parental_advisory','Edited', !empty($sdata->parental_advisory && $sdata->parental_advisory=='Edited')?true:false) }}  Edited</div>               
                </div>
                </div>
                       
                       
                 <div class="form-group @if ($errors->has('preview_start')) has-error @endif">
                   <label for="title" class="control-label">{{ trans('backend.tiktok_preview_start') }}</label>
                   {{ Form::text('preview_start',null,['class'=>'form-control','placeholder'=>trans("backend.tiktok_preview_start")])}}
                 @if ($errors->has('preview_start'))<span class="error">{{ $errors->first('preview_start') }}</span>@endif
                </div>
                 <div class="form-group @if ($errors->has('title_language')) has-error @endif">
                   <label for="title" class="control-label required">{{ trans('backend.title_language') }}</label>
                 {{ Form::select('title_language',$language,null,['class'=>'form-control title_language','placeholder'=> ucwords(trans("backend.select").' '.trans("backend.title_language")),'style'=>'text-transform: capitalize;']) }}
                   @if ($errors->has('title_language'))<span class="error">{{ $errors->first('title_language') }}</span>@endif
                </div>
                 <div class="form-group @if ($errors->has('lyrics_language')) has-error @endif">
                   <label for="title" class="control-label">{{ trans('backend.lyrics_language') }}</label>
                 {{ Form::select('lyrics_language',$language,null,['class'=>'form-control lyrics_language','placeholder'=> ucwords(trans("backend.select").' '.trans("backend.lyrics_language")),'style'=>'text-transform: capitalize;']) }}
                 @if ($errors->has('lyrics_language'))<span class="error">{{ $errors->first('lyrics_language') }}</span>@endif
                </div>
                       
                 <div class="form-group @if ($errors->has('track_lyrics')) has-error @endif">
                   <label for="title" class="control-label">{{ trans('backend.track_lyrics') }}</label>
                   {{ Form::text('track_lyrics',null,['class'=>'form-control','placeholder'=>trans("backend.track_lyrics")])}}
                 @if ($errors->has('track_lyrics'))<span class="error">{{ $errors->first('track_lyrics') }}</span>@endif
                </div>
                       
        	</div>
        </div>
   </div>
</div>

            <hr>
            <div class="row">
                <div class="col-sm-12">
                    <a href="javascript:;" class="btn btn-danger btn-sm cancel_cls_track pull-left"><i class="fas fa-times"></i> {{ trans("backend.cancel") }}</a>
                    <button type="button" class="btn btn-primary btn-sm pull-right form_submit_track"><i class="fas fa-save"></i> save </button>
                </div>
            </div>
   </div>
</div>
{{ Form::close() }}
<script>
    $(document).ready(function(){        
        $('#datetimepicker').datetimepicker({
            timepicker:false,
            format:'Y-m-d',
        });
        $(document).on('click','.remove_field',function(){
          $(this).closest('.form-group').remove();
        });
        
        
        $('.various_artists').click(function(){
            var ck_status=  $(this).is(":checked");
            if(ck_status==true){
                $('.primary_artist').attr('readonly','readonly');
                $('.featuring_artist').attr('readonly','readonly');
                $('.primary_artist_error').html('');
                $('.featuring_artist_error').html('');
            }
            else{
                $('.primary_artist').removeAttr('readonly');
                $('.featuring_artist').removeAttr('readonly');
                $('.primary_artist_error').html('');                
                $('.featuring_artist_error').html('');                
            }
        })
        
        var i=1;
        function addmore(key,value){
           var status = 'false'; 
            $('.'+key).each(function(){
               var primary_artist =  $(this).val();
               if(primary_artist==''){
                   status = 'true';
                   return;
               }
           });
           if(status=='false'){
                $('.'+key+'_append').append('<div class="form-group"><label class="control-label">&nbsp;</label><div class="row"><div class="col-md-11"><input class="form-control '+key+'" placeholder="'+value+'" name="'+key+'[]" type="text"></div><div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-danger remove_field"><i class="fa fa-minus-circle"></i></a></div></div></div>')
                $('.'+key+'_error').html('');             
                i++;  
            }
            else{
                $('.'+key+'_error').html('<label class="control-label">&nbsp;</label>You must enter a name on the current field before adding a new one');                      
            } 
        }
        
       
        $('.add_primary_artist').click(function(){
            addmore('primary_artist','Primary Artist');
        });
        $('.add_featuring_artist').click(function(){
            addmore('featuring_artist','Featuring Artist');
        });
        
        $('.add_remixer').click(function(){
            addmore('remixer','Film Director');
        });
        $('.add_author').click(function(){
            addmore('author','Author');
        });
        $('.add_composer').click(function(){
            addmore('composer','Composer');
        });
        $('.add_arranger').click(function(){
            addmore('arranger','Music Director');
        });
        
        $('.add_producer').click(function(){
            addmore('producer','Producer');
        });
        
        
        $('.genre_id').change(function(){
        var genre_id = $(this).val();
        $(".loding_img").show();
            $.ajax({
                 url: '{{ routeUser('ajax.subgenre') }}',
                 type: 'post',
                 data: {
                   '_token': '{{ csrf_token() }}',
                   'genre_id':genre_id,
                 },
                 success: function (data) 
                 {
                     $('.subgenre_id').html(data);
                     $(".loding_img").hide();
                 },
                 error: function (data) {
                     return false;
                 }
             });
        });
       
        $('.cancel_cls_track').click(function(){
            $('#myModal').modal('hide'); 
        });
   });
   
</script>
