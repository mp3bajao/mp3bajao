{{Form::open(array('route' => [routeFormUser($page.'.upload',encrypt($release))],'files'=>true,'class'=>'upload_submit'))}}
<input type="hidden" name="track_id" value="{{$release}}"/>

<div class="row">
    <div class="col-sm-12">
      <div class="modal-body">
            <div class="progress" style="height: 2.125em; display: none;">
    <div class="progress-bar progress-bar-striped"> 0% Complete (success)</div>
  </div>
          <div class="content-header1"></div>
          
          <h5> You can upload the following format:
                  <small>     
    FLAC,
    MP3
    </small></b></h5>
        <div class="form-group imgs_video text-center">
            <div class="vimgupload">
                <span class="btn btn-primary btn-file" >
                    <i class="fa fa-upload"></i> 
                        <div>upload Audio file</div>
                        <input type="file" name="audio_file[]" class="file_upload" id="file_upload" />
                        <span id="btn-file-error"></span>
                </span>
            </div>
                  </div>
        <h6> <b> Please make sure to avoid using special characters such as & / % # etc. when naming your audio files. Files might not upload correctly otherwise.

                You will be able to match your audio files with your tracks during the 'submission' step.</b></h6>
          <h6 style="color:#FF414E">You will be able to match your audio files with your tracks during the 'submission' step.</h6>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
          <button type="submit" class="btn btn-primary upload_submit" >Upload Files</button>
      </div>

   
       
       
   </div>
</div>                  
{{ Form::close() }}

<script>
$(document).ready(function(){
       

        $('#file_upload').change(function(){
            var fi = document.getElementById('file_upload');
            var type = 'audio';
            FileDetails_audio(fi,'btn-file-error',type);
        });
        
        function FileDetails_audio(fi,cla,type) {
            
             if (fi.files.length > 0 && fi.files.length < 20) {
                 document.getElementById(cla).innerHTML =
                        'Total Files: <b>' + fi.files.length + '</b>';
                 for (var i = 0; i <= fi.files.length - 1; i++) {
                     if(type !='imgInp_all')
                     {
                        var result = readURL(fi.files.item(i),type);                       
                        if(result=='false')
                        {
                            return false;
                        }
                     }
                     
                     var fname = fi.files.item(i).name;
                     var fsize = fi.files.item(i).size;
                     //if(fsize<'20971520')
                    // {
                        document.getElementById(cla).innerHTML =
                        document.getElementById(cla).innerHTML + '<br /> ' +
                        fname + ' (<b>' + fsize + '</b> bytes)';
                    /*}
                    else
                    {                       
                       $('#'+cla).html('');
                       $(fi.files).val('');
                       $('#'+cla).text("Only flie size 10 MB  allowed");
                       return false;
                    }*/
                 }
             }
             else{
                       $('#'+cla).html('');
                       $(fi.files).val('');
                       $('#'+cla).text(" You can only upload a maximum of 20 files.");
                       return false;
             }
             
        }
        function readURL(input,type) {
            if (input) {              
                var type_reg = /^audio\/(FLAC|flac|MP3|mp3|mpeg|MPEG)$/;
                var selecttype =  input.type;
                if (type_reg.test(selecttype)) {
                   
                } else {
                    $(input).val('');
                    $('#btn-file-error').text("Only formats are allowed :FLAC, MP3, MPEG");
                    return 'false';
                }  
           } 
       }




    var progressbar= $('.progress-bar');
    $('.upload_submit').on('submit', function(event){       
       // $(".loding_img").show();
       $('#myModal').modal('hide');
       event.preventDefault();
        $.ajax({
            url:"{{ routeUser('track.upload')}}",
            method:"POST",
            data:new FormData(this),
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $(".progress").show();
                progressbar.width('0%');
                progressbar.text('0%');
                $('.form_submit').hide();
            },
            uploadProgress: function (event, position, total, percentComplete) {
                progressbar.width(percentComplete + '%');
		progressbar.text(percentComplete + '%');
                console.log(percentComplete);
            },
            xhr: function() {
        var xhr = new window.XMLHttpRequest();
        xhr.upload.addEventListener("progress", function(evt) {
            console.log(evt.loaded);
            console.log(evt.total);            
            console.log(evt);            
            if (evt.lengthComputable) {
                var percentComplete = (evt.loaded / evt.total) * 100;                
                progressbar.width(percentComplete.toFixed(2) + '%');
		progressbar.text(percentComplete.toFixed(2) + '% Processing...');
            }
       }, false);
       return xhr;
    },
    success:function(data)
            {
                console.log(data.status);
               $('.form_submit').show();
               // $(".loding_img").hide();
                 $('#btn-file-error').html('');
                 $('#file_upload').val('');
                if(data.status==false)
                {
                    $('.progress').hide();
                    $('.content-header1').html('<div class="alert alert-danger"><strong>Danger!</strong> '+data.message+'</div>')
                }
                else{
                    if(data.type=='Both')
                    {
                        progressbar.text( '100 % Process completed');
                        $('.content-header1').html('<div class="alert alert-success"><strong>Success!</strong> '+data.success+'</div><div class="alert alert-warning">  <strong>Warning!</strong> '+data.invalid+'</div>');
                    }
                    else if(data.type=='itype'){
                        progressbar.text( '100 % Process completed');
                        $('.content-header1').html('<div class="alert alert-success"><strong>Success!</strong> '+data.success+'</div>');                        
                    }
                    else if(data.type=='ktype'){
                         progressbar.text( 'Process error');
                         $('.content-header1').html('<div class="alert alert-warning">  <strong>Warning!</strong> '+data.invalid+'</div>');                       
                    }
                }
                
            }
        });                
    });

});

      
     
    </script>