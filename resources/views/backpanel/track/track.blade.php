
<table class="table table-striped table-bordered" style="width:100%">
    <thead>
        <tr> 
            <th>{{ trans('backend.track').' '.trans('backend.name') }}</th>
            <th>{{ trans('backend.artist') }}</th>
            <th>{{ trans('backend.duration')}}</th>
            <th>{{ trans("backend.action") }}
        </tr>
    </thead>
    <tbody>
        @forelse($data as $key => $data)
        <tr>
            <td>{{$key+1}}.{!! ucwords($data->title) !!}<br><small>{{$data->ISRC}}</small></td>
            <td>{{ ucwords(str_replace('|',', ',$data->primary_artist))  }}</td>
            <td>{{ $data->getMedia->file_duration ?? 'Empty'}}</td>
            <td><a href="javascript:;" data-track_id="{{$data->id}}" data-track_name="{!! ucwords($data->title) !!}" data-track_artist="{{ ucwords(str_replace(',',', ',$data->primary_artist))  }}" data-cover="{{$data->getRelease->album_profile}}" data data-song="{{ url_checker(str_replace('flac','mp3',$data->getMedia->base_path.'/'.$data->getMedia->folder_path))}}" class="btn btn-success btn-sm playsong"><i class="fas fa-play"></i></a></td>
        </tr>
        @empty
        <tr>
            <td colspan="5">No Track Found.</td>
        </tr>
        @endforelse
    </tbody>
    <tbody>

    </tbody>
</table>

@include('layouts.audio-player' )
