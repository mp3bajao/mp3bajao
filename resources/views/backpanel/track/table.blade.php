<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">                   


<table  id="listing" class="table table-striped table-bordered" style="width:100%">
    <thead>
        <tr>
            <th>#</th>    
            <th>{{ trans('backend.album_id') }}</th>
            <th>{{ trans('backend.track') }}</th>
            <th>{{ trans('backend.artist') }}</th>
            <th>{{ trans('backend.album_name')}}</th>
            <th>{{ trans('backend.status')}}</th>
            <th>{!! trans('backend.created_at')!!}</th>
            <th>{{ trans("backend.action") }}
        </tr>
    </thead>
    <tbody>

    </tbody>
</table>

        
               <div class="modal" id="myModal" >
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
              <h4 class="modal-title">Track Assets Metadata</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
      <!-- Modal body -->
      <div class="modal-body">
          <div class="modal-body-content colspace_col">
              
          </div>
      </div>
    </div>
  </div>
                   
</div>
@section('script')    
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/parsley.min.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>

<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>

<script>
var ajax_datatable;
$(document).ready(function(){
    
ajax_datatable = $('#listing').DataTable({
    processing: true,
    serverSide: true,
    ajax:{
      url:'{{ routeUser('track.index') }}',
      data: function (d) {
        return $.extend({}, d, {
            "release_id":$('.release_id option:selected').val(),
        });
      }
    },
    columns: [
      { data: 'DT_RowId', name: 'DT_RowId', orderable: false, searchable: false, "width": "20px"},
      { data: 'album_id', name: 'album_id' },     
      { data: 'title', name: 'title' },
       { data: 'track_type', name: 'track_type' },
      { data: 'album_name', name: 'album_name' },
      { data: 'status', name: 'status' },
      { data: 'created_at', name: 'created_at' },
      { data: 'actions', name: 'actions' },
    ],
    order: [ [0, 'desc'] ],
});

$('.release_id').on('change', function(e) {
      ajax_datatable.draw();
        e.preventDefault();
});
$('#refresh').click(function(){
  $('#chef_id').val('');
  $('#celebrity_id').val('');
  ajax_datatable.draw();
 });
 

        
       
        
        
 });
 
  
        $(document).on('click','.edit_track',function(){ 
        var url = $(this).data('href_url');
        $(".loader").show();
            $.ajax({
                 url: url,
                 type: 'get',
                 data:{'form_type':'update'},
                 success: function (data) 
                 {
                    $('#myModal').modal('show');
                    $(".loader").hide();
                    $('.modal-body-content').html(data);
                 },               
             });
        }); 
 
  $(document).on('click','.form_submit_track',function(){
            $(".upload_submit_track").validate({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },		
                rules: {
                    title: "required",
                    'primary_artist[]': "required",
                    'author[]': "required",
                    //'composer[]': "required",
                    'arranger[]': "required",
                    genre_id: "required",				
                    label_id: "required",				
                    format: "required",				
                    original_release_date: "required",				
                    p_line: "required",				
                    c_line: "required",				
                    production_year: "required",
                    UPC: "required",
                    //price_id: "required",
                    preview_start:{
                                  required: false,
                                  digits: true
                                },
                    title_language: "required",
                    //lyrics_language: "required",
                    ISRC: "required",
		},
		messages: {
                    title: "The title field is required.",
                    'primary_artist[]': "The primary artist field is required.",
                    'author[]': "The author field is required.",
                    'composer[]': "The composer field is required.",
                    genre_id: "The genre field is required.",				
                    label_id: "The label field is required.",				
                    format: "The format field is required.",				
                    original_release_date: "The Physical/Original release date field is required.",				
                    p_line: "The  ℗ line field is required.",				
                    c_line: "The &copy; line  field is required.",				
                    production_year: "The production year  field is required.",				
                    UPC: "The UPC/EAN  field is required.",
                    //price_id: "The main price tier field is required.",
                    title_language: "The title language field is required.",
                    lyrics_language: "The lyrics language field is required.",
                    ISRC: "The ISRC field is required.",
		}
            });
            if ($('.upload_submit_track').valid()) {
                $(".loding_img").show();
                var route_url =  $("input[name=route_url]").val();
                var track_id_main =  $("input[name=track_id_main]").val();
                var track_type =  $("input[name=track_type]").val();
                var secondary_track_type =  $("input[name=secondary_track_type]:checked").val();
                var instrumental =  $("input[name=instrumental]:checked").val();
                var title =  $("input[name=title]").val();
                var subtitle =  $("input[name=subtitle]").val();
                var publisher =  $("input[name=publisher]").val();
                var p_line =  $("input[name=p_line]").val();
                var ISRC =  $("input[name=ISRC]").val();
                var producer_catalogue_number =  $("input[name=producer_catalogue_number]").val();
                var track_for_the_release =  $("input[name=track_for_the_release]:checked").val();
                var parental_advisory =  $("input[name=parental_advisory]:checked").val();
                var track_lyrics =  $("input[name=track_lyrics]").val();
                var preview_start =  $("input[name=preview_start]").val();

                var production_year =  $(".production_year option:selected").val();
                var genre_id =  $(".genre_id option:selected").val();
                var subgenre_id =  $(".subgenre_id option:selected").val();
                var price_id =  $(".price_id option:selected").val();
                var lyrics_language =  $(".lyrics_language option:selected").val();
                var title_language =  $(".title_language option:selected").val();



                var primary_artist = [] ;
                $(".primary_artist").each(function(i){
                    primary_artist[i] = $(this).val();
                });

                var featuring_artist = [] ;
                $(".featuring_artist").each(function(i){
                    featuring_artist[i] = $(this).val();
                });
                var remixer = [] ;
                $(".remixer").each(function(i){
                    remixer[i] = $(this).val();
                });
                var author = [] ;
                $(".author").each(function(i){
                    author[i] = $(this).val();
                });
                var composer = [] ;
                $(".composer").each(function(i){
                    composer[i] = $(this).val();
                });
                var arranger = [] ;
                $(".arranger").each(function(i){
                    arranger[i] = $(this).val();
                });
                var producer = [] ;
                $(".producer").each(function(i){
                    producer[i] = $(this).val();
                });


                var form_data = {
                   'route_url':route_url,
                   'track_id_main':track_id_main,
                   'track_type':track_type,
                   'secondary_track_type':secondary_track_type,
                   'instrumental':instrumental,
                   'title':title,
                   'subtitle':subtitle,
                   'primary_artist':primary_artist,
                   'featuring_artist':featuring_artist,
                   'remixer':remixer,
                   'author':author,
                   'composer':composer,
                   'arranger':arranger,
                   'producer':producer,
                   'p_line':p_line,
                   'production_year':production_year,
                   'publisher':publisher,              
                   'ISRC':ISRC,
                   'genre_id':genre_id,
                   'subgenre_id':subgenre_id,
                   'track_for_the_release':track_for_the_release,
                   'price_id':price_id,
                   'producer_catalogue_number':producer_catalogue_number,                                         
                   'parental_advisory':parental_advisory,
                   'preview_start':preview_start, 
                   'title_language':title_language,
                   'track_lyrics':track_lyrics, 
                   'lyrics_language':lyrics_language,              
                };                
                
                $.ajax({                    
                    headers: {
                        'X-CSRF-Token': '{{ csrf_token() }}'
                    },
                    url:route_url,
                    type: 'post',
                    data: form_data,
                    
                    dataType:'JSON',
                    success: function (data) 
                    {
                         $(".loding_img").hide();
                         $('#myModal').modal('hide');
                        if(data.status==false)
                        {
                            $('.content-header1').html('<div class="alert alert-danger"><strong>Danger!</strong> '+data.message+'</div>')
                        }
                    else{
                         
                         table.ajax.reload();
                        $('.content-header1').html('<div class="alert alert-success"><strong>Success!</strong> '+data.success+'</div>');                
                        }
                    }
                    
                });
            }
        });     
</script>

@endsection
