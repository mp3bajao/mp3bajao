
<form method="POST" action="{{  routeUser($page.'.edit',[$category->id]) }}" id="edit_role">
    @csrf

    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label class="control-label" for="name"> {{__('backend.name')}} *</label>
          <input type="text" name="name" value="{{$category->name}}" id="name" class="form-control" placeholder="Name"  />
        </div>
      </div>  
        <div class="col-md-6">
          <div class="form-group">
            <label class="control-label" for="name"> {{__('backend.duration')}}*</label>
            <input type="text" name="time"  id="timeedit" class="form-control"  value="{{$category->duration}}" placeholder="Time in sec"  />
          </div>
        </div> 
    </div>


    <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label class="col-md-12" for="image">Adds Media file (Mp3)</label>
            <input type="file" id="editfile" name="file" class="form-control" accept="audio/mp3,audio/*;capture=microphone">
            <div id="image_preview">
                <audio controls class="myaudioedit" id="myAudiosedit">
                    <source src="{{$category->track}}" type="audio/mpeg">
                  Your browser does not support the audio element.
                </audio>
            </div>
          </div>
        </div>
      </div>                   

    <hr style="margin: 1em -15px">
    <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-primary btn-xs float-right save"><span class="spinner-grow spinner-grow-sm formloader"
            style="display: none;" role="status" aria-hidden="true"></span> Save</button>

</form>

<script>
$(document).ready(function(){
$('#edit_role').parsley();
$("#edit_role").on('submit',function(e){ 

  e.preventDefault();
  var _this=$(this); 
    var formData = new FormData(this);
    formData.append('_method', 'POST');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
      url:'{{ routeUser($page.'.edit',[$category->id])}}',
      dataType:'json',
      data:formData,
      type:'POST',
      cache:false,
      contentType: false,
      processData: false,
      beforeSend: function (){before(_this)},
      // hides the loader after completion of request, whether successfull or failor.
      complete: function (){complete(_this)},
      success:function(res){
        if(res.status === 1){ 
          $('#editModal').modal('hide') 
          toastr.success(res.message);
          $('#edit_role').parsley().reset();
          ajax_datatable.draw();
        }else{
          toastr.error(res.message);
        }
      },
      error:function(jqXHR,textStatus,textStatus){
        if(jqXHR.responseJSON.errors){
          $.each(jqXHR.responseJSON.errors, function( index, value ) {
            toastr.error(value)
          });
        }else{
          toastr.error(jqXHR.responseJSON.message)
        }
      }
    });
  return false;   
});


  $("#myAudiosedit").on("canplaythrough", function(e){
            var seconds = e.currentTarget.duration;
            $('#timeedit').val(seconds);
        });
    
    $("#editfile").change(function(){
        var URL = window.URL || window.webkitURL
        var fileObj = this.files[0];
        var imageFileType = fileObj.type;
        var imageSize = fileObj.size;
        var fileURL = URL.createObjectURL(fileObj);
        $('.myaudioedit').attr('src',fileURL);

    });
    /*
$("#editfile").change(function(){
    var fileObj = this.files[0];
    var imageFileType = fileObj.type;
    var imageSize = fileObj.size;
  
     var match = ["audio/mp3","audio/mpeg"];
    if(!((imageFileType == match[0]) || (imageFileType == match[1]))){
      $('#editpreviewing').attr('src','images/image.png');
     toastr.error('Please Select A valid Mp3 File <br> Note: Only Mp3 File Allowed!!');
      return false;
    }else{
      //console.log(imageSize);
       if(imageSize < 100000000){
        var reader = new FileReader();
        reader.onload = imageIsLoaded;
        reader.readAsDataURL(this.files[0]);
      }else{
        toastr.error('Images Size Too large Please Select 100MB File!!');
        return false;
      }
      
    }
    
  });
  */
});

function imageIsLoaded(e){
			$("#editfile").css("color","green");
			$('#editpreviewing').attr('src',e.target.result);

		}
</script>