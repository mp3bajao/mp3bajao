@extends('layouts.master')
@section('content')


<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}"> 
<!-- Content Header (Page header) -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">{{ __('backend.'.$lang).' '.__('backend.manager') }}</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">{{ __('backend.home') }}</a></li>
                <li class="breadcrumb-item active">{{ __('backend.'.$lang).' '.__('backend.manager') }}</li>
            </ol>
               <?php // @can(ucfirst($page).'-create') ?>
              <a href="#" class="btn btn-primary btn-xs d-none d-lg-block m-l-15" title="{{ __('backend.add_title_') }}" data-toggle="modal" data-target="#add_modal" ><i class="fa fa-plus"></i> {{ __('backend.add') }}</a>
              <?php // @endcan ?>
        </div>
    </div>
</div>
<!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
    <div class="row">
        <div class="col-md-12">
        <div class="card card-primary card-outline">
                <div class="card-body">
                    <div class="table-responsive">
                    <table  id="category_listing" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>{{ __('backend.sr_no') }}</th>
                                <th>{{ __('backend.name') }}</th>
                                <th>{{ __('backend.duration') }}</th>
                                <th>{{ __('backend.status') }}</th>                                 
                                <th>{!! __('backend.created_at') !!}</th>
                                <th>{{ __('backend.action') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
    </div>
</div>

</div>
    <!-- /.content -->

<!-- Modals -->

<div class="modal fade" id="add_modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <form method="POST" action="{{ routeUser($page.'.create') }}" id="add_category">
    @csrf
      <div class="modal-header">
        <h4 class="modal-title">{{ __('backend.add').' '.__('backend.'.$lang) }}</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="tab-content" style="margin-top:10px">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label class="control-label" for="name"> {{__('backend.name')}}*</label>
            <input type="text" name="name" value="" id="name" class="form-control" placeholder=" Name"  />
          </div>
        </div> 
           <div class="col-md-6">
          <div class="form-group">
            <label class="control-label" for="name"> {{__('backend.duration')}}*</label>
            <input type="text" name="time" value="" id="time" class="form-control" step="2" placeholder="Time in Sec"  readonly="true" />
          </div>
           </div>
      </div>
      </div>
          
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label class="col-md-12" for="image">Adds Media file (Mp3)</label>
            <input type="file" id="file" name="file" class="form-control" accept="audio/mp3,audio/*;capture=microphone">
            <?php /*
            <div id="image_preview"><img height="100" width="100" id="previewing" src="{{ URL::asset('images/image.png')}}"></div>
            */?>
          </div>
            <audio controls="" class="myaudio" id="myAudios">
                <source src="" type="audio/mp3">
                  Your browser does not support the audio element.
            </audio>
        </div>
      </div>
            
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary  btn-xs save"><span class="spinner-grow spinner-grow-sm formloader" style="display: none;" role="status" aria-hidden="true"></span> Save</button>
      </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="editModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">{{ __('backend.edit').' '.__('backend.'.$lang) }}</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div id="edit_category_response"></div>  
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="viewModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">{{ __('backend.view') .' '.__('backend.'.$lang) }}</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div id="view_response"></div>  
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/parsley.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
var ajax_datatable;
$(document).ready(function(){
$('#add_category').parsley();
ajax_datatable = $('#category_listing').DataTable({
    processing: true,
    serverSide: true,
    ajax: '{{ routeUser($page.'.index') }}',
    columns: [
      { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
//      { data: 'image', name: 'image' },
      { data: 'name', name: 'name' },
      { data: 'duration', name: 'duration' },
      { data: 'status', name: 'status' },
      { data: 'created_at', name: 'created_at' },
      
      {data: 'id', name: 'id', orderable: false, searchable: false}
    ],
    order: [ [5, 'desc'] ],
    rowCallback: function(row, data, iDisplayIndex) {  
      
      var links='';
      links += `<div class="btn-group" role="group" >`;
     <?php /* @can(ucfirst($page).'-edit') */?>
      links += `<a href="#" data-category_id="${data.id}" title="Edit Details" class="btn btn-primary btn-xs edit_category" ><span class="fa fa-edit"></span></a>`;
      links += `<a href="#" data-category_id="${data.id}" title="Delete category" class="btn btn-danger btn-xs delete_category " ><span class="fa fa-trash"></span></a>`;
      
<?php /*      @endcan
      @can(ucfirst($page).'-delete') 
      @endcan
      */?>
      links += `</div>`;
      var status = '';
      if(data.status === 1){
        status += `<a href="#" data-category_id="${data.id}" title="{{__('backend.active_category')}}" data-status="deactive" class="change_status"><span class='label label-rounded label-success'>{{__('backend.active')}}</span></a>`;
      }else{
        status += `<a href="#" data-category_id="${data.id}" title="{{__('backend.deactive_category')}}" data-status="active" class="change_status"><span class='label label-rounded label-warning'>{{__('backend.deactive')}}</span></a>`;
      }
      var rank =`<input type="number" class="form-control changeArtistRank" data-id="${data.id}" min="0" value="${data.is_top}">`;
      //var image = `<img src="${data.image}" width="50px">`;
     // $('td:eq(1)', row).html(image);
      $('td:eq(3)', row).html(status);
      //$('td:eq(5)', row).html(rank);
      $('td:eq(5)', row).html(links);
      },
});


     $(document).on('keyup','.changeArtistRank',function(){
    var values = $(this).val();
    var id = $(this).data('id');
    $.ajax({
          type: 'get',
          data: {values:values,id:id},
          dataType:'html',
            url: "{{ routeUser('chart.add_chart_artist_rank') }}",
          success:function(res){
           // ajax_datatable.draw();
          },   
          error:function(jqXHR,textStatus,textStatus){
            console.log(jqXHR);
            toastr.error(jqXHR.statusText)
          }
      });
});

$(document).on('click','.change_status',function(e){
      e.preventDefault();
      status = $(this).data('status');
      if(status == 'active'){
        var response = confirm('Are you sure want to active?');
      }else{
        var response = confirm('Are you sure want to deactive?');
      }
      if(response){
        id = $(this).data('category_id');
        $.ajax({
          type: 'post',
          data: {_method: 'get', _token: "{{ csrf_token() }}"},
          dataType:'json',
          url: "{!! routeUser($page.'.status')!!}" + "/" + id +'/'+status,
          success:function(res){
            if(res.status === 1){ 
              toastr.success(res.message);
              ajax_datatable.draw();
            }else{
              toastr.error(res.message);
            }
          },   
          error:function(jqXHR,textStatus,textStatus){
            console.log(jqXHR);
            toastr.error(jqXHR.statusText)
          }
      });
      }
      return false;
    }); 
    
    <?php /*@can(ucfirst($page).'-create') */ ?>
    $("#add_category").on('submit',function(e){
      e.preventDefault();
      var _this=$(this); 
        var formData = new FormData(this);
        $.ajax({
            url:'{{ routeUser($page.'.create')}}',
            dataType:'json',
            data:formData,
            type:'POST',
            cache:false,
            contentType: false,
            processData: false,
            beforeSend: function (){before(_this)},
            // hides the loader after completion of request, whether successfull or failor.
            complete: function (){complete(_this)},
            success:function(res){
                  if(res.status === 1){
                    $('#add_modal').modal('hide') ; 
                    toastr.success(res.message);
                    $('#add_category')[0].reset();
                    $('#previewing').attr('src','images/image.png');
                    $('#add_category').parsley().reset();
                    ajax_datatable.draw();
                  }else{
                    toastr.error(res.message);
                  }
              },
            error:function(jqXHR,textStatus,textStatus){
              if(jqXHR.responseJSON.errors){
                $.each(jqXHR.responseJSON.errors, function( index, value ) {
                  toastr.error(value)
                });
              }else{
                toastr.error(jqXHR.responseJSON.message)
              }
            }
          });
          return false;   
        });
    <?php /*@endcan
    
    @can(ucfirst($page).'-edit')*/ ?>
    //Edit staff
    $(document).on('click','.view_btn',function(e){
        e.preventDefault();
        $('#view_response').empty();
        id = $(this).attr('data-category_id');
        $.ajax({
           url:'{{ routeUser($page.'.view')}}/'+id,
           dataType: 'html',
           success:function(result)
           {
            $('#view_response').html(result);
           } 
        });
        $('#viewModal').modal('show');
     });
   <?php /* @endcan
    
    @can(ucfirst($page).'-edit') */?>
    //Edit staff
    $(document).on('click','.edit_category',function(e){
        e.preventDefault();
        $('#edit_category_response').empty();
        id = $(this).attr('data-category_id');
        $.ajax({
           url:'{{ routeUser($page.'.edit')}}/'+id,
           dataType: 'html',
           success:function(result)
           {
            $('#edit_category_response').html(result);
           } 
        });
        $('#editModal').modal('show');
     });
    <?php /*@endcan
    
    @can(ucfirst($page).'-delete') */?>
    $(document).on('click','.delete_category',function(e){
      e.preventDefault();
      var response = confirm('Are you sure want to delete ?');
      if(response){
        id = $(this).data('category_id');
        $.ajax({
            type: 'post',
            data: {_method: 'delete', _token: "{{ csrf_token() }}"},
            dataType:'json',
            url: "{!! routeUser($page.'.destroy')!!}" + "/" + id,
            success:function(res){
              if(res.status === 1){ 
                  toastr.success(res.message);
                  ajax_datatable.draw();
                }else{
                  toastr.error(res.message);
                }
            },   
            error:function(jqXHR,textStatus,textStatus){
              console.log(jqXHR);
              toastr.error(jqXHR.statusText)
            }
        });
      }
      return false;
    }); 
   <?php /* @endcan */?>
        $("#myAudios").on("canplaythrough", function(e){
            var seconds = e.currentTarget.duration;
            $('#time').val(seconds);
        });
    
    $("#file").change(function(){
        var URL = window.URL || window.webkitURL
        var fileObj = this.files[0];
        var imageFileType = fileObj.type;
        var imageSize = fileObj.size;
        var fileURL = URL.createObjectURL(fileObj);
        $('.myaudio').attr('src',fileURL);

    });
    });
    function imageIsLoaded(e){
	//console.log(e);
	$("#file").css("color","green");
	$('#previewing').attr('src',e.target.result);
    }
</script>
@endsection
