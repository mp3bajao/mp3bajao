@extends('admin.layouts.layout')
@section('content')
<div class="content-wrapper">
  <section class="content-header1">
    @include('admin.partials.errors')
  </section>
  <section class="content-header">
    <h1>
    {{ trans('admin_lang.'.$langPath) }}
    <small>Compilation</small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="{!! url('admin/dashboard') !!}"><i class="fa fa-dashboard"></i> {{ trans('admin_lang.home') }}</a></li>
       <li class="active"><a href="{!! routeUser($pagePath.'.index') !!}">{{ trans('admin_lang.'.$langPath) }}</a></li>
       <li class="active"> Compilation</li>
    </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- Info boxes -->
      <div class="row">
         <div class="col-xs-12">
            <div class="box">
               <div class="box-header">
                  <h3 class="box-title">Compilation</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                  {{Form::open(array('route' => [routeFormUser($pagePath.'.compilation_store')],'files'=>true,'class'=>'upload_submit'))}}
                  
                  <div class="col-sm-12">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                      <div class="">
                        <div  >
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group @if ($errors->has('title')) has-error @endif">
                            {{ Form::text('title',null,['class'=>'form-control','id'=>'title','placeholder'=>'Title for this release'])}}
                            @if ($errors->has('title'))<span class="error">{{ $errors->first('title') }}</span>@endif
                          </div>
                        </div>
                      </div>
                        
                      <div class="row">
                        <div class="col-md-2">
                          {{Form::radio('globel','MUSIC',true,['class'=>'access_option'])}}
                          <label for="globel" class="control-label">{{ trans("admin_lang.music") }}</label>
                        </div>
                        <div class="col-md-2">
                          {{Form::radio('globel','CLASSIC MUSIC', false, ['class'=>'access_option'])}}
                          <label for="globel" class="control-label">{{ trans("admin_lang.classic_music") }}</label>
                        </div>
                        <div class="col-md-2">
                          {{Form::radio('globel','JAZZ MUSIC', false, ['class'=>'access_option'])}}
                          <label for="globel" class="control-label">{{ trans("admin_lang.jazz_music") }}</label>
                        </div>
                      </div>
                      <br>
                      <div class="row">           
                        <div class="col-md-7">
                          <?php /*?><label for="role" class="control-label required">Existing Tracks</label>
                          {{ Form::text('track_id',null,['class'=>'form-control searchable','placeholder'=>'Search'])}}
                          @if ($errors->has('track_id'))<span class="error">{{ $errors->first('track_id') }}</span>@endif<?php */?>
                          <table id="trackTable" class="table table-bordered table-hover datatable">
                            <thead>
                              <tr>
                                <th class="action">{{ trans("admin_lang.title") }}</th>
                                <th class="action">{{ trans("admin_lang.duration") }}</th>
                                <th class="action">{{ trans("admin_lang.action") }}</th>
                              </tr>
                            </thead>
                            <tfoot>
                              <tr>
                                <th>{{ trans("admin_lang.title") }}</th>
                                <th>{{ trans("admin_lang.duration") }}</th>
                                <th>{{ trans("admin_lang.action") }}</th>
                              </tr>
                            </tfoot>
                          </table>
                        </div>
                        <div class="col-md-5">
                          <table id="userTable" class="table table-bordered table-hover datatable">
                            <thead>
                              <tr>
                                <th>{{ trans("admin_lang.title") }}</th>
                                <th>{{ trans("admin_lang.duration") }}</th>
                                <th class="action">{{ trans("admin_lang.action") }}</th>
                              </tr>
                            </thead>
                            <tbody class="compile_items">
                              
                            </tbody>
                            <tfoot>
                              <tr>
                                <th>{{ trans("admin_lang.title") }}</th>
                                <th>{{ trans("admin_lang.duration") }}</th>
                                <th>{{ trans("admin_lang.action") }}</th>
                              </tr>
                            </tfoot>
                          </table>
                          @if ($errors->has('track_id'))<span class="error">{{ $errors->first('track_id') }}</span>@endif
                        </div>
                      </div>
                      <br />
                      <div class="row">
                         <div class="col-sm-12">
                            <!-- <a href="{{ routeUser($pagePath.'.index') }}" class="btn btn-danger cancel_cls pull-left">{{ trans("admin_lang.cancel") }}</a> -->
                            <button type="button" class="btn btn-primary pull-right form_submit" value="{{ trans('admin_lang.create_release') }}">Create a release</button>
                         </div>
                      </div>
                    </div>
                </div>
              </div>
                    </div>
                  </div>
                  {{ Form::close() }}
               </div>
               <!-- /.box-body -->
            </div>
            <!-- /.box -->
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- <link href="{{ asset('public/css/token-input.css') }}" media="screen" rel="stylesheet" type="text/css">
<script src="{{asset('public/js/jquery.tokeninput.js')}}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function () {
  $(".searchable").tokenInput("{{routeUser($pagePath.'.get_tracks')}}",{preventDuplicates:true});
});
</script> -->
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function () {
      loadSelectedTracks();
       var tables = $('#trackTable').DataTable({
            "bProcessing": true,
            "serverSide": true,
            "pageLength": 10,
            "order": [[0, "desc"]],
            "ajax": {
                url: "{{ routeUser($pagePath.'.get_tracks') }}",
                data: function (d) {
                    /*return $.extend({}, d, {
                    });*/
                },
                error: function () {
                    alert("{{trans('admin_lang.something_went_wrong')}}");
                }
            },
            "aoColumns": [
                {mData: 'title'},
                {mData: 'duration'},
                {mData: 'actions'}
            ],
            language: {
                    searchPlaceholder: "Search by title, primary artist"
                },
            "aoColumnDefs": [
                {"bSortable": false, "aTargets": ['action']}
            ],
            "fnDrawCallback": function() {
                $(".addToCompile").click(function(){
                  AddToCompile($(this).data('id'));
                });
            },
            "dom":"<'row'<'col-sm-6'l><'col-sm-6'f><'col-sm-6'>>" +"<'row'<'col-sm-12'tr>>" +"<'br'><'row'<'col-sm-12'i><'col-sm-12'p>>",

        });
    });
    $('.filter').click(function (e) {
      tables.ajax.reload();
    });
    function AddToCompile(track_id){
      $.ajax({
        url: "{{ routeUser($pagePath.'.add_tracks') }}",
        type: 'get',
        data: {
          'id': track_id
        },
        success: function (res){
          loadSelectedTracks();
        },
        error: function (data) {
          return false;
        }
      });
    }
    function loadSelectedTracks(){
      $.ajax({
        url: "{{ routeUser($pagePath.'.view_tracks') }}",
        type: 'get',
        success: function (res) {
          $('.compile_items').html(res);
        },
        error: function (data) {
            return false;
        }
      });
    }
    function deleteTracks(track_id){
      $.ajax({
        url: "{{ routeUser($pagePath.'.delete_tracks') }}",
        type: 'get',
        data: {
          'id': track_id
        },
        success: function (res){
            loadSelectedTracks();
        },
        error: function (data) {
            return false;
        }
      });
    }
</script>
<style type="text/css">
  #trackTable_info{margin-bottom: 10px;}
</style>
@endsection
