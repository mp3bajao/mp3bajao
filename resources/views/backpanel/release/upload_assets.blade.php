<div class="row">
    <div class="col-sm-12">
           <div class="col-md-12">
                             {{ form::hidden('id',$sdata->id) }}
                             {{ form::hidden('type',$type) }}

                        <a class="btn btn-social btn-primary add_files_upload" href="javascript:;" >
                        <i class="fa fa-plus"></i> Click here to launch the uploader
                        </a>
                             <span class="error error-message"></span>
                         </div>
       <br/><br/><br/>
       
       <div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">NEW UPLOADER</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
          
          <h5> You can upload the following format:
                  <small>     
    FLAC, 
    AIFF, 
    </small></b></h5>


        <div class="form-group imgs_video text-center">
            <div class="vimgupload">
                <span class="btn btn-primary btn-file" >
                    <i class="fa fa-cloud-upload"></i> 
                        <div>{!! trans("admin_lang.upload_file") !!}</div>
                        <input type="file" name="audio_file[]" class="file_upload" id="file_upload" multiple="multiple"/>
                        <span id="btn-file-error"></span>
                </span>
            </div>
                  </div>
        <h6> <b> Please make sure to avoid using special characters such as & / % # etc. when naming your audio files. Files might not upload correctly otherwise.

                You will be able to match your audio files with your tracks during the 'submission' step.</b></h6>
          <h6 style="color:#FF414E">You will be able to match your audio files with your tracks during the 'submission' step.</h6>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
          <button type="submit" class="btn btn-primary upload_submit" >Upload Files</button>
      </div>

    </div>
  </div>
</div>
       
       
         <div class="box-body">
               <div class="preview"></div>
                                    
               
               <div class="progress" style="display:none;">
  <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar"
  aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:0%">
    0% Complete (success)
  </div>
</div>
                  
                  <table id="userTable" class="table table-bordered table-hover datatable">
                     <thead>
                        <tr>
                           <th class="action">{{ trans("admin_lang.id") }}</th>                 
                           <th class="action">{{ trans('admin_lang.file_information') }}</th>
                           <th class="action">{{ trans('admin_lang.upload')}}</th>
                           <th class="action">{{ trans('admin_lang.type') }}</th>
                           <th class="action" width="110px">{{ trans("admin_lang.action") }}
                           </th>
                        </tr>
                     </thead>
                     <tfoot>
                        <tr>
                           <th>{{ trans("admin_lang.id") }}</th>                 
                           <th>{{ trans('admin_lang.file_information') }}</th>
                           <th>{{ trans('admin_lang.upload')}}</th>
                           <th class="action">{{ trans('admin_lang.type') }}</th>
                           <th class="action" width="110px">{{ trans("admin_lang.action") }}
                        </tr>
                     </tfoot>
                  </table>
               </div>
   </div>
</div>

<script>
$(document).ready(function(){
    
    $('.add_files_upload').click(function(){
        $('#myModal').modal('show');
    });
    
    

    var table = $('#userTable').DataTable({
           "bProcessing": true,
           "serverSide": true,
           "pageLength": 100,
           "searching": false,
           "order": [[0, "asc"]],
            "bPaginate": false,
           "ajax": {
               url: "{{ routeUser('track.albums',['id'=>$sdata->id]) }}",
               data: function (d) {
                    return $.extend({}, d, {
                        "start_date": $('.sdate').val(),
                        "end_date": $('.edate').val(),
                    });
                },
               error: function () {
                   alert("{{trans('admin_lang.something_went_wrong')}}");
               }
           },
          
           "aoColumns": [
               {mData: 'DT_RowId'},
               {mData: 'title'},
               {mData: 'upload'},
               {mData: 'type'},
               {mData: 'actions'}
           ],
           "aoColumnDefs": [
               {"bSortable": false, "aTargets": ['action']}
           ],
            language: {
            searchPlaceholder: "Search"
        },
       });
       

        $('#file_upload').change(function(){
            var fi = document.getElementById('file_upload');
            var type = 'audio';
            FileDetails_audio(fi,'btn-file-error',type);
        });
        
        function FileDetails_audio(fi,cla,type) {
            
             if (fi.files.length > 0 && fi.files.length < 20) {
                 document.getElementById(cla).innerHTML =
                        'Total Files: <b>' + fi.files.length + '</b>';
                 for (var i = 0; i <= fi.files.length - 1; i++) {
                     if(type !='imgInp_all')
                     {
                        var result = readURL(fi.files.item(i),type);                       
                        if(result=='false')
                        {
                            return false;
                        }
                     }
                     
                     var fname = fi.files.item(i).name;
                     var fsize = fi.files.item(i).size;
                     //if(fsize<'20971520')
                    // {
                        document.getElementById(cla).innerHTML =
                        document.getElementById(cla).innerHTML + '<br /> ' +
                        fname + ' (<b>' + fsize + '</b> bytes)';
                    /*}
                    else
                    {                       
                       $('#'+cla).html('');
                       $(fi.files).val('');
                       $('#'+cla).text("Only flie size 10 MB  allowed");
                       return false;
                    }*/
                 }
             }
             else{
                       $('#'+cla).html('');
                       $(fi.files).val('');
                       $('#'+cla).text(" You can only upload a maximum of 20 files.");
                       return false;
             }
             
        }
        function readURL(input,type) {
            if (input) {              
                var type_reg = /^audio\/(FLAC|flac|AIFF|aiff|WAV|wav)$/;
                var selecttype =  input.type;
                if (type_reg.test(selecttype)) {
                   
                } else {
                    $(input).val('');
                    $('#btn-file-error').text("Only formats are allowed :WAV, FLAC, WMA");
                    return 'false';
                }  
           } 
       }




    var progressbar= $('.progress-bar');
    $('.upload_submit').on('submit', function(event){       
       // $(".loding_img").show();
       $('#myModal').modal('hide');
       event.preventDefault();
        $.ajax({
            url:"{{ routeUser('track.upload')}}",
            method:"POST",
            data:new FormData(this),
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $(".progress").css("display","block");
                progressbar.width('0%');
                progressbar.text('0%');
                $('.form_submit').hide();
            },
            uploadProgress: function (event, position, total, percentComplete) {
                progressbar.width(percentComplete + '%');
		progressbar.text(percentComplete + '%');
                console.log(percentComplete);
            },
            xhr: function() {
        var xhr = new window.XMLHttpRequest();
        xhr.upload.addEventListener("progress", function(evt) {
            console.log(evt.loaded);
            console.log(evt.total);            
            console.log(evt);            
            if (evt.lengthComputable) {
                var percentComplete = (evt.loaded / evt.total) * 100;                
                progressbar.width(percentComplete.toFixed(2) + '%');
		progressbar.text(percentComplete.toFixed(2) + '% Processing...');
            }
       }, false);
       return xhr;
    },
    success:function(data)
            {
               $('.form_submit').show();
               // $(".loding_img").hide();
                 $('#btn-file-error').html('');
                 $('#file_upload').val('');
                if(data.status==false)
                {
                    $('.progress').hide();
                    $('.content-header1').html('<div class="alert alert-danger"><strong>Danger!</strong> '+data.message+'</div>')
                }
                else{
                   table.ajax.reload(); 
                    if(data.type=='Both')
                    {
                        progressbar.text( '100 % Process completed');
                        $('.content-header1').html('<div class="alert alert-success"><strong>Success!</strong> '+data.success+'</div><div class="alert alert-warning">  <strong>Warning!</strong> '+data.invalid+'</div>');
                    }
                    else if(data.type=='itype'){
                        progressbar.text( '100 % Process completed');
                        $('.content-header1').html('<div class="alert alert-success"><strong>Success!</strong> '+data.success+'</div>');                        
                    }
                    else if(data.type=='ktype'){
                         progressbar.text( 'Process error');
                         $('.content-header1').html('<div class="alert alert-warning">  <strong>Warning!</strong> '+data.invalid+'</div>');                       
                    }
                }
                
            }
        });                
    });

});

      
     
    </script>