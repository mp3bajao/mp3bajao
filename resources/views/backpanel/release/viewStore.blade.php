<div class="modal-body" style="height: 570px;overflow-y: scroll;">
  <?php 
  $itunes_stores = array();
  $boomplay_stores = array();
  $hungama_stores = array();
  $shemaroo_stores = array();
  // $itunes_send = array();
  // $boomplay_send = array();
  // $hungama_send = array();
  // $shemaroo_send = array();
  ?>
  <?php if(Auth::user()->role_id==1){?>
  <div id="exTab2" class=""> 
    <ul class="nav nav-tabs">
      <li class="active">
        <a href="#neumeta" data-toggle="tab" data-target="#neumeta, #neumeta_footer">NeuMeta</a>
      </li>
      <li>
        <a href="#itunes" data-toggle="tab" data-target="#itunes, #itunes_footer">iTunes</a>
      </li>
      <li>
        <a href="#boomplay" data-toggle="tab" data-target="#boomplay, #boomplay_footer">BoomPlay</a>
      </li>
      <li>
        <a href="#hungama" data-toggle="tab" data-target="#hungama, #hungama_footer">Hungama</a>
      </li>
      <li>
        <a href="#shemaroo" data-toggle="tab" data-target="#shemaroo, #shemaroo_footer">Shemaroo</a>
      </li>
      <li>
        <a href="#pandora" data-toggle="tab" data-target="#pandora, #pandora_footer">Pandora</a>
      </li>
      <li>
        <a href="#deezer" data-toggle="tab" data-target="#deezer, #deezer_footer">Deezer</a>
      </li>
    </ul>

    <div class="tab-content row">
      <div class="tab-pane active" id="neumeta">
          
        @forelse($data->getStoreRelease as $store)  
          @if($store->getStore['receiver']=="NEUMETA")
            @if($store_type==1)
              @if($store->is_send==1)
                <div class="col-sm-2">
                  <div class="boxsetup" style="border: 1px solid #ccc; margin: 5px;height: 200px;padding: 15px;text-align: center;box-shadow: 0px 0px 0px 5px aliceblue;">
                    <img src="{{asset($store->getStore->image)}}" style="width:100%;"><br/>
                    <span>{{$store->getStore->title}} @if($data->release_current_status=='NONE' && $store->is_send==0)  <i class="fa fa-cloud-upload" style="color:#555"></i> @else @if($store->is_send==1) <i class="fa fa-check-circle" style="color:green"></i> @else <i class="fa fa-times-circle" style="color:red"></i> @endif @endif</span><br>
                    @if(isset($store->store_url) && $store->store_url!='')
                    <a href="{{ $store->store_url }}" class="btn btn-xs btn-success" target="_blank">View on Store <i class="fa fa-external-link"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-primary" data-url="{{ $store->store_url }}" onclick="copyToClipboard($(this))" style="margin-top: 5px;">Copy URL <i class="fa fa-copy"></i></a>
                    @endif
                  </div>
                </div>
              @endif
            @endif
            @if($store_type==0)
              <div class="col-sm-2">
                <div class="boxsetup" style="border: 1px solid #ccc; margin: 5px;height: 200px;padding: 15px;text-align: center;box-shadow: 0px 0px 0px 5px aliceblue;">
                  <img src="{{asset($store->getStore->image)}}" style="width:100%;"><br/>
                    <span>{{$store->getStore->title}} @if($data->release_current_status=='NONE' &&  $store->is_send==0)  <i class="fa fa-cloud-upload" style="color:#555"></i> @else @if($store->is_send==1) <i class="fa fa-check-circle" style="color:green"></i> @else <i class="fa fa-times-circle" style="color:red"></i> @endif @endif</span><br>
                    @if(isset($store->store_url) && $store->store_url!='')
                      <a href="{{ $store->store_url }}" class="btn btn-xs btn-success" target="_blank">View on Store <i class="fa fa-external-link"></i></a>
                      <a href="javascript:;" class="btn btn-xs btn-primary" data-url="{{ $store->store_url }}" onclick="copyToClipboard($(this))" style="margin-top: 5px;">Copy URL <i class="fa fa-copy"></i></a>
                    @endif
                </div>
              </div>
            @endif
          @endif
        @empty
        @endforelse
      </div>
      <div class="tab-pane" id="itunes">
        @forelse($data->getStoreRelease as $store)
          @if($store->getStore['receiver']=="ITUNES")
            <?php $itunes_stores[] = $store->getStore->id; ?>
            @if($store_type==1)
              @if($store->is_send==1)
                <div class="col-sm-2">
                  <div class="boxsetup" style="border: 1px solid #ccc; margin: 5px;height: 200px;padding: 15px;text-align: center;box-shadow: 0px 0px 0px 5px aliceblue;">
                    <img src="{{asset($store->getStore->image)}}" style="width:100%;"><br/>
                    <span>{{$store->getStore->title}} @if($data->release_current_status=='NONE' && $store->is_send==0)  <i class="fa fa-cloud-upload" style="color:#555"></i> @else @if($store->is_send==1) <i class="fa fa-check-circle" style="color:green"></i> @else <i class="fa fa-times-circle" style="color:red"></i> @endif @endif</span><br>
                    @if(isset($store->store_url) && $store->store_url!='')
                    <a href="{{ $store->store_url }}" class="btn btn-xs btn-success" target="_blank">View on Store <i class="fa fa-external-link"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-primary" data-url="{{ $store->store_url }}" onclick="copyToClipboard($(this))" style="margin-top: 5px;">Copy URL <i class="fa fa-copy"></i></a>
                    @endif
                  </div>
                </div>
              @endif
            @endif
            @if($store_type==0)
              <div class="col-sm-2">
                <div class="boxsetup" style="border: 1px solid #ccc; margin: 5px;height: 200px;padding: 15px;text-align: center;box-shadow: 0px 0px 0px 5px aliceblue;">
                  <img src="{{asset($store->getStore->image)}}" style="width:100%;"><br/>
                    <span>{{$store->getStore->title}} @if($data->release_current_status=='NONE' &&  $store->is_send==0)  <i class="fa fa-cloud-upload" style="color:#555"></i> @else @if($store->is_send==1) <i class="fa fa-check-circle" style="color:green"></i> @else <i class="fa fa-times-circle" style="color:red"></i> @endif @endif</span><br>
                    @if(isset($store->store_url) && $store->store_url!='')
                      <a href="{{ $store->store_url }}" class="btn btn-xs btn-success" target="_blank">View on Store <i class="fa fa-external-link"></i></a>
                      <a href="javascript:;" class="btn btn-xs btn-primary" data-url="{{ $store->store_url }}" onclick="copyToClipboard($(this))" style="margin-top: 5px;">Copy URL <i class="fa fa-copy"></i></a>
                    @endif
                </div>
              </div>
            @endif
          @endif
        @empty
        @endforelse
      </div>
        
      <div class="tab-pane" id="boomplay">
        @forelse($data->getStoreRelease as $store)
          @if($store->getStore['receiver']=="BOOMPLAY")
            <?php $boomplay_stores[] = $store->getStore->id; ?>
            @if($store_type==1)
              @if($store->is_send==1)
                <div class="col-sm-2">
                  <div class="boxsetup" style="border: 1px solid #ccc; margin: 5px;height: 200px;padding: 15px;text-align: center;box-shadow: 0px 0px 0px 5px aliceblue;">
                    <img src="{{asset($store->getStore->image)}}" style="width:100%;"><br/>
                    <span>{{$store->getStore->title}} @if($data->release_current_status=='NONE' && $store->is_send==0)  <i class="fa fa-cloud-upload" style="color:#555"></i> @else @if($store->is_send==1) <i class="fa fa-check-circle" style="color:green"></i> @else <i class="fa fa-times-circle" style="color:red"></i> @endif @endif</span><br>
                    @if(isset($store->store_url) && $store->store_url!='')
                    <a href="{{ $store->store_url }}" class="btn btn-xs btn-success" target="_blank">View on Store <i class="fa fa-external-link"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-primary" data-url="{{ $store->store_url }}" onclick="copyToClipboard($(this))" style="margin-top: 5px;">Copy URL <i class="fa fa-copy"></i></a>
                    @endif
                  </div>
                </div>
              @endif
            @endif
            @if($store_type==0)
              <div class="col-sm-2">
                <div class="boxsetup" style="border: 1px solid #ccc; margin: 5px;height: 200px;padding: 15px;text-align: center;box-shadow: 0px 0px 0px 5px aliceblue;">
                  <img src="{{asset($store->getStore->image)}}" style="width:100%;"><br/>
                    <span>{{$store->getStore->title}} @if($data->release_current_status=='NONE' &&  $store->is_send==0)  <i class="fa fa-cloud-upload" style="color:#555"></i> @else @if($store->is_send==1) <i class="fa fa-check-circle" style="color:green"></i> @else <i class="fa fa-times-circle" style="color:red"></i> @endif @endif</span><br>
                    @if(isset($store->store_url) && $store->store_url!='')
                      <a href="{{ $store->store_url }}" class="btn btn-xs btn-success" target="_blank">View on Store <i class="fa fa-external-link"></i></a>
                      <a href="javascript:;" class="btn btn-xs btn-primary" data-url="{{ $store->store_url }}" onclick="copyToClipboard($(this))" style="margin-top: 5px;">Copy URL <i class="fa fa-copy"></i></a>
                    @endif
                </div>
              </div>
            @endif
          @endif
        @empty
        @endforelse
      </div>
        
      <div class="tab-pane" id="hungama">
        @forelse($data->getStoreRelease as $store)
          @if($store->getStore['receiver']=="HUNGAMA")
            <?php $hungama_stores[] = $store->getStore->id; ?>
            @if($store_type==1)
              @if($store->is_send==1)
                <div class="col-sm-2">
                  <div class="boxsetup" style="border: 1px solid #ccc; margin: 5px;height: 200px;padding: 15px;text-align: center;box-shadow: 0px 0px 0px 5px aliceblue;">
                    <img src="{{asset($store->getStore->image)}}" style="width:100%;"><br/>
                    <span>{{$store->getStore->title}} @if($data->release_current_status=='NONE' && $store->is_send==0)  <i class="fa fa-cloud-upload" style="color:#555"></i> @else @if($store->is_send==1) <i class="fa fa-check-circle" style="color:green"></i> @else <i class="fa fa-times-circle" style="color:red"></i> @endif @endif</span><br>
                    @if(isset($store->store_url) && $store->store_url!='')
                    <a href="{{ $store->store_url }}" class="btn btn-xs btn-success" target="_blank">View on Store <i class="fa fa-external-link"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-primary" data-url="{{ $store->store_url }}" onclick="copyToClipboard($(this))" style="margin-top: 5px;">Copy URL <i class="fa fa-copy"></i></a>
                    @endif
                  </div>
                </div>
              @endif
            @endif
            @if($store_type==0)
              <div class="col-sm-2">
                <div class="boxsetup" style="border: 1px solid #ccc; margin: 5px;height: 200px;padding: 15px;text-align: center;box-shadow: 0px 0px 0px 5px aliceblue;">
                  <img src="{{asset($store->getStore->image)}}" style="width:100%;"><br/>
                    <span>{{$store->getStore->title}} @if($data->release_current_status=='NONE' &&  $store->is_send==0)  <i class="fa fa-cloud-upload" style="color:#555"></i> @else @if($store->is_send==1) <i class="fa fa-check-circle" style="color:green"></i> @else <i class="fa fa-times-circle" style="color:red"></i> @endif @endif</span><br>
                    @if(isset($store->store_url) && $store->store_url!='')
                      <a href="{{ $store->store_url }}" class="btn btn-xs btn-success" target="_blank">View on Store <i class="fa fa-external-link"></i></a>
                      <a href="javascript:;" class="btn btn-xs btn-primary" data-url="{{ $store->store_url }}" onclick="copyToClipboard($(this))" style="margin-top: 5px;">Copy URL <i class="fa fa-copy"></i></a>
                    @endif
                </div>
              </div>
            @endif
          @endif
        @empty
        @endforelse
      </div>
      <div class="tab-pane" id="shemaroo">
        @forelse($data->getStoreRelease as $store)
          @if($store->getStore['receiver']=="SHEMAROO")
            <?php $shemaroo_stores[] = $store->getStore->id; ?>
            @if($store_type==1)
              @if($store->is_send==1)
                <div class="col-sm-2">
                  <div class="boxsetup" style="border: 1px solid #ccc; margin: 5px;height: 200px;padding: 15px;text-align: center;box-shadow: 0px 0px 0px 5px aliceblue;">
                    <img src="{{asset($store->getStore->image)}}" style="width:100%;"><br/>
                    <span>{{$store->getStore->title}} @if($data->release_current_status=='NONE' && $store->is_send==0)  <i class="fa fa-cloud-upload" style="color:#555"></i> @else @if($store->is_send==1) <i class="fa fa-check-circle" style="color:green"></i> @else <i class="fa fa-times-circle" style="color:red"></i> @endif @endif</span><br>
                    @if(isset($store->store_url) && $store->store_url!='')
                    <a href="{{ $store->store_url }}" class="btn btn-xs btn-success" target="_blank">View on Store <i class="fa fa-external-link"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-primary" data-url="{{ $store->store_url }}" onclick="copyToClipboard($(this))" style="margin-top: 5px;">Copy URL <i class="fa fa-copy"></i></a>
                    @endif
                  </div>
                </div>
              @endif
            @endif
            @if($store_type==0)
              <div class="col-sm-2">
                <div class="boxsetup" style="border: 1px solid #ccc; margin: 5px;height: 200px;padding: 15px;text-align: center;box-shadow: 0px 0px 0px 5px aliceblue;">
                  <img src="{{asset($store->getStore->image)}}" style="width:100%;"><br/>
                    <span>{{$store->getStore->title}} @if($data->release_current_status=='NONE' &&  $store->is_send==0)  <i class="fa fa-cloud-upload" style="color:#555"></i> @else @if($store->is_send==1) <i class="fa fa-check-circle" style="color:green"></i> @else <i class="fa fa-times-circle" style="color:red"></i> @endif @endif</span><br>
                    @if(isset($store->store_url) && $store->store_url!='')
                      <a href="{{ $store->store_url }}" class="btn btn-xs btn-success" target="_blank">View on Store <i class="fa fa-external-link"></i></a>
                      <a href="javascript:;" class="btn btn-xs btn-primary" data-url="{{ $store->store_url }}" onclick="copyToClipboard($(this))" style="margin-top: 5px;">Copy URL <i class="fa fa-copy"></i></a>
                    @endif
                </div>
              </div>
            @endif
          @endif
        @empty
        @endforelse
      </div>
      <div class="tab-pane" id="pandora">
        @forelse($data->getStoreRelease as $store)
          @if($store->getStore['receiver']=="PANDORA")
            <?php $pandora_stores[] = $store->getStore->id; ?>
            @if($store_type==1)
              @if($store->is_send==1)
                <div class="col-sm-2">
                  <div class="boxsetup" style="border: 1px solid #ccc; margin: 5px;height: 200px;padding: 15px;text-align: center;box-shadow: 0px 0px 0px 5px aliceblue;">
                    <img src="{{asset($store->getStore->image)}}" style="width:100%;"><br/>
                    <span>{{$store->getStore->title}} @if($data->release_current_status=='NONE' && $store->is_send==0)  <i class="fa fa-cloud-upload" style="color:#555"></i> @else @if($store->is_send==1) <i class="fa fa-check-circle" style="color:green"></i> @else <i class="fa fa-times-circle" style="color:red"></i> @endif @endif</span><br>
                    @if(isset($store->store_url) && $store->store_url!='')
                    <a href="{{ $store->store_url }}" class="btn btn-xs btn-success" target="_blank">View on Store <i class="fa fa-external-link"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-primary" data-url="{{ $store->store_url }}" onclick="copyToClipboard($(this))" style="margin-top: 5px;">Copy URL <i class="fa fa-copy"></i></a>
                    @endif
                  </div>
                </div>
              @endif
            @endif
            @if($store_type==0)
              <div class="col-sm-2">
                <div class="boxsetup" style="border: 1px solid #ccc; margin: 5px;height: 200px;padding: 15px;text-align: center;box-shadow: 0px 0px 0px 5px aliceblue;">
                  <img src="{{asset($store->getStore->image)}}" style="width:100%;"><br/>
                    <span>{{$store->getStore->title}} @if($data->release_current_status=='NONE' &&  $store->is_send==0)  <i class="fa fa-cloud-upload" style="color:#555"></i> @else @if($store->is_send==1) <i class="fa fa-check-circle" style="color:green"></i> @else <i class="fa fa-times-circle" style="color:red"></i> @endif @endif</span><br>
                    @if(isset($store->store_url) && $store->store_url!='')
                      <a href="{{ $store->store_url }}" class="btn btn-xs btn-success" target="_blank">View on Store <i class="fa fa-external-link"></i></a>
                      <a href="javascript:;" class="btn btn-xs btn-primary" data-url="{{ $store->store_url }}" onclick="copyToClipboard($(this))" style="margin-top: 5px;">Copy URL <i class="fa fa-copy"></i></a>
                    @endif
                </div>
              </div>
            @endif
          @endif
        @empty
        @endforelse
      </div>
      <div class="tab-pane" id="deezer">
        @forelse($data->getStoreRelease as $store)
          @if($store->getStore['receiver']=="DEEZER")
            <?php $deezer_stores[] = $store->getStore->id; ?>
            @if($store_type==1)
              @if($store->is_send==1)
                <div class="col-sm-2">
                  <div class="boxsetup" style="border: 1px solid #ccc; margin: 5px;height: 200px;padding: 15px;text-align: center;box-shadow: 0px 0px 0px 5px aliceblue;">
                    <img src="{{asset($store->getStore->image)}}" style="width:100%;"><br/>
                    <span>{{$store->getStore->title}} @if($data->release_current_status=='NONE' && $store->is_send==0)  <i class="fa fa-cloud-upload" style="color:#555"></i> @else @if($store->is_send==1) <i class="fa fa-check-circle" style="color:green"></i> @else <i class="fa fa-times-circle" style="color:red"></i> @endif @endif</span><br>
                    @if(isset($store->store_url) && $store->store_url!='')
                    <a href="{{ $store->store_url }}" class="btn btn-xs btn-success" target="_blank">View on Store <i class="fa fa-external-link"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-primary" data-url="{{ $store->store_url }}" onclick="copyToClipboard($(this))" style="margin-top: 5px;">Copy URL <i class="fa fa-copy"></i></a>
                    @endif
                  </div>
                </div>
              @endif
            @endif
            @if($store_type==0)
              <div class="col-sm-2">
                <div class="boxsetup" style="border: 1px solid #ccc; margin: 5px;height: 200px;padding: 15px;text-align: center;box-shadow: 0px 0px 0px 5px aliceblue;">
                  <img src="{{asset($store->getStore->image)}}" style="width:100%;"><br/>
                    <span>{{$store->getStore->title}} @if($data->release_current_status=='NONE' &&  $store->is_send==0)  <i class="fa fa-cloud-upload" style="color:#555"></i> @else @if($store->is_send==1) <i class="fa fa-check-circle" style="color:green"></i> @else <i class="fa fa-times-circle" style="color:red"></i> @endif @endif</span><br>
                    @if(isset($store->store_url) && $store->store_url!='')
                      <a href="{{ $store->store_url }}" class="btn btn-xs btn-success" target="_blank">View on Store <i class="fa fa-external-link"></i></a>
                      <a href="javascript:;" class="btn btn-xs btn-primary" data-url="{{ $store->store_url }}" onclick="copyToClipboard($(this))" style="margin-top: 5px;">Copy URL <i class="fa fa-copy"></i></a>
                    @endif
                </div>
              </div>
            @endif
          @endif
        @empty
        @endforelse
      </div>
        
    </div>
  </div>
<?php }else{?>
<div class="row">
<?php $store_id= array(); ?>
        @forelse($data->getStoreRelease as $store)
        <?php $store_id[] = $store->getStore->id; ?>
        @if($store_type==1)
        @if($store->is_send==1)
            <div class="col-sm-2">
                <div class="boxsetup" style="border: 1px solid #ccc; margin: 5px;height: 200px;padding: 15px;text-align: center;box-shadow: 0px 0px 0px 5px aliceblue;">
                    <img src="{{asset($store->getStore->image)}}" style="width:100%;"><br/>                
                <span>{{$store->getStore->title}} @if($data->release_current_status=='NONE' && $store->is_send==0)  <i class="fa fa-cloud-upload" style="color:#555"></i> @else @if($store->is_send==1) <i class="fa fa-check-circle" style="color:green"></i> @else <i class="fa fa-times-circle" style="color:red"></i> @endif @endif</span><br>
                @if(isset($store->store_url) && $store->store_url!='')
                <a href="{{ $store->store_url }}" class="btn btn-xs btn-success" target="_blank">View on Store <i class="fa fa-external-link"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-primary" data-url="{{ $store->store_url }}" onclick="copyToClipboard($(this))" style="margin-top: 5px;">Copy URL <i class="fa fa-copy"></i></a>
                @endif
            </div>
            </div>
         @endif    
        @endif        
         @if($store_type==0)      
            <div class="col-sm-2">
                <div class="boxsetup" style="border: 1px solid #ccc; margin: 5px;height: 200px;padding: 15px;text-align: center;box-shadow: 0px 0px 0px 5px aliceblue;">
                    <img src="{{asset($store->getStore->image)}}" style="width:100%;"><br/>      
                <span>{{$store->getStore->title}} @if($data->release_current_status=='NONE' &&  $store->is_send==0)  <i class="fa fa-cloud-upload" style="color:#555"></i> @else @if($store->is_send==1) <i class="fa fa-check-circle" style="color:green"></i> @else <i class="fa fa-times-circle" style="color:red"></i> @endif @endif</span><br>
                @if(isset($store->store_url) && $store->store_url!='')
                <a href="{{ $store->store_url }}" class="btn btn-xs btn-success" target="_blank">View on Store <i class="fa fa-external-link"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-primary" data-url="{{ $store->store_url }}" onclick="copyToClipboard($(this))" style="margin-top: 5px;">Copy URL <i class="fa fa-copy"></i></a>
                @endif
            </div>
            </div>
        @endif
        @empty        
        @endforelse
</div>
<?php }?>
</div>

<div class="modal-footer">
  @if(Auth::user()->role_id==1 && $store_type!=1)
  <div class="tab-content">
    <div class="tab-pane active" id="neumeta">
      @if($data->release_current_status=='NONE' && $store->is_send==0 ) 
        <a href="{{routeUser('release.final',['action'=>'InitialDelivery','release_id'=>$data->id])}}" class="btn btn-success upload_save">InitialDelivery @if($data->release_current_status=='InitialDelivery') <i class="fa fa-check-circle"></i> @endif</a>
      @else
        <a href="{{routeUser('release.queue',['release_id'=>$data->id])}}" class="btn btn-danger upload_save">Add to Queue</a>
        <a href="javascript:;" data-url_href="{{routeUser('release.final',['action'=>'InitialDelivery','release_id'=>$data->id])}}" class="btn btn-success upload_save">InitialDelivery <i class="fa fa-check-circle"></i></a>
        <a href="javascript:;" data-url_href="{{routeUser('release.final',['action'=>'FullUpdate','release_id'=>$data->id])}}" class="btn btn-warning upload_save">FullUpdate @if($data->release_current_status=='FullUpdate') <i class="fa fa-check-circle"></i> @endif</a>
        <a href="javascript:;" data-url_href="{{routeUser('release.final',['action'=>'MetadataOnlyUpdate','release_id'=>$data->id])}}" class="btn btn-default upload_save">MetadataOnlyUpdate @if($data->release_current_status=='MetadataOnlyUpdate') <i class="fa fa-check-circle"></i> @endif</a>
        <a href="javascript:;" data-url_href="{{routeUser('release.final',['action'=>'TakeDown','release_id'=>$data->id])}}" class="btn btn-primary upload_save">TakeDown @if($data->release_current_status=='TakeDown') <i class="fa fa-check-circle"></i> @endif</a>
      @endif
    </div>
    <div class="tab-pane " id="itunes_footer">
      @if(!empty($itunes_stores))
        <a href="javascript:;" style="background: #000;" data-url_href="{{routeUser('release.final_yat',['action'=>'YATDelivery','release_id'=>$data->id])}}" class="btn btn-info upload_save">Delivery On Youtube, Amazon and Itune @if($data->release_current_status=='YATDelivery') <i class="fa fa-check-circle"></i> @endif</a>
      @endif
    </div>
    <div class="tab-pane " id="boomplay_footer">
     <?php //&& $data->release_current_status=='InitialDelivery' ?>
      @if(!empty($boomplay_stores))
        <a href="javascript:;" data-url_href="{{routeUser('release.final_sftp',['action'=>'InitialDelivery', 'receiver'=>'BOOMPLAY', 'release_id'=>$data->id])}}" class="btn btn-success upload_save">InitialDelivery</a>
        <a href="javascript:;" data-url_href="{{routeUser('release.final_sftp',['action'=>'MetadataOnlyUpdate', 'receiver'=>'BOOMPLAY', 'release_id'=>$data->id])}}" class="btn btn-default upload_save">MetadataOnlyUpdate</a>
        <a href="javascript:;" data-url_href="{{routeUser('release.final_sftp',['action'=>'TakeDown', 'receiver'=>'BOOMPLAY', 'release_id'=>$data->id])}}" class="btn btn-primary upload_save">TakeDown</a>
        <a href="javascript:;" data-url_href="{{routeUser('release.final_sftp',['action'=>'FullUpdate','receiver'=>'BOOMPLAY','release_id'=>$data->id])}}" class="btn btn-warning upload_save">FullUpdate @if($data->release_current_status=='FullUpdate') <i class="fa fa-check-circle"></i> @endif</a>
      @endif
    </div>
    <div class="tab-pane " id="hungama_footer">
      @if(!empty($hungama_stores))
        <a href="javascript:;" style="background: #000;" data-url_href="{{routeUser('release.final_sftp',['action'=>'InitialDelivery', 'receiver'=>'HUNGAMA', 'release_id'=>$data->id])}}" class="btn btn-info upload_save">InitialDelivery</a>
        <a href="javascript:;" data-url_href="{{routeUser('release.final_sftp',['action'=>'MetadataOnlyUpdate', 'receiver'=>'HUNGAMA', 'release_id'=>$data->id])}}" class="btn btn-default upload_save">MetadataOnlyUpdate</a>
        <a href="javascript:;" data-url_href="{{routeUser('release.final_sftp',['action'=>'TakeDown', 'receiver'=>'HUNGAMA', 'release_id'=>$data->id])}}" class="btn btn-primary upload_save">TakeDown</a>
        <a href="javascript:;" data-url_href="{{routeUser('release.final_sftp',['action'=>'FullUpdate','receiver'=>'HUNGAMA','release_id'=>$data->id])}}" class="btn btn-warning upload_save">FullUpdate @if($data->release_current_status=='FullUpdate') <i class="fa fa-check-circle"></i> @endif</a>
        @endif
    </div>
    <div class="tab-pane " id="shemaroo_footer">
      @if(!empty($shemaroo_stores))
        <a href="javascript:;" style="background: #000;" data-url_href="{{routeUser('release.final_sftp',['action'=>'InitialDelivery', 'receiver'=>'SHEMAROO', 'release_id'=>$data->id])}}" class="btn btn-info upload_save">InitialDelivery</a>
      @endif
    </div>
   <div class="tab-pane " id="pandora_footer">
      @if(!empty($pandora_stores))
        <a href="javascript:;" data-url_href="{{routeUser('release.final_sftp',['action'=>'InitialDelivery', 'receiver'=>'PANDORA', 'release_id'=>$data->id])}}" class="btn btn-success upload_save">InitialDelivery</a>
        <a href="javascript:;" data-url_href="{{routeUser('release.final_sftp',['action'=>'MetadataOnlyUpdate', 'receiver'=>'PANDORA', 'release_id'=>$data->id])}}" class="btn btn-default upload_save">MetadataOnlyUpdate</a>
        <a href="javascript:;" data-url_href="{{routeUser('release.final_sftp',['action'=>'TakeDown', 'receiver'=>'PANDORA', 'release_id'=>$data->id])}}" class="btn btn-primary upload_save">TakeDown</a>
        <a href="javascript:;" data-url_href="{{routeUser('release.final_sftp',['action'=>'FullUpdate','receiver'=>'PANDORA','release_id'=>$data->id])}}" class="btn btn-warning upload_save">FullUpdate @if($data->release_current_status=='FullUpdate') <i class="fa fa-check-circle"></i> @endif</a>
      @endif
    </div>
    <div class="tab-pane " id="deezer_footer">
      @if(!empty($deezer_stores))
        <a href="javascript:;" data-url_href="{{routeUser('release.final_sftp',['action'=>'InitialDelivery', 'receiver'=>'DEEZER', 'release_id'=>$data->id])}}" class="btn btn-success upload_save">InitialDelivery</a>
        <a href="javascript:;" data-url_href="{{routeUser('release.final_sftp',['action'=>'MetadataOnlyUpdate', 'receiver'=>'DEEZER', 'release_id'=>$data->id])}}" class="btn btn-default upload_save">MetadataOnlyUpdate</a>
        <a href="javascript:;" data-url_href="{{routeUser('release.final_sftp',['action'=>'TakeDown', 'receiver'=>'DEEZER', 'release_id'=>$data->id])}}" class="btn btn-primary upload_save">TakeDown</a>
        <a href="javascript:;" data-url_href="{{routeUser('release.final_sftp',['action'=>'FullUpdate','receiver'=>'DEEZER','release_id'=>$data->id])}}" class="btn btn-warning upload_save">FullUpdate @if($data->release_current_status=='FullUpdate') <i class="fa fa-check-circle"></i> @endif</a>
      @endif
    </div>
      
  </div>
  @endif
  <?php /*
  @if(Auth::user()->role_id==1)
      @if($store_type!=1)
        @if($data->release_current_status=='NONE' && $store->is_send==0 ) 
            <a href="{{routeUser('release.final',['action'=>'InitialDelivery','release_id'=>$data->id])}}" class="btn btn-success upload_save">InitialDelivery @if($data->release_current_status=='InitialDelivery') <i class="fa fa-check-circle"></i> @endif</a>
        @else
            <a href="{{routeUser('release.queue',['release_id'=>$data->id])}}" class="btn btn-danger upload_save">Add to Queue</a>
            <a href="javascript:;" data-url_href="{{routeUser('release.final',['action'=>'InitialDelivery','release_id'=>$data->id])}}" class="btn btn-success upload_save">InitialDelivery <i class="fa fa-check-circle"></i></a>
            <a href="javascript:;" data-url_href="{{routeUser('release.final',['action'=>'FullUpdate','release_id'=>$data->id])}}" class="btn btn-warning upload_save">FullUpdate @if($data->release_current_status=='FullUpdate') <i class="fa fa-check-circle"></i> @endif</a>
            <a href="javascript:;" data-url_href="{{routeUser('release.final',['action'=>'MetadataOnlyUpdate','release_id'=>$data->id])}}" class="btn btn-default upload_save">MetadataOnlyUpdate @if($data->release_current_status=='MetadataOnlyUpdate') <i class="fa fa-check-circle"></i> @endif</a>
            <a href="javascript:;" data-url_href="{{routeUser('release.final',['action'=>'TakeDown','release_id'=>$data->id])}}" class="btn btn-primary upload_save">TakeDown @if($data->release_current_status=='TakeDown') <i class="fa fa-check-circle"></i> @endif</a>
            @if(in_array(22,$store_id)||in_array(23,$store_id)||in_array(24,$store_id))
              <a href="javascript:;" style="background: #000;" data-url_href="{{routeUser('release.final_yat',['action'=>'YATDelivery','release_id'=>$data->id])}}" class="btn btn-info upload_save">Delivery On Youtube, Amazon and Itune @if($data->release_current_status=='YATDelivery') <i class="fa fa-check-circle"></i> @endif</a>
            @endif
        @endif
      @endif    
  @endif */?>
</div>
<style type="text/css">
  .modal-body img{max-width: 105px;}
</style>
<script>
$(document).on('click','.setstore',function(){
   var status = 0; 
   if($(this).is(':checked'))
   {
     status = 1;  
   }
   else
   {
     status = 0;       
   }
   var id =  $(this).val();
   var release_id = $(this).data('release_id');   
   $(".loding_img").show();
        $.ajax({
            url: "{{ routeUser('ajax.store.set') }}",
            type: 'get',
            data:{status:status,id:id,release_id:release_id},
            success: function (data) 
            {
                  $(".loding_img").hide();
            },               
        });   
});

$(document).ready(function(){
   $(document).on('click','.upload_save',function(){
        $(".loding_img").show();
        var url = $(this).data('url_href');
        location.replace(url);
   }); 
});
function copyToClipboard(element) {
  var $temp = $("<input>");
  $("#myModalRelease").append($temp);//alert(element.data('url'));
  $temp.val(element.data('url')).select();
  document.execCommand("copy");
  $temp.remove();
}
</script>
