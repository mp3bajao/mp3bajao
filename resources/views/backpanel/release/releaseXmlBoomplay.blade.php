<?php use App\Models\Track; ?>
<?xml version="1.0" encoding="utf-8"?>
<ern38:NewReleaseMessage
    xmlns:ern38="http://ddex.net/xml/ern/38"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    LanguageAndScriptCode="en"
    xsi:schemaLocation="http://ddex.net/xml/ern/38 http://ddex.net/xml/ern/38/release-notification.xsd"
    MessageSchemaVersionId="ern/38">

    <MessageHeader>
    <MessageThreadId>{!! $release->UPC !!}</MessageThreadId>
    <MessageId>{!! $release->ftp_one_folder !!}</MessageId>
    <MessageSender>
        <PartyId>PADPIDA2019041703R</PartyId>
        <PartyName>
            <FullName>Sanjivani Digital Entertainment Pvt Ltd</FullName>
        </PartyName>
    </MessageSender>
    <MessageRecipient>
      <PartyId>PADPIDA20190314047</PartyId>
      <PartyName>
        <FullName>Transsnet Music Limited</FullName>
      </PartyName>
    </MessageRecipient>
      <?php $dt = dateFormate(date('Y-m-dTH:i:s'));?>
      <MessageCreatedDateTime>{{$dt->format('Y-m-d').'T'.$dt->format('H:i:s').'Z'}}</MessageCreatedDateTime>
      <MessageControlType>LiveMessage</MessageControlType>
    </MessageHeader>
  <UpdateIndicator>{{$updateIndicator}}</UpdateIndicator>
   
  <ResourceList> 
    <?php $i=1; ?>
    @forelse($release->getReleaseTrack as $track)
    <?php $media = $track->getTrackMedia; ?>
    @if(isset($media))
    <?php $i++; ?>
    <?php 
        if(isset($media))
        {
            $fiename = explode('.',$media->folder_path);
            $track_array = explode('_',$fiename[0]);
            $data['track-number']=(int)end($track_array);
        }
        else
        {
            $data['track-number']=0;
        }
        ?>

    <SoundRecording>
        <SoundRecordingType>MusicalWorkSoundRecording</SoundRecordingType>
        <SoundRecordingId>
             <ISRC>{{ $track->ISRC }}</ISRC>
        </SoundRecordingId>
        <!-- key -->
        <ResourceReference>A{{ $data['track-number'] }}</ResourceReference>

        <ReferenceTitle>
            <TitleText>{!!str_replace('&',' and ',$track->title)!!}</TitleText>
            <SubTitle>{!!str_replace('&',' and ',$track->subtitle)!!}</SubTitle>
        </ReferenceTitle>
        <LanguageOfPerformance>{{strtolower($track->title_language)}}</LanguageOfPerformance>
        <?php $duration =  explode(':',$media->file_duration);  ?>
        <Duration>{{ 'PT'.$duration[0].'H'.$duration[1].'M'.$duration[2].'S' }}</Duration>
        
        <SoundRecordingDetailsByTerritory>
            <TerritoryCode>Worldwide</TerritoryCode>
            <Title TitleType="DisplayTitle">
                <TitleText>{!!str_replace('&',' and ',$track->title)!!}</TitleText>
                <SubTitle>{!!str_replace('&',' and ',$track->subtitle)!!}</SubTitle>
            </Title>            
            <?php $main_artist = explode('|',$track->primary_artist); ?>
            @forelse($main_artist as $k => $main_artist)
            <DisplayArtist>
              <PartyName>
                <FullName>{!!str_replace('&',' and ',$main_artist)!!}</FullName>
              </PartyName>
              <ArtistRole>MainArtist</ArtistRole>
            </DisplayArtist>
            @empty
            @endforelse
            
            
            

            <?php $main_producer = explode('|',$track->producer); ?>
            @forelse($main_producer as $k => $main_producer)
            <ResourceContributor>
              <PartyName>
                <FullName>{!!str_replace('&',' and ',$main_producer)!!}</FullName>
              </PartyName>
              <ResourceContributorRole>Producer</ResourceContributorRole>
            </ResourceContributor>
            @empty
            @endforelse
            
            <?php $main_composer = explode('|',$track->arranger); ?>
            @forelse($main_composer as $k => $main_composer)
            <IndirectResourceContributor>
              <PartyName>
                <FullName>{!!str_replace('&',' and ',$main_composer)!!}</FullName>
              </PartyName>
              <IndirectResourceContributorRole>Composer</IndirectResourceContributorRole>
            </IndirectResourceContributor>
            @empty
            @endforelse
            
             <?php $main_lyricist = explode('|',$track->author); ?>
            @forelse($main_lyricist as $k => $main_lyricist)
            <IndirectResourceContributor>
              <PartyName>
                <FullName>{!!str_replace('&',' and ',$main_lyricist)!!}</FullName>
              </PartyName>
              <IndirectResourceContributorRole>Lyricist</IndirectResourceContributorRole>
            </IndirectResourceContributor>
            @empty
            @endforelse
            
            <?php $main_publisher = explode('|',$track->publisher); ?>
            @forelse($main_publisher as $k => $main_publisher)
            <IndirectResourceContributor>
              <PartyName>
                <FullName>{!!str_replace('&',' and ',$main_publisher)!!}</FullName>
              </PartyName>
              <IndirectResourceContributorRole>MusicPublisher</IndirectResourceContributorRole>
            </IndirectResourceContributor>
            @empty
            @endforelse
                        
            <LabelName>{!! str_replace('&',' and ',$release->label_value) !!}</LabelName>
            <PLine>
                <Year>{!! $track->production_year !!}</Year>
                <PLineText>{!! str_replace('&',' and ',$track->p_line) !!}</PLineText>
            </PLine>
            <Genre>
                <GenreText>{!! str_replace('&',' and ',$release->genre_value) !!}</GenreText>
                <SubGenre>{!! str_replace('&',' and ',$release->subgenre_value) !!}</SubGenre>
            </Genre>
            <ParentalWarningType>NotExplicit</ParentalWarningType>
            
            <TechnicalSoundRecordingDetails>
                <TechnicalResourceDetailsReference>T{{ $data['track-number'] }}</TechnicalResourceDetailsReference>
                <AudioCodecType>FLAC</AudioCodecType>
                <BitRate>1411</BitRate>
                <NumberOfChannels>2</NumberOfChannels>
                <SamplingRate>44100</SamplingRate>
                <BitsPerSample>16</BitsPerSample>
                <IsPreview>false</IsPreview>
                <PreviewDetails>
                    <StartPoint>20</StartPoint>
                    <ExpressionType>Instructive</ExpressionType>
                </PreviewDetails>
                <File>
                    <FileName>{!! $media->folder_path!!}</FileName>
                    <FilePath>resources/</FilePath> <!--FilePath>resources/</FilePath-->
                    <HashSum>
                        <HashSum>{!! md5($data['track-number']) !!}</HashSum>
                        <HashSumAlgorithmType>MD5</HashSumAlgorithmType>
                    </HashSum>
                </File>
            </TechnicalSoundRecordingDetails>
        </SoundRecordingDetailsByTerritory>
    </SoundRecording>

    @endif
    @empty
    @endforelse

    <Image IsUpdated="true">
        <ImageType>FrontCoverImage</ImageType>
        <ImageId>
            <ProprietaryId Namespace="DPID:PADPIDA2019041703R">{{ $release->UPC }}</ProprietaryId>
        </ImageId>
        <!-- key -->
        <ResourceReference>A{{ $i }}</ResourceReference>
        <ImageDetailsByTerritory>
            <TerritoryCode>Worldwide</TerritoryCode>
            <TechnicalImageDetails>
                <!-- key -->
                <TechnicalResourceDetailsReference>T{{ $i }}</TechnicalResourceDetailsReference>
                <ImageCodecType>JPEG</ImageCodecType>
                <ImageHeight>3000</ImageHeight>
                <ImageWidth>3000</ImageWidth>
                <AspectRatio>1.1</AspectRatio>
                <ImageResolution>300</ImageResolution>
                <File>
                    <FileName>{!! $release->folder_path !!}</FileName>
                    <FilePath>resources/</FilePath><!--FilePath>resources/</FilePath-->
                    <!-- key -->
                    <HashSum>
                      <HashSum>{!! md5($data['track-number']) !!}</HashSum>
                      <HashSumAlgorithmType>MD5</HashSumAlgorithmType>
                    </HashSum>
                </File>
            </TechnicalImageDetails>
        </ImageDetailsByTerritory>
    </Image>
</ResourceList>
 
<ReleaseList>
    <Release>
        <ReleaseId>
            <ICPN IsEan="false">{{ $release->UPC }}</ICPN>
            <CatalogNumber Namespace="DPID:PADPIDA2019041703R">{{str_replace('&',' and ',$release->producer_catalogue_number)}}</CatalogNumber>
        </ReleaseId>
        <ReleaseReference>R0</ReleaseReference>
        <ReferenceTitle>
            <TitleText>{{str_replace('&',' and ',$release->title)}}</TitleText>
        </ReferenceTitle>
        <ReleaseResourceReferenceList>
            @forelse($release->getReleaseTrack as $track)
            <?php $media = $track->getTrackMedia; ?>
            @if(isset($media))    
            <?php 
            if(isset($media))
            {
                $fiename = explode('.',$media->folder_path);
                $track_array = explode('_',$fiename[0]);
                $data['track-number']=(int)end($track_array);
            }
            else
            {
                $data['track-number']=0;
            }
            ?>
            <ReleaseResourceReference ReleaseResourceType="PrimaryResource">A{{$data['track-number']}}</ReleaseResourceReference>
            @endif
            @empty
            @endforelse
        </ReleaseResourceReferenceList>
        <ReleaseType>{{ ucfirst(strtolower($release->format))}}</ReleaseType>
        <ReleaseDetailsByTerritory>
            <TerritoryCode>Worldwide</TerritoryCode>
            
                <?php 
                $primary_artist = Track::where('release_id',$release->id)->pluck('primary_artist')->toArray();
                $primary_artist =array_unique(explode('|',str_replace(',','|',implode('|',$primary_artist))));?>
                @if(count($primary_artist)>3)
                 <DisplayArtistName>Various Artists</DisplayArtistName>
                @else
                @forelse($primary_artist as $k => $main_artist)
                    <DisplayArtistName>{!!str_replace('&',' and ',$main_artist)!!}</DisplayArtistName>
            @empty
            @endforelse
            @endif
                
            
            
            <LabelName>{{ str_replace('&',' and ',$release->label_value) }}</LabelName>
            <Title TitleType="FormalTitle">
                <TitleText>{{ str_replace('&',' and ',$release->title) }}</TitleText>
            </Title>
            <Title TitleType="DisplayTitle">
                <TitleText>{{ str_replace('&',' and ',$release->title) }}</TitleText>
            </Title>
           @if(count($primary_artist)>3)
            <DisplayArtist>                 
              <PartyName>
                <FullName>Various Artists</FullName>
              </PartyName>
                <ArtistRole>MainArtist</ArtistRole>
            </DisplayArtist>
                  @else
            @forelse($primary_artist as $k => $main_artist)
            <DisplayArtist>
              <PartyName>
                <FullName>{!!str_replace('&',' and ',$main_artist)!!}</FullName>
              </PartyName>
             <ArtistRole>MainArtist</ArtistRole>
            </DisplayArtist>
            @empty
            @endforelse
            @endif
              
            
           
            
            <ParentalWarningType>NotExplicit</ParentalWarningType>
            <ResourceGroup>
                <ResourceGroup>
                    @forelse($release->getReleaseTrack  as $key => $track)
                    <?php $media = $track->getTrackMedia; ?>
                    @if(isset($media))    
                    <?php 
                    if(isset($media))
                    {
                        $fiename = explode('.',$media->folder_path);
                        $track_array = explode('_',$fiename[0]);
                        $data['track-number']=(int)end($track_array);
                    }
                    else
                    {
                        $data['track-number']=0;
                    }
                    ?>
                    
                    <ResourceGroupContentItem>
                        <SequenceNumber>{{$data['track-number']}}</SequenceNumber>
                        <ResourceType>SoundRecording</ResourceType>
                        <ReleaseResourceReference>A{{$data['track-number']}}</ReleaseResourceReference>
                    </ResourceGroupContentItem>                    
                    @endif
                    @empty
                    @endforelse                   
                    
                </ResourceGroup>
                <ResourceGroupContentItem>
                    <ResourceType>Image</ResourceType>
                    <ReleaseResourceReference>A{{$data['track-number']}}</ReleaseResourceReference>
                </ResourceGroupContentItem>
            </ResourceGroup>
            <Genre>
                <GenreText>{!! str_replace('&',' and ',$release->genre_value) !!}</GenreText>
                <SubGenre>{!! str_replace('&',' and ',$release->subgenre_value) !!}</SubGenre>
            </Genre>
            <OriginalReleaseDate>{!! $release->original_release_date !!}</OriginalReleaseDate>
        </ReleaseDetailsByTerritory>
        <PLine>
            <Year>{!! $release->production_year !!}</Year>
            <PLineText>{!! str_replace('&',' and ',$release->p_line) !!}</PLineText>
        </PLine>
        <CLine>
            <Year>{!! $release->production_year !!}</Year>
            <CLineText>{!! $release->production_year !!} /{!! str_replace('&',' and ',$release->c_line) !!}</CLineText>
        </CLine>
    </Release>
    
    
    
  @forelse($release->getReleaseTrack  as $key => $track)
    <?php $media = $track->getTrackMedia; ?>
    @if(isset($media))
    
     <?php 
        if(isset($media))
        {
            $fiename = explode('.',$media->folder_path);
            $track_array = explode('_',$fiename[0]);
            $data['track-number']=(int)end($track_array);
        }
        else
        {
            $data['track-number']=0;
        }
        ?>    
    
    <Release>
        <ReleaseId>
            <ISRC>{!! $track->ISRC !!}</ISRC>
            <ProprietaryId Namespace="DPID:PADPIDA2019041703R">{{$track->id}}</ProprietaryId>
        </ReleaseId>
        <!-- key -->
        <ReleaseReference>R{!! $data['track-number'] !!}</ReleaseReference>
        <ReferenceTitle>
            <TitleText>{!! str_replace('&',' and ',$track->title) !!}</TitleText>
        </ReferenceTitle>        
        <ReleaseResourceReferenceList>
            <ReleaseResourceReference ReleaseResourceType="PrimaryResource">A{!! $data['track-number'] !!}</ReleaseResourceReference>
        </ReleaseResourceReferenceList>
        <ReleaseType>TrackRelease</ReleaseType>
        
        <ReleaseDetailsByTerritory>
            <TerritoryCode>Worldwide</TerritoryCode>
            <?php $main_artist = explode('|',$track->primary_artist); ?>
            @if(count($main_artist)<3)
            @forelse($main_artist as $k => $main_artist)
            <DisplayArtistName>{!!str_replace('&',' and ',$main_artist)!!}</DisplayArtistName>
            @empty
            @endforelse
            @else
            <DisplayArtistName>Various Artists</DisplayArtistName>
            @endif
            
            <LabelName>{!! str_replace('&',' and ',$track->label_value)!!}</LabelName>
            <Title TitleType="FormalTitle">
                <TitleText>{!! str_replace('&',' and ',$track->title) !!}</TitleText>
                <SubTitle>{!! str_replace('&',' and ',$track->subtitle) !!}</SubTitle>
            </Title>
            <Title TitleType="DisplayTitle">
                <TitleText>{!! str_replace('&',' and ',$release->title) !!}</TitleText>
            </Title>
            <?php $main_artist = explode('|',$track->primary_artist); ?>
            @forelse($main_artist as $k => $main_artist)
            <DisplayArtist>
              <PartyName>
                <FullName>{!!str_replace('&',' and ',$main_artist)!!}</FullName>
              </PartyName>
              <ArtistRole>MainArtist</ArtistRole>
            </DisplayArtist>
            @empty
            @endforelse
            <ParentalWarningType>NotExplicit</ParentalWarningType>
            
                <ResourceGroup>
                    <SequenceNumber>{{$data['track-number']}}</SequenceNumber>
                    <ResourceGroupContentItem>
                        <SequenceNumber>{{$data['track-number']}}</SequenceNumber>
                        <ResourceType>SoundRecording</ResourceType>
                        <ReleaseResourceReference ReleaseResourceType="PrimaryResource">A{{$data['track-number']}}</ReleaseResourceReference>
                    </ResourceGroupContentItem>
                </ResourceGroup>
            <Genre>
                <GenreText>{!! str_replace('&',' and ',$release->genre_value) !!}</GenreText>
                <SubGenre>{!! str_replace('&',' and ',$release->subgenre_value) !!}</SubGenre>
            </Genre>
            <OriginalReleaseDate>{{$release->original_release_date}}</OriginalReleaseDate>
        </ReleaseDetailsByTerritory>
        <PLine>
            <Year>{!! $track->production_year !!}</Year>
            <PLineText>{!! str_replace('&',' and ',$track->p_line) !!}</PLineText>
        </PLine>
    </Release>
    @endif
    @empty
    @endforelse
</ReleaseList>
  

  
    <DealList>
       <ReleaseDeal>
        <DealReleaseReference>R0</DealReleaseReference>
        @if($messageType=='NewDealMessage')
        <Deal>
          <DealTerms>
               <TakeDown>true</TakeDown>
              <TerritoryCode>Worldwide</TerritoryCode>
              <ValidityPeriod>
                    <StartDate>{{date('Y-m-d')}}</StartDate>
                </ValidityPeriod>
            </DealTerms>
        </Deal>
        @else
        <Deal>
            <DealTerms>
                <CommercialModelType>AdvertisementSupportedModel</CommercialModelType>
                <Usage>
                    <UseType>NonInteractiveStream</UseType>
                    <UseType>OnDemandStream</UseType>
                </Usage>
                <TerritoryCode>Worldwide</TerritoryCode>                
                <ValidityPeriod>
                    <StartDate>{{date('Y-m-d')}}</StartDate>
                </ValidityPeriod>
            </DealTerms>
        </Deal>
        <Deal>
            <DealTerms>
                <CommercialModelType>SubscriptionModel</CommercialModelType>
                <Usage>
                    <UseType>ConditionalDownload</UseType>
                    <UseType>NonInteractiveStream</UseType>
                    <UseType>OnDemandStream</UseType>
                </Usage>
                <TerritoryCode>Worldwide</TerritoryCode>
                <ValidityPeriod>
                    <StartDate>{{date('Y-m-d')}}</StartDate>
                </ValidityPeriod>
            </DealTerms>
        </Deal>
        @endif
        <EffectiveDate>{{date('Y-m-d')}}</EffectiveDate>
    </ReleaseDeal>
      @forelse($release->getReleaseTrack  as $key => $track)
    <?php $media = $track->getTrackMedia; ?>
    @if(isset($media))
    
     <?php 
        if(isset($media))
        {
            $fiename = explode('.',$media->folder_path);
            $track_array = explode('_',$fiename[0]);
            $data['track-number']=(int)end($track_array);
        }
        else
        {
            $data['track-number']=0;
        }
        ?>
    
    <ReleaseDeal>
        <DealReleaseReference>R{{$data['track-number']}}</DealReleaseReference>
        @if($messageType=='NewDealMessage')
        <Deal>
          <DealTerms>
               <TakeDown>true</TakeDown>
              <TerritoryCode>Worldwide</TerritoryCode>
              <ValidityPeriod>
                    <StartDate>{{date('Y-m-d')}}</StartDate>
                </ValidityPeriod>
            </DealTerms>
        </Deal>
        @else
        <Deal>
            <DealTerms>
                <CommercialModelType>AdvertisementSupportedModel</CommercialModelType>
                <Usage>
                    <UseType>NonInteractiveStream</UseType>
                    <UseType>OnDemandStream</UseType>
                </Usage>
                <TerritoryCode>Worldwide</TerritoryCode>
                <ValidityPeriod>
                    <StartDate>{{date('Y-m-d')}}</StartDate>
                </ValidityPeriod>
            </DealTerms>
        </Deal>
        <Deal>
            <DealTerms>
                <CommercialModelType>SubscriptionModel</CommercialModelType>
                <Usage>
                    <UseType>ConditionalDownload</UseType>
                    <UseType>NonInteractiveStream</UseType>
                    <UseType>OnDemandStream</UseType>
                </Usage>
                <TerritoryCode>Worldwide</TerritoryCode>
                <ValidityPeriod>
                    <StartDate>{{date('Y-m-d')}}</StartDate>
                </ValidityPeriod>
            </DealTerms>
        </Deal>
        @endif
        <EffectiveDate>{{date('Y-m-d')}}</EffectiveDate>
    </ReleaseDeal>
    @endif
    @empty
    @endforelse
</DealList>
</ern38:NewReleaseMessage>
