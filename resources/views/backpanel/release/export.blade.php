@extends('admin.layouts.layout')
@section('content')
<link rel="stylesheet" href="{{ asset('public/admintheme/dist/css/form_custom.css')}}">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         {{ trans('admin_lang.'.$langPath) }}
         <small>{{ trans('admin_lang.export').' '.trans('admin_lang.'.$langPath) }}</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="{!! url('admin/dashboard') !!}"><i class="fa fa-dashboard"></i> {{ trans('admin_lang.home') }}</a></li>
         <li class="active"><a href="{!! routeUser($pagePath.'.index') !!}">{{ trans('admin_lang.'.$langPath) }}</a></li>
         <li class="active"> {{ trans('admin_lang.export').' '.trans('admin_lang.'.$langPath) }}</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
     <div class="row">
         <div class="col-xs-12">
            <div class="box">
               <div class="box-header">
                   <a class="btn btn-social btn-primary" href="{{routeUser($pagePath.'.index')}}">
                        <i class="fa fa-arrow-circle-o-left"></i> {{ trans('admin_lang.back') }}
                        </a>
                  <!-- <h3 class="box-title">Hover Data Table</h3> -->
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="row">
                       <div class="col-sm-12">
                            <div class="bg box box-default">
                                <div class="row">

                                    <div class="col-sm-2">
                                         <div class="form-group ">
                                            <label class="control-label" >{!! trans("admin_lang.status") !!}</label>
                                            {{Form::select('status',['0'=>'Created by Sub-User','1'=>'Submitted for approval','4'=>'Approved','2'=>'Decline'],'',['class'=>'form-control status_select','placeholder'=>'Select Status'])}}                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-2 status_dev">
                                         <div class="form-group ">
                                            <label class="control-label" >Release {!! trans("admin_lang.status") !!}</label>
                                            {{Form::select('release_current_status',['InitialDelivery'=>'InitialDelivery','FullUpdate'=>'FullUpdate','MetadataOnlyUpdate'=>'MetadataOnlyUpdate','TakeDown'=>'TakeDown'],'All',['class'=>'form-control release_current_status','placeholder'=>'Select Current Status'])}}                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                         <div class="form-group ">
                                            <label class="control-label" >User Name</label>
                                            {{Form::select('user_id',$sdata,null,['class'=>'form-control user_id','placeholder'=>'Select Username'])}}
                                        </div>
                                    </div>
                                    
                <div class="col-sm-2 ">               
                  <div class="form-group @if($errors->has('start_date')) has-error @endif ">
                     <label class="control-label" >{!! trans("admin_lang.start_date") !!}</label>
                     {{ Form::text('start_date',date('Y-m-d',strtotime('-2 month',strtotime(date('Y-m-d')))),['class'=>'sdate form-control req_date_time','autocomplete'=>'off','placeholder'=>trans("admin_lang.start_date")])}}
                   @if ($errors->has('start_date'))<span class="error">{{ $errors->first('start_date') }}</span>@endif
                  </div>
               </div>
               <div class="col-sm-2">
                  <div class="form-group @if($errors->has('end_date')) has-error @endif ">
                     <label class="control-label" >{{ trans("admin_lang.end_date") }}</label>
                     {{ Form::text('end_date',date('Y-m-d'),['class'=>'edate form-control req_date_time','autocomplete'=>'off','placeholder'=>trans("admin_lang.end_date")])}}
                  @if ($errors->has('end_date'))<span class="error">{{ $errors->first('end_date') }}</span>@endif
                  </div>
               </div>
               <div class="col-sm-2">
                  <div class="form-group @if($errors->has('label')) has-error @endif ">
                     <label class="control-label" >{{ trans("admin_lang.label") }}</label>
                     {{ Form::text('label',null,['class'=>'form-control label_id','placeholder'=>trans("admin_lang.label")])}}
                  @if ($errors->has('label'))<span class="error">{{ $errors->first('label') }}</span>@endif
                  </div>
               </div>
               <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label" >&nbsp;</label><br/>
                                    <a class="btn export btn-primary" href="javascript:;">
                                            <i class="fa fa-file-excel-o"></i> Export </a>
                                    </div>
                                    </div>
                                </div>                       
                            </div>                           
                       </div>                           
                   </div>                       
               </div>                   
            </div>                
         </div>             
     </div>
   </section>
   <!-- /.content -->
</div>
<script>
    $(document).ready(function(){
    $('.status_select').change(function(){
       if($(this).val()==1 || $(this).val()==0)
       {
           $('.status_dev').hide();
       }
       else{
           $('.status_dev').show();           
       }
    });
    
    $('.export').click(function(){
       var label = $('.label_id').val(); 
       var start_date = $('.sdate').val(); 
       var end_date = $('.edate').val();
       var status = $('.status_select  option:selected').val();
       var release_current_status = $('.release_current_status  option:selected').val();
       var user_id = $('.user_id  option:selected').val();
       window.location.href = "{{routeUser('file.export')}}?start_date="+start_date+'&end_date='+end_date+'&status='+status+'&release_current_status='+release_current_status+'&user_id='+user_id+'&label='+label;
    });
    
$(".sdate").datepicker({
        numberOfMonths: 1,
        dateFormat: 'yy-mm-dd',
        minDate: '-1 year',
        maxDate: 0,
        setDate: new Date(),
        onSelect: function(selected) {
          $(".edate").datepicker("option","minDate", selected)
        }
    });
    $(".edate").datepicker({ 
        numberOfMonths: 1,
        dateFormat: 'yy-mm-dd',
        minDate: '-1 year',
        maxDate: 0,
        setDate: new Date(),
        onSelect: function(selected) {
           $(".sdate").datepicker("option","maxDate", selected)
        }
    });     
    });
</script>
@endsection

