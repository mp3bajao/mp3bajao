<link rel="stylesheet" href="{{ asset('public/admintheme/dist/css/form_custom.css')}}">
<div class="row">
    <div class="col-sm-12">
        <div class="bg box box-default">          
            <ul class="nav nav-tabs">
                <li @if($type =='release') class="active" @endif ><a  href="{{routeUser($pagePath.'.edit',['id'=>encrypt($sdata->id),'type'=>'release'])}}">Release</a></li>
                <li @if($type =='upload_assets') class="active" @endif ><a href="{{routeUser($pagePath.'.edit',['id'=>encrypt($sdata->id),'type'=>'upload_assets'])}}">Upload Assets</a></li>
                <li @if($type =='edit_assets') class="active" @endif ><a href="{{routeUser($pagePath.'.edit',['id'=>encrypt($sdata->id),'type'=>'edit_assets'])}}">Edit Assets</a></li>
                <li @if($type =='price_tire') class="active" @endif ><a href="{{routeUser($pagePath.'.edit',['id'=>encrypt($sdata->id),'type'=>'price_tire'])}}">Price Tier</a></li>
                <li @if($type =='release_date') class="active" @endif ><a href="{{routeUser($pagePath.'.edit',['id'=>encrypt($sdata->id),'type'=>'release_date'])}}">Release Date</a></li>
                <li @if($type =='store') class="active" @endif ><a href="{{routeUser($pagePath.'.edit',['id'=>encrypt($sdata->id),'type'=>'store'])}}">Store</a></li>
                <li @if($type =='submission') class="active" @endif ><a href="{{routeUser($pagePath.'.edit',['id'=>encrypt($sdata->id),'type'=>'submission'])}}">Submission</a></li>
            </ul>
            <div class="tab-content">
                <br>
                @if($type =='release')
                 @include('admin.'.$pagePath.'.release')      
                @elseif($type =='upload_assets')
                @include('admin.'.$pagePath.'.upload_assets')
                @elseif($type =='edit_assets')
                @include('admin.'.$pagePath.'.edit_assets')
                @elseif($type =='price_tire')
                @include('admin.'.$pagePath.'.price_tire')
                @elseif($type =='release_date')
                @include('admin.'.$pagePath.'.release_date')
                @elseif($type =='store')
                @include('admin.'.$pagePath.'.store')
                @elseif($type =='submission')
                @include('admin.'.$pagePath.'.submission')
                @endif
            </div>
            
            <hr>
            <div class="row">
                <div class="col-sm-12">
                    <a href="{{ routeUser($pagePath.'.index') }}" class="btn btn-danger cancel_cls pull-left">{{ trans("admin_lang.cancel") }}</a>
                    @if($type=='submission')
                    @if($total_track>0)
                    @if($e_release+$e_price+$e_release_date+$e_album<1)
                    @if($total_media_count>0)
                    @if($total_media_count==$total_track)
                    
                    <div class="submit_message" style="display: none">
                         
                         
                           <div class="modal" id="myModalSubmit">
  <div class="modal-dialog" style="width:60%">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Submit My Releae</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
          <div class="scroll_page" style="    margin-left: 50px;
    margin-right: 50px;
    height: 400px;
    overflow-y: auto;">
              <h3>CONTENT THAT WILL NOT BE DELIVERED TO ITUNES AND SPOTIFY</h3>
              <ul><li>Any content breaking the rules in the iTunes Style Guide and the Spotify Style Guide</li>
<li>Any content containing artists found in our Hidden Artist list</li>
<li>Tribute, Karaoke and Sound-a-like releases</li>
<li>Releases with fake / keyworded artist names</li>
<li>Generic artists, orchestras and performers (e.g. Chorus, Orchestra, Top Hits, Singer...)</li>
<li>Generic holiday content</li>
<li>Generic genres including:</li>
<dl><dt>
Nature sounds</dt>
<dt>Workout music</dt>
<dt>Fake Chill</dt>
<dt>Fake New Age</dt>
<dt>Relax, Meditation and Yoga music</dt></dl>
<li>Brain power</li>
<li>Duplicated albums, besides Deluxe versions (albums containing at least 50% of the tracklisting of another album)</li>
<li>Tracks delivered more than five times</li>
<li>Low quality cover art (pixelated, off-centred, blurry, scanned...)</li>
<li>Public Domain artists as well as recordings dated pre-1963</li>
<li>Any low budget content with little editorial value</li>
<li>Podcasts and radio broadcasts</li>
<li>Live performances will be subject to the approval of the support team</li>
              </ul>
              <h3>CONTENT THAT WILL NOT BE DELIVERED TO YOUTUBE AUDIO FINGERPRINT</h3>
              <p>In addition to the restrictions detailed above for iTunes and Spotify, content that matches one of the following criteria will not be delivered to YouTube Audio Fingerprint.</p>
              <ul><li>Mash-ups, re-mastered versions and remixes of other works</li>
<li>Compilations (third party licences), "best ofs": these content are only valid when exclusively owned or previously unreleased
    Soundtracks of movies and video games</li>
<li>Music that was licenced, but without exclusivity – Ex: Rap / Hip-hop beats & instrumentals</li>
<li>Non-exclusive content I.e. catalogues in the public domain, recordings of classical and traditional content</li></ul>
              <h3>CONTENT THAT WILL NOT BE DELIVERED TO ANY STORE</h3>
<ul><li>Misleading/deceptive content, including the use of inaccurate unauthorised audio or cover art.</li>
<li>Any content that depict Nazi symbolism</li></ul>
<h3>DELIVERY RULES AND LEAD TIME FOR NEW RELEASES</h3>
<ul><li>All content should be submitted at least two weeks ahead of release date</li>
<li>Any release requiring a special set up will have to be submitted three weeks prior to the release date</li>
<li>SaregamaTone cannot guarantee that out of process setups with the stores, will be properly reflected on releases</li></ul>
<p>Please take into consideration that these guidelines are subject to update. And if you have any questions, please reach out to your local Support team.</p>
<p id="guidelinesLink"><a href="{{asset('public/Sanjivani_digital_content_delivery_guidelines_for_audio_stores.pdf')}}" target="_blank">Content delivery guidelines for audio stores</a></p>
<p><input type="checkbox" name="submit_term_and_condiation"> I understand and I accept content delivery guidelines for audio stores</p>
          </div> 

      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
         <button type="button" class="btn btn-primary" data-dismiss="modal" >Previous</button>
          <button type="button" class="btn btn-primary form_submit"  >Submit my release</button>
      </div>

    </div>
  </div>
</div>  
                        <input type="button" class="btn btn-primary pull-right submit_my_release" value="Submit my release">                    
                    
                    </div>
                    @endif
                    @endif
                    @endif
                    @endif                    
                    @else
                    @if($type != 'edit_assets' && $type !='upload_assets')
                    <input type="button" class="btn btn-primary pull-right form_submit" value="{{ trans('admin_lang.save') }}">
                    @endif
                    @endif
                </div>
            </div>
   </div>
</div>
    <script>
    $(document).on('click','.submit_my_release',function(){
    $('#myModalSubmit').modal('show'); 
        });
    </script>

