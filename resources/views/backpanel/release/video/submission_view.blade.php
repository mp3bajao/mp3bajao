@extends('admin.layouts.layout')
@section('content')
<?php
use App\Models\Track;
?>
<link rel="stylesheet" href="{{ asset('public/admintheme/dist/css/form_custom.css')}}">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
     <section class="content-header1">
         @include('admin.partials.errors')
   </section>
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         {{ trans('admin_lang.'.$langPath) }}
         <small>{{ trans('admin_lang.view').' '.trans('admin_lang.'.$langPath) }}</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="{!! url('admin/dashboard') !!}"><i class="fa fa-dashboard"></i> {{ trans('admin_lang.home') }}</a></li>
         <li class="active"><a href="{!! routeUser($pagePath.'.index') !!}">{{ trans('admin_lang.'.$langPath) }}</a></li>
         <li class="active"> {{ trans('admin_lang.view').' '.trans('admin_lang.'.$langPath) }}</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- Info boxes -->
      <div class="row">
         <div class="col-xs-12">
            <div class="box">
               <div class="box-header">
                   <a class="btn btn-social btn-primary" href="{{routeUser($pagePath.'.index',['type'=>'all'])}}">
                        <i class="fa fa-arrow-circle-o-left"></i> {{ trans('admin_lang.back') }}
                        </a>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="row">
   <div class="col-sm-12">
      <div class="bg box box-default">
                   <div class="row">
    <div class="col-sm-12">
        

        <h1>{{isset($sdata->title)?ucWords($sdata->title):'empty'}}</h1><br/>
        <?php $i=0; ?>
        
          <table class="table table-striped table-bordered" >
                       <tbody>
                           <tr>
                               <td rowspan="8"><img src="{{$sdata->album_profile}}" class="img-responsive img-thumbnail"  width="150px"></td>
                                <td style="width:40%">
                                    <label for="title" class="control-label required">{{ trans('admin_lang.rtitle') }}</label>&nbsp; : &nbsp; {{isset($sdata->title)?ucfirst($sdata->title):'empty'}}</td>
                                <td style="width:40%" rowspan="8" ></td>

                            </tr>
                            
                            <tr><td><label for="primary_artist" class="control-label required">{{ trans('admin_lang.primary') }}</label>&nbsp; : &nbsp; 
                            
                                    {{isset($sdata->primary_artist)?ucwords($sdata->primary_artist):'empty'}}</td></tr>
                            <tr><td><label for="label_id" class="control-label required">{{ trans("admin_lang.label_name") }}</label>&nbsp; : &nbsp; {{!empty($sdata->label_value)?ucwords($sdata->label_value):'empty'}}</td></tr>
                            <tr><td><label for="genre_id" class="control-label required">{{ trans("admin_lang.genre") }}</label>&nbsp; : &nbsp;  {{!empty($sdata->genre_value)?$sdata->genre_value:'empty'}}</td></tr>
                            <tr><td><label for="format" class="control-label required">{{ trans("admin_lang.choose_price_tire") }}</label>&nbsp; : &nbsp; {{!empty($sdata->price_value)?ucfirst($sdata->price_value):'empty'}}</td></tr>
                            <tr><td><label for="format" class="control-label required">{{ trans("admin_lang.release_date") }}</label>&nbsp; : &nbsp; {{isset($sdata->release_date)?date('d M Y h:i:s',strtotime($sdata->release_date)):'empty'}}</td></tr>
                            <tr><td><label for="production_year" class="control-label required">{{ trans('admin_lang.production_year') }}</label>&nbsp; : &nbsp; {{isset($sdata->production_year)?ucwords($sdata->production_year):'empty'}}</td></tr>
                            <tr><td><label for="original_release_date" class="control-label required">#Video</label>&nbsp; : &nbsp; 1</td>                            </tr>
                            
                       </tbody>
          </table>



   </div>
</div>
               </div>
               <!-- /.box-body -->
            </div>
            <!-- /.box -->
         </div>
      </div>
      </div>
      </div>
      </div>
     
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>

@endsection

