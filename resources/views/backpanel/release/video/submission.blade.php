<div class="row">
    <div class="col-sm-12">
        
                  <div class="modal" id="myModal" >
  <div class="modal-dialog" style="width:50%">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Assets Metadata</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
          <div class="modal-body-content colspace_col">
              
          </div>
      </div>
    </div>
  </div>
</div>
      
        <h1><small>Release information @if($e_release>0) <a href="{{routeUser($pagePath.'.edit',['id'=>encrypt($sdata->id),'type'=>'release'])}}"><span class="label label-danger">{{$e_release}} error(s) found</span></a> @else <a href="{{routeUser($pagePath.'.show',['id'=>encrypt($sdata->id)])}}"><span class="label label-success">View details</span></a> @endif</small></h1>
        <?php $i=0; ?>
        
          <table class="table table-striped table-bordered" >
                       <tbody>
                            <tr>
                                <td style="width:40%">
                                    <label for="title" class="control-label required">{{ trans('admin_lang.rtitle') }}</label>&nbsp; : &nbsp; {{isset($sdata->title)?ucfirst($sdata->title):'empty'}}</td>
                                <td style="width:40%">  <label for="genre_id" class="control-label required">{{ trans("admin_lang.genre") }}</label>&nbsp; : &nbsp;  {{!empty($sdata->genre_value)?$sdata->genre_value:'empty'}}</td>
                                <td rowspan="7"><img src="{{$sdata->album_profile}}" class="img-responsive img-thumbnail"  width="60%">
                                <h1><small>Release Album @if($e_album>0) <a href="{{routeUser($pagePath.'.edit',['id'=>encrypt($sdata->id),'type'=>'release'])}}"><span class="label label-danger">{{$e_album}} error(s) found</span></a> @else <a href="{{routeUser($pagePath.'.show',['id'=>encrypt($sdata->id)])}}"><span class="label label-success">View details</span></a> @endif</small></h1>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:40%">
                                     <label for="subtitle" class="control-label">{{ trans('admin_lang.version') }}</label>&nbsp; : &nbsp; {{isset($sdata->subtitle)?ucfirst($sdata->subtitle):'empty'}}</td>
                                <td style="width:40%"> <label for="subgenre_id" class="control-label">{{ trans("admin_lang.subgenre") }}</label>&nbsp; : &nbsp;  {{!empty($sdata->subgenre_value)?$sdata->subgenre_value:'empty'}}</td>
                            
                            </tr>
                            <tr>
                                <td style="width:40%">
                                    
                                      <label for="primary_artist" class="control-label required">{{ trans('admin_lang.primary') }}</label>&nbsp; : &nbsp; @if(count(explode('|',$sdata->primary_artist))<=3) {{isset($sdata->primary_artist)?ucwords(str_replace('|',', ',$sdata->primary_artist)):'empty'}} @else Various Artists @endif</td>
                                <td style="width:40%"> <label for="p_line" class="control-label required">  ℗ {{ trans('admin_lang.line') }}</label>&nbsp; : &nbsp; {{isset($sdata->p_line)?ucwords($sdata->p_line):'empty'}}</td>
                                
                            </tr>
                            
                            <tr>
                                <td style="width:40%"><label for="primary_artist" class="control-label required">{{ trans('admin_lang.director') }}</label>&nbsp; : &nbsp; @if(count(explode('|',$sdata->director))<=3) {{isset($sdata->director)?ucwords(str_replace('|',', ',$sdata->director)):'empty'}} @else Various Director @endif</td>
                                <td style="width:40%"><label for="primary_artist" class="control-label required">{{ trans('admin_lang.film_director') }}</label>&nbsp; : &nbsp; @if(count(explode('|',$sdata->film_director))<=3) {{isset($sdata->film_director)?ucwords(str_replace('|',', ',$sdata->film_director)):'empty'}} @else Various Film Director @endif</td>                                
                            </tr>
                            
                            <tr>
                                <td style="width:40%"><label for="primary_artist" class="control-label required">{{ trans('admin_lang.actor') }}</label>&nbsp; : &nbsp; @if(count(explode('|',$sdata->actor))<=3) {{isset($sdata->actor)?ucwords(str_replace('|',', ',$sdata->actor)):'empty'}} @else Various Actor @endif</td>
                                <td style="width:40%"><label for="primary_artist" class="control-label required">{{ trans('admin_lang.editor') }}</label>&nbsp; : &nbsp; @if(count(explode('|',$sdata->editor))<=3) {{isset($sdata->editor)?ucwords(str_replace('|',', ',$sdata->editor)):'empty'}} @else Various  Editor @endif</td>                                
                            </tr>

                            <tr>
                                <td style="width:40%">
                                       <label for="label_id" class="control-label required">{{ trans("admin_lang.label_name") }}</label>&nbsp; : &nbsp; {{!empty($sdata->label_value)?ucwords($sdata->label_value):'empty'}}</td>
                                <td style="width:40%"><label for="title" class="control-label required">&copy; {{ trans('admin_lang.line') }}</label>&nbsp; : &nbsp; {{isset($sdata->c_line)?ucwords($sdata->c_line):'empty'}}</td>
                                
                            </tr>
                            <tr>
                                <td style="width:40%">
                                        <label for="format" class="control-label required">{{ trans("admin_lang.format") }}</label>&nbsp; : &nbsp; {{!empty($sdata->format)?ucwords($sdata->format):'empty'}}</td>
                                <td style="width:40%"><label for="production_year" class="control-label required">{{ trans('admin_lang.production_year') }}</label>&nbsp; : &nbsp; {{isset($sdata->production_year)?ucwords($sdata->production_year):'empty'}}</td>
                               
                            </tr>
                              <tr>
                                <td style="width:40%">
                                        <label for="original_release_date" class="control-label required">#Video</label>&nbsp; : &nbsp; 
                                @if($total_media_count<1) <a href="{{routeUser($pagePath.'.edit',['id'=>encrypt($sdata->id),'type'=>'upload_assets'])}}"><span class="label label-danger"> error(s) found</span></a> @else <span class="label label-success">View details</span>@endif        
                                        {{!empty($total_media_count)?$total_media_count:'empty'}}
                                
                                </td>
                                <td style="width:40%"></td>
                                
                            </tr>
                       </tbody>
          </table>
        
        <h1><small>Store & Price  @if($e_price>0) <a href="{{routeUser($pagePath.'.edit',['id'=>encrypt($sdata->id),'type'=>'price_tire'])}}"><span class="label label-danger">{{$e_price}} error(s) found</span></a> @else <a href="{{routeUser($pagePath.'.show',['id'=>encrypt($sdata->id)])}}"><span class="label label-success">View details</span></a> @endif </small></h1>
         <table class="table table-striped table-bordered" >
                       <tbody>
                            <tr>
                                <td style="width:100%">
                                    <label for="format" class="control-label required">{{ trans("admin_lang.choose_price_tire") }}</label>&nbsp; : &nbsp; $9.99 {{-- !empty($sdata->price_value)?ucfirst($sdata->price_value):'empty'--}}</td>
                            </tr>
                       </tbody>
         </table>

        <h1><small>Release Date & time   @if($e_release_date>0) <a href="{{routeUser($pagePath.'.edit',['id'=>encrypt($sdata->id),'type'=>'release_date'])}}"><span class="label label-danger">{{$e_release_date}} error(s) found</span></a> @else <a href="{{routeUser($pagePath.'.show',['id'=>encrypt($sdata->id)])}}"><span class="label label-success">View details</span></a> @endif</small></h1>
         <table class="table table-striped table-bordered" >
                       <tbody>
                            <tr>
                                <td style="width:10%">
                                    <label for="format" class="control-label required">{{ trans("admin_lang.choose_price_tire") }}</label>&nbsp; : &nbsp; {{isset($sdata->release_date)?date('d M Y h:i:s',strtotime($sdata->release_date)):'empty'}}</td>
                            </tr>
                       </tbody>
         </table>
        
   </div>
</div>

