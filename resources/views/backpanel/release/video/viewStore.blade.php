<div class="modal-body" style="height: 570px;overflow-y: scroll;">
<?php 
  $boomplay_stores = array();
  ?>
<?php if(Auth::user()->role_id==1){?>
  <div id="exTab2" class=""> 
    <ul class="nav nav-tabs">
      <li class="active">
        <a href="#neumeta" data-toggle="tab" data-target="#neumeta, #neumeta_footer">NeuMeta</a>
      </li>
      <li>
        <a href="#boomplay" data-toggle="tab" data-target="#boomplay, #boomplay_footer">BoomPlay</a>
      </li>
    </ul>
    <div class="tab-content row">
      <div class="tab-pane active" id="neumeta">
        @forelse($data->getStoreRelease as $store)
          @if($store->getStore['receiver']=="NEUMETA")
            <?php $boomplay_stores[] = $store->getStore->id; ?>
            @if($store_type==1)
              @if($store->is_send==1)
                <div class="col-sm-2">
                  <div class="boxsetup" style="border: 1px solid #ccc;margin: 5px;height: 180px;padding: 15px;text-align: center;box-shadow: 0px 0px 0px 5px aliceblue;">
                    <img src="{{asset($store->getStore->image)}}" style="width:100%;"><br/>                
                    <span>{{$store->getStore->title}} @if($data->release_current_status=='NONE' && $store->is_send==0)  <i class="fa fa-cloud-upload" style="color:#555"></i> @else @if($store->is_send==1) <i class="fa fa-check-circle" style="color:green"></i> @else <i class="fa fa-times-circle" style="color:red"></i> @endif @endif</span>
                  </div>
                </div>
              @endif    
            @endif        
            @if($store_type==0)      
              <div class="col-sm-2">
                <div class="boxsetup" style="    border: 1px solid #ccc;    margin: 5px;height: 180px;padding: 15px;text-align: center;box-shadow: 0px 0px 0px 5px aliceblue;">
                  <img src="{{asset($store->getStore->image)}}" style="width:100%;"><br/>                
                  <span>{{$store->getStore->title}} @if($data->release_current_status=='NONE' &&  $store->is_send==0)  <i class="fa fa-cloud-upload" style="color:#555"></i> @else @if($store->is_send==1) <i class="fa fa-check-circle" style="color:green"></i> @else <i class="fa fa-times-circle" style="color:red"></i> @endif @endif</span>
                </div>
              </div>
            @endif
          @endif
        @empty
        @endforelse
      </div>
      <div class="tab-pane" id="boomplay">
        @forelse($data->getStoreRelease as $store)
          @if($store->getStore['receiver']=="BOOMPLAY")
            <?php $boomplay_stores[] = $store->getStore->id; ?>
            @if($store_type==1)
              @if($store->is_send==1)
                <div class="col-sm-2">
                  <div class="boxsetup" style="border: 1px solid #ccc;margin: 5px;height: 180px;padding: 15px;text-align: center;box-shadow: 0px 0px 0px 5px aliceblue;">
                    <img src="{{asset($store->getStore->image)}}" style="width:100%;"><br/>                
                    <span>{{$store->getStore->title}} @if($data->release_current_status=='NONE' && $store->is_send==0)  <i class="fa fa-cloud-upload" style="color:#555"></i> @else @if($store->is_send==1) <i class="fa fa-check-circle" style="color:green"></i> @else <i class="fa fa-times-circle" style="color:red"></i> @endif @endif</span>
                  </div>
                </div>
              @endif    
            @endif        
            @if($store_type==0)      
              <div class="col-sm-2">
                <div class="boxsetup" style="    border: 1px solid #ccc;    margin: 5px;height: 180px;padding: 15px;text-align: center;box-shadow: 0px 0px 0px 5px aliceblue;">
                  <img src="{{asset($store->getStore->image)}}" style="width:100%;"><br/>                
                  <span>{{$store->getStore->title}} @if($data->release_current_status=='NONE' &&  $store->is_send==0)  <i class="fa fa-cloud-upload" style="color:#555"></i> @else @if($store->is_send==1) <i class="fa fa-check-circle" style="color:green"></i> @else <i class="fa fa-times-circle" style="color:red"></i> @endif @endif</span>
                </div>
              </div>
            @endif
          @endif
        @empty
        @endforelse
      </div>
    </div>
  </div>
<?php }else{?>

<div class="row">
<?php $store_id= array(); ?>
        @forelse($data->getStoreRelease as $store)
        <?php $store_id[] = $store->getStore->id; ?>
        @if($store_type==1)
        @if($store->is_send==1)
            <div class="col-sm-2">
                <div class="boxsetup" style="    border: 1px solid #ccc;    margin: 5px;height: 180px;padding: 15px;text-align: center;box-shadow: 0px 0px 0px 5px aliceblue;">
                    <img src="{{asset($store->getStore->image)}}" style="width:100%;"><br/>                
                <span>{{$store->getStore->title}} @if($data->release_current_status=='NONE' && $store->is_send==0)  <i class="fa fa-cloud-upload" style="color:#555"></i> @else @if($store->is_send==1) <i class="fa fa-check-circle" style="color:green"></i> @else <i class="fa fa-times-circle" style="color:red"></i> @endif @endif</span>
            </div>
            </div>
         @endif    
        @endif        
         @if($store_type==0)      
            <div class="col-sm-2">
                <div class="boxsetup" style="    border: 1px solid #ccc;    margin: 5px;height: 180px;padding: 15px;text-align: center;box-shadow: 0px 0px 0px 5px aliceblue;">
                    <img src="{{asset($store->getStore->image)}}" style="width:100%;"><br/>                
                <span>{{$store->getStore->title}} @if($data->release_current_status=='NONE' &&  $store->is_send==0)  <i class="fa fa-cloud-upload" style="color:#555"></i> @else @if($store->is_send==1) <i class="fa fa-check-circle" style="color:green"></i> @else <i class="fa fa-times-circle" style="color:red"></i> @endif @endif</span>
            </div>
            </div>
        @endif
        @empty
        
        @endforelse

</div>
<?php }?>
</div>

<div class="modal-footer">
  @if(Auth::user()->role_id==1 && $store_type!=1)
  <div class="tab-content">
    <div class="tab-pane active" id="neumeta">
      <!-- @if($data->release_current_status=='NONE' && @$store->is_send==0 ) 
        <a href="{{routeUser('release.video.final',['action'=>'InitialDelivery','release_id'=>$data->id])}}" class="btn btn-success upload_save">InitialDelivery @if($data->release_current_status=='InitialDelivery') <i class="fa fa-check-circle"></i> @endif</a>
      @else
        <a href="javascript:;" data-url_href="{{routeUser('release.video.final',['action'=>'InitialDelivery','release_id'=>$data->id])}}" class="btn btn-success upload_save">InitialDelivery <i class="fa fa-check-circle"></i></a>
        <a href="javascript:;" data-url_href="{{routeUser('release.video.final',['action'=>'FullUpdate','release_id'=>$data->id])}}" class="btn btn-warning upload_save">FullUpdate @if($data->release_current_status=='FullUpdate') <i class="fa fa-check-circle"></i> @endif</a>
        <a href="javascript:;" data-url_href="{{routeUser('release.video.final',['action'=>'MetadataOnlyUpdate','release_id'=>$data->id])}}" class="btn btn-default upload_save">MetadataOnlyUpdate @if($data->release_current_status=='MetadataOnlyUpdate') <i class="fa fa-check-circle"></i> @endif</a>
        <a href="javascript:;" data-url_href="{{routeUser('release.video.final',['action'=>'TakeDown','release_id'=>$data->id])}}" class="btn btn-primary upload_save">TakeDown @if($data->release_current_status=='TakeDown') <i class="fa fa-check-circle"></i> @endif</a>        
      @endif  -->
    </div>
    <div class="tab-pane " id="boomplay_footer">
      @if(!empty($boomplay_stores))
        <a href="javascript:;" data-url_href="{{routeUser('release.video.final_sftp',['action'=>'InitialDelivery', 'receiver'=>'BOOMPLAY', 'release_id'=>$data->id])}}" class="btn btn-success upload_save">InitialDelivery</a>
        <a href="javascript:;" data-url_href="{{routeUser('release.video.final_sftp',['action'=>'MetadataOnlyUpdate', 'receiver'=>'BOOMPLAY', 'release_id'=>$data->id])}}" class="btn btn-default upload_save">MetadataOnlyUpdate</a>
        <a href="javascript:;" data-url_href="{{routeUser('release.video.final_sftp',['action'=>'TakeDown', 'receiver'=>'BOOMPLAY', 'release_id'=>$data->id])}}" class="btn btn-primary upload_save">TakeDown</a>
      @endif
    </div>
  </div>
@endif
</div>
    

     

<script>
$(document).on('click','.setstore',function(){
   var status = 0; 
   if($(this).is(':checked'))
   {
     status = 1;  
   }
   else
   {
     status = 0;       
   }
   var id =  $(this).val();
   var release_id = $(this).data('release_id');   
   $(".loding_img").show();
        $.ajax({
            url: "{{ routeUser('ajax.store.set') }}",
            type: 'get',
            data:{status:status,id:id,release_id:release_id},
            success: function (data) 
            {
                  $(".loding_img").hide();
            },               
        });   
});

$(document).ready(function(){
   $(document).on('click','.upload_save',function(){
        $(".loding_img").show();
        var url = $(this).data('url_href');
        location.replace(url);
   }); 
});
</script>
