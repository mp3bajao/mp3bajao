<?xml version="1.0" encoding="UTF-8"?>
<ernm:NewReleaseMessage xmlns:ernm="http://ddex.net/xml/ern/382" xmlns:xs="http://www.w3.org/2001/XMLSchema-instance" LanguageAndScriptCode="en" MessageSchemaVersionId="ern/382" xs:schemaLocation="http://ddex.net/xml/ern/382 http://ddex.net/xml/ern/382/release-notification.xsd">
  <MessageHeader>
    <MessageThreadId>{!! $release->id !!}</MessageThreadId>
    <MessageId>{!! md5($release->id) !!}</MessageId>
    <MessageSender>
        <PartyId>PA-DPIDA-2019041703-R</PartyId>
        <PartyName>
            <FullName>Sanjivani Digital Entertainment Pvt Ltd</FullName>
        </PartyName>
    </MessageSender>
    <MessageRecipient>
      <PartyId></PartyId>
      <PartyName>
        <FullName></FullName>
      </PartyName>
    </MessageRecipient>
      <?php $dt = dateFormate(date('Y-m-dTH:i:s'));?>
      <MessageCreatedDateTime>{{$dt->format('Y-m-d').'T'.$dt->format('H:i:s').'Z'}}</MessageCreatedDateTime>
  </MessageHeader>
  <UpdateIndicator>OriginalMessage</UpdateIndicator>
  
  
  <ResourceList>      
    @forelse($release->getReleaseTrack as $track)
    <?php $media = $track->getTrackMedia; ?>
    @if(isset($media))
    <SoundRecording>
      <SoundRecordingType>MusicalWorkSoundRecording</SoundRecordingType>
      <SoundRecordingId>
        <ISRC>{{ $track->ISRC }}</ISRC>
      </SoundRecordingId>
      <ResourceReference>A{{ $track->track_number }}</ResourceReference>
      <ReferenceTitle>
        <TitleText>{!!$track->title!!}</TitleText>
        <SubTitle>{!!$track->subtitle!!}</SubTitle>
      </ReferenceTitle>
      <?php $duration =  explode(':',$media->file_duration);  ?>
      <Duration>{{ 'PT'.$duration[0].'H'.$duration[1].'M'.$duration[2].'S' }}</Duration>
      <SoundRecordingDetailsByTerritory>
        <TerritoryCode>Worldwide</TerritoryCode>
        <Title LanguageAndScriptCode="en" TitleType="FormalTitle">
          <TitleText>{!!$track->title!!}</TitleText>
          <SubTitle>{!!$track->subtitle!!}</SubTitle>
        </Title>
        <Title LanguageAndScriptCode="en" TitleType="DisplayTitle">
          <TitleText>{!!$track->title!!}</TitleText>
        </Title>
        <Title LanguageAndScriptCode="en" TitleType="AbbreviatedDisplayTitle">
          <TitleText>{!!$track->title!!}</TitleText>
        </Title>
        <?php $main_artist = explode('|',$track->primary_artist); ?>
        @forelse($main_artist as $k => $main_artist)
        <DisplayArtist SequenceNumber="{{$k+1}}">
          <PartyName LanguageAndScriptCode="en">
            <FullName>{!!$main_artist!!}</FullName>
          </PartyName>
          <ArtistRole>MainArtist</ArtistRole>
        </DisplayArtist>
        @empty
        @endforelse
        
        
        <?php $author = explode('|',$track->author); ?>
        @forelse($author as $k => $author)
        <IndirectResourceContributor>
          <PartyName LanguageAndScriptCode="en">
            <FullName>{!! $author !!}</FullName>
          </PartyName>
          <IndirectResourceContributorRole>Author</IndirectResourceContributorRole>
        </IndirectResourceContributor>
        @empty
        @endforelse
        
        <?php $composer = explode('|',$track->composer); ?>
        @forelse($composer as $k => $composer)
        <IndirectResourceContributor>
          <PartyName LanguageAndScriptCode="en">
            <FullName>{!! $composer !!}</FullName>
          </PartyName>
          <IndirectResourceContributorRole>Composer</IndirectResourceContributorRole>
        </IndirectResourceContributor>
        @empty
        @endforelse
        
        <?php $publisher= explode('|',$track->publisher); ?>
        @forelse($publisher as $k => $publisher)
        <IndirectResourceContributor SequenceNumber="{{$k+1}}">
          <PartyName LanguageAndScriptCode="en">
            <FullName>{!! $publisher !!}</FullName>
          </PartyName>
          <IndirectResourceContributorRole>MusicPublisher</IndirectResourceContributorRole>
        </IndirectResourceContributor>
        @empty
        @endforelse
        
        
        <?php /* $producer= explode('|',$track->producer); ?>
        @forelse($producer as $k => $producer)
        <IndirectResourceContributor>
          <PartyName LanguageAndScriptCode="en">
            <FullName>{!! $producer !!}</FullName>
          </PartyName>
          <IndirectResourceContributorRole>MusicProducer</IndirectResourceContributorRole>
        </IndirectResourceContributor>
        @empty
        @endforelse
        */?>
        
        
        
        
        <LabelName>{!! $release->label_value !!}</LabelName>
        <PLine>
          <Year>{!! $track->production_year !!}</Year>
          <PLineText>{!! $track->p_line !!}</PLineText>
        </PLine>
        <Genre>
          <GenreText>{!! $release->genre_value !!}</GenreText>
        </Genre>                
        <ParentalWarningType>Unknown</ParentalWarningType>
        
        
        <?php 
        if(isset($media))
        {
            $fiename = explode('.',$media->folder_path);
            $track_array = explode('_',$fiename[0]);
            $data['track-number']=(int)end($track_array);
        }
        else
        {
            $data['track-number']=0;
        }
        ?>
        <TechnicalSoundRecordingDetails>
          <TechnicalResourceDetailsReference>T{{ $data['track-number'] }}</TechnicalResourceDetailsReference>
          <AudioCodecType>PCM</AudioCodecType>
          <BitRate>320</BitRate>
          <NumberOfChannels>2</NumberOfChannels>
          <SamplingRate>44.1</SamplingRate>
          <IsPreview>false</IsPreview>
          <File>
            <FileName>{!! $media->folder_path!!}</FileName>
            <FilePath>{!! $media->base_path!!}</FilePath>
            <HashSum>
              <HashSum>{!! md5($data['track-number']) !!}</HashSum>
              <HashSumAlgorithmType>MD5</HashSumAlgorithmType>
            </HashSum>
          </File>
        </TechnicalSoundRecordingDetails>
        
      </SoundRecordingDetailsByTerritory>
    </SoundRecording>
    
   
    
  
    
    @endif
    @empty
    @endforelse
       
     
     <Image>
      <ImageType>FrontCoverImage</ImageType>
      <ImageId>
        <ProprietaryId Namespace="DPID:">GGL_UIM_ID:{{ $release->id }}</ProprietaryId>
      </ImageId>
      <ResourceReference>AI{{ $track->track_number }}</ResourceReference>
      <ImageDetailsByTerritory>
        <TerritoryCode>Worldwide</TerritoryCode>
        <TechnicalImageDetails>
          <TechnicalResourceDetailsReference>TI{{ $data['track-number'] }}</TechnicalResourceDetailsReference>
          <ImageCodecType>JPEG</ImageCodecType>
          <ImageHeight>3000</ImageHeight>
          <ImageWidth>3000</ImageWidth>
          <File>
            <FileName>{!! $release->folder_path !!}</FileName>
            <FilePath>{!! $release->base_path !!}</FilePath>
            <HashSum>
              <HashSum>{!! md5($data['track-number']) !!}</HashSum>
              <HashSumAlgorithmType>MD5</HashSumAlgorithmType>
            </HashSum>
          </File>
        </TechnicalImageDetails>
      </ImageDetailsByTerritory>
    </Image>
    
  </ResourceList>
  
  
  
  
  
  
  <ReleaseList>
    @forelse($release->getReleaseTrack  as $key => $track)
    <?php $media = $track->getTrackMedia; ?>
    @if(isset($media))
    <Release>
      <ReleaseId>
        <ISRC>{!! $track->ISRC !!}</ISRC>
        <ProprietaryId Namespace="DPID:">{!! $track->id !!}</ProprietaryId>
      </ReleaseId>
      <ReleaseReference>R{!! $key !!}</ReleaseReference>
      <ReferenceTitle>
        <TitleText>{!! $track->title !!}</TitleText>
      </ReferenceTitle>
      <ReleaseResourceReferenceList>
        <ReleaseResourceReference ReleaseResourceType="PrimaryResource">A{!! $track->track_number !!}</ReleaseResourceReference>
      </ReleaseResourceReferenceList>
      <ReleaseType>TrackRelease</ReleaseType>
      <ReleaseDetailsByTerritory>
        <TerritoryCode>Worldwide</TerritoryCode>        
        <DisplayArtistName>{!! $release->title !!}</DisplayArtistName>
        <LabelName>{!! $release->label_value!!}</LabelName>
        <Title LanguageAndScriptCode="en" TitleType="FormalTitle">
          <TitleText>{!! $release->title !!}</TitleText>
          <SubTitle>{!! $release->subtitle !!}</SubTitle>
        </Title>
        <Title TitleType="DisplayTitle">
          <TitleText>{!! $release->title !!}</TitleText>
        </Title>
        <Title TitleType="GroupingTitle">
          <TitleText>{!! $release->title !!}</TitleText>
          <SubTitle>{!! $release->subtitle !!}</SubTitle>
        </Title>
        
        <?php $main_artist = explode('|',$track->primary_artist); ?>
        @forelse($main_artist as $k => $main_artist)
        <DisplayArtist SequenceNumber="{{$k+1}}">
          <PartyName LanguageAndScriptCode="en">
            <FullName>{!!$main_artist!!}</FullName>
          </PartyName>
          <ArtistRole>MainArtist</ArtistRole>
        </DisplayArtist>
        @empty
        @endforelse
        
        <ParentalWarningType>NotExplicit</ParentalWarningType>
         <Genre>
          <GenreText>{!! $release->genre_value !!}</GenreText>
        </Genre>
        <OriginalReleaseDate>{!! $release->original_release_date !!}</OriginalReleaseDate>
        <Keywords></Keywords>
      </ReleaseDetailsByTerritory>      
      <PLine>
        <Year>{!! $release->production_year !!}</Year>
        <PLineText>{!! $release->p_line !!}</PLineText>
      </PLine>      
      <CLine>
        <Year>{!! $release->production_year !!}</Year>
        <CLineText>{!! $release->c_line !!}</CLineText>
      </CLine>
    </Release>
    @endif
    @empty
    @endforelse

      
      
  </ReleaseList>
  
  
  
  <DealList>
       @forelse($release->getReleaseTrack as $track)
    <?php $media = $track->getTrackMedia; ?>
    @if(isset($media))
    <ReleaseDeal>
      <DealReleaseReference>R{!! $track->track_number !!}</DealReleaseReference>
      <Deal>
        <DealTerms>
          <CommercialModelType>PayAsYouGoModel</CommercialModelType>
          <CommercialModelType>SubscriptionModel</CommercialModelType>
          <CommercialModelType>AdvertisementSupportedModel</CommercialModelType>
          <Usage>
            <UseType>PermanentDownload</UseType>
            <UseType>OnDemandStream</UseType>
            <UseType>NonInteractiveStream</UseType>
            <DistributionChannelType>AsPerContract</DistributionChannelType>
          </Usage>
          <TerritoryCode>Worldwide</TerritoryCode>
          <PriceInformation>
            <PriceType Namespace="DPID:"></PriceType>
          </PriceInformation>
          <ValidityPeriod>
            <StartDate>{{date('Y-m-d')}}</StartDate>
          </ValidityPeriod>
          <PreOrderPreviewDate>{{date('Y-m-d')}}</PreOrderPreviewDate>
        </DealTerms>
      </Deal>     
      <EffectiveDate>{{date('Y-m-d')}}</EffectiveDate>
    </ReleaseDeal>
     @endif
    @empty
    @endforelse
  </DealList>
</ernm:NewReleaseMessage>
