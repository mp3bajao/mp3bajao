<div class="row ">
    <div class="col-sm-6">
        <div class="form-group @if ($errors->has('release_date')) has-error @endif">
                    <label for="release date" class="control-label required ">{{ trans("admin_lang.main_choose_price_tire") }}</label>
                    {{ Form::text('release_date',null,['class'=>'form-control release_date changeDate','id'=>'datetimepicker','autocomplete'=>'off','placeholder'=>trans("admin_lang.main_choose_price_tire"),'readonly'=>'readonly'])}}
                @if ($errors->has('release_date'))<span class="error">{{ $errors->first('release_date') }}</span>@endif
                </div>
   </div>
</div>

<script>
    $(document).ready(function(){         
       $('#datetimepicker').datetimepicker({
            //datepicker:true,
            format:'Y-m-d H:i:s',
            //step:5,
            inline:false,
            maxDate:new Date(),
            onChangeDateTime:exampleFunction
        });
        function exampleFunction(){
  $('.upload_submit').submit();
}
   });
   
</script>
