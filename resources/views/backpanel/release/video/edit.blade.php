@extends('admin.layouts.layout')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header1">
         @include('admin.partials.errors')
   </section>
   <!-- Content Header (Page header) -->
   <section class="content-header">
       
      <h1>
         {{ trans('admin_lang.'.$langPath)  }}
         <small>{{ trans("admin_lang.edit").' '.trans('admin_lang.'.$langPath)  }}</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="{!! userUrl('dashboard') !!}"><i class="fa fa-dashboard"></i> {{ trans("admin_lang.home") }}</a></li>
         <li class="active"><a href="{!! routeUser($pagePath.'.index') !!}">{{ trans("admin_lang.".$langPath)  }}</a></li>
         <li class="active"> {{ trans("admin_lang.edit").' '.trans('admin_lang.'.$langPath) }}</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- Info boxes -->
      <div class="row">
         <div class="col-xs-12">
            <div class="box">
               <div class="box-header">
                  <!-- <h3 class="box-title">Hover Data Table</h3> -->
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                  {!! Form::model($sdata,  ['route'=>[routeFormUser($pagePath.'.update'),encrypt($sdata->id),$type], 'method'=>'post','files'=>true,'class'=>'upload_submit']) !!}
                  @include('admin.'.$pagePath.'.form')
                  {{ Form::close() }}
               </div>
               <!-- /.box-body -->
            </div>
            <!-- /.box -->
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

