@extends('admin.layouts.layout')
@section('content')
<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">
    <section class="content-header1">
         @include('admin.partials.errors')
   </section>
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         {{ trans('admin_lang.'.$langPath) }}
         <small>{{ trans('admin_lang.add').' '.trans('admin_lang.'.$langPath) }}</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="{!! url('admin/dashboard') !!}"><i class="fa fa-dashboard"></i> {{ trans('admin_lang.home') }}</a></li>
         <li class="active"><a href="{!! routeUser($pagePath.'.index') !!}">{{ trans('admin_lang.'.$langPath) }}</a></li>
         <li class="active"> {{ trans('admin_lang.add').' '.trans('admin_lang.'.$langPath) }}</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- Info boxes -->
      <div class="row">
         <div class="col-xs-6">
            <div class="box">
               <div class="box-header">
                  <!-- <h3 class="box-title">Hover Data Table</h3> -->
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                  {{Form::open(array('route' => [routeFormUser($pagePath.'.store')],'files'=>true,'class'=>'upload_submit'))}}
                  
                  <div class="col-sm-12">
                      <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="product-head-album">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#product-album" aria-expanded="true" aria-controls="product-album">
                            <div class="">
                                Create a release                            </div>
                        </a>
                    </h4>
                </div>
                <div id="product-album" class="panel-collapse collapse" role="tabpanel" aria-labelledby="product-head-album" rel="album">
                    <div class="panel-body">
                       <div class="form-group @if ($errors->has('title')) has-error @endif">
                           {{ Form::text('title',null,['class'=>'form-control','id'=>'title','placeholder'=>'Title for this release'])}}
                           @if ($errors->has('title'))<span class="error">{{ $errors->first('title') }}</span>@endif
                        </div>
                        
                        {{Form::hidden('track_id',null,['class'=>'track_id'])}}
                        <div class="row">           
                            <div class="col-md-3">
                                {{Form::radio('globel','MUSIC',true,['class'=>'access_option'])}}<label for="globel" class="control-label">{{ trans("admin_lang.music") }}</label>
                            </div>
                            <div class="col-md-5">
                                {{Form::radio('globel','ENTERTAINMENT VIDEO',false,['class'=>'access_option'])}}<label for="globel" class="control-label">Entertainment Video</label>
                            </div>
                            <div class="col-md-4">
                                {{Form::radio('globel','LYRIC VIDEO',false,['class'=>'access_option'])}}<label for="globel" class="control-label">Lyric Video</label>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                           <div class="col-sm-12">
                              <!-- <a href="{{ routeUser($pagePath.'.index') }}" class="btn btn-danger cancel_cls pull-left">{{ trans("admin_lang.cancel") }}</a> -->
                              <button type="button" class="btn btn-primary pull-right form_submit" value="{{ trans('admin_lang.create_release') }}">Create a release</button>
                           </div>
                        </div>
                    </div>
                </div>
                            </div>
                   
                    
                       
                    </div>

                  </div>

                  {{ Form::close() }}
               </div>
               <!-- /.box-body -->
            </div>
            <!-- /.box -->
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<script>
$(document).ready(function(){
   $('.title_search').keyup(function(){
       var search = $(this).val().length;
        if(search>2 )
        {
        $.ajax({
            url:"{{ routeUser('release.video.search') }}",
            type: 'get',
            data: {'search':$(this).val()},
            dataType: 'html',
            success: function(res) {
                $('.track_list').html(res);
                }
            });
        }
        else{
            $('.track_list').html('');
        }
   });
   
   $(document).on('click','.track_details',function(){
     var track_title =  $(this).data('track_title'); 
     var isrc =  $(this).data('track_isrc'); 
     var track_id =  $(this).data('track_id'); 
     $('.title_search').val(track_title);
     $('.audio_isrc').val(isrc);
     $('.track_id').val(track_id);
     $('.track_list').html('');
     
   });
});
</script>
<style>
    .track_list{
        max-height: 200px;
    overflow-y: scroll;
    border: 1px solid #ccc;
    border-top: 0px;        
    }
    .track_list table {
       width: 100%;
    }
    .track_list table td{
        border-bottom: 1px solid #ccc;
        width: 100%;
        padding: 5px;
    }

    
</style>

<!-- /.content-wrapper -->
@endsection

