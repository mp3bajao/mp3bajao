@extends('admin.layouts.layout')
@section('content')
<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">
    <section class="content-header1">
          @include('admin.partials.errors')
   </section>
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         {{ trans('admin_lang.'.$langPath) }}
         <small>{{ trans('admin_lang.add').' '.trans('admin_lang.'.$langPath) }}</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="{!! url('admin/dashboard') !!}"><i class="fa fa-dashboard"></i> {{ trans('admin_lang.home') }}</a></li>
         <li class="active"><a href="{!! routeUser($pagePath.'.index') !!}">{{ trans('admin_lang.'.$langPath) }}</a></li>
         <li class="active"> {{ trans('admin_lang.add').' '.trans('admin_lang.'.$langPath) }}</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- Info boxes -->
      <div class="row">
         <div class="col-xs-6">
            <div class="box">
               <div class="box-header">
                  <!-- <h3 class="box-title">Hover Data Table</h3> -->
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                  {{Form::open(array('route' => [routeFormUser($pagePath.'.store')],'files'=>true,'class'=>'upload_submit'))}}
                  
                  <div class="col-sm-12">
                      <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="product-head-album">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#product-album" aria-expanded="true" aria-controls="product-album">
                            <div class="">
                                Create a release                            </div>
                        </a>
                    </h4>
                </div>
                                <div id="product-album" class="panel-collapse collapse" role="tabpanel" aria-labelledby="product-head-album" rel="album">
                    <div class="panel-body">
                       <div class="form-group @if ($errors->has('title')) has-error @endif">
                           {{ Form::text('title',null,['class'=>'form-control','id'=>'title','placeholder'=>'Title for this release'])}}
                         @if ($errors->has('title'))<span class="error">{{ $errors->first('title') }}</span>@endif
                        </div>

                        
                        <div class="row">           
                            <div class="col-md-4">
                                {{Form::radio('globel','MUSIC',true,['class'=>'access_option'])}}<label for="globel" class="control-label">{{ trans("admin_lang.music") }}</label>
                            </div>
                            <div class="col-md-4">
                                {{Form::radio('globel','CLASSIC MUSIC',false,['class'=>'access_option'])}}<label for="globel" class="control-label">{{ trans("admin_lang.classic_music") }}</label>
                            </div>
                            <div class="col-md-4">
                                {{Form::radio('globel','JAZZ MUSIC',false,['class'=>'access_option'])}}<label for="globel" class="control-label">{{ trans("admin_lang.jazz_music") }}</label>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                           <div class="col-sm-12">
                              <!-- <a href="{{ routeUser($pagePath.'.index') }}" class="btn btn-danger cancel_cls pull-left">{{ trans("admin_lang.cancel") }}</a> -->
                              <button type="button" class="btn btn-primary pull-right form_submit" value="{{ trans('admin_lang.create_release') }}">Create a release</button>
                           </div>
                        </div>
                    </div>
                </div>
                            </div>
                   
                    
                       
                    </div>

                  </div>

                  {{ Form::close() }}
               </div>
               <!-- /.box-body -->
            </div>
            <!-- /.box -->
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

