<div class="row">
  <div class="col-sm-12 " style="font-size: 20px; text-align: right;">
    <span><input class="selectallstore" name="selectallstore" type="checkbox"></span>
    <span>Select All</span>
  </div>
        @forelse($store as $store)
            <div class="col-sm-1" style="padding:0;">
                <div class="boxsetup storeBoxes" >
                <img src="{{asset($store->getStore->image)}}" title="{{$store->getStore->title}}">
                @if(Auth::user()->role_id==1)
                {{ Form::text('store_url[]',$store->store_url,['class'=>'store_url form-control','placeholder'=>'Enter store URL'])}}
                {{ Form::hidden('store_id[]',$store->id,null,['class'=>'setstore1'])}}
                @endif
                <div class="ChckBx">
                <label class="container ">
                {{ Form::checkbox('store',$store->id,($store->status==0)?false:true,['class'=>'setstore','data-release_id'=>$store->release_id])}}
                <span class="checkmark"></span>
                <span class="texts">{{$store->getStore->title}}</span>
                </label>
                </div>
                </div>
            </div>
        @empty
        @endforelse
    
</div>
<style type="text/css">
  .store_url{ width: 100%; margin-top: 3px;}
</style>

<script>
$(document).on('click','.setstore',function(){
   var status = 0; 
   if($(this).is(':checked'))
   {
     status = 1;  
   }
   else
   {
     status = 0;       
   }
   var id =  $(this).val();
   var release_id = $(this).data('release_id');   
   $(".loding_img").show();
        $.ajax({
            url: "{{ routeUser('ajax.store.set') }}",
            type: 'get',
            data:{status:status,id:id,release_id:release_id},
            success: function (data) 
            {
                  $(".loding_img").hide();
                  validateSelectAll();
            },               
        });   
});

$(document).on('click','.selectallstore',function(){
   var status = 0;
   if($(this).is(':checked')){
     status = 1;
     $(".setstore").prop('checked', true);
   }else{
     status = 0;
     $(".setstore").prop('checked', false);
   }
   var release_id = '{{ app('request')->input('id') }}';
   $(".loding_img").show();
      $.ajax({
          url: "{{ routeUser('ajax.store.selectall') }}",
          type: 'get',
          data:{status:status,release_id:release_id},
          success: function (data)
          {
            $(".loding_img").hide();
          }
      });
});

$(document).ready(function(){
  validateSelectAll();
});

function validateSelectAll(){
  var totcheckbox = $('.setstore').length;
  var selectcheckbox = $('.setstore:checked').length;
  if(totcheckbox==selectcheckbox){
    $(".selectallstore").prop('checked', true);
  }
  else{
    $(".selectallstore").prop('checked', false);
  }
}

</script>
