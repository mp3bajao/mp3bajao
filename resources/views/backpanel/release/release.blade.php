<?php
use App\Models\Track;
?>
<div class="row colspace_col">
    <div class="col-sm-12">
        <div class="row">
            <div class="vimg1" style="overflow: scroll;width: 1px;height: 1px;"></div>
            <div class="col-md-2">
                <div id="release-col-cover" class="">
                    <div class="col-sm-9">
                        <div id="releaseCover" class="">
                          <div class="vimg">
                            <a style="display: block;" href="#"  data-toggle="modal" data-target="#myModal">
                                <img id="v_icon_img" src="{!! $sdata->album_profile !!}" class="img-responsive img-thumbnail" id="coverOfThisAlbum">
                            <br></a>
                        </div>
                            <a style="display: block;" href="#"  data-toggle="modal" data-target="#myModal">
                                <center>Upload Album Artwork</center>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
          <div class="row">
              <div class="col-md-8"> <h4 class="modal-title">Update the cover</h4></div>
              <div class="col-md-4"> <button type="button" class="close pull-right" data-dismiss="modal">&times;</button></div>
          </div>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        
              <h5>  Your cover must be:
                  <small>
    Size: 3000*3000 pixels,
- Format:jpg</small></b></h5>
             
                <div class="form-group imgs_video text-center">
                    <div class="vimgupload">
                        <span class="btn btn-primary btn-file" >
                            <i class="fa fa-cloud-upload"></i> 
                            <div>{!! trans("admin_lang.upload_file") !!}
                            </div>
                                <input type="file" name="album_profile" id="album_profile"  class="file_upload" />
                                <span id="btn-file-error"></span>
                        </span>
                    </div>
                </div>          
     
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
          <button type="button" class="btn btn-primary upload_image">Upload Image</button>
      </div>

    </div>
  </div>
</div>
            </div>
        	<div class="col-md-5">
        		<div class="form-group @if ($errors->has('title')) has-error @endif">
                   <label for="title" class="control-label required">{{ trans('admin_lang.rtitle') }}</label>
                   {{ Form::text('title',null,['class'=>'form-control title','data-type'=>'title','id'=>'title','placeholder'=>trans("admin_lang.rtitle")])}}
                 @if ($errors->has('title'))<span class="error">{{ $errors->first('title') }}</span>@endif
                </div>

                <div class="form-group @if ($errors->has('subtitle')) has-error @endif">
                   <label for="subtitle" class="control-label">{{ trans('admin_lang.version') }}</label>
                    {{ Form::text('subtitle',null,['class'=>'form-control subtitle','data-type'=>'subtitle','placeholder'=>trans("admin_lang.version")])}}
                 @if ($errors->has('subtitle'))<span class="error">{{ $errors->first('subtitle') }}</span>@endif
                </div>
                    <?php     
                $primary_artist1='';
                $primary_artist2 =  isset($sdata->primary_artist)?explode('|',$sdata->primary_artist):array();
                $primary_artist3 = Track::where('release_id',$sdata->id)->pluck('primary_artist')->toArray();
                if(!empty($primary_artist3))
                {
                    $primary_artist3 =array_unique(explode('|',str_replace(',','|',implode('|',$primary_artist))));//ask
                    if($primary_artist3>$primary_artist2)
                    {
                     $primary_artist =  $primary_artist3;
                    }
                    else{
                       $primary_artist =   $primary_artist2;
                    }
                }
                ?>
                    
                     
                    
                    
                    
                    
                <div class="form-group @if ($errors->has('primary_artist.0')) has-error @endif">
                   <label for="primary_artist " class="control-label required">{{ trans('admin_lang.primary') }}</label>
                   <div class="row">
                       <div class="col-md-10">{{ Form::text('primary_artist[]',isset($primary_artist[0])?$primary_artist[0]:null,['class'=>'form-control primary_artist primary_artist_0','data-type'=>'primary_artist','placeholder'=>trans("admin_lang.primary")])}}</div>
                       <div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-success add_primary_artist"><i class="fa fa-plus-circle"></i></a></div>
                   </div>
                 @if ($errors->has('primary_artist.0'))<span class="error">{{ $errors->first('primary_artist.0') }}</span>@endif
                 <span class="error primary_artist_error" ></span>
                </div>
                    
                    @if(count($primary_artist)>1)
                   @for($i=1;$i<count($primary_artist);$i++)
                       <div class="form-group">
                           <label for="primary_artist " class="control-label "></label>
                       <div class="row">
                       <div class="col-md-10">
                       {{ Form::text('primary_artist[]',isset($primary_artist[$i])?$primary_artist[$i]:null,['class'=>'form-control primary_artist primary_artist_$i','data-type'=>'primary_artist','placeholder'=>trans("admin_lang.primary")])}}
                       <br/>
                     </div>
                     <div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-danger remove_primary_artist"><i class="fa fa-minus-circle"></i></a></div>
                     </div>
                     </div>
                       
                   @endfor
                   @endif
                    <div class="primary_artist_append"></div>
                  <?php $featuring_artist =  isset($sdata->featuring_artist)?explode('|',$sdata->featuring_artist):array();?>
                <div class="form-group @if ($errors->has('featuring_artist')) has-error @endif">
                   <label for="featuring" class="control-label ">{{ trans('admin_lang.featuring') }}</label>
                   <div class="row">
                       <div class="col-md-10">{{ Form::text('featuring_artist[]',isset($featuring_artist[0])?$featuring_artist[0]:null,['class'=>'form-control featuring_artist featuring_artist_0','data-type'=>'featuring_artist','placeholder'=>trans("admin_lang.featuring")])}}</div>
                       <div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-success add_featuring_artist"><i class="fa fa-plus-circle"></i></a></div>
                   </div>

                 @if ($errors->has('featuring_artist'))<span class="error">{{ $errors->first('featuring_artist') }}</span>@endif
                 <span class="error featuring_artist_error" ></span>
                </div>
                       @if(count($featuring_artist)>1)
                   @for($i=1;$i<count($featuring_artist);$i++)
                                              <div class="form-group">
                           <label for="primary_artist " class="control-label "></label>
                       <div class="row">
                       <div class="col-md-10">
                        {{ Form::text('featuring_artist[]',isset($featuring_artist[$i])?$featuring_artist[$i]:null,['class'=>'form-control featuring_artist featuring_artist_0','data-type'=>'featuring_artist','placeholder'=>trans("admin_lang.featuring")])}}
                       <br/>
                       </div>
                       <div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-danger remove_featuring_artist"><i class="fa fa-minus-circle"></i></a></div>
                       </div>
                       </div>
                   @endfor
                   @endif
                    <div class="featuring_artist_append"></div>
                <div class="form-group">
                 <label for="genre_id" class="control-label ">Various Artists / Compilation</label>
                    
                 {{ Form::checkbox('various_artists',1,false,['class'=>' various_artists' ,'data-type'=>'various_artists']) }}
                </div>    
                <div class="form-group  @if ($errors->has('genre_id')) has-error @endif">
                 <label for="genre_id" class="control-label required">{{ trans("admin_lang.genre") }}</label>
                    {{ Form::select('genre_id',$genre,isset($sdata->genre_id)?$sdata->genre_id:null,['class'=>'form-control genre_id','data-type'=>'genre_id','placeholder'=> ucwords(trans("admin_lang.select").' '.trans("admin_lang.genre")),'style'=>'text-transform: capitalize;']) }}
                    @if ($errors->has('genre_id'))<span class="error">{{ $errors->first('genre_id') }}</span>@endif
                </div>
                
                <div class="form-group  @if ($errors->has('subgenre_id')) has-error @endif">
                 <label for="subgenre_id" class="control-label">{{ trans("admin_lang.subgenre") }}</label>
                    {{ Form::select('subgenre_id',$subgenre,isset($sdata->subgenre_id)?$sdata->subgenre_id:null,['class'=>'form-control subgenre_id','data-type'=>'subgenre_id','placeholder'=> ucwords(trans("admin_lang.select").' '.trans("admin_lang.subgenre")),'style'=>'text-transform: capitalize;']) }}
                    @if ($errors->has('subgenre_id'))<span class="error">{{ $errors->first('subgenre_id') }}</span>@endif
                </div>
                
                    
                <div class="form-group  @if ($errors->has('label_id')) has-error @endif">
                 <label for="label_id" class="control-label required">{{ trans("admin_lang.label_name") }}</label>
                    {{ Form::select('label_id',$label,isset($sdata->label_id)?$sdata->label_id:null,['class'=>'form-control label_id','data-type'=>'label_id','placeholder'=> ucwords(trans("admin_lang.select").' '.trans("admin_lang.label")),'style'=>'text-transform: capitalize;']) }}
                    @if ($errors->has('label_id'))<span class="error">{{ $errors->first('label_id') }}</span>@endif
                </div>

                <div class="form-group new_label @if ($errors->has('new_label')) has-error @endif" style="display: none;">
                    <label for="new_label" class="control-label"> &nbsp;</label>
                   {{ Form::text('new_label',null,['class'=>'form-control','data-type'=>'new_label','placeholder'=>trans("admin_lang.new_label")])}}
                 @if ($errors->has('new_label'))<span class="error">{{ $errors->first('new_label') }}</span>@endif
                </div>

              <div class="form-group  @if ($errors->has('format')) has-error @endif">
                 <label for="format" class="control-label required">{{ trans("admin_lang.format") }}</label>
                    {{ Form::select('format',['SINGLE'=>'SINGLE','ALBUM'=>'ALBUM'],isset($sdata->format)?$sdata->format:null,['class'=>'form-control format','data-type'=>'format','style'=>'text-transform: capitalize;']) }}
                    @if ($errors->has('format'))<span class="error">{{ $errors->first('format') }}</span>@endif
                </div>
                <div class="form-group @if ($errors->has('original_release_date')) has-error @endif">
                    <label for="original_release_date" class="control-label required">{{ trans("admin_lang.release_date") }}</label>
                    {{ Form::text('original_release_date',null,['class'=>'form-control original_release_date','id'=>'datetimepicker','data-type'=>'original_release_date','autocomplete'=>'off','placeholder'=>trans("admin_lang.release_date"),'data-type'=>'original_release_date','readonly'=>'readonly'])}}
                @if ($errors->has('original_release_date'))<span class="error">{{ $errors->first('original_release_date') }}</span>@endif
                </div>
        	</div>
        

                       
                       
                       
        	<div class="col-md-5">
        		<div class="form-group @if ($errors->has('p_line')) has-error @endif">
                   <label for="p_line" class="control-label required">  ℗ {{ trans('admin_lang.line') }}</label>
                   {{ Form::text('p_line',null,['class'=>'form-control p_line','data-type'=>'p_line','id'=>'p_line','placeholder'=>trans("admin_lang.line")])}}
                 @if ($errors->has('p_line'))<span class="error">{{ $errors->first('p_line') }}</span>@endif
                </div>

                <div class="form-group @if ($errors->has('c_line')) has-error @endif">
                   <label for="title" class="control-label required">&copy; {{ trans('admin_lang.line') }}</label>
                   {{ Form::text('c_line',null,['class'=>'form-control c_line','data-type'=>'c_line','id'=>'c_line','placeholder'=>trans("admin_lang.line")])}}
                 @if ($errors->has('c_line'))<span class="error">{{ $errors->first('c_line') }}</span>@endif
                </div>
                <div class="form-group @if ($errors->has('production_year')) has-error @endif">
                   <label for="production_year" class="control-label required">{{ trans('admin_lang.production_year') }}</label>
                   <select name="production_year" class="form-control production_year" placeholder="Select Year" data-type='production_year'>
                       <option>Select Year</option>
                       @for($i=date('Y')+1;$i>=1950; $i--)
                       <option value="{{$i}}" @if(isset($sdata->production_year) && $sdata->production_year==$i) selected @endif>{{$i}}</option>
                       @endfor                       
                   </select>
                 @if ($errors->has('production_year'))<span class="error">{{ $errors->first('production_year') }}</span>@endif
                </div>
                    
                <div class="form-group @if ($errors->has('UPC')) has-error @endif">
                   <label for="title" class="control-label required">{{ trans('admin_lang.upc') }}</label>
                   {{ Form::text('UPC',isset($sdata->UPC)?$sdata->UPC:null,['class'=>'form-control UPC','id'=>'upc','data-type'=>'UPC','placeholder'=>trans("admin_lang.upc"),'readonly'=>'readonly'])}}
                 @if ($errors->has('UPC'))<span class="error">{{ $errors->first('UPC') }}</span>@endif
                </div>

                <div class="form-group @if ($errors->has('title')) has-error @endif">
                   <label for="title" class="control-label required">{{ trans('admin_lang.producer') }}</label>
                   @if(isset($sdata->producer_catalogue_number) && !empty($sdata->producer_catalogue_number))
                   {{ Form::text('producer_catalogue_number',null,['class'=>'form-control producer_catalogue_number','data-type'=>'producer_catalogue_number','id'=>'producer','placeholder'=>trans("admin_lang.producer"),'readonly'=>'readonly'])}}
                 @else
                  {{ Form::text('producer_catalogue_number',null,['class'=>'form-control producer_catalogue_number','data-type'=>'producer_catalogue_number','id'=>'producer','placeholder'=>trans("admin_lang.producer")])}}                 
                 @endif
                   @if ($errors->has('producer_catalogue_number'))<span class="error">{{ $errors->first('producer_catalogue_number') }}</span>@endif
                </div>
                    
                    
                  </div>
        	</div>
        </div>
   </div>

<script type="text/javascript">
  $(document).ready(function(){
    $(".label_id").select2({placeholder: 'Select Label'});
    $('.label_id').change(function() {
      var id = $(this).val();
      if(id=="new")
      {
        $(".new_label").show();
      }
      else
      {
        $(".new_label").hide();;
      }
    });    

  });

</script>

<script>
    $(document).ready(function(){
        $('.title, .subtitle, .p_line, .c_line, .original_release_date, .producer_catalogue_number').keyup(function(){
          var val =  $(this).val();
          var type = $(this).data('type');
         ajaxFormSubmit(val,type);
        });
        
         $(document).on('change','.genre_id, .subgenre_id, .label_id, .format, .production_year, .original_release_date ',function(){
          var val =  $(this).val();
          var type = $(this).data('type');          
            ajaxFormSubmit(val,type);              
        });
        
        
        
        
         $(document).on('keyup','.primary_artist, .featuring_artist',function(){
            var val = [];
            var type = $(this).data('type');
             $('.'+type).each(function($i){
                 val[$i] = $(this).val();
             });             
              ajaxFormSubmit(val,type);  
         });
        
        function ajaxFormSubmit(val,type){
             $.ajax({
                 url: '{{ routeUser('ajax.formsubmit') }}',
                 type: 'post',
                 data: {
                   '_token': '{{ csrf_token() }}',
                   'val':val,
                   'type':type,
                   'release_id':{{$sdata->id}}
                 },
                 success: function (data) 
                 {
                 },
             });
        }
        
        
        
        $('#datetimepicker').datetimepicker({
            timepicker:false,
            format:'Y-m-d',
            maxDate:new Date(),
        onChangeDateTime:exampleFunction
        });
        function exampleFunction(){
            var val =  $('.original_release_date').val();
            var type = $('.original_release_date').data('type');
            ajaxFormSubmit(val,type);
        }
        
        
        $('.various_artists').click(function(){
            var ck_status=  $(this).is(":checked");
            if(ck_status==true){
                $('.primary_artist').attr('readonly','readonly');
                $('.featuring_artist').attr('readonly','readonly');
                $('.primary_artist_error').html('');
                $('.featuring_artist_error').html('');
            }
            else{
                $('.primary_artist').removeAttr('readonly');
                $('.featuring_artist').removeAttr('readonly');
                $('.primary_artist_error').html('');                
                $('.featuring_artist_error').html('');                
            }
        })
        
        var i=1;
        $('.add_primary_artist').click(function(){
            var status = 'false'; 
            $('.primary_artist').each(function(){
               var primary_artist =  $(this).val();
               if(primary_artist==''){
                   status = 'true';
                   return;
               }
           });
           if(status=='false'){
                $('.primary_artist_append').append('<div class="form-group pri_art'+i+'"><label class="control-label">&nbsp;</label><div class="row"><div class="col-md-10"><input class="form-control primary_artist primary_artist_'+i+'" data-type="primary_artist" placeholder="Primary Artist" name="primary_artist[]" type="text"></div><div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-danger remove_primary_artist"><i class="fa fa-minus-circle"></i></a></div></div></div>')
                $('.primary_artist_error').html('');             
                i++;  
            }
            else{
                $('.primary_artist_error').html('<br>You must enter a name on the current field before adding a new one<br>');                      
            }
        });
        $(document).on('click','.remove_primary_artist',function(){
          $(this).closest('.form-group').remove();
        });
        var j=1;
        $('.add_featuring_artist').click(function(){
            var status = 'false';
            $('.featuring_artist').each(function(){
               var primary_artist =  $(this).val();
               if(primary_artist==''){
                   status = 'true';
                   return;
               }
           });
           if(status=='false'){
                $('.featuring_artist_append').append('<div class="form-group pri_art'+j+'"><label class="control-label">&nbsp;</label><div class="row"><div class="col-md-10"><input class="form-control featuring_artist featuring_artist_'+j+'" data-type="featuring_artist" placeholder="Featuring" name="featuring_artist[]" type="text"><br/></div><div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-danger remove_featuring_artist"><i class="fa fa-minus-circle"></i></a></div></div>')
                $('.featuring_artist_error').html('');             
                j++;  
            }
            else{
                $('.featuring_artist_error').html('<br>You must enter a name on the current field before adding a new one');                      
            }
        });
        $(document).on('click','.remove_featuring_artist',function(){
          $(this).closest('.form-group').remove();
        });
        
       $('.genre_id').change(function(){
       var genre_id = $(this).val();
       $(".loding_img").show();
            $.ajax({
                 url: '{{ routeUser('ajax.subgenre') }}',
                 type: 'post',
                 data: {
                   '_token': '{{ csrf_token() }}',
                   'genre_id':genre_id,
                 },
                 success: function (data) 
                 {
                     $('.subgenre_id').html(data);
                     $(".loding_img").hide();
                 },
                 error: function (data) {
                     return false;
                 }
             });
       }); 
       
       $('#album_profile').change(function(){
            var fi = document.getElementById('album_profile');         
           FileDetails_single(fi,'btn-file-error','IMAGE')
       });
       
       
       function FileDetails_single(fi,cla,type) {

             if (fi.files.length > 0 && fi.files.length < 6) {
       
                 document.getElementById(cla).innerHTML =
                        'Total Files: <b>' + fi.files.length + '</b>';
                
                 for (var i = 0; i <= fi.files.length - 1; i++) {
                     var result = readURL_single(fi.files.item(i),type);
                     if(result==false)
                     {
                        $(fi).val('');
                        return false;
                     }
                   
                     
                     var fname = fi.files.item(i).name;
                     // THE NAME OF THE FILE.
                     var fsize = fi.files.item(i).size;
                     if(fsize<'10485760')
                     {

                         document.getElementById(cla).innerHTML =
                         document.getElementById(cla).innerHTML + '<br /> ' +
                             fname + ' (<b>' + fsize + '</b> bytes)';
                    }
                    else
                    {
                       $('#'+cla).html('');
                       $(fi).val('');
                       $('#'+cla).text('Only flie size 10 MB  allowed');
                       return false;
                    }
                 }
             }
             else{
                $(fi).val('');
                $('.'+cla).text('You can only upload a maximum 1 file');
             }             
         }
         
        function readURL_single(input,type) {
            var type_reg = /^image\/(jpg|jpeg)$/;
            console.log(type_reg);
            var selecttype =  input.type;
            console.log(type_reg.test(selecttype));
            if (type_reg.test(selecttype)) {
             console.log(input);
                var reader = new FileReader();
                    reader.onload = function (e) {                       
                        $(input).parents('.file-upload').find('abbr').show();
                        $('.vimg1').html('<img class="v_icon_img1" src="' + e.target.result + '">');
                   }
                   reader.readAsDataURL(input);
                } else {
                    $(input).val('');
                    $('#btn-file-error').text("Only formats are allowed : jpg");
                    return false;
                   }  
               }
               $(document).on('click','.upload_image',function(){
               var width = $('.v_icon_img1').width();
               var height = $('.v_icon_img1').height();
               var src = $('.v_icon_img1').attr('src');
               if(width==3000 && height==3000)
               {
                 $('#v_icon_img').attr('src',src);
                 $('#myModal').modal('hide');
               }
               else{
                   $('#album_profile').val('');
                   $('#btn-file-error').text("Your cover must be: Size: 3000*3000 pixels"); 
                   return false;
               }
               });
               
               
               
               
   });
   
</script>
