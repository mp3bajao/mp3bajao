@extends('admin.layouts.layout')
@section('content')
<link rel="stylesheet" href="{{ asset('public/admintheme/dist/css/form_custom.css')}}">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header1">
         @include('admin.partials.errors')
   </section>
   <section class="content-header">
      <h1>
         {{ trans('admin_lang.'.$langPath) }}
         <small>{{ trans('admin_lang.view').' '.trans('admin_lang.'.$langPath) }}</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="{!! url('admin/dashboard') !!}"><i class="fa fa-dashboard"></i> {{ trans('admin_lang.home') }}</a></li>
         <li class="active"><a href="{!! routeUser($pagePath.'.index') !!}">{{ trans('admin_lang.'.$langPath) }}</a></li>
         <li class="active"> {{ trans('admin_lang.view').' '.trans('admin_lang.'.$langPath) }}</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- Info boxes -->
      <div class="row">
         <div class="col-xs-12">
            <div class="box">
               <div class="box-header">
                   <a class="btn btn-social btn-primary" href="{{routeUser($pagePath.'.index')}}">
                        <i class="fa fa-arrow-circle-o-left"></i> {{ trans('admin_lang.back') }}
                        </a>
                  <!-- <h3 class="box-title">Hover Data Table</h3> -->
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <div class="row">
   <div class="col-sm-12">
      <div class="bg box box-default">
                   <div class="row">
    <div class="col-sm-12">
        
                  <div class="modal" id="myModal" >
  <div class="modal-dialog" style="width:50%">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Assets Metadata</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
          <div class="modal-body-content">
              
          </div>
      </div>
    </div>
  </div>
</div>
      
        <h1><small>Release information @if($e_release>0) <a href="{{routeUser($pagePath.'.edit',['id'=>encrypt($sdata->id),'type'=>'release'])}}"><span class="label label-danger">{{$e_release}} error(s) found</span></a> @else <a href="{{routeUser($pagePath.'.show',['id'=>encrypt($sdata->id),])}}"><span class="label label-success">View details</span></a> @endif</small></h1>
        <?php $i=0; ?>
        
          <table class="table table-striped table-bordered" >
                       <tbody>
                            <tr>
                                <td style="width:40%">
                                    <label for="title" class="control-label required">{{ trans('admin_lang.rtitle') }}</label>&nbsp; : &nbsp; {{isset($sdata->title)?ucfirst($sdata->title):'empty'}}</td>
                                <td style="width:40%">  <label for="genre_id" class="control-label required">{{ trans("admin_lang.genre") }}</label>&nbsp; : &nbsp;  {{!empty($sdata->genre_value)?$sdata->genre_value:'empty'}}</td>
                                <td rowspan="7"><img src="{{$sdata->album_profile}}" class="img-responsive img-thumbnail"  width="60%"></td>
                            </tr>
                            <tr>
                                <td style="width:40%">
                                     <label for="subtitle" class="control-label">{{ trans('admin_lang.version') }}</label>&nbsp; : &nbsp; {{isset($sdata->subtitle)?ucfirst($sdata->subtitle):'empty'}}</td>
                                <td style="width:40%"> <label for="subgenre_id" class="control-label">{{ trans("admin_lang.subgenre") }}</label>&nbsp; : &nbsp;  {{!empty($sdata->subgenre_value)?$sdata->subgenre_value:'empty'}}</td>
                            
                            </tr>
                            <tr>
                                <td style="width:40%">
                                      <label for="primary_artist" class="control-label required">{{ trans('admin_lang.primary') }}</label>&nbsp; : &nbsp; {{isset($sdata->primary_artist)?ucwords(str_replace('|',', ',$sdata->primary_artist)):'empty'}}</td>
                                <td style="width:40%"> <label for="p_line" class="control-label required">  ℗ {{ trans('admin_lang.line') }}</label>&nbsp; : &nbsp; {{isset($sdata->p_line)?ucwords($sdata->p_line):'empty'}}</td>
                                
                            </tr>
                            <tr>
                                <td style="width:40%">
                                      <label for="primary_artist" class="control-label required">{{ trans('admin_lang.featuring') }}</label>&nbsp; : &nbsp; {{isset($sdata->featuring_artist)?ucwords(str_replace('|',', ',$sdata->featuring_artist)):'empty'}}</td>
                                <td style="width:40%"> <label for="p_line" class="control-label required">  {{ trans('admin_lang.producer_catalogue_number') }}</label>&nbsp; : &nbsp; {{isset($sdata->producer_catalogue_number)?ucwords($sdata->producer_catalogue_number):'empty'}}</td>
                                
                            </tr>

                            <tr>
                                <td style="width:40%">
                                       <label for="label_id" class="control-label required">{{ trans("admin_lang.label_name") }}</label>&nbsp; : &nbsp; {{!empty($sdata->label_value)?ucwords($sdata->label_value):'empty'}}</td>
                                <td style="width:40%"><label for="title" class="control-label required">&copy; {{ trans('admin_lang.line') }}</label>&nbsp; : &nbsp; {{isset($sdata->c_line)?ucwords($sdata->c_line):'empty'}}</td>
                                
                            </tr>
                            <tr>
                                <td style="width:40%">
                                        <label for="format" class="control-label required">{{ trans("admin_lang.format") }}</label>&nbsp; : &nbsp; {{!empty($sdata->format)?ucwords($sdata->format):'empty'}}</td>
                                <td style="width:40%"><label for="production_year" class="control-label required">{{ trans('admin_lang.production_year') }}</label>&nbsp; : &nbsp; {{isset($sdata->production_year)?ucwords($sdata->production_year):'empty'}}</td>
                               
                            </tr>
                            <tr>
                                <td style="width:40%">
                                        <label for="original_release_date" class="control-label required">{{ trans("admin_lang.release_date") }}</label>&nbsp; : &nbsp; {{isset($sdata->release_date)?date('d M Y',strtotime($sdata->release_date)):'empty'}}</td>
                                <td style="width:40%"><label for="title" class="control-label required">{{ trans('admin_lang.upc') }}</label>&nbsp; : &nbsp; {{isset($sdata->UPC)?ucwords($sdata->UPC):'empty'}}</td>
                               
                            </tr>
                            <tr>
                                <td style="width:40%">
                                        <label for="original_release_date" class="control-label required">#Songs</label>&nbsp; : &nbsp; {{!empty($sdata->track)?$sdata->track:'empty'}}</td>
                                <td style="width:40%"></td>
                                
                            </tr>
                       </tbody>
          </table>
        
        <h1><small>Store & Price  @if($e_price>0) <a href="{{routeUser($pagePath.'.edit',['id'=>encrypt($sdata->id),'type'=>'price_tire'])}}"><span class="label label-danger">{{$e_price}} error(s) found</span></a> @else <a href="{{routeUser($pagePath.'.show',['id'=>encrypt($sdata->id)])}}"><span class="label label-success">View details</span></a> @endif </small></h1>
         <table class="table table-striped table-bordered" >
                       <tbody>
                            <tr>
                                <td style="width:40%">
                                    <label for="format" class="control-label required">{{ trans("admin_lang.choose_price_tire") }}</label>&nbsp; : &nbsp; $9.99 {{--!empty($sdata->price_value)?ucfirst($sdata->price_value):'empty'--}}</td>
                            </tr>
                       </tbody>
         </table>
        <h1><small>Release Date & time   @if($e_release_date>0) <a href="{{routeUser($pagePath.'.edit',['id'=>encrypt($sdata->id),'type'=>'release_date'])}}"><span class="label label-danger">{{$e_release_date}} error(s) found</span></a> @else <a href="{{routeUser($pagePath.'.show',['id'=>encrypt($sdata->id)])}}"><span class="label label-success">View details</span></a> @endif</small></h1>
         <table class="table table-striped table-bordered" >
                       <tbody>
                            <tr>
                                <td style="width:100%">
                                    <label for="format" class="control-label required">{{ trans("admin_lang.release_date") }}</label>&nbsp; : &nbsp; {{isset($sdata->release_date)?date('d M Y h:i:s',strtotime($sdata->release_date)):'empty'}}</td>
                            </tr>
                       </tbody>
         </table>
        <h1><small>Assets @if($total_track<1) <a href="{{routeUser($pagePath.'.edit',['id'=>encrypt($sdata->id),'type'=>'upload_assets'])}}"><span class="label label-danger">{{$total_track}} Assets not found</span></a> @else @if($total_track_count>0) <a href="{{routeUser($pagePath.'.edit',['id'=>encrypt($sdata->id),'type'=>'upload_assets'])}}"><span class="label label-danger">{{$total_track}} error(s) found</span></a> @else <a href="{{routeUser($pagePath.'.show',['id'=>encrypt($sdata->id)])}}"><span class="label label-success">View details</span></a> @endif @endif</small>
          @if(Auth::user()->role_id==1 && !in_array($sdata->release_current_status,['TakeDown','NONE']))
          <a href="{{routeUser($pagePath.'.updateMetadataOnTiktok',['upc'=>encrypt($sdata->UPC),'action'=>'FullUpdate','store_id'=>21])}}" class="btn btn-success pull-right">FullUpdate on Tiktok</a>
          @endif
        </h1>
         <table id="userTable" class="table table-bordered table-hover datatable">
            <thead>
                <tr>
                    <th class="action">{{ trans("admin_lang.id") }}</th>                 
                    <th class="action">{{ trans('admin_lang.length_upload_file') }}</th>
                    <th class="action">{{ trans('admin_lang.artist')}}</th>
                    <th class="action">{{ trans('admin_lang.ISRC') }}</th>
                    <th class="action">{{ trans('admin_lang.tiktok_preview_start') }}</th>
                    <th class="action">{{ trans('admin_lang.status') }}</th>
                    <th class="action" width="110px">{{ trans("admin_lang.action") }}</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>{{ trans("admin_lang.id") }}</th>                 
                    <th>{{ trans('admin_lang.length_upload_file') }}</th>
                    <th>{{ trans('admin_lang.artist')}}</th>
                    <th class="action">{{ trans('admin_lang.ISRC') }}</th>
                    <th class="action">{{ trans('admin_lang.tiktok_preview_start') }}</th>
                    <th class="action">{{ trans('admin_lang.status') }}</th>
                    <th class="action" width="110px">{{ trans("admin_lang.action") }}</th>
                </tr>
            </tfoot>
        </table>
   </div>
</div>
               </div>
               <!-- /.box-body -->
            </div>
            <!-- /.box -->
         </div>
      </div>
      </div>
      </div>
      </div>
     
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
$(document).ready(function(){
       var table = $('#userTable').DataTable({
           "bProcessing": true,
           "serverSide": true,
           "pageLength": 100,
           "order": [[0, "asc"]],
           "bPaginate": false,
           "searching": false,
           "ajax": {
               url: "{{ routeUser($pagePath.'.track',['id'=>$sdata->id]) }}",
               data: function (d) {
                    return $.extend({}, d, {
                        "start_date": $('.sdate').val(),
                        "end_date": $('.edate').val(),
                        'view_type':'view',
                    });
                },
               error: function () {
                   alert("{{trans('admin_lang.something_went_wrong')}}");
               }
           },
          
           "aoColumns": [
               {mData: 'DT_RowId'},
               {mData: 'title'},
               {mData: 'artist'},
               {mData: 'ISRC'},
               {mData: 'preview_start'},
               {mData: 'e_track'},
               {mData: 'actions'}
           ],
           "aoColumnDefs": [
               {"bSortable": false, "aTargets": ['action']}
           ],
            language: {
            searchPlaceholder: "Search by file information"
        },
       }); 
       
         $(document).on('click','.edit_track',function(){ 
        var url = $(this).data('href_url');
        $(".loding_img").show();
            $.ajax({
                 url: url,
                 type: 'get',
                 data:{'form_type':'update'},
                 success: function (data) 
                 {
                    $('#myModal').modal('show');
                    $(".loding_img").hide();
                    $('.modal-body-content').html(data);
                 },               
             });
        });
        @if(Auth::user()->role_id==1)
        $(document).on('change','.preview_start',function(){
          if($(this).val()=='' || $.isNumeric($(this).val())){
            $(".loding_img").show();
            $.ajax({
              url:"{{routeUser('track.update_preview_time')}}",
              type: 'post',
              data: {
                  '_token': '{{ csrf_token() }}',
                  'track_id':$(this).data('id'),
                  'preview_start':$(this).val()                        
              },
              dataType:'JSON',
              success: function (data){
                $(".loding_img").hide();
                $('#myModal').modal('hide');
                if(data.status==false){
                      $('.content-header1').html('<div class="alert alert-danger"><strong>Danger!</strong> '+data.message+'</div>')
                }else{
                  table.ajax.reload();
                  $('.content-header1').html('<div class="alert alert-success"><strong>Success!</strong> '+data.message+'</div>');
                }
              }
            });
          }else{
            alert('Only numeric value is allowed.');
          }
        });
        @endif
        
        
        $(document).on('click','.form_submit_track',function(){
            $(".upload_submit_track").validate({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },		
                rules: {
                    title: "required",
                    'primary_artist[]': "required",
                    'author[]': "required",
                    //'composer[]': "required",
                    'arranger[]': "required",
                    genre_id: "required",				
                    label_id: "required",				
                    format: "required",				
                    original_release_date: "required",				
                    p_line: "required",				
                    c_line: "required",				
                    production_year: "required",
                    UPC: "required",
                    price_id: "required",
                    title_language: "required",
                    //lyrics_language: "required",
                    ISRC: "required",
                    preview_start:{
                                required: false,
                                digits: true
                              },
		},
		messages: {
                    title: "The title field is required.",
                    'primary_artist[]': "The primary artist field is required.",
                    'author[]': "The author field is required.",
                    'composer[]': "The composer field is required.",
                    genre_id: "The genre field is required.",				
                    label_id: "The label field is required.",				
                    format: "The format field is required.",				
                    original_release_date: "The Physical/Original release date field is required.",				
                    p_line: "The  ℗ line field is required.",				
                    c_line: "The &copy; line  field is required.",				
                    production_year: "The production year  field is required.",				
                    UPC: "The UPC/EAN  field is required.",
                    price_id: "The main price tier field is required.",
                    title_language: "The title language field is required.",
                    lyrics_language: "The lyrics language field is required.",
                    ISRC: "The ISRC field is required.",
		}
            });
            if ($('.upload_submit_track').valid()) {
                $(".loding_img").show();
                var route_url =  $("input[name=route_url]").val();
                var track_id_main =  $("input[name=track_id_main]").val();
                var track_type =  $("input[name=track_type]").val();
                var secondary_track_type =  $("input[name=secondary_track_type]:checked").val();
                var instrumental =  $("input[name=instrumental]:checked").val();
                var title =  $("input[name=title]").val();
                var subtitle =  $("input[name=subtitle]").val();
                var publisher =  $("input[name=publisher]").val();
                var p_line =  $("input[name=p_line]").val();
                var ISRC =  $("input[name=ISRC]").val();
                var producer_catalogue_number =  $("input[name=producer_catalogue_number]").val();
                var track_for_the_release =  $("input[name=track_for_the_release]:checked").val();
                var parental_advisory =  $("input[name=parental_advisory]:checked").val();
                var track_lyrics =  $("input[name=track_lyrics]").val();
                var preview_start =  $("input[name=preview_start]").val();

                var production_year =  $(".production_year option:selected").val();
                var genre_id =  $(".genre_id option:selected").val();
                var subgenre_id =  $(".subgenre_id option:selected").val();
                var price_id =  $(".price_id option:selected").val();
                var lyrics_language =  $(".lyrics_language option:selected").val();
                var title_language =  $(".title_language option:selected").val();



                var primary_artist = [] ;
                $(".primary_artist").each(function(i){
                    primary_artist[i] = $(this).val();
                });

                var featuring_artist = [] ;
                $(".featuring_artist").each(function(i){
                    featuring_artist[i] = $(this).val();
                });
                var remixer = [] ;
                $(".remixer").each(function(i){
                    remixer[i] = $(this).val();
                });
                var author = [] ;
                $(".author").each(function(i){
                    author[i] = $(this).val();
                });
                var composer = [] ;
                $(".composer").each(function(i){
                    composer[i] = $(this).val();
                });
                var arranger = [] ;
                $(".arranger").each(function(i){
                    arranger[i] = $(this).val();
                });
                var producer = [] ;
                $(".producer").each(function(i){
                    producer[i] = $(this).val();
                });


                var form_data = {
                   'route_url':route_url,
                   'track_id_main':track_id_main,
                   'track_type':track_type,
                   'secondary_track_type':secondary_track_type,
                   'instrumental':instrumental,
                   'title':title,
                   'subtitle':subtitle,
                   'primary_artist':primary_artist,
                   'featuring_artist':featuring_artist,
                   'remixer':remixer,
                   'author':author,
                   'composer':composer,
                   'arranger':arranger,
                   'producer':producer,
                   'p_line':p_line,
                   'production_year':production_year,
                   'publisher':publisher,              
                   'ISRC':ISRC,
                   'genre_id':genre_id,
                   'subgenre_id':subgenre_id,
                   'track_for_the_release':track_for_the_release,
                   'price_id':price_id,
                   'producer_catalogue_number':producer_catalogue_number,                                         
                   'parental_advisory':parental_advisory,
                   'preview_start':preview_start, 
                   'title_language':title_language,
                   'track_lyrics':track_lyrics, 
                   'lyrics_language':lyrics_language,              
                };                
                
                $.ajax({
                    url:route_url,
                    type: 'post',
                    data: {
                        '_token': '{{ csrf_token() }}',
                        'form_data':form_data,                        
                    },
                    dataType:'JSON',
                    success: function (data) 
                    {
                         $(".loding_img").hide();
                         $('#myModal').modal('hide');
                        if(data.status==false)
                        {
                            $('.content-header1').html('<div class="alert alert-danger"><strong>Danger!</strong> '+data.message+'</div>')
                        }
                    else{
                         
                         table.ajax.reload();
                        $('.content-header1').html('<div class="alert alert-success"><strong>Success!</strong> '+data.success+'</div>');                
                        }
                    }
                    
                });
            }
        });                
});

</script>

@endsection

