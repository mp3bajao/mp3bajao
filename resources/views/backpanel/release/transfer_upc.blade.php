@extends('admin.layouts.layout')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header1">
    @include('admin.partials.errors')
   </section>
   <section class="content-header">
      <h1>{{ trans('admin_lang.transfer_upc') }}</h1>
      <ol class="breadcrumb">
         <li><a href="{!! userUrl('dashboard') !!}"><i class="fa fa-dashboard"></i> {{ trans("admin_lang.home") }}</a></li>
         <li class="active"><a href="{!! routeUser($pagePath.'.index') !!}">{{ trans("admin_lang.".$langPath).' '.trans('admin_lang.music')  }}</a></li>
         <li class="active"> {{ trans("admin_lang.transfer_upc")}}</li>
      </ol>
   </section>
   <section class="content">
      <!-- Info boxes -->
      <div class="row ">
         <div class="col-xs-12 ">
            <div class="box  newScroll">
               <div class="box-header">
                {{Form::open(array('route' => [routeFormUser($pagePath.'.transferUPCStore')], 'method'=>'POST', 'files'=>true, 'class'=>'upload_submit'))}}
                  <div class="row">
                    <div class="col-md-4">
                      {{Form::text('upc',isset($_GET['upc'])?$_GET['upc']:null,['class'=>'form-control','placeholder'=>'UPC'])}}
                      @if ($errors->has('upc'))<span class="error">{{ $errors->first('upc') }}</span>@endif
                    </div>
                    <div class="col-md-4">
                      {{Form::select('user_id',$users,null,['class'=>'form-control user_id','placeholder'=>'Select User'])}}
                      @if ($errors->has('user_id'))<span class="error">{{ $errors->first('user_id') }}</span>@endif
                    </div>
                    <div class="col-md-2">
                      <input type="submit" class="btn btn-primary pull-right form_submit" value="{{ trans('admin_lang.submit') }}">
                    </div>
                  </div>
                {{ Form::close() }}
               </div>
               <!-- /.box-header -->
            </div>
            <!-- /.box -->
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection
