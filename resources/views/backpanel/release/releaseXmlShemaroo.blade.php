<?php use App\Models\Track; ?>
<?xml version="1.0" encoding="UTF-8"?>
<album>
    <title>{{str_replace('&',' and ',$release->title)}}</title>
    <description></description>
    <language>{!!ucfirst(strtolower($release->getReleaseTrack[0]->language_name))!!}</language>
    <director>{!!str_replace('&',' and ',$release->getReleaseTrack[0]->arranger)!!}</director>
    <label>{!! str_replace('&',' and ',$release->label_value) !!}</label>
    <songs>
    @forelse($release->getReleaseTrack as $track)
    <?php $media = $track->getTrackMedia; ?>
    @if($media->is_converted==1)
        <track>
            <title>{!!str_replace('&',' and ',$track->title)!!}</title>
            <name>{!!$media->mp3_file!!}</name>
            <artists>
                <?php $main_artist = explode('|',$track->primary_artist); ?>
                @forelse($main_artist as $k => $main_artist)
                <artist>{!!str_replace('&',' and ',$main_artist)!!}</artist>
                @empty
                @endforelse
            </artists>
            <lyricist>{!!str_replace('&',' and ',$track->author)!!}</lyricist>
            <genres>
                <genre>{!! str_replace('&',' and ',$release->genre_value) !!}</genre>
            </genres>
            <tags>
                <tag></tag>
            </tags>
            <description></description>
            <lyrics></lyrics>
        </track>
    @endif
    @empty
    @endforelse
    </songs>
</album>