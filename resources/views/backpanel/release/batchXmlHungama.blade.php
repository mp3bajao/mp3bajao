<?php use App\Models\Track; ?>
<?xml version="1.0" encoding="UTF-8"?>
<echo:ManifestMessage xmlns:echo="http://ddex.net/xml/2011/echo/11"
                      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                      xsi:schemaLocation="http://ddex.net/xml/2011/echo/11 http://ddex.net/xml/2011/echo/11/echo.xsd"
                      MessageVersionId="ern/382">
    <MessageHeader>
        <MessageSender>
          <PartyId>PADPIDA2019041703R</PartyId>
          <PartyName>
            <FullName>Sanjivani Digital Entertainment Pvt Ltd</FullName>
          </PartyName>
        </MessageSender>
        <MessageRecipient>
          <PartyId>PADPIDA20140114048</PartyId>
          <PartyName>
            <FullName>Hungama</FullName>
          </PartyName>
        </MessageRecipient>
        <?php $dt = dateFormate(date('Y-m-dTH:i:s'));?>
        <MessageCreatedDateTime>{{$dt->format('Y-m-d').'T'.$dt->format('H:i:s').'Z'}}</MessageCreatedDateTime>
    </MessageHeader>
    <IsTestFlag>false</IsTestFlag>
    <DeliveryType UserDefinedValue="{{ ucfirst(strtolower($release->format))}}"
                Namespace="PADPIDA2019041703R">UserDefined</DeliveryType>
    <BatchProductType UserDefinedValue="{{ ucfirst(strtolower($release->format))}}"
                    Namespace="PADPIDA2019041703R">UserDefined</BatchProductType>
    <RootDirectory>./</RootDirectory>
    <NumberOfMessages>1</NumberOfMessages>
    <MessageInBatch>
        <MessageType>{{$messageType}}</MessageType>
        <MessageId>{!! $release->UPC !!}</MessageId>
        <URL>./{!! $release->UPC !!}/{!! $release->UPC !!}.xml</URL>
        <IncludedReleaseId>
          <GRid></GRid>
          <ICPN IsEan="true">{!! $release->UPC !!}</ICPN>
          <ProprietaryId Namespace="PADPIDA2019041703R">{!! $release->UPC !!}</ProprietaryId>
         </IncludedReleaseId>
        <HashSum>
          <HashSum>{{md5(count($release->getReleaseTrack))}}</HashSum>
          <HashSumAlgorithmType>MD5</HashSumAlgorithmType>
        </HashSum>
    </MessageInBatch>
</echo:ManifestMessage>
