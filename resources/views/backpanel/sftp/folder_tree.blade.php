<link rel="stylesheet" href="{{ asset('css/file-explore.css') }}"> 
<div class="row">

<ul class="file-tree">
    @if(!empty($data))
        @foreach($data as $folder)
        <input type="hidden" value="{{$folder}}" class="folder_name" name="folder_name[]"/>
        <li>
            
            <input type="checkbox" value="{{$folder}}" class="folder_name_checked">
            <a href="#">{{$folder}}</a>
            <?php 
                $sub_folder_name = $folder;
                $sub_folder_path =  $main_folder_path.$slash.$folder;                
                $sub_folder = array_slice(scandir($sub_folder_path),2);
                foreach($sub_folder as $key => $sub_folder_one)
                {?>
                <ul>
                        <?php 
                        $sub_folder_one_path =  $sub_folder_path.$slash.$sub_folder_one;
                        if(is_dir($sub_folder_one_path)==true)
                        {
                            $sub_folder_one_name = $sub_folder_one;
                            $sub_folder_two = array_slice(scandir($sub_folder_one_path),2);                            
                            //dd($sub_folder_two);
                        ?>
                            <li><a href="#">{{$sub_folder_one}}</a>                                
                            <ul>
                            <?php 
                            foreach($sub_folder_two as $key => $sub_folder_three)
                            {
                                $sub_folder_two_name = $sub_folder_one_path.$slash.$sub_folder_three;
                                if(is_dir($sub_folder_two_name)==true)
                                {
                                    $sub_folder_one_name = $sub_folder_three;
                                    $sub_folder_three_folder = array_slice(scandir($sub_folder_two_name),2);
                                ?>
                                <li><a href="#">{{$sub_folder_three}}</a> 
                                    <ul>
                                        <?php 
                                        foreach($sub_folder_three_folder as $sub_folder_three_folder){
                                        ?> 
                                            <li><a href="#">{{$sub_folder_three_folder}}</a> </li> 
                                        <?php } ?>
                                    </ul>
                                </li>  
                                <?php                                
                                }
                                else
                                { 
                                ?>
                                   <li><a href="#">{{$sub_folder_three}}</a> </li> 
                                <?php                                
                                }

                                ?>
                              
                            <?php                             
                            }
                            ?>
                                
                            </ul>
                         </li>   
                        <?php                         
                        }
                        else
                        { 
                        ?>
                           <li><a href="#">{{$sub_folder_one}}</a> </li>
                        <?php                          
                        }
                        ?>
                         </ul> 
                <?php } 
            ?>
           
        </li>
        @endforeach
    @else
    No Pending Folder in this sftp.
    @endif
    
    

<script src="{{ asset('js/file-explore.js') }}"></script> 
<script>
$(document).ready(function() {
    $(".file-tree").filetree();
});
</script>