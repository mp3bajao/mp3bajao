@extends('layouts.master')
@section('content')
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}"> 
<!-- Content Header (Page header) -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">{{ __('backend.'.$lang).' '.__('backend.manager') }}</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">{{ __('backend.home') }}</a></li>
                <li class="breadcrumb-item active">{{ __('backend.'.$lang).' '.__('backend.manager') }}</li>
            </ol>
        </div>
    </div>
</div>
<!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
    <div class="row">
        <div class="col-md-12">
        <div class="card card-primary card-outline">
                <div class="card-body">
                    <div class="table-responsive">
                    <table  id="category_listing" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>{{ __('backend.sr_no') }}</th>
                                <th>{{ __('backend.name') }}</th>
                                <th>{{ __('backend.folder') }}</th>                                
                                <th>{{ __('backend.upc') }}</th>                                
                                <th style="width: 70px;">{{ __('backend.status') }}</th>  
                                <th>{{ __('backend.message') }}</th>
                                <th>{!! __('backend.created_at') !!}</th>
                                <th>{{ __('backend.action') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
    </div>
</div>

</div>
    <!-- /.content -->

@endsection
@section('script')

<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/parsley.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
var ajax_datatable;
$(document).ready(function(){
        ajax_datatable = $('#category_listing').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ routeUser($page.'.report') }}',
            columns: [
              { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
              { data: 'title', name: 'stores.title' },
              { data: 'folder_name', name: 'folder_name' },
              { data: 'upc', name: 'upc' },
              { data: 'status', name: 'status' },
              { data: 'error_message', name: 'error_message' },
              { data: 'created_at', name: 'created_at' },
              {data: 'id', name: 'id', orderable: false, searchable: false}
            ],
            order: [ [6, 'desc'] ],
            rowCallback: function(row, data, iDisplayIndex) {  

              var links='';
              links += `<div class="btn-group" role="group" >`;  
              if(data.status === -1){
              links += `<a href="#" data-store_id="${data.store_id}" data-category_id="${data.folder_name}" title="Upload Album" class="btn btn-success btn-xs upload_album" ><span class="fas fa-upload"></span></a>`;
                }
              links += `</div>`;
              var status = '';
              if(data.status === 1){
                status += `<span class="label label-rounded label-success"><i class="fas fa-check-circle"></i> success</span>`;
              }else{
                status += `<span class='label label-rounded label-danger'><i class="fas fa-times"></i> Error </span>`;
              }
              $('td:eq(4)', row).html(status);
              $('td:eq(7)', row).html(links);
              },
        });
    });
    
    
    $(document).on('click','.upload_album',function(){
        var store_id = $(this).data('store_id');
        var folder_name  = [];
             folder_name[0] = $(this).data('category_id'); ;
            fileupload(store_id,folder_name,true); 
    });


    

    function fileupload(store_id,folder_name,upload){
  
        if(store_id!='')
        {
            var response = confirm('Are you sure want to upload files ?');
            if(response==true)
            {
                $('#lodder').show();
                var url = '{!! routeUser($page.'.upload_album')!!}'
                $.ajax({
                  type: 'post',
                  data: {_method: 'post', _token: "{{ csrf_token() }}",store_id:store_id,folder_name:folder_name,upoad:upload},
                  dataType:'json',
                  url: url,
                  success:function(res){
                    if(res.status === 1){ 
                      toastr.success(res.message);
                      ajax_datatable.draw();
                    }else{
                      toastr.error(res.message);
                    }
                  },   
                  error:function(jqXHR,textStatus,textStatus){
                    console.log(jqXHR);
                    toastr.error(jqXHR.statusText)
                  }
              });
              $('#lodder').hide();
            }
        }
        else
        {
            alert('Please Select store.');
        }
    }
  
</script>
@endsection
