@extends('layouts.master')

@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">{{ __($lang.' Manage') }}</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ routeUser('dashboard') }}">{{ __('Home') }}</a></li>
                <li class="breadcrumb-item active">{{ __($lang.' Manage') }}</li>
            </ol>

        </div>
    </div>
</div>
<!-- /.content-header -->
    <!-- Main content -->
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-outline">
                <div class="card-body"> 
                    <div class="row">
                        <div class="col-md-10">
                           
                        </div>                        
                        <div class="col-md-2">
                        </div>
                    </div>   
                    <Br/>
                    <div class="row">
                        <div class="col-md-12 folder_tree"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    @endsection
    @section('script')

@endsection
