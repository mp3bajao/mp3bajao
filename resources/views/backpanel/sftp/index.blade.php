@extends('layouts.master')

@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">{{ __($lang.' Manage') }}</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ routeUser('dashboard') }}">{{ __('Home') }}</a></li>
                <li class="breadcrumb-item active">{{ __($lang.' Manage') }}</li>
            </ol>

        </div>
    </div>
</div>
<!-- /.content-header -->
    <!-- Main content -->
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-outline">
                <div class="card-body"> 
                    <div class="row">
                        <div class="col-md-10">
                            {!! selectbox('store',$store,null,'','store') !!}
                        </div>                        
                        <div class="col-md-2">
                            <button type="button" class="btn btn-primary btn-sm uploadAlbum"><i class="fa fa-upload"></i> Upload Album</button>
                        </div>
                    </div>   
                    <Br/>
                    <div class="row">
                        <div class="col-md-12 folder_tree"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    @endsection
    @section('script')
<script>

   
$(document).on('click','.uploadAlbum',function(){    
   
    var store_id = $('.store option:selected').val();
     var folder_name = [];
    $('.folder_name').each(function(i){
      folder_name[i] = $(this).val()    ;
    });
    fileupload(store_id,folder_name,false); 
   
});  

$(document).on('click','.folder_name_checked',function(){
    var store_id = $('.store option:selected').val();
    var folder_name = [];
    if ($(this).prop("checked")) {
        folder_name[0] = $(this).val();
        fileupload(store_id,folder_name,true); 
    }
    //$('.loading_page').hide();
});


    function fileupload(store_id,folder_name,upload){
        if(store_id!='')
        {
            var response = confirm('Are you sure want to upload files ?');
            if(response==true)
            {
                $('.loading_page').show();
                var url = '{!! routeUser($page.'.upload_album')!!}'
                $.ajax({
                  type: 'post',
                  data: {_method: 'post', _token: "{{ csrf_token() }}",store_id:store_id,folder_name:folder_name,upoad:upload},
                  dataType:'json',
                  url: url,
                  success:function(res){
                    if(res.status === 1){ 
                      toastr.success(res.message);
                      setTimeout(function(){ 
                            $('.loading_page').hide();
                      }, 500);
                    
                    }else{
                      toastr.error(res.message);
                      setTimeout(function(){ 
                            $('.loading_page').hide();
                      }, 500);
                    }
                  },   
                  error:function(jqXHR,textStatus,textStatus){
                    console.log(jqXHR);
                    toastr.error(jqXHR.statusText)
                     setTimeout(function(){ 
                            $('.loading_page').hide();
                      }, 500);
                  }
              });
            }
        }
        else
        {
            alert('Please Select store.');
        }
    }
    
$(document).on('change','.store',function(){

   $('.loading_page').show();
   var id =  $(this).val();
   var url = "{{routeUser($page.'.folder_tree')}}";
   url = url+'/'+id;
   $('.folder_tree').load(url);
   setTimeout(function(){ 
         $('.loading_page').hide();
   }, 500);


});
</script>
@endsection
