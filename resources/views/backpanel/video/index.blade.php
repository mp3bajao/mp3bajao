@extends('layouts.master')
@section('content')
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}"> 
<!-- Content Header (Page header) -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">{{ __('backend.'.$lang).' '.__('backend.manager') }}</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">{{ __('Home') }}</a></li>
                <li class="breadcrumb-item active">{{ __('backend.'.$lang).' '.__('backend.manager') }}</li>
            </ol>

        </div>
    </div>
</div>
<!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
    <div class="row">
        <div class="col-md-12">
        <div class="card card-primary card-outline">
                <div class="card-body">
                    <div class="table-responsive">                    
                    <table  id="listing" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>{{ __('No.') }}</th>
                                <th>{{ __('Album') }}</th>
                                <th>{{ __('Title') }}</th>                                
                                <th>{{ __('Genre(s)') }}</th>
                                <th>{{ __('Year') }}</th>
                                <th>{{ __('No. of Track') }}</th>
                                <th>{{ __('Catalogue Number') }}</th>
                                <th>{{ __('Created At') }}</th>
                                <th>{{ __('Action') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
    </div>
</div>
</div>
    
    
<div class="modal" id="myModal" >
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
              <h4 class="modal-title">Tracks</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
      <!-- Modal body -->
      <div class="modal-body">
          <div class="modal-body-content trackListView">
              
          </div>
      </div>
    </div>
  </div>
                   
</div>
    
    @endsection
    @section('script')
    
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/parsley.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
var ajax_datatable;
$(document).ready(function(){
ajax_datatable = $('#listing').DataTable({
    processing: true,
    serverSide: true,
    ajax:{
      url:'{{ routeUser($page.'.index') }}',
      data: function (d) {
          
      }
    },
    columns: [
      { data: 'DT_RowId', name: 'DT_RowId', orderable: false, searchable: false, "width": "20px"},
      { data: 'album', name: 'album' },
      { data: 'title', name: 'title' },
      { data: 'genre', name: 'genre' },
      { data: 'year', name: 'year' },
      { data: 'no_of_track', name: 'no_of_track' },
      { data: 'producer_catalogue_number', name: 'producer_catalogue_number' },
      { data: 'created_at', name: 'created_at' },
      { data: 'actions', name: 'actions' },
    ],
    order: [ [0, 'desc'] ],
   /* rowCallback: function(row, data, iDisplayIndex) {  
      
      var links='';
      links += `<div class="btn-group" role="group" >`;
      @can('Product-edit')
      links += `<a href="{{url('product/edit')}}/${data.id}" title="Edit Details" class="btn btn-primary btn-xs" ><span class="fa fa-edit"></span></a>`;
      @endcan
      @can('Product-delete')
      //links += `<a href="#" data-product_id="${data.id}" title="Delete" class="btn btn-danger btn-xs delete_btn" ><span class="fa fa-trash"></span></a>`;
      @endcan

      links += `<a href="#" data-product_id="${data.id}" title="Images Details" class="btn btn-primary btn-xs images_btn" ><span class="fa fa-image"></span></a>`;
      @can('Product-edit')
      links += `<a href="#" data-product_id="${data.id}" title="View Details" class="btn btn-info btn-xs view_btn" ><span class="fa fa-eye"></span></a>`;
      @endcan
      links += `</div>`;
      var status = '';
      if(data.is_active === 1){
        status += `<a href="#" data-product_id="${data.id}" title="Active Account" data-status="deactive" class="change_status"><span class='label label-rounded label-success'>Active</span></a>`;
      }else{
        status += `<a href="#" data-product_id="${data.id}" title="Deactive Account" data-status="active" class="change_status"><span class='label label-rounded label-warning'>Deactive</span></a>`;
      }
      $('td:eq(4)', row).html(status);
      $('td:eq(6)', row).html(links);
      },*/
});

$('#search-form').on('submit', function(e) {
      ajax_datatable.draw();
        e.preventDefault();
});
$('#refresh').click(function(){
  $('#chef_id').val('');
  $('#celebrity_id').val('');
  ajax_datatable.draw();
 });
 });
 
$(document).on('click','.trackList',function(e){
var release_id = $(this).data('release_data');
var url = '{{ routeUser('track.list')}}/'+release_id;
$('.trackListView').load(url);
$('#myModal').modal('show');
}); 

$(document).on('click','.change_status',function(e){
      e.preventDefault();
      status = $(this).data('status');
      if(status == 'active'){
        var response = confirm('Are you sure want to active this product?');
      }else{
        var response = confirm('Are you sure want to deactive this product?');
      }
      if(response){
        id = $(this).data('product_id');
        $.ajax({
          type: 'post',
          data: {_method: 'get', _token: "{{ csrf_token() }}"},
          dataType:'json',
          url: "{!! url('product/changeStatus' )!!}" + "/" + id +'/'+status,
          success:function(res){
            if(res.status === 1){ 
              toastr.success(res.message);
              ajax_datatable.draw();
            }else{
              toastr.error(res.message);
            }
          },   
          error:function(jqXHR,textStatus,textStatus){
            console.log(jqXHR);
            toastr.error(jqXHR.statusText)
          }
      });
      }
      return false;
    }); 
    
</script>

@endsection
