<div class="row colspace_col">
    <div class="col-sm-6">
       <div class="form-group  @if ($errors->has('format')) has-error @endif">
            <label for="format" class="control-label required">{{ trans("admin_lang.choose_price_tire") }}</label>$9.99
            {{-- Form::select('price_id',$price,null,['class'=>'form-control price_id','placeholder'=> ucwords(trans("admin_lang.choose_price_tire")),'style'=>'text-transform: capitalize;']) --}}
             @if ($errors->has('price_id'))<span class="error">{{ $errors->first('price_id') }}</span>@endif
        </div>
   </div>
</div>
<script>
$(document).ready(function(){
   $('.price_id').change(function(){
      $('.upload_submit').submit(); 
   }); 
});
</script>