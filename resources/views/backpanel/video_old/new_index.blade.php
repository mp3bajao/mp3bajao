@extends('admin.layouts.layout')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
    <section class="content-header1">
        @include('admin.partials.errors')
    </section>
    <section class="content-header">
      <h1>
         {{ trans('admin_lang.'.$langPath) }}
      </h1>
      <ol class="breadcrumb">
         <li>
            <a href="{{ routeUser('dashboard') }}"><i class="fa fa-dashboard"></i> {{ trans("admin_lang.home") }}</a>
         </li>
         <li class="active">
            <!-- <i class="fa fa-gears"></i> -->{{ trans('admin_lang.'.$langPath) }}
         </li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- Info boxes -->
      <div class="row">
         <div class="col-xs-12">
            <div class="box newScroll">
               <div class="box-header">
               <div class="box-body">
                   
                <div id="bs-main-content" class="col-md-12">                        
                <h2>Create your releases</h2>
                <p id="guidelinesLink"><a href="{{asset('public/Sanjivani_digital_content_delivery_guidelines_for_audio_stores.pdf  ')}}" target="_blank">Content delivery guidelines for audio stores</a></p>
                <div class="row bs-fullPageMenu-row">
                    <div class="col-md-12">
                        <div class="row">                
                                <div class="col-lg-4">
                                    <a class="bs-fullPageMenu-element-link" href="{{ routeUser('release.create') }}">
                                        <div class="bs-fullPageMenu-element">
                                            <div class="bs-fullPageMenu-element-logo">
                                                <span class="helper"></span><img src="{{ asset('public/One_release.svg') }}">
                                            </div>
                                            <div class="bs-fullPageMenu-element-details">
                                                <div class="bs-fullPageMenu-element-title">
                                                    <span>One Release Music</span>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4">
                                    <a class="bs-fullPageMenu-element-link" href="{{ routeUser('release.multiple') }}">
                                        <div class="bs-fullPageMenu-element">
                                            <div class="bs-fullPageMenu-element-logo">
                                                <span class="helper"></span><img src="{{ asset('public/Multi_release.svg') }}">
                                            </div>
                                            <div class="bs-fullPageMenu-element-details">
                                                <div class="bs-fullPageMenu-element-title">
                                                    <span>Multiple Release Music</span>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4">
                                    <a class="bs-fullPageMenu-element-link" href="{{ routeUser('release.video.create') }}">
                                        <div class="bs-fullPageMenu-element">
                                            <div class="bs-fullPageMenu-element-logo">
                                                <span class="helper"></span><img src="{{ asset('public/One_release.svg') }}">
                                            </div>
                                            <div class="bs-fullPageMenu-element-details">
                                                <div class="bs-fullPageMenu-element-title">
                                                    <span>One Release Video</span>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>
               </div>              
               <!-- /.box-body -->
            </div>
            <!-- /.box -->
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<style>


#guidelinesLink {
    margin-bottom: 48px;
}

.bs-fullPageMenu-element {
    box-shadow: 0px 2px 3px 0px rgba(46, 40, 64, 0.5);
    text-align: center;
    margin-bottom: 24px;
}

.bs-fullPageMenu-element-logo {
    height: 250px;
    background-color: rgba(203, 205, 209, 0.15);
}

.bs-fullPageMenu-element-logo  img {
    height: 160px;
}

.bs-fullPageMenu-element-title {
    font-size: 15px;
    padding: 30px 0px;
    color: #7C818D;
}

.bs-fullPageMenu-element-description {
    font-size: 12px;
    color: #242833;
}

.bs-fullPageMenu-element-go {
    height: 47px;
    padding-bottom: 32px;
    font-size: 12px;
    bottom: 0;
    position: absolute;
    width: calc(100% - 48px);
    color: #3377E0;
}

.bs-fullPageMenu-element-link:hover {
    text-decoration: none;
}

.bs-fullPageMenu-element-link:hover .bs-fullPageMenu-element-title {
    color: #3377E0;
}

.helper {
    display: inline-block;
    height: 100%;
    vertical-align: middle;
}
</style>


@endsection
