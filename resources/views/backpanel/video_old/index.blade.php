@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
  
   <section class="content-header">
      <h1>
         {{ trans('admin_lang.'.$langPath).' '.trans('admin_lang.music')  }}
      </h1>
      <ol class="breadcrumb">
         <li>
            <a href="{{ routeUser('dashboard') }}"><i class="fa fa-dashboard"></i> {{ trans("admin_lang.home") }}</a>
         </li>
         <li class="active">
            <!-- <i class="fa fa-gears"></i> -->{{ trans('admin_lang.'.$langPath).' '.trans('admin_lang.music') }}
         </li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- Info boxes -->
      <div class="row">
         <div class="col-xs-12">
            <div class="box ">
               <div class="box-header">
                  <div class="row">
                       <div class="col-md-8">
                       </div>
                     
                        <div class="col-md-4">
                         {{ Form::hidden('type',$type,['class'=>'release_type']) }}
                         @if($type=='final')
                         <?php //,'InitialDelivery'=>'InitialDelivery','FullUpdate'=>'FullUpdate','MetadataOnlyUpdate'=>'MetadataOnlyUpdate','TakeDown'=>'TakeDown' ?>
                         {{Form::select('release_current_status',['All'=>'All Release','NONE'=>'New Release'],'All',['class'=>'form-control release_current_status'])}}
                          <br/>
                         @else
                         {{Form::select('release_current_status',['All'=>'All Release'],'All',['style'=>'display:none','class'=>'form-control release_current_status'])}}                         
                         @endif
                         @if(Auth::user()->role_id==1)       
                     {{--Form::select('user_id',$user,isset($_GET['user_id'])?$_GET['user_id']:null,['class'=>'form-control user_id','style'=>'text-transform: capitalize;','placeholder'=>'Select Username'])--}}                                           
                     <br>
                     @endif
                        
                     <!--div class="row">
                         
                              <div class="col-md-4">
                                  <div class="form-group ">
                     <label class="control-label" >{{ trans("admin_lang.from_date") }}</label>
                     {{ Form::text('start_date',date('Y-m-d',strtotime('-2 month',strtotime(date('Y-m-d')))),['class'=>'sdate form-control req_date_time','autocomplete'=>'off','placeholder'=>trans("admin_lang.from_date"),'readonly'=>'readonly'])}}
                   </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group">
                     <label class="control-label" >{{ trans("admin_lang.to_date") }}</label>
                     {{ Form::text('end_date',date('Y-m-d'),['class'=>'edate form-control req_date_time','autocomplete'=>'off','placeholder'=>trans("admin_lang.to_date"),'readonly'=>'readonly'])}}
                   </div>
                                  
                              </div>
                              <div class="col-md-4">
                                <div class="form-group ">
                                    <label class="control-label " >&nbsp;</label><br/>
                                    {{ Form::button('Filter',['class'=>'filter btn btn-primary'] ) }}
                                    
                                  </div>                                  
                              </div>

                              
                          </div>
                  </div-->
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                  
                  <table id="userTable" class="table table-bordered table-hover datatable">
                     <thead>
                        <tr>
                           <th class="action" width="50px"></th>                 
                           <th class="action" width="50px"></th>                 
                           <th>{{ trans('admin_lang.album') }}</th>
                           <th>{{ trans('admin_lang.title') }}</th>
                           <th>{{ trans('admin_lang.type') }}</th>
                           <th>{{ trans('admin_lang.label') }}</th>
                           <th class="action">{{ trans('admin_lang.producer_catalogue_number') }}</th>
                           <th class="action">{{ trans('admin_lang.store')}}</th>
                           <th class="action" width="110px">{{ trans("admin_lang.action") }}
                           </th>
                        </tr>
                     </thead>
                     <tfoot>
                        <tr>
                           <th></th>
                           <th></th>
                           <th>{{ trans('admin_lang.album') }}</th>
                           <th>{{ trans('admin_lang.title')}}</th>
                           <th>{{ trans('admin_lang.type')}}</th>
                           <th>{{ trans('admin_lang.label')}}</th>
                           <th>{{ trans('admin_lang.producer_catalogue_number')}}</th>
                           <th>{{ trans('admin_lang.store')}}</th>
                           <th>{{ trans("admin_lang.action") }}</th>
                        </tr>
                     </tfoot>
                  </table>
               </div>
              
               <!-- /.box-body -->
            </div>
            <!-- /.box -->
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
          
<div class="modal" id="myModalRelease" >
  <div class="modal-dialog" style="width:80%">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
          <div class="row">
              <div class="col-md-6"><h4 class="modal-title">Release Notification</h4></div>
              <div class="col-md-6"><button type="button" class="close" data-dismiss="modal">&times;</button></div>
          </div>
        
        
      </div>
      <!-- Modal body -->
      <div class="modal-body-content-notification ">
      
 
    </div>
    </div>
  </div>
</div>
      
@endsection
@section('script')
<script type="text/javascript">
   $(document).ready(function () {
       
    $(".sdate").datepicker({
        numberOfMonths: 1,
        dateFormat: 'yy-mm-dd',
        maxDate: 0,
        setDate: new Date(),
        onSelect: function(selected) {
          $(".edate").datepicker("option","minDate", selected)
        }
    });
    $(".edate").datepicker({ 
        numberOfMonths: 1,
        dateFormat: 'yy-mm-dd',
        maxDate: 0,
        setDate: new Date(),
        onSelect: function(selected) {
           $(".sdate").datepicker("option","maxDate", selected)
        }
    }); 

   
    var table = $('#userTable').DataTable({
           "bProcessing": true,
           "serverSide": true,
           "pageLength": 25,
           "order": [[2, "desc"]],
           "ajax": {
               url: "{{ routeUser($pagePath.'.index') }}",
               data: function (d) {
                    return $.extend({}, d, {
                        "type": $('.release_type').val(),
                        "user_id":  $('.user_id option:selected').val(),
                        "start_date": $('.sdate').val(),
                        "end_date": $('.edate').val(),
                        "current_status": $('.release_current_status option:selected').val(),
                    });
                },
               error: function () {
                   alert("{{trans('admin_lang.something_went_wrong')}}");
               }
           },
          
           "aoColumns": [
               {mData: 'DT_RowId'},
               {mData: 'DT_RowId1'},
               {mData: 'album'},
               {mData: 'title'},
               {mData: 'track_type'},
               {mData: 'label'},
               {mData: 'producer_catalogue_number'},
               {mData: 'terrs'},
               {mData: 'actions'}
           ],
           "aoColumnDefs": [
               {"bSortable": false, "aTargets": ['action']}
           ],
            language: {
            searchPlaceholder: "Search by title, type, post by"
        },
       });
       
        $('.release_current_status').change(function (e) {
            $(".loding_img").show();
            table.ajax.reload();
            $(".loding_img").hide();
        });
        $('.report').click(function(){
            var category_id = $('.category_id option:selected').val();
            var type_id =  $('.type_id option:selected').val();
            var start_date =  $('.sdate').val();
            var end_date =  $('.edate').val();
            window.location.href = window.location.href+'/report?category_id='+category_id+'&type_id='+type_id+'&start_date'+start_date+'&end_date='+end_date ;
        });
   });

</script>
<script>
   $(document).ready(function(){
     $('.university').change(function(){
        var id = $(this).val();
        updateQueryStringParam('university_id',id);
        location.reload();
     });
     
     $('.user_id').change(function(){
        var user_id = $(this).val();
        updateQueryStringParam('user_id',user_id);
        location.reload();        
     });
     
   });
   
   function updateQueryStringParam(key, value) {
         
         var baseUrl = [location.protocol, '//', location.host, location.pathname].join(''),
           urlQueryString = document.location.search,
           newParam = key + '=' + value,
           params = '?' + newParam;
         
         // If the "search" string exists, then build params from it
         if (urlQueryString) {
         
           updateRegex = new RegExp('([\?&])' + key + '[^&]*');
           removeRegex = new RegExp('([\?&])' + key + '=[^&;]+[&;]?');
         
           if( typeof value == 'undefined' || value == null || value == '' ) { // Remove param if value is empty
         
               params = urlQueryString.replace(removeRegex, "$1");
               params = params.replace( /[&;]$/, "" );
         
           } else if (urlQueryString.match(updateRegex) !== null) { // If param exists already, update it
         
               params = urlQueryString.replace(updateRegex, "$1" + newParam);
         
           } else { // Otherwise, add it to end of query string
         
               params = urlQueryString + '&' + newParam;
         
           }
         
         }
         window.history.replaceState({}, "", baseUrl + params);
         };

   @if(Auth::user()->role_id==1)
   $(document).on('click','.releaseupload,.checkrelease',function(){
      var release_id = $(this).data('release_data');
      var store_type = $(this).data('store_type');
      $(".loding_img").show();
       $.ajax({
            url: "{{ routeUser('ajax.video.store.view') }}",
            type: 'get',
            data:{release_id:release_id,store_type:store_type},
            success: function (data) 
            {
                $(".loding_img").hide();
                $('#myModalRelease').modal('show');
                $('.modal-body-content-notification').html(data);
            },               
        }); 

   });
   @endif
</script>
@endsection

