@extends('admin.layouts.layout')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header1">
            @include('admin.partials.errors')
   </section>
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         {{ trans('admin_lang.'.$langPath) }}
         <small>{{ trans('admin_lang.add').' '.trans('admin_lang.'.$langPath) }}</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="{!! url('admin/dashboard') !!}"><i class="fa fa-dashboard"></i> {{ trans('admin_lang.home') }}</a></li>
         <li class="active"><a href="{!! routeUser($pagePath.'.index') !!}">{{ trans('admin_lang.'.$langPath) }}</a></li>
         <li class="active"> {{ trans('admin_lang.add').' '.trans('admin_lang.'.$langPath) }}</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- Info boxes -->
      <div class="row">
         <div class="col-xs-12">
            <div class="box">
               <div class="box-header">
                  <!-- <h3 class="box-title">Hover Data Table</h3> -->
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                                          {{Form::open(array('route' => [routeFormUser('file.import')],'files'=>true,'class'=>'upload_submit'))}}
                        <h2>Create multiple releases</h2>
                       <h4>Select our Excel metadata template and complete it with your releases' data :</h4>
                       <p><span style="">- Download the Album Xls Template <a href="{{ asset('public/METADATA-TEMPLATE.xls') }}" target="_blank" style="text-decoration:underline;color:red">here</a></span></p>
                    <p><span style="color:red">-</span> <a class="highSlide" href="#viewChannels" onclick="return hs.htmlExpand(this, {contentId: 'viewChannels', objectType:'ajax', preserveContent: false});"><span style="color:red">See your video Channels resume </span></a>			
                                <br><span style="color:red">
				- Please follow the guidelines included in the file.<br>
				- Do not try to alter the template in any way.<br>
				    (i.e.: by adding or removing columns or changing columns titles)</span><br><br>
                                    <strong>Your file is completed? Upload it here:&nbsp;<br><span style="color:red">Please Don't change your file formate and don't add and remove exta column in xlsx file</p>  </strong>
                                
                                <div class="informationBox" >
				
				                                
				
        <div class="form-group imgs_video text-center">
            <div class="vimgupload">
                <span class="btn btn-primary btn-file" >
                    <i class="fa fa-cloud-upload"></i> 
                        <div>{!! trans("admin_lang.upload_file") !!}</div>
                        <input type="file" name="upload_file" class="upload_file" id="upload_file"/>
                        <span id="btn-file-error"></span>
                </span>
            </div>
                  </div>
				</p>
				<div style="text-align:center;margin-top:10px">
				 <input type="button" class="btn btn-primary pull-center form_submit" value="{{ trans('admin_lang.import') }}">
				</div>
			</div>
                             {{ Form::close() }}      
                    </div>
                    <div class="col-md-3"></div>
                </div>
                   
                   
                   
               </div>
               <!-- /.box-body -->
            </div>
            <!-- /.box -->
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
$(document).ready(function(){

        $('#upload_file').change(function(){
            var fi = document.getElementById('upload_file');
            var type = 'audio';
            FileDetails_audio(fi,'btn-file-error',type);
        });
        
        function FileDetails_audio(fi,cla,type) {
            
             if (fi.files.length > 0 && fi.files.length < 20) {
                 document.getElementById(cla).innerHTML =
                        'Total Files: <b>' + fi.files.length + '</b>';
                 for (var i = 0; i <= fi.files.length - 1; i++) {
                    var exten =   fi.files.item(i).name.split('.').pop();
                    if(exten!='xls'){
                       $('#'+cla).html('');
                       $(fi.files).val('');
                       $('#'+cla).text("Only formats are allowed :xls");
                       return false;
                    }
                     
                     var fname = fi.files.item(i).name;
                     var fsize = fi.files.item(i).size;
                     if(fsize<'20971520')
                     {
                        document.getElementById(cla).innerHTML =
                        document.getElementById(cla).innerHTML + '<br /> ' +
                        fname + ' (<b>' + fsize + '</b> bytes)';
                    }
                    else
                    {                       
                       $('#'+cla).html('');
                       $(fi.files).val('');
                       $('#'+cla).text("Only flie size 10 MB  allowed");
                       return false;
                    }
                 }
             }
             else{
                       $('#'+cla).html('');
                       $(fi.files).val('');
                       $('#'+cla).text(" You can only upload a maximum of 20 files.");
                       return false;
             }
             
        }

        });
            </script>
@endsection

