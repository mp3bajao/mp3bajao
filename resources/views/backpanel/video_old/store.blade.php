<div class="row">
    <div class="col-sm-12 " style="font-size: 20px; text-align: right; display: none;">
        <span><input class="selectallstore" name="selectallstore" type="checkbox"></span>
        <span>Select All</span>
    </div>
        @forelse($store as $store)
            <div class="col-sm-2">
                <div class="boxsetup" style="    border: 1px solid #ccc;    margin: 5px;height: 180px;padding: 15px;text-align: center;box-shadow: 0px 0px 0px 5px aliceblue;">
                <img src="{{asset($store->getStore->image)}}" style="width:100%;"><br/>
                <span>{{ Form::checkbox('store',$store->id,($store->status==0)?false:true,['class'=>'setstore','data-release_id'=>$store->release_id])}}</span>
                <span>{{$store->getStore->title}}</span>
            </div>
            </div>
        @empty
        @endforelse
    
</div>


<script>
$(document).on('click','.setstore',function(){
   var status = 0;
   if($(this).is(':checked')){
     status = 1;
   }else{
     status = 0;
   }
   var id =  $(this).val();
   var release_id = $(this).data('release_id');
   $(".loding_img").show();
      $.ajax({
          url: "{{ routeUser('ajax.video.store.set') }}",
          type: 'get',
          data:{status:status,id:id,release_id:release_id},
          success: function (data)
          {
            $(".loding_img").hide();
            validateSelectAll();
          }
      });
});

$(document).on('click','.selectallstore',function(){
   var status = 0;
   if($(this).is(':checked')){
     status = 1;
     $(".setstore").prop('checked', true);
   }else{
     status = 0;
     $(".setstore").prop('checked', false);
   }
   var release_id = '{{ app('request')->input('id') }}';   
   $(".loding_img").show();
      $.ajax({
          url: "{{ routeUser('ajax.video.store.selectall') }}",
          type: 'get',
          data:{status:status,release_id:release_id},
          success: function (data)
          {
            $(".loding_img").hide();
          }
      });
});

$(document).ready(function(){
  validateSelectAll();
});

function validateSelectAll(){
  var totcheckbox = $('.setstore').length;
  var selectcheckbox = $('.setstore:checked').length;
  if(totcheckbox==selectcheckbox){
    $(".selectallstore").prop('checked', true);
  }
  else{
    $(".selectallstore").prop('checked', false);
  }
}
</script>
