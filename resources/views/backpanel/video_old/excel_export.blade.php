  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<?php use App\Models\Media; ?>
<table  class="table table-bordered">
    <thead>
        <tr>
            <th>S.N0.</th>
            <th>RELEASE ID</th>
            <th>USER NAME</th>
            <th>TRACK TYPE</th>
            <th>SECONDARY TRACK  TYPE</th>
            <th>PRODUCT TITLE</th>
            <th>PRODUCT SUB TITLE</th>
            <th>CATALOGUE NO.</th>
            <th>LABEL</th>
            <th>MAIN GENRE</th>
            <th>ALTERNATE GENRE</th>
            <th>FORMATE</th>
            <th>ORIGINAL RELEASE DATE</th>
            <th>PLINE</th>
            <th>CLINE</th>
            <th>PRODUCTION YEAR</th>
            <th>UPC</th>
            <th>RELEASE DATE</th>
            <th>RELEASE ART</th>
            <th>RELEASE CURRENT STATUS</th>
            <th>APPROVE DATE</th>
            <th>TRACK ID</th>
            <th>TRACK NO</th>
            <th>INSTRUMENTAL</th>
            <th>TRACK TITLE</th>
            <th>TRACK SUB TITLE</th>
            <th>PRIMARY ARTIST</th>
            <th>FEATURING ARTIST</th>
            <th>REMIXER</th>
            <th>AUTHOR</th>
            <th>COMPOSER</th>
            <th>ARRANGER</th>
            <th>ISRC</th>
            <th>LANGUAGE</th>
            <th>PRODUCER</th>
            <th>COMPOSER</th>
            <th>PUBLISHER</th>
            <th>PARENTAL ADVISORY</th>
            <th>PREVIEW START</th>
            <th>TITLE LANGUAGE</th>
            <th>LYRICS LANGUAGE</th>
            <th>Media Title</th>
            <th>Media Art</th>
        </tr>
    </thead>
    <tbody>
        <?php $i=0; ?>
        @if(isset($release[0]))
        @foreach($release as $release_data)
        @foreach($release_data->getReleaseTrack as $key => $track)
        <?php  $media = Media::where('media_id',$track->track_id)->first(); ?>
        <tr>
            <td>{{++$i}}.</td>
            <td>{{$release_data->id}}</td>
            <td>{{$release_data->getUser->name}}</td>
            <td>{{$track->track_type}}</td>
            <td>{{$track->secondary_track_type}}</td>
            <td>{{$release_data->title}}</td>
            <td>{{$release_data->subtitle}}</td>
            <td>{{$release_data->producer_catalogue_number}}</td>
            <td>{{$release_data->label_value}}</td>
            <td>{{$release_data->genre_value}}</td>
            <td>{{$release_data->subgenre_value}}</td>
            <td>{{$release_data->format}}</td>
            <td>{{$release_data->original_release_date}}</td>
            <td>{{$track->p_line}}</td>
            <td>{{$release_data->c_line}}</td>
            <td>{{$release_data->production_year}}</td>
            <td>{{$release_data->UPC}}</td>
            <td>{{date('Y-m-d',strtotime($release_data->release_date))}}</td>
            <td>{{$release_data->album_profile}}</td>
            <td>{{$release_data->release_current_status}}</td>
            <td>{{$release_data->approve_at}}</td>
            <td>{{$track->id}}</td>
            <td>{{$track->track_number}}</td>
            <td>{{$track->instrumental}}</td>
            <td>{{$track->title}}</td>
            <td>{{$track->subtitle}}</td>
            <td>{{$track->primary_artist}}</td>
            <td>{{$track->featuring_artist}}</td>
            <td>{{$track->remixer}}</td>
            <td>{{$track->author}}</td>
            <td>{{$track->composer}}</td>
            <td>{{$track->composer}}</td>
            <td>{{$track->ISRC}}</td>
            <td>{{$track->title_language}}</td>
            <td>{{$track->producer}}</td>
            <td>{{$track->composer}}</td>
            <td>{{$track->publisher}}</td>
            <td>{{$track->parental_advisory}}</td>
            <td>{{$track->preview_start}}</td>
            <td>{{$track->title_language}}</td>
            <td>{{$track->lyrics_language}}</td>
            <td>{{isset($media->orignal_name)?$media->orignal_name:''}}</td>
            <td>{{isset($media->file)?$media->file:''}}</td>
        </tr>
        @endforeach
        @endforeach
        @endif
    </tbody>
</table>