<?php
use App\Models\Track;
?>
<div class="row colspace_col">
    <div class="col-sm-12">
        <div class="row">
            <div class="vimg1" style="overflow: scroll;width: 1px;height: 1px;"></div>
            <div class="col-md-2">
                <div id="release-col-cover" class="">
                    <div class="col-sm-9">
                        <div id="releaseCover" class="">
                          <div class="vimg">
                            <a style="display: block;" href="#"  data-toggle="modal" data-target="#myModal">
                                <img id="v_icon_img" src="{!! $sdata->album_profile !!}" class="img-responsive img-thumbnail" id="coverOfThisAlbum">
                            <br></a>
                        </div>
                            <a style="display: block;" href="#"  data-toggle="modal" data-target="#myModal">
                                <center>Upload Album Artwork</center>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
          <div class="row">
              <div class="col-md-8"> <h4 class="modal-title">Update the cover</h4></div>
              <div class="col-md-4"> <button type="button" class="close pull-right" data-dismiss="modal">&times;</button></div>
          </div>
       
       
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        
              <h5>  Your cover must be:
                  <small>
    Size: 1920*1080 pixels,
- Format:jpg</small></b></h5>
             
                <div class="form-group imgs_video text-center">
                    <div class="vimgupload">
                        <span class="btn btn-primary btn-file" >
                            <i class="fa fa-cloud-upload"></i> 
                            <div>{!! trans("admin_lang.upload_file") !!}
                            </div>
                                <input type="file" name="album_profile" id="album_profile"  class="file_upload" />
                                <span id="btn-file-error"></span>
                        </span>
                    </div>
                </div>          
     
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
          <button type="button" class="btn btn-primary upload_image">Upload Image</button>
      </div>

    </div>
  </div>
</div>
            </div>
        	<div class="col-md-5">
        		<div class="form-group @if ($errors->has('title')) has-error @endif">
                           
                   <label for="title" class="control-label required">{!! trans('admin_lang.vtitle').'<br/> (as it appears on youtube)' !!}</label>
                   {{ Form::text('title',null,['class'=>'form-control title','data-type'=>'title','id'=>'title','placeholder'=>trans("admin_lang.rtitle")])}}
                 @if ($errors->has('title'))<span class="error">{{ $errors->first('title') }}</span>@endif
                </div>

                <div class="form-group @if ($errors->has('subtitle')) has-error @endif">
                   <label for="subtitle" class="control-label">{{ trans('admin_lang.version') }}</label>
                    {{ Form::text('subtitle',null,['class'=>'form-control subtitle','data-type'=>'subtitle','placeholder'=>trans("admin_lang.version")])}}
                 @if ($errors->has('subtitle'))<span class="error">{{ $errors->first('subtitle') }}</span>@endif
                </div>

                <?php $primary_artist =  isset($sdata->primary_artist)?explode('|',$sdata->primary_artist):array();?>
                <div class="form-group @if ($errors->has('primary_artist.0')) has-error @endif">
                   <label for="primary_artist" class="control-label required">{{ trans('admin_lang.primary') }}</label>
                   <div class="row">
                       <div class="col-md-10">{{ Form::text('primary_artist[]',isset($primary_artist[0])?$primary_artist[0]:null,['class'=>'form-control primary_artist','data-type'=>'primary_artist','placeholder'=>trans("admin_lang.primary")])}}</div>
                       <div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-success add_primary_artist"><i class="fa fa-plus-circle"></i></a></div>
                   </div>
                 @if ($errors->has('primary_artist.0'))<span class="error">{{ $errors->first('primary_artist.0') }}</span>@endif
                 <span class="error primary_artist_error" ></span>
                </div>
                    @if(count($primary_artist)>1)
                   @for($i=1;$i<count($primary_artist);$i++)
                        <div class="form-group">
                           <label for="primary_artist " class="control-label "></label>
                       <div class="row">
                       <div class="col-md-10">
                       {{ Form::text('primary_artist[]',isset($primary_artist[$i])?$primary_artist[$i]:null,['class'=>'form-control primary_artist','data-type'=>'primary_artist','placeholder'=>trans("admin_lang.primary")])}}
                       <br/>
                       </div>
                       </div>
                       </div>
                   @endfor
                   @endif
                    <div class="primary_artist_append"></div>
 
                  <?php $featuring_artist =  isset($sdata->featuring_artist)?explode('|',$sdata->featuring_artist):array();?>
                <div class="form-group @if ($errors->has('featuring_artist')) has-error @endif">
                   <label for="featuring" class="control-label ">{{ trans('admin_lang.featuring') }}</label>
                   <div class="row">
                        <div class="col-md-10">{{ Form::text('featuring_artist[]',isset($featuring_artist[0])?$featuring_artist[0]:null,['class'=>'form-control featuring_artist featuring_artist_0','data-type'=>'featuring_artist','placeholder'=>trans("admin_lang.featuring")])}}</div>
                       <div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-success add_featuring_artist"><i class="fa fa-plus-circle"></i></a></div>
                   </div>

                 @if ($errors->has('featuring_artist'))<span class="error">{{ $errors->first('featuring_artist') }}</span>@endif
                 <span class="error featuring_artist_error" ></span>
                </div>
                @if(count($featuring_artist)>1)
                   @for($i=1;$i<count($featuring_artist);$i++)
                       <div class="form-group">
                           <label for="primary_artist " class="control-label "></label>
                       <div class="row">
                       <div class="col-md-10">
                        {{ Form::text('featuring_artist[]',isset($featuring_artist[$i])?$featuring_artist[$i]:null,['class'=>'form-control featuring_artist featuring_artist_0','data-type'=>'featuring_artist','placeholder'=>trans("admin_lang.featuring")])}}
                       <br/>
                       </div>
                       </div>
                       </div>
                   @endfor
                @endif                
                <div class="featuring_artist_append"></div> 
                       
                <?php $director =  isset($sdata->director)?explode('|',$sdata->director):array();?>
                <div class="form-group @if ($errors->has('director')) has-error @endif">
                   <label for="featuring" class="control-label ">{{ trans('admin_lang.director') }}</label>
                   <div class="row">
                        <div class="col-md-10">{{ Form::text('director[]',isset($director[0])?$director[0]:null,['class'=>'form-control director director_0','data-type'=>'director','placeholder'=>trans("admin_lang.director")])}}</div>
                       <div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-success add_director"><i class="fa fa-plus-circle"></i></a></div>
                   </div>

                 @if ($errors->has('director'))<span class="error">{{ $errors->first('director') }}</span>@endif
                 <span class="error director_error" ></span>
                </div>
                @if(count($director)>1)
                   @for($i=1;$i<count($director);$i++)
                       <div class="form-group">
                           <label for="director " class="control-label "></label>
                       <div class="row">
                       <div class="col-md-10">
                        {{ Form::text('director[]',isset($director[$i])?$director[$i]:null,['class'=>'form-control director director_0','data-type'=>'director','placeholder'=>trans("admin_lang.director")])}}
                       <br/>
                       </div>
                       </div>
                       </div>
                   @endfor
                @endif
                <div class="director_append"></div> 
                
                       
                <?php $film_director =  isset($sdata->film_director)?explode('|',$sdata->film_director):array();?>
                <div class="form-group @if ($errors->has('film_director')) has-error @endif">
                   <label for="featuring" class="control-label ">Composer/Music Director</label>
                   <div class="row">
                        <div class="col-md-10">{{ Form::text('film_director[]',isset($film_director[0])?$film_director[0]:null,['class'=>'form-control film_director film_director_0','data-type'=>'film_director','placeholder'=>'Composer/Music Director'])}}</div>
                       <div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-success add_film_director"><i class="fa fa-plus-circle"></i></a></div>
                   </div>

                 @if ($errors->has('film_director'))<span class="error">{{ $errors->first('film_director') }}</span>@endif
                 <span class="error film_director_error" ></span>
                </div>
                @if(count($film_director)>1)
                   @for($i=1;$i<count($film_director);$i++)
                       <div class="form-group">
                           <label for="director " class="control-label "></label>
                       <div class="row">
                       <div class="col-md-10">
                        {{ Form::text('film_director[]',isset($film_director[$i])?$film_director[$i]:null,['class'=>'form-control film_director film_director_0','data-type'=>'film_director','placeholder'=>trans("admin_lang.film_director")])}}
                       <br/>
                       </div>
                       </div>
                       </div>
                   @endfor
                @endif
                <div class="film_director_append"></div> 
                
                <?php $actor =  isset($sdata->actor)?explode('|',$sdata->actor):array();?>
                <!-- <div class="form-group @if ($errors->has('actor')) has-error @endif">
                   <label for="featuring" class="control-label ">{{ trans('admin_lang.actor') }}</label>
                   <div class="row">
                        <div class="col-md-10">{{ Form::text('actor[]',isset($actor[0])?$actor[0]:null,['class'=>'form-control actor actor_0','data-type'=>'actor','placeholder'=>trans("admin_lang.actor")])}}</div>
                       <div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-success add_actor"><i class="fa fa-plus-circle"></i></a></div>
                   </div>

                 @if ($errors->has('actor'))<span class="error">{{ $errors->first('actor') }}</span>@endif
                 <span class="error actor_error" ></span>
                </div> -->
                @if(0 && count($actor)>1)
                   @for($i=1;$i<count($actor);$i++)
                       <div class="form-group">
                           <label for="actor" class="control-label "></label>
                       <div class="row">
                       <div class="col-md-10">
                        {{ Form::text('actor[]',isset($actor[$i])?$actor[$i]:null,['class'=>'form-control actor actor_0','data-type'=>'actor','placeholder'=>trans("admin_lang.actor")])}}
                       <br/>
                       </div>
                       </div>
                       </div>
                   @endfor
                @endif
                <!-- <div class="actor_append"></div> -->
                                              
                <?php $editor =  isset($sdata->editor)?explode('|',$sdata->editor):array();?>
                <div class="form-group @if ($errors->has('editor')) has-error @endif">
                   <label for="featuring" class="control-label ">{{ trans('admin_lang.author') }}</label>
                   <div class="row">
                        <div class="col-md-10">{{ Form::text('editor[]',isset($editor[0])?$editor[0]:null,['class'=>'form-control editor editor_0','data-type'=>'editor','placeholder'=>trans("admin_lang.editor")])}}</div>
                       <div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-success add_editor"><i class="fa fa-plus-circle"></i></a></div>
                   </div>

                 @if ($errors->has('editor'))<span class="error">{{ $errors->first('editor') }}</span>@endif
                 <span class="error editor_error" ></span>
                </div>
                @if(count($editor)>1)
                   @for($i=1;$i<count($editor);$i++)
                       <div class="form-group">
                           <label for="editor" class="control-label "></label>
                       <div class="row">
                       <div class="col-md-10">
                        {{ Form::text('editor[]',isset($editor[$i])?$editor[$i]:null,['class'=>'form-control editor editor_0','data-type'=>'editor','placeholder'=>trans("admin_lang.editor")])}}
                       <br/>
                       </div>
                       </div>
                       </div>
                   @endfor
                @endif
                <div class="editor_append"></div> 
                       
                       
                <?php $producer =  isset($sdata->producer)?explode('|',$sdata->producer):array();?>
                <div class="form-group @if ($errors->has('producer')) has-error @endif">
                   <label for="featuring" class="control-label ">Producer</label>
                   <div class="row">
                        <div class="col-md-10">{{ Form::text('producer[]',isset($producer[0])?$producer[0]:null,['class'=>'form-control producer producer_0','data-type'=>'producer','placeholder'=>trans("admin_lang.producers")])}}</div>
                       <div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-success add_producer"><i class="fa fa-plus-circle"></i></a></div>
                   </div>

                 @if ($errors->has('producer'))<span class="error">{{ $errors->first('producer') }}</span>@endif
                 <span class="error producer_error" ></span>
                </div>
                @if(count($producer)>1)
                   @for($i=1;$i<count($producer);$i++)
                       <div class="form-group">
                           <label for="editor" class="control-label "></label>
                       <div class="row">
                       <div class="col-md-10">
                        {{ Form::text('producer[]',isset($producer[$i])?$producer[$i]:null,['class'=>'form-control producer producer_0','data-type'=>'producer','placeholder'=>'Producer'])}}
                       <br/>
                       </div>
                       </div>
                       </div>
                   @endfor
                @endif
                <div class="producer_append"></div> 
                       
                <?php /* $publisher =  isset($sdata->publisher)?explode('|',$sdata->publisher):array();?>
                <div class="form-group @if ($error    <?php /*s->has('publisher')) has-error @endif">
                   <label for="featuring" class="control-label ">{{ trans('admin_lang.publisher') }}</label>
                   <div class="row">
                        <div class="col-md-10">{{ Form::text('publisher[]',isset($publisher[0])?$publisher[0]:null,['class'=>'form-control publisher publisher_0','data-type'=>'publisher','placeholder'=>trans("admin_lang.publisher")])}}</div>
                       <div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-success add_publisher"><i class="fa fa-plus-circle"></i></a></div>
                   </div>

                   
                @if ($errors->has('publisher'))<span class="error">{{ $errors->first('publisher') }}</span>@endif
                 <span class="error publisher_error" ></span>
                </div>
                @if(count($publisher)>1)
                   @for($i=1;$i<count($publisher);$i++)
                       <div class="form-group">
                           <label for="publisher" class="control-label "></label>
                       <div class="row">
                       <div class="col-md-10">
                        {{ Form::text('publisher[]',isset($publisher[$i])?$publisher[$i]:null,['class'=>'form-control publisher publisher_0','data-type'=>'publisher','placeholder'=>trans("admin_lang.publisher")])}}
                       <br/>
                       </div>
                       </div>
                       </div>
                   @endfor
                @endif
                <div class="publisher_append"></div> 
                    */   ?>
                       
                <?php $remixer =  isset($sdata->remixer)?explode('|',$sdata->remixer):array();?>
                <div class="form-group @if ($errors->has('remixer')) has-error @endif">
                   <label for="remixer" class="control-label ">{{ trans('admin_lang.remixer') }}</label>
                   <div class="row">
                        <div class="col-md-10">{{ Form::text('remixer[]',isset($remixer[0])?$remixer[0]:null,['class'=>'form-control remixer remixer_0','data-type'=>'remixer','placeholder'=>trans("admin_lang.remixer")])}}</div>
                       <div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-success add_remixer"><i class="fa fa-plus-circle"></i></a></div>
                   </div>

                 @if ($errors->has('remixer'))<span class="error">{{ $errors->first('remixer') }}</span>@endif
                 <span class="error remixer_error" ></span>
                </div>
                @if(count($remixer)>1)
                   @for($i=1;$i<count($remixer);$i++)
                       <div class="form-group">
                           <label for="remixer" class="control-label "></label>
                       <div class="row">
                       <div class="col-md-10">
                        {{ Form::text('remixer[]',isset($remixer[$i])?$remixer[$i]:null,['class'=>'form-control remixer remixer_0','data-type'=>'remixer','placeholder'=>trans("admin_lang.remixer")])}}
                       <br/>
                       </div>
                       </div>
                       </div>
                   @endfor
                @endif
                <div class="remixer_append"></div>
                       
                <?php $keywords =  isset($sdata->keywords)?explode('|',$sdata->keywords):array();?>
                <div class="form-group @if ($errors->has('keywords')) has-error @endif">
                   <label for="keywords" class="control-label ">{{ trans('admin_lang.keywords') }}</label>
                   <div class="row">
                        <div class="col-md-10">{{ Form::text('keywords[]',isset($keywords[0])?$keywords[0]:null,['class'=>'form-control keywords keywords_0','data-type'=>'keywords','placeholder'=>trans("admin_lang.keywords")])}}</div>
                       <div class="col-md-1"><a href="javascript:;" class="btn btn-xs btn-success add_keywords"><i class="fa fa-plus-circle"></i></a></div>
                   </div>

                 @if ($errors->has('keywords'))<span class="error">{{ $errors->first('keywords') }}</span>@endif
                 <span class="error keywords_error" ></span>
                </div>
                @if(count($keywords)>1)
                   @for($i=1;$i<count($keywords);$i++)
                       <div class="form-group">
                           <label for="keywords" class="control-label "></label>
                       <div class="row">
                       <div class="col-md-10">
                        {{ Form::text('keywords[]',isset($keywords[$i])?$keywords[$i]:null,['class'=>'form-control keywords keywords_0','data-type'=>'keywords','placeholder'=>trans("admin_lang.keywords")])}}
                       <br/>
                       </div>
                       </div>
                       </div>
                   @endfor
                @endif
                <div class="keywords_append"></div>
                       
                       
                
                       
               <div class="form-group">
                 <label for="genre_id" class="control-label ">{!! __('admin_lang.additional_information')!!}</label>
                 {{ Form::text('additional_information',null,['class'=>'form-control  additional_information','data-type'=>'additional_information','placeholder'=>trans("admin_lang.additional_information")])}}
                </div>    
                <div class="form-group  @if ($errors->has('genre_id')) has-error @endif">
                 <label for="genre_id" class="control-label required">{{ trans("admin_lang.genre") }}</label>
                    {{ Form::select('genre_id',$genre,isset($sdata->genre_id)?$sdata->genre_id:null,['class'=>'form-control genre_id','data-type'=>'genre_id','placeholder'=> ucwords(trans("admin_lang.select").' '.trans("admin_lang.genre")),'style'=>'text-transform: capitalize;']) }}
                    @if ($errors->has('genre_id'))<span class="error">{{ $errors->first('genre_id') }}</span>@endif
                </div>
                
                <div class="form-group  @if ($errors->has('subgenre_id')) has-error @endif">
                 <label for="subgenre_id" class="control-label">{{ trans("admin_lang.subgenre") }}</label>
                    {{ Form::select('subgenre_id',$subgenre,isset($sdata->subgenre_id)?$sdata->subgenre_id:null,['class'=>'form-control subgenre_id','data-type'=>'subgenre_id','placeholder'=> ucwords(trans("admin_lang.select").' '.trans("admin_lang.subgenre")),'style'=>'text-transform: capitalize;']) }}
                    @if ($errors->has('subgenre_id'))<span class="error">{{ $errors->first('subgenre_id') }}</span>@endif
                </div>
                
                    
                <div class="form-group  @if ($errors->has('label_id')) has-error @endif">
                 <label for="label_id" class="control-label required">{{ trans("admin_lang.label_name") }}</label>
                    {{ Form::select('label_id',$label,isset($sdata->label_id)?$sdata->label_id:null,['class'=>'form-control label_id','data-type'=>'label_id','placeholder'=> ucwords(trans("admin_lang.select").' '.trans("admin_lang.label")),'style'=>'text-transform: capitalize;']) }}
                    @if ($errors->has('label_id'))<span class="error">{{ $errors->first('label_id') }}</span>@endif
                </div>

                <div class="form-group new_label @if ($errors->has('new_label')) has-error @endif" style="display: none;">
                    <label for="new_label" class="control-label"> &nbsp;</label>
                   {{ Form::text('new_label',null,['class'=>'form-control','data-type'=>'new_label','placeholder'=>trans("admin_lang.new_label")])}}
                 @if ($errors->has('new_label'))<span class="error">{{ $errors->first('new_label') }}</span>@endif
                </div>

                <div class="form-group @if ($errors->has('publisher')) has-error @endif">
                   <label for="title" class="control-label"> {{ trans('admin_lang.publisher') }}</label>
                   {{ Form::text('publisher','SaregamaTone',['class'=>'form-control','readonly'=>'readonly','placeholder'=>trans("admin_lang.publisher")])}}
                 @if ($errors->has('publisher'))<span class="error">{{ $errors->first('publisher') }}</span>@endif
                </div>
                 
                {{--<div class="form-group @if ($errors->has('lyrics_language')) has-error @endif">
                   <label for="title" class="control-label required">{{ trans('admin_lang.lyrics_language') }}</label>
                 {{ Form::select('lyrics_language',$language,null,['class'=>'form-control lyrics_language','data-type'=>'lyrics_language','placeholder'=> ucwords(trans("admin_lang.select").' '.trans("admin_lang.lyrics_language")),'style'=>'text-transform: capitalize;']) }}
                 @if ($errors->has('lyrics_language'))<span class="error">{{ $errors->first('lyrics_language') }}</span>@endif
                </div>--}}    
                       
                <div class="form-group @if ($errors->has('title_language')) has-error @endif">
                   <label for="title" class="control-label required">{{ trans('admin_lang.video_language') }}</label>
                 {{ Form::select('title_language',$language,null,['class'=>'form-control title_language','data-type'=>'title_language   ','placeholder'=> ucwords(trans("admin_lang.select").' '.trans("admin_lang.video_language")),'style'=>'text-transform: capitalize;']) }}
                   @if ($errors->has('title_language'))<span class="error">{{ $errors->first('title_language') }}</span>@endif
                </div>
                       
                <div class="form-group @if ($errors->has('description')) has-error @endif">
                   <label for="title" class="control-label required">{{ trans('admin_lang.description') }}</label>
                   {{Form::textarea('description',null,['class'=>'form-control description','data-type'=>'description','minlength'=>0,'maxlength'=>5000,'style'=>'max-width: 66% !important; height:100px; float:right;','placeholder'=>'Description'])}}
                   @if ($errors->has('description'))<span class="error">{{ $errors->first('description') }}</span>@endif
                </div>
                       
                       
                 

              <?php /*<div class="form-group  @if ($errors->has('format')) has-error @endif">
                 <label for="format" class="control-label required">{{ trans("admin_lang.format") }}</label>
                    {{ Form::select('format',['SINGLE'=>'SINGLE','ALBUM'=>'ALBUM'],isset($sdata->format)?$sdata->format:null,['class'=>'form-control format','data-type'=>'format','style'=>'text-transform: capitalize;']) }}
                    @if ($errors->has('format'))<span class="error">{{ $errors->first('format') }}</span>@endif
                </div>
                <div class="form-group @if ($errors->has('original_release_date')) has-error @endif">
                    <label for="original_release_date" class="control-label required">{{ trans("admin_lang.release_date") }}</label>
                    {{ Form::text('original_release_date',null,['class'=>'form-control original_release_date','id'=>'datetimepicker','data-type'=>'original_release_date','autocomplete'=>'off','placeholder'=>trans("admin_lang.release_date"),'data-type'=>'original_release_date','readonly'=>'readonly'])}}
                @if ($errors->has('original_release_date'))<span class="error">{{ $errors->first('original_release_date') }}</span>@endif
                </div>*/?>
        	</div>
        

                       
                       
                       
        	<div class="col-md-5">
        		<div class="form-group @if ($errors->has('p_line')) has-error @endif">
                   <label for="p_line" class="control-label required">  ℗ {{ trans('admin_lang.line') }}</label>
                   {{ Form::text('p_line',null,['class'=>'form-control p_line','data-type'=>'p_line','id'=>'p_line','placeholder'=>trans("admin_lang.line")])}}
                 @if ($errors->has('p_line'))<span class="error">{{ $errors->first('p_line') }}</span>@endif
                </div>

                <div class="form-group @if ($errors->has('c_line')) has-error @endif">
                   <label for="title" class="control-label required">&copy; {{ trans('admin_lang.line') }}</label>
                   {{ Form::text('c_line',null,['class'=>'form-control c_line','data-type'=>'c_line','id'=>'c_line','placeholder'=>trans("admin_lang.line")])}}
                 @if ($errors->has('c_line'))<span class="error">{{ $errors->first('c_line') }}</span>@endif
                </div>
                <div class="form-group @if ($errors->has('production_year')) has-error @endif">
                   <label for="production_year" class="control-label required">{{ trans('admin_lang.production_year') }}</label>
                   <select name="production_year" class="form-control production_year" placeholder="Select Year" data-type='production_year'>
                       <option>Select Year</option>
                       @for($i=date('Y')+1;$i>1990; $i--)
                       <option value="{{$i}}" @if(isset($sdata->production_year) && $sdata->production_year==$i) selected @endif>{{$i}}</option>
                       @endfor                       
                   </select>
                 @if ($errors->has('production_year'))<span class="error">{{ $errors->first('production_year') }}</span>@endif
                </div>
                    
                <div class="form-group @if ($errors->has('ISRC')) has-error @endif">
                   <label for="title" class="control-label required"> {{ trans('admin_lang.ISRC') }}</label>
                   {{ Form::text('ISRC',null,['class'=>'form-control','placeholder'=>trans("admin_lang.ISRC"),'readonly'=>'readonly'])}}
                 @if ($errors->has('ISRC'))<span class="error">{{ $errors->first('ISRC') }}</span>@endif
                </div>
                    
                <div class="form-group @if ($errors->has('audio_isrc')) has-error @endif">
                   <label for="title" class="control-label required"> {{ trans('admin_lang.audio_isrc') }}</label>
                   {{ Form::text('audio_isrc',null,['class'=>'form-control','placeholder'=>trans("admin_lang.audio_isrc"),'readonly'=>'readonly'])}}
                 @if ($errors->has('audio_isrc'))<span class="error">{{ $errors->first('audio_isrc') }}</span>@endif
                </div>
                    
                    
                <div class="form-group @if ($errors->has('UPC')) has-error @endif">
                   <label for="title" class="control-label required">{{ trans('admin_lang.upc') }}</label>
                   {{ Form::text('UPC',isset($sdata->UPC)?$sdata->UPC:null,['class'=>'form-control UPC','id'=>'upc','data-type'=>'UPC','placeholder'=>trans("admin_lang.upc"),'readonly'=>'readonly'])}}
                 @if ($errors->has('UPC'))<span class="error">{{ $errors->first('UPC') }}</span>@endif
                </div>

                <div class="form-group @if ($errors->has('title')) has-error @endif">
                   <label for="title" class="control-label required">{{ trans('admin_lang.producer') }}</label>
                   @if(isset($sdata->producer_catalogue_number) && !empty($sdata->producer_catalogue_number))
                   {{ Form::text('producer_catalogue_number',null,['class'=>'form-control producer_catalogue_number','data-type'=>'producer_catalogue_number','id'=>'producer','placeholder'=>trans("admin_lang.producer"),'readonly'=>'readonly'])}}
                 @else
                  {{ Form::text('producer_catalogue_number',null,['class'=>'form-control producer_catalogue_number','data-type'=>'producer_catalogue_number','id'=>'producer','placeholder'=>trans("admin_lang.producer")])}}                 
                 @endif
                   @if ($errors->has('producer_catalogue_number'))<span class="error">{{ $errors->first('producer_catalogue_number') }}</span>@endif
                </div>
                    
        
                    
                    
                  </div>
        	</div>
        </div>
   </div>

<script type="text/javascript">
  $(document).ready(function(){
    $('.label_id').change(function() {
      var id = $(this).val();
      if(id=="new")
      {
        $(".new_label").show();
      }
      else
      {
        $(".new_label").hide();;
      }
    });    

  });

</script>

<script>
    $(document).ready(function(){
        
        
        function addmore(key,value){
           var status = 'false'; 
            $('.'+key).each(function(){
               var primary_artist =  $(this).val();
               if(primary_artist==''){
                   status = 'true';
                   return;
               }
           });
           if(status=='false'){
                $('.'+key+'_append').append('<div class="form-group"><label class="control-label">&nbsp;</label><div class="row"><div class="col-md-10"><input class="form-control '+key+'" placeholder="'+value+'" name="'+key+'[]" data-type="'+key+'" type="text"></div></div></div>')
                $('.'+key+'_error').html('');             
                i++;  
            }
            else{
                $('.'+key+'_error').html('<label class="control-label">&nbsp;</label>You must enter a name on the current field before adding a new one');                      
            } 
        }
        
        $('.add_primary_artist').click(function(){
            addmore('primary_artist','Primary Artist');
        });
        
        $('.add_featuring_artist').click(function(){
            addmore('featuring_artist','Featuring Artist');
        });
        
        $('.add_director').click(function(){
            addmore('director','Director');
        });
        
        $('.add_film_director').click(function(){
            addmore('film_director','Film Director');
        });
        
        $('.add_actor').click(function(){
            addmore('actor','Actor');
        });
        
        $('.add_editor').click(function(){
            addmore('editor','Editor');
        });
        
        $('.add_producer').click(function(){
            addmore('producer','Producer');
        });
        
        $('.add_publisher').click(function(){
            addmore('publisher','Publisher');
        });
        
        $('.add_remixer').click(function(){
            addmore('remixer','Remixer');
        });
        
        $('.add_keywords').click(function(){
            addmore('keywords','Keywords');
        });
        
        
        
        
        $('.title, .subtitle, .p_line, .c_line, .original_release_date, .producer_catalogue_number, .additional_information, .description').keyup(function(){
          var val =  $(this).val();
          var type = $(this).data('type');
         ajaxFormSubmit(val,type);
        });
        
            $(document).on('change','.genre_id, .subgenre_id, .label_id, .format, .production_year, .original_release_date, .lyrics_language, .title_language ',function(){
          var val =  $(this).val();
          var type = $(this).data('type');    
            ajaxFormSubmit(val,type);              
        });
        
        
        
        
        $(document).on('keyup','.primary_artist, .featuring_artist, .director, .film_director, .actor, .editor, .producer, .publisher, .remixer, .keywords',function(){
            var val = [];
            var type = $(this).data('type');
             $('.'+type).each(function($i){
                 val[$i] = $(this).val();
             });     
              ajaxFormSubmit(val,type);  
         });
        
        function ajaxFormSubmit(val,type){
             $.ajax({
                 url: '{{ routeUser('ajax.video.formsubmit') }}',
                 type: 'post',
                 data: {
                   '_token': '{{ csrf_token() }}',
                   'val':val,
                   'type':type,
                   'release_id':{{$sdata->id}}
                 },
                 success: function (data) 
                 {
                 },
             });
        }
        
        
        
        $('#datetimepicker').datetimepicker({
            timepicker:false,
            format:'Y-m-d',
        onChangeDateTime:exampleFunction
        });
        function exampleFunction(){
            var val =  $('.original_release_date').val();
            var type = $('.original_release_date').data('type');
            ajaxFormSubmit(val,type);
        }
        
        
        /*$('.various_artists').click(function(){
            var ck_status=  $(this).is(":checked");
            if(ck_status==true){
                $('.primary_artist').attr('readonly','readonly');
                $('.featuring_artist').attr('readonly','readonly');
                $('.primary_artist_error').html('');
                $('.featuring_artist_error').html('');
            }
            else{
                $('.primary_artist').removeAttr('readonly');
                $('.featuring_artist').removeAttr('readonly');
                $('.primary_artist_error').html('');                
                $('.featuring_artist_error').html('');                
            }
        })
        */
        var i=1;
       /* $('.add_primary_artist').click(function(){
            var status = 'false'; 
            $('.primary_artist').each(function(){
               var primary_artist =  $(this).val();
               if(primary_artist==''){
                   status = 'true';
                   return;
               }
           });
           if(status=='false'){
                $('.primary_artist_append').append('<div class="form-group pri_art'+i+'"><label class="control-label">&nbsp;</label><div class="row"><div class="col-md-10"><input class="form-control primary_artist primary_artist_'+i+'" data-type="primary_artist" placeholder="Primary Artist" name="primary_artist[]" type="text"></div></div></div>')
                $('.primary_artist_error').html('');             
                i++;  
            }
            else{
                $('.primary_artist_error').html('<br>You must enter a name on the current field before adding a new one<br>');                      
            }
        });*/
        var j=1;
        $('.add_featuring_artist').click(function(){
            var status = 'false';
            $('.featuring_artist').each(function(){
               var primary_artist =  $(this).val();
               if(primary_artist==''){
                   status = 'true';
                   return;
               }
           });
           if(status=='false'){
                $('.featuring_artist_append').append('<div class="form-group pri_art'+j+'"><label class="control-label">&nbsp;</label><div class="row"><div class="col-md-10"><input class="form-control featuring_artist featuring_artist_'+j+'" data-type="featuring_artist" placeholder="Featuring" name="featuring_artist[]" type="text"><br/></div></div>')
                $('.featuring_artist_error').html('');             
                j++;  
            }
            else{
                $('.featuring_artist_error').html('<br>You must enter a name on the current field before adding a new one');                      
            }
        });
        
        
       $('.genre_id').change(function(){
       var genre_id = $(this).val();
       $(".loding_img").show();
            $.ajax({
                 url: '{{ routeUser('ajax.subgenre') }}',
                 type: 'post',
                 data: {
                   '_token': '{{ csrf_token() }}',
                   'genre_id':genre_id,
                 },
                 success: function (data) 
                 {
                     $('.subgenre_id').html(data);
                     $(".loding_img").hide();
                 },
                 error: function (data) {
                     return false;
                 }
             });
       }); 
       
       $('#album_profile').change(function(){
            var fi = document.getElementById('album_profile');         
           FileDetails_single(fi,'btn-file-error','IMAGE')
       });
       
       
       function FileDetails_single(fi,cla,type) {

             if (fi.files.length > 0 && fi.files.length < 6) {
       
                 document.getElementById(cla).innerHTML =
                        'Total Files: <b>' + fi.files.length + '</b>';
                
                 for (var i = 0; i <= fi.files.length - 1; i++) {
                     var result = readURL_single(fi.files.item(i),type);
                     if(result==false)
                     {
                        $(fi).val('');
                        return false;
                     }
                   
                     
                     var fname = fi.files.item(i).name;
                     // THE NAME OF THE FILE.
                     var fsize = fi.files.item(i).size;
                     if(fsize<'10485760')
                     {

                         document.getElementById(cla).innerHTML =
                         document.getElementById(cla).innerHTML + '<br /> ' +
                             fname + ' (<b>' + fsize + '</b> bytes)';
                    }
                    else
                    {
                       $('#'+cla).html('');
                       $(fi).val('');
                       $('#'+cla).text('Only flie size 10 MB  allowed');
                       return false;
                    }
                 }
             }
             else{
                $(fi).val('');
                $('.'+cla).text('You can only upload a maximum 1 file');
             }             
         }
         
        function readURL_single(input,type) {
            var type_reg = /^image\/(jpg|jpeg)$/;
            var selecttype =  input.type;
            
            if (type_reg.test(selecttype)) {
             console.log(input);
                var reader = new FileReader();
                    reader.onload = function (e) {                       
                        $(input).parents('.file-upload').find('abbr').show();
                        $('.vimg1').html('<img class="v_icon_img1" src="' + e.target.result + '">');
                   }
                   reader.readAsDataURL(input);
                } else {
                    $(input).val('');
                    $('#btn-file-error').text("Only formats are allowed : jpeg,jpg,png");
                    return false;
                   }  
               }
               $(document).on('click','.upload_image',function(){
               var width = $('.v_icon_img1').width();
               var height = $('.v_icon_img1').height();
               var src = $('.v_icon_img1').attr('src');
               if(width==1920 && height==1080)
               {
                 $('#v_icon_img').attr('src',src);
                 $('#myModal').modal('hide');
               }
               else{
                   $('#album_profile').val('');
                   $('#btn-file-error').text("Your cover must be: Size: 1920*1080 pixels"); 
                   return false;
               }
               });
               
               
               
               
   });
   
</script>
