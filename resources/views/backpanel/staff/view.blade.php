@if($users)
<div class="row">
 <div class="col-md-12">
    <center>
           
            <img src="{{$users->image}}" alt="user" class="img-circle"  width="100" height="100">       
    </center>
    <table class="table table-striped table-bordered table-condensed" id="table" style="width: 100%;">
            <tr>
                <td style="width: 150px;"><strong>Name:</strong></td>
                <td>{{ucwords($users->name)}}</td>
            </tr>
            <tr>
                <td><strong>Email:</strong></td>
                <td>{{$users->email}}</td>
            </tr>
            <tr>
                <td><strong>Mobile:</strong></td>
                <td>{{$users->country_code}}{{$users->mobile}}</td>
            </tr>
            <!--tr>
                <td><strong>Dob:</strong></td>
                <td>{{$users->dob}}</td>
            </tr-->
            <tr>
                <td><strong>Type:</strong></td>
                <td>
                    @if($users->type  === 1)
                        
                    @else
                        Cutomer
                    @endif
                </td>
            </tr>
            <tr>
                <td><strong>Gender:</strong></td>
                <td>
                    @if($users->gender  === 1)
                        Female
                    @else
                        Male 
                    @endif
                </td>
            </tr>
            <tr>
                <td><strong>Address:</strong></td>
                <td>{{$users->address}}</td>
            </tr>
            <tr>
                <td><strong>Status:</strong></td>
                <td>
                    @if($users->status  === 1)
                        Active
                    @else
                        In-active
                    @endif
                </td>
            </tr>
            <tr>
                <td><strong>Created At:</strong></td>
                <td>{{date('j F, Y', strtotime($users->created_at))}} </td>
            </tr>
    </table>
  </div>      
</div>
@endif