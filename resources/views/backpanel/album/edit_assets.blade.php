<div class="row">
    <div class="col-sm-12">
        <?php /*     <a class="btn btn-sm btn-primary add_track" href="#">
                    <i class="fa fa-plus"></i> {{ trans('backend.add').' '.trans('backend.track') }}
                </a>
        */ ?>

        <select name="release_id" class="release_id" style="display: none;">
            <option value="{{$sdata->id}}" selected="">{{$sdata->title}}</option>
                   </select>
         <div class="box-body">                  
                   @include('backpanel.track.table') 
               </div>
   </div>
</div>


<script>
$(document).ready(function(){
         $('.add_track').click(function(){
            $(".loding_img").show();
            var id = '{{encrypt($sdata->id)}}';
            var type = '{{$type}}';
            $.ajax({
                 url: "{{routeUser('track.create')}}",
                 type: 'get',
                 data:{'id':id,'type':type,'form_type':'store'},
                 success: function (data) 
                 {
                    $('#myModal').modal('show');
                    $(".loding_img").hide();
                    $('.modal-body-content').html(data);
                 },               
             });
        });
             
        });
</script>