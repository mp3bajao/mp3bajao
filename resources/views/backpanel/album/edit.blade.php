@extends('layouts.master')
@section('content')

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">{{ __('backend.'.$lang).' '.__('backend.manager') }}</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">{{ __('Home') }}</a></li>
                <li class="breadcrumb-item active">{{ __('backend.'.$lang).' '.__('backend.manager') }}</li>
            </ol>

        </div>
    </div>
</div>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary card-outline">
                    <div class="card-body">
                        {!! Form::model($sdata,  ['route'=>[routeFormUser($page.'.update'),encrypt($sdata->id),$type], 'method'=>'post','files'=>true,'class'=>'upload_submit']) !!}
                        @include('backpanel.'.$page.'.form')
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

