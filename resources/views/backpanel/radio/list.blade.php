@if(isset($data))
@foreach($data->getRadioList as $key => $row)
    
<tr>
    <td>{{++$key}}</td>
    <td>{{ucwords($row->table_type)}}</td>
    <td>
        @if($row->table_type=='program')
            {{ucwords($row->getProgram->name)}}
        @else
            {{ucwords($row->getAdds->name)}}
        @endif
    </td>
    <td>{{ucwords($row->duration)}}</td>
    <td></td>
</tr>
@endforeach
@endif