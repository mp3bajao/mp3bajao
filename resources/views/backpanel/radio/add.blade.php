@extends('layouts.master')
@section('content')


<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}"> 
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<!-- Content Header (Page header) -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">{{ __('backend.'.$lang).' '.__('backend.manager') }}</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">{{ __('backend.home') }}</a></li>
                <li class="breadcrumb-item active">{{ __('backend.'.$lang).' '.__('backend.manager') }}</li>
            </ol>
            <?php /*@can(ucfirst($page).'-create') */?>
              <a href="{{ routeUser($page.'.create')}}" class="btn btn-primary btn-xs d-none d-lg-block m-l-15" title="{{ __('backend.add_title_') }}"><i class="fa fa-plus"></i> {{ __('backend.add') }}</a>
            <?php /*  @endcan */?>
        </div>
    </div>
</div>

<!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
    <div class="row">
        <div class="col-md-12">
        <div class="card card-primary card-outline">
                <div class="card-body">                   
                    <form method="POST" action="{{  routeUser($page.'.create') }}" id="edit_role">
                        @csrf
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label class="control-label" for="name"> {{__('backend.station')}} *</label>
                              <select name="station_id" class="form-control station_id">
                                  <option value="">--- Select Station ---</option>
                                  @if($station)
                                    @foreach($station as $key => $value)
                                        <option value="{{$key}}">{{ucfirst($value)}}</option>
                                    @endforeach
                                  @endif
                              </select>
                             
                            </div>
                          </div>
                              <div class="col-md-6">
                            <div class="form-group">
                              <label class="control-label" for="name"> {{__('backend.week')}} *</label>
                              <select name="week" class="form-control week">
                                  <option value="Sunday">Sunday</option>
                                  <option value="Monday">Monday</option>
                                  <option value="Tusday">Tusday</option>
                                  <option value="Wednesday">Wednesday</option>
                                  <option value="Thursday">Thursday</option>
                                  <option value="Friday">Friday</option>
                                  <option value="Saturday">Saturday</option>
                              </select>                             
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="name"> {{__('backend.start_time')}} *</label>
                                <input class="flatpickr flatpickr-input start_time active form-control" name="start_time" type="text" placeholder="Select Start Time" readonly="readonly">
                            </div>                                
                          </div>
                            <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="name"> {{__('backend.end_time')}} *</label>
                                <input class="flatpickr flatpickr-input active end_time form-control" name="end_time" type="text" placeholder="Select End Time" readonly="readonly">
                            </div>                                
                          </div>
                       </div>
                        <hr style="margin: 1em -15px">
                        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-xs float-right save"><span class="spinner-grow spinner-grow-sm formloader"
                                style="display: none;" role="status" aria-hidden="true"></span> Save</button>
                    </form>


                </div>
            </div>
    </div>
</div>
</div>
    <script>
$(document).ready(function(){
    

       
    
    
$('#edit_role').parsley();
$("#edit_role").on('submit',function(e){ 

  e.preventDefault();
  var _this=$(this); 
    var formData = new FormData(this);
    formData.append('_method', 'POST');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
      url:'{{ routeUser($page.'.create')}}',
      dataType:'json',
      data:formData,
      type:'POST',
      cache:false,
      contentType: false,
      processData: false,
      beforeSend: function (){before(_this)},
      // hides the loader after completion of request, whether successfull or failor.
      complete: function (){complete(_this)},
      success:function(res){
        if(res.status === 1){ 
             $('#editModal').modal('hide') ; 
          toastr.success(res.message);
          $('#edit_role').parsley().reset();
          ajax_datatable.draw();
        }else{
          toastr.error(res.message);
        }
      },
      error:function(jqXHR,textStatus,textStatus){
        if(jqXHR.responseJSON.errors){
          $.each(jqXHR.responseJSON.errors, function( index, value ) {
            toastr.error(value)
          });
        }else{
          toastr.error(jqXHR.responseJSON.message)
        }
      }
    });
  return false;   
});


    
$("#editfile").change(function(){
    var fileObj = this.files[0];
    var imageFileType = fileObj.type;
    var imageSize = fileObj.size;
  
    var match = ["image/jpeg","image/png","image/jpg"];
    if(!((imageFileType == match[0]) || (imageFileType == match[1]) || (imageFileType == match[2]))){
      $('#editpreviewing').attr('src','images/image.png');
      toastr.error('Please Select A valid Image File <br> Note: Only jpeg, jpg and png Images Type Allowed!!');
      return false;
    }else{
      //console.log(imageSize);
      if(imageSize < 1000000){
        var reader = new FileReader();
        reader.onload = imageIsLoaded;
        reader.readAsDataURL(this.files[0]);
      }else{
        toastr.error('Images Size Too large Please Select 1MB File!!');
        return false;
      }
      
    }
    
  });
});

function imageIsLoaded(e){
			$("#editfile").css("color","green");
			$('#editpreviewing').attr('src',e.target.result);

		}
</script>
@endsection
