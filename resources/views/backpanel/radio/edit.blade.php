@extends('layouts.master')
@section('content')


<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}"> 
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<!-- Content Header (Page header) -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">{{ __('backend.'.$lang).' '.__('backend.manager') }}</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">{{ __('backend.home') }}</a></li>
                <li class="breadcrumb-item active">{{ __('backend.'.$lang).' '.__('backend.manager') }}</li>

        </div>
    </div>
</div>

<!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
    <div class="row">
        <div class="col-md-12">
        <div class="card card-primary card-outline">
                <div class="card-body">   
                    <table class="table table-striped">
                        <thead>
                            <tr>
                              <th scope="col">Name</th>
                              <th scope="col">Start Time</th>
                              <th scope="col">End Time</th>
                              <th scope="col">Week</th>
                              <th scope="col">Duration</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{$data->name}}</td>
                                <td>{{$data->start_time}}</td>
                                <td>{{$data->end_time}}</td>
                                <td>{{$data->week}}</td>
                                <td>{{$data->duration}} Sec</td>
                            </tr>  
                        </tbody>
                    </table>
                   
                    <form method="POST" action="{{  routeUser($page.'.edit') }}" id="edit_role">
                        <div class="row">
                            <div class="col-md-6" style="border: 1px solid #ccc;">
                                <label style="text-align: center;width: 100%; font-weight: bold;">Program</label>
                                <table class="table table-condensed">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Program</th>
                                            <th>Duration</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($program[0]))
                                        @foreach($program as $program)
                                        <tr>
                                            <td>
                                                <input type="checkbox" class="add_program" data-type="program" data-duration="{{$program->duration}}" value="{{$program->id}}"  @if(in_array($program->id,$programList)) checked @endif>
                                            </td>
                                            <td>
                                                {{ucfirst($program->name)}}<Br>
                                                <b>Track : {{$program->track_count}}, Adds :  {{$program->adds_count}}</b>
                                            </td>
                                            <td>
                                                <b>{{$program->duration}} sec</b>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                           
                            <div class="col-md-6" style="border: 1px solid #ccc;">
                                 <label style="text-align: center;width: 100%; font-weight: bold;">Radio Program List</label>
                                <table class="table table-condensed">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Type</th>
                                            <th>Program</th>
                                            <th>Duration</th>
                                        </tr>
                                    </thead>
                                    
                                    <tbody class="radio_data">
                                        
                                        
                                    </tbody>
                                </table>
                        </div>
                    </form>


                </div>
            </div>
    </div>
</div>
</div>
    <script>
$(document).ready(function(){
    
    $('.add_program').click(function(){    
        var radio_id = "{{$data->id}}";
        var radio_duration = "{{$data->duration}}";
        var table_type = $(this).data('type');
        var duration = $(this).data('duration');
        var table_id = $(this).val();

        if($(this).is(':checked'))
        {
            var type = 'true';    
        }
        else
        {
            var type = 'false';    
        }
        $.ajax({
          type: 'get',
          data: {type:type,radio_id:radio_id,table_type:table_type,table_id:table_id,duration:duration,radio_duration:radio_duration},
          dataType:'html',
          url: "{{ routeUser($page.'.add_radio_list') }}",
          success:function(res){
            playlist();
//             ajax_datatable.draw();
          },   
          error:function(jqXHR,textStatus,textStatus){
            console.log(jqXHR);
            toastr.error(jqXHR.statusText)
          }
        });
    });
});
playlist();
function playlist(){
    var url = "{{ routeUser('radio.programList',['id'=>$data->id])}}";
    $('.radio_data').load(url);

}
       
    
    
/*
$('#edit_role').parsley();
$("#edit_role").on('submit',function(e){ 

  e.preventDefault();
  var _this=$(this); 
    var formData = new FormData(this);
    formData.append('_method', 'POST');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
      url:'{{ routeUser($page.'.create')}}',
      dataType:'json',
      data:formData,
      type:'POST',
      cache:false,
      contentType: false,
      processData: false,
      beforeSend: function (){before(_this)},
      // hides the loader after completion of request, whether successfull or failor.
      complete: function (){complete(_this)},
      success:function(res){
        if(res.status === 1){ 
             $('#editModal').modal('hide') ; 
          toastr.success(res.message);
          $('#edit_role').parsley().reset();
          ajax_datatable.draw();
        }else{
          toastr.error(res.message);
        }
      },
      error:function(jqXHR,textStatus,textStatus){
        if(jqXHR.responseJSON.errors){
          $.each(jqXHR.responseJSON.errors, function( index, value ) {
            toastr.error(value)
          });
        }else{
          toastr.error(jqXHR.responseJSON.message)
        }
      }
    });
  return false;   
});


    
$("#editfile").change(function(){
    var fileObj = this.files[0];
    var imageFileType = fileObj.type;
    var imageSize = fileObj.size;
  
    var match = ["image/jpeg","image/png","image/jpg"];
    if(!((imageFileType == match[0]) || (imageFileType == match[1]) || (imageFileType == match[2]))){
      $('#editpreviewing').attr('src','images/image.png');
      toastr.error('Please Select A valid Image File <br> Note: Only jpeg, jpg and png Images Type Allowed!!');
      return false;
    }else{
      //console.log(imageSize);
      if(imageSize < 1000000){
        var reader = new FileReader();
        reader.onload = imageIsLoaded;
        reader.readAsDataURL(this.files[0]);
      }else{
        toastr.error('Images Size Too large Please Select 1MB File!!');
        return false;
      }
      
    }
    
  });
});

function imageIsLoaded(e){
			$("#editfile").css("color","green");
			$('#editpreviewing').attr('src',e.target.result);

		}
                */
</script>
@endsection
