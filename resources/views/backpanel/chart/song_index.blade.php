@extends('layouts.master')

@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">{{ __($lang.' Manage') }}</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ routeUser('dashboard') }}">{{ __('Home') }}</a></li>
                <li class="breadcrumb-item active">{{ __($lang.' Manage') }}</li>
            </ol>

        </div>
    </div>
</div>
<!-- /.content-header -->
    <!-- Main content -->
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-outline">
                <div class="card-body"> 
                    <div class="row">
                        
                        <!--div class="col-md-1">
                            <button type="button" class="btn btn-primary btn-sm">
                                <i class="fa fa-plus"></i>
                                 Add 
                            </button>
                        </div-->
                        
                    </div>   
                    <Br/>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="row">                                
                                <div class="col-md-12">
                                      <div class="form-group">
                                        <select class="form-control chart_type" name="chart_type">
                                            @foreach($chart as $key => $chart)
                                            <option value="{{$chart->id}}">{{ ucwords($chart->name)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                      <div class="form-group">
                                        <select class="form-control album_type" name="album_type">
                                            <option value="SINGLE">SINGLE</option>
                                            <option value="MULTIPLE">MULTIPLE</option>
                                        </select>
                                    </div>
                                </div>
                              
                                <div class="col-md-12 multileAlbums" style="display:none">
                                      <div class="form-group">
                                        <input type="text" placeholder="Search Playlist" class="form-control searchplaylist">
                                        <div class="col-md-12 showsearchplaylist" style="overflow-y: auto; max-height: 500px;"></div>
                                    </div>
                                </div>
                                                               
                                <div class="col-md-12 upcsearch">
                                    <div class="form-group">
                                        <input type="text" placeholder="Search Album, UPC, Song, Atrist" class="form-control searchbox">
                                    </div>
                                </div>
                                <div class="col-md-12 searchList" style="overflow-y: auto; max-height: 500px;"></div>
                                
                            </div>

                        </div>
                        <div class="col-md-8">
                              <table  id="listing" class="table table-striped table-bordered" style="width:100%;">
                                <thead>
                                    <tr>
                                        <th>{{ __('No.') }}</th>
                                        <th>{{ __('Title') }}</th> 
                                        <th>{{ __('Track') }}</th> 
                                        <th>{{ __('Rank') }}</th>
                                        <th>{{ __('Action') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    
    
    <div class="modal fade" id="add_modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <form method="POST" action="{{ routeUser($page.'.create') }}" id="add_category">
    @csrf
      <div class="modal-header">
        <h4 class="modal-title">{{ __('backend.add').' '.__('backend.'.$lang) }}</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="tab-content" style="margin-top:10px">
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label class="control-label" for="name"> {{__('backend.name')}}*</label>
            <input type="text" name="name" value="" id="name" class="form-control" placeholder=" Name"  />
          </div>
        </div>        
      </div>
      </div>
          
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label class="col-md-12" for="image">Image</label>
            <input type="file" id="file" name="image" class="form-control">
            <div id="image_preview"><img height="100" width="100" id="previewing" src="{{ URL::asset('images/image.png')}}"></div>
          </div>
        </div>
      </div>
            
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary  btn-xs save"><span class="spinner-grow spinner-grow-sm formloader" style="display: none;" role="status" aria-hidden="true"></span> Save</button>
      </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
    
@endsection
@section('script')
<style>
    .dataTables_wrapper {
    padding-top: 0px !important;
}
</style>
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/parsley.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
var ajax_datatable;
$(document).ready(function(){
    
$('#add_category').parsley();
ajax_datatable = $('#listing').DataTable({
    processing: true,
    serverSide: true,
    pageLength: 100,
    ajax:{
      url:'{{ routeUser($page.'.song.index') }}',
      data: function (d) {
        return $.extend({}, d, {
            "chart_type":"Home",
            "chart_id":$('.chart_type option:selected').val(),
            "album_type":$('.album_type option:selected').val(),
            "album_id":$('.album_id option:selected').val(),
        });         
      }
    },
    columns: [
      { data: 'id', name: 'id', orderable: false, searchable: false, "width": "20px"},
      { data: 'title', name: 'title' },
      { data: 'track', name: 'track' },
      { data: 'rank', name: 'rank' },
      { data: 'actions', name: 'actions' },
    ],
    order: [ [0, 'desc'] ],  
});

    $('#search-form').on('submit', function(e) {
        ajax_datatable.draw();
        e.preventDefault();
    });
    $('.chart_type').change(function(){
        ajax_datatable.draw();
    });
});

$(document).on('change','.album_type',function(){
   var type =  $(this).val();
   if(type == 'MULTIPLE')
   {
      $('.multileAlbums').show(); 
      $('.upcsearch').hide();
      $('.searchList').hide();
   }
   else
   {
      $('.multileAlbums').hide(); 
      $('.upcsearch').show();
      $('.searchList').show();
   }
     searchAlbum(); 
});

$(document).on('click','.add_top_list',function(){
    var values = $(this).val();     
    var chart_type = "Home";
    var chart_id = $('.chart_type option:selected').val();
    var album_type = $('.album_type option:selected').val();
    var format = $(this).data('format');
    if($(this).is(':checked'))
    {
        var type = 'true';    
    }
    else
    {
        var type = 'false';    
    }
    
     $.ajax({
          type: 'get',
          data: {type:type,chart_type:chart_type,chart_id:chart_id,values:values,chart_type:chart_type,album_type:album_type,format:format},
          dataType:'html',
          url: "{{ routeUser($page.'.add_top_list') }}",
          success:function(res){
             ajax_datatable.draw();
          },   
          error:function(jqXHR,textStatus,textStatus){
            console.log(jqXHR);
            toastr.error(jqXHR.statusText)
          }
      });
});


$(document).on('keyup','.changeRank',function(){
    var values = $(this).val();
    var id = $(this).data('id');
    $.ajax({
          type: 'get',
          data: {values:values,id:id},
          dataType:'html',
            url: "{{ routeUser($page.'.add_chart_song_rank') }}",
          success:function(res){
           // ajax_datatable.draw();
          },   
          error:function(jqXHR,textStatus,textStatus){
            console.log(jqXHR);
            toastr.error(jqXHR.statusText)
          }
      });
}); 
    
$(document).on('click','.album_id',function(){
    var values = $(this).val();
    if(values == 'Add')
    {
        $('#add_modal').modal('show');
    }
});

$(document).on('keyup','.searchbox',function(){
    searchAlbum();   
});

$(document).on('change','.chart_type',function(){
     searchAlbum(); 
});


$(document).on('change','.language',function(){
     searchAlbum(); 
});

$(document).on('keyup','.searchplaylist',function(){
    if($('.searchplaylist').val().length >= 3)
    {
        mutipCharge();
    }
});
    


function searchAlbum(){
    if($('.searchbox').val().length >= 3)
    {
        var serching = $('.searchbox').val();
        
        var chart_type = "Home";
        var chart_id = $('.chart_type option:selected').val();
        var album_type = $('.album_type option:selected').val();
        var album_id = $('.album_id option:selected').val();
        
        $.ajax({
          type: 'get',
          data: {serching:serching,chart_type:chart_type,chart_id:chart_id,album_type:album_type,album_id:album_id},
          dataType:'html',
          url: "{{ routeUser($page.'.search') }}",
          success:function(res)
          {
              $('.searchList').html(res);
          },   
          error:function(jqXHR,textStatus,textStatus){
            console.log(jqXHR);
            toastr.error(jqXHR.statusText)
          }
      });
    }
}

$("#add_category").on('submit',function(e){
      e.preventDefault();
      var _this=$(this); 
        var formData = new FormData(this);
        $.ajax({
            url:'{{ routeUser("mulitchart.create")}}',
            dataType:'json',
            data:formData,
            type:'POST',
            cache:false,
            contentType: false,
            processData: false,
            beforeSend: function (){before(_this)},
            // hides the loader after completion of request, whether successfull or failor.
            complete: function (){complete(_this)},
            success:function(res){
                  if(res.status === 1){ 
                    toastr.success(res.message);
                    $('#add_category')[0].reset();
                    $('#previewing').attr('src','images/image.png');
                    $('#add_category').parsley().reset();
                    $('#add_modal').modal('hide');
                   // ajax_datatable.draw();
                    mutipCharge();
                  }else{
                    toastr.error(res.message);
                  }
              },
            error:function(jqXHR,textStatus,textStatus){
              if(jqXHR.responseJSON.errors){
                $.each(jqXHR.responseJSON.errors, function( index, value ) {
                  toastr.error(value)
                });
              }else{
                toastr.error(jqXHR.responseJSON.message)
              }
            }
          });
          return false;   
        });

     $("#file").change(function(){
        var fileObj = this.files[0];
        var imageFileType = fileObj.type;
        var imageSize = fileObj.size;

        var match = ["image/jpeg","image/png","image/jpg"];
        if(!((imageFileType == match[0]) || (imageFileType == match[1]) || (imageFileType == match[2]))){
          $('#previewing').attr('src','images/image.png');
          toastr.error('Please Select A valid Image File <br> Note: Only jpeg, jpg and png Images Type Allowed!!');
          return false;
        }else{
          //console.log(imageSize);
          if(imageSize < 1000000){
            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(this.files[0]);
          }else{
            toastr.error('Images Size Too large Please Select 1MB File!!');
            return false;
          }
        }
      });



 function imageIsLoaded(e){
	//console.log(e);
	$("#file").css("color","green");
	$('#previewing').attr('src',e.target.result);
    }
    mutipCharge();
    function mutipCharge(){
        var search = $('.searchplaylist').val();        
            $.ajax({
              type: 'get',
              data: {search:search},
              dataType:'html',
                url: "{{ routeUser('mulitchart.list') }}",
              success:function(res){
                $('.showsearchplaylist').html(res);
              },   
              error:function(jqXHR,textStatus,textStatus){
                console.log(jqXHR);
                toastr.error(jqXHR.statusText)
              }
        });
    }
</script>
@endsection
