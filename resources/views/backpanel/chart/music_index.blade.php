@extends('layouts.master')

@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">{{ __($lang.' Manage') }}</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ routeUser('dashboard') }}">{{ __('Home') }}</a></li>
                <li class="breadcrumb-item active">{{ __($lang.' Manage') }}</li>
            </ol>

        </div>
    </div>
</div>
<!-- /.content-header -->
    <!-- Main content -->
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-outline">
                <div class="card-body"> 
                    <div class="row">
                        
                        <!--div class="col-md-1">
                            <button type="button" class="btn btn-primary btn-sm">
                                <i class="fa fa-plus"></i>
                                 Add 
                            </button>
                        </div-->
                        
                    </div>   
                    <Br/>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="row">                                
                                <div class="col-md-12">
                                      <div class="form-group">
                                        <select class="form-control language" name="language">   
                                            @foreach($language as $key => $language)
                                            <option value="{{$language->language_code}}">{{ ucwords($language->title)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>                               
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" placeholder="Search Album, UPC, Song, Atrist" class="form-control searchbox">                                   
                                    </div>
                                </div>
                                <div class="col-md-12 searchList" style="overflow-y: auto; max-height: 500px;"></div>
                            </div>

                        </div>
                        <div class="col-md-8">
                              <table  id="listing" class="table table-striped table-bordered" style="width:100%;">
                                <thead>
                                    <tr>
                                        <th>{{ __('No.') }}</th>
                                        <th>{{ __('UPC') }}</th>                                      
                                        <th>{{ __('Title') }}</th> 
                                        <th>{{ __('Rank') }}</th>
                                        <th>{{ __('Action') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<style>
    .dataTables_wrapper {
    padding-top: 0px !important;
}
</style>
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/parsley.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
var ajax_datatable;
$(document).ready(function(){
ajax_datatable = $('#listing').DataTable({
    processing: true,
    serverSide: true,
    pageLength: 100,
    ajax:{
      url:'{{ routeUser($page.'.music.index') }}',
      data: function (d) {
        return $.extend({}, d, {
            "chart_id":0,
            "chart_type" :'Music',
            "language" :$('.language option:selected').val()
        });         
      }
    },
    columns: [
      { data: 'id', name: 'id', orderable: false, searchable: false, "width": "20px"},
      { data: 'upc', name: 'upc' },
      { data: 'title', name: 'title' },
      { data: 'rank', name: 'rank' },
      { data: 'actions', name: 'actions' },
    ],
    order: [ [0, 'desc'] ],  
});

    $('#search-form').on('submit', function(e) {
        ajax_datatable.draw();
        e.preventDefault();
    });
    $('.chart_type').change(function(){
        ajax_datatable.draw();
    });
});


$(document).on('click','.add_top_list',function(){
    var values = $(this).val(); 
    var chart_type = 'Music';
    var chart_id = 0;
    var language = $('.language option:selected').val();
    var format = $(this).data('format');
    
    
    if($(this).is(':checked'))
    {
        var type = 'true';    
    }
    else
    {
        var type = 'false';    
    }
    
     $.ajax({
          type: 'get',
          data: {type:type,values:values,language:language,chart_id:chart_id,chart_type:chart_type,format:format},
          dataType:'html',
          url: "{{ routeUser($page.'.add_top_list') }}",
          success:function(res){
             ajax_datatable.draw();
          },   
          error:function(jqXHR,textStatus,textStatus){
            console.log(jqXHR);
            toastr.error(jqXHR.statusText)
          }
      });
});


$(document).on('keyup','.changeRank',function(){
    var values = $(this).val();
    var id = $(this).data('id');
    $.ajax({
          type: 'get',
          data: {values:values,id:id},
          dataType:'html',
            url: "{{ routeUser($page.'.add_chart_song_rank') }}",
          success:function(res){
           // ajax_datatable.draw();
          },   
          error:function(jqXHR,textStatus,textStatus){
            console.log(jqXHR);
            toastr.error(jqXHR.statusText)
          }
      });
}); 
    

$(document).on('keyup','.searchbox',function(){
    searchAlbum();   
});

$(document).on('change','.chart_type',function(){
     searchAlbum(); 
});
$(document).on('change','.language',function(){
    ajax_datatable.draw();
     searchAlbum(); 
});


function searchAlbum()
{
    if($('.searchbox').val().length >= 3)
    {
        var serching = $('.searchbox').val();
        var chart_id = $('.chart_type option:selected').val();
        var chart_type ='Music';
        var language = $('.language option:selected').val();
        $.ajax({
          type: 'get',
          data: {serching:serching,language:language,chart_type:chart_type,chart_id:chart_id},
          dataType:'html',
          url: "{{ routeUser($page.'.search') }}",
          success:function(res)
          {
              $('.searchList').html(res);
          },   
          error:function(jqXHR,textStatus,textStatus){
            console.log(jqXHR);
            toastr.error(jqXHR.statusText)
          }
      });
    }
}
</script>
@endsection
