<table class="table table-condensed">
<input type="hidden" name="format" >

@forelse($data as $data)
<tr>
    <td>
        @if($data->getRelease->format=='Single')
            <input type="checkbox" class="add_top_list" data-format="Track" value="{{$data->id}}"/>
        @else
            <input type="checkbox" class="add_top_list" data-format="Album" value="{{$data->release_id}}"/>
        @endif
    </td>
    <td>{{$data->UPC}}
     <br/>
    @if($data->getRelease->format=='Single')
        <b>Track</b>
    @else
       <b> Album</b>
    @endif
    </td>
    <td>
    @if($data->getRelease->format=='Single')
        <b> {{ ucfirst($data->title)}}</b>
    @else
       <b>  {{ ucfirst($data->getRelease->title)}}</b>
    @endif
    </td>
</tr>
@empty
<tr>
No Song found.
</tr>
@endforelse
</table>