@extends('layouts.master')

@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">{{ __($lang.' Manage') }}</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ routeUser('dashboard') }}">{{ __('Home') }}</a></li>
                <li class="breadcrumb-item active">{{ __($lang.' Manage') }}</li>
            </ol>

        </div>
    </div>
</div>
<!-- /.content-header -->
    <!-- Main content -->
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-outline">
                <div class="card-body"> 
                    <div class="row">
                        
                        <!--div class="col-md-1">
                            <button type="button" class="btn btn-primary btn-sm">
                                <i class="fa fa-plus"></i>
                                 Add 
                            </button>
                        </div-->
                        
                    </div>   
                    <Br/>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="row">                                
                                <div class="col-md-12">
                                      <div class="form-group">
                                        <select class="form-control chart_type" name="chart_type">
                                            @foreach($chart as $key => $chart)
                                            <option value="{{$chart->id}}">{{ ucwords($chart->name)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                             
                                
                                <div class="col-md-12 multileAlbums" style="display:none">
                                      <div class="form-group">
                                        <select class="form-control album_id" name="album_id">
                                            @foreach($multichart as $key => $multichart)
                                            <option value="{{$multichart->id}}">{{ ucwords($multichart->name)}}</option>
                                            @endforeach
                                            
                                            
                                            <option value="Add">+ Create New</option>
                                        </select>
                                    </div>
                                </div>
                                                               
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" placeholder="Search Album, UPC, Song, Atrist" class="form-control searchbox">                                   
                                    </div>
                                </div>
                                <div class="col-md-12 searchList" style="overflow-y: auto; max-height: 500px;"></div>
                            </div>

                        </div>
                        <div class="col-md-8">
                           <h3>Artist List</h3>
                            <table  id="Artistlisting" class="table table-striped table-bordered" style="width:100%;">
                                <thead>
                                    <tr>
                                        <th>{{ __('No.') }}</th>
                                        <th>{{ __('Title') }}</th>
                                        <th>{{ __('Rank') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                           <hr/>
                            <h3>Track List</h3>
                              <table  id="listing" class="table table-striped table-bordered" style="width:100%;">
                                <thead>
                                    <tr>
                                        <th>{{ __('No.') }}</th>
                                        <th>{{ __('Title') }}</th> 
                                        <th>{{ __('Track') }}</th> 
                                        <th>{{ __('Rank') }}</th>
                                        <th>{{ __('Action') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    
    
  <div class="modal fade" id="add_modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <form method="POST" action="{{ routeUser($page.'.create') }}" id="add_category">
    @csrf
      <div class="modal-header">
        <h4 class="modal-title">{{ __('backend.add').' '.__('backend.'.$lang) }}</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="tab-content" style="margin-top:10px">
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label class="control-label" for="name"> {{__('backend.name')}}*</label>
            <input type="text" name="name" value="" id="name" class="form-control" placeholder=" Name"  />
          </div>
        </div>        
      </div>
      </div>
          
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label class="col-md-12" for="image">Image</label>
            <input type="file" id="file" name="image" class="form-control">
            <div id="image_preview"><img height="100" width="100" id="previewing" src="{{ URL::asset('images/image.png')}}"></div>
          </div>
        </div>
      </div>
            
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary  btn-xs save"><span class="spinner-grow spinner-grow-sm formloader" style="display: none;" role="status" aria-hidden="true"></span> Save</button>
      </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
    
@endsection
@section('script')
<style>
    .dataTables_wrapper {
    padding-top: 0px !important;
}
</style>
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/parsley.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
var ajax_datatable;
$(document).ready(function(){
    
$('#add_category').parsley();
ajax_datatable = $('#listing').DataTable({
    processing: true,
    serverSide: true,
    pageLength: 100,
    ajax:{
      url:'{{ routeUser($page.'.artist.song.index') }}',
      data: function (d) {
        return $.extend({}, d, {
            "chart_type":"Home",
            "chart_id":$('.chart_type option:selected').val(),
            "album_type":$('.album_type option:selected').val(),
            "album_id":$('.album_id option:selected').val(),
        });         
      }
    },
    columns: [
      { data: 'id', name: 'id', orderable: false, searchable: false, "width": "20px"},
      { data: 'title', name: 'title' },
      { data: 'track', name: 'track' },
      { data: 'rank', name: 'rank' },
      { data: 'actions', name: 'actions' },
    ],
    order: [ [0, 'desc'] ],  
});




ajax_datatable = $('#Artistlisting').DataTable({
    processing: true,
    serverSide: true,
    pageLength: 10,
    ajax:{
      url:'{{ routeUser($page.'.artist.index') }}',
      data: function (d) {
        return $.extend({}, d, {
            "chart_type":"Artist",
            "chart_id":$('.chart_type option:selected').val(),
        });         
      }
    },
    columns: [
      { data: 'id', name: 'id', orderable: false, searchable: false, "width": "20px"},
      { data: 'title', name: 'title' },
      { data: 'rank', name: 'rank' },
    ],
    order: [ [0, 'desc'] ],  
});


    $('#search-form').on('submit', function(e) {
        ajax_datatable.draw();
        e.preventDefault();
    });
    $('.chart_type').change(function(){
        ajax_datatable.draw();
    });
});

$(document).on('change','.album_type',function(){
   var type =  $(this).val();
   if(type == 'MULTIPLE')
   {
      $('.multileAlbums').show(); 
   }
   else
   {
      $('.multileAlbums').hide(); 
   }
     searchAlbum(); 
});

$(document).on('click','.add_top_list',function(){
    var values = $(this).val();     
    var chart_type = "Artist";
    var chart_id = $('.chart_type option:selected').val();
    
    if($(this).is(':checked'))
    {
        var type = 'true';    
    }
    else
    {
        var type = 'false';    
    }
    
     $.ajax({
          type: 'get',
          data: {type:type,chart_type:chart_type,chart_id:chart_id,values:values,chart_type:chart_type},
          dataType:'html',
          url: "{{ routeUser($page.'.add_top_artist_list') }}",
          success:function(res){
             ajax_datatable.draw();
          },   
          error:function(jqXHR,textStatus,textStatus){
            console.log(jqXHR);
            toastr.error(jqXHR.statusText)
          }
    });
});


$(document).on('keyup','.changeRank',function(){
    var values = $(this).val();
    var id = $(this).data('id');
    $.ajax({
          type: 'get',
          data: {values:values,id:id},
          dataType:'html',
            url: "{{ routeUser($page.'.add_chart_song_rank') }}",
          success:function(res){
           // ajax_datatable.draw();
          },   
          error:function(jqXHR,textStatus,textStatus){
            console.log(jqXHR);
            toastr.error(jqXHR.statusText)
          }
      });
}); 

$(document).on('keyup','.changeArtistRank',function(){
    var values = $(this).val();
    var id = $(this).data('id');
    $.ajax({
          type: 'get',
          data: {values:values,id:id},
          dataType:'html',
            url: "{{ routeUser($page.'.add_chart_artist_rank') }}",
          success:function(res){
           // ajax_datatable.draw();
          },   
          error:function(jqXHR,textStatus,textStatus){
            console.log(jqXHR);
            toastr.error(jqXHR.statusText)
          }
      });
}); 
    
$(document).on('click','.album_id',function(){
    var values = $(this).val();
    if(values == 'Add')
    {
        $('#add_modal').modal('show');
    }
});

$(document).on('keyup','.searchbox',function(){
    searchAlbum();   
});

$(document).on('change','.chart_type',function(){
     searchAlbum(); 
});


$(document).on('change','.language',function(){
     searchAlbum(); 
});


function searchAlbum(){
    if($('.searchbox').val().length >= 3)
    {
        var serching = $('.searchbox').val();        
        var chart_type = "Artist";
        var chart_id = $('.chart_type option:selected').val();        
        $.ajax({
          type: 'get',
          data: {serching:serching,chart_type:chart_type,chart_id:chart_id},
          dataType:'html',
          url: "{{ routeUser($page.'.search.artist') }}",
          success:function(res)
          {
                $('.searchList').html(res);
          },   
          error:function(jqXHR,textStatus,textStatus){
            console.log(jqXHR);
            toastr.error(jqXHR.statusText)
          }
      });
    }
}

</script>
@endsection
