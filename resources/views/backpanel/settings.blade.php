@extends('layouts.master')

@section('content')
<!-- Content Header (Page header) -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">{{ __('Settings') }}</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ routeUser('dashboard') }}">{{ __('Home') }}</a></li>
                <li class="breadcrumb-item active">{{ __('Settings') }}</li>
                
            </ol>
            <a href="#" class="btn btn-primary btn-sm d-none d-lg-block m-l-15" title="{{ __('Change Password') }}" data-toggle="modal" data-target="#change_passwords" ><i class="fa fa-plus"></i> {{ __('Change Password') }}</a>
                 <a href="#" class="btn btn-primary btn-sm d-none d-lg-block m-l-15" title="{{ __('Email Settings') }}" data-toggle="modal" data-target="#email_change" ><i class="fa fa-plus"></i> {{ __('Email Settings') }}</a>
        </div>
    </div>
</div>
<!-- /.content-header -->
<!-- Main content -->
<div class="content">
<div class="row">
<!-- Column -->
<div class="col-lg-4 col-xlg-3 col-md-5">
    <div class="card">
        <div class="card-body">
            <center class="m-t-30">       
                <img src="{{Auth::user()->image}}" class="img-circle" width="150">
                <h4 class="card-title m-t-10">{{ucwords(Auth::user()->name)}}</h4>
            </center>
        </div>
        <div>
            <hr> </div>
        <div class="card-body"> <small class="text-muted">Email address </small>
            <h6>{{Auth::user()->email}}</h6> <small class="text-muted p-t-30 db">Phone</small>
            <h6>{{Auth::user()->mobile}}</h6> 
            
        </div>
    </div>
</div>
<!-- Column -->
<!-- Column -->
<div class="col-lg-8 col-xlg-9 col-md-7">
    <div class="card">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs profile-tab" role="tablist">
            <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#settings" role="tab" aria-selected="true">Settings</a> </li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active" id="settings" role="tabpanel">
            <form method="POST" action="{{ routeUser('saveProfile') }}" id="save_profile">
            @csrf
             <div class="card-body">
                <div class="form-group">
                    <label class="col-md-12" for="first_name">First Name *</label>
                    <div class="col-md-12">
                        <input type="text" placeholder="First Name" id="first_name" value="{{Auth::user()->first_name}}" name="first_name" class="form-control form-control-line" data-parsley-required="true">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="last_name">Last Name *</label>
                    <div class="col-md-12">
                        <input type="text" placeholder="Last Name" id="first_name" value="{{Auth::user()->last_name}}" name="last_name" class="form-control form-control-line" data-parsley-required="true">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="email">Email</label>
                    <div class="col-md-12">
                        <input type="text" value="{{Auth::user()->email}}" id="email" placeholder="Email" class="form-control form-control-line"  readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="mobile">Mobile *</label>
                    <div class="col-md-12">
                        <input type="text" id="mobile" name="mobile" placeholder="Mobile" value="{{Auth::user()->mobile}}" class="form-control form-control-line"  data-parsley-required="true">
                    </div>
                </div>

                <div class="form-group">
                  <label class="col-md-12" for="image">Image</label>
                  <input type="file" id="file" name="image" class="form-control">
                  <div id="image_preview"><img height="50" width="50" id="previewing" src="{{ URL::asset('images/image.png')}}"></div>
                </div>

                <div class="form-group">
                    <label class="col-md-12" for="mobile"></label>
                    <div class="col-md-12">
                    <button type="submit" class="btn btn-success btn-sm"><span class="spinner-grow spinner-grow-sm formloader" style="display: none;" role="status" aria-hidden="true"></span> Save</button>
                    </div>
                </div>
                
             </div>
             </form>
            </div>
        </div>
    </div>
</div>
<!-- Column -->
</div>
</div>


<div class="modal fade" id="change_passwords">
        <div class="modal-dialog">
          <div class="modal-content">
          <form method="POST" action="{{ routeUser('changePassword') }}" id="change_password">
          @csrf
            <div class="modal-header">
              <h4 class="modal-title">{{ __('Password Settings') }}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label class="control-label" for="name">{{ __('Current Password') }}*</label>
                <input type="password" name="current_password" value="" id="current_password" class="form-control" placeholder="{{ __('Current Password') }}" data-parsley-required="true"  />
              </div>
              <div class="form-group">
                <label class="control-label" for="name">{{ __('New Password') }}*</label>
                <input type="password" name="new_password" value="" id="new_password" class="form-control" placeholder="{{ __('New Password') }}" data-parsley-required="true"  />
              </div>
              <div class="form-group">
                <label class="control-label" for="name">{{ __('Confirm Password') }}*</label>
                <input type="password" name="confirm_password" value="" id="confirm_password" class="form-control" placeholder="{{ __('Confirm Password') }}" data-parsley-required="true" data-parsley-equalto="#new_password" />
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               <button type="submit" class="btn btn-primary float-right save"><span class="spinner-grow spinner-grow-sm formloader" style="display: none;" role="status" aria-hidden="true"></span> Save</button>
            </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
      <div class="modal fade" id="email_change">
        <div class="modal-dialog">
          <div class="modal-content">
          <form method="POST" action="{{ routeUser('sendVerificationLink') }}" id="change_email">
            @csrf
            <div class="modal-header">
              <h4 class="modal-title">{{ __('Email Settings') }}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label class="control-label" for="email">Email*</label>
                
                <input type="text" name="email" value="{{Auth::user()->email}}" id="email" class="form-control" placeholder="Email" data-parsley-required="true"  />
              </div>
              <div class="form-group">
                <label class="control-label" for="name">Current Password*</label>
                <input type="password" name="current_password" value="" id="current_password" class="form-control" placeholder="Current Password" data-parsley-required="true"  />
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary float-right save"><span class="spinner-grow spinner-grow-sm formloader" style="display: none;" role="status" aria-hidden="true"></span> Save</button>
            </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
    <!-- /.content -->

<script src="{{ asset('js/parsley.min.js') }}"></script>
<script>
$(document).ready(function(){
$('#change_email').parsley();
$('#change_password').parsley();
$('#save_profile').parsley();

$("#change_email").on('submit',function(e){
  e.preventDefault();
  var _this=$(this); 
    var values = $('#change_email').serialize();
    $.ajax({
    url:'{{ routeUser('sendVerificationLink') }}',
    dataType:'json',
    data:values,
    type:'POST',
    beforeSend: function (){before(_this)},
    // hides the loader after completion of request, whether successfull or failor.
    complete: function (){complete(_this)},
    success:function(result){
        if(result.status){
          toastr.success(result.message)
          $('#change_email')[0].reset();
          $('#change_email').parsley().reset();
        }else{
          toastr.error(result.message)
        }
      },
    error:function(jqXHR,textStatus,textStatus){
      if(jqXHR.responseJSON.errors){
        $.each(jqXHR.responseJSON.errors, function( index, value ) {
          toastr.error(value)
        });
      }else{
        toastr.error(jqXHR.responseJSON.message)
      }
    }
      });
      return false;   
    });

$("#change_password").on('submit',function(e){
  e.preventDefault();
  var _this=$(this); 
    var values = $('#change_password').serialize();
    $.ajax({
    url:'{{ routeUser('changePassword') }}',
    dataType:'json',
    data:values,
    type:'POST',
    beforeSend: function (){before(_this)},
    // hides the loader after completion of request, whether successfull or failor.
    complete: function (){complete(_this)},
    success:function(result){
        if(result.status){
          toastr.success(result.message)
          $('#change_password')[0].reset();
          $('#change_password').parsley().reset();
        }else{
          toastr.error(result.message)
        }
      },
    error:function(jqXHR,textStatus,textStatus){
      if(jqXHR.responseJSON.errors){
        $.each(jqXHR.responseJSON.errors, function( index, value ) {
          toastr.error(value)
        });
      }else{
        toastr.error(jqXHR.responseJSON.message)
      }
    }
      });
      return false;   
    });
    $.ajaxSetup({
       headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
   });
    $("#save_profile").on('submit',function(e){
      e.preventDefault();
      var _this=$(this); 
      var formData = new FormData(this);
        $.ajax({
        url:'{{ routeUser('saveProfile') }}',
        dataType:'json',
        data:formData,
        cache:false,
        contentType: false,
        processData: false,
        type:'POST',
        beforeSend: function (){before(_this)},
        // hides the loader after completion of request, whether successfull or failor.
        complete: function (){complete(_this)},
        success:function(result){
            if(result.status){
              toastr.success(result.message)
              $('#save_profile')[0].reset();
              $('#save_profile').parsley().reset();
            }else{
              toastr.error(result.message)
            }
          },
        error:function(jqXHR,textStatus,textStatus){
          if(jqXHR.responseJSON.errors){
            $.each(jqXHR.responseJSON.errors, function( index, value ) {
              toastr.error(value)
            });
          }else{
            toastr.error(jqXHR.responseJSON.message)
          }
        }
          });
          return false;   
    });

    $("#file").change(function(){
				var fileObj = this.files[0];
				var imageFileType = fileObj.type;
				var imageSize = fileObj.size;
			
				var match = ["image/jpeg","image/png","image/jpg"];
				if(!((imageFileType == match[0]) || (imageFileType == match[1]) || (imageFileType == match[2]))){
					$('#previewing').attr('src','images/image.png');
          toastr.error('Please Select A valid Image File <br> Note: Only jpeg, jpg and png Images Type Allowed!!');
					return false;
				}else{
					//console.log(imageSize);
					if(imageSize < 1000000){
						var reader = new FileReader();
						reader.onload = imageIsLoaded;
						reader.readAsDataURL(this.files[0]);
					}else{
            toastr.error('Images Size Too large Please Select 1MB File!!');
						return false;
					}
					
				}
				
			});

  });
  function imageIsLoaded(e){
			//console.log(e);
			$("#file").css("color","green");
			$('#previewing').attr('src',e.target.result);

		}
</script>

@endsection
