@extends('layouts.app')

@section('content')
<section id="wrapper">
    <div class="login-register" style="background: url({{ URL::asset('assets/images/background/music_background.jpg')}}); background-size: 100% 100%;">>
        <div class="login-box">
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <form method="POST" action="{{ route('password.email') }}">
                    <div class="text-center">
                        <img src="{{ URL::asset('assets/images/logo2.png')}}" style="padding: 10px;"  width="130" alt="homepage" /></span> 
                        <hr>
                        <h1 class="h4 text-gray-900 mb-4">{{ __('Welcome to Mp3 Bajao') }} ! </h1>
                    </div>
                    @csrf

                    <div class="form-group">
                        
                        <div class="col-xs-12">
                            <input id="email" placeholder="Email" type="email" class="form-control @error('email') is-invalid @enderror textBoxround" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-block  btn-primary btn-rounded">
                                {{ __('Forgot Password') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
