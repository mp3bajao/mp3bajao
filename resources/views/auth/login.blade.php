@extends('layouts.app')
@section('content')
<section id="wrapper">
    <div class="login-register" style="background: url({{ URL::asset('assets/images/background/music_background.jpg')}}); background-size: 100% 100%;">
            <div class="login-box">
            <div class="login-box ">
                <div class="card-body">
                    <form method="POST" class="form-horizontal form-material" id="loginform" action="{{ route('login') }}">
                        <div class="text-center">
                                <img src="{{ URL::asset('assets/images/logo2.png')}}"  width="130" style="padding: 10px;" alt="homepage" /></span> 
                            <hr>
                            <h1 class="h4 text-gray-900 mb-4">{{ __('Welcome to Mp3 Bajao') }} ! </h1>
                        </div>
                        @csrf

                        <div class="form-group">
                            <div class="col-xs-12">
                                <input id="email"  placeholder="Email" type="email" class="form-control @error('email') is-invalid @enderror textBoxround" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                                <input id="password" placeholder="Password" type="password" class=" form-control @error('password') is-invalid @enderror textBoxround" name="password" required autocomplete="current-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="custom-control custom-checkbox">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                    @if (Route::has('password.request'))
                                    <a class="text-dark float-right" href="{{ route('password.request') }}">
                                        <i class="fa fa-lock m-r-5"></i> {{ __('Forgot Password') }}
                                    </a>
                                @endif
                                </div>
                            </div>
                        </div>
                     
                        

                        <div class="form-group text-center">
                            <div class="col-xs-12 p-b-20">
                                <button type="submit" class="btn btn-block btn-primary btn-rounded">
                                    {{ __('Login') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    <center>
                        <span><script>document.write(new Date().getFullYear())</script> © {{ config('app.name')}}.</script></span>
                    </center>
                </div>
                </div>
            </div>
    </div>
</section>
@endsection
