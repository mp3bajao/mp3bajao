@extends('layouts.master')

@section('content')
<!-- Content Header (Page header) -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">{{ __('Admin Settings') }}</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">{{ __('Home') }}</a></li>
                <li class="breadcrumb-item active">{{ __('Admin Settings') }}</li>
                
            </ol>
          </div>
    </div>
</div>
<!-- /.content-header -->
<!-- Main content -->
<div class="content">
<div class="row">
<!-- Column -->
<!-- Column -->
<!-- Column -->

<div class="col-lg-12 col-xlg-12 col-md-12">
    <div class="card">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs profile-tab" role="tablist">
            <li class="nav-item"> <a class="nav-link active">Delivery Price</a> </li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active" id="settings" role="tabpanel">
            <form method="POST" id="prices_save_form">
            @csrf
             <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                    <label class="control-label" for="type" style="display:block;">Type *</label>              
                    <div class="custom-control custom-radio" style="display:inline-block;">
                        <input type="radio" class="custom-control-input" id='customRadio1' name="type" data-parsley-required="true" value="KM" {{($deliveryPrice[0]['type']=="KM"?'checked':'')}}/>
                        <label class="custom-control-label" for="customRadio1">Km</label>
                    </div>
                    <div class="custom-control custom-radio" style="display:inline-block;">
                        <input type="radio" class="custom-control-input" id='customRadio2' name="type" data-parsley-required="true" value="MIN" {{($deliveryPrice[0]['type']=="MIN"?'checked':'')}}/> 
                        <label class="custom-control-label" for="customRadio2">Minute</label>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-6" for="per_prices">Prices *</label>
                        <input type="text" placeholder="Per Price" id="per_prices" value="{{$deliveryPrice[0]['prices']}}" name="per_prices" class="form-control form-control-line" data-parsley-required="true">
                    </div>
                  </div>
                </div>
             
                <div class="form-group">
                    <label class="col-md-12" for="mobile"></label>
                    <div class="col-md-12">
                    <button type="submit" class="btn btn-success"><span class="spinner-grow spinner-grow-sm formloader" style="display: none;" role="status" aria-hidden="true"></span> Save</button>
                    </div>
                </div>
                
             </div>
             </form>
            </div>
        </div>
    </div>
</div>
<!-- Column -->
</div>
</div>

<script src="{{ asset('js/parsley.min.js') }}"></script>
<script>
$(document).ready(function(){
  $('#prices_save_form').parsley();
  $("#prices_save_form").on('submit',function(e){
  e.preventDefault();
  var _this=$(this); 
    var values = $('#prices_save_form').serialize();
    $.ajax({
    url:'{{ url('price-save/'.$deliveryPrice[0]['id']) }}',
    dataType:'json',
    data:values,
    type:'POST',
    beforeSend: function (){before(_this)},
    // hides the loader after completion of request, whether successfull or failor.
    complete: function (){complete(_this)},
    success:function(result){
        if(result.status){
          toastr.success(result.message);
        }else{
          toastr.error(result.message)
        }
      },
    error:function(jqXHR,textStatus,textStatus){
      if(jqXHR.responseJSON.errors){
        $.each(jqXHR.responseJSON.errors, function( index, value ) {
          toastr.error(value)
        });
      }else{
        toastr.error(jqXHR.responseJSON.message)
      }
    }
      });
      return false;   
    });
});

</script>

@endsection
