@yield('css')
<link type="text/css" rel="stylesheet"  href="{{ URL::asset('assets/jsgrid/jsgrid.min.css')}}" />
<link type="text/css" rel="stylesheet" href="{{ URL::asset('assets/jsgrid/jsgrid-theme.min.css')}}" />
<link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/custom.css') }}">

<link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('plugins/pace-progress/pace.min.js') }}"></script>
<!-- Custom CSS -->
<link href="{{ URL::asset('dist/css/style.min.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
