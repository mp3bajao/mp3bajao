
      <link rel="apple-touch-icon" href="../assets/images/logos/touch-icon-iphone.png">
      <link rel="apple-touch-icon" sizes="152x152" href="{{ URL::asset('frontend/assets/images/logos/touch-icon-ipad.png')}}">
      <link rel="apple-touch-icon" sizes="180x180" href="{{ URL::asset('frontend/assets/images/logos/touch-icon-iphone-retina.png')}}">
      <link rel="apple-touch-icon" sizes="167x167" href="{{ URL::asset('frontend/assets/images/logos/touch-icon-ipad-retina.png')}}">           
      <link href="{{ URL::asset('frontend/css/styles.bundle.css')}}" rel="stylesheet" type="text/css">
      <script src="{{ URL::asset('frontend/js/vendors.bundle.js')}}"></script>
      <script src="{{ URL::asset('frontend/js/scripts.bundle.js')}}"></script>
      <script>
      </script>


    <?php /*<script async src="https://www.googletagmanager.com/gtag/js?id=G-9F9W6HMJWQ"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-9F9W6HMJWQ');
        localStorage.setItem("listAudio",'');  
        var k = 0;   
        var myarray = [];
    </script>
    <script data-ad-client="ca-pub-9974854305750285" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>*/ ?>
@yield('css')
