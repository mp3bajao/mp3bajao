<?php 
    $parms = [];
    $result = apicurl('country','Basic',$parms);
    $country = $result['data'] ?? array();
?>
<footer id="footer" class="bg-img">
               <div class="footer-content">
                  <a href=""mailto:info@mp3bajao.com" class="email">info@mp3bajao.com</a>
                  <div class="platform-btn-inline">
                     <a href="https://play.google.com/store/apps/details?id=com.mp3bajao.mp3_bajao" class="btn btn-dark btn-air platform-btn">
                        <i class="ion-logo-android"></i>
                        <div class="platform-btn-info"><span class="platform-desc">Available on</span> <span class="platform-name">Android</span></div>
                     </a>
                     <a href="#" class="btn btn-danger btn-air platform-btn">
                        <i class="ion-logo-apple"></i>
                        <div class="platform-btn-info"><span class="platform-desc">Available on</span> <span class="platform-name">App Store</span></div>
                     </a>
                  </div>
               </div>
            </footer>
            
          
         </main>
         <?php 
            $parms = ['ip_address'=>$_SERVER['REMOTE_ADDR'],'limit'=>20];
            $data= apicurl('history','Basic',$parms);    
         ?>
    <aside id="rightSidebar">
            <div class="right-sidebar-header">Song Queue</div>
            <div class="right-sidebar-body" data-scrollable="true">
               <ul class="list-group list-group-flush">
 

                   @if(isset($data) && $data['status']==true)
                           
                   
                   @foreach($data['data'] as $key => $row)
                   @if($key==0)
                   <script>
                    var  autolist =   localStorage.getItem("listAudio");
                    var  newlist =  '{"id":"{!! ucwords($row->id) !!}","name":"{!! ucwords($row->name) !!}", "artist":"{!! ucwords($row->primary_artist) !!}", "album":"{!! ucwords($row->name) !!}", "url":"{!! $row->song !!}", "cover_art_url":"{!! $row->image !!}","duration":"{!! $row->duration !!}" }';
                    if(autolist!='')
                    {
                        localStorage.setItem("listAudio",autolist+','+newlist);                        
                    }
                    else
                    {
                        localStorage.setItem("listAudio",newlist);                        
                    }
                    </script>
                    @else
                    <script>
                    var  autolist =   localStorage.getItem("listAudio");
                    var  newlist =  '{"id":"{!! ucwords($row->id) !!}","name":"{!! ucwords($row->name) !!}", "artist":"{!! ucwords($row->primary_artist) !!}", "album":"{!! ucwords($row->name) !!}", "url":"{!! $row->song !!}", "cover_art_url":"{!! $row->image !!}","duration":"{!! $row->duration !!}" }';
                    localStorage.setItem("listAudio",autolist+','+newlist);                    
                    </script>
                   
                   @endif
                   
                   
                  <li class="custom-list--item list-group-item">
                     <div class="text-dark custom-card--inline amplitude-song-container amplitude-play-pause" data-amplitude-song-index="0" data-amplitude-playlist="special">
                         <div class="custom-card--inline-img">
                             <a href="javascript:void(0);" class="playsong"  data-audio='{"id":"{!! ucwords($row->id) !!}","name":"{!! ucwords($row->name) !!}", "artist":"{!! ucwords($row->primary_artist) !!}", "album":"{!! ucwords($row->name) !!}", "url":"{!! $row->song !!}", "cover_art_url":"{!! $row->image !!},"duration":"{!! $row->duration !!}" }'>
                               <img src="{{$row->image}}" alt="" class="card-img--radius-sm">
                            </a>
                        </div>
                        <div class="custom-card--inline-desc">
                           <a href="javascript:void(0);" class="playsong"  data-audio='{"id":"{!! ucwords($row->id) !!}","name":"{!! ucwords($row->name) !!}", "artist":"{!! ucwords($row->primary_artist) !!}", "album":"{!! ucwords($row->name) !!}", "url":"{!! $row->song !!}", "cover_art_url":"{!! $row->image !!}","duration":"{!! $row->duration !!}" }'>
                            <p class="text-truncate mb-0">{!! $row->name !!}</p>
                            <p class="text-truncate text-muted font-sm">{!! $row->primary_artist !!}</p>
                           </a>
                        </div>                       
                     </div>
                     <ul class="custom-card--labels d-flex ml-auto">
                        <li class="dropleft">
                           <a href="javascript:void(0);" class="btn btn-icon-only p-0 w-auto h-auto" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                           <ul class="dropdown-menu">                               
                                @if(!isset($userdata))
                                <li class="dropdown-item"><a href="javascript:void(0);"  data-toggle="modal" data-target="#singin" class="dropdown-link">
                                    <i class="la la-heart-o"></i> 
                                    <span>Favorite</span></a>
                                </li>
                                <li class="dropdown-item">
                                    <a href="javascript:void(0);"  data-toggle="modal" data-target="#singin" class="dropdown-link">
                                        <i class="la la-plus"></i> 
                                        <span>Add to Playlist</span>
                                    </a>
                                </li>
                                @else
                                <li class="dropdown-item">
                                    <a href="javascript:void(0);" class="dropdown-link add_favorite" data-track_id="{{$row->id}}" data-type="Track" data-status="1">
                                        <i class="la la-heart-o"></i>
                                        <span>Favorite</span></a>
                                    </a>
                                </li>                                    
                                <li class="dropdown-item">
                                    <a href="javascript:void(0);" class="dropdown-link add_playlist" data-track_id="{{$row->id}}"  data-type="Track" data-status="1" >
                                        <i class="la la-download"></i>
                                        <span>Add to Playlist</span>
                                    </a>
                                </li>
                                 
                              
                                @endif
                                <li class="dropdown-item">
                                        <a href="javascript:void(0);" class="dropdown-link add_share" data-track_id="{{$row->id}}"  data-type="Track" data-status="1">
                                            <i class="la la-share-alt"></i>
                                              <span>Share</span>
                                        </a>
                                </li>
                             
                              <!--li class="dropdown-item"><a href="javascript:void(0);" class="dropdown-link"><i class="la la-share-alt"></i> <span>Share</span></a></li-->
                              <li class="dropdown-item"><a href="{{route('song.details',['id'=>base64_encode($row->id)])}}" class="dropdown-link"><i class="la la-info-circle"></i> <span>Song Info</span></a></li>
                           </ul>
                        </li>
                     </ul>
                  </li>
                  @endforeach
                  @endif
                  
               </ul>
            </div>
         </aside>
    @include('layouts.frontend.audio-player' )  
      </div>
      
      
      <div class="modal fade" id="singin" tabindex="-1" role="dialog" aria-labelledby="signTitle" aria-hidden="true">
         <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
               <div class="modal-header" style="border-bottom:none;">
                  <div>
                     <h5 class="modal-title mb-1" id="langTitle">Log In</h5>
                  </div>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               </div>
                 <div class="alert-mssage"></div>
               <form method="post" action="{{url('api/login')}}" id="login" name="login" enctype="multipart/form-data">
               <div class="modal-body">
                   <div class="row">
                       <div class="col-md-12 loginscreen">                           
                           <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                  <span class="" id="inputGroup-sizing-default">
                                      <select name="country_code" class="form-control country_code" id="country_code">
                                        <option value="+91" >+91</option>                                          

                                      </select>
                                  </span>
                                </div>
                               <input type="text" class="form-control phone_number" name="mobile" placeholder="Mobile Number" id="phone_number" autocomplete="off" value="" onkeyup="return checkButtonActive(event);" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" maxlength="10" minlength="10" aclen="10">
                              
                              </div>
                           <div class="error" style="color: red;"></div>                           
                       </div>
                       <div class="col-md-12 text-center" style="padding-bottom: 10px;"  >
                       ----- OR -----
                       </div>
                       
                        <div class="col-md-12">
                            <div class="input-group mb-3">                           
                                <input type="email" class="form-control email_address" placeholder="Email" name="email" id="email">                              
                            </div>                      
                        </div> 
                        <div class="col-md-12">
                            <div class="input-group mb-3">
                                <input class="form-control password" name="password" type="password" autocomplete="new-password" placeholder="Password(6-15 alphanumerics)" maxlength="16" autocapitalize="none" data-pattern="[^0-9a-zA-Z]" data-flags="g" aria-autocomplete="list">
                            </div>
                        </div>
                       <div class="col-md-12">
                            <div class="input-group mb-3">
                                <a href="javascript:;" class="forgotpassword">Forgot password ? </a>
                            </div>
                      </div>
                    </div>
               </div>
               <div  class="modal-footer text-center d-block">
                   <button type="submit" class="btn btn-primary btn-pill ">Log In</button>
               </div>
        </form>
         </div>
      </div>
      </div>
      
      <div class="modal fade" id="singup" tabindex="-1" role="dialog" aria-labelledby="signTitle" aria-hidden="true">
         <div class="modal-dialog modal-md" role="document">
            <div class="modal-content ">
               <div class="modal-header" style="border-bottom:none;">
                  <div>
                     <h5 class="modal-title mb-1" id="langTitle">Sign up</h5>
                  </div>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               </div>
                <div class="alert-mssage"></div>
                <div class="signupscreen">
                    <form method="post" action="{{url('api/register')}}" id="retister" name="retister" enctype="multipart/form-data">
                <div class="modal-body">                    
                 <div class="row">
                    <div class="col-md-12">
                        <div class="input-group mb-3">                           
                            <input type="text" class="form-control" name="name" placeholder="User Name" id="name" >                              
                        </div>                      
                    </div>
                     <div class="col-md-12">
                        <div class="input-group mb-3">                           
                            <input type="email" class="form-control" placeholder="Email" name="email" id="email">                              
                        </div>                      
                    </div>
                     
                     <div class="col-md-12 loginscreen">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="" id="inputGroup-sizing-default">
                                      <select name="country_code" class="form-control country_code" id="country_code">
                                        <option value="+91" >+91</option>                                          

                                      </select>
                                </span>
                            </div>
                            <input type="text" name="mobile" class="form-control" placeholder="Mobile Number" id="phone_number" autocomplete="off" value="" onkeyup="return checkButtonActive(event);" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" maxlength="10" minlength="10" aclen="10">                              
                        </div>
                    <div class="error" style="color: red;"></div>                            
                    </div>
                     
                     <div class="col-md-12">
                        <div class="input-group mb-3">
                            <input class="form-control password" name="password" type="password" autocomplete="new-password" placeholder="Password(6-15 alphanumerics)" maxlength="16" autocapitalize="none" data-pattern="[^0-9a-zA-Z]" data-flags="g" aria-autocomplete="list">
                        </div>
                    </div>
                     <div class="col-md-12">
                        <div class="input-group mb-3">
                            <input class="form-control password_confirmation" name="password_confirmation" type="password" autocomplete="new-password" placeholder="Password(6-15 alphanumerics)" maxlength="16" autocapitalize="none" data-pattern="[^0-9a-zA-Z]" data-flags="g" aria-autocomplete="list">
                        </div>
                    </div>
                                          
                     <div class="col-md-12">
                        <div class="input-group mb-3">
                            <label class="checkbox checked">
                                <input name="accept" type="checkbox" value="T" required>
                                I have read and accept
                                <a href="#" rel="nofollow" target="_newBp" data-link="normal">Terms and Conditions</a>
                            </label>
                        </div>                      
                    </div>
                       
                       <div class="col-md-6 otpscreen" style="display:none;">
                           <div style="padding: 5%;" class="optscreenmessage"></div>  
                               <div class="input-group mb-3">
                                   <input type="text" class="form-control" id="user-otp" autocomplete="off" value="" onkeyup="return checkOTPButtonActive(event);" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" maxlength="6" style="letter-spacing: 8px; text-align: center;"> 
                           </div>
                            <div class="error-otp" style="color: red;"></div>                           
                       </div>
                       <div class="col-md-3"></div>
                   </div>
                  
               </div>
               <div class="modal-footer text-center d-block">
                   <button type="submit" class="btn btn-primary btn-pill formSignup">SIGN UP</button>
               </div>
            </form>
            </div>
            <div class="signupverify" style="display: none;">
                <form method="post" action="{{url('api/otpVerify')}}" id="otpverify" name="otpverify" enctype="multipart/form-data">
                    <div class="modal-body">                    
                        <div class="row">
                            <input class="new_mobile_number" name="mobile" type="hidden">
                            <input class="new_country_code" name="country_code" type="hidden">                    
                            <input class="new_email" name="email" type="hidden">                    
                            <div class="col-md-12">
                               <div class="input-group mb-3">
                                   <input type="text" class="form-control otp" name="otp" id="otp" autocomplete="off" onkeyup="return checkOTPButtonActive(event);" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" maxlength="6" style="letter-spacing: 8px; text-align: center;" placeholder="OTP CODE"> 
                               </div>
                           </div>
                        </div>
                    </div>
                    <div class="modal-footer text-center d-block">
                        <button type="submit" class="btn btn-primary btn-pill">Verify OTP</button>
                    </div>
                </form>
            </div>
            </div>
         </div>
      </div> 
      
      
      <div class="modal fade" id="forgot_password_modal" tabindex="-1" role="dialog" aria-labelledby="signTitle" aria-hidden="true">
         <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
               <div class="modal-header" style="border-bottom:none;">
                  <div>
                     <h5 class="modal-title mb-1" id="langTitle">Forgot Password</h5>
                  </div>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               </div>
                 <div class="alert-mssage"></div>
                 <div class="forgot_password_div">
                    <form method="post" action="{{url('api/forgot-password')}}" id="forgot_password" name="forgot_password" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="row">
                             <div class="col-md-12">
                                 <div class="input-group mb-3">                           
                                     <input type="email" class="form-control email_address" placeholder="Email" name="email" id="email">                              
                                 </div>                      
                             </div> 

                         </div>
                    </div>
                    <div  class="modal-footer text-center d-block">
                        <button type="submit" class="btn btn-primary btn-pill ">Send Password Reset OTP </button>
                    </div>
                 </form>
                </div>
                 
                 <div class="reset_password_div" style="display:none">
                    <form method="post" action="{{url('api/reset_password')}}" id="reset_password" name="reset_password" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="row">
                             <div class="col-md-12">
                                 <input type="hidden" class="form-control reset_email" name="email">
                                  <div class="col-md-12">
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control otp" name="otp" id="otp" autocomplete="off" onkeyup="return checkOTPButtonActive(event);" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" maxlength="6" style="letter-spacing: 8px; text-align: center;" placeholder="OTP CODE"> 
                                    </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="input-group mb-3">
                                            <input class="form-control password" name="password" type="password" autocomplete="new-password" placeholder="Password(6-15 alphanumerics)" maxlength="16" autocapitalize="none" data-pattern="[^0-9a-zA-Z]" data-flags="g" aria-autocomplete="list">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="input-group mb-3">
                                            <input class="form-control password_confirmation" name="password_confirmation" type="password" autocomplete="new-password" placeholder="Password(6-15 alphanumerics)" maxlength="16" autocapitalize="none" data-pattern="[^0-9a-zA-Z]" data-flags="g" aria-autocomplete="list">
                                        </div>
                                    </div>
                                 </div>                      
                             </div> 

                         </div>
                    
                    <div  class="modal-footer text-center d-block">
                        <button type="submit" class="btn btn-primary btn-pill ">Reset Password</button>
                    </div>
                 </form>
                     </div>
                </div>
                 
                 
         </div>
      </div>
      
      
      
       <div class="modal fade" id="playlistModal" tabindex="-1" role="dialog" aria-labelledby="signTitle" aria-hidden="true">
         <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
               <div class="modal-header" style="border-bottom:none;">
                  <div>
                     <h5 class="modal-title mb-1" id="langTitle">Play List</h5>
                  </div>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               </div>
                 <div class="alert-mssage"></div>
                 <div class="forgot_password_div">
                    <form method="post" action="{{url('api/add_playlist')}}" id="add_playlist" name="add_playlist" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="row">
                             <div class="col-md-12">
                                 <div class="input-group  mb-3 playlist_type_div" > 
                                     <select class="form-control" name="playlist_type" id="playlist_type">
                                         <option>-- Select Playlist -- </option>
                                         <option value="newplay">Create New Playlist</option>
                                     </select>
                                    
                                 </div>                      
                             </div>
                        
                             <div class="col-md-12 addplaylisttype" style="display: none;">
                                 <div class="input-group mb-3"> 
                                     <input type="text" class="form-control" name="playlist_type_name" placeholder="+ Add New">
                                 </div>                      
                             </div>
                             <input type="hidden" class="form-control playlist_track_id" name="track_id">

                         </div>
                    </div>
                    <div  class="modal-footer text-center d-block">
                        <button type="submit" class="btn btn-primary btn-pill ">Add Play List</button>
                    </div>
                 </form>
                </div>
                </div>
                 
                 
         </div>
      </div>
      
      
        <div class="modal fade" id="ShareModal" tabindex="-1" role="dialog" aria-labelledby="signTitle" aria-hidden="true">
         <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
               <div class="modal-header" style="border-bottom:none;">
                  <div>
                     <h5 class="modal-title mb-1" id="langTitle">Share</h5>
                  </div>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               </div>
                 <div class="forgot_password_div">                    
                    <div class="modal-body">
                        <div class="alert-mssage"></div>
                        <table class="table table-striped" style="color: #ffffff; font-size: 15px; "
                            <tr>
                                <td>
                                    <span class="maleToShare"><i class="la la-envelope"></i> &nbsp; Share on Mail</span>
                                </td>
                                </tr>
                                   <tr>
                                <td>
                                    <span class="facebookToShare"><i class="la la-facebook"></i> &nbsp; Share in Facebook</span>
                                </td>
                                 </tr>
                                   <tr>
                                <td>
                                    <span class="twitterToShare"><i class="la la-twitter"></i> &nbsp; Share in Twitter</span>
                                </td>
                                   </tr><tr>
                                <td>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                           <span class="input-group-text copylink"><i class="la la-link "></i> &nbsp; Copy</span>
                                        </div>
                                        <input type="hidden"  class="form-control sharetrackname" id="sharetrackname" value=""/>
                                        <input type="text"  class="form-control sharelinkbox" id="sharelinkbox" value=""/>
                                        <input type="hidden"  class="form-control sharetrackimage" id="sharetrackimage" value=""/>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>                    
                </div>
            </div>
         </div>
        </div>
      
      
      </div>
      
      
      <div class="backdrop header-backdrop"></div>
      <div class="backdrop sidebar-backdrop"></div>