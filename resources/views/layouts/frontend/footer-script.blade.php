
 <?php $userdata =  Session::get('userData');?>

@if(!isset($userdata))
<script>
$(document).ready(function(){
    $(document).on('click','.radio_action',function(){
        parmis = 'radio';
        var url = "{{url('/')}}/"+parmis;
        window.location.href = url;
    })
    
    $(document).on('click','.singin',function(){
        $('.alert-mssage').html('');
        $('#singin').modal('show');
    });
    $(document).on('click','.forgotpassword',function(){
        $('.alert-mssage').html('');
        $('#singin').modal('hide');
        $('#forgot_password_modal').modal('show');
    });
    
    $(document).on('click','.singup',function(){
        $('.alert-mssage').html('');
        $('#singup').modal('show');
        $('.signupscreen').show();
        $('.signupverify').hide();
    });

    $("#login").submit(function(e) {
        e.preventDefault();
        var phone_number = $('.phone_number').val();
        var country_code = $('.country_code').val();  
        var email = $('.email_address').val();  
        
        if(phone_number != '' || email != '')
        {
            var form = new FormData(this);
            form.append("device_type", "Android");
            form.append("device_id", "123123123");

            var settings = {
              "async": true,
              "crossDomain": true,
              "url": "{{url('api/login')}}",
              "method": "POST",
              "headers": {
                "authorization": "Basic bXAzYmFqYW86bXAzYmFqYW9AMjAyMA==",
                "cache-control": "no-cache",
                "postman-token": "b5da91ac-c838-f93e-3e01-5e81b1788bc6"
              },
              "processData": false,
              "contentType": false,
              "mimeType": "multipart/form-data",
              "data": form
            }

            $.ajax(settings).done(function (response) {
              var response = JSON.parse(response);
                if(response.status==true)
                {
                    $('.alert-mssage').html('<div class="alert alert-success">'+response.message+'</div>');
                    localStorage.setItem("token",response.data.token);                    
                    var token = response.data.token;
                    document.cookie = "token="+token;
                    setTimeout(function(){
                        $('.alert-mssage').html('');
                        $('#singin').modal('hide');
                        //$('#singin').html('');
                        $('.singin').hide();
                        location.reload();
                    },1000); 
                    
                }
                else
                {                    
                     $('.alert-mssage').html('<div class="alert alert-danger">'+response.message+'</div>'); 
                }
            });    
        }
        else
        {
            $('.alert-mssage').html('<div class="alert alert-danger">Please enter mobile number or email address</div>'); 
        }
    });
        
    $("#retister").submit(function(e) {
        e.preventDefault();
        $('#prossing').show();
        var form = new FormData(this);
        form.append("device_type", "Android");
        form.append("device_id", "123123");
        var settings = {
          "async": true,
          "crossDomain": true,
          "url": "{{url('/api/register')}}",
          "method": "POST",
          "headers": {
            "authorization": "Basic bXAzYmFqYW86bXAzYmFqYW9AMjAyMA==",
            "accept": "application/json",
            "cache-control": "no-cache",
            "postman-token": "f3dfb89e-997d-0b21-1ea8-4a24b1b218d9"
          },
          "processData": false,
          "contentType": false,
          "mimeType": "multipart/form-data",
          "data": form
        }
        
        $.ajax(settings).done(function (response) {
             var response = JSON.parse(response);
              if(response.status==true)
              {
                $('.alert-mssage').html('<div class="alert alert-success">'+response.message+'</div>');
                $('.signupscreen').hide();
                
                $('.new_mobile_number').attr('value',response.data.mobile);
                $('.new_country_code').attr('value',response.data.country_code);
                $('.new_email').attr('value',response.data.email);
                setTimeout(function(){
                    $('.alert-mssage').html('');
                    $('.signupverify').show();
                },1000);                
              }
              else
              {
                $('.alert-mssage').html('<div class="alert alert-danger">'+response.message+'</div>'); 
              }
              $('#prossing').hide();
        }); 
    });   
    
    $("#otpverify").submit(function(e) {
        e.preventDefault();
         $('#prossing').show();
        var form = new FormData(this);
        form.append("password",$('.password_confirmation').val());
        form.append("device_type", "Android");
        form.append("device_id", "123123123");
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "{{url('api/otpVerify')}}",
            "method": "POST",
            "headers": {
               "authorization": "Basic bXAzYmFqYW86bXAzYmFqYW9AMjAyMA==",
               "cache-control": "no-cache",
               "postman-token": "b5da91ac-c838-f93e-3e01-5e81b1788bc6"
            },
            "processData": false,
            "contentType": false,
            "mimeType": "multipart/form-data",
            "data": form
        }
        $.ajax(settings).done(function (response) {
          var response = JSON.parse(response);
            if(response.status==true)
            {
                $('.alert-mssage').html('<div class="alert alert-success">'+response.message+'</div>'); 
                localStorage.setItem("token",response.data.token);                    
                var token = response.data.token;
                document.cookie = "token="+token;    
                setTimeout(function(){
                    $('.alert-mssage').html('');
                    $('#singin').modal('hide');
                    $('#singin').html('');
                    $('.singin').hide();
                    location.reload();
                },1000);       
            }
            else
            {                    
               $('.alert-mssage').html('<div class="alert alert-danger">'+response.message+'</div>'); 
            }
            $('#prossing').hide();
        });
    
    });
        
    $("#forgot_password").submit(function(e) {
        e.preventDefault();
         $('#prossing').show();
        var form = new FormData(this);
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "{{url('api/forgot_password')}}",
            "method": "POST",
            "headers": {
               "authorization": "Basic bXAzYmFqYW86bXAzYmFqYW9AMjAyMA==",
               "cache-control": "no-cache",
               "postman-token": "b5da91ac-c838-f93e-3e01-5e81b1788bc6"
            },
            "processData": false,
            "contentType": false,
            "mimeType": "multipart/form-data",
            "data": form
        }
        $.ajax(settings).done(function (response) {
          var response = JSON.parse(response);
            if(response.status==true)
            {
                $('.reset_email').attr('value',response.data.email);    
                $('.alert-mssage').html('<div class="alert alert-success">'+response.message+'</div>');                
                setTimeout(function(){                    
                    $('.forgot_password_div').hide();
                    $('.reset_password_div').show();
                    $('.alert-mssage').html('');
                },1000);       
            }
            else
            {                    
               $('.alert-mssage').html('<div class="alert alert-danger">'+response.message+'</div>'); 
            }
             $('#prossing').hide();
        });
    
    });
    
    $("#reset_password").submit(function(e) {
        e.preventDefault();
         $('#prossing').hide();
        var form = new FormData(this);
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "{{url('api/reset_password')}}",
            "method": "POST",
            "headers": {
               "authorization": "Basic bXAzYmFqYW86bXAzYmFqYW9AMjAyMA==",
               "cache-control": "no-cache",
               "postman-token": "b5da91ac-c838-f93e-3e01-5e81b1788bc6"
            },
            "processData": false,
            "contentType": false,
            "mimeType": "multipart/form-data",
            "data": form
        }
        $.ajax(settings).done(function (response) {
          var response = JSON.parse(response);
            if(response.status==true)
            {
                $('.reset_email').attr('value',response.data.email);    
                $('.alert-mssage').html('<div class="alert alert-success">'+response.message+'</div>');                
                setTimeout(function(){
                   
                    $('#forgot_password_modal').modal('hide');
                     $('.alert-mssage').html('');
                },1000);       
            }
            else
            {                    
               $('.alert-mssage').html('<div class="alert alert-danger">'+response.message+'</div>'); 
            }
             $('#prossing').hide();
        });
    
    });
    
    
});




/*
$(document).on('click','.formSignup',function(){
  
});

*/

</script>
@else

<script>

$(document).on('click','.logout',function(){
    setTimeout(function(){
       location.href="{{route('home')}}";
    },500);
    /*var authtoken = $(this).data('token');
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "{{url('api/logout')}}",
      "method": "POST",
      "headers": {
        "authorization": "Bearer "+authtoken,
        "cache-control": "no-cache",
        "postman-token": "ec37f0d4-da79-d92f-97ba-cbd5811dbd94"
      }
    }

    $.ajax(settings).done(function (response) {
        var abc ="{{Session::reflash()}} {{Session::forget('userData')}}";        
       location.reload();  
    });*/
});

$(document).on('click','.add_favorite',function(){    
    var authtoken = $('.logout').data('token');
    var track_id  = $(this).data('track_id');
    var status  = $(this).data('status');
    var type  = $(this).data('type');
    var form = new FormData();
    form.append("track_id", track_id);
    form.append("status", status);
    form.append("type", type);

    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "{{url('api/add_favorite')}}",
      "method": "POST",
      "headers": {
        "authorization": "Bearer "+authtoken,
        "cache-control": "no-cache",
        "postman-token": "a4d21446-9804-db1e-0143-9d44649d7a91"
      },
      "processData": false,
      "contentType": false,
      "mimeType": "multipart/form-data",
      "data": form
    }

    $.ajax(settings).done(function (response) {
        var response = JSON.parse(response);
        if(response.status==true)
        {
            if(status==1)
            {
                $('.add_favorite_status_'+track_id).data('status',0);
                $('.'+type+'_'+track_id).addClass('la-heart');
                $('.'+type+'_'+track_id).removeClass('la-heart-o');              
            }
            else
            {
                 $('.add_favorite_status_'+track_id).data('status',1);
                $('.'+type+'_'+track_id).removeClass('la-heart');
                $('.'+type+'_'+track_id).addClass('la-heart-o'); 
            }
        }
    });
});

$(document).on('change','#playlist_type',function(){
    var types = $(this).val();
    if(types == 'newplay')
    {
       $('.addplaylisttype').show(); 
    }
});

$(document).on('click','.add_playlist, .play_add_playlist',function(){    
    var authtoken = $('.logout').data('token');
    var track_id  = $(this).data('track_id');
    var status  = $(this).data('status');
    var authtoken = $('.logout').data('token');
    $('.playlist_track_id').attr('value',track_id);
    
    
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "{{url('/api/playlist_type')}}",
        "method": "POST",
        "headers": {
          "authorization": "Bearer "+authtoken,
          "cache-control": "no-cache",
          "postman-token": "4c458d55-c1bc-a0e2-a2a7-ef33bd5a0d7f"
        }
    }

    $.ajax(settings).done(function (response) {
       
        var optionhtml = '<select class="form-control" name="playlist_type" id="playlist_type"><option>-- Select Playlist -- </option>';
        if(response.status==true)
        {
          response.data.map(function(data){
               optionhtml = optionhtml+ '<option value="'+data.id+'">'+data.name+'</option>';
          })
        }        
         optionhtml = optionhtml+'<option value="newplay">Create New Playlist</option></select>';
        $('.playlist_type_div').html(optionhtml);
    });
    $('#playlistModal').modal('show');
});


$(document).ready(function(){
    $("#add_playlist").submit(function(e) {
        e.preventDefault();
        var authtoken = $('.logout').data('token');
        var form = new FormData(this);
        form.append("status", "0");
        var settings = {
          "async": true,
          "crossDomain": true,
          "url": "{{url('api/add_playlist')}}",
          "method": "POST",
          "headers": {
            "authorization": "Bearer "+authtoken,
            "cache-control": "no-cache",
            "postman-token": "d309a457-dbb8-e287-b96b-322eeff2f0f2"
          },
          "processData": false,
          "contentType": false,
          "mimeType": "multipart/form-data",
          "data": form
        }

        $.ajax(settings).done(function (response) {
          var response = JSON.parse(response);
              if(response.status==true)
              {
                $('.alert-mssage').html('<div class="alert alert-success">'+response.message+'</div>');
                setTimeout(function(){
                    $('.alert-mssage').html('');
                    $('#playlistModal').modal('hide');
                },1000);                
              }
              else
              {
                $('.alert-mssage').html('<div class="alert alert-danger">'+response.message+'</div>'); 
              }
        });
        
    });
}); 



$(document).on('click','.add_download',function(){    
    var authtoken = $('.logout').data('token');
    var track_id  = $(this).data('track_id');
});



$(document).on('mouseover','.addRating',function(){    
    var authtoken = $('.logout').data('token');
    var track_id  = $(this).data('track_id');
    var values = $(this).data('value');
    var type = $(this).data('type');
    var status = 1;
    $('.rating_num').removeClass('s0');
    $('.rating_num').removeClass('s10');
    $('.rating_num').removeClass('s20');
    $('.rating_num').removeClass('s30');
    $('.rating_num').removeClass('s40');
    $('.rating_num').removeClass('s50');
    $('.rating_num').addClass('s'+values+'0');

    var form = new FormData();
    form.append("track_id", track_id);
    form.append("status", status);
    form.append("type", type);
    form.append("rating", values);

    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "{{url('api/rating')}}",
      "method": "POST",
      "headers": {
        "authorization": "Bearer "+authtoken ,
        "cache-control": "no-cache",
        "postman-token": "8e176f11-a300-ce3b-d29d-0b35fca561c0"
      },
      "processData": false,
      "contentType": false,
      "mimeType": "multipart/form-data",
      "data": form
    }

    $.ajax(settings).done(function (response) {
      console.log(response);
    });
});


</script>

@endif
<script>
$(document).on('click','.add_share',function(){   
    var authtoken = $('.logout').data('token');
    var track_id  = $(this).data('track_id');
    var track_type  = $(this).data('type');
    var track_name  = $(this).data('track_name');
    
    var form = new FormData();
    form.append("type", track_type);
    form.append("type_id", track_id);
    form.append("ip_address", "{{$_SERVER['REMOTE_ADDR']}}");

    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "{{url('api/sharing')}}",
      "method": "POST",
      "headers": {
        "authorization": "Basic bXAzYmFqYW86bXAzYmFqYW9AMjAyMA==",
        "cache-control": "no-cache",
        "postman-token": "f63158c1-f9cc-2e25-234f-c90b2b88efc0"
      },
      "processData": false,
      "contentType": false,
      "mimeType": "multipart/form-data",
      "data": form
    }

    $.ajax(settings).done(function (response) {
      console.log(response);
    });    
        
        
    if(track_type=='Artist')
    {
        shareurl = "{{route('artist.details')}}";
        $('.sharelinkbox').attr('value',shareurl+'/'+window.btoa(track_id));
    }
    else
    {
       shareurl = "{{route('song.details')}}";
        $('.sharelinkbox').attr('value',shareurl+'/'+window.btoa(track_id)+'/'+track_type);  
    }
    $('.sharetrackname').attr('value',track_name);
    $('#ShareModal').modal('show');    
//    var win = window.open('mailto:?subject=Listen%20to%20Main%20Kisi%20Aur%20Ka%20on%20gaana.&body=https://gaana.com/song/main-kisi-aur-ka', '_blank');
//  win.focus();
});

$(document).on('click','.maleToShare',function(){
    var name = $('.sharetrackname').val();
    var url = $('.sharelinkbox').val();
    var win = window.open('mailto:?subject='+name+'&body='+url, '_blank');
    win.focus();
});

$(document).on('click','.facebookToShare',function(){
   var url = $('.sharelinkbox').val();
    var win = window.open('https://www.facebook.com/sharer.php?u='+url, 'Mp3bajao',"width=500,height=350");
    //win.focus(); 
});
$(document).on('click','.twitterToShare',function(){
   var name = $('.sharetrackname').val();
   var url = $('.sharelinkbox').val();
   var shareurl = 'https://twitter.com/share?url='+url+'&text='+name+'&hashtags=nowplaying&via=mp3bajao';
   var win = window.open(shareurl, 'Mp3bajao',"width=500,height=350");
});



$(document).on('click','.copylink',function(){
  $('.alert-mssage').html('');
  var copyText = document.getElementById("sharelinkbox");
  copyText.select();
  copyText.setSelectionRange(0, 99999)
  document.execCommand("copy");
    $('.alert-mssage').html('<div class="alert alert-success">Copied URL.</div>'); 
    

});


    
$(document).ready(function(){
    var url = "{{route('searching')}}";
    $('.search_listing').load(url);     
    $(document).on('keyup','#searchInput',function(){
        var search = $(this).val();         
        var url = "{{route('searching')}}";
        url=url+'/'+search.replaceAll(' ','+');
        $('.search_listing').load(url);            
    }); 
});


</script>

