<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <!-- Mirrored from www.kri8thm.in/html/listen/theme/demo/home.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 09 Nov 2020 02:06:41 GMT -->
   <head>
       <title>@yield('title') Download Latest MP3 Songs Online: Play Old & New MP3 Music Online Free on Mp3bajao.com</title>
    <meta charset="utf-8" /> 
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta name="description" content="@yield('description') Mp3bajao.com- Listen & Download latest MP3 songs online. Play Online Radio-Bajao Radio,Download new or old Hindi songs, Bollywood songs, English songs* & more on Mp3bajao+ and play offline. Create, share and listen to streaming music playlists for free." />
    <meta name="keywords" content="@yield('keyword') songs, music, songs online, listen songs, MP3 songs, listen to free online songs, online latest songs, free music, popular songs, movie songs, mp3bajao.com, listen to songs online, listen to free music, online music, free music online, latest movie songs, hindi songs, play songs, online songs, hindi songs, Bhojpuri Music, Panjabi Music, Rajasthani Music,music albums, free music, online music, english songs, hollywood songs, playlists music, music online" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">        
    <link rel="shortcut icon" href="{{ URL::asset('assets/images/favicon.ico')}}">

    @yield('metadata')
     <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>

            <script>
                $(document).ready(function (){
                    //alert(navigator.userAgent.toLowerCase().indexOf("android"));
                    if(navigator.userAgent.toLowerCase().indexOf("android") > -1){
                        window.location.href = 'https://play.google.com/store/apps/details?id=com.mp3bajao.mp3_bajao';
                    }
                    if(navigator.userAgent.toLowerCase().indexOf("iphone") > -1){
                      //  window.location.href = 'http://itunes.apple.com/lb/app/truecaller-caller-id-number/id448142450?mt=8';
                    }
                });
            </script>
      @include('layouts.frontend.head')
   </head>
   
   <body oncontextmenu="return false;"   class="theme-dark">
       <div id="loading">
          
         <div class="loader">
              <img src="{{asset('assets/images/logo2.webp')}}"  alt="Mp3bajao">
            <div class="eq">
                <span class="eq-bar eq-bar--1"></span> 
                <span class="eq-bar eq-bar--2"></span> 
                <span class="eq-bar eq-bar--3"></span> 
                <span class="eq-bar eq-bar--4"></span> 
                <span class="eq-bar eq-bar--5"></span> 
                <span class="eq-bar eq-bar--6"></span>
                <span class="eq-bar eq-bar--7"></span>
                <span class="eq-bar eq-bar--8"></span>
                <span class="eq-bar eq-bar--9"></span>
                <span class="eq-bar eq-bar--10"></span>
                <span class="eq-bar eq-bar--11"></span>
                <span class="eq-bar eq-bar--12"></span>
            </div>
            <span class="text">Loading</span>
         </div>
      </div>
       <div id="prossing">
         <div class="loader">
              <img src="{{asset('assets/images/logo2.webp')}}"  alt="Mp3bajao">
            <div class="eq"><span class="eq-bar eq-bar--1"></span> <span class="eq-bar eq-bar--2"></span> <span class="eq-bar eq-bar--3"></span> <span class="eq-bar eq-bar--4"></span> <span class="eq-bar eq-bar--5"></span> <span class="eq-bar eq-bar--6"></span>
            <span class="eq-bar eq-bar--7"></span>
                <span class="eq-bar eq-bar--8"></span>
                <span class="eq-bar eq-bar--9"></span>
                <span class="eq-bar eq-bar--10"></span>
                <span class="eq-bar eq-bar--11"></span>
                <span class="eq-bar eq-bar--12"></span>
            </div>
            <span class="text">Loading</span>
         </div>
      </div>
      <div id="wrapper" data-scrollable="true">
          
       
        @include('layouts.frontend.topbar')
        @include('layouts.frontend.sidebar')
        <div class="container-fluid">
            @include('layouts.frontend.errors')
            <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
                <div class="container-fluid">
                     <div class="main-container" id="appRoute">
                    @yield('content')
                </div>
                </div>
            </div>
        </div>
        <footer class="footer">
        </footer>
    </div>

     @yield('script')
     
<script language="JavaScript">
    setTimeout(function(){  
        $('.carousel-item-6').css("display", "block");
        $('#loading').hide(); 
    }, 1500);
    
  /*
    * Disable right-click of mouse, F12 key, and save key combinations on page
    * By Arthur Gareginyan (https://www.arthurgareginyan.com)
    * For full source code, visit https://mycyberuniverse.com
    *
    "*/
  window.onload = function() {
    document.addEventListener("contextmenu", function(e){
      e.preventDefault();
    }, false);
    document.addEventListener("keydown", function(e) {
    //document.onkeydown = function(e) {
      // "I" key
      if (e.ctrlKey && e.shiftKey && e.keyCode == 73) {
        disabledEvent(e);
      }
      // "J" key
      if (e.ctrlKey && e.shiftKey && e.keyCode == 74) {
        disabledEvent(e);
      }
      // "S" key + macOS
      if (e.keyCode == 83 && (navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)) {
        disabledEvent(e);
      }
      // "U" key
      if (e.ctrlKey && e.keyCode == 85) {
        disabledEvent(e);
      }
      // "F12" key
      if (event.keyCode == 123) {
        disabledEvent(e);
      }
    }, false);
    function disabledEvent(e){
      if (e.stopPropagation){
        e.stopPropagation();
      } else if (window.event){
        window.event.cancelBubble = true;
      }
      e.preventDefault();
      return false;
    }
  };
</script>


  
</body>
</html>