 <?php $userdata =  Session::get('userData');?>
<aside id="sidebar" class="sidebar-primary">
            <div class="sidebar-header d-flex align-items-center">
                <a href="{{url('/')}}" class="brand">
                    <img src="{{ URL::asset('assets/images/logo2.png')}}" style="padding: 15px;" alt="Mp3bajao"> </a>
                    <button type="button" class="btn p-0 ml-auto" id="hideSidebar">
                        <i class="ion-md-arrow-back h3"></i>
                    </button> 
                    <button type="button" class="btn toggle-menu" id="toggleSidebar">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
            </div>
            <nav class="navbar">
               <ul class="navbar-nav" data-scrollable="true">
                  <li class="nav-item nav-header">Browse Music</li>
                  <li class="nav-item">
                      <a href="{{route('home')}}"  class=" nav-link active">
                          <i class="la la-home"></i>
                          <span>Home</span>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a href="{{route('genres')}}" data-href="genres" class="nav-link">
                          <i class="la la-diamond"></i>
                          <span>Genres</span>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a href="{{route('song')}}" class="nav-link">
                          <i class="la la-music"></i>
                          <span>Music</span>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a href="{{route('artist')}}" class="nav-link">
                          <i class="la la-microphone"></i>
                          <span>Artists</span>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a href="javascript:;" class="radio_action nav-link">
                          <i class="la la-bullseye"></i>
                          <span>Radio</span>
                      </a>
                  </li>
                  
                  <li class="nav-item nav-header">Your Music</li>
                  <!--li class="nav-item">
                      <a href="analytics.html" class="nav-link">
                          <i class="la la-bar-chart"></i>
                          <span>Analytics</span>
                      </a>
                  </li-->
               
                  <li class="nav-item">
                      @if(!isset($userdata))
                      <a href="javascript:void(0);" data-toggle="modal" data-target="#singin" class="nav-link">
                      @else
                      <a href="{{route('playlist')}}" class="nav-link">
                      @endif
                          <i class="la la-dropbox"></i>
                          <span>Playlist</span>
                      </a>
                  </li>
                  <li class="nav-item">
                      @if(!isset($userdata))
                      <a href="javascript:void(0);" data-toggle="modal" data-target="#singin" class="nav-link">
                      @else
                        <a href="{{route('favorites')}}" class="nav-link">
                      @endif
                    
                          <i class="la la-heart-o"></i>
                          <span>Favorites</span>
                      </a>
                  </li>
                  <li class="nav-item">                      
                        <a href="{{route('history')}}" class="nav-link">                      
                          <i class="la la-history"></i>
                          <span>History</span>
                      </a>
                  </li>
                  <li  class="nav-item">
                 <?php /*<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-9974854305750285"
                        crossorigin="anonymous"></script>
                   <!-- 200-200 -->
                   <ins class="adsbygoogle"
                        style="display:inline-block;width:100%;height:350px"
                        data-ad-client="ca-pub-9974854305750285"
                        data-ad-slot="5112939651"></ins>
                   <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                   </script>
                   */?>
                  </li>
                  
                  <!--li class="nav-item nav-header">Music Events</li>
                  <li class="nav-item">
                      <a href="events.html" class="nav-link">
                          <i class="la la-calendar"></i>
                          <span>Events</span>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a href="event-details.html" class="nav-link">
                          <i class="la la-newspaper-o"></i>
                          <span>Event Details</span>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a href="add-event.html" class="nav-link">
                          <i class="la la-pencil-square-o"></i>
                          <span>Add Event</span>
                      </a>
                  </li>
                  <li class="nav-item nav-header">Extra Pages</li>
                  <li class="nav-item">
                      <a href="error.html" class="nav-link load-page">
                          <i class="la la-times-circle-o"></i>
                          <span>Error</span>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a href="blank.html" class="nav-link">
                          <i class="la la-file"></i>
                          <span>Blank</span>
                      </a>
                  </li-->
                 
               </ul>
            </nav>
            <!--div class="sidebar-footer">
                <a href="add-music.html" class="btn btn-block btn-danger btn-air btn-bold">
                    <i class="ion-md-musical-note"></i> 
                    <span>Add Music</span>
                </a>
                </div-->
          

         </aside>
         <main id="pageWrapper">            
            <div class="banner bg-home"></div>