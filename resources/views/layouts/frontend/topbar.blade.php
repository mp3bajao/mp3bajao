<header id="header" class="bg-primary">
               <div class="d-flex align-items-center">
                  <button type="button" class="btn toggle-menu mr-3" id="openSidebar"><span></span> <span></span> <span></span></button>
                      <form action="{{route('song')}}" id="searchForm" method="get">                          
                     <button type="button" class="btn ion-ios-search"></button> 
                     <input type="text" name="search" placeholder="Search..." id="searchInput" class="form-control" value="<?php echo $_GET['search'] ?? null ?>">
                     <div class="search_listing"></div>
                  </form>
                  
                  
                  
                  <ul class="header-options d-flex align-items-center">
                     <!--li><a href="javascript:void(0);" data-toggle="modal" data-target="#lang" class="language"><span>Language</span> <img src="https://www.kri8thm.in/html/listen/theme/assets/images/svg/translate.svg" alt=""></a></li-->
                     <?php $userdata =  Session::get('userData');
                     ?>
                      
                      @if(!isset($userdata))
                     <li>
                         <a href="javascript:void(0);"  class="singin">
                             <span>SIGN IN</span>
                         </a>
                     </li>
                     <li>
                         <a href="javascript:void(0);"  class="singup">
                             <span>SIGN UP</span>
                         </a>
                     </li>
                     @else
                     <li class="dropdown fade-in">
                        <a href="javascript:void(0);" class="d-flex align-items-center py-2" role="button" id="userMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                           <div class="avatar avatar-sm avatar-circle"><img src="{{$userdata->image }}" alt="user"></div>
                           <span class="pl-2">{{ucfirst($userdata->name) }}</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userMenu">
                           <a class="dropdown-item" href="profile"><i class="ion-md-contact"></i> <span>Profile</span></a> 
                           <!--a class="dropdown-item" href="plan.html"><i class="ion-md-radio-button-off"></i> <span>Your Plan</span></a>
                           <a class="dropdown-item" href="settings.html"><i class="ion-md-settings"></i> <span>Settings</span></a>
                           <div class="dropdown-divider"></div-->
                           <div class="px-4 py-2"><a href="{{route('logout')}}" class="logout btn btn-sm btn-air btn-pill btn-danger" data-token="{{isset($_COOKIE['token'])? $_COOKIE['token']:null}}">Logout</a></div>
                        </div>
                     </li>
                     @endif
                  </ul>
               </div>
            </header>