<?php 
use Illuminate\Support\Facades\Route;   
$routename = Route::currentRouteName();
?>
@if($routename!='radio')
 <div id="audioPlayer" class="player paused player-primary">
               <!--div id="progress-container">
                   <span style="color:red;" class="progress"><span></span></span
                   <input type="range" class="amplitude-song-slider">
                   <progress class="audio-progress audio-progress--played amplitude-song-played-progress"></progress>
                   <progress class="audio-progress audio-progress--buffered amplitude-buffered-progress" value="0"></progress>
               </div-->
     
                <div id="myProgress">
             <div id="myBar"></div>
           </div>
                 
               <div class="audio">
                  <div class="song-image album_image">

                  </div>
                  <div class="song-info pl-3">
                      <span class="song-name d-inline-block text-truncate" ></span>
                      <span class="song-artists d-block text-muted" ></span></div>
               </div>
               <div class="audio-controls">
                  <!--div class="audio-controls--left d-flex mr-auto">
                      <button class="repeat btn btn-icon-only amplitude-repeat">
                          <i class="ion-md-sync"></i>
                      </button>
                  </div-->
                  <div class="audio-controls--main d-flex">
                     
                      <button class="btn btn-icon-only amplitude-prev button rw" onclick="previous()">
                          <i class="ion-md-skip-backward"></i>
                      </button> 
                      
                      <button class="btn btn-icon-only amplitude-prev button rw"  onclick="rewind()">
                          <i class="ion-md-rewind"></i>
                      </button> 
                       
                      
          
                      <button class="btn btn-air btn-pill btn-default btn-icon-only amplitude-play-pause amplitude-playing playpusebutton button play-pause" onclick="toggleAudio()">
                          <span clsss="playpause" id="btn-faws-play-pause">
                            <i class='ion-md-play' id="icon-play"></i>
                            <i class='ion-md-pause' id="icon-pause" style="display: none"></i>
                           </span>
                      </button> 
                      <button class="btn btn-icon-only amplitude-next button ff" onclick="forward()">
                          <i class="ion-md-fastforward"></i>
                      </button>
                      <button class="btn btn-icon-only amplitude-next button ff" onclick="next()">
                          <i class="ion-md-skip-forward"></i>
                      </button>
                  </div>
                   
                  <!--div class="audio-controls--right d-flex ml-auto">                    
                      <button class="btn btn-icon-only amplitude-shuffle amplitude-shuffle-off shuffle">
                          <i class="ion-md-shuffle"></i>
                      </button>
                  </div-->
               </div>
               <div class="audio-info d-flex align-items-center pr-4">
                  <span class="mr-4">
                      <span class="current-time timer">0:00</span>                     
                      / 
                      <span class="duration">0:00</span>                         
                  </span>
                   
                   
                  <div class="audio-volume dropdown">
          
                      
                     <button class="btn btn-icon-only" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i id="icon-vol-up" class='ion-md-volume-high'></i>
                          <i id="icon-vol-mute" class='ion-md-volume-off' style="display: none"></i>
                     </button>
                     <div class="dropdown-menu dropdown-menu-right volume-dropdown-menu">
                             <input  type="range" min="0" max="100" value="50" class="amplitude-volume-slider volume-slider" id="skeakervolumeRange">
                     </div>
                  </div>
                  <div class="dropleft">
                     <button class="btn btn-icon-only" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                         <i class="la la-ellipsis-v"></i>
                     </button>
                     <ul class="dropdown-menu">
                        <li class="dropdown-item">
                            <a href="javascript:void(0);" class="dropdown-link play_add_favorite" data-type="Track" data-status="1">
                                <i class="la la-heart-o"></i> 
                                <span>Favorite</span>
                            </a>
                        </li>
                        <li class="dropdown-item">
                            <a href="javascript:void(0);" class="dropdown-link play_add_playlist" data-type="Track" data-status="1">
                                <i class="la la-plus"></i> 
                                <span>Add to Playlist</span>
                            </a>
                        </li>
                        <li class="dropdown-item">
                            <a href="javascript:void(0);" class="dropdown-link play_add_download" data-type="Track" data-status="1">
                                <i class="la la-download"></i> 
                                <span>Download</span>
                            </a>
                        </li>
                        <!--li class="dropdown-item">
                            <a href="javascript:void(0);" class="dropdown-link" share>
                                <i class="la la-share-alt"></i> 
                                <span>Share</span></a>
                        </li>
                        <li class="dropdown-item">
                            <a href="song-details.html" class="dropdown-link" songinfo>
                                <i class="la la-info-circle"></i> 
                                <span>Song Info</span>
                            </a>
                        </li-->
                     </ul>
                  </div>
                  <button class="btn btn-icon-only" id="playList">
                      <i class="ion-md-musical-note"></i>
                  </button>
               </div>

        <audio id="myAudio" ontimeupdate="onTimeUpdate()">
          <!-- <source src="audio.ogg" type="audio/ogg"> -->
          <source id="source-audio" src="" type="audio/mpeg">
          Your browser does not support the audio element.
        </audio>

  
 
</div>
  <div class="playlist-ctn" style="padding-left: 450px; display: none;"></div>
<style>

#myProgress {
  width: 100%;
   background-color: #79828c; 
  cursor: pointer;
  border-radius: 10px;
}

#myBar {
  width: 0%;
  height: 5px;
  background-color: #753fdc;
  border-radius: 10px;
}
  </style>

  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/a062562745.js" crossorigin="anonymous"></script>
  <style>
  body{
  background-color: #2d2d2d;
  color: #ffc266;
  font-family: 'Roboto', sans-serif;
}


.logo {
  fill: red;
}

.btn-action{
  cursor: pointer;
  padding-top: 10px;
  width: 30px;
}

.btn-ctn, .infos-ctn{
  display: flex;
  align-items: center;
  justify-content: center;
}
.infos-ctn{
padding-top: 20px;
}

.btn-ctn > div {
 padding: 5px;
 margin-top: 18px;
 margin-bottom: 18px;
}

.infos-ctn > div {
 margin-bottom: 8px;
 color: #ffc266;
}

.first-btn{
  margin-left: 3px;
}

.duration{
  margin-left: 10px;
}

.title{
  margin-left: 10px;
  width: 210px;
  text-align: center;
}

.player-ctn{
  border-radius: 15px;
  width: 420px;
  padding: 10px;
  background-color: #373737;
  margin:auto;
  margin-top: 100px;
}

.playlist-track-ctn{
  display: flex;
  background-color: #464646;
  margin-top: 3px;
  border-radius: 5px;
  cursor: pointer;
}
.playlist-track-ctn:last-child{
  /*border: 1px solid #ffc266; */
}

.playlist-track-ctn > div{
  margin:10px;
}
.playlist-info-track{
  width: 80%;
}
.playlist-info-track,.playlist-duration{
  padding-top: 7px;
  padding-bottom: 7px;
  color: #e9cc95;
  font-size: 14px;
  pointer-events: none;
}
.playlist-ctn{
   padding-bottom: 20px;
}
.active-track{
  background: #4d4d4d;
  color: #ffc266 !important;
  font-weight: bold;
  
}

.active-track > .playlist-info-track,.active-track >.playlist-duration,.active-track > .playlist-btn-play{
  color: #ffc266 !important;
}


.playlist-btn-play{
  pointer-events: none;
  padding-top: 5px;
  padding-bottom: 5px;
}
.fas{
  color: #ffc266;
  font-size: 20px;
}
  </style>

<script>
    

    var  autolist =   localStorage.getItem("listAudio");
    var listAudio = '['+autolist+']';    
    localStorage.setItem("newlistAuddio",listAudio);
    var listAudio = JSON.parse(listAudio); 
    creteTrack(listAudio);
    var indexAudio = 0;
    localStorage.setItem("indexAudio",indexAudio);
    var playListItems = document.querySelectorAll(".playlist-track-ctn");
    var currentAudio = document.getElementById("myAudio");
    getTrackload(playListItems,currentAudio,listAudio);

    $(document).on('click','.playsong',function(){
        
        var jsonData = $(this).data('audio');
        var id=$(this).data('id');
        var formate =$(this).data('formate');
       
        
        var string = JSON.stringify(jsonData);
     
        
        var  autolist =   localStorage.getItem("listAudio");
        if(autolist!='')
        {
            var  autolist1 =   localStorage.setItem("listAudio",string+','+autolist);
        }else{
            var  autolist1 =   localStorage.setItem("listAudio",string);
        }
        var  autolist =   localStorage.getItem("listAudio");        
            var listAudio = '['+autolist+']';    
        var listAudio = JSON.parse(listAudio);
        
        var newlistAuddio = [];
        var k=0;
        var myarray = [];
        
        
        for(var i=0; i<listAudio.length; i++)
        {
          if(listAudio[i]){
              
          
          if(jQuery.inArray(listAudio[i].id, myarray) != -1) {
                console.log("is in array");
            } else {
                myarray[k] = listAudio[i].id;
                newlistAuddio[k] = listAudio[i];

                k++;
                console.log("is NOT in array");
            } 
            }
        }
        var string = JSON.stringify(newlistAuddio);
        localStorage.setItem("newlistAuddio",string);
        listAudio = JSON.parse(string);

        
        
        $('.playlist-ctn').html('');
        creteTrack(listAudio);
        var indexAudio = 0;
        localStorage.setItem("indexAudio",indexAudio);
        var playListItems = document.querySelectorAll(".playlist-track-ctn");
        var currentAudio = document.getElementById("myAudio");
        
        getTrackload(playListItems,currentAudio,listAudio);
        toggleAudio();
        
        var form = new FormData();
        form.append("type", formate);
      
        form.append("type_id", id);
        form.append("ip_address", "{{$_SERVER['REMOTE_ADDR']}}");

        var settings = {
          "async": true,
          "crossDomain": true,
          "url": "{{url('api/views')}}",
          "method": "POST",
          "headers": {
            "authorization": "Basic bXAzYmFqYW86bXAzYmFqYW9AMjAyMA==",
            "cache-control": "no-cache",
            "postman-token": "f63158c1-f9cc-2e25-234f-c90b2b88efc0"
          },
          "processData": false,
          "contentType": false,
          "mimeType": "multipart/form-data",
          "data": form
        }

        $.ajax(settings).done(function (response) {
          console.log(response);
        });  

        
        var form = new FormData();
            form.append("track_id", jsonData.id);
            form.append("ip_address","{{$_SERVER['REMOTE_ADDR']}}");

            var settings = {
              "async": true,
              "crossDomain": true,
              "url": "{{url('api/add_history')}}",
              "method": "POST",
              "headers": {
                "authorization": "Basic bXAzYmFqYW86bXAzYmFqYW9AMjAyMA==",
                "cache-control": "no-cache",
                "postman-token": "171310e7-2968-1fa5-add2-cd0c708ddcce"
              },
              "processData": false,
              "contentType": false,
              "mimeType": "multipart/form-data",
              "data": form
            }

            $.ajax(settings).done(function (response) {
              console.log(response);
            });
      
     });
    
    
    
    
  function createTrackItem(index,name,duration){

    var trackItem = document.createElement('div');
    trackItem.setAttribute("class", "playlist-track-ctn");
    trackItem.setAttribute("id", "ptc-"+index);
    trackItem.setAttribute("data-index", index);
    document.querySelector(".playlist-ctn").appendChild(trackItem);

    var playBtnItem = document.createElement('div');
    playBtnItem.setAttribute("class", "playlist-btn-play");
    playBtnItem.setAttribute("id", "pbp-"+index);
    document.querySelector("#ptc-"+index).appendChild(playBtnItem);

    var btnImg = document.createElement('i');
    btnImg.setAttribute("class", "fas fa-play");
    btnImg.setAttribute("height", "40");
    btnImg.setAttribute("width", "40");
    btnImg.setAttribute("id", "p-img-"+index);
    document.querySelector("#pbp-"+index).appendChild(btnImg);

    var trackInfoItem = document.createElement('div');
    trackInfoItem.setAttribute("class", "playlist-info-track");
    trackInfoItem.innerHTML = name
    document.querySelector("#ptc-"+index).appendChild(trackInfoItem);

    var trackDurationItem = document.createElement('div');
    trackDurationItem.setAttribute("class", "playlist-duration");
    trackDurationItem.innerHTML = duration
    document.querySelector("#ptc-"+index).appendChild(trackDurationItem);
  }

  
  function creteTrack(listAudio){
      
    for (var i = 0; i < listAudio.length; i++) {
        if(listAudio[i])
        {

            createTrackItem(i,listAudio[i].name,listAudio[i].duration);
        }
    }
  }
  
 

  function loadNewTrack(index){

    var player = document.querySelector('#source-audio')
     var listAudio =  localStorage.getItem("newlistAuddio");
    var listAudio = JSON.parse(listAudio); 
    player.src = listAudio[index].url
    document.querySelector('.song-name').innerHTML = listAudio[index].name
    document.querySelector('.song-artists').innerHTML = listAudio[index].artist
    document.querySelector('.album_image').innerHTML = '<img src="'+listAudio[index].cover_art_url+'"/>'
    this.currentAudio = document.getElementById("myAudio");
    this.currentAudio.load()
    this.toggleAudio()
    localStorage.setItem("indexAudio",indexAudio);
    this.updateStylePlaylist(this.indexAudio,index)
    this.indexAudio = index;
  }

   // setCurrentTrack();
    var slider = document.getElementById("skeakervolumeRange");
    slider.oninput = function() {
      currentAudio.volume = (this.value/100);
    }
  

  function getClickedElement(event) {
    
    for (let i = 0; i < playListItems.length; i++){
      if(playListItems[i] == event.target){
        var clickedIndex = event.target.getAttribute("data-index")
        if (clickedIndex == this.indexAudio ) { // alert('Same audio');
            this.toggleAudio()
        }else{
            loadNewTrack(clickedIndex);
        }
      }
    }
  }
    
    function getTrackload(playListItems,currentAudio,listAudio){
      
        for (let i = 0; i < playListItems.length; i++){
          playListItems[i].addEventListener("click", getClickedElement.bind(this));
        }
        if(listAudio!=''){
            if(listAudio[indexAudio])
            {
                indexAudio =  parseInt(localStorage.getItem("indexAudio"));            
                document.querySelector('#source-audio').src = listAudio[indexAudio].url;
                document.querySelector('.song-name').innerHTML = listAudio[indexAudio].name;
                document.querySelector('.song-artists').innerHTML = listAudio[indexAudio].artist;
                document.querySelector('.album_image').innerHTML = '<img src="'+listAudio[indexAudio].cover_art_url+'"/>';
            }
        }
        currentAudio.load()
        currentAudio.onloadedmetadata = function() {
              document.getElementsByClassName('duration')[0].innerHTML = this.getMinutes(this.currentAudio.duration)
        }.bind(this);

    }
  
  var interval1;
    localStorage.setItem("loadone",'0');
  function toggleAudio() {
  
    var listAudio =  localStorage.getItem("newlistAuddio");
    var listAudio = JSON.parse(listAudio);
    var track_id = listAudio[this.indexAudio].id;
    console.log('playlist',track_id);
    if (this.currentAudio.paused) {
    var loadone = localStorage.getItem("loadone",'0'); 
    if(loadone == '0')
    {
        setTimeout(function(){
             $('.eq-white').hide();
          $('.play-white').show();
          $('.play-white-'+track_id).hide();
          $('.eq-white-'+track_id).show();   
        }, 7000);
        localStorage.setItem("loadone",'1');
    }
    
       $('.eq-white').hide();
      $('.play-white').show();
      $('.play-white-'+track_id).hide();
      $('.eq-white-'+track_id).show();
        
      document.querySelector('#icon-play').style.display = 'none';
      document.querySelector('#icon-pause').style.display = 'block';
      document.querySelector('#ptc-'+this.indexAudio).classList.add("active-track");
      this.playToPause(this.indexAudio)
      this.currentAudio.play();
    }else{
      $('.play-white-'+track_id).show();
      $('.eq-white-'+track_id).hide(); 
      document.querySelector('#icon-play').style.display = 'block';
      document.querySelector('#icon-pause').style.display = 'none';
      this.pauseToPlay(this.indexAudio)
      this.currentAudio.pause();
    }
  }

  function pauseAudio() {

    this.currentAudio.pause();
    clearInterval(interval1);
  }

  var timer = document.getElementsByClassName('timer')[0]

  var barProgress = document.getElementById("myBar");


  var width = 0;

  function onTimeUpdate() {
    
    var t = this.currentAudio.currentTime
    timer.innerHTML = this.getMinutes(t);
    this.setBarProgress();
    if (this.currentAudio.ended) {
      document.querySelector('#icon-play').style.display = 'block';
      document.querySelector('#icon-pause').style.display = 'none';
      this.pauseToPlay(this.indexAudio)
      var listAudio =  localStorage.getItem("newlistAuddio");
      var listAudio = JSON.parse(listAudio); 
      if (this.indexAudio < listAudio.length-1) {
          var index = parseInt(this.indexAudio)+1
          this.loadNewTrack(index)
      }
    }
  }


  function setBarProgress(){
    var progress = (this.currentAudio.currentTime/this.currentAudio.duration)*100;
    document.getElementById("myBar").style.width = progress + "%";
  }


  function getMinutes(t){

    var min = parseInt(parseInt(t)/60);
    var sec = parseInt(t%60);
    if (sec < 10) {
      sec = "0"+sec
    }
    if (min < 10) {
      min = "0"+min
    }
    return min+":"+sec
  }

  var progressbar = document.querySelector('#myProgress')
  progressbar.addEventListener("click", seek.bind(this));


  function seek(event) {
     
    var percent = event.offsetX / progressbar.offsetWidth;
    this.currentAudio.currentTime = percent * this.currentAudio.duration;
    barProgress.style.width = percent*100 + "%";
  }

  function forward(){

    this.currentAudio.currentTime = this.currentAudio.currentTime + 5
    this.setBarProgress();
  }

  function rewind(){

    this.currentAudio.currentTime = this.currentAudio.currentTime - 5
    this.setBarProgress();
  }


  function next(){

    var listAudio =  localStorage.getItem("newlistAuddio");
    var listAudio = JSON.parse(listAudio); 
 
    if (this.indexAudio <listAudio.length-1) {
        var oldIndex = this.indexAudio
        this.indexAudio++;
        updateStylePlaylist(oldIndex,this.indexAudio)
        this.loadNewTrack(this.indexAudio);
    }
  }

  function previous(){

   
    if (this.indexAudio>0) {
        var oldIndex = this.indexAudio
        this.indexAudio--;
        updateStylePlaylist(oldIndex,this.indexAudio)
        this.loadNewTrack(this.indexAudio);
    }
  }

  function updateStylePlaylist(oldIndex,newIndex){
      
        var listAudio =  localStorage.getItem("newlistAuddio");
        var listAudio = JSON.parse(listAudio);
        var track_id = listAudio[newIndex].id;
        console.log(listAudio[newIndex],'------------------');
        
      
        $('.eq-white').hide();
        $('.play-white').show();
        $('.play-white-'+track_id).hide();
        $('.eq-white-'+track_id).show(); 
      
        var form = new FormData();

        form.append("type", "Track");
      
        form.append("type_id", track_id);
        form.append("ip_address", "{{$_SERVER['REMOTE_ADDR']}}");

        var settings = {
          "async": true,
          "crossDomain": true,
          "url": "{{url('api/views')}}",
          "method": "POST",
          "headers": {
            "authorization": "Basic bXAzYmFqYW86bXAzYmFqYW9AMjAyMA==",
            "cache-control": "no-cache",
            "postman-token": "f63158c1-f9cc-2e25-234f-c90b2b88efc0"
          },
          "processData": false,
          "contentType": false,
          "mimeType": "multipart/form-data",
          "data": form
        }

        $.ajax(settings).done(function (response) {
          console.log(response);
        });  

      
 
    document.querySelector('#ptc-'+oldIndex).classList.remove("active-track");
    this.pauseToPlay(oldIndex);
    document.querySelector('#ptc-'+newIndex).classList.add("active-track");
    this.playToPause(newIndex)
  }

  function playToPause(index){

    var ele = document.querySelector('#p-img-'+index)
    ele.classList.remove("fa-play");
    ele.classList.add("fa-pause");
  }

  function pauseToPlay(index){
    var ele = document.querySelector('#p-img-'+index)
    ele.classList.remove("fa-pause");
    ele.classList.add("fa-play");
  }


  /*function toggleMute(){
    var btnMute = document.querySelector('#toggleMute');
    var volUp = document.querySelector('#icon-vol-up');
    var volMute = document.querySelector('#icon-vol-mute');
    if (this.currentAudio.muted == false) {
       this.currentAudio.muted = true
       volUp.style.display = "none"
       volMute.style.display = "block"
    }else{
      this.currentAudio.muted = false
      volMute.style.display = "none"
      volUp.style.display = "block"
    }
  }*/
  </script>
@endif