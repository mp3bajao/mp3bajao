<aside class="left-sidebar" >
    <div class="d-flex no-block nav-text-box align-items-center">
        <span><img src="{{ URL::asset('assets/images/logo2.png')}}" style="padding-left: 35%;" width="90" alt="elegant admin template"></span>
        <a class="nav-lock waves-effect waves-dark ml-auto hidden-md-down" href="javascript:void(0)"><i class="mdi mdi-toggle-switch"></i></a>
        <a class="nav-toggler waves-effect waves-dark ml-auto hidden-sm-up" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
    </div>
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li> 
                    <a class="waves-effect waves-dark {{ (Route::current()->uri=='/'?'active':'') }}" href="{{ routeUser('dashboard') }}" aria-expanded="false">
                        <i class="icon-speedometer"></i>
                        <span class="hide-menu">{{ __('backend.dashboard') }}
                            <!-- <span class="badge badge-pill badge-cyan">4</span> -->
                        </span>
                    </a>
                </li>

                <li> 
                    <a class="has-arrow waves-effect waves-dark" id="collapseTest" href="javascript:void(0)" aria-expanded="false">
                        <i class="fas fa-user"></i><span class="hide-menu">{{ __('Users Manager') }}
                    </a>
                    <ul aria-expanded="false" class="collapse" >
                        @can('Users-section')
                        <li>
                            <a class="{{ (Route::current()->uri=='users'?'active':'') }}" href="{{ routeUser('users.index') }}">
                          
                                {{ __('Cutomer Manager') }}
                            </a>
                        </li>
                        @endcan
                        @can('Staff-section')                        
                        <li>
                            <a class="{{ (Route::current()->uri=='staff'?'active':'') }}" href="{{ routeUser('staff.index') }}">                                
                                {{ __('Staff Manager') }}
                            </a>
                        </li> 
                         @endcan
                    </ul>
                </li>
                
                 <li> 
                    <a class="has-arrow waves-effect waves-dark" id="collapseTest" href="javascript:void(0)" aria-expanded="false">
                           <i class="fas fa-compact-disc"></i><span class="hide-menu">{{ __('Genres Manager') }}
                    </a>
                     <ul aria-expanded="false" class="collapse" >
                         @can('Genres-section')
                        <li>
                            <a class="waves-effect waves-dark {{ (Route::current()->uri=='genres'?'active':'') }}" href="{{ routeUser('genres.index') }}">
                                {{ __('Genres') }}
                            </a>
                        </li>
                          @endcan
                        @can('Subgenres-section')
                        <li>
                            <a class="waves-effect waves-dark {{ (Route::current()->uri=='sub-genres'?'active':'') }}" href="{{ routeUser('subgenres.index') }}">
                                {{ __('Sub Genres') }}
                            </a>
                        </li>
                  @endcan 
                     </ul>
                 </li>
                
                
                   <li>
                    <a class="waves-effect waves-dark {{ (Route::current()->uri=='artist'?'active':'') }}" href="{{ routeUser('artist.index') }}">
                       <i class="fab fa-artstation"></i>
                        {{ __('Artists Manager') }}
                    </a>
                </li>
                
                @can('Store-section')
                <li>
                    <a class="waves-effect waves-dark {{ (Route::current()->uri=='store'?'active':'') }}" href="{{ routeUser('store.index') }}">
                        <i class="fas fa-tag"></i>
                        {{ __('Store Manager') }}
                    </a>
                </li>
                @endcan
             
                
                <li>
                    <a class="waves-effect waves-dark {{ (Route::current()->uri=='sftp'?'active':'') }}" href="{{ routeUser('sftp.report') }}">
                        <i class="fas fa-flag-checkered"></i>
                         {{ __('Sftp Report') }}
                    </a>
                </li>
                
                <li> 
                    <a class="has-arrow waves-effect waves-dark" id="collapseTest" href="javascript:void(0)" aria-expanded="false">
                        <i class="fas fa-headphones"></i><span class="hide-menu">{{ __('Music Manager') }}
                    </a>
                    <ul aria-expanded="false" class="collapse" >
                        <li>
                            <a class="waves-effect waves-dark {{ (Route::current()->uri=='album'?'active':'') }}" href="{{ routeUser('album.index') }}">
                                {{ __('Album Manger') }}
                            </a>
                        </li>
                        <li>
                            <a class="waves-effect waves-dark {{ (Route::current()->uri=='album'?'active':'') }}" href="{{ routeUser('track.index') }}">
                                {{ __('Track Manger') }}
                            </a>
                        </li> 
                        <li>
                            <a class="waves-effect waves-dark {{ (Route::current()->uri=='vidoe'?'active':'') }}" href="{{ routeUser('video.index') }}">
                                {{ __('Vidoe Manger') }}
                            </a>
                        </li> 
                   </ul>
                </li>
                
                
                
               <li>
                    <a class="waves-effect waves-dark {{ (Route::current()->uri=='playlist'?'active':'') }}" href="{{ routeUser('playlist.index') }}">
                        <i class="fas fa-music"></i>
                        {{ __('Playlist Manger') }}
                    </a>
                </li>
                
                
                <li> 
                    <a class="has-arrow waves-effect waves-dark" id="collapseTest" href="javascript:void(0)" aria-expanded="false">
                        <i class="fas fa-chart-bar"></i><span class="hide-menu">{{ __('Chart Manager') }}
                    </a>
                    <ul aria-expanded="false" class="collapse" >
                        <li>
                            <a class="{{ (Route::current()->uri=='chart'?'active':'') }}" href="{{ routeUser('chart.index') }}">                          
                                {{ __('Chart List') }}
                            </a>
                        </li>
                        <li>
                            <a class="{{ (Route::current()->uri=='chart'?'active':'') }}" href="{{ routeUser('chart.song.index') }}">
                                {{ __('Top Chart List') }}
                            </a>
                        </li>
                        <li>
                            <a class="{{ (Route::current()->uri=='music'?'active':'') }}" href="{{ routeUser('chart.music.index') }}">
                                {{ __('Top Music List') }}
                            </a>
                        </li>
                        <?php /*<li>
                            <a class="{{ (Route::current()->uri=='artist'?'active':'') }}" href="{{ routeUser('chart.artist.index') }}">
                                {{ __('Top Artist List') }}
                            </a>
                        </li> */ ?>
                        <li>
                            <a class="{{ (Route::current()->uri=='banner'?'active':'') }}" href="{{ routeUser('banner.index') }}">
                                {{ __('Banner List') }}
                            </a>
                        </li>
                    </ul>
                </li>
                
                
                <li> 
                    <a class="has-arrow waves-effect waves-dark" id="collapseTest" href="javascript:void(0)" aria-expanded="false">
                       <i class="fas fa-broadcast-tower"></i><span class="hide-menu">{{ __('Radio Manager') }}
                    </a>
                    <ul aria-expanded="false" class="collapse" >
                        <li>
                            <a class="{{ (Route::current()->uri=='station'?'active':'') }}" href="{{ routeUser('station.index') }}">                          
                                {{ __('Station List') }}
                            </a>
                        </li>
                        <li>
                            <a class="{{ (Route::current()->uri=='adds_program'?'active':'') }}" href="{{ routeUser('adds_program.index') }}">                          
                                {{ __('Adds Program List') }}
                            </a>
                        </li>
                        <li>
                            <a class="{{ (Route::current()->uri=='program'?'active':'') }}" href="{{ routeUser('program.index') }}">                          
                                {{ __('Program List') }}
                            </a>
                        </li>
                      
                        <li>
                            <a class="{{ (Route::current()->uri=='radio'?'active':'') }}" href="{{ routeUser('radio.index') }}">                          
                                {{ __('Radio Manage') }}
                            </a>
                        </li>
                        
                    </ul>
                </li>
               
                @can('Permission-section')
                <li>
                    <a class="waves-effect waves-dark {{ (Route::current()->uri=='permissions'?'active':'') }}" href="{{ route('backpanel.permissions') }}">
                        <i class="fas fa-lock"></i>
                        {{ __('Permissions') }}
                    </a>
                </li>
                @endcan

               
                
               
              
<?php    /*

                <li>
                    <a class="waves-effect waves-dark {{ (Route::current()->uri=='orders/0'?'active':'') }}" href="{{ url('orders/0') }}">
                    <i class="fas fa-shopping-cart"></i>
                        {{ __('Orders Manager') }}
                    </a>
                </li>
                   
                <li>
                    <a class="waves-effect waves-dark {{ (Route::current()->uri=='cash-register'?'active':'') }}" href="{{ route('cash-register') }}">
                    <i class="fas fa-cash-register"></i>
                        {{ __('Cash Register Manager') }}
                    </a>
                </li>
               
                @can('Content-section')
                <li>
                    <a class="waves-effect waves-dark {{ (Route::current()->uri=='content'?'active':'') }}" href="{{ route('content') }}">
                        <i class="fas fa-fw fa-edit"></i>
                        {{ __('Content Manager') }}
                    </a>
                </li>
                @endcan

                @can('Content-section')
                <li>
                    <a class="waves-effect waves-dark {{ (Route::current()->uri=='notifications'?'active':'') }}" href="{{ route('notifications') }}">
                    <i class="fas fa-bell"></i>
                        {{ __('Notifications Manager') }}
                    </a>
                </li>
                @endcan
                
             
                <li>
                    <a class="waves-effect waves-dark {{ (Route::current()->uri=='price-settings'?'active':'') }}" href="{{ route('price-settings') }}">
                    <i class="fas fa-cog"></i>
                        {{ __('Admin Settings') }}
                    </a>
                </li>
                 
              
                      */?>  
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<script>
    $(document).ready(function() {
        
        $('.has-arrow').on('click', function (e) {
          $(".collapse").css({ 'display' : '' });
          $(".collapse").removeClass("in");
        });
    });
</script>