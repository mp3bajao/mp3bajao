<link rel="stylesheet" href="{{ URL::asset('css/playar.css')}}">
                   
<div class="player paused" style="display:none" >
  <div class="album">
    <div class="cover">
      <div><img class="audio-cover-page" src="" alt="3rdburglar by Wordburglar" /></div>
    </div>
  </div>
  
  <div class="info">
    <div class="time">
      <span class="current-time">0:00</span>
      <span class="progress"><span></span></span>
      <span class="duration">0:00</span>
    </div>
    
    <h1 class="audio-track-name">Drawings With Words</h1>
    <h2 class="audio-track-artist"> 3RDBURGLAR</h2>
  </div>
  
  <div class="actions">
    <button class="shuffle">
      <div class="arrow"></div>
      <div class="arrow"></div>
    </button>
    <button class="button rw">
      <div class="arrow"></div>
      <div class="arrow"></div>
    </button>
    <button class="button play-pause">
      <div class="arrow"></div>
    </button>
    <button class="button ff">
      <div class="arrow"></div>
      <div class="arrow"></div>
    </button>
    <button class="repeat"></button>
    <i  class="fa fa-volume-up volume-dropdown-menu" aria-hidden="true"></i>
    <input  type="range" min="0" max="100" value="50" class="volume-slider" id="volumeRange">
  </div>
  
  <audio prelaod src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/38273/Wordburglar_Drawings_with_Words.mp3" id="audioPlayer"></audio>
</div>
<link rel="stylesheet" href="{{ URL::asset('css/playar.css')}}">


<script>


    
var player = $('.player'),
    audio = player.find('audio'),
    duration = $('.duration'),
    currentTime = $('.current-time'),
    progressBar = $('.progress span'),
    mouseDown = false,
    rewind, showCurrentTime;

function secsToMins(time) {
  var int = Math.floor(time),
      mins = Math.floor(int / 60),
      secs = int % 60,
      newTime = mins + ':' + ('0' + secs).slice(-2);
  
  return newTime;
}

function getCurrentTime() {
  var currentTimeFormatted = secsToMins(audio[0].currentTime),
      currentTimePercentage = audio[0].currentTime / audio[0].duration * 100;
  
  currentTime.text(currentTimeFormatted);
  progressBar.css('width', currentTimePercentage + '%');
  
  if (player.hasClass('playing')) {
    showCurrentTime = requestAnimationFrame(getCurrentTime);
  } else {
    cancelAnimationFrame(showCurrentTime);
  }
}

audio.on('loadedmetadata', function() {
  var durationFormatted = secsToMins(audio[0].duration);
  duration.text(durationFormatted);
}).on('ended', function() {
  if ($('.repeat').hasClass('active')) {
    audio[0].currentTime = 0;
    audio[0].play();
  } else {
    player.removeClass('playing').addClass('paused');
    audio[0].currentTime = 0;
  }
});

$('button').on('click', function() {
  var self = $(this);
  if (self.hasClass('play-pause') && player.hasClass('paused')) {
    player.removeClass('paused').addClass('playing');
    audio[0].play();
    getCurrentTime();
  } else if (self.hasClass('play-pause') && player.hasClass('playing')) {
    player.removeClass('playing').addClass('paused');
    audio[0].pause();
  }
  
  if (self.hasClass('shuffle') || self.hasClass('repeat')) {
    self.toggleClass('active');
  }
}).on('mousedown', function() {
  var self = $(this);
  
  if (self.hasClass('ff')) {
    player.addClass('ffing');
    audio[0].playbackRate = 2;
  }
  
  if (self.hasClass('rw')) {
    player.addClass('rwing');
    rewind = setInterval(function() { audio[0].currentTime -= .3; }, 100);
  }
}).on('mouseup', function() {
  var self = $(this);
  
  if (self.hasClass('ff')) {
    player.removeClass('ffing');
    audio[0].playbackRate = 1;
  }
  
  if (self.hasClass('rw')) {
    player.removeClass('rwing');
    clearInterval(rewind);
  }
});

player.on('mousedown mouseup', function() {
  mouseDown = !mouseDown;
});

progressBar.parent().on('click mousemove', function(e) {
  var self = $(this),
      totalWidth = self.width(),
      offsetX = e.offsetX,
      offsetPercentage = offsetX / totalWidth;
  
  if (mouseDown || e.type === 'click') {
    audio[0].currentTime = audio[0].duration * offsetPercentage;
    if (player.hasClass('paused')) {
      progressBar.css('width', offsetPercentage * 100 + '%');
    }
  }
});
var slider = document.getElementById("volumeRange");
slider.oninput = function() {
  audio[0].volume = (this.value/100);
}

   $(document).on('click','.playsong',function(){
        $('#audioPlayer').attr('src',$(this).data('song'));
        $('.audio-track-name').html($(this).data('track_name'));
        $('.audio-track-artist').html($(this).data('track_artist'));
        $('.audio-cover-page').attr('src',$(this).data('cover'));
        $('.audio-cover-page').attr('alt',$(this).data('track_name'));
        $('#audioPlayer').attr('src',$(this).data('song'));
        $('.player').show();
        $('.playsong').html('<i class="fas fa-play"></i>');
        $(this).html('<i class="fas fa-pause"></i>');
        if(player.hasClass('playing')) {
            player.removeClass('playing').addClass('paused');
            audio[0].pause();
           
              //player.volume = 0.2;
        }
        if(player.hasClass('paused')){
            player.removeClass('paused').addClass('playing');
            audio[0].play();
            getCurrentTime();
        }
    });
     $(document).on('click','.volume-dropdown-menu',function(){   
       $('.volume-slider').toggle(); 
    });
</script>

