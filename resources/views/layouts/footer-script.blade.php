<div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
                        <div class="r-panel-body">
                            <ul id="themecolors" class="m-t-20">
                                <li><b>With Light sidebar</b></li>
                                <li><a href="javascript:void(0)" data-skin="skin-default" class="default-theme">1</a></li>
                                <li><a href="javascript:void(0)" data-skin="skin-green" class="green-theme">2</a></li>
                                <li><a href="javascript:void(0)" data-skin="skin-red" class="red-theme">3</a></li>
                                <li><a href="javascript:void(0)" data-skin="skin-blue" class="blue-theme">4</a></li>
                                <li><a href="javascript:void(0)" data-skin="skin-purple" class="purple-theme">5</a></li>
                                <li><a href="javascript:void(0)" data-skin="skin-megna" class="megna-theme">6</a></li>
                                <li class="d-block m-t-30"><b>With Dark sidebar</b></li>
                                <li><a href="javascript:void(0)" data-skin="skin-default-dark" class="default-dark-theme">7</a></li>
                                <li><a href="javascript:void(0)" data-skin="skin-green-dark" class="green-dark-theme">8</a></li>
                                <li><a href="javascript:void(0)" data-skin="skin-red-dark" class="red-dark-theme working">9</a></li>
                                <li><a href="javascript:void(0)" data-skin="skin-blue-dark" class="blue-dark-theme">10</a></li>
                                <li><a href="javascript:void(0)" data-skin="skin-purple-dark" class="purple-dark-theme">11</a></li>
                                <li><a href="javascript:void(0)" data-skin="skin-megna-dark" class="megna-dark-theme ">12</a></li>
                            </ul>
                            <ul class="m-t-20 chatonline">
                                <li><b>Chat option</b></li>
                                <li>
                                    <a href="javascript:void(0)"><img src="assets/images/users/1.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="assets/images/users/2.jpg" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="assets/images/users/3.jpg" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="assets/images/users/4.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="assets/images/users/5.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="assets/images/users/6.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="assets/images/users/7.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="assets/images/users/8.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                    
<div class="modal" id="myModalTrack" >
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
              <h4 class="modal-title">Upload Audio Track</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
      <!-- Modal body -->
      <div class="modal-body">
          <div class="modal-body-content trackUploadView">
              
          </div>
      </div>
    </div>
  </div>
                   
</div>


<script data-cfasync="false" src="{{ URL::asset('assets/jquery/email-decode.min.js')}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{ URL::asset('assets/popper/popper.min.js')}}"></script>
<script src="{{ URL::asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{ URL::asset('dist/js/perfect-scrollbar.jquery.min.js')}}"></script>
<!--Wave Effects -->
<script src="{{ URL::asset('dist/js/waves.js')}}"></script>
<!--Menu sidebar -->
<script src="{{ URL::asset('dist/js/sidebarmenu.js')}}"></script>
<!--stickey kit -->
<script src="{{ URL::asset('assets/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
<script src="{{ URL::asset('assets/sparkline/jquery.sparkline.min.js')}}"></script>
<!--Custom JavaScript -->
<script src="{{ URL::asset('dist/js/custom.min.js')}}"></script>
<!-- Editable -->
<script src="{{ URL::asset('assets/jsgrid/db.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/jsgrid/jsgrid.min.js')}}"></script>
<script src="{{ URL::asset('dist/js/pages/jsgrid-init.js')}}"></script>
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('assets/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('assets/morrisjs/morris.min.js') }}"></script>
    <script src="{{ asset('assets/jquery-sparkline/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('assets/d3/d3.min.js') }}"></script>
    <script src="{{ asset('assets/c3-master/c3.min.js') }}"></script>
    <script src="{{ asset('dist/js/dashboard1.js') }}"></script>
<script>
    toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": true,
    "progressBar": true,
    "positionClass": "toast-top-center",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
    }
function before(_this=null){
_this.find('.save').prop('disabled',true);
_this.find('.formloader').css("display","inline-block");
}
function complete(_this=null){
_this.find('.save').prop('disabled',false);
_this.find('.formloader').css("display","none");
}
$(document).ready(function(){
$('.kpip_logo_mini').css('display','inline-block').toggle();

$('[data-widget="pushmenu"]').click(function(){
  if($( window ).width()>1000){
    $('.kpip_logo_mini').fadeToggle(); 
  }
});
});

$(document).on('click','.trakeUpload',function(){
   var track_id = $(this).data('track_id');
    var url = '{{ routeUser('track.upload')}}/'+track_id;
    $('.trackUploadView').load(url);
    $('#myModalTrack').modal('show');
});
</script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places&key=AIzaSyCnC4j2VEpmhzkDbSAPSG27BI4Ux5bwNrk"></script> -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCnC4j2VEpmhzkDbSAPSG27BI4Ux5bwNrk&libraries=places"></script> -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places&key=AIzaSyCnC4j2VEpmhzkDbSAPSG27BI4Ux5bwNrk"></script>
  @yield('script')
<script>
	var geocoder;
	var map;
	var marker;
	var infowindow = new google.maps.InfoWindow({
		size: new google.maps.Size(150, 50)
	});
    initialize();
	autoload(-34.397, 150.644);

	
    var autocomplete;
    function initialize() {
      
		autocomplete = new google.maps.places.Autocomplete((document.getElementById('address')),{ types: [] });
   
		google.maps.event.addListener(autocomplete, 'place_changed', function() {
			var place = autocomplete.getPlace();
            // place variable will have all the information you are looking for.
            $('#latitude').val(place.geometry['location'].lat());
            $('#longitude').val(place.geometry['location'].lng());
			codeAddress();
		});
    }
	function autoload(latitude,longitude) {
		geocoder = new google.maps.Geocoder();
		var latlng = new google.maps.LatLng(latitude, longitude);
		var mapOptions = {
			zoom: 13,
			center: latlng,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}
		map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
		google.maps.event.addListener(map, 'click', function() {
			infowindow.close();
		});
		
		marker = new google.maps.Marker({
            map: map,
            draggable: false,
            animation: google.maps.Animation.DROP,
            position: {lat:latitude, lng: longitude}
          });
          marker.addListener('click', toggleBounce);
	}
	
	function toggleBounce() 
	{
        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }
	function geocodePosition(pos) {
		geocoder.geocode({
			latLng: pos
		}, function(responses) {
			if (responses && responses.length > 0) {
				marker.formatted_address = responses[0].formatted_address;
			} else {
				marker.formatted_address = 'Cannot determine address at this location.';
			}
			$('#address').val(marker.formatted_address);
			$('#latitude').val(marker.getPosition().lat());
			$('#longitude').val(marker.getPosition().lng());
			infowindow.setContent(marker.formatted_address + "<br>coordinates: " + marker.getPosition().toUrlValue(6));
			infowindow.open(map, marker);
		});
	}

	function codeAddress() {
		var address = document.getElementById('address').value;
		geocoder.geocode({
			'address': address
		}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			map.setCenter(results[0].geometry.location);
			if (marker) {
				marker.setMap(null);
				if (infowindow) infowindow.close();
			}
			marker = new google.maps.Marker({
				map: map,
				draggable: true,
				animation: google.maps.Animation.DROP,
				position: results[0].geometry.location
		 });
		google.maps.event.addListener(marker, 'dragend', function() {
			geocodePosition(marker.getPosition());
		});
		google.maps.event.addListener(marker, 'click', function() {
			if (marker.formatted_address) {
			  infowindow.setContent(marker.formatted_address + "<br>coordinates2: " + marker.getPosition().toUrlValue(6));
			  $('#address').val(marker.formatted_address);
			} else {
			  infowindow.setContent(address + "<br>coordinates3: " + marker.getPosition().toUrlValue(6));
			  $('#address').val(address);
			}		
			$('#latitude').val(marker.getPosition().lat());
			$('#longitude').val(marker.getPosition().lng());
			infowindow.open(map, marker);
		});
			google.maps.event.trigger(marker, 'click');
		} else {
		  alert('Geocode was not successful for the following reason: ' + status);
		}
	});
}

</script>
