@extends('beautymail::templates.sunny')

@section('content')

    @include ('beautymail::templates.sunny.heading' , [
        'heading' => 'Notifications',
        'level' => 'h1',
    ])

    @include('beautymail::templates.sunny.contentStart')

        <h4 class="secondary"><strong>Hi ,</strong></h4>


    @include('beautymail::templates.sunny.contentEnd')

@stop