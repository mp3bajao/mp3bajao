@extends('layouts.frontend.master')
@section('content')

                     <?php /*   <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- Blow Add 2 -->
            <ins class="adsbygoogle"
                 style="display:inline-block;width:100%;height:90px"
                 data-ad-client="ca-pub-9974854305750285"
                 data-ad-slot="3579376846"></ins>
            <script>
                 (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
            */?>
               <div class="section row">
                   @if(!empty($data))
                    @foreach($data as $row)
                    <div class="col-xl-3 col-lg-4 col-sm-6 pb-4">
                       
                      <div class="custom-card">
                        <div class="custom-card--img">
                            <a href="{{route('song',['genres'=>base64_encode($row->id)])}}">
                                <img src="{{$row->image}}" alt="{{ucwords($row->name)}}" class="card-img--radius-md">
                                <span class="bg-blur">{{ucwords($row->name)}}</span>
                            </a>
                         </div>
                       </div>

                     </div>
                    @endforeach
                    @endif
               </div>
          <?php /*    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- Blow page -->
        <ins class="adsbygoogle"
             style="display:inline-block;width:100%;height:90px"
             data-ad-client="ca-pub-9974854305750285"
             data-ad-slot="7622563635"></ins>
        <script>
             (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
        */?>
@endsection