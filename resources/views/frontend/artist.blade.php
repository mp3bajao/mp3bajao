@extends('layouts.frontend.master')
@section('content')

@if(isset($data) && $data['status']==true)
            <div class="main-container" id="appRoute">
             <?php /*<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- Blow Add 2 -->
            <ins class="adsbygoogle"
                 style="display:inline-block;width:100%;height:90px"
                 data-ad-client="ca-pub-9974854305750285"
                 data-ad-slot="3579376846"></ins>
            <script>
                 (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
           */ ?>
                 <div class="row align-items-end">
                  <span class="col-6 font-weight-bold">Artists {{number_format($data['data']->meta->total)}} </span>
                  <div class="col-6 ml-auto">
                     <form action="#" class="form-inline justify-content-end">
                        <label class="mr-2" for="filter2">Sorted By:</label> 
                        <select class="custom-select mr-sm-2" id="filter2">
                           <option selected="selected">Popular</option>
                           <option value="1">A-Z</option>
                           <option value="2">Z-A</option>
                        </select>
                     </form>
                  </div>
                  <div class="col-12">
                     <hr>
                  </div>
               </div>
               <div class="section row " id="result">
                   @if(isset($data['data']->data))
                    @foreach($data['data']->data as $row)
                    <div class="col-xl-2  col-lg-3 col-sm-4 pb-4">
                        <div class="custom-card">
                           <div class="custom-card--img">
                               <a href="{{route('artist.details',['id'=>base64_encode($row->id)])}}">
                                   <img src="{{$row->image}}" alt="{{ucwords($row->name)}}" class="card-img--radius-lg round-image">
                               </a>
                           </div>
                           <a href="{{route('artist.details',['id'=>base64_encode($row->id)])}}" class="custom-card--link mt-2">
                               <h6 class="mb-0 text-center"><small>{{ucwords($row->name)}}</small></h6>
                               <p class="text-center"><img src="{{asset('frontend/images/logos/icon_listeners.svg')}}" style="margin: 5px; margin-top: 1px;"><span> {{$row->total_views}}</span></p>
                           </a>
                        </div>
                    </div>                   
                    @endforeach
                    @endif
               </div>
                <div class="row">
                    <div class="col-md-12" style="text-align: center;">
                <a href="javascript:;" class="btn btn-primary btn-pill btn-sm loadMore">Load More..</a>
                </div>
                </div>
          
            </div>

@else
{{$data['message']}}
@endif

<script>
var i=1;
function ArtistLoading(page){
    var url = "{{route('artist.loading')}}";
    url = url+'/'+page;
    
    $('#prossing').show();
    $.get(url,function (dados,status, xhr) {        
       if(status == "success")
       {
            $("#result").append(dados);               
       }
           $('#prossing').hide();
    });
}
$(document).on('click','.loadMore',function(){
    i++;
   ArtistLoading(i);
});

</script>
@endsection