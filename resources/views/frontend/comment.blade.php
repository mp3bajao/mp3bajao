<?php $userdata =  Session::get('userData');

?>
@if(isset($comment) && $comment['status']==true)
                    
                        @foreach($comment['data']->data as $comment)
                        <div class="d-flex mb-4">
                            <div class="pl-3 flex-grow-1 flex-basis-0">                               
                                    <div class="row" style="background-color: #0a0c11;padding: 5px; border-radius: 37px; margin-bottom: 7px;">
                                        <div class="col-md-1" style="max-width: 50px;">
                                             <div class="avatar avatar-sm avatar-circle">
                                                 <img src="{{$comment->get_user->image}}" alt="">
                                             </div>
                                        </div>
                                        <div class="col-md-11">
                                            <span class="d-block font-weight-bold mb-1" style="padding-top: 8px;">
                                                {{ucwords($comment->get_user->name)}} <span style="float: right;">{{date('d M, Y H:i A',strtotime('+330 minutes',strtotime(date($comment->created_at))))}}</span>
                                            </span>
                                        </div>
                                    </div>
                            
                                
                               <?php /*<div class="text-warning stars mb-2 font-md">
                                   <i class="la la-star"></i> 
                                   <i class="la la-star"></i> 
                                   <i class="la la-star"></i> 
                                   <i class="la la-star"></i> 
                                   <i class="la la-star-half-empty"></i>
                               </div>*/
                               ?>
                               <p class="mb-2">
                                   {{ucfirst($comment->comment)}} 
                               </p>
                                  <i class="la la-thumb"></i>                                  
                                  <a href="#" class="btn p-0 replaybox" data-comment_id="{{$comment->id}}" style="color:#fff;">
                                  <i class="la la-reply"></i> ({{count($comment->get_reply)}})
                                  Reply
                                  </a>
                            </div>
                        </div>


                        <div class="ml-5 mb-4">
                            <div class="showreplaybox_{{$comment->id}}" style="display:none; margin-bottom: 15px;">                   
                                <textarea name="reply" id="reply_{{$comment->id }}" cols="30" rows="5" minlength='0' maxlength='500' class="form-control" placeholder="Please share your thoughts..."></textarea>
                                <div class="text-right mt-2">
                                    <?php /*@if(!isset($userdata)) 
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#singin">  
                                        <button type="button" class="btn btn-sm btn-primary">
                                            <i class="la la-reply"></i> Reply
                                        </button>
                                    </a>
                                    @else
                                      @endif
                                     */ ?>
                                        <button class="btn btn-primary btn-sm addReply" data-comment_id="{{$comment->id}}">  
                                            <i class="la la-reply"></i>  Reply 
                                        </button>
                                  
                                </div>
                             </div>
                            @if(!empty($comment->get_reply))
                                @foreach($comment->get_reply as $reply)
                                <div class="d-flex">
                                    <div class="pl-3 flex-grow-1 flex-basis-0">
                                    <div class="row" style="background-color: #0a0c11;padding: 5px; border-radius: 37px; margin-bottom: 7px;">
                                        <div class="col-md-1" style="max-width: 50px;">
                                             <div class="avatar avatar-sm avatar-circle">
                                                 <img src="{{$reply->get_user->image}}" alt="">
                                             </div>
                                        </div>
                                        <div class="col-md-11">
                                            <span class="d-block font-weight-bold mb-1" style="padding-top: 8px;">
                                                {{ucwords($reply->get_user->name)}} <span style="float: right;">{{date('d M, Y H:i A',strtotime('+330 minutes',strtotime(date($reply->created_at))))}}</span>
                                            </span>
                                        </div>
                                    </div>                                                                       
                                    <p style="margin-bottom: 15px;">                                            
                                        {{ucfirst($reply->reply)}}
                                    </p>
                                    </div>
                                </div>
                                @endforeach
                            @endif
                        </div>
                        <hr/>
                        @endforeach
                    @endif