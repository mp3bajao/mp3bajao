@extends('layouts.frontend.master')
@section('content')
 <?php $userdata =  Session::get('userData');?>
@if(isset($data) && $data['status']==true)
 <div class="section">

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
       @if(isset($data['data']->banner))
            @forelse($data['data']->banner as $key => $banner)
    <li data-target="#carouselExampleIndicators" data-slide-to="{{$key}}" class=" @if($key==0) active @endif"></li>
     @endforeach
        @endif 
    
  </ol>
  
  <div class="carousel-inner">
    @if(isset($data['data']->banner))
        @forelse($data['data']->banner as $key => $banner)
            <div class="carousel-item @if($key==0) active @endif">
                    <a href="{{route('song.details',array('id'=>base64_encode($banner->track_id),'from'=>$banner->format))}}"><img class="d-block w-100" src="{{ $banner->image }}" alt="{{$banner->name}}"></a>
            </div>        
        @endforeach
    @endif   
  </div>
    
    
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
 </div>



    @if(isset($data['data']->album))
     @forelse($data['data']->album as $chart)
                <div class="section">
                  <div class="heading">
                     <div class="d-flex flex-wrap align-items-end">
                        <div class="flex-grow-1">
                           <h4>{{ucfirst($chart->name)}}</h4>
                           <p>{{ucfirst($chart->description)}}</p>
                        </div>
                        <a href="{{route('song',['charts'=>base64_encode(strtolower(str_replace(' ','_',$chart->name)))])}}" class="btn btn-sm btn-pill btn-air btn-primary">View All</a>
                     </div>
                     <hr>
                  </div>
                  <div class="carousel-item-6 arrow-pos-2">
                      @if(isset($chart->track))
                      @foreach($chart->track as $row)

                      <div class="custom-card">
                        <div class="custom-card--img">
                            <a
                            @if($row->chart_type=='MULTIPLE')
                            href="{{route('song.details',['id'=>base64_encode($row->data->id),'from'=>'playlist'])}}" data-id="{{$row->data->id}}" data-formate="Playlist"
                            @elseif($row->chart_type=='SINGLE')
                            @if($row->format=='Album')
                               href="{{route('song.details',['id'=>base64_encode($row->data->id),'from'=>'album'])}}"  data-id="{{$row->data->id}}" data-formate="Alubm"    
                            @else
                                href="{{route('song.details',['id'=>base64_encode($row->data->id),'from'=>'track'])}}"  data-id="{{$row->data->id}}" data-formate="Track"
                            @endif
                            @endif
                             class="playsong"  data-audio='{"id":"{!! ucwords($row->data->track_id) !!}", "name":"{!! ucwords($row->data->name) !!}", "artist":"{!! ucwords($row->data->primary_artist) !!}", "album":"{!! ucwords($row->data->name) !!}", "url":"{!! $row->data->song !!}", "cover_art_url":"{!! $row->data->image !!}","duration":"{!! $row->data->duration !!}","formate":"{{$row->format}}" }'>
                           <div class="custom-card--info">
                            
                              <div class="dropdown dropdown-icon">
                                <span class="playiconnew"><img src="{{asset('frontend/images/logos/iconPlay.svg')}}"</span>
                              </div>
                               
                           </div>
                            @if($row->chart_type=='MULTIPLE')
                            
                             @elseif($row->chart_type=='SINGLE')
                                            @if($row->format=='Album')
                                             @else
                                              @endif
                                        @endif
                            
                               <img src="{!! $row->data->image !!}" alt="{!! ucwords($row->data->name) !!}" class="card-img--radius-lg">
                               <span class="listen">{{$row->data->total_views}}</span>       
                            </a>
                        </div>
                        @if($row->chart_type=='MULTIPLE')
                        <a href="{{route('song.details',['id'=>base64_encode($row->data->id),'from'=>'playlist'])}}" class="custom-card--link mt-2">    
                        @elseif($row->chart_type=='SINGLE')
                            @if($row->format=='Album')
                                <a href="{{route('song.details',['id'=>base64_encode($row->data->id),'from'=>'album'])}}" class="custom-card--link mt-2">    
                            @else
                                <a href="{{route('song.details',['id'=>base64_encode($row->data->id),'from'=>'track'])}}" class="custom-card--link mt-2">    
                            @endif
                        @endif
                           <h6>{!! ucwords($row->data->name) !!}</h6>
                           <p><?php
                               $artist =  explode('|',$row->data->primary_artist);
                               $artist =  count($artist);
                           ?>
                               
                               {!! ($artist < 2) ? $row->data->primary_artist :'Various Artist' !!}</p>
                        </a>
                     </div>
                      
                      
                      @endforeach
                      @endif                     
                  </div>
               </div>
   @empty    
    @endforelse
@endif




               <div class="row">
                  <div class="section col-xl-8 col-lg-7">
                     <div class="row h-100">
                        <div class="col-sm-5 pb-4">
                           <div class="h-100 event event-v bg-img bg-img-radius-lg" style="background-image: url({{asset('frontend/images/background/vertical/1.webp')}})">
                              <div class="event-content p-4">
                                 <a href="#">
                                    <h6>Bajao Radio</h6>
                                 </a>
                                 <!--span class="countdown mb-3"></span--> <a href="javascript:;" class="radio_action  btn btn-bold btn-pill btn-air btn-warning btn-sm">Bajate raho Dil se..</a>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-7">
                           <div class="h-50 pb-4">
                              <div class="h-100 event event-h bg-img bg-img-radius-lg" style="background-image: url({{asset('frontend/images/background/horizontal/7.webp')}})">
                                 <div class="event-content p-4">
                                    <a href="#">
                                       <!--h6>Dance with DJ Nowan</h6-->
                                    </a>
                                    <!--p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae consectetur, ex explicabo.</p-->
                                 </div>
                              </div>
                           </div>
                           <div class="h-50 pb-4">
                              <div class="h-100 event event-h bg-img bg-img-radius-lg" style="background-image: url('frontend/images/background/horizontal/8.webp')">
                                 <div class="event-content p-4">
                                    <a href="#">
                                       <!--h6>Move You's Legs</h6-->
                                    </a>
                                    <!--p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae consectetur, ex explicabo.</p-->
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                   
                    <div class="section col-xl-4 col-lg-5">
                        <ul class="nav nav-tabs line-tabs line-tabs-primary text-uppercase mb-4" id="songsList" role="tablist">
                           <li class="nav-item"><a class="nav-link active" id="recent-tab" data-toggle="tab" href="#recent" role="tab" aria-controls="recent" aria-selected="true">Recent</a></li>
                           <!--li class="nav-item"><a class="nav-link" id="trending-tab" data-toggle="tab" href="#trending" role="tab" aria-controls="trending" aria-selected="false">Trending</a></li>
                           <li class="nav-item"><a class="nav-link" id="international-tab" data-toggle="tab" href="#international" role="tab" aria-controls="international" aria-selected="false">international</a></li-->
                        </ul>
                      
                     <div class="tab-content" id="songsListContent">
                        <div class="tab-pane fade show active" id="recent" role="tabpanel" aria-labelledby="recent-tab">
                           <div class="custom-list">
                                @if(isset($data['data']->recent))
                                    @foreach($data['data']->recent as $row) 
                                        <div class="custom-list--item">
                                            <div class="text-dark custom-card--inline">
                                               <div class="custom-card--inline-img">
                                                   <a href="{{route('song.details',['id'=>base64_encode($row->id),'from'=>'album'])}}" class="external custom-card--link mt-2" data-audio='{"id":"{!! $row->track_id!!}","name":"{!! ucwords($row->name) !!}", "artist": "{!! ucwords($row->primary_artist) !!}", "album": "{!! ucwords($row->name) !!}", "url":"{!! $row->song !!}", "cover_art_url":{!! $row->image !!}","duration":"{!! $row->duration !!}" }'>formate
                                                       <img src="{!! $row->image !!}" alt="" class="card-img--radius-sm">
                                                   </a>
                                                   </div>
                                               <div class="custom-card--inline-desc">
                                                   <a href="{{route('song.details',['id'=>base64_encode($row->id),'from'=>'album'])}}" class="external custom-card--link mt-2" data-audio='{"name":"{!! ucwords($row->name) !!}", "artist": "{!! ucwords($row->primary_artist) !!}", "album": "{!! ucwords($row->name) !!}", "url":"{!! $row->song !!}", "cover_art_url":{!! $row->image !!}","duration":"{!! $row->duration !!}" }'>
                                                        <p class="text-truncate mb-0">{!! ucwords($row->name) !!}</p>
                                                        <p class="text-truncate text-muted font-sm">{!! ucwords($row->primary_artist) !!}</p>
                                                   </a>
                                               </div>
                                            </div>                                            
                                         </div>                      
                                    @endforeach                      
                                @endif                              
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

                 <div class="section">
                  <div class="heading">
                     <div class="d-flex flex-wrap align-items-end">
                        <div class="flex-grow-1">
                           <h4>Featured Artists</h4>
                           <p>Select you best to listen</p>
                        </div>
                        <a href="{{route('artist')}}" class="btn btn-sm btn-pill btn-air btn-primary">View All</a>
                     </div>
                     <hr>
                  </div>
                  <div class="carousel-item-6 arrow-pos-2">
                      @if(isset($data['data']->artist))
                      @foreach($data['data']->artist as $row)  
                   
                     <div class="custom-card">
                        <div class="custom-card--img">
                            <a href="{{route('artist.details',['id'=>base64_encode($row->id)])}}">
                                <img src="{!! $row->image !!}" alt="{!! ucwords($row->name) !!}" class="card-img--radius-lg round-image">
                            </a>
                        </div>
                        <a href="{{route('artist.details',['id'=>base64_encode($row->id)])}}" class="custom-card--link mt-2">
                           <h6 class="mb-0 text-center">{!! ucwords($row->name) !!}</h6>
                        </a>
                     </div>
                      @endforeach
                      @endif                   
                  </div>
               </div>

             

                <div class="section">
                  <div class="heading">
                     <div class="d-flex flex-wrap align-items-end">
                        <div class="flex-grow-1">
                           <h4>Genres</h4>
                           <p>Select you genre</p>
                        </div>
                        <a href="{{route('genres')}}" class="btn btn-sm btn-pill btn-air btn-primary">View All</a>
                     </div>
                     <hr>
                  </div>
                  <div class="carousel-item-6 arrow-pos-1 genres-section">
                      @if(isset($data['data']->genres))
                      @foreach($data['data']->genres as $row)                     
                        <div class="custom-card">
                           <div class="custom-card--img"><a href="{{route('song',['genres'=>base64_encode($row->id)])}}"><img src="{!! $row->image !!}" alt="{{$row->name}}" class="card-img--radius-md"> <span class="bg-blur">{{$row->name}}</span></a></div>
                        </div>
                      @endforeach
                      @endif
                  </div>
               </div>
    
    @else
    No Data found.
    @endif
    

@endsection
