@extends('layouts.frontend.master')
@section('content')


 <?php $userdata =  Session::get('userData');?>
            <div class="main-container" id="appRoute">
            <?php /*<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- Blow Add 2 -->
            <ins class="adsbygoogle"
                 style="display:inline-block;width:100%;height:90px"
                 data-ad-client="ca-pub-9974854305750285"
                 data-ad-slot="3579376846"></ins>
            <script>
                 (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
            */?>
                <ul class="nav nav-tabs line-tabs line-tabs-primary text-uppercase mb-4" id="songDetails" role="tablist">
                    @if($language['data'])
                    @foreach($language['data'] as $row)
                    <li class="nav-item">
                        <a href="{{route('song',['lang'=>$row->language_code])}}" style="border: 1px solid #343a40; margin: 5px" class="nav-link @if($row->language_code==$lang) active @endif" id="overview-tab" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="true">{{ucwords($row->title)}}</a>
                    </li>
                    @endforeach
                    @endif
                </ul>
            @if(isset($data) && $data['status']==true)
            
             <?php /*   <div class="row align-items-end">                   
                  <span class="col-6 font-weight-bold">Albums {{number_format($data['data']->meta->total)}}</span>
                  <div class="col-6 ml-auto">
                     <form action="#" class="form-inline justify-content-end">
                        <label class="mr-2" for="filter2">Sorted By:</label> 
                        <select class="custom-select mr-sm-2" id="filter2">
                           <option selected="selected">Popular</option>
                           <option value="1">A-Z</option>
                           <option value="2">Z-A</option>
                        </select>
                     </form>
                  </div>
                  
                  <div class="col-12">
                     <hr>
                  </div>
               </div>*/ ?>
                <div class="row section" id="songresult">
                @if(isset($data['data']->track))
                @foreach($data['data']->track as $row)
                
                  <div class="col-xl-2 col-lg-3 col-sm-4 pb-4">
                     <div class="custom-card">
                        <div class="custom-card--img">
                        <a href="{{route('song.details',['id'=>base64_encode($row->data->id),'from'=>$row->format ?? 'Playlist'])}}" class="playsong"  data-audio='{"id":"{!! ucwords($row->data->track_id) !!}","name":"{!! ucwords($row->data->name) !!}", "artist":"{!! ucwords($row->data->primary_artist) !!}", "album":"{!! ucwords($row->data->name) !!}", "url":"{!! $row->data->song !!}", "cover_art_url":"{!! $row->data->image !!}" }'  data-id="{{$row->data->id}}" data-formate="{{$row->format}}">
                            <div class="custom-card--info">                            
                              <div class="dropdown dropdown-icon">
                                  <span class="playiconnew"><img src="{{asset('frontend/images/logos/iconPlay.svg')}}"></span>
                                
                              </div>
                            </div>             
                            <img src="{!! $row->data->image !!}" alt="{!! ucwords($row->data->name) !!}" class="card-img--radius-lg">
                         <span class="listen">{{$row->data->total_views}}</span>   
                        </a>
                            
                        </div>
                        <a href="{{route('song.details',['id'=>base64_encode($row->data->id),'from'=>$row->format])}}" class="custom-card--link mt-2">
                            <h6 class="text-center"><small>{!! ucwords($row->data->name) !!}</small></h6>
                           <p class="text-center"><small>{!! ucwords($row->data->primary_artist) !!}</small></p>                           
                        </a>
                     </div>
                  </div>
                @endforeach
               @endif
               
               </div> 
             <div class="row">
                    <div class="col-md-12" style="text-align: center;">
                        <a href="javascript:;" class="btn btn-primary btn-pill btn-sm loadMore">Load More..</a>
                </div>
                </div>
@else
{{$data['message']}}
@endif
<?php /*
  <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- Blow page -->
        <ins class="adsbygoogle"
             style="display:inline-block;width:100%;height:90px"
             data-ad-client="ca-pub-9974854305750285"
             data-ad-slot="7622563635"></ins>
        <script>
             (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
        */?>
            </div>  

<script>
var i=1;
function SongLoading(page){
    $('#prossing').show();
    var url = "{{route('song.loading')}}";
    @if(isset($parms['charts']))
    url = url+'/'+page+'/charts/'+"{{base64_encode($parms['charts'])}}";
    @elseif(isset($parms['genres']))
    url = url+'/'+page+'/genres/'+"{{base64_encode($parms['genres'])}}";
     @elseif(isset($parms['lang']))
    url = url+'/'+page+'/lang/'+"{{base64_encode($parms['lang'])}}";
    @elseif(isset($parms['search']))
    url = url+'/'+page+'/lang/'+"{{base64_encode($parms['search'])}}";
    @else
         url = url+'/'+page+'/'+'page'+'/'+page;  
   @endif
  
  $.get(url,function (dados,status, xhr) {        
       if(status == "success")
       {
             $("#songresult").append(dados);               
       }
       $('#prossing').hide();
       });
}
//SongLoading(i);
$(document).on('click','.loadMore',function(){
    i++;
   SongLoading(i);
});

</script>
@endsection