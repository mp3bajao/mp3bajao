@extends('layouts.frontend.master')
@section('content')
@if($data['status']==true)
 <?php $userdata =  Session::get('userData'); 
  $data = $data['data'];
  ?>
            <div class="main-container" id="appRoute">
               <div class="row section text-center text-md-left">
                  <div class="col-xl-3 col-lg-4 col-sm-5"><img src="{{$data->track->getRelease->image}}" alt="{{ $data->track->name }}" class="card-img--radius-lg"></div>
                  <div class="col-xl-9 col-lg-8 col-sm-7">
                     <div class="row pt-4">
                        <div class="col-xl-8 col-lg-6">
                            @section('title',$data->track->name)
                            @section('description',$data->track->name)
                            @section('keywords',$data->track->name)
                            @section('metadata')
                           <link rel="manifest" href="/manifest.json"> 
                        <link rel="dns-prefetch" href="//mp3bajao.com"> 
                        <link rel="dns-prefetch" href="//css375.mp3bajao.com"> 
                        <link rel="dns-prefetch" href="//css5.mp3bajao.com"> 
                        <link rel="dns-prefetch" href="//static.mp3bajao.com"> 
                        <link rel="dns-prefetch" href="//a1.mp3bajao.com"> 
                        <link rel="dns-prefetch" href="//a2.mp3bajao.com"> 
                        <link rel="dns-prefetch" href="//a10.mp3bajao.com"> 
                        <link rel="dns-prefetch" href="https://mp3bajao.com"> 
                        <link rel="dns-prefetch" href="https://jssocdn.indiatimes.com"> 
                        <link rel="dns-prefetch" href="https://cdn.moengage.com"> 
                        <link rel="dns-prefetch" href="https://www.google-analytics.com"> 
                        <link rel="dns-prefetch" href="https://apis.google.com"> 
                        <link rel="dns-prefetch" href="https://stats.g.doubleclick.net"> 
                        <link rel="dns-prefetch" href="https://connect.facebook.net"> 
                        <link rel="dns-prefetch" href="https://www.google.co.in"> 
                        <link rel="dns-prefetch" href="https://www.gstatic.com"> 
                        <link rel="dns-prefetch" href="https://www.google.com"> 
                        <link rel="dns-prefetch" href="http://b.scorecardresearch.com"> 
                        <link rel="dns-prefetch" href="https://b-s.tercept.com"> 
                        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
                        <meta name="google-site-verification" content="Isu9IQakMXodVZHO_6tV6DUNWSRF7s9I5FY-_lLKqNA"> 
                        <meta name="msvalidate.01" content="1E6490AA5F9D0438A03769318F2A1088"> 
                        <meta name="twitter:card" content="summary"> 
                        <meta name="twitter:url" content="{{route('song.details',base64_encode($data->track->id))}}"> 
                        <meta name="twitter:site" content="@gaana"> 
                        <meta name="twitter:title" content="Listen to {{$data->track->name}} Song by {{$data->track->primary_artist}} on Mp3bajao.com"> 
                        <meta name="twitter:description" content="Play {{$data->track->name}} Song by {{$data->track->primary_artist}} the album {{$data->track->getRelease->name}} song online free on Mp3bajao.com."> 
                        <meta name="twitter:image" content="{!! $data->track->getRelease->image !!}"> 
                        <meta name="twitter:app:name:googleplay" content="Mp3bajao"> <meta name="twitter:app:id:googleplay" content="com.mp3bajao"> 
                        <meta name="twitter:app:url:googleplay" content="mp3bajao://share/titemI29547130"> <meta name="twitter:app:name:iphone" content="Mp3bajao"> 
                        <meta name="twitter:app:id:iphone" content="585270521"> 
                        <meta name="twitter:app:url:iphone" content="mp3bajaoApp://share/titemI29547130">  
                        <meta name="format-detection" content="telephone=no"> 
                        <meta property="fb:app_id" content="183019041719404"> 
                        <meta property="og:site_name" content="Mp3bajao.com"> 
                        <meta property="og:type" content="music.song"> 
                        <meta property="og:url" content="{{route('song.details',base64_encode($data->track->id))}}"> 
                        <meta property="og:title" content="Listen to {{$data->track->name}} Song by {{$data->track->primary_artist}} on Mp3bajao.com"> 
                        <meta property="og:description" content="Play {{$data->track->name}} Song by {{$data->track->primary_artist}} the album {{$data->track->getRelease->name}} song online free on Mp3bajao.com."> 
                        <meta property="og:image" content="{!! $data->track->getRelease->image !!}">  
                        <meta property="og:image:width" content="250"> <meta property="og:image:height" content="250">  
                        <meta property="music:album" content="{{route('song.details',base64_encode($data->track->id))}}">  
                        <meta property="og:audio" content="{{route('song.details',base64_encode($data->track->id))}}">   
                        <meta property="music:duration" content="4">    
                        <link rel="canonical" href="{{route('song.details',base64_encode($data->track->id))}}">   
                        <link rel="amphtml" href="{{route('song.details',base64_encode($data->track->id))}}">  
                        <!--link rel="alternate" href="android-app://com.gaana/gaanagoogle/song/dus-bahane-20">  
                        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no"--> 

                            @endsection
                            
                            
                            <h5> {{ $data->track->name }}</h5>
                           <p>By {{ str_replace('|',',',$data->track->primary_artist) }}</p>
                           <p>{!! $data->track->p_line !!}</p>
                           <div class="text-warning stars">
                               <span class="gl-star-rating-stars rating_num s{{$data->track->rating}}0">
                                    @if(!isset($userdata))
                                   <span data-value="1" data-track_id="{{$data->track->id}}"></span>
                                   <span data-value="2" data-track_id="{{$data->track->id}}"></span>
                                   <span data-value="3" data-track_id="{{$data->track->id}}"></span>
                                   <span data-value="4" data-track_id="{{$data->track->id}}"></span>
                                   <span data-value="5" data-track_id="{{$data->track->id}}"></span>
                                   @else
                                   <span class="addRating" data-value="1" data-track_id="{{$data->track->id}}" data-type="Track"></span>
                                   <span class="addRating" data-value="2" data-track_id="{{$data->track->id}}"  data-type="Track"></span>
                                   <span class="addRating" data-value="3" data-track_id="{{$data->track->id}}"  data-type="Track"></span>
                                   <span class="addRating" data-value="4" data-track_id="{{$data->track->id}}"  data-type="Track"></span>
                                   <span class="addRating" data-value="5" data-track_id="{{$data->track->id}}"  data-type="Track"></span>
                                   @endif
                               </span>
                               
                            </div>
                           <!--span class="badge badge-pill badge-warning mt-3">Premium</span-->
                           <div class="mt-4">
                               <a href="javascript:void(0);" class="btn btn-pill btn-air btn-bold btn-danger external playsong"  data-audio='{"name":"{!! ucwords($data->track->name) !!}", "artist":"{!! ucwords($data->track->primary_artist) !!}", "album":"{!! ucwords($data->track->getRelease->name) !!}", "url":"{!! $data->track->song !!}", "cover_art_url":"{!! $data->track->getRelease->image !!}" }'>Play</a>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6">
                           <div class="pt-3 pt-lg-0 text-lg-right">
                               @if(!isset($userdata))
                                    <button class="btn btn-pill btn-air btn-danger btn-icon-only" data-toggle="modal" data-target="#singin">
                                   <i class="la la-heart-o"></i>
                                </button> 
                                <button class="btn btn-pill btn-air btn-warning btn-icon-only" data-toggle="modal" data-target="#singin">
                                    <i class="la la-plus"></i>
                                </button>
                                <?php /*<button class="btn btn-pill btn-air btn-success btn-icon-only" data-toggle="modal" data-target="#singin">
                                    <i class="la la-download"></i>
                                </button> */ ?> 
                                  <button class="btn btn-pill btn-air btn-brand btn-icon-only add_share" data-track_id="{{$data->track->id}}" data-track_name="{{$data->track->name}}" data-type="Track" data-status="1">
                                    <i class="la la-share-alt"></i>
                                </button>
                                    @else
                                    <button class="btn btn-pill btn-air btn-danger btn-icon-only add_favorite" data-track_id="{{$data->track->id}}" data-type="Track" data-status="1">
                                   <i class="la la-heart-o"></i>
                                </button> 
                                <button class="btn btn-pill btn-air btn-warning btn-icon-only add_playlist" data-track_id="{{$data->track->id}}" data-type="Track" data-status="1">
                                    <i class="la la-plus"></i>
                                </button>
                                <?php /*<button class="btn btn-pill btn-air btn-success btn-icon-only add_download"  data-track_id="{{$data->track->id}}" data-type="Track" data-status="1">
                                    <i class="la la-download"></i>
                                </button>*/ ?>
                                <button class="btn btn-pill btn-air btn-brand btn-icon-only add_share" data-track_id="{{$data->track->id}}" data-track_name="{{$data->track->name}}" data-type="Track" data-status="1">
                                    <i class="la la-share-alt"></i>
                                </button>                                
                                @endif 
                                
                            </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="section">
                  <ul class="nav nav-tabs line-tabs line-tabs-primary text-uppercase mb-4" id="songDetails" role="tablist">
                     <li class="nav-item"><a class="nav-link active" id="overview-tab" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="true">Overview</a></li>
                     <li class="nav-item"><a class="nav-link" id="lyrics-tab" data-toggle="tab" href="#lyrics" role="tab" aria-controls="lyrics" aria-selected="false">Track</a></li>
                  </ul>
                  <div class="tab-content" id="songDetailsContent">
                     <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview-tab">
                        <ul class="list-group list-group-flush">
                           <li class="list-group-item pl-0 border-0">Artist by: <span class="font-weight-bold">{{$data->track->primary_artist}}</span></li>
                           <li class="list-group-item pl-0 border-0">Compose by: <span class="font-weight-bold">{{$data->track->composer}}</span></li>
                           <li class="list-group-item pl-0 border-0">Lyrics by: <span class="font-weight-bold">{{$data->track->author}}</span></li>
                           <li class="list-group-item pl-0 border-0">Music Producer: <span class="font-weight-bold">{{$data->track->producer}}</span></li>
                           <li class="list-group-item pl-0 border-0">Remixer: <span class="font-weight-bold">{{$data->track->remixer}}</span></li>
                           <li class="list-group-item pl-0 border-0">Downloads: <span class="font-weight-bold">10,234,014</span></li>
                        </ul>
                     </div>
                     <div class="tab-pane fade show" id="lyrics" role="tabpanel" aria-labelledby="lyrics-tab">
                        <div class="row section">
                        <div class="col-md-12">
                        <div class="custom-list">
                          @if(!empty($data->track_list->data))
                          @foreach($data->track_list->data as $row)
                           <div class="custom-list--item">
                               <a href="javascript:void(0);" class="playsong"  data-audio='{"name":"{!! ucwords($row->name) !!}", "artist":"{!! ucwords($row->primary_artist) !!}", "album":"{!! ucwords($row->name) !!}", "url":"{!! $row->song !!}", "cover_art_url":"{!! $row->getRelease->image !!}" }'>

                              <div class="text-dark custom-card--inline">
                                 <div class="custom-card--inline-img">
                                     <img src="{{$row->getRelease->image}}" alt="{{$row->getRelease->name}}" class="card-img--radius-sm"></div>
                                 <div class="custom-card--inline-desc">
                                    <p class="text-truncate mb-0">{{$row->name}}</p>
                                    <p class="text-truncate text-muted font-sm">{{$row->composer}}</p>
                                 </div>
                              </div>
                               </a>
                              <ul class="custom-card--labels d-flex ml-auto">
                                 <li>
                                     <span class="badge badge-pill badge-warning">
                                         <i class="la la-star"></i>
                                     </span>
                                 </li>
                                 <li class="hard_icon_{{$row->id}}">
                                     @if($row->is_favorite==1)
                                     <span class="badge badge-pill badge-danger add_favorite" data-track_id="{{$row->id}}" data-type="Track" data-status="0"><i class="la la-heart"></i></span>
                                     @endif                                     
                                 </li>
                                 <li>{{$row->duration}}</li>
                                 <li class="dropleft">
                                    <a href="javascript:void(0);" class="btn btn-icon-only p-0 w-auto h-auto" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                                    <ul class="dropdown-menu">
                                             @if(!isset($userdata))
                                     <li class="dropdown-item">
                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#singin" class="dropdown-link">
                                            <i class="la la-heart-o"></i>
                                             <span>Favorite</span>
                                        </a>
                                    </li>
                                    <li class="dropdown-item">
                                         <a href="javascript:void(0);" data-toggle="modal" data-target="#singin" class="dropdown-link">
                                            <i class="la la-plus"></i>
                                            <span>Add to Playlist</span>
                                         </a>
                                    </li>
                                    <?php /*<li class="dropdown-item">
                                         <a href="javascript:void(0);" data-toggle="modal" data-target="#singin" class="dropdown-link">
                                            <i class="la la-download"></i>
                                            <span>Download</span>
                                        </a>
                                    </li>
                                    */ ?>
                                      <li class="dropdown-item">
                                        <a href="javascript:void(0);" class="dropdown-link" data-toggle="modal" data-target="#singin">
                                            <i class="la la-share-alt"></i>
                                            <span>Share</span>
                                        </a>
                                    </li>
                                    @else
                                    <li class="dropdown-item">
                                        <a href="javascript:void(0);" class="dropdown-link add_favorite" data-track_id="{{$row->id}}" data-type="Track" data-status="1">
                                            <i class="la la-heart-o"></i>
                                             <span>Favorite</span>
                                        </a>
                                    </li>
                                    <li class="dropdown-item">
                                        <a href="javascript:void(0);" class="dropdown-link add_playlist" data-track_id="{{$row->id}}"  data-type="Track" data-status="1">
                                            <i class="la la-plus"></i>
                                            <span>Add to Playlist</span>
                                        </a>                                   
                                    </li>
                                    <li class="dropdown-item">
                                        <a href="javascript:void(0);" class="dropdown-link add_share" data-track_id="{{$row->id}}" data-track_name="{{$row->name}}"  data-type="Track" data-status="1">
                                            <i class="la la-share-alt"></i>
                                            <span>Share</span>
                                        </a>
                                    </li>
                                   <?php /*  <li class="dropdown-item">
                                        <a href="javascript:void(0);" class="dropdown-link add_download" data-track_id="{{$row->id}}"  data-type="Track" data-status="1" >
                                            <i class="la la-download"></i>
                                            <span>Download</span>
                                        </a>
                                    </li> */ ?>
                                    @endif                                       
                                       <li class="dropdown-item">
                                           <a href="{{route('song.details',['id'=>base64_encode($row->id)])}}" class="dropdown-link">
                                               <i class="la la-info-circle"></i> 
                                               <span>Song Info</span>
                                           </a>
                                       </li>
                                    </ul>
                                 </li>
                              </ul>
                           </div>
                          @endforeach
                          @endif                        
                        </div>
                  
               </div>
               </div>
                        
                     </div>
                  </div>
               </div>
                            
                            
               <div class="section">
                  <div class="heading">
                     <div class="d-flex flex-wrap align-items-end">
                        <div class="flex-grow-1">
                           <h4>Songs</h4>
                        </div>
                        <!--a href="songs.html" class="btn btn-sm btn-pill btn-air btn-primary">View Album</a-->
                     </div>
                     <hr>
                  </div>
                  <div class="carousel-item-6 arrow-pos-3">
                    @if(isset($data->release->data))
                        @foreach($data->release->data as $row)
                        <div class="custom-card">
                            <div class="custom-card--img">
                               <div class="custom-card--info">
                                  <div class="dropdown dropdown-icon">
                                     <a href="javascript:void(0);" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ion-md-more"></i></a>
                                     <ul class="dropdown-menu dropdown-menu-right">
                                        @if(!isset($userdata))
                                        <li class="dropdown-item"><a href="javascript:void(0);" class="dropdown-link" data-toggle="modal" data-target="#singin"><i class="la la-heart-o"></i></a></li>
                                        <li class="dropdown-item"><a href="javascript:void(0);" class="dropdown-link" data-toggle="modal" data-target="#singin"><i class="la la-plus"></i></a></li>
                                        <!--li class="dropdown-item"><a href="javascript:void(0);" class="dropdown-link" data-toggle="modal" data-target="#singin"><i class="la la-download"></i></a></li-->
                                        <li class="dropdown-item"><a href="javascript:void(0);" class="dropdown-link" data-toggle="modal" data-target="#singin"><i class="la la-share-alt"></i></a></li>
                                        @else
                                        <li class="dropdown-item"><a href="javascript:void(0);" class="dropdown-link add_favorite" data-track_id="{{$row->get_track->id}}" data-type="Track" data-status="1"><i class="la la-heart-o"></i></a></li>
                                        <li class="dropdown-item"><a href="javascript:void(0);" class="dropdown-link add_playlist" data-track_id="{{$row->get_track->id}}" data-type="Track" data-status="1"><i class="la la-plus"></i></a></li>
                                        <!--li class="dropdown-item"><a href="javascript:void(0);" class="dropdown-link" data-toggle="modal" data-target="#singin"><i class="la la-download"></i></a></li-->
                                        <li class="dropdown-item"><a href="javascript:void(0);" class="dropdown-link  add_share" data-track_id="{{$row->get_track->id}}" data-track_name="{{$row->get_track->name}}" data-type="Track" data-status="1"><i class="la la-share-alt"></i></a></li> 
                                        @endif
                                     </ul>
                                  </div>
                               </div>
                                <a href="javascript:void(0);" class="playsong"  data-audio='{"name":"{!! ucwords($row->get_track->name) !!}", "artist":"{!! ucwords($row->get_track->primary_artist) !!}", "album":"{!! ucwords($row->name) !!}", "url":"{!! $row->get_track->song !!}", "cover_art_url":"{!! $row->image !!}" }'>
                                    <img src="{!! $row->image !!}" alt="{!! ucwords($row->get_track->name) !!}" class="card-img--radius-lg">
                                </a>
                            </div>
                              <a href="{{route('song.details',['id'=>base64_encode($row->get_track->id)])}}" class="custom-card--link mt-2">
                                <h6>{!! ucwords($row->get_track->name) !!}</h6>
                                <p>{!! ucwords($row->get_track->primary_artist) !!}</p>
                             </a>
                         </div>
                        @endforeach
                    @endif
                  </div>
               </div>
            </div>
         

<style>
    /*!* Star Rating
* @version: 3.1.4
* @author: Paul Ryley (http://geminilabs.io)
* @url: https://github.com/pryley/star-rating.js
* @license: MIT*/.gl-star-rating[data-star-rating]{position:relative;display:block}.gl-star-rating[data-star-rating]>select{overflow:hidden;visibility:visible!important;position:absolute!important;top:0;width:1px;height:1px;clip:rect(1px,1px,1px,1px);-webkit-clip-path:circle(1px at 0 0);clip-path:circle(1px at 0 0);white-space:nowrap}.gl-star-rating[data-star-rating]>select::before,.gl-star-rating[data-star-rating]>select::after{display:none!important}.gl-star-rating-ltr[data-star-rating]>select{left:0}.gl-star-rating-rtl[data-star-rating]>select{right:0}.gl-star-rating[data-star-rating]>select:focus+.gl-star-rating-stars::before{opacity:.5;display:block;position:absolute;width:100%;height:100%;content:'';outline:dotted 1px currentColor;pointer-events:none}.gl-star-rating-stars{position:relative;display:inline-block;height:26px;vertical-align:middle;cursor:pointer}.gl-star-rating-stars>span{display:inline-block;width:24px;height:24px;background-size:24px;background-repeat:no-repeat;background-image:url(img/star-empty.svg);margin:0 4px 0 0}.gl-star-rating-stars>span:last-of-type{margin-right:0}.gl-star-rating-rtl[data-star-rating] .gl-star-rating-stars>span{margin:0 0 0 4px}.gl-star-rating-rtl[data-star-rating] .gl-star-rating-stars>span:last-of-type{margin-left:0}.gl-star-rating-stars.s10>span:nth-child(1),.gl-star-rating-stars.s20>span:nth-child(-1n+2),.gl-star-rating-stars.s30>span:nth-child(-1n+3),.gl-star-rating-stars.s40>span:nth-child(-1n+4),.gl-star-rating-stars.s50>span:nth-child(-1n+5),.gl-star-rating-stars.s60>span:nth-child(-1n+6),.gl-star-rating-stars.s70>span:nth-child(-1n+7),.gl-star-rating-stars.s80>span:nth-child(-1n+8),.gl-star-rating-stars.s90>span:nth-child(-1n+9),.gl-star-rating-stars.s100>span{background-image:url(img/star-full.svg)}.gl-star-rating-text{display:inline-block;position:relative;height:26px;line-height:26px;font-size:.8em;font-weight:600;color:#fff;background-color:#1a1a1a;white-space:nowrap;vertical-align:middle;padding:0 12px 0 6px;margin:0 0 0 12px}.gl-star-rating-text::before{position:absolute;top:0;left:-12px;width:0;height:0;content:"";border-style:solid;border-width:13px 12px 13px 0;border-color:transparent #1a1a1a transparent transparent}.gl-star-rating-rtl[data-star-rating] .gl-star-rating-text{padding:0 6px 0 12px;margin:0 12px 0 0}.gl-star-rating-rtl[data-star-rating] .gl-star-rating-text::before{left:unset;right:-12px;border-width:13px 0 13px 12px;border-color:transparent transparent transparent #1a1a1a}
</style>
@else
{{$data['message']}}
@endif
@endsection