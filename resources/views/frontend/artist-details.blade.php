@extends('layouts.frontend.master')
@section('content')
@if(isset($data) && $data['status']==true) 
<?php $userdata =  Session::get('userData');
$data = $data['data'];

 ?>
<style>
    .banner{
            background-image: url('{!! $data->image !!}');
            
            width: 140%;
            background-repeat: no-repeat;
            background-position: center center;
            background-size: cover;
            -webkit-filter: brightness(0.9) blur(30px);
    }
</style>

            <div class="main-container" id="appRoute">
                <?php /* <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- Blow Add 2 -->
                <ins class="adsbygoogle"
                     style="display:inline-block;width:100%;height:90px"
                     data-ad-client="ca-pub-9974854305750285"
                     data-ad-slot="3579376846"></ins>
                <script>
                     (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
               */ ?>
               <div class="row section text-center text-md-left">
                  <div class="col-xl-3 col-lg-4 col-sm-5">
                      <img src="{{$data->image}}" alt="" class="card-img--radius-lg">
                  </div>
                   <div class="col-xl-9 col-lg-9  col-sm-7">
                     <div class="row pt-4">
                        <div class="col-xl-8 col-lg-6">
                            
                        @section('title',ucwords($data->name))
                        @section('description',ucwords($data->name))
                        @section('keywords',ucwords($data->name))
                        @section('metadata')
                    
                        <link rel="manifest" href="/manifest.json"> 
                        <link rel="dns-prefetch" href="//mp3bajao.com"> 
                        <link rel="dns-prefetch" href="//css375.mp3bajao.com"> 
                        <link rel="dns-prefetch" href="//css5.mp3bajao.com"> 
                        <link rel="dns-prefetch" href="//static.mp3bajao.com"> 
                        <link rel="dns-prefetch" href="//a1.mp3bajao.com"> 
                        <link rel="dns-prefetch" href="//a2.mp3bajao.com"> 
                        <link rel="dns-prefetch" href="//a10.mp3bajao.com"> 
                        <link rel="dns-prefetch" href="https://mp3bajao.com"> 
                        <link rel="dns-prefetch" href="https://jssocdn.indiatimes.com"> 
                        <link rel="dns-prefetch" href="https://cdn.moengage.com"> 
                        <link rel="dns-prefetch" href="https://www.google-analytics.com"> 
                        <link rel="dns-prefetch" href="https://apis.google.com"> 
                        <link rel="dns-prefetch" href="https://stats.g.doubleclick.net"> 
                        <link rel="dns-prefetch" href="https://connect.facebook.net"> 
                        <link rel="dns-prefetch" href="https://www.google.co.in"> 
                        <link rel="dns-prefetch" href="https://www.gstatic.com"> 
                        <link rel="dns-prefetch" href="https://www.google.com"> 
                        <link rel="dns-prefetch" href="http://b.scorecardresearch.com"> 
                        <link rel="dns-prefetch" href="https://b-s.tercept.com"> 
                        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
                        <meta name="google-site-verification" content="Isu9IQakMXodVZHO_6tV6DUNWSRF7s9I5FY-_lLKqNA"> 
                        <meta name="msvalidate.01" content="1E6490AA5F9D0438A03769318F2A1088"> 
                        <meta name="twitter:card" content="summary"> 
                      
                        <meta name="twitter:url" content="{{route('artist.details',['id'=>base64_encode($data->id)])}}"> 
                        <meta name="twitter:site" content="@gaana"> 
                        <meta name="twitter:title" content="Song by {{$data->name}} on Mp3bajao.com"> 
                        <meta name="twitter:description" content=" Song by {{$data->name}} the album {{$data->name}} song online free on Mp3bajao.com."> 
                        <meta name="twitter:image" content="{!! str_replace('size=0.4','size=1',$data->image) !!}"> 
                        <meta name="twitter:app:name:googleplay" content="Mp3bajao"> <meta name="twitter:app:id:googleplay" content="com.mp3bajao"> 
                        <meta name="twitter:app:url:googleplay" content="mp3bajao://share/titemI29547130"> <meta name="twitter:app:name:iphone" content="Mp3bajao"> 
                        <meta name="twitter:app:id:iphone" content="585270521"> 
                        <meta name="twitter:app:url:iphone" content="mp3bajaoApp://share/titemI29547130">  
                        <meta name="format-detection" content="telephone=no"> 
                        <meta property="fb:app_id" content="183019041719404"> 
                        <meta property="og:site_name" content="Mp3bajao.com"> 
                        <meta property="og:type" content="music.song"> 
                        <meta property="og:url" content="{{route('artist.details',['id'=>base64_encode($data->id)])}}"> 
                        <meta property="og:title" content="Song by {{$data->name}} on Mp3bajao.com"> 
                        <meta property="og:description" content=" Song by {{$data->name}} the album {{$data->name}} song online free on Mp3bajao.com."> 
                        <meta property="og:image" content="{!! str_replace('size=0.4','size=1',$data->image) !!}">  
                        <meta property="og:image:width" content="450"> <meta property="og:image:height" content="450">  
                        <meta property="music:album" content="{{route('artist.details',['id'=>base64_encode($data->id)])}}">  
                        <meta property="og:audio" content="{{route('artist.details',['id'=>base64_encode($data->id)])}}">   
                        <meta property="music:duration" content="4">    
                        <link rel="canonical" href="{{route('artist.details',['id'=>base64_encode($data->id)])}}">   
                        <link rel="amphtml" href="{{route('artist.details',['id'=>base64_encode($data->id)])}}">  
                        <!--link rel="alternate" href="android-app://com.gaana/gaanagoogle/song/dus-bahane-20">  
                        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no"--> 
                    
                        @endsection
                            
                            
                           <h5>{{ucfirst($data->name)}}</h5>
                           
                           <div class="text-warning stars">
                               <span class="gl-star-rating-stars rating_num s{{$data->rating}}0">
                                    @if(!isset($userdata))
                                    <span data-value="1" data-track_id="{{$data->id}}"></span>
                                    <span data-value="2" data-track_id="{{$data->id}}"></span>
                                    <span data-value="3" data-track_id="{{$data->id}}"></span>
                                    <span data-value="4" data-track_id="{{$data->id}}"></span>
                                    <span data-value="5" data-track_id="{{$data->id}}"></span>
                                   @else
                                    <span class="addRating" data-value="1" data-track_id="{{$data->id}}" data-type="Artist"></span>
                                    <span class="addRating" data-value="2" data-track_id="{{$data->id}}"  data-type="Artist"></span>
                                    <span class="addRating" data-value="3" data-track_id="{{$data->id}}"  data-type="Artist"></span>
                                    <span class="addRating" data-value="4" data-track_id="{{$data->id}}"  data-type="Artist"></span>
                                    <span class="addRating" data-value="5" data-track_id="{{$data->id}}"  data-type="Artist"></span>
                                   @endif
                               </span>
                               
                            </div>
                           <div class="mt-4">
                                <a href="javascript:void(0);" class="btn btn-pill btn-air btn-bold btn-default btn-sm " style="font-size:16px" onclick="playListgroup()"><i class="la la-play"></i> Play</a>
                                 @if(!isset($userdata))
                                <a href="javascript:void(0);" class="btn btn-pill btn-air btn-bold btn-default btn-sm" data-toggle="modal" data-target="#singin" style="font-size:16px">                                     
                                    <i class="la la-heart-o "></i>
                                    &nbsp; {{$data->total_like}}
                                </a>
                                @else
                                <a href="javascript:void(0);" class="btn btn-pill btn-air btn-bold btn-default btn-sm add_favorite add_favorite_status_{{$data->id}}"  data-track_id="{{$data->id}}" data-type="{{$data->from}}" data-status="@if($data->is_like==1) 0  @else 1 @endif" style="font-size:16px"  > 
                                    <i class="{{$data->from}}_{{$data->id}} la @if($data->is_like==1) la-heart  @else la-heart-o @endif"></i>
                                    &nbsp; {{$data->total_like}}
                                </a>
                                @endif
                                <a href="javascript:void(0);" class="btn btn-pill btn-air btn-bold btn-default btn-sm add_share" data-track_id="{{$data->id}}" data-track_name="{{$data->name}}"  data-type="{{$data->from}}" data-status="1"  style="font-size:16px"> <i class="la la-share-alt"></i> &nbsp; {{$data->total_sharing}}</a> 
                                <a href="javascript:void(0);" class="btn btn-pill btn-air btn-bold btn-default btn-sm "  style="font-size:16px"> <i class="la la-eye"></i> &nbsp; {{$data->total_views}}</a>
                            </div>
                     
                        
                       
                    
                        <div class="mt-4">
                            
                            <div class="folowItems">
                                
                                <div class="folowItem" style="padding-right:0px">
                                        @if(!isset($userdata))
                                        <a href="javascript:void(0);"  data-toggle="modal" data-target="#singin" >
                                            <button type="button" class="btn btn-sm btn-primary" >
                                        <i class="la la-plus"></i> Follow</button>                                            
                                        </a>
                                        @else
                                        @if($data->is_follow==0)
                                    <button type="button" class="btn btn-sm btn-primary addFollow" data-id="{{$data->id}}"  data-status="1">
                                        <i class="la la-plus"></i> Follow</button>
                                        @else
                                    <button type="button" class="btn btn-sm btn-primary addFollow" data-id="{{$data->id}}"  data-status="0">
                                        <i class="la la-plus"></i>Unfollow</button>                                        
                                        @endif
                                        @endif
                                </div>
                                <div class="folowItem" style="text-align: center;">{{count($data->track_list)}}
                                    <br/> Songs
                                </div>
                                <div class="folowItem" style="text-align: center;">{{number_format($data->follow)}}
                                    <br/> Following
                                </div>
                                <div class="folowItem" style="text-align: center;">{{ number_format($data->unfollow)}}<br>
                                Un follows
                                </div>
                            </div>
 </div>
                              </div>
                         </div>
                  </div>

               </div>
               <div class="section">
                  <ul class="nav nav-tabs line-tabs line-tabs-primary text-uppercase mb-4" id="artistDetails" role="tablist">
                     <li class="nav-item"><a class="nav-link active" id="song-tab" data-toggle="tab" href="#song" role="tab" aria-controls="song" aria-selected="true">Top Songs</a></li>
                     <!--li class="nav-item"><a class="nav-link" id="album-tab" data-toggle="tab" href="#album" role="tab" aria-controls="album" aria-selected="false">Top Albums</a></li>
                     <li class="nav-item"><a class="nav-link" id="bio-tab" data-toggle="tab" href="#bio" role="tab" aria-controls="bio" aria-selected="false">Biography</a></li-->
                  </ul>
                  <div class="tab-content" id="artistDetailsContent">
                     <div class="tab-pane fade show active" id="song" role="tabpanel" aria-labelledby="song-tab">
                        <div class="row align-items-end">
                           <span class="col-6 font-weight-bold">{{count($data->track_list)}} Track</span>
                           <div class="col-6 ml-auto">
                              <form action="#" class="form-inline justify-content-end">
                                 <label class="mr-2" for="filter1">Sorted By:</label> 
                                 <select class="custom-select mr-sm-2" id="filter1">
                                    <option selected="selected">Popular</option>
                                    <option value="1">A-Z</option>
                                    <option value="2">Z-A</option>
                                 </select>
                              </form>
                           </div>
                           <div class="col-12">
                              <hr>
                           </div>
                        </div>
                        <div class="custom-list">
                             <table class="table table-dark">
                                <thead>
                                   <tr>
                                    <th width="5%"></th>
                                    <th width="5%">#</th>
                                    <th width="30%">Title</th>
                                    <th width="30%">Atrist</th>
                                    <th width="10%"></th>
                                    <th width="5%"></th>
                                    <th width="10%">Time</th>
                                    <th width="5%"></th>
                                   </tr>
                                </thead>
                                <tbody>
                            <script>
                            var newlistdata = '';
                            </script>
                          @if(!empty($data->track_list))
                          @foreach($data->track_list as $key => $row)
                          <tr>
                              <td>
                                <ul class="list-group list-group-horizontal">
                                    <li class="list-group-item listBox" style="30px">
                                         <script>
                                         var  newlist =  '{"id":"{!! $row->id!!}","name":"{!! ucwords($row->name) !!}", "artist":"{!! ucwords($row->primary_artist) !!}", "album":"{!! ucwords($row->name) !!}", "url":"{!! $row->song !!}", "cover_art_url":"{!! $row->image !!}","duration":"{!! $row->duration !!}" }';
                                        </script>
                                        @if($key==0)
                                        <script>
                                          newlistdata = newlist; 
                                         </script>
                                         @else
                                         <script>
                                            newlistdata = newlistdata+','+newlist;
                                         </script>
                                        @endif
                                        <a href="javascript:void(0);" class="playsong"   data-id="{{$row->id}}" data-formate="Track"  data-audio='{"id":{!! $row->id!!},"name":"{!! ucwords($row->name) !!}", "artist":"{!! ucwords($row->primary_artist) !!}", "album":"{!! ucwords($row->name) !!}", "url":"{!! $row->song !!}", "cover_art_url":"{!! $row->image !!}","duration":"{!! $row->duration !!}" }'>
                                            <div class="eq-white eq-white-{{$row->id}}" style="display:none;">
                                                <span class="eq-bar eq-bar--1"></span> 
                                                <span class="eq-bar eq-bar--2"></span> 
                                                <span class="eq-bar eq-bar--3"></span> 
                                                <span class="eq-bar eq-bar--4"></span> 
                                                <span class="eq-bar eq-bar--5"></span> 
                                                <span class="eq-bar eq-bar--6"></span>
                                            </div>                                            
                                            <i class="la la-play play-white play-white-{{$row->id}}" style="font-size:30px;"></i>
                                        </a>
                                    </li>
                                </ul>
                              </td>
                             
                              <td>
                                  <div class="custom-card--inline-img">
                                       <img src="{{$row->image}}" alt="{{$row->name}}" class="card-img--radius-sm" style="max-width: 40px;">
                                   </div>
                              </td>
                        
                              <td>
                                   <div class="custom-card--inline-desc">
                                       <p class="text-truncate mb-0"><a href="{{route('song.details',['id'=>base64_encode($row->id),'from'=>'Track'])}}">{{$row->name}}</a></p>                                      
                                   </div>
                              </td>
                              <td>
                                  <div class="custom-card--inline-desc">
                                  <p class="text-truncate font-sm">
                                  
                                      <?php $artists = explode('|',$row->primary_artist); ?>
                                      @foreach($artists as $key => $artist)
                                      <a href="{{route('artist.details',['id'=>base64_encode($artist)])}}">{{ucfirst($artist)}}</a> 
                                      @if((count($artists)-1)!=$key)
                                      | 
                                      @endif
                                      @endforeach
                                  </p>
                                  </div>
                              </td>
                              <td>
                                  <ul class="list-group list-group-horizontal">
                                        @if(!isset($userdata)) 
                                        <li class="list-group-item listBox">
                                            <a href="javascript:void(0);" data-toggle="modal" data-target="#singin" class="dropdown-link">
                                                 @if(isset($row->is_favorite) &&  $row->is_favorite==1)
                                                 <i class="la la-heart"></i>
                                                 @else
                                                 <i class="la la-heart-o"></i>
                                                 @endif
                                                
                                            </a>                                            
                                        </li>
                                        <li class="list-group-item listBox">                                            
                                            <a href="javascript:void(0);" data-toggle="modal" data-target="#singin" class="dropdown-link">
                                                <i class="la la-plus-square"></i>
                                             </a>
                                        </li>                                        
                                        @else
                                        <li class="list-group-item listBox">
                                            <a href="javascript:void(0);" class="dropdown-link add_favorite add_favorite_status_{{$row->id}}"  data-track_id="{{$row->id}}" data-type="Track" data-status="@if($row->is_like==1) 0  @else 1 @endif"   > 
                                                <i class="Track_{{$row->id}} la @if($row->is_like==1) la-heart  @else la-heart-o @endif"></i>                                                
                                            </a>
                                        </li>
                                        
                                        <li class="list-group-item listBox">                                            
                                            <a href="javascript:void(0);"  class="dropdown-link  add_playlist" data-track_id="{{$row->id}}" data-type="Track" data-status="1">
                                                <i class="la la-plus-square"></i>
                                             </a>
                                        </li>                                        
                                        @endif
                                        <li class="list-group-item listBox">
                                            <a href="javascript:void(0);" class="dropdown-link add_share" data-track_id="{{$row->id}}" data-track_name="{{$row->name}}"  data-type="Track" data-status="1">
                                                <i class="la la-share-alt"></i>
                                            </a>
                                        </li>
                                    </ul>                                  
                              </td>
                              <td></td>
                              <td> {{$row->duration}}</td>
                              <td>
                                 <li class="dropleft">

                                    <a href="javascript:void(0);" class="btn btn-icon-only p-0 w-auto h-auto" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                                    <ul class="dropdown-menu" style="padding-left: 5px;">                                        
                                        <li class="dropdown-item">
                                           <a href="javascript:void(0);" class="dropdown-link songinfo">
                                               <i class="la la-info-circle"></i> 
                                               <span>Song Info</span>
                                           </a>
                                        </li>
                                        <li class="dropdown-item">
                                        <a href="javascript:void(0);" class="dropdown-link add_download" data-track_id="{{$row->id}}"  data-type="Track" data-status="1" >
                                            <i class="la la-download"></i>
                                            <span>Download</span>
                                        </a>
                                    </li>
                                    <li class="dropdown-item">
                                        <a href="javascript:void(0);" class="dropdown-link add_to_quee" data-track_id="{{$row->id}}"  data-type="Track" data-status="1" >
                                            <i class="la la-list-ul"></i>
                                            <span>Add To Queue</span>
                                        </a>
                                    </li>
                                    <li class="dropdown-item">
                                        <a href="javascript:void(0);" >
                                            <i class="la la-file-audio"></i>
                                            <span>Go To Album</span>
                                        </a>
                                    </li>
                                    </ul>
                                 </li>                                  
                              </td>                              
                          </tr>                       
                          @endforeach
                          @endif  
                          </tbody>
                        </table>                      
                        </div>
                     </div>
                 </div>
               </div>
                
 @if(!empty($data->release_latest))
                <div class="section">
                  <div class="heading">
                     <div class="d-flex flex-wrap align-items-end">
                        <div class="flex-grow-1">
                           <h4>Latest Songs</h4>
                        </div>
                        <a href="{{route('song')}}" class="btn btn-sm btn-pill btn-air btn-primary">View Album</a>
                     </div>
                     <hr>
                  </div>

                  <div class="carousel-item-6 arrow-pos-3">
                   
                        @foreach($data->release_latest as $row)
                        <div class="custom-card">
                            
                            
                            <div class="custom-card--img">
                        <a href="{{route('song.details',['id'=>base64_encode($row->id),'from'=>'Album'])}}" class="playsong"  data-id="{{$row->track_id}}" data-formate="Track"  data-audio='{"id":"{!! ucwords($row->track_id) !!}","name":"{!! ucwords($row->name) !!}", "artist":"{!! ucwords($row->primary_artist) !!}", "album":"{!! ucwords($row->name) !!}", "url":"{!! $row->song !!}", "cover_art_url":"{!! $row->image !!}" }'>                                

                            <div class="custom-card--info">                            
                              <div class="dropdown dropdown-icon">
                                                               <span class="playiconnew"><img src="{{asset('frontend/images/logos/iconPlay.svg')}}"</span>
                              </div>
                            </div>             
                            <img src="{!! $row->image !!}" alt="{!! ucwords($row->name) !!}" class="card-img--radius-lg">
                            </a>
                              
                            </div>
                              <a href="{{route('song.details',['id'=>base64_encode($row->id),'from'=>'Album'])}}" class="custom-card--link mt-2">
                                <h6>{!! ucwords($row->name) !!}</h6>
                                <p>{!! ucwords($row->primary_artist) !!}</p>
                             </a>
                         </div>
                        @endforeach
                 
                  </div>
               </div>
                   @endif

                    @if(!empty($data->release_list))
                <div class="section">
                  <div class="heading">
                     <div class="d-flex flex-wrap align-items-end">
                        <div class="flex-grow-1">
                           <h4>Releated Songs</h4>
                        </div>
                        <a href="{{route('song')}}" class="btn btn-sm btn-pill btn-air btn-primary">View Album</a>
                     </div>
                     <hr>
                  </div>

                  <div class="carousel-item-6 arrow-pos-3">
                
                        @foreach($data->release_list as $row)
                        <div class="custom-card">
                            
                            
                            <div class="custom-card--img">
                        <a href="{{route('song.details',['id'=>base64_encode($row->id),'from'=>'Album'])}}" class="playsong"  data-audio='{"id":"{!! ucwords($row->track_id) !!}","name":"{!! ucwords($row->name) !!}", "artist":"{!! ucwords($row->primary_artist) !!}", "album":"{!! ucwords($row->name) !!}", "url":"{!! $row->song !!}", "cover_art_url":"{!! $row->image !!}" }'>                                

                            <div class="custom-card--info">                            
                              <div class="dropdown dropdown-icon">
                                                                <span class="playiconnew"><img src="{{asset('frontend/images/logos/iconPlay.svg')}}"</span>
                              </div>
                            </div>             
                            <img src="{!! $row->image !!}" alt="{!! ucwords($row->name) !!}" class="card-img--radius-lg">
                            </a>
                              
                            </div>
                              <a href="{{route('song.details',['id'=>base64_encode($row->id),'from'=>'Album'])}}" class="custom-card--link mt-2">
                                <h6>{!! ucwords($row->name) !!}</h6>
                                <p>{!! ucwords($row->primary_artist) !!}</p>
                             </a>
                         </div>
                        @endforeach
               
                  </div>
               </div>
                         @endif
                
                
                  @if(isset($data->playlist))
                <div class="section">
                  <div class="heading">
                     <div class="d-flex flex-wrap align-items-end">
                        <div class="flex-grow-1">
                           <h4>Playlist</h4>
                        </div>
                     </div>
                     <hr>
                  </div>
                  <div class="carousel-item-6 arrow-pos-3">
          
                        @foreach($data->playlist as $row)
                        <div class="custom-card">
                            
                            
                            <div class="custom-card--img">
                        <a href="{{route('song.details',['id'=>base64_encode($row->id),'from'=>'Playlist'])}}" class="playsong"  data-audio='{"id":"{!! ucwords($row->track_id) !!}","name":"{!! ucwords($row->name) !!}", "artist":"{!! ucwords($row->primary_artist) !!}", "album":"{!! ucwords($row->name) !!}", "url":"{!! $row->song !!}", "cover_art_url":"{!! $row->image !!}" }'>                                

                            <div class="custom-card--info">                            
                              <div class="dropdown dropdown-icon">
                                                                <span class="playiconnew"><img src="{{asset('frontend/images/logos/iconPlay.svg')}}"</span>
                              </div>
                            </div>             
                            <img src="{!! $row->image !!}" alt="{!! ucwords($row->name) !!}" class="card-img--radius-lg">
                            </a>
                              
                            </div>
                              <a href="{{route('song.details',['id'=>base64_encode($row->id),'from'=>'Playlist'])}}" class="custom-card--link mt-2">
                                <h6>{!! ucwords($row->name) !!}</h6>
                                <p>{!! ucwords($row->primary_artist) !!}</p>
                             </a>
                         </div>
                        @endforeach
                
                  </div>
               </div>
                    @endif
                
               <div class="section commentList">
                   <h5>Comment ({{$comment['data']->total}})</h5>
                        <div class="mb-4">
                            <textarea name="comment" id="user_comment" cols="30" rows="5" minlength='0' maxlength='500' class="form-control" placeholder="Please share your thoughts..."></textarea>
                            <div class="text-right mt-2">
                                 @if(isset($userdata)) 
                                <button  type="button" class="btn btn-sm btn-primary addComment" data-type="artist" data-type_id="{{$data->id}}">
                                   <i class="la la-comment"></i> Comment
                                </button>
                                 @else
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#singin">  
                                         <button type="button" class="btn btn-sm btn-primary">
                                             <i class="la la-comment"></i> Comment
                                         </button>
                                    </a>
                                 @endif
                            </div>
                        </div>

                   <div class="commentListData">
                  
                </div>  
                   
                   <div class="col-md-12" style="text-align: center;">
                <a href="javascript:;" class="btn btn-primary btn-pill btn-sm loadMoreComment">Load More</a>
                </div>
            </div>
        </div>
           
<style>
    /*!* Star Rating
* @version: 3.1.4
* @author: Paul Ryley (http://geminilabs.io)
* @url: https://github.com/pryley/star-rating.js
* @license: MIT*/.gl-star-rating[data-star-rating]{position:relative;display:block}.gl-star-rating[data-star-rating]>select{overflow:hidden;visibility:visible!important;position:absolute!important;top:0;width:1px;height:1px;clip:rect(1px,1px,1px,1px);-webkit-clip-path:circle(1px at 0 0);clip-path:circle(1px at 0 0);white-space:nowrap}.gl-star-rating[data-star-rating]>select::before,.gl-star-rating[data-star-rating]>select::after{display:none!important}.gl-star-rating-ltr[data-star-rating]>select{left:0}.gl-star-rating-rtl[data-star-rating]>select{right:0}.gl-star-rating[data-star-rating]>select:focus+.gl-star-rating-stars::before{opacity:.5;display:block;position:absolute;width:100%;height:100%;content:'';outline:dotted 1px currentColor;pointer-events:none}.gl-star-rating-stars{position:relative;display:inline-block;height:26px;vertical-align:middle;cursor:pointer}.gl-star-rating-stars>span{display:inline-block;width:24px;height:24px;background-size:24px;background-repeat:no-repeat;background-image:url({{asset('img/star-empty.svg')}});margin:0 4px 0 0}.gl-star-rating-stars>span:last-of-type{margin-right:0}.gl-star-rating-rtl[data-star-rating] .gl-star-rating-stars>span{margin:0 0 0 4px}.gl-star-rating-rtl[data-star-rating] .gl-star-rating-stars>span:last-of-type{margin-left:0}.gl-star-rating-stars.s10>span:nth-child(1),.gl-star-rating-stars.s20>span:nth-child(-1n+2),.gl-star-rating-stars.s30>span:nth-child(-1n+3),.gl-star-rating-stars.s40>span:nth-child(-1n+4),.gl-star-rating-stars.s50>span:nth-child(-1n+5),.gl-star-rating-stars.s60>span:nth-child(-1n+6),.gl-star-rating-stars.s70>span:nth-child(-1n+7),.gl-star-rating-stars.s80>span:nth-child(-1n+8),.gl-star-rating-stars.s90>span:nth-child(-1n+9),.gl-star-rating-stars.s100>span{background-image:url({{asset('img/star-full.svg')}})}.gl-star-rating-text{display:inline-block;position:relative;height:26px;line-height:26px;font-size:.8em;font-weight:600;color:#fff;background-color:#1a1a1a;white-space:nowrap;vertical-align:middle;padding:0 12px 0 6px;margin:0 0 0 12px}.gl-star-rating-text::before{position:absolute;top:0;left:-12px;width:0;height:0;content:"";border-style:solid;border-width:13px 12px 13px 0;border-color:transparent #1a1a1a transparent transparent}.gl-star-rating-rtl[data-star-rating] .gl-star-rating-text{padding:0 6px 0 12px;margin:0 12px 0 0}.gl-star-rating-rtl[data-star-rating] .gl-star-rating-text::before{left:unset;right:-12px;border-width:13px 0 13px 12px;border-color:transparent transparent transparent #1a1a1a}
</style>
<script>
    $(document).on('click','.addFollow',function(){
        var artist_id = $(this).data('id');
        var status = $(this).data('status');
        var authtoken = $('.logout').data('token');
        var form = new FormData();
        form.append("artist_id", artist_id);
        form.append("status", status);
        if(status==1)
        {
            $(this).attr('status',0);
            $(this).html('<i class="la la-plus"></i>Unfollow');
        }
        else
        {
            $(this).attr('status',1);
            $(this).html('<i class="la la-plus"></i>Follow');            
        }

        var settings = {
          "async": true,
          "crossDomain": true,
          "url": "{{url('api/follows')}}",
          "method": "POST",
          "headers": {
            "authorization": "Bearer "+authtoken,
            "cache-control": "no-cache",
            "postman-token": "f69653d9-062f-f83f-e561-4aaed7ea1d35"
          },
          "processData": false,
          "contentType": false,
          "mimeType": "multipart/form-data",
          "data": form
        }

        $.ajax(settings).done(function (response) {
          console.log(response);
        });
        
    });
    
    $(document).on('click','.addComment',function(){
        var type = $(this).data('type');
        var type_id = $(this).data('type_id');
        var comment = $('#user_comment').val();
        var authtoken = $('.logout').data('token');
        var form = new FormData();
        form.append("comment", comment);
        form.append("table_id", type_id);
        form.append("table_type", type);

        var settings = {
          "async": true,
          "crossDomain": true,
          "url": "{{url('api/add_comment')}}",
          "method": "POST",
          "headers": {
            "authorization": "Bearer "+authtoken,
            "accept": "application/json",
            "cache-control": "no-cache",
            "postman-token": "72a734df-e3ae-6d62-08b8-16728f773b25"
          },
          "processData": false,
          "contentType": false,
          "mimeType": "multipart/form-data",
          "data": form
        }

        $.ajax(settings).done(function (response) {
            console.log(response);
            var i=1;
              $('.commentListData').html('');
            CommentLoading(i);
        });
    });
    
    $(document).on('click','.addReply',function(){
         var authtoken = $('.logout').data('token');
        var comment_id =$(this).data('comment_id');
        var reply = $('#reply_'+comment_id).val();
        
        var form = new FormData();
        form.append("comment_id", comment_id);
        form.append("reply", reply);

        var settings = {
          "async": true,
          "crossDomain": true,
          "url": "{{url('api/add_reply')}}",
          "method": "POST",
          "headers": {
            "authorization": "Bearer "+authtoken,
            "accept": "application/json",
            "cache-control": "no-cache",
            "postman-token": "72a734df-e3ae-6d62-08b8-16728f773b25"
          },
          "processData": false,
          "contentType": false,
          "mimeType": "multipart/form-data",
          "data": form
        }

        $.ajax(settings).done(function (response) {
          console.log(response);
           var i=1;
             $('.commentListData').html('');
            CommentLoading(i);
        });
    });
    

    
    function playListgroup(){
        var  autolist =   localStorage.getItem("listAudio");
        if(autolist!='')
        {
            localStorage.setItem("listAudio",newlistdata+','+autolist);                                  
        }
        else
        {
            localStorage.setItem("listAudio",newlistdata);                                  
        }

        var autolist =   localStorage.getItem("listAudio");
        var listAudio = '['+autolist+']';    
        var listAudio = JSON.parse(listAudio);
        console.log('dfsdf',listAudio);
        var newlistAuddio = [];
        var k=0;
        var myarray = [];
        for(var i=0; i<listAudio.length; i++)
        {
            if(listAudio[i])
            {
                if(jQuery.inArray(listAudio[i].id, myarray) != -1) {              
                console.log("is in array");
                } else {
                    myarray[k] = listAudio[i].id;
                    newlistAuddio[k] = listAudio[i];

                    k++;
                    console.log("is NOT in array");
                }  
            }
        }
        var string = JSON.stringify(newlistAuddio);
        localStorage.setItem("newlistAuddio",string);
        listAudio = newlistAuddio;
        console.log('song-details',listAudio)
        
            creteTrack(listAudio);
        var indexAudio = 0;
        var playListItems = document.querySelectorAll(".playlist-track-ctn");
        var currentAudio = document.getElementById("myAudio");
        getTrackload(playListItems,currentAudio,listAudio);
        toggleAudio();
    }
</script>

<script>
    
$(document).on('click','.replaybox',function(){
    var id = $(this).data('comment_id');
    @if(isset($userdata))
        $('.showreplaybox_'+id).show();         
    @else
        $('#singin').modal('show');             
    @endif
    
    
});
    
var i=1;
CommentLoading(i);
function CommentLoading(page){
    var url = "{{route('comment',['table_type'=>'artist','table_id'=>$data->id])}}";
    url = url+'/'+page;
    
    $('#prossing').show();
   
    $.get(url,function (dados,status, xhr) {        
        if(status == "success")
        {
            $(".commentListData").append(dados);               
        }
        $('#prossing').hide();
    });
}
$(document).on('click','.loadMoreComment',function(){
    i++;
   
    CommentLoading(i);
   
});

</script>
<?php /*
<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Blow page -->
<ins class="adsbygoogle"
     style="display:inline-block;width:100%;height:90px"
     data-ad-client="ca-pub-9974854305750285"
     data-ad-slot="7622563635"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
 */ ?>

        @endif
@endsection