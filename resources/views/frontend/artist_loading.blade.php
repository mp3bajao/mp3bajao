@if(isset($data) && $data['status']==true)
@if(isset($data['data']->data))
@foreach($data['data']->data as $row)
<div class="col-xl-2  col-lg-3 col-sm-4 pb-4">
    <div class="custom-card">
       <div class="custom-card--img">
           <a href="{{route('artist.details',['id'=>base64_encode($row->id)])}}">
               <img src="{{$row->image}}" alt="{{ucwords($row->name)}}" class="card-img--radius-lg round-image">
           </a>
       </div>
       <a href="{{route('artist.details',['id'=>base64_encode($row->id)])}}" class="custom-card--link mt-2">
           <h6 class="mb-0 text-center"><small>{{ucwords($row->name)}}</small></h6>
           <p class="text-center"><small>{{$row->total_views}}</small></p>
       </a>
    </div>
</div>                   
@endforeach
@endif
@endif