@extends('layouts.frontend.master')
@section('content')
@if(isset($data) && $category['status']==true)
 <?php $userdata =  Session::get('userData');?>
    <div class="main-container" id="appRoute">                
        <div class="section">
            <ul class="nav nav-tabs line-tabs line-tabs-primary text-uppercase mb-4" id="songDetails" role="tablist">
                @if($category['data'])
                @foreach($category['data'] as $row)
                <li class="nav-item">
                    <a href="{{route('playlist',['type_id'=>$row->id])}}" style="border: 1px solid #343a40; margin: 5px" class="nav-link @if($row->id==$type_id) active @endif" id="overview-tab" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="true">{{ucwords($row->name)}}</a>
                </li>
                @endforeach
                @endif
            </ul>
            @if($data['status']==true)
        <div class="tab-content" id="songDetailsContent">
            <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview-tab">
               <div class="row align-items-end">
                  <span class="col-6 font-weight-bold">Songs {{number_format(count($data['data']))}}</span>                 
                  <div class="col-12">
                     <hr>
                  </div>
               </div>
               <div class="row section">
              <div class="col-md-12">
                  <div class="custom-list">
                             <table class="table table-dark">
                                <thead>
                                   <tr>
                                    <th width="5%"></th>
                                    <th width="5%">#</th>
                                    <th width="30%">Title</th>
                                    <th width="30%">Atrist</th>
                                    <th width="10%"></th>
                                    <th width="5%"></th>
                                    <th width="10%">Time</th>
                                    <th width="5%"></th>
                                   </tr>
                                </thead>
                                <tbody>
                          @if(!empty($data['data']))
                          @foreach($data['data'] as $row)
                          <tr>
                              <td>
                                <ul class="list-group list-group-horizontal">
                                    <li class="list-group-item listBox" style="30px">
                                        <a href="javascript:void(0);" class="playsong"  data-audio='{"id":"{!! $row->id!!}","name":"{!! ucwords($row->name) !!}", "artist":"{!! ucwords($row->primary_artist) !!}", "album":"{!! ucwords($row->name) !!}", "url":"{!! $row->song !!}", "cover_art_url":"{!! $row->image !!}", "duration":"{!! $row->duration !!}" }'>
                                            <div class="eq-white eq-white-{{$row->id}}" style="display:none;">
                                                <span class="eq-bar eq-bar--1"></span> 
                                                <span class="eq-bar eq-bar--2"></span> 
                                                <span class="eq-bar eq-bar--3"></span> 
                                                <span class="eq-bar eq-bar--4"></span> 
                                                <span class="eq-bar eq-bar--5"></span> 
                                                <span class="eq-bar eq-bar--6"></span>
                                            </div>                                            
                                            <i class="la la-play play-white play-white-{{$row->id}}" style="font-size:30px;"></i>
                                        </a>
                                    </li>
                                </ul>
                              </td>
                             
                              <td>
                                  <div class="custom-card--inline-img">
                                       <img src="{{$row->image}}" alt="{{$row->name}}" class="card-img--radius-sm" style="max-width: 40px;">
                                   </div>
                              </td>
                        
                              <td>
                                   <div class="custom-card--inline-desc">
                                      <p class="text-truncate mb-0">{{$row->name}}</p>                                      
                                   </div>
                              </td>
                              <td>
                                  <div class="custom-card--inline-desc">
                                  <p class="text-truncate font-sm">
                                      
                                      <?php $artists = explode('|',$row->primary_artist); ?>
                                      @foreach($artists as $key => $artist)
                                      <a href="{{route('artist.details',['id'=>base64_encode($artist)])}}">{{ucfirst($artist)}}</a>
                                      @if((count($artists)-1)!=$key)
                                      | 
                                      @endif
                                      @endforeach
                                  
                                  </p>
                                  </div>
                              </td>
                              <td>
                                  <ul class="list-group list-group-horizontal">
                                        @if(!isset($userdata)) 
                                        <li class="list-group-item listBox">
                                            <a href="javascript:void(0);" data-toggle="modal" data-target="#singin" class="dropdown-link">
                                                 @if(isset($row->is_favorite) &&  $row->is_favorite==1)
                                                 <i class="la la-heart"></i>
                                                 @else
                                                 <i class="la la-heart-o"></i>
                                                 @endif
                                                
                                            </a>                                            
                                        </li>
                                       <!-- <li class="list-group-item listBox">                                            
                                            <a href="javascript:void(0);" data-toggle="modal" data-target="#singin" class="dropdown-link">
                                                <i class="la la-plus-square"></i>
                                             </a>
                                        </li>  -->                                      
                                        @else
                                        <li class="list-group-item listBox">
                                            <a href="javascript:void(0);" class="dropdown-link add_favorite add_favorite_status_{{$row->id}}"  data-track_id="{{$row->id}}" data-type="Track" data-status="@if($row->is_like==1) 0  @else 1 @endif"   > 
                                                <i class="Track_{{$row->id}} la @if($row->is_like==1) la-heart  @else la-heart-o @endif"></i>                                                
                                            </a>
                                        </li>
                                        
                                        <li class="list-group-item listBox">                                            
                                            <a href="javascript:void(0);"  class="dropdown-link  add_playlist" data-track_id="{{$row->id}}" data-type="Track" data-status="1">
                                                <i class="la la-plus-square"></i>
                                             </a>
                                        </li>                                        
                                        @endif
                                        <li class="list-group-item listBox">
                                            <a href="javascript:void(0);" class="dropdown-link add_share" data-track_id="{{$row->id}}" data-track_name="{{$row->name}}"  data-type="Track" data-status="1">
                                                <i class="la la-share-alt"></i>
                                            </a>
                                        </li>
                                    </ul>                                  
                              </td>
                              <td></td>
                              <td> {{$row->duration}}</td>
                              <td>
                                 <li class="dropleft">

                                    <a href="javascript:void(0);" class="btn btn-icon-only p-0 w-auto h-auto" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                                    <ul class="dropdown-menu" style="padding-left: 5px;">                                        
                                        <li class="dropdown-item">
                                           <a href="javascript:void(0);" class="dropdown-link songinfo">
                                               <i class="la la-info-circle"></i> 
                                               <span>Song Info</span>
                                           </a>
                                        </li>
                                        <li class="dropdown-item">
                                        <a href="javascript:void(0);" class="dropdown-link add_download" data-track_id="{{$row->id}}"  data-type="Track" data-status="1" >
                                            <i class="la la-download"></i>
                                            <span>Download</span>
                                        </a>
                                    </li>
                                    <li class="dropdown-item">
                                        <a href="javascript:void(0);" class="dropdown-link add_to_quee" data-track_id="{{$row->id}}"  data-type="Track" data-status="1" >
                                            <i class="la la-list-ul"></i>
                                            <span>Add To Queue</span>
                                        </a>
                                    </li>
                                    <li class="dropdown-item">
                                        <a href="javascript:void(0);" >
                                            <i class="la la-file-audio"></i>
                                            <span>Go To Album</span>
                                        </a>
                                    </li>
                                    </ul>
                                 </li>                                  
                              </td>                              
                          </tr>                       
                          @endforeach
                          @endif  
                          </tbody>
                        </table>                      
                        </div>
                  
                  <?php /*
                        <div class="custom-list">
                          @if(!empty($data['data']))
                          @foreach($data['data'] as $row)
                           <div class="custom-list--item">
                               <a href="javascript:void(0);" class="playsong"  data-audio='{"name":"{!! ucwords($row->name) !!}", "artist":"{!! ucwords($row->primary_artist) !!}", "album":"{!! ucwords($row->name) !!}", "url":"{!! $row->song !!}", "cover_art_url":"{!! $row->getRelease->image !!}" }'>

                              <div class="text-dark custom-card--inline">
                                 <div class="custom-card--inline-img">
                                     <img src="{{$row->getRelease->image}}" alt="{{$row->getRelease->name}}" class="card-img--radius-sm"></div>
                                 <div class="custom-card--inline-desc">
                                    <p class="text-truncate mb-0">{{$row->name}}</p>
                                    <p class="text-truncate text-muted font-sm">{{$row->composer}}</p>
                                 </div>
                              </div>
                               </a>
                              <ul class="custom-card--labels d-flex ml-auto">
                                 <li>
                                     <span class="badge badge-pill badge-warning">
                                         <i class="la la-star"></i>
                                     </span>
                                 </li>
                                 <li class="hard_icon_{{$row->id}}">
                                     @if($row->is_favorite==1)
                                     <span class="badge badge-pill badge-danger add_favorite" data-track_id="{{$row->id}}" data-type="Track" data-status="0"><i class="la la-heart"></i></span>
                                     @endif                                     
                                 </li>
                                 <li>{{$row->duration}}</li>
                                 <li class="dropleft">
                                    <a href="javascript:void(0);" class="btn btn-icon-only p-0 w-auto h-auto" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
                                    <ul class="dropdown-menu">
                                             @if(!isset($userdata))
                                     <li class="dropdown-item">
                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#singin" class="dropdown-link">
                                            <i class="la la-heart-o"></i>
                                             <span>Favorite</span>
                                        </a>
                                    </li>
                                    <li class="dropdown-item">
                                         <a href="javascript:void(0);" data-toggle="modal" data-target="#singin" class="dropdown-link">
                                            <i class="la la-plus"></i>
                                            <span>Add to Playlist</span>
                                         </a>
                                    </li>
                                    <?php /*<li class="dropdown-item">
                                         <a href="javascript:void(0);" data-toggle="modal" data-target="#singin" class="dropdown-link">
                                            <i class="la la-download"></i>
                                            <span>Download</span>
                                        </a>
                                    </li>
                                    *
                                    <li class="dropdown-item">
                                        <a href="javascript:void(0);" class="dropdown-link add_share" data-track_id="{{$row->id}}" data-track_name="{{$row->name}}"  data-type="Track" data-status="1">
                                            <i class="la la-share-alt"></i>
                                            <span>Share</span>
                                        </a>
                                    </li>
                                    @else
                                    <li class="dropdown-item">
                                        <a href="javascript:void(0);" class="dropdown-link add_favorite" data-track_id="{{$row->id}}" data-type="Track" data-status="1">
                                            <i class="la la-heart-o"></i>
                                             <span>Favorite</span>
                                        </a>
                                    </li>
                                    <li class="dropdown-item">
                                        <a href="javascript:void(0);" class="dropdown-link add_playlist" data-track_id="{{$row->id}}"  data-type="Track" data-status="1">
                                            <i class="la la-plus"></i>
                                            <span>Add to Playlist</span>
                                        </a>                                   
                                    </li>
                                   
                                   <?php /*  <li class="dropdown-item">
                                        <a href="javascript:void(0);" class="dropdown-link add_download" data-track_id="{{$row->id}}"  data-type="Track" data-status="1" >
                                            <i class="la la-download"></i>
                                            <span>Download</span>
                                        </a>
                                    </li> *
                                    @endif                                       
                                       <li class="dropdown-item">
                                           <a href="{{route('song.details',['id'=>base64_encode($row->id),'from'=>'track'])}}" class="dropdown-link">
                                               <i class="la la-info-circle"></i> 
                                               <span>Song Info</span>
                                           </a>
                                       </li>
                                    </ul>
                                 </li>
                              </ul>
                           </div>
                          @endforeach
                          @endif                        
                        </div> */?>
               </div>
               </div>
            </div> 
                       </div>
                      @else
                {!! ucfirst($data['message']) !!}
                @endif 
                  </div>
               </div>
@else
{!! ucfirst($category['message']) !!}
@endif
  @endsection