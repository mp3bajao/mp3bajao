@if(isset($data) && $data['status']==true)
<div class="search-card" data-scrollable="true" style="max-height: 400px; overflow-y: auto;">
    @if(isset($data['data']->genres[0]))
                        <div class="mb-3">
                           <div class="d-flex"><span class="text-uppercase mr-auto font-weight-bold text-dark">Genres</span> <a href="{{route('genres')}}">View All</a></div>
                           <hr>
                           <div class="row">
                               @foreach($data['data']->genres as $row)
                              <div class="col-xl-2 col-md-4 col-6">
                                 <div class="custom-card mb-3">
                                    <a href="{{route('song',['genres'=>base64_decode($row->id)])}}" class="text-dark">
                                       <img src="{{$row->image}}" alt="" class="card-img--radius-md">
                                       <p class="text-truncate mt-2">{{$row->name}}</p>
                                    </a>
                                 </div>
                              </div>
                               @endforeach                              
                           </div>
                        </div>    
                    @endif
                    
                    @if(isset($data['data']->artist[0]))
                        <div class="mb-3">
                           <div class="d-flex"><span class="text-uppercase mr-auto font-weight-bold text-dark">Artists</span> <a href="{{route('artist')}}">View All</a></div>
                           <hr>
                           <div class="row">
                               @foreach($data['data']->artist as $row)
                              <div class="col-xl-2 col-md-4 col-6">
                                 <div class="custom-card mb-3">
                                    <a href="{{route('artist.details',['id'=>base64_encode($row->id)])}}" class="text-dark">
                                       <img src="{{$row->image}}" alt="" class="card-img--radius-md">
                                       <p class="text-truncate mt-2">{{$row->name}}</p>
                                    </a>
                                 </div>
                              </div>
                               @endforeach                              
                           </div>
                        </div>    
                    @endif
                   
                          @if(isset($data['data']->track[0]))
                        <div class="mb-3">
                           <div class="d-flex"><span class="text-uppercase mr-auto font-weight-bold text-dark">Track</span> <a href="{{route('song')}}">View All</a></div>
                           <hr>
                           <div class="row">
                              
                               @foreach($data['data']->track as $row)

                              <div class="col-xl-4 col-md-6 col-12">
                                 <div class="custom-card mb-3">
                                    <a href="{{route('song.details',['id'=>base64_encode($row->id),'from'=>'Track'])}}" class="text-dark custom-card--inline">
                                        
                                       <div class="custom-card--inline-img">
                                           <img src="{{$row->getRelease->image ?? null}}" alt="" class="card-img--radius-sm">
                                       </div>
                                       <div class="custom-card--inline-desc">
                                          <p class="text-truncate mb-0">{{$row->name ?? null }}</p>
                                          <p class="text-truncate text-muted font-sm">{{$row->primary_artist ?? null}}</p>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                               @endforeach
                               
                             
                           </div>
                        </div>
                         @endif
                        <div>
                        
                            
                           @if(isset($data['data']->release[0]))
                           <div class="d-flex"><span class="text-uppercase mr-auto font-weight-bold text-dark">Albums</span> <a href="{{route('song')}}">View All</a></div>            
                           <hr>
                           <div class="row">
                              @foreach($data['data']->release as $row)
                                <div class="col-xl-4 col-md-6 col-12">
                                   <div class="custom-card mb-3">
                                      <a href="{{route('song.details',['id'=>base64_encode($row->id),'from'=>'Album'])}}" class="text-dark custom-card--inline">
                                         <div class="custom-card--inline-img"><img src="{{$row->image}}" alt="" class="card-img--radius-sm"></div>
                                         <div class="custom-card--inline-desc">
                                            <p class="text-truncate mb-0">{{$row->name}}</p>
                                         </div>
                                      </a>
                                   </div>
                                </div>
                              @endforeach                              
                           </div>
                           @endif
                        </div>
                        </div>

@endif
                         