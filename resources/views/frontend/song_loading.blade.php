
            @if(isset($data['data']->track))
                @foreach($data['data']->track as $row)
                  <div class="col-xl-2 col-lg-3 col-sm-4 pb-4">
                       <div class="custom-card">
                        <div class="custom-card--img">
                        <a href="{{route('song.details',['id'=>base64_encode($row->data->id),'from'=>$row->format])}}" class="playsong"  data-audio='{"id":{!! $row->data->track_id!!},"name":"{!! ucwords($row->data->name) !!}", "artist":"{!! ucwords($row->data->primary_artist) !!}", "album":"{!! ucwords($row->data->name) !!}", "url":"{!! $row->data->song !!}", "cover_art_url":"{!! $row->data->image !!}", "duration":"{!! $row->data->duration !!}" }'>
                            <div class="custom-card--info">                            
                              <div class="dropdown dropdown-icon">
                                <span class="playiconnew"><img src="{{asset('frontend/images/logos/iconPlay.svg')}}"</span>
                              </div>
                            </div>             
                            <img src="{!! $row->data->image !!}" alt="{!! ucwords($row->data->name) !!}" class="card-img--radius-lg">
                         <span class="listen">{{$row->data->total_views}}</span>       
                        </a>
                            
                        </div>
                        <a href="{{route('song.details',['id'=>base64_encode($row->data->id),'from'=>$row->format])}}" class="custom-card--link mt-2">
                            <h6 class="text-center"><small>{!! ucwords($row->data->name) !!}</small></h6>
                            <p class="text-center"><small>{!! ucwords($row->data->primary_artist) !!}</small></p>

                        </a>
                     </div>
                  </div>
                @endforeach
               @endif
               