<?php $userdata =  Session::get('userData');?>
@extends('layouts.frontend.master')
@section('content')
<style>
    .slider{
  width: 50%;
}
.slider {
  -webkit-appearance: none;
  width: 50%;
  height: 8px;
  background: #fe7f03;
  outline: none;
  opacity: 0.7;
  -webkit-transition: .2s;
  transition: opacity .2s;
  border-radius: 25px;
  margin-bottom: 25px
}

.slider:hover {
  opacity: 1;
}

.slider::-webkit-slider-thumb {
  -webkit-appearance: none;
  appearance: none;
  width: 20px;
  height: 20px;
  background: #009cff;
  cursor: pointer;
  border-radius: 25px;
}

.slider::-moz-range-thumb {
  width: 25px;
  height: 25px;
  background: #ccc;
  cursor: pointer;
  
}
#pageWrapper{
    background-color: #10131b !important
}
    
</style>

<div class="main-container" id="appRoute" style="background:url('{{asset('images/Rado_screen_background.png')}}');
    height: 1050px;
    background-size: 70% 100%;
">
    <form action="" method="get" id="radioForm">
        <select name="station_id" class="form-control station_id" >
        @foreach($station['data'] as $values)
        <option value="{{$values->id}}">{{ucfirst($values->name)}}</option>
        @endforeach
        </select>
    </form>
    <div class="row">
        <div class="col-md-12">
            <center><img src='{{asset('images/mp3bajaoradio.png')}}' width="30%"/></center>                 
        </div>
        <div class="col-md-12">
            <center>
                <input type="range" min="1" max="100" value="100" id="myNumber"  class="slider">
            </center>
        </div>
        <div class="col-md-12">
             <center><img src='{{asset('images/play.png')}}' class="playRadio" width="10%"/><img src='{{asset('images/pause.png')}}' class="pause" width="10%"/></center> 
        </div>
    </div>
<?php $array_track = array(); ?>
    @if($data['status']==true)
        @foreach($data['data']->track as $track)
            <?php $array_track[] = $track->track  ?>
        @endforeach   
        
    @else
    Radio not available
    @endif
    <?php $string = implode(',',$array_track); ?>

    
</div>

    <script>
            var string_value = "{{$string}}";
            var strings = string_value.split(",");
            var index = 1;
            audio = new Audio();
            audio.src=strings[0];
          

            audio.onended = function() {
                if(index < strings.length){
                    audio.src=strings[index];
                    audio.play();
                    index++;
                }
            };
            
      
       localStorage.setItem("AudioPlay",0);
        $(document).on('click','.playRadio',function(){
           var plays = localStorage.getItem("AudioPlay");
            if(parseInt(plays)==0)
            {
                audio.currentTime += parseInt("{{$data['data']->track[0]->skip ?? 0}}");   
                audio.play();  
                localStorage.setItem("AudioPlay",1);
            }
         });
         $(document).on('click','.pause',function(){
           audio.pause();   
         });
         
         
            $(document).on('change','.station_id',function(){
               $('#radioForm').submit();
            });
            
            var slider = document.getElementById("myNumber");
            slider.oninput = function() {
              audio.volume = (this.value/100);
            }
        
        
    </script>
     
@endsection