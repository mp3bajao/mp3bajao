@extends('layouts.master')

@section('content')
 
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}"> 
<!-- Content Header (Page header) -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">{{ __('Notifications Manage') }}</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">{{ __('Home') }}</a></li>
                <li class="breadcrumb-item active">{{ __('Notifications Manage') }}</li>
            </ol>
            @can('Celebrity-create')
              <a href="#" class="btn btn-primary d-none d-lg-block m-l-15" title="{{ __('Add Notification') }}" data-toggle="modal" data-target="#add_modal" ><i class="fa fa-plus"></i> {{ __('Add') }}</a>
            @endcan
        </div>
    </div>
</div>
<!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
    <div class="row">
        <div class="col-md-12">
        <div class="card card-primary card-outline">
                <div class="card-body">
                   
                    <div class="table-responsive">
                    <table  id="notification_listing" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>{{ __('Sr. no') }}</th>
                                <th>{{ __('Title') }}</th>
                                <th>{{ __('Notification Type') }}</th>
                                <th>{{ __('User Type') }}</th>
                                <th>{{ __('Message') }}</th>
                                <th>{{ __('Date') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
    </div>
</div>

</div>
    <!-- /.content -->

<!-- Modals -->

<div class="modal fade" id="add_modal">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
          <form method="POST" action="{{ url('api/notifications') }}" id="add_notification">
          @csrf
            <div class="modal-header">
              <h4 class="modal-title">Add New Notification</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">   
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label" for="user_type">User Type *</label>
                    
                    <select name="user_type" class="form-control" data-placeholder="Select User Type" style="width: 100%;" data-parsley-required="true" >
                        <option value=''>--Select User Type--</option>
                        @foreach ($user_type as $key => $user_type)
                            <option value="{{ $key }}">{{ $user_type }}</option>
                        @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label" for="notification_type">Notification Type *</label>
                    
                    <select name="notification_type" class="form-control" data-placeholder="Select Notification Type" style="width: 100%;" data-parsley-required="true" >
                        <option value=''>--Select Notification Type--</option>
                        @foreach ($notification_type as $key => $notification_type)
                            <option value="{{ $key }}">{{ $notification_type }}</option>
                        @endforeach
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label" for="title">Title *</label>
                    <input type="text" name="title" value="" id="title" class="form-control" placeholder="Title" data-parsley-required="true"  />
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label" for="message">Message *</label>
                    
                    <textarea name="message" id="message" class="form-control" placeholder="Message" data-parsley-required="true"></textarea>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary save"><span class="spinner-grow spinner-grow-sm formloader" style="display: none;" role="status" aria-hidden="true"></span> Save</button>
            </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->



<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/parsley.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>

<script>
var ajax_datatable;
$(document).ready(function(){
$('#add_notification').parsley();
ajax_datatable = $('#notification_listing').DataTable({
    processing: true,
    serverSide: true,
    ajax: '{{ url('api/notifications') }}',
    columns: [
      { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
      { data: 'title', name: 'title' },
      { data: 'notification_type', name: 'notification_type' },
      { data: 'user_type', name: 'user_type' },
      { data: 'message', name: 'message' },
      { data: 'created_at', name: 'created_at' },
    ],
    order: [ [5, 'desc'] ],
    rowCallback: function(row, data, iDisplayIndex) {  
      
     
    },
});

@can('Celebrity-create')
$("#add_notification").on('submit',function(e){
  e.preventDefault();
  var _this=$(this); 
    var values = $('#add_notification').serialize();
    $.ajax({
    url:'{{ url('api/notifications') }}',
    dataType:'json',
    data:values,
    type:'POST',
    beforeSend: function (){before(_this)},
    // hides the loader after completion of request, whether successfull or failor.
    complete: function (){complete(_this)},
    success:function(res){
          console.log(res);
          if(res.status === 1){ 
            toastr.success(res.message);
            $('#add_notification')[0].reset();
            $('#add_notification').parsley().reset();
            ajax_datatable.draw();
          }else{
            toastr.error(res.message);
          }
      },
    error:function(jqXHR,textStatus,textStatus){
      if(jqXHR.responseJSON.errors){
        $.each(jqXHR.responseJSON.errors, function( index, value ) {
          toastr.error(value)
        });
      }else{
        toastr.error(jqXHR.responseJSON.message)
      }
    }
      });
      return false;   
    });
@endcan
});
</script>

@endsection
