@if($content)
<div class="row">
 <div class="col-md-12">
  <table class="table table-striped table-bordered table-condensed" id="table" style="width: 100%;">
        <tr>
            <td><strong>Name:</strong></td>
            <td>{{$content->name}}</td>
        </tr>
        <tr>
            <td><strong>Description:</strong></td>
            <td>{!! $content->description !!}</td>
        </tr>
        
        <tr>
            <td><strong>Status:</strong></td>
            <td>
                @if($content->status  === 1)
                    Active
                @else
                    In-active
                @endif
            </td>
        </tr>
        <tr>
            <td><strong>Created At:</strong></td>
            <td>{{date('j F, Y', strtotime($content->created_at))}} </td>
        </tr>
  </table>
  </div>      
  </div>
@endif