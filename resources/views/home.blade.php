@extends('layouts.master')

@section('content')
<script type="text/javascript">
    var no_of_customers = <?php echo $no_of_customers; ?>;
    var no_of_chef = <?php echo $no_of_chef; ?>;
    var no_of_celebrity = <?php echo $no_of_celebrity; ?>;
    
</script>
<!-- Content Header (Page header) -->
  <div class="row page-titles">
      <div class="col-md-5 align-self-center">
          <h4 class="text-themecolor">{{ __('Dashboard') }}</h4>
      </div>
      <div class="col-md-7 align-self-center text-right">
          <div class="d-flex justify-content-end align-items-center">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ url('/') }}">{{ __('Home') }}</a></li>
                  <li class="breadcrumb-item active">{{ __('Dashboard') }}</li>
              </ol>
          </div>
      </div>
  </div>
<!-- /.content-header -->
<div class="card-group">
                <!-- Column -->
                <div class="card bg-light">
                    <div class="card-body">
                        <a class="round bg-danger" href="javascript:void(0)">
                                <i class="ti-user text-white"></i>
                            </a>
                        <h2 class="m-t-10 m-b-0">{{$no_of_customers}}</h2>
                        <small>Total Customers</small>
                    </div>
                </div>
                <div class="card ">
                    <div class="card-body">
                        <a class="round bg-info" href="javascript:void(0)">
                        <i class="fas fa-bread-slice"></i>
                            </a>
                        <h2 class="m-t-10 m-b-0">
                            <!-- <sup class="font-16">$</sup> -->
                        {{$no_of_chef}}</h2>
                        <small>Total Chef</small>
                    </div>
                </div>
                <!-- Column -->
               
                <div class="card bg-light">
                    <div class="card-body">
                        <a class="round bg-success" href="javascript:void(0)">
                                <i class="fas fa-star text-white"></i>
                            </a>
                        <h2 class="m-t-10 m-b-0">{{$no_of_celebrity}}</h2>
                        <small>Total Celebrity</small>
                    </div>
                </div>
                <div class="card ">
                    <div class="card-body">
                        <a class="round bg-primary" href="javascript:void(0)">
                                <i class="ti-shopping-cart text-white"></i>
                            </a>
                        <h2 class="m-t-10 m-b-0">{{$no_of_orders}}</h2>
                        <small>Total Orders</small>
                    </div>
                </div>
            </div>
<div class="card-group">
<!-- card -->
<div class="card o-income">
    <div class="card-body">
        <div class="d-flex m-b-30 no-block">
            <h4 class="card-title m-b-0 align-self-center">Daily Income</h4>
            <div class="ml-auto">
                <select class="form-control">
                    <option selected="">Today</option>
                    <option value="1">Tomorrow</option>
                </select>
            </div>
        </div>
        <div id="income" style="height:260px; width:100%;"></div>
        <ul class="list-inline m-t-30 text-center font-12">
            <li><i class="fa fa-circle text-success"></i> Growth</li>
            <li><i class="fa fa-circle text-info"></i> Net</li>
        </ul>
    </div>
</div>
<!-- card -->
<div class="card">
    <div class="card-body">
        <div class="d-flex m-b-30 no-block">
            <h4 class="card-title m-b-0 align-self-center">Users</h4>
            <!-- <div class="ml-auto">
                <select class="form-control">
                    <option selected="">Today</option>
                    <option value="1">Tomorrow</option>
                </select>
            </div> -->
        </div>
        <div id="visitor" style="height:260px; width:100%;"></div>
        <ul class="list-inline m-t-30 text-center font-12">
            <li><i class="fa fa-circle text-warning"></i> Chef ( {{$no_of_chef}} )</li>
            <li><i class="fa fa-circle text-danger"></i> Celebrity ( {{$no_of_celebrity}} )</li>
            <li><i class="fa fa-circle text-info"></i> Customer ( {{$no_of_customers}} )</li>
            
        </ul>
    </div>
</div>
<!-- card -->
<div class="card">
    <div class="p-20 p-t-25">
        <h4 class="card-title">Reports</h4>
    </div>
    <div class="d-flex no-block p-15 align-items-center">
        <div class="round round-info"><i class="icon-wallet font-16"></i></div>
        <div class="m-l-10 ">
            <h3 class="m-b-0">$18090</h3>
            <h6 class="text-muted font-light m-b-0">Your last year total Income</h6> </div>
    </div>
    <hr>
    <div class="d-flex no-block p-15 align-items-center">
        <div class="round round-primary"><i class="icon-umbrella font-16"></i></div>
        <div class="m-l-10">
            <h3 class="m-b-0">18%</h3>
            <h6 class="text-muted font-light m-b-0">Your site bounce rate</h6></div>
    </div>
    <hr>
    <div class="d-flex no-block p-15 m-b-15 align-items-center">
        <div class="round round-danger"><i class="icon-chart font-16"></i></div>
        <div class="m-l-10">
            <h3 class="m-b-0">21090</h3>
            <h6 class="text-muted font-light m-b-0">Your last year site visits</h6></div>
    </div>
</div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="card oh">
            <div class="card-body">
                <div class="d-flex m-b-30 align-items-center no-block">
                    <h5 class="card-title ">Yearly Sales</h5>
                    <div class="ml-auto">
                        <ul class="list-inline font-12">
                            <li><i class="fa fa-circle text-info"></i> New Order</li>
                            <li><i class="fa fa-circle text-primary"></i> Completed Order</li>
                        </ul>
                    </div>
                </div>
                <div id="morris-area-chart" style="height: 350px;"></div>
            </div>
            <div class="card-body bg-light">
                <div class="row text-center m-b-20">
                    <div class="col-lg-4 col-md-4 m-t-20">
                        <h2 class="m-b-0 font-light">6000</h2><span class="text-muted">Total sale</span>
                    </div>
                    <div class="col-lg-4 col-md-4 m-t-20">
                        <h2 class="m-b-0 font-light">4000</h2><span class="text-muted">New Order</span>
                    </div>
                    <div class="col-lg-4 col-md-4 m-t-20">
                        <h2 class="m-b-0 font-light">2000</h2><span class="text-muted">Completed Order</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
    <div class="row">
    <!-- column -->
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title text-success">Product Revenue</h5>
                <div class="stats-row m-t-20 m-b-20">
                    <div class="stat-item">
                        <h6>Growth</h6> <b>80.40%</b></div>
                    <div class="stat-item">
                        <h6>Montly</h6> <b>20.40%</b></div>
                    <div class="stat-item">
                        <h6>Daily</h6> <b>5.40%</b></div>
                </div>
            </div>
            <div id="sales1"></div>
        </div>
    </div>
    <!-- column -->
    <!-- column -->
    
    <!-- column -->
</div>
    </div>
</div>

   
@endsection

