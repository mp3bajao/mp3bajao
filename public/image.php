<?php 

    $image = base64_decode($_GET['image'] ?? '');
   // echo ($image);
   // die;
    $img = file_get_contents($image); 
    $percent = $_GET['size']  ?? 0.4;
    $im = imagecreatefromstring($img);	
    $width = imagesx($im);
    $height = imagesy($im);
    $newwidth = $width * $percent;
    $newheight = $height * $percent;
    $thumb = imagecreatetruecolor($newwidth, $newheight);
    imagecopyresized($thumb, $im, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
    header('Content-type: image/webp');
    imagewebp($thumb, null, 100);
    imagedestroy($thumb);
    ?>