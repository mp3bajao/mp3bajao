<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'url' => env('AWS_URL'),
            'endpoint' => env('AWS_ENDPOINT'),
        ],
        
        'backstage' => [
            'driver' => 's3',
            'key' => env('BACKSTAGE_AWS_ACCESS_KEY_ID'),
            'secret' => env('BACKSTAGE_AWS_SECRET_ACCESS_KEY'),
            'region' => env('BACKSTAGE_AWS_DEFAULT_REGION'),
            'bucket' => env('BACKSTAGE_AWS_BUCKET'),
            'url' => env('BACKSTAGE_AWS_URL'),
            'endpoint' => env('AWS_ENDPOINT'),
        ],
        'backstagevideo' => [
            'driver' => 's3',
            'key' => env('BACKSTAGE_VIDEO_AWS_ACCESS_KEY_ID'),
            'secret' => env('BACKSTAGE_VIDEO_AWS_SECRET_ACCESS_KEY'),
            'region' => env('BACKSTAGE_VIDEO_AWS_DEFAULT_REGION'),
            'bucket' => env('BACKSTAGE_VIDEO_AWS_BUCKET'),
            'url' => env('BACKSTAGE_VIDEO_AWS_URL'),
            'endpoint' => env('AWS_ENDPOINT'),
        ],
          'linode'      => [
                'driver'     => 's3',
                'key'        => env('LINODE_S3_ACCESS_KEY'),
                'secret'     => env('LINODE_S3_SECRET'),
                'endpoint'   => env('LINODE_S3_ENDPOINT'),
                'region'     => env('LINODE_S3_REGION'),
                'bucket'     => env('LINODE_S3_BUCKET'),
                'url'        => env('LINODE_S3_BUCKET_URL'),
                'visibility' => 'public',
            ],
        
        
        /*'backstage' => [
            'driver'   => 'sftp',
            'host'     => '44.242.117.91',
            'username' => 'backstage',
            'password' => 'backstage@2005',
            'timeout'  => 300,
            'root'     => '/files/',
           // 'root'      =>'/var/sftp',
            'visibility' => 'public',
            'permPublic' => 0755,
        ],*/
        
    ],

    /*
    |--------------------------------------------------------------------------
    | Symbolic Links
    |--------------------------------------------------------------------------
    |
    | Here you may configure the symbolic links that will be created when the
    | `storage:link` Artisan command is executed. The array keys should be
    | the locations of the links and the values should be their targets.
    |
    */

    'links' => [
        public_path('storage') => storage_path('app/public'),
    ],

];
